Title: Paramilitares amenazan a defensores  de DD.HH en Ciudad Bolivar y Fontibón
Date: 2016-04-13 12:29
Category: DDHH, Nacional
Tags: Amenazas, Panfletos, paramilitares
Slug: aparecen-dos-panfletos-mas-en-bogota-del-bloque-capital-de-las-aguilas-negras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Ciudad-Bolivar-e1460565457378.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Ciudad Bolívar] 

###### [Abr 13 2016] 

En las últimas horas se dieron a conocer dos panfletos más del Bloque Capital de las Aguilas Negras, uno en la localidad de Fontibón y otro en la Localidad de Ciudad Bolívar en donde se amenaza a la ciudadanía y a defensores de derechos humanos por sus actividades en el territorio.

El panfleto que apareció en la Localidad de Fontibón establece un t**oque de queda desde las 11:00 p.m y advierte que toda aquella persona que se encuentre a esa hora por la calle será asesinada**. En el panfleto se nombra los barrios Veracruz, Villa Beatriz y La Cabaña, lugares en donde estos grupos controlarían a la población.

El segundo panfleto aparece en la Localidad de Ciudad Bolívar y sentencia a muerte a todas aquellas personas defensoras de derechos humanos y participantes en la Red de Organizaciones y a la Mesa Distrital de Ciudad Bolivar, s**e les advierte que a partir de la divulgación del panfleto se convierten en objetivo militar** junto con todos aquellos que los acompañen. Los nombres de las personas que aparecen son: William Peña, Amalia Peña, Jorge Ariza Alba, Marina Quiñones y Eliana Rocio Alvarado.

En ambos panfletos estos grupos identificados como El Bloque Capital de las Aguilas Negras dicen que recuperarán en control de los territorios y a**cusan a los defensores de derechos humanos, a los militantes de izquierda y líderes comunales de ser guerrilleros.**

Sobre el paramilitarismo,  el  **Ministro de Defensa Luís Carlos Villegas**, ha dicho a medios de información "En el país no hay paramilitarismo ni permitiremos que vuelva a aparecer". Por otra parte las organizaciones de derechos humanos evidenciaron ante la CIDH[ la vigencia del paramilitarismo](https://archivo.contagioradio.com/ante-la-cidh-se-exige-al-gobierno-enfrentar-fenomeno-paramilitar/) en diferentes regiones de Colombia.

[![amenazas paramilitares](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/amenazas-paramilitares.jpg){.aligncenter .size-full .wp-image-22651 width="600" height="400"}](https://archivo.contagioradio.com/aparecen-dos-panfletos-mas-en-bogota-del-bloque-capital-de-las-aguilas-negras/amenazas-paramilitares/)
