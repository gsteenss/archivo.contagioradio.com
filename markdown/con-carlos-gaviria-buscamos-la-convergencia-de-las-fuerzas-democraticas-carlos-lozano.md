Title: Con Carlos Gaviria buscamos “la convergencia de las fuerzas democráticas” Carlos Lozano
Date: 2015-04-01 21:10
Author: CtgAdm
Category: Entrevistas, Política
Tags: carlos gaviria, Carlos Lozano, Partido Comunista, Polo Democrático Alternativo
Slug: con-carlos-gaviria-buscamos-la-convergencia-de-las-fuerzas-democraticas-carlos-lozano
Status: published

######  Foto: youtube.com 

<iframe src="http://www.ivoox.com/player_ek_4296419_2_1.html?data=lZemmJmVfY6ZmKiakp2Jd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh9DijKjO1NHTt4y7wtvW1M7Fb8Pp1MjOz9TXb4a5k4qlkoqdh83VjMjc0NvJtsjZz8jWw5DIqYzgwtiYyJKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Carlos Lozano, Director del Semanario Voz] 

*“Yo por supuesto estoy altamente conmovido con el fallecimiento del maestro Carlos Gaviria” afirma Carlos Lozano, dirigente político de la izquierda colombiana.*

El director del Partido Comunista Colombiano, periodista y también director de Semanario Voz, fue una de las personas que logró, hace cerca de dos décadas, que los esfuerzos de unidad confluyeran en  la creación del Polo Democrático Alternativo, luego de inmensos esfuerzos de la mano con Carlos Gaviria y otros dirigentes políticos de la izquierda colombiana.

Para Lozano es una lástima lo que sucedió al interior del Polo cuando se dividió y según él, también fue un golpe muy fuerte para Carlos Gaviria que vio en estos hechos un golpe a uno de sus principales ideales o esfuerzos.

Para Lozano es inevitable que la paz obligue a que se fortalezca y se construya la unidad. “la paz a pesar de todas las dificultades que tiene se va a dar… y ese desafío le pone un reto a la izquierda y a los sectores democráticos” sería inadmisible que no se asumieran esos retos que van a requerir cambios muy profundos, señala Lozano.

Afirma también que el legado de Gaviria tiene mucho de fondo en la posibilidad de la unidad y el Polo Democrático Alternativo, que sigue siendo un referente histórico para la izquierda democrática en Colombia, eso, el POLO, lo “va a tener que reafirmar en el próximo congreso”
