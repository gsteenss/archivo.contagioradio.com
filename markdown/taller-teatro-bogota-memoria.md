Title: Un taller teatral para la memoria y contra el olvido
Date: 2016-11-08 12:57
Category: eventos
Tags: Bogotá, Corporación Colombiana de Teatro, memoria, teatro
Slug: taller-teatro-bogota-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/maxresdefault-1-e1478627528708.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Teatro Malayerba 

##### 6 Nov 2016 

Con el el objetivo de enseñarles a los participantes pautas que les permitan indagar en las memorias de los dramaturgos, actores y crear vínculos entre lo vivido y lo representado, inicia en Bogotá el taller teatral “**Memoria y olvido en la Acción Dramática**”, una apuesta pedagógica que tendrá lugar entre el 7 y el 15 de este mes de 4 a 8 p.m. Le puede interesar: [La obra de Cortázar llega al Teatro Quimera](https://archivo.contagioradio.com/cortazar-llega-al-teatro-quimera/).

El evento, organizado por la Corporación Colombiana de Teatro y la Sala Seki Sano, cuenta con la participación especial del maestro de las tablas **Aristídes Vargas, fundador y director de “Malayerba” agrupación teatral latinoamericana proveniente de Ecuador**, que en su propuesta trae una serie de ejercicios que siven como "instrumento para indagar en la memoria, suponemos también, que donde están los momentos felices y los más dolorosos de nuestra existencia”

Durante las 64 horas que dura el taller, se abordarán algunos temas relacionados con el campo de la realización teatral como lo son la **escritura, un detalle impreciso, cuerpo y memoria, improvisaciones precarias e imágenes secundarias**. Los interesados en obtener más información pueden comunicarse al correo tallerpermanentecct@gmail.com o a la página web de la Corporación Colombiana de Teatro.
