Title: Firmante de paz, Plutarco Cardozo fue asesinado en cercanías de ETCR en Guaviare
Date: 2020-10-13 22:36
Author: AdminContagio
Category: Actualidad, DDHH
Tags: asesinato de excombatientes, Guaviare
Slug: plutarco-cardozo-asesinado-guaviare
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/10/Plutarco-Cardozo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Colectivo Radial y Cultural Localizate / FARC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El pasado 12 de octubre fue asesinado el excombatiente de [FARC](https://twitter.com/PartidoFARC) Plutarco Cardozo de 46 años a tan solo cuatro kilómetros del Espacio Territorial de Capacitación y Reincorporación (ETCR) Jaime Pardo Leal del municipio de San José del Guaviare. El Partido Fuerza Alternativa Revolucionaria del Común ya ha alertado en varias ocasiones sobre la vulnerabilidad que existe sobre la población de firmantes que viven por fuera de estas zonas.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Los hechos se dieron cerca de las 8:00 pm cuando un sujeto que llegó hasta a su casa en Colinas y le disparó en varias ocasiones con arma de fuego. El partido advierte que este homicidio en proximidades de un ETCR, se da en un área cercana a puntos permanentes de presencia constante de la Fuerza Pública. [(Lea también: Alexander Parra, representante ambiental y excombatiente es asesinado en ETCR)](https://archivo.contagioradio.com/alexander-parra-representante-ambiental-y-excombatiente-es-asesinado-en-etcr/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ya las Naciones Unidas habían alertado a través de su informe trimestral cómo dos de tres tercios de los excombatientes de FARC, que corresponden a cerca del 70% de la población desmovilizada habitan por fuera de los ETCR. Población que según el Consejo Nacional de Reincorporación, **no cuenta con garantías de vida ni económicas.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/PartidoFARC/status/1315984753238179841","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/PartidoFARC/status/1315984753238179841

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Con respecto a la seguridad en los espacios territoriales y las viviendas aledañas, Pastor Alape, delegado al Consejo Nacional de Reincorporación, ha señalado en ocasiones anteriores que la seguridad no puede darse con un enfoque militarista, sino que por el contrario **"hay que desarrollar un enfoque en derechos humanos y reconciliación, de nada sirve un soldado o un policía en el territorio cuando desde la institucionalidad se promueve la polarización"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Con el asesinato de Plutarco Cardozo, según el Partido FARC son 232 los excombatientes asesinados desde la firma del Acuerdo de Paz, para la ONU el registro se acerca a los 224 firmantes de paz y 20 los que han sido víctimas de desaparición. [Con Cristian Sánchez ascienden a 230 los firmantes de paz asesinados](https://archivo.contagioradio.com/con-cristian-sanchez-ascienden-a-230-los-firmantes-de-paz-asesinados/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
