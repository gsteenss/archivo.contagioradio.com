Title: Día del estudiante caído
Date: 2019-06-08 06:00
Author: AdminContagio
Category: Memoria, Movilización
Tags: Gonzalo Bravo, Movimiento estudiantil, Uriel Gutiérrez
Slug: dia-del-estudiante-caido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/dia-del-estudiante-caido-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

El **8 y 9 de junio se conmemora en Colombia el día del estudiante caído, una fecha que recuerda a los estudiantes que han sido asesinados** o desaparecidos en la historia del movimiento estudiantil.

### **Hagamos Memoria** 

El 7 de junio de 1929, los estudiantes se movilizaban por Bogotá protestando contra el gobierno de Abadía Méndez, rechazando la masacre de las bananeras, perpetrada un año atrás y denunciado la crisis económica en la que se encontraba el país. Cuando los estudiantes pasaban por el Palacio de Nariño, fueron víctimas de ráfagas disparadas por La Policía, el estudiante **Gonzalo Bravo Pérez, cayó sin vida en medio de la multitud alcanzado por una bala**, siendo el primer estudiante asesinado por la Fuera Pública en Colombia.

En homenaje a la memoria de Bravo Pérez, cada 8 de junio se celebraba el día de los estudiantes con una movilización, 25 años después, mientras los estudiantes regresaban al campus de la Universidad Nacional, la Policía ordenó el despeje de la manifestación. Cuando los estudiantes se rehusaron, de nuevo las balas fueron utilizadas para disipar la marcha, acabando **con la vida de Uriel Gutiérrez, estudiante de Filosofía y Medicina de la universidad Nacional**.

Al día siguiente, el 9 de junio, una gran movilización de estudiantes con pañuelos blancos se dirigía a la Plaza de Bolívar para exigir justicia en el asesinato de Uriel Gutiérrez, sin embargo, sobre las 11.00 am Tropas del Batallón Colombia abrieron fuego contra los estudiantes, asesinando a Álvaro Gutiérrez Góngora, Hernando Ospina López, Jaime Pacheco, Hugo León Velásquez, Hernando Morales, Elmo Gómez, Jaime Moore, Rafael Chávez y Carlos Grisales.

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fsinolvidoesmemoria%2Fvideos%2F442697153211034%2F&amp;width=500&amp;show_text=false&amp;appId=894195857389402&amp;height=280" width="500" height="280" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
