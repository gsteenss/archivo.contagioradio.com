Title: Peñalosa tendrá que responder sobre venta de ETB en Cabildo Abierto
Date: 2017-02-27 17:52
Category: DDHH, Economía, Nacional
Tags: ETB, revocatoria Peñalosa
Slug: penalosa-tendra-que-responder-sobre-venta-de-etb-en-cabildo-abierto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/penalosa-enr07pvg003x.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Archivo Particular ] 

###### [27 Feb 2017] 

Este 28 de febrero se llevará a cabo el Cabildo Abierto contra la venta de la Empresa de Telefonía Pública de Bogotá, ETB, que pretende explicar los motivos por los cuales la venta de la empresa sería una perdida para la capital. **A este escenario deberá asistir el Alcalde Enrique Peñalosa y podrá ingresar cualquier ciudadano.**

De acuerdo con Alejandra Wilches, presidenta de Asociación Nacional  de Técnicos de Telecomunicaciones de la ETB (ATELCA), la administración de Peñalosa ha demostrado que va a seguir en el mismo rumbo de **“privatizar todos los aspectos de la ciudad” generando que la ciudadanía se movilice en diferentes escenarios para buscar reversas sus decisiones o revocarlo de la alcaldía.**

De igual forma Wilches afirma que este Cabildo también es un mensaje para el Consejo de Bogotá, porque a través de su votación dieron vía libre para que la venta de la ETB se realizara, **sin tener en cuenta las diferentes movilizaciones y expresiones de la ciudadanía en rechazo a esta acción.** Le puede interesar:["60 mil firmas recolectadas para Cabildo Abierto contra venta de ETB"](https://archivo.contagioradio.com/60-mil-firmas-recolectadas-para-cabildo-abierto-contra-venta-de-etb/)

Cabe resaltar que los **cabildos abiertos no son escenarios de toma de decisión**, sino que permiten discutir temas, motivo por el cual otras organizaciones como Sintrateléfonos están analizando la posibilidad de hacer una consulta popular, que si es facultativa. Le puede interesar:["Acción de nulidad de Sintrteléfonos podría frenar venta de la ETB" ](https://archivo.contagioradio.com/accion-de-nulidad-de-sintratelefonos-podria-frenar-venta-de-la-etb/)

El evento se llevará a cabo en el Instituto de Recreación y Deporte, **a partir de las 9:00am, el ingreso se hará hasta completar los cupos del auditorio** y es convocado por los sindicatos de trabajadores de la ETB.

<iframe id="audio_17247992" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_17247992_4_1.html?c1=ff6600"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
