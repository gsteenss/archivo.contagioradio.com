Title: "Mi Pueblo" un documental liberador sobre el padre Bernardo Marín
Date: 2015-11-23 16:51
Author: AdminContagio
Category: 24 Cuadros, eventos
Tags: Documental "Mipueblo" Padre Bernardo Marin, Sacerdote Bernardo Marin, San Vicente de Chucurí, Teología de la liberación documental
Slug: mi-pueblo-un-documental-liberador-sobre-el-padre-bernardo-marin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/IMG-20151113-WA0001-e1448315393764.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [23 Nov 2015]

Este 23 de noviembre se estrena ‘Mi Pueblo’, el documental que recorre la vida del sacerdote Bernardo Marín, un personaje que despertaba la admiración por parte de la población de San Vicente de Chucurí y recelo de quienes veían su trabajo con la comunidad y sus denuncias como una amenaza a intereses económicos.

La cinta dirigida por Angie Osorio y Yeimy Daza, tiene la tarea de integrar momentos de la historia nacional, entre los que se encuentran el avance de la incursión paramilitar, el olvido del Estado hacia las zonas más alejadas de los cascos urbanos y la corrupción, con la labor del religioso.

El padre Bernardo Marín impulsó la organización de las comunidades, fomentando labores de expresiones artísticas con jóvenes, creó las Comunidades Cristianas Campesinas y a la par que trabajaba con diversos sectores de la población.

Debido a sus constantes denuncias a particulares, funcionarios del Estado y grupos paramilitares, se orquestó un atentado en contra suya, del que pudo salir con vida. A partir de allí, estuvo en Canadá y luego en Brasil; en donde realizó estudios dedoctorado en Teología e integró al departamento de formación ideológica de Movimiento de los Trabajadores Rurales Sin Tierras del Brasil.

‘Mi Pueblo’, es un homenaje al sacerdote que creía en la justicia, la inclusión social y en la filosofía de la ‘Teología de la Liberación’ del cura Camilo Torres. El documental nos brinda la oportunidad de reconocer a un hombre, que antes que nada, creía en las personas; creía en su comunidad. La cita es en el Auditorio Menor Alfonso Gómez Gómez de la Universidad Autónoma de Bucaramanga a partir de las 6:10 p.m.
