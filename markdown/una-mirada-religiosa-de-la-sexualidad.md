Title: Una mirada religiosa de la sexualidad que genera afecciones
Date: 2020-03-30 18:08
Author: CtgAdm
Category: A quien corresponda, Opinion
Tags: a quien corresponda, Biblia, pecados, Religión, sexualidad
Slug: una-mirada-religiosa-de-la-sexualidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

***No hagas a nadie lo que no quieres que te hagan.***

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"right"} -->

(Tobías 4, 15)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Bogotá, 13 de marzo 2020

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*El amor no se impone por la fuerza, la autoridad o el poder.* *El mensaje de Jesucristo es armonía, verdad, justicia, y amor. *

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estimado

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### **Hermano en la fe**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Cristianas, cristianos, personas interesadas

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cordial saludo, 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La sexualidad, es otra de las realidades difíciles de abordar, honesta e informadamente, con tu círculo religioso y político. Esa forma religiosa de abordarla genera afectaciones y conflictos en muchos creyentes y distorsiona la comprensión del ser humano y de la fe.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Escribo, aunque no sea experto, con la esperanza de generar discusiones honestas y responsables que permitan recuperar una visión positiva de la fe, de la afectividad y la sexualidad, es decir, una visión más cristiana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hablaré de sexualidad en general, aunque se debe diferenciar entre la sexualidad como genitalidad (para referirse a lo relacionado con los órganos genitales masculinos y femeninos) y sexualidad como afectividad (para hablar del ser y la manera de relacionarnos marcada por la condición sexual).  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Unas corrientes cristianas han abordado la sexualidad de manera negativa, “oscura” y cerrada, relacionándola, casi exclusivamente, con el pecado y el infierno.

<!-- /wp:heading -->

<!-- wp:paragraph -->

En las “predicaciones”, hablan de la sexualidad como “el pecado” con una exageración y minuciosidad, a veces, obsesiva y escandalosa. En estas corrientes, prevalece una visión negativa de lo humano, de la afectividad, del placer y del amor. Separan la sexualidad del amor. Parecen desconocer que la sexualidad es parte integrante del ser humano *“creado a imagen y semejanza de Dios”* (Génesis 1,27). Si la sexualidad es obra de Dios no puede ser mala en sí misma, aunque haya manejos y “usos” de la sexualidad que dañan la dignidad humana, que degradan al ser humano y por eso están en contra del querer de Dios para sus hijos e hijas.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta comprensión de la sexualidad, impide hablar abierta, respetuosa y tranquilamente de ella en la familia, la escuela, la iglesia y la sociedad. El silencio estimula actitudes morbosas y dañinas que la desfiguran y crea una moral deformada, que dice una cosa, piensa otra, hace otra y siente otra. Esta realidad genera conflictos personales, morales y religiosos; culpabilidades insanas y patologizantes; daños profundos e irreparables en las personas; disfunciones y traumas sexuales que afectan las relaciones de pareja; complejos y conflictos silenciados que se agudizan por el miedo al juicio social y religioso, por el miedo al infierno.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Estas patologías y traumas sexo-afectivas pueden “estimular” tendencias al abuso y a la violencia sexual, lo mismo que el silencio frente a ellos. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las distorsiones, creencias, patologías y traumas relacionados con la sexualidad y la afectividad se pueden trasmitir de padres a hijos, inconscientemente y con lenguajes no verbales, si los padres y las madres no “sanan” y abordan los interrogantes e inquietudes con sus hijas e hijos, tranquilamente, sin generar desconfianza, inseguridades y miedos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esta moral que considera todo lo relacionado con la sexualidad (sentimientos, pensamientos, preguntas, miradas, atracciones…) como pecado grave y siempre malo, puede llevar a culpabilizaciones y miedos, que agravados, generan depresiones y daños humanos profundos. Además, esta moral usa una doble medida para juzgar los mismos hechos, dependiendo si los hacen los hombres o las mujeres; hay comportamientos o actitudes que consideran “normales” y religiosamente aceptables si los realizan los hombres, pero no las mujeres.  Dos ejemplos ilustran la forma como este tipo de moral agudizan los problemas: 

<!-- /wp:paragraph -->

<!-- wp:list -->

-   El primero, en el proceso evolutivo de niñas y niños, hay una etapa (de los 3 a 5 años), en la que empiezan a descubrir la diferencia de sus genitales; sin maldad y malicia exploran, se miran y tocan entre hermanos, primos y amiguitos de su misma edad. A medida que crecen aprenden a valorar sus acciones como buenas y malas, escuchan y asumen los mensajes religiosos y morales que les enseñan lo que está bien y lo qué está mal. Si en este proceso de crecimiento se encuentran con esta corriente religiosa que ve todo lo relacionado con la sexualidad como **lo más** malo y pecaminoso; los niños y las niñas pueden reinterpretar esos hechos, realizados sin malicia y sin maldad, como acciones gravísimas o perversas, se ven a sí mismas como personas malas, indignas ante Dios y los demás, lo cual puede generar traumas, depresiones y baja autoestima. 

<!-- /wp:list -->

<!-- wp:list -->

-   El segundo ejemplo, a una persona víctima de abuso o violencia sexual, se le pueden agravar las afectaciones humanas, psicológicas, espirituales, morales, relacionales y afectivas por esta compresión religiosa y social: de un lado, por el ambiente social sumamente erotizado, que habla constantemente, idealizando la sexualidad, es decir, como una realidad siempre hermosa y sin problemas, a veces, como manera de esconder los traumas por parte de quienes más hablan del tema;  de otro lado, por las predicaciones de estas corrientes religiosas que consideran “los pecados sexuales” como los únicos y los más graves, llevan a las víctimas a considerarse “la persona más mala y pecadora del mundo” (cargando sobre ella la culpa que le corresponde al agresor o victimario). Esta realidad profundiza las afectaciones, genera miedo para hablar de lo que ha pasado e inseguridad para buscar ayuda. Además, impide que personas que tienen “tendencias” que las pueden convertir abusadores, reconozcan sus patologías y traumas y busquen ayuda, aumentando la posibilidad de llegar a ser abusadores y violadores. Si un problema no se reconoce y se nombra, no se puede solucionar. 

<!-- /wp:list -->

<!-- wp:heading {"level":5} -->

##### Para las personas creyentes, esta manera de asumir y abordar la sexualidad, impide que busquen ayuda para resolver traumas y afectaciones.

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pecado y el miedo al infierno puede conducir a una especie de esquizofrenia (separación) entre lo que se piensa, cree, se vive y siente; ocultando o camuflando realidades que se pueden superar, haciéndolas más dañinas. Cuando traumas y afectaciones, que pueden llevar al abuso o violencia sexual, las viven pastores, sacerdotes o personas con papeles religiosos relevantes, es mucho más difícil su aceptación y por lo tanto su tratamiento y solución. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estas corrientes cristianas, tienen contradicciones profundas con el mensaje de la Palabra de Dios, con la Biblia que poco habla del tema. Para comprobar esta afirmación  revisé en la página [BibliaTodo](https://www.bibliatodo.com/concordancia-biblica) palabras que pueden relacionarse con la sexualidad y el **número de veces** que aparece en la Biblia,  el resultado fue: sexo: *2*, adulterio: *20*, fornicación: *40*, adulterio: *20*, adúltera: *21*, adúltero: *13*, prostitución: *4*, prostituido: *2*, infidelidad: *4*, impureza: *10*, infiel: *7*; no aparecen las palabra sexual, sexuales, sexualidad o prostituta. Hay 11 pasajes relacionados con la sexualidad sin nombrarla.  Las palabras pecado y pecadores aparecen 590 veces, varias de ellas referida a relaciones sexuales indebidas, pero la mayoría de las veces se refieren a la desobediencia a Dios, a la esclavitud, a la idolatría, a la falta de ayuda al pobre y necesitado, a la soberbia, a la injusticia o a romper la alianza. Cómo contraste revisé las siguientes palabras: amor: *305*, amar: *144*, améis: *18*, amado *131*, ayuda: *122*, Justo: *305*, Justicia: *537*, derecho: *175* (con frecuencia justicia y derechos aparecen juntos), injusticia: *26*, injusto: *14*; tierra aparece *2661* veces. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Una mirada integral a la Palabra de Dios, a la Biblia, muestra que la importancia que le da a los temas relacionados con la sexualidad es muy poca en relación con la abundancia de la predicación de estas corrientes religiosas. En cambio, temas relacionados con la justicia, el derecho, el amor, la libertad, la ayuda o la tierra son abundantes en la Biblia y casi ausentes en su predicación. Esta evidencia debería llevarnos a ampliar la comprensión del pecado y abordar la sexualidad con naturalidad y respeto en la familia, la escuela, la Iglesia y la sociedad.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Fraternalmente, su hermano en la fe,

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

P. Alberto Franco, CSsR, J&P <francoalberto9@gmail.com>

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver mas: [A quien corresponde](https://archivo.contagioradio.com/author/a-quien-corresponde/)  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  

<!-- /wp:paragraph -->
