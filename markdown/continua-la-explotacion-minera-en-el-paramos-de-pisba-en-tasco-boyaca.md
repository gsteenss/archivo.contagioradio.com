Title: Continúa la explotación minera en el Páramo de Pisba en Tasco - Boyacá
Date: 2016-08-05 13:55
Category: Ambiente, Nacional
Tags: Páramo de Pisba
Slug: continua-la-explotacion-minera-en-el-paramos-de-pisba-en-tasco-boyaca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/páramo-de-Pisba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: VemosyEscuchamos] 

###### [5 de Agost] 

Pese a las múltiples advertencias de diferentes organizaciones ambientales sobre la explotación minera en el Páramo de Pisba, [la sentencia de la Corte Constitucional que ordena protegerlos](https://archivo.contagioradio.com/delimitacion-de-paramos-en-colombia-nuevos-paramos/) y los daños irreversibles que esta causando al ecosistema, **la extracción de carbón continúa afectado a 500 familias y a la fauna y flora de lugar.**

De acuerdo con Anastasio Cruz, miembro de la Red de Acueductos Rurales, la explotación al Páramo de Pisba se hace desde aproximadamente 12 años y como **saldo ha dejado secos 22 nacimientos de agua, la quebrada La Humadera y un daño medioambiental irreparable como la muerte de frailejones**, a su vez índico que la comunidad en este momento se encuentra en defensa del territorio por la posible re activación de una mina de hierro por parte de la empresa  Acerías Paz del Río que cuenta con 100 hectáreas en el posible lugar de explotación.

"Hay agrietamiento en la mayoría de las fincas cercanas. Lo ideal sería que las autoridades tomaran cartas en el asunto y que **eviten que nos saquen de nuestros terrunios, porque realmente son tierras de nosotros desde nuestros ancestros"**.afirmo Cruz.

De otro lado, la Corporación Autónoma Ambiental de Boyacá, CORPOBOYACA, índico que aún existen licencias mineras vigentes en el Páramo de Pisba, de igual modo, según la organización CENSAT Agua Viva en la actualidad hay [466 títulos mineros concedidos en ecosistemas de páramos en Colombia](https://archivo.contagioradio.com/400-tituloa-mineros-seran-retirados-por-fallo-de-la-corte-constitucional/).

Durante los días **13, 14 y 15 se va a llevar a cabo el Tercer Encuentro Nacional de Defensores de Páramos "Territorios y Soberanía"**, con la finalidad de hacer un llamado al gobierno colombiano **para que frene y atienda las diferentes porblemáticas ambientales que ha generado la explotación de minería en páramos**. El encuentro contará con la participación de diferentes comunidades y se llevará a cabo en Tasco - Boyacá, en el centro social y de encuentro Suscriptores acueducto San Isidro.

<iframe src="http://co.ivoox.com/es/player_ej_12454439_2_1.html?data=kpehl5mYd5qhhpywj5aWaZS1kZyah5yncZOhhpywj5WRaZi3jpWah5yncaLiwtjhw9jNs4y309rnh5enb7PZxZDRx5Clp9bZxdrQ1tTXb7Pp08bZx9iRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 

   
   
 
