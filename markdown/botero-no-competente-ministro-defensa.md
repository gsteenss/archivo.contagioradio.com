Title: Botero no es una persona competente ni ética para ser ministro de Defensa: David Racero
Date: 2019-06-10 23:06
Author: CtgAdm
Category: Entrevistas, Política
Tags: Cámara de Representantes, Fuerzas militares, Guillermo Botero, Moción de Censura
Slug: botero-no-competente-ministro-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/Ministro-Guillermo-Botero.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @camaracolombia] 

Este lunes se desarrolló en el Congreso de la República el debate  de moción de censura contra el **ministro de defensa, Guillermo Botero**; allí, los citantes al debate presentaron algunas de las razones para intentar que Botero deje el cargo por su incompetencia para manejar la cartera. Entre los hechos por los que el Ministro está en el centro de la opinión pública están la directriz denunciada por el diario New York Times que podría volver a producir ejecuciones extra judiciales, y el manejo que se dió al asesinato de Dimar Torres por integrantes del Ejército.

### **La moción de censura de Botero era un tema que se venía trabajando en el Congreso**

Congresistas de la oposición habían citado para el pasado miércoles 15 de mayo el debate de moción de censura contra Botero por sus declaraciones en el caso de Dimar Torres; un excombatiente de las FARC que estaba cumpliendo con su proceso de reincorporación y fue asesinado por un integrante del Ejército. Tras la noticia, el Ministro excusó al uniformado del asesinato de Torres, afirmando que los hechos ocurrieron en medio de un forcejeo.

Los hechos denunciados por la comunidad apuntaban a un asesinato e intento de desaparición del cuerpo; posteriormente, el militar implicado en el homicidio afirmó que el [Ejército estaría haciendo seguimientos irregulares al excombatiente](https://archivo.contagioradio.com/militar-implicado-senala-que-ejercito-ordeno-seguir-a-dimar-torres/). En ese momento,  los citantes a ese debate decidieron esperar para la realización del mismo en vista de una nueva información que surgiría.

> El Ministro de defensa anda haciendo Lobby por la [\#MocionDeCensura](https://twitter.com/hashtag/MocionDeCensura?src=hash&ref_src=twsrc%5Etfw), me mando esta invitación por equivocación, para mañana a las 7:30 am, media hora antes de la hora a la que esta programado el debate [pic.twitter.com/AViQuh91Hr](https://t.co/AViQuh91Hr)
>
> — Inti Asprilla (@intiasprilla) [14 de mayo de 2019](https://twitter.com/intiasprilla/status/1128358899088752640?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Como lo explicó el representante a la cámara David Racero, decidieron aplazar el debate en vista de la información que publicaría el New York Times, según la cual, había una directriz en las Fuerzas Militares para aumentar el número de bajas y reducir el grado de confiabilidad en la puesta en marcha de operaciones, lo que podría conllevar a que se presentaran nuevos casos de ejecuciones extrajudiciales. En esa medida, los congresistas consideraron oportuno esperar que la opinión pública conociera esta denuncia, para llegar con más argumentos favorables a la moción de censura.

### **Lor argumentos para pedir que Botero abandone el Ministerio**

Los principales argumentos de los citantes giraron entorno al caso de Dimar Torres y lo denunciado por el diario norteamericano; a estos se añadirán la f**alta de confiabilidad en que Botero ejerza su función,** pues desde que este Gobierno inició su mandato han sido asesinados más de 170 líderes sociales, se ha apoyado incuestionablemente el uso del Glifosato y  se ha estigmatizado a la protesta social. (Le puede interesar: ["Gobierno estigmatiza movilización indígena en el Cauca"](https://archivo.contagioradio.com/gobierno-estigmatiza-movilizacion-indigena-en-el-cauca/))

Racero expresó que la moción de censura es un juicio político y ético, en el que se hace una evaluación sobre las actuaciones, decisiones y pronunciamientos de altos funcionarios. Por eso, **"toda la bancada alternativa planteó que el ministro Botero no es una persona competente ni ética para ocupar esa cartera".**

### **Nunca ha sido aprobada una moción de censura, y Duque hará lo posible para que siga siendo así**

La figura de moción de censura fue introducida por la Constitución de 1991 en el ordenamiento político y jurídico colombiano, pese a que ha sido empleada en diferentes ocasiones, nunca ha salido adelante. Para el Representante por la lista de Decentes, ello es una muestra de nuestra "inmadurez" en cultura política, "porque **en Colombia los altos funcionarios están acostumbrados a hacer y deshacer pero nunca asumir la responsabilidad de sus actos"**.

Por su parte, el presidente Iván Duque dio un 'espaldarazo' a Botero, al delegar funciones presidenciales al Ministro durante su viaje por Argentina. Para Racero, es un acto que no responde al palpitar ciudadano, y un mensaje preocupante para el país, porque parece que muchas de sus decisiones se toman en consideración del Centro Democrático y no del grueso de la población. (Le puede interesar: ["El que la hace la paga no aplica para el ministro Carrasquilla"](https://archivo.contagioradio.com/paga-ministro-carrasquilla/))

### **¿Qué sigue tras el debate de moción de censura?**

Este lunes la agenda inició con la intervención de todos los citantes al debate, quienes presentaron los argumentos para promover la moción de censura, después, el Ministro tuvo la posibilidad de contraargumentar, con los mismos tiempos y garantías que los citantes. Luego se abrió el debate y se escucharon las intervenciones de quienes pidieron la palabra. El siguiente paso será en menos de una semana, cuando se convoque para la votación que determinará si Botero debe, o no, dejar el cargo.

Así las cosas, la opinión pública tendrá algunos días para ambientar el debate antes que los partidos y congresistas decidan sus votos; que para Racero, deberían estar orientados hacía la protección de la vida de los colombianos, **buscando tener un ministro de defensa que responda a las necesidades de seguridad y garantías para el ejercicio de los derechos de todos los colombianos.**

<iframe id="audio_36912987" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36912987_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
