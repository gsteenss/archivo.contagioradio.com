Title: Fuertes protestas en Palestina contra decisión de Trump sobre Jerusalén
Date: 2017-12-11 13:15
Category: Onda Palestina
Tags: Apartheid Israel, BDS, Palestina
Slug: palestina-trump-jerusalen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/trump-palestina-e1513015551439.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Mostafa Alkharouf / Agencia Anadolu] 

##### [06 Dic 2017] 

El presidente de Estados Unidos, Donald Trump, pronunció un discurso el miércoles pasado **reconociendo a Jerusalén como la capital de Israel**, una medida que podría causar un retroceso de décadas en la política estadounidense respecto a la colonización israelí de Palestina. **Las protestas a esta medida por parte de palestinos, ya dejan 4 muertos y cientos de heridos**.

La noticia está en línea con la información difundida ayer por dos funcionarios del gobierno que dijeron que incluso cuando **Trump estaba considerando una controvertida declaración de que Jerusalén era la capital de Israel**, se esperaba que demorara nuevamente su promesa de campaña de trasladar allí la embajada de Estados Unidos desde Tel Aviv.

**Israel ocupó la parte oriental de la ciudad de Jerusalén durante la guerra de 1967 y luego la anexó en una medida no reconocida internacionalmente**. Los líderes palestinos, los gobiernos árabes y los aliados occidentales han exhortado a Trump a no proceder con la reubicación de la embajada, lo que iría en contra de décadas de política estadounidense al significar un reconocimiento de hecho de la pretensión de Israel de que toda Jerusalén sea su capital.

Sin embargo, si Trump decide declarar a Jerusalén como la capital de Israel, incluso sin ordenar una mudanza de la embajada, **lo seguro es que habrá una fuerte y negativa respuesta a nivel internacional**.

Una pregunta clave sería si **dicha declaración se consagrará como una acción presidencial formal o simplemente será una declaración simbólica de Trump**. Algunos de los principales asesores de Trump le han presionado en privado para que mantenga su promesa de campaña para satisfacer a una variedad de seguidores, incluidos los cristianos evangélicos, mientras que otros han advertido sobre el posible daño a las relaciones de Estados Unidos con los países árabes.

En esta emisión de Onda Palestina tendremos noticias de la ocupación, arte, entrevistas y la mejor música Palestina, a continuación puedes escuchar el programa completo.

<iframe id="audio_22584840" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22584840_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
