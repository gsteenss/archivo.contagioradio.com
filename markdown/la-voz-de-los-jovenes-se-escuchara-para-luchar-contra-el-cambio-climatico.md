Title: La voz de los jóvenes se escuchará para luchar contra el cambio climático
Date: 2019-04-24 13:04
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: cambio climatico
Slug: la-voz-de-los-jovenes-se-escuchara-para-luchar-contra-el-cambio-climatico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/WhatsApp-Image-2019-03-15-at-2.19.54-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

Desde el pasado 22 de abril, entidades de las Naciones Unidas y otras organizaciones invitan a los niños, niñas, adolescentes y jóvenes del mundo a participar en una encuesta en línea para registrar sus opiniones y ofrecer soluciones sobre el cambio climático. Los resultados de esta iniciativa, que tendrá fin en 2022, se estudiarán para formular una declaración internacional que se entregará a los líderes mundiales.

Como explican los promotores de esta Iniciativa Global, los niños se encuentran entre los grupos más vulnerables al daño ambiental, en vista de que más de 1,5 millones de niños menores de cinco años pierden sus vidas cada año como resultado de los impactos ambientales evitables. Millones más sufren enfermedades, discapacidades y una variedad de otras formas de daño.

Según William León, integrante de Terres des Homme, unas de las organizaciones involucradas, las recientes huelgas escolares, conocidas como Fridays For Future, que iniciaron en Europa pero convocaron a miles de jóvenes a nivel internacional, demuestran el interés y dedicación de esta generación a temas ambientales como el cambio climático. En particular, el ambientalista resaltó el creciente rol de los jóvenes en movimientos ambientales en América Latina.

Como parte de esta iniciativa, también se realizará un taller en Bogotá, Colombia, a principios de mayo que reunirá a alrededor de 45 adultos y 25 niños, niñas y jóvenes de toda América Latina y el Caribe para discutir los problemas ambientales clave de la región y promover soluciones tangibles para promover los derechos de esta generación a un ambiente sano. (Le puede interesar: "[Jóvenes de Colombia se movilizan en contra del cambio climático"](https://archivo.contagioradio.com/jovenes-colombia-se-movilizan-del-cambio-climatico/))

La iniciativa se lleva a cabo por el Relator Especial de las Naciones Unidas sobre Derechos Humanos y Medio Ambiente, con el apoyo de Child Rights Connect, Deutsche Gesellschaft für Internationale Zusammenarbeit, el Ministerio Federal Alemán de Cooperación Económica y Desarrollo, Global Child Forum, Heinrich Boll Foundation, Project Dryad, Terre des Hommes, UNICEF, Naciones Unidas para el Medio Ambiente, UNESCO, WWF y otros.

<iframe id="audio_34913673" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34913673_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
