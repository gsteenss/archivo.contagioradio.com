Title: ¿Suicidio militar de Estados Unidos?
Date: 2015-12-03 12:32
Category: Opinion, Ricardo
Tags: la guerra de irak, la intervencion de libia, las guerras de estados unidos, Otan y estados unidos, primavera árabe
Slug: suicidio-militar-de-estados-unidos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/estados-unidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: AFP] 

#### [**Ricardo Ferrer Espinosa - @ferr\_es**] 

** EE UU salvó a mandos nazis de los juicios de Nuremberg**. En 1945, Estados Unidos protegió a criminales de guerra, porque serían útiles en la confrontación que se iniciaba con la URSS. \[1\] Poco después se creó la CIA con modelos  nazis; es decir, quien decía combatir el fascismo terminó asimilando sus métodos. La CIA se fortalece a tal punto que altera las decisiones militares de Estados Unidos. Fruto de esa contradicción, luego del desastre en Playa Girón (Bahía Cochinos), es asesinado el presidente John F. Kennedy. ** **

Durante los años siguientes, Estados Unidos continuó sus derivas y contradicciones:

**EE UU entrenó a los fundamentalistas musulmanes en Afganistán.** En 1978 los socialistas llegaron al poder en Afganistán. Como es habitual, ese modelo de sociedad produjo reacciones alérgicas en “Occidente”.

La lógica de **“El enemigo de mi enemigo es mi amigo”**, se coló de nuevo en los cerebros militares de EE UU. Algún genio de la estrategia urdió la idea de armar a cada  grupo Muyahidín (Yihadistas) \[2\] y a los Talibán \[3\]. Poco importó a Occidente que los fanáticos  coartaban derechos civiles, impedían la reforma agraria, prohibían la educación femenina y traficaban con opio. Ante la presencia de EE UU en las fronteras, los soviéticos invadieron Afganistán. La llamada “guerra de Afganistán (1978 –1992) \[4\], fue otro escenario de la guerra fría.

Para derrotar a los rusos, la CIA armó, entrenó y alimentó a los fundamentalistas islámicos afganos y árabes. Entre ellos, a Osama Ben laden. Ya sabemos los resultados.

**- La guerra de Irak y las compañías militares privadas**. Desde 2003 la guerra en Irak se desarrolla bajo el nuevo modelo de mercenarios en la primera línea de combate. Como botín,  Estados Unidos y Europa  extraen ilegalmente el petróleo. Se trata de una victoria pírrica porque Irak ya no es gobernable. Nadie garantiza que mañana “Occidente” pueda seguir bombeando petróleo, porque cada milicia tiene lealtades oscilantes. El problema es que **Irak nunca olvida** y menos el asesinato masivo de civiles a manos de los soldados y mercenarios pagados por EE UU.

- **La intervención en Libia:** En 2011, la OTAN arrasa Libia. Dado que ***“El enemigo de mi enemigo es mi amigo”*** los genios militares de Europa y USA armaron prolijamente a grupos cercanos a los Hermanos Musulmanes (Egipto), a fracciones de ideología wahabí (Arabia Saudí) y a Yihadistas reconocidos. Las milicias y la prensa europea celebraron el asesinato de Omar Gadafy.  En medio del caos, funciona como un reloj el saqueo del petróleo libio hacia occidente.

A un año de este experimento, los mismos grupos que había armado la OTAN quemaron la embajada de los Estados Unidos y a su embajador.  Hoy Libia sigue en guerra civil.

- La primavera árabe

Desde 2010, diferentes pueblos árabes y musulmanes reclaman democracia y libertad. En Africa  y Asia Menor, EE UU mueve hilos, defiende a unos gobiernos y derroca otros. Como resultado de esa “diplomacia” hoy vemos la explosión del yihadismo más radical.

Las muertes no preocupaban mucho porque ocurrían lejos de Europa, pero la primavera árabe, hoy marchita,  se cuestiona cuando estallan los tiros y las bombas en París.

**- EE UU y Europa apoyan al gobierno de Erdogán en Turquía. Mientras el Califato o Estado islámico crece, Turquía le compra petróleo y lo revende en el mercado internacional. Recep Tayyip Erdogan recibe dinero a manos llenas de Occidente mientras masacra poblaciones kurdas y omite proteger a las minorías. De nuevo EE UU toma como aliado al personaje discutible, voluble, que mañana estará matando a sus compatriotas.  La deriva suicida de Estados Unidos  hoy es más evidente que nunca.**

###### [\[1\] EE.UU. protegió a criminales de guerra nazis, confirman archivos de CIA <http://www.dw.com/es/eeuu-protegi%C3%B3-a-criminales-de-guerra-nazis-confirman-archivos-de-cia/a-6326914>] 

###### [\[2\] Muyahidín ([pl.](https://es.wikipedia.org/wiki/Plural) muyahidines) En un contexto [islámico](https://es.wikipedia.org/wiki/Islam), la «persona que hace la [yihad](https://es.wikipedia.org/wiki/Yihad)», es decir, «alguien que lucha por su fe». /] 

###### [\[3\] Talibán: En idioma [pashtún](http://pastún) «estudiantes»; Singular: Talib.] 

###### [\[4\] Guerra de Afganistán 1978 – 1992)  <https://es.wikipedia.org/wiki/Guerra_de_Afganist%C3%A1n_%281978-1992%29>] 
