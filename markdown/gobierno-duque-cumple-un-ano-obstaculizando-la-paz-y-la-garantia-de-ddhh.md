Title: Gobierno Duque cumple un año obstaculizando la paz y la garantía de DDHH
Date: 2019-08-06 17:53
Author: CtgAdm
Category: Paz, Política
Tags: Asesinato contra líderes sociales, asesinato de excombatientes, DD.HH, Erradicación Forzada, Gobierno Duque, Paramilitarismo
Slug: gobierno-duque-cumple-un-ano-obstaculizando-la-paz-y-la-garantia-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/duque-juramento.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista  El Congreso] 

Para diversos analistas, el gobierno de Iván Duque cumple un año tratando de poner obstáculos a la paz. Los proyectos de ley entre los que se encuentran las polémicas objeciones a la JEP, el asesinato de excombatientes de FARC, la ruptura de los diálogos con el ELN y las medidas que pretenden priorizar la erradicación forzada de los cultivos de uso ilícito, entre otras, son algunas de las razones de dicho análisis.

Para Alirio Uribe, abogado defensor de DDHH e integrante del Movimiento Defendamos La Paz, este año de gobierno Duque ha sido nefasto en materia de construcción de paz, pero este no es el único tema, la economía, la defensa de los DDHH, la lucha contra la impunidad y otros son una muestra de que este gobierno está siendo fallido.

### **Proyectos de ley en contra de la paz** 

Durante la legislatura vigente el partido de gobierno, Centro Democrático ha presentado 12 iniciativas que atentan contra la implementación del acuerdo de paz y en contra de los derechos de las víctimas. Una reforma a la ley 1448 o ley de víctimas para que la restitución deje de ser la prioridad, es solamente un ejemplo de las intenciones del gobierno.

Para el senador Ivan Cepeda, referenciado por la revista Semana, **esos proyectos buscan restarle a las víctimas la posibilidad de retornar y desarrollar su proyecto de vida en las tierras que les fueron despojadas violentamente, **reducir las posibilidades de nuevas negociaciones de paz, modificar la Jurisdicción Especial de Paz y evitar que los delitos medioambientales sean conexos a la rebelión, con lo cual se bloquearía una eventual negociación con el ELN.

### **Asesinatos de excombatientes** 

En este punto las cifras hablan por si solas, desde la firma del Acuerdo de paz en 2016 han sido asesinados, según el propio partido FARC, cerca de 180 personas excombatientes o familiares de excombatientes. Aunque para muchos este es un indicativo de los riesgos que corre la implementación en los territorios y la ausencia de política eficaces de protección para quienes decidieron dejar las armas.

Al mismo tiempo varios análisis sobre este fenómeno aseguran qu**e los asesinatos de personas en proceso de reincorporación, obedecen a la ausencia de medidas efectivas para el desmonte del paramilitarismo** y una ausencia serie de desmonte del discurso de la guerra, puesto que si se sigue adjetivando como terroristas y narcotraficantes a integrantes de FARC, la reacción en territorio es legitimar cualquier acción en su contra.

### **Erradicación forzada y aspersión aérea atentan de frente contra el Acuerdo de paz** 

Este es otro de los puntos críticos para la paz. Comunidades en el Putumayo, habitantes de la Perla Amazónica denunciaron que el incumplimiento en materia de apoyos técnicos que faciliten la sustitución de los cultivos de uso ilícito deberían estar en marcha al tiempo que los subsidios familiares. Según ellos, este es el último mes en que se recibe el apoyo y sin embargo no se ha podido establecer algún tipo de salida a corto, mediano o largo plazo, puesto que no hay apoyos técnicos para implementar otro tipo de siembras.

 Por su parte, el Instituto de Estudios para la Paz, INDEPAZ afirmó en su más reciente informe que por lo menos el 50% de las hectáreas de tierra sembradas con Coca y que fueron sometidas a erradicación forzada presentan “resiembras de la planta” lo que evidencia que sin sustitución el problema se resuelve en un mínimo porcentaje. [(Lea también: Presidente Duque, fumigando no se acaba con el narcotráfico: lideresa Maydany Salcedo)](https://archivo.contagioradio.com/presidente-duque-fumigando-no-se-acaba-con-el-narcotrafico-lideresa-maydany-salcedo/)

En esta materia el gobierno de Duque plantea como salida el regreso de las fumigaciones aéreas a pesar de los múltiples pronunciamientos de la Corte Constitucional que exigen que este tipo de acciones tengan los estudios suficientes para demostrar que no son un peligro para la salud de las personas o la salubridad para el ambiente.

### **No se ha desmontado el paramilitarismo y parece que no se avanza en ello** 

Diversas organizaciones de DDHH o de presencia internacional como el CICR o la Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos han coincidido en señalar que la proliferación de los grupos armados en los territorios representa una amenaza real para la construcción de la paz, en tanto que propician economías ilegales e impiden, entre otros, el regreso de las personas víctimas a sus tierras.

Según la Fiscalía, más del 57% de los casos de asesinatos de líderes sociales, 274 a Mayo de 2019, han sido responsabilidad de grupos criminales, los demás, 47% tendrían como autores al Clan de Golfo y a disidencias de las FARC que siguen operando en los territorios. Sin embargo, dicha cifra dista mucho del registro de varias organizaciones que señalan que **desde la firma del acuerdo han sido asesinadas 627 personas líderes sociales y defensores de Derechos Humanos.**

En resumen, la proliferación de los grupos de delincuencia relacionados al narcotráfico y la reedición del paramilitarismo en varias regiones, son las principales causas de una crisis humanitaria y el Gobierno no da muestras de estar buscando salidas efectivas. [(Le puede interesar: Duque recortó 30% de presupuesto al Sistema Integral de Verdad Justicia Reparación y No Repetición)](https://archivo.contagioradio.com/duque-recorto-presupuesto-sistema-integral/)

Un ejemplo de ello es la reacción ante la movilización del pasado 26 de Julio en cerca de 130 ciudades del mundo luego de la cual la Fiscalía habilitó un correo electrónico al que los líderes sociales deben remitir sus denuncias que no soluciona el problema, dado que la denuncia de la amenaza no es el problema sino la protección efectiva, territorial y conjunta de los procesos que se ponen en riesgo.

### **Desmonte de la mesa nacional de garantías durante gobierno Duque** 

Soraya Gutiérrez, abogada del Colectivo de Abogados José Alvear Restrepo, aseguró que uno de los pocos espacios de participación de las organizaciones sociales con presencia territorial era la Mesa Nacional de Garantías, creada durante el gobierno de Juan Manuel Santos para hacer un seguimiento y acompañamiento a las acciones del Estado en materia de protección de los DDHH.

Sin embargo, durante el gobierno de Iván Duque este espacio no ha sido convocado, por el contrario, se han definido una serie de políticas de manera unilateral pero que han sido nombradas como pactos, uno de los ejemplos claros está en el Plan Nacional de Desarrollo que menciona una serie de pactos que no han tenido ningún espacio de concertación con la sociedad civil.

Así las cosas, el balance de este primer año del gobierno Duque es más que desalentador en materias de paz y derechos humanos. Situación que podría no mejorar o incluso empeorar al hacer el análisis del discurso del uribismo que sigue priorizando las medidas de guerra o policivas por encima de la concertación y el diálogo democrático.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

<iframe id="audio_39648606" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_39648606_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
