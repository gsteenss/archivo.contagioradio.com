Title: AGC amenazan a líder Orlando Castillo del Espacio Humanitario Puente Nayero
Date: 2017-04-26 08:33
Category: DDHH, Nacional
Tags: buenaventura, paramilitares, puente nayero
Slug: paramilitares_amenazan_orlando_castillo
Status: published

###### Foto: Comisión de Justicia y Paz 

###### [26 Abr 2017] 

Orlando Castillo, lider comunitario del Espacio Humanitario Puente Nayero, en Buenaventura, nuevamente es blanco de amenazas por parte **del grupo paramilitar, autodenominado Autodefensas Gaitanistas de Colombia, AGC.**

De acuerdo con la denuncia se trata de un plan que trabaja, **el integrante de las AGC conocido como "Mono Cocha"**. El objetivo es asesinar al lider, debido a las denuncias públicas que ha hecho, en torno a las extorsiones que realiza esta estructura armada en el acceso fluvial al puerto de Buenaventura en el Espacio Humanitario de Punta Icaco. Es así como el grupo paramilitar, **busca silenciar las denuncias, pero además evitar la consolidación del Espacio Humanitario de Punta Icaco.**

Frente a esa nueva amenaza y las anteriores recibidas desde hace varios años, se viene solicitando  medidas estructurales urgentes para proteger a los habitantes y líderes comunitarios de la zona.

Cabe recordar que por el mismo extorsionista de las AGC, en el barrio la Playita, ya había sido asesinado **Wilder Ubeimar, propietario de un granero ubicado Punta Icaco.** Un comerciante que se negó a pagar extorsiones a los paramilitares asentados en el barrio Alfonso López.

Sobre ese hecho, en su momento la Comisión de Justicia y Paz denunció que: “en el momento en que los **paramilitares asesinaron al vendedor se encontraban unidades de la Infantería de Marina a menos de 80 metros del lugar**.  Los ejecutores pasaron al lado de estos e ingresaron al Espacio Humanitario pasando por enfrente de policiales que evidentemente no les observaron al ejecutar la acción”. En esa misma época Orlando Castillo, y otros dos líderes fueron amenazados de muerte por los paramilitares.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
