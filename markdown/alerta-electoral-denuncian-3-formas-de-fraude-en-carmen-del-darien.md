Title: Alerta Electoral. Denuncian 3 formas de fraude en Carmen del Darién
Date: 2015-10-25 09:45
Category: Nacional, Política
Tags: Cambio Radical, Carmen del Darién, elecciones, Partido Verde, Raul Palacios
Slug: alerta-electoral-denuncian-3-formas-de-fraude-en-carmen-del-darien
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Carmen-del-Darién-e1509572467830.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

###### [25 Oct 2015]

Según la denuncia de los habitantes del municipio de Carmen del Darién en el departamento del Chocó las personas encargadas de custodiar los tarjetones decidieron dejarlos en el Hotel Santi, en el corregimiento de Bajirá. La dueña del hotel denunció que en las mismas instalaciones se encontraban alojadas personas que trabajan para la campaña del candidato de Cambio Radical Erlin Ibarguen Moya.

La otra situación preocupante tiene que ver con que la denuncia que se realizó ante la policía de Carmen del Darien no fue respondida porque según la propia respuesta de los uniformados “guardar esos tarjetones” es muy peligroso, situación que indignó a los habitantes del municipio y a los seguidores del candidato del Partido Verde Raúl Palacios.

También se denunció que desde la ciudad de Turbo se trasladaron buses con personas que inscribieron la cédula en el municipio y a pesar de las denuncias no se verificó ningún tipo de actuación por parte de las autoridades competentes.

Adicionalmente se conoció que en la esquina del centro de votación en este municipio se están pagando los votos y se está presionando a los electores en presencia de las Fuerzas Militares.
