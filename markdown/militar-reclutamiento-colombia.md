Title: Los cambios en el sistema de reclutamiento militar en Colombia
Date: 2017-08-06 14:45
Category: DDHH, Nacional
Tags: batidas, ejercito, reclutamiento
Slug: militar-reclutamiento-colombia
Status: published

##### Foto: Archivo 

###### 06 Ago 2017 

Tras la sanción presidencial del **nuevo sistema de reclutamiento militar en Colombia**, varias son las condiciones que cambiarían para quienes prestan e ingresan al servicio en las fuerzas armadas del país. El **incremento en la remuneración** de los reclutas y reducciones en el tiempo de prestación, costos por multas y valor de la libreta militar son algunas.

A a partir del viernes, fecha en la que el presidente Juan Manuel Santos firmó la ley, los cerca de un millón de remisos que hay en el país **tendrán un año para definir su situación militar**, tiempo en el que estarán eximidos del cobro de multas correspondientes. El mandatario aseguró además que ningún ciudadano con 24 años o más puede prestar el servicio, aunque deberá pagar la respectiva cuota de compensación.

**Remuneración**

Con la medida, la bonificación mensual que reciben quienes prestan el servicio militar **pasa del 14% al 30% de un salario mínimo**, que correspondería a 221.000 pesos. La ley establece que, en caso de presentar calamidades domésticas como la muerte de un ser querido los militares podrán acceder a un salario mínimo legal vigente.

Adicionalmente **podrán obtener subsidios y créditos de estudio en caso de que quieran incorporarse como profesionales**. Los soldados que no tengan el título de educación media (bachillerato) podrán obtenerlo durante el tiempo de prestación del servicio, y la posibilidad de **incorporarse a programas técnicos y tecnológicos** en convenio con el SENA.

**Costos para la libreta militar**

El monto mínimo es de **147.543 pesos** (antes estaba en 485.000 pesos) y contempla un tope de \$ **29’508.680.** Para los jóvenes de estratos 1, 2 y 3 que pertenezcan al Sisbén, los desplazados, los desmovilizados y las víctimas del conflicto armado **no deberán pagar por la “cuota de compensación militar”** que se cobra a quienes quedan eximidos de prestar el servicio.

Quienes deban cancelar tal cuota, para lo cual tendrán un plazo de 180 días,  pagarán el **20% de un salario mínimo equivalente a 150 mil pesos**, una reducción en más de la mitad de los 430.o00 que se pagaban desde 1993 cuando se aprobó un cobro del 60%.

**Obligatoriedad**

La decisión presidencial, contempla que **la prestación del servicio militar continua siendo de carácter obligatorio** para los hombres mayores de 18 años, sin embargo l**a normativa prohíbe a la fuerza pública el realizar batidas callejeras** a jóvenes que no han definido su situación militar remplazandolas por 4 jornadas de incorporación durante el año.

Para quienes aun no definen su situación militar se contempla una **condonación de las multas y podrán obtener la libreta pagando 110.000 pesos** correspondientes a su costo de elaboración acercándose a los diferentes distritos en el país. Para responder inquietudes se dispondrá de un sitio web donde los interesados podrán realizar consultas en línea.

**Tiempo de servicio**

Para los soldados bachilleres e**l tiempo de prestación del servicio continúa siendo de 12 meses** mientras que la duración máxima pasa de 24 meses a 18 meses.

**Sistema laboral**

Las empresas están en obligadas a otorgar **18 meses a los jóvenes que aún no tengan su libreta militar** y deseen iniciar su vida laboral.

**Servicio militar femenino**

Las mujeres entre **18 y 24 años** que quieran ingresar a la Institución lo podrán hacer sin ningún inconveniente.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
