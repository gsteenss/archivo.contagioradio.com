Title: Empresas de electricidad insisten en desviar tres afluentes en Génova, Quindío
Date: 2019-07-24 17:57
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Génova, Pequeñas Hidroeléctricas, Quindío
Slug: empresas-de-electricidad-insisten-en-desviar-tres-afluentes-en-genova-quindio
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Génova.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Génova-2.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: EcoGénova] 

Comunidades, Alcaldía y el Consejo de Génova en Quindio vienen rechazando las socializaciones *express* que busca adelantar la empresas **E-lectrica S.A.S. y Econ Empresa Consultora** con la intención de cumplir la etapa fundamental del proceso de licenciamiento que le permitiría poner en marcha tres proyectos de pequeñas centrales hidroélectricas, en los tres afluentes del municipio. Pese a que la comunidad se reunió y se manifestó en contra del “proyecto de Energía Renovable Pequeña Central Hidroeléctrica” esta continúa haciendo presencia en el territorio.

Viviana Viera, caficultora e integrante de fundación Eco Génova explica que en septiembre del 2018 la empresa ya había intentado acercarse a las comunidades invitándolas a una reunión informativa sobre un una iniciativa denominada “proyecto de Energía Renovable Pequeña Central Hidroeléctrica” la que fue rechazada por los habitantes del municipio. [(Lea también: pequeñas hidroeléctricas ¿una nueva amenaza para el Quindío?)](https://archivo.contagioradio.com/pequenas-hidroelectricas-quindio/)

Casi un año después, aunque todos los permisos ambientales les han sido retirados, E-lectrica S.A.S intentó compartir la misma información de forma muy rápida, "afortunadamente la información rotó y fuimos a bloquear y la socialización, no los queremos y se los hemos hecho saber de todas las formas".

### Impactos negativos sobre los afluentes de Génova

De adelantarse estos proyectos, se captaría parte del agua en zona alta para conducirlo mediante una tubería hasta la sala de máquinas, donde se generaría la energía, desviando más de seis kilómetros  del caudal del río, ocasionando un incremento en la temperatura que además afectaría a especies endémicas y directamente a los cultivos de café.

**"No queremos que nos pase lo que les ha pasado a otras comunidades que no tuvieron la oportunidad de oponerse o de entender los impactos de estos proyectos"** afirma Viviana Viera quien señala que en  municipios como Génova, no se acude a una consulta previa sino que las empresas imponen una socialización del proyecto, mediante firmas de asistencia o fotografías que demuestren que informaron a la comunidad pese a que esta no esté de acuerdo, para así obtener una licencia ambiental y comenzar el proyecto.

### ¿Existe un aporte real de estos proyectos al Quindio?

Según Viera, ninguno de los doce proyectos que estaban licenciando "pretendía dar un kilobatio de energía al Quindio" pues la electricidad llega al departamento desde Caldas por lo que no existe una necesidad real de producción.

Pese a la continúa persistencia de las empresas, las comunidades aún desconocen a qué población sería destinada la energía que pudieran aportar estas pequeñas hidroeléctricas o qué objetivo en específico tendrían con su llegada al territorio, sin embargo las investigaciones que han podido realizar desde Ecogénova apuntan a que los accionistas de las empresas están ligados a la minería de carbón, en particular a la compañía Drummond.

### Empresas fantasma 

La caficultora también denuncia que cuando han intentado recolectar información adicional, no han podido hallar datos que permitan conocer más sobre los proyectos de la empresa como una sede física o una página web, **"cuando investigamos quienes son los dueños llegamos a los papeles de Panamá o a empresas fachada**" afirma Viera quien señala que la creación de consorcios financieros entre este tipo de empresas les impiden rastrearlas o demandarlas pues al hacerlo el consorcio se ha disuelto.

Ante la insistencia de estas pequeñas centrales, Viviana Viera asegura que seguirán instruyendo a la comunidad sobre los impactos negativos de la llegada de compañías como E-lectrica S.A.S y movilizándose en el territorio y a través de redes sociales para garantizar la autonomía de las fuentes hídricas de la región.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38869404" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38869404_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
