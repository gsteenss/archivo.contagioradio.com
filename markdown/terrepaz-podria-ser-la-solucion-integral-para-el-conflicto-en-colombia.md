Title: TERREPAZ podría ser la "solución integral para el conflicto en Colombia"
Date: 2015-12-07 16:32
Category: Nacional, Paz
Tags: Diálogos de paz en la Habana, FARC Diálogos de paz, la propuesta de la farc en los terrepaz, Propuesta de TERREPAZ, Sergio Ibáñez, TERREPAZ
Slug: terrepaz-podria-ser-la-solucion-integral-para-el-conflicto-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Diálogos-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: colombia.com 

<iframe src="http://www.ivoox.com/player_ek_9628023_2_1.html?data=mpufmpWWd46ZmKiak52Jd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmKbGs6q9o7%2BPtNDY04qwlYqliMKf1MrfjdHFb4amk9jcztrHrYa3lIqvldOPrc%2Foxszfw9GPtMLmwpDSzpDHs8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Sergio Ibáñez, FARC-EP] 

###### [7 Dic 2015 ]

[La delegación de paz de las FARC-EP presentó en La Habana su propuesta para el punto del fin del conflicto, los **‘Territorios Especiales para la Construcción de la Paz’ TERREPAZ**, zonas delimitadas en las que se conservarían los principios económicos, políticos y culturales bajo los que esta guerrilla ha operado en ciertas áreas geográficas y que permitirían la **reparación de las víctimas**, la **dejación de las armas** y el **eventual cese bilateral definitivo**.]

[Sergio Ibáñez, representante de las FARC en la 'subcomisión técnica para el fin del conflicto', asegura que la propuesta de los TERREPAZ surge de la intención de solucionar racionalmente el conflicto colombiano atendiendo a sus especificidades, “aunque el conflicto en Colombia ha abarcado la mayor parte del país es evidente que ha tenido un **desarrollo más agudo en ciertas regiones** y es sobre esas áreas geográficas y **frente a esas comunidades** **que se han visto más afectadas**, que deben implementarse **estrategias más concretas, soluciones más integrales**”.]

[De acuerdo con Ibáñez la **propuesta de los TERREPAZ no está pensada para acorralar a nadie** sino para expandir las posibilidades productivas, económicas, políticas y sociales de todas las regiones en las que las FARC han tenido presencia histórica, por lo que requerirán acompañamiento estatal y de la sociedad civil para que se pase “de un Estado ultracentralista, que siempre ha trabajado en función de la acumulación de poder y riqueza en las grandes ciudades” hacia una administración que transforme las dinámicas regionales. “**Este planteamiento no lo hacemos con la idea de construir guetos desde los cuales se ataque a la sociedad**, al contrario estamos hablando de un planteamiento que congregue voluntades para la reconciliación y reconstrucción nacional”.  ]

[Frente a los lugares en los que se prevé estarían construidos los TERRAPAZ, Ibáñez afirma que aún **no hay un acuerdo definitivo, la propuesta es general y requiere ser discutida democráticamente con diversos sectores de la sociedad** pues “en el desarrollo de este conflicto ha habido regiones históricas de presencia de las FARC, distintos espacios geográficos por todo el país, desde la Guajira hasta la Amazonia y desde el Chocó hasta Arauca, en los que existen **comunidades que esperan que la firma de un acuerdo de paz implique transformaciones en sus condiciones materiales de vida**”.   ]

[Como condiciones para la implementación de los TERREPAZ se establecen una serie de **cambios en la lógica estatal** que implican la **transformación de la actual concepción de seguridad nacional y el desmonte de las estructuras paramilitares**, “en los TERREPAZ consideramos que debe haber presencia de La Policía, por esto debe transformarse la consideración de la seguridad nacional centrada en un enemigo interno que hace que el **Estado considere como un adversario para ser eliminado a todo aquel que exija sus derechos**”, afirma Ibáñez.]

[“Partimos de que **este acuerdo de paz está basado en la idea de dejar las armas**, lo que implica que tanto las FARC, como el Estado y todos los actores políticos abandonen la idea de usar las armas para obligar a otros a pensar como ellos piensan, a defender intereses que les convienen o a respaldar un status quo que todos sabemos que está podrido” agrega Ibáñez e insiste en que **los TERREPAZ se consideran en el marco de la jurisdicción que implique el fin de conflicto** y en ellos se promoverá la participación de la sociedad en su conjunto.]

[Para concluir Ibáñez se refiere a la **participación política en los TERREPAZ** asegurando que "nosotros tendremos que ser como todos los colombianos, ciudadanos de primera categoría, con todos los derechos que otorgaría el marco legal y constitucional de ese país en paz, las **formas concretas que asumiría nuestra participación en esos espacios geográficos necesariamente serían muy variadas**” e incluirían la participación en juntas de acción comunal, sindicatos, organizaciones juveniles o de mujeres.]
