Title: Santiago Uribe habría financiado voluntariamente grupos paramilitares
Date: 2017-02-14 16:11
Category: Entrevistas, Judicial
Tags: 12 apostoles, alvaro uribe velez, Paramilitares en Antioquia y Chocó, Santiago Uribe Vélez
Slug: santiago-uribe-habria-financiado-voluntariamente-grupos-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Santiago-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

###### [14 Feb 2017] 

La Sala de Justicia y Paz del Tribunal Superior de Medellín, profirió una sentencia contra 4 paramilitares y **ordena la investigación a Santiago Uribe Vélez y otras 50 personas, por su responsabilidad en la ejecución de crímenes y graves violaciones** a los Derechos Humanos y al Derecho Internacional Humanitario, ligados al accionar de las Autodefensas Unidas de Colombia en Antioquia y Chocó.

De acuerdo con el abogado Daniel Prado, **exalcaldes, oficiales y exoficiales del Ejército y la Policía, empresarios, mineros, comerciantes y funcionarios judiciales**, deberán responder por los delitos de concierto para delinquir, homicidio en persona protegida, desaparición forzada, tortura, desplazamiento forzado, reclutamiento, violencia sexual y ejecuciones extrajudiciales, que afectaron gravemente a comunidades campesinas, afros e indígenas.

### Las pruebas 

El documento de 1.767 páginas, reúne **“pruebas concretas” logradas a partir de las declaraciones de alías ‘Don Berna’ y ‘Don Mario’**, los cuales relacionan al hermano de Álvaro Uribe Vélez, Santiago Uribe Vélez, con el funcionamiento del grupo paramilitar ‘La Escopeta’, el Bloque Pacífico-Héroes del Chocó y el Frente Suroeste de las AUC.

Los testimonios entregados que comprometen a ganaderos, empresarios del carbón, comerciantes, transportadores, narcotraficantes y miembros de la Fuerza Pública, algunos de ellos aún activos, **reafirmaron la existencia de una frecuencia radial cerrada, que comunicaba a cabecillas paramilitares con Santiago Uribe** y personajes como el exsenador y expresidente de la Asociación de Cafeteros, **Ernesto Garcés Soto.**

A partir de la versión libre de Rodrigo Alberto Zapata Sierra, sujeto cercano a Vicente Castaño y conocido por algunos como el 'Canciller de las AUC', otro aparte de la sentencia vincula a varias empresas que “contribuían voluntariamente al grupo paramilitar”, como **Segurcol, Tejares San Fernando, Ladrillera Ambalá, Porcícola Industrial Colombia, Bellanita de Transportes y otras.**

### Casos impunes deberán investigarse a profundidad 

El abogado manifiesta que en el documento, relaciona directamente el caso de los ’12 apóstoles’, sus hostigamientos y asesinatos en Yarumal, “donde delinquían como pedro por su casa”, con el financiamiento paramilitar por parte de Garcés Soto, quien además **“daba ordenes para la ejecución de ciudadanos en esta región y patrocinó campañas de Álvaro Uribe Vélez”.**

Por último, el abogado Daniel Prado indicó que los homicidios, desapariciones y otros crímenes que “no se investigaron con la seriedad que ameritaban en su momento”, como el de la personera de Titiribí quien después de declarar en contra de la familia Uribe Vélez, “fue desaparecida y días después apareció su cadáver sin lengua” y la ejecución extrajudicial de dos jóvenes, Jorge Alarcón y Edgar Monsalve, **no prescriben por ser de lesa humanidad y deben ser investigados “hasta las ultimas consecuencias”, puesto que aún se encuentran en la impunidad. **

<iframe id="audio_17014395" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17014395_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
