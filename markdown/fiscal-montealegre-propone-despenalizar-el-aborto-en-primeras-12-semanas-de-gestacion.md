Title: Fiscal propone despenalizar el aborto en las primeras 12 semanas de gestación
Date: 2015-11-10 09:20
Category: Mujer, Nacional
Tags: Aborto en Colombia, Armando Benedetti, Catalina Ruíz Navarro, Cifras sobre aborto, Fiscal Montealegre, legalización del aborto, Womens Link WorlWide
Slug: fiscal-montealegre-propone-despenalizar-el-aborto-en-primeras-12-semanas-de-gestacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/aborto-facebook6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Women's Link 

<iframe src="http://www.ivoox.com/player_ek_9343705_2_1.html?data=mpihlZyUeY6ZmKiakpmJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRisrnxMbZjbLTstXZwtHSydfJb9qf1NqY0tfTtNbZ1NnOjcaPqsLq0NeYxsrQb4y1w9Tf1tSPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Eduardo Montealegre, Fiscal General] 

###### [10 Nov 2015]

En el **foro sobre aborto** impulsado por la fundación Buen Gobierno y **Women's Link Wold Wide, **que se realizó en la ciudad de Bogotá en el Auditorio Luis Guillermo Vélez, el **Fiscal Eduardo Montealegre** manifestó que presentará en el  Congreso un proyecto de ley que tiene por propuesta avalar el aborto sin restricciones durante los  tres primeros meses del embarazo.

“La propuesta es avanzar en la despenalización del aborto. Si bien existen estas tres causales en las cuales el aborto es permitido en la legislación colombiana, debemos dar un paso más y establecer que el aborto debe ser permitido al menos durante las doce semanas de gestación”, dijo el fiscal Montealegre.

El debate se realizó en diferentes momentos, entre ellos la representante a la cámara, Angélica Lozano, quien aseguró que primer **"Necesitamos políticas eficaces de educación sexual, asesoría en anticoncepción y adopción". **

Así mismo, el senador Armando Benedetti, evidenció que **"De cada 10 centros asistenciales, 9 se niegan a practicar aborto**. Esto es un problema de Derechos fundamentales". A su vez, la analista y columnista Catalina Ruíz Navarro, señaló que "**los servicios de aborto deben estar disponibles para mujeres de todas las edades y creencias".**

Cabe recordar, que actualmente en Colombia el aborto es legal en casos de violación, por grave malformación del feto y cuando la salud física o mental de la madre están en riesgo Según estadísticas, en Bogotá, entre el 2006 y el 2013 se reportaron 16.947; y en todo el país, entre el 2009 y el 2012, hubo información de 14.536.
