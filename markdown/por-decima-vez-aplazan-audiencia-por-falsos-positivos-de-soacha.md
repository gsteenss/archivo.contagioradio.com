Title: Por décima vez aplazan audiencia por "falsos positivos" de Soacha
Date: 2017-03-24 13:03
Category: DDHH, Nacional
Tags: Ejecuciones Extrajudiciales, falsos positivos, madres de soacha
Slug: por-decima-vez-aplazan-audiencia-por-falsos-positivos-de-soacha
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Madres-de-Soacha.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Cajar] 

###### [23 Mar 2017] 

Cuando se esperaba que se diera lectura al fallo **contra 21 militares pertenecientes a la brigada móvil \# 15, entre ellos el Teniente Gabriel Roncón** que sería uno de los mandos responsables del asesinato de los jóvenes de Soacha en el mes de agosto de 2008 conocidos como **"falsos positivos"**, los militares y sus abogados no acudieron a la cita judicial alegando problemas en el traslado.

Las madres de Soacha han manifestado que **este accionar hace parte de una serie de maniobras dilatorias de mala fe que o contribuyen a la verdad.** Además, relata **Carolina Daza, abogada del Colectivo de Abogados José Alvear Restrepo** y representante de una de las madres “han sido once años de impunidad que han tenido que soportar las madres en estos casos de “falsos positivos”. Le puede interesar: [Durante comandancia del General Montoya se documentaron 4.300 falsos positivos](https://archivo.contagioradio.com/durante-comandancia-del-general-montoya-se-documentaron-4-300-falsos-positivos/)

En el caso de los jóvenes de Soacha cursan dos procesos penales en los cuales está vinculado el **Teniente (r) Gabriel de Jesús Rincón Amado**, Oficial de Operaciones de la Brigada Móvil 15 del Batallón Contraguerrillas 96 en Norte de Santander, es el máximo responsable vinculado y 19 integrantes más de la misma unidad militar.

Cabe anotar que el **Teniente Coronel Rincón Amado** también está condenado a más de 35 años de prisión por **la ejecución extrajudicial del mototaxista** Luis Antonio Sánchez, en abril de 2007.

Los cargos por los cuales están acusados son **concierto para delinquir, desaparición forzada, homicidio agravado y falsedad ideológica en documento público.**

Según lo denuncia Daza, pese a que se ha avanzado bastante, “aún hace falta mucho por caminar” dado que este proceso debía haber terminado hace 2 años, sin embargo, por **diversas actuaciones de los abogados de los militares y del INPEC no se ha podido conocer el fallo en este caso".** Le puede interesar: [Corte Penal Internacional investigaría "falsos positivos" en Colombia](https://archivo.contagioradio.com/corte-penal-internacional-investigaria-falsos-positivos-en-colombia/)

“Recibimos con esperanza la noticia de que la juez compulsará copias para i**nvestigar a los abogados de los militares por faltar a esta cita judicial**, de la cual ellos conocían, así mismo, **investigarán al INPEC** por no llevar a los militares” contó Daza.

La próxima citación quedó para el 3 de Abril, fecha en la que se espera pueda ser conocida por las madres de Soacha la condena y **“no permitir que se sigan burlando del dolor y la tristeza de estas madres”** recalcó Daza. Le puede interesar: [Primer General del Ejército será llamado a juicio por "falsos positivos"](https://archivo.contagioradio.com/general-del-ejercito-llamado-a-juicio35257/)

Cabe recordar que según el informe presentado las Naciones Unidas sobre ejecuciones extrajudiciales, **en Colombia la impunidad frente a estos crímenes cometidos por las Fuerzas Militares es del 98,5%** y una gran cantidad de los mismos fue cometida durante el gobierno de Álvaro Uribe Vélez y con los dineros del Plan Colombia.

**“Más de 3.000 ejecuciones extrajudiciales**, sumarias y arbitrarias, perpetradas en Colombia entre 2002 y 2009 son crímenes de carácter internacional. **Lo sucedido a 16 jóvenes de Soacha mostró la extrema crueldad con la que se puede actuar** para lograr efectividad en supuestos combates a variados enemigos” reza el documento. Le puede interesar: [Militares implicados en “falsos positivos” no deben ser ascendidos: HRW](https://archivo.contagioradio.com/se-debe-revaluar-ascenso-de-militares-implicados-en-casos-de-falsos-positivos/)

Este es el listado de los militares implicados por el caso del asesinato de los jóvenes Diego Alberto Tamayo Garcerá, Víctor Fernando Gómez Romero, Jader Andrés Palacio Bustamante, Julio César Mesa Vargas y Jhonatan Orlando Soto Bermúdez.

1\. Teniente Coronel Gabriel de Jesús Amado,  
2. Mayor Henry Mauricio Blanco Barbosa,  
3. Sargento Medardo Ríos Díaz,  
4. Cabo Tercero Juan Gabriel Espinoza Restrepo,  
5. Cabo Segundo Richard Jojoa Bastidas,  
6. SLP: José González Ceballos,  
7. SLP Nixon Cubides Cuesta,  
8. SLP Kevis Jiménez Escalante,  
9. SLP Luis Alirio López,  
10. SLP Mauricio Cuniche Delgadillo,  
11. SLP José Fernández Ramírez,  
12. Cabo Manuel Zorrilla Gámez,  
13. SLP Ricardo Eliud Gónzalez Gómez,  
14. SLP Ferney Gijalba Flor,  
15. SLP Eider Guerrero Andrade,  
16. SLP Geiner Fuertes Billermo,  
17. SLP Pedro Hernández Malagón,  
18. SLP Juan Ramón Marín Ramírez,  
19. Sargento Segundo Janer Ediel Duque Marín,  
20. SLP John Anderson Díaz  
21. Cabo Ricardo Coronado Martínez

<iframe id="audio_17750975" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17750975_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
