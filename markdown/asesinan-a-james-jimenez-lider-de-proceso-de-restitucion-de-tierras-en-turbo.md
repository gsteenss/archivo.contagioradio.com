Title: Asesinan a James Jímenez líder de proceso de restitución de tierras en Turbo
Date: 2018-04-26 16:40
Category: DDHH, Nacional
Tags: James Jiménez, Lider social, Restitución de tierras, Turbo
Slug: asesinan-a-james-jimenez-lider-de-proceso-de-restitucion-de-tierras-en-turbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/lideres-asesinados-20116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Líderes Sociales] 

###### [26 Abrl 2018]

El pasado 20 de abril, fue asesinado **James Luís Jiménez Estrada, en la vereda Santa Rosa, del municipio de San Pedro de Uraba, en Turbo**. El hecho se registró mientras Jiménez visitaba a su hermano, en horas de la mañana. Organizaciones defensoras de derechos humanos le exigen a la Fiscalía General de la Nación que esclarezca y judicialice a los responsables intelectuales y beneficiarios de estos crímenes.

Jiménez se desempeñaba como presidente de la Junta de Acción Comunal de Cacahual, en el corregimiento de San Vicente del Congo, en Turbo. Jiménez tenía amplios conocimientos en varias de las problemáticas que aquejaban a su territorio, las principales **son los procesos de formalización y restitución de tierras y de sustitución de cultivos de uso ilícitos**.

La organización Forjando Futuros, la Comisión de Justicia y Paz y el Instituto afirmaron en un comunicado de prensa que **"con este hecho se incrementa la preocupación por la protección de la vida e integridad de quienes participan y lideran organizaciones sociales y comunitarias**". (Le puede interesar: ["Familiares del líder Temístocles Machado continuarán con su legado"](https://archivo.contagioradio.com/familiares-de-temistocles-machado-lider-social-asesinado-continuaran-con-su-legado/))

De igual forma manifestaron que, actos como los ocurridos el pasado 11 de abril, en la vereda el Tomate de San Pedro de Urabá, en el que murieron 8 miembros de la Fuerza Pública cuando acompañaban diligencias relacionadas con la restitución de tierras, reflejan la incapacidad estatal para asegurar procesos de restitución, **y desconocen mecanismos de sometimiento judicial colectivos de grupos criminales para garantizar derechos a la tierra.**

En ese sentido hicieron un llamado al Estado colombiano y al gobierno nacional para tomar medidas que permitan la implementación de los acuerdos de Paz entre el Estado colombiano y las FARC, "ya que parece que en estos territorios las comunidades no pueden beneficiarse de temas como la restitución de tierras, formalización, banco de tierras, la sustitución de cultivos de uso ilícito; y el de sometimiento judicial colectivo de herederos del paramilitarismo".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
