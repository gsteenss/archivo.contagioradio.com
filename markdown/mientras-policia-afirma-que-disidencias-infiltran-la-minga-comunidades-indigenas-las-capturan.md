Title: Mientras Policía afirma que disidencias infiltran la Minga, comunidades indígenas las capturan
Date: 2019-03-18 16:42
Author: ContagioRadio
Category: Movilización, Nacional
Tags: CRIC, Minga en el Cauca, ONIC
Slug: mientras-policia-afirma-que-disidencias-infiltran-la-minga-comunidades-indigenas-las-capturan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC_0049.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: CRIC 

###### 18 Mar 2019 

Tras completar una semana de minga en la que representantes de los pueblos indígenas han exigido como requisito la presencia del presidente Duque para negociar el cumplimiento de los acuerdos pactados con anterioridad y el fin del bloqueo de la vía Panamericana, los manifestantes afirman que continuarán resistiendo las confrontaciones con la Fuerza Pública y defendiendo a las comunidades de infiltraciones que buscan desprestigiar la protesta, hasta que el Gobierno acuda para escucharlos.

**Edwin Capaz coordinador de DD.HH. de la ACIN** indica que en general las comunidades indígenas y campesinas mantienen el control absoluto de cerca de 23 kilómetros de la vía Panamericana; sin embargo denuncia los hechos que ocurrieron durante el fin de semana en El Cairo donde el ESMAD desalojó a las personas **destruyendo sus provisiones y cambuches** mientras realizaban acciones pedagógicas con la comunidad.

Así mismo, Capaz explica que en el sector de Coconuco hubo "una arremetida contras las comunidades" dejando tres indígenas heridos, **lo que eleva la cifra a 23 comuneros que han resultado lesionados por la Fuerza Pública**. De los casos reportados, dos han sido heridas de consideración debido al uso de armas modificadas por parte del ESMAD.

**La Fuerza Pública busca desprestigiar la minga**

Según el coordinador, durante el resto del fin de semana, la guardia indígena garantizó la la protección de las comunidades y frustro 17 posibles intentos de infiltración  de personas "ajenas al movimiento que tendrían intenciones de desligitimar la Minga y generar distorsión",  Capaz precisa que dichas personas ya fueron expulsadas del lugar. [(Le puede interesar La Minga indígena que reclama a Duque cumplir los acuerdos) ](https://archivo.contagioradio.com/minga-indigena-duque/)

Agrega que ven con preocupación los señalamientos de autoridades políticas, civiles y militares, que afirman que disidencias de FARC estarían infiltrando la minga social, **"es una modalidad de desprestigio, se quiere tratar de justificar un panorama para la agresión"** afirma Capaz quien considera paradójico que mientras la Fuerza Pública asegura que hay disidencias infiltradas en la minga, las comunidades indígenas capturan las han capturado y expulsado del territorio.[(Lea también Presidente Duque seguimos esperando su llegada: Minga del Cauca) ](https://archivo.contagioradio.com/presidente-duque-seguimos-en-minga-esperando-su-llegada-comunidades-indigenas-del-cauca/)

Aunque se reiteró la invitación al presidente Duque para que se reúna con las comunidades, éste ha preferido reunirse con los gobernadores de Nariño, Cauca y Valle lo que para las comunidades indígenas representa un "acto dilatorio" por parte del mandatario, **"lo hemos visto enérgico en otras correrías que no son del Estado colombiano, es muy difícil pensar que no pueda llegar acá"** afirma Capaz quien señala que se trata de un acto de "arrogancia institucional".

<iframe id="audio_33482353" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33482353_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

###### 
