Title: Movimiento Ríos Vivos nuevamente es blanco de amenazas
Date: 2015-06-17 13:52
Category: Ambiente, Nacional
Tags: Antioquia, desalojo forzoso, Desplazamiento HidroItuango, Empresas Públicas de Medellín, Hidroituango, Isabel Cristina Zuleta, Movimientos Ríos Vivos
Slug: movimiento-rios-vivos-nuevamente-es-blanco-de-amenazas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Hidroituango-no-es-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [alianzademediosalternativos.blogspot.com]

###### Jun 17 de 2015 

<iframe src="http://www.ivoox.com/player_ek_4653376_2_1.html?data=lZuilZibeo6ZmKiakpyJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2Foxszfw9PYqdSfxcqYr9Tarc7dxtPh0ZC2aaSnhqax0diPmsrq0NiY1dTSb8LhxtPO3MbIs9Shhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Isabel Cristina Zuleta, Movimiento Ríos Vivos] 

El pasado 11 de junio, integrantes del Movimiento Ríos de Antioquia, nuevamente fueron víctimas de amenazas de muerte, por medio de un mensaje de texto que llegó al celular de María Eugenia Gómez, quien hace parte del movimiento. En el mensaje **se amenaza la vida de todas las personas que se estén oponiendo a desalojar la playa del Río Cauca,** que necesita Empresas Públicas de Medellín, EPM, para la continuación de la  construcción de la represa Hidroituango.

De acuerdo a Isabel Cristina Zuleta, **las amenazas aumentaron cuando se inició con los desalojos por parte de la empresa,** pero aunque se ha realizado las denuncias pertinentes frente a la Fiscalía General y la Defensoría del Pueblo, "**La Unidad Nacional de Protección no ha brindado garantías al movimiento,** por el contrario, ha empeorado la situación de seguridad" asegura Zuleta, quien agrega que pese a que desde la UNP se había entregado unas motos para que las personas de la comunidad y el colectivo se movilizaran, lo que se entregó no tiene las condiciones adecuadas para que pueda ser usado.

Así mismo, los integrantes del Movimiento Ríos Vivos denuncian que con frecuencia, “se ha observado que ha sido el **personal de vigilancia privada de EPM los que a diario persiguen y presionan a barequeros** para que salgan de las playas del río Cauca, argumentando que fueron contratados para no permitir que alguien esté allí, estos han insistido a los barequeros en que deben salir y han llegado a impedir el ingreso por los caminos que conducen a las playas".

Las amenazas ya venían ocurriendo en la playa La Arenera, que fue desalojada el 27 de marzo de este año,  donde también estaba Eugenia y su familia que viven de la actividad barequera realizada ancestralmente en el Cañón del Río Cauca.

Las personas amenazadas exigen al gobierno nacional, que se **brinde medidas de protección colectivas e individuales,** las cuales se comprometió a adoptar la Unidad Nacional de Protección desde el año 2013 y hasta el momento no lo ha realizado. También se solicita que se realicen las investigaciones pertinentes con agilidad y se halle a los responsables, además de las razones por las cuales las amenazas y violaciones del derecho a la vida, libertad, integridad hacia miembros del Movimiento se han convertido en un asunto sistemático desde que se conformó el proceso organizativo de afectados por Hidroituango.

 
