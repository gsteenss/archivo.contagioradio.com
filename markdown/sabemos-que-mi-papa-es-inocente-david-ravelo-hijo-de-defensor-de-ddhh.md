Title: "Sabemos que mi papá es inocente” David Ravelo hijo de defensor de DDHH
Date: 2017-06-20 13:08
Category: DDHH, Nacional
Tags: David Ravelo, DDHH, Defensor de DDHH, Libertad
Slug: sabemos-que-mi-papa-es-inocente-david-ravelo-hijo-de-defensor-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/david-ravelo-en-la-carcel-e1497982086387.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PBI Colombia] 

###### [20 Jun 2017] 

Después de 7 años de estar en prisión, **hoy es dejado en libertad condicional el defensor de Derechos Humanos David Ravelo.** Su hijo, quien lleva el mismo nombre, afirmó una vez más que su padre es inocente y manifestó sentir alegría por la salida de Ravelo de la cárcel. Además recordó que Ravelo fue clave en señalar vínculos directos del ex presidente Uribe con paramilitares.

Ravelo, quien recuperaría la libertad hoy, **se encuentra detenido desde el 14 de septiembre de 2010** tras ser acusado por “homicidio agravado” por el crimen del ex candidato a la Alcaldía de Barrancabermeja, David Núñez Cala, en abril de 1991. (Le puede interesar: "[Defensor de DDHH David Ravelo en libertad tras siete años de prisión](https://archivo.contagioradio.com/david-rabelo-es-dejado-en-libertad-luego-de-siete-anos-de-prision/)")

Organizaciones civiles e internacionales como la Federación Internacional para los derechos humanos FDIH y la Oficina Internacional de los Derechos Humanos- Acción Colombia OIDHACO, han venido manifestando desde 2014 **las irregularidades que tuvo y ha tenido el proceso en contra de David Ravelo.**

De igual forma, en repetidas ocasiones la Organización *Front Line Defenders*, que protege a los defensores de derechos humanos en riesgo, ha manifestado que a Ravelo “**no se le han garantizado los derechos y las garantías exigidas en la normatividad nacional e internacional** de legítima defensa y juicio justo”. Por lo tanto, junto con las demás organizaciones, han pedido, desde hace 3 años, que Ravelo “debe ser puesto en libertad y debe tener acceso a un proceso judicial justo e imparcial”.

**“Mi padre fue víctima de un falso positivo judicial”**

David Ravelo hijo, manifestó que, si bien su padre estará en libertad condicional, “vamos a hacer todo lo que se deba hacer para que en la JEP o donde sea **se sepa que mi papá es inocente y que fue víctima de un falso positivo judicial**”. Afirmó además que el paramilitar Mario Jaimes Mejía alias “el panadero”, “ha mentido en repetidas ocasiones y lo están investigando por falso testimonio”. (Le puede interesar: "[Continúa dilación en caso del defensor David Ravelo Crespo](https://archivo.contagioradio.com/continuan-las-dilaciones-en-caso-de-david-ravelo-crespo/)")

En 2015 David Ravelo hizo pública una carta a través del MOVICE en donde manifiesta las irregularidades generales que ha tenido su proceso. Ravelo destaca que en varias ocasiones la Misión de Apoyo al Proceso de Paz en Colombia de la OEA le advirtió de amenazas contra su vida por parte de grupos paramilitares. Él estableció que “**los asesinos cambiaron de estrategia y parece que desistieron del asesinato físico** y emprendieron la persecución para ejercer el asesinato judicial”.

Según David Ravelo hijo, su padre fue víctima de un “falso positivo judicial” porque en el 2007 el defensor se atrevió a denunciar una **reunión entre el hoy senador Álvaro Uribe Vélez y un grupo de paramilitares en Puerto Berrío Antioquía**. Ravelo hijo afirmó que “Como no lo pudieron asesinar, le están cobrando su valor y coraje para enfrentar al paramilitarismo”.

<iframe id="audio_19371170" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19371170_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
