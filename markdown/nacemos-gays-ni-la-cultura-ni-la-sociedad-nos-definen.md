Title: "Nacemos gays, ni la cultura ni la sociedad nos definen".
Date: 2015-10-27 20:02
Category: closet, LGBTI
Tags: Construcción Cultural, Construcción Social, Derechos Humanos, Estereotipos, gays, Heterosexualidad, Hombres Gays, Homosexualidad, Imaginarios, Lesbianas, LGBTI, Prejuicios, Sigla G
Slug: nacemos-gays-ni-la-cultura-ni-la-sociedad-nos-definen
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/MG_9310-e1526584740197.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Contagio Radio 

###### <iframe src="http://www.ivoox.com/player_ek_6734122_2_1.html?data=l5yglpaWdo6ZmKial5mJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8rbzcaYqZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

###### [Programa 9 de febrero] 

**Clóset Abierto** brinda un espacio para hablar de la sigla **G (hombres gays)**, población visible pero rechazada socialmente a causa de los estereotipos que impone una normativa **heterosexual**, incluidos en las siglas **LGBTI **pero sin ser relevantes a la hora de hablar de derechos, pues son las mujeres lesbianas las más favorecidas en esta materia; se resalta su forma de vivir la **homosexualidad**, desde el hombre gay más masculino hasta el más afeminado y amanerado.

Basados en los prejuicios que se imponen dentro de una “cultura”, los invitados **Mario Umaña, Juan Pablo González y Nicolás Torres** hablan desde su experiencia personal sobre la inquietud basada en nacer o construirse como una persona homosexual según los factores que puedan afectar a nuestro alrededor; diversas teorías se centran en que se trata de algo genético, mientras otras sostienen que es una construcción social y/o cultural.

Tras el debate planteado se concluye desde los testimonios de los tres invitados que siempre existió una preferencia sexual orientada hacia personas de su mismo género y que sin importar su edad habían sensaciones que demostraban que ese gusto creció con ellos, la construcción se evidencia en el momento en el que deciden cómo actuar en cuanto a su personalidad y como se muestran refiriéndose a su apariencia física, lo que hace que las personas gays sean encasilladas en ciertos imaginarios.
