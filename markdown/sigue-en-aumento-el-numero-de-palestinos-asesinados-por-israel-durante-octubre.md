Title: Sigue en aumento el número de palestinos asesinados por Israel durante octubre
Date: 2015-10-19 13:11
Category: El mundo
Tags: Benyamin Netanyahu, Cisjordania, Muros en Jerusalén, Palestina, Proyecto de ley contra palestinos, Terrorismo de Israel
Slug: sigue-en-aumento-el-numero-de-palestinos-asesinados-por-israel-durante-octubre
Status: published

###### [Foto: Palestina Libre ] 

###### [[19 oct 2015]]

[De acuerdo con el Ministerio de Salud Palestino, los ataques perpetrados por Israel desde principios de este mes en la Franja de Gaza, Cisjordania y los territorios ocupados ilegalmente por colonos israelíes, han provocado la **muerte de por lo menos 45 palestinos, mientras que cerca de 1829 han resultado heridos**.]

[Rami Hamdalá, Primer ministro de Palestina, anunció este domingo que “El régimen de Tel Aviv es el directamente responsable de la escalada de violencia contra los palestinos en sus propios territorios… las **ejecuciones sumarias** \[de civiles palestinos, en especial, **mujeres y niños** autorizadas por Benyamin Netanyahu\] **demuestran el creciente terrorismo, racismo y extremismo** entre los soldados y colonos israelíes”.]

[Hamdalá insistió en la creación de un comité internacional para investigar estos “crímenes”, presionando la necesidad de “**Poner fin a la ocupación palestina por el régimen israelí** para crear un Estado palestino soberano e independiente con Al-Quds como capital”.]

[Entre tanto, el **parlamento israelí aprobó un proyecto de ley contra los palestinos** que faculta a los soldados israelíes para detener e inspeccionar a cualquier palestino que les parezca sospechoso de incitar la violencia y autoriza la **construcción de nuevos muros en Al-Quds (Jerusalén)** para separar a los palestinos de los israelíes.]

[Este domingo se registraron **[ataques con gases lacrimógenos y balas de goma lanzados por Israel a palestinos que se manifestaban](https://archivo.contagioradio.com/asi-operan-las-estructuras-paramilitares-de-israel-en-medio-de-las-protestas-palestinas/)** en Cisjordania, en las ciudades de Ramalá y Al-Jalil (Hebrón) y la región de Tulkarem (norte).   ]
