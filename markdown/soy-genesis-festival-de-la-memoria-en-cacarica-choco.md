Title: Soy génesis, festival de la memoria en Cacarica - Chocó
Date: 2017-02-28 16:48
Category: DDHH, Otra Mirada
Tags: cacarica, militares, Operacion genesis, paramilitares
Slug: soy-genesis-festival-de-la-memoria-en-cacarica-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/soy-genesis.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo - Contagio Radio ] 

###### [28 Feb. 2017] 

Entre música y baile, **las comunidades de Cacarica se reunieron para conmemorar los 20 años de la Operación Génesis**, en la que cerca de 6 mil afrodescendientes fueron desplazados por una operación militar y paramilitar en su territorio y por el que tuvieron que vivir 4 años desplazados, hacinados en el Coliseo de Turbo.

Luego, estuvo el regreso a su territorio en medio de muchas dificultades, como la continuación de la presencia paramilitar, el  hostigamiento por parte de los militares y por las empresas presentes en la región. Un retorno que también ha estado marcado por la dignidad de este pueblo afrodescendiente.

**Jhon Jairo Mena, habitante de Cacarica** e integrante de la Junta Directiva de las Comunidades de Autodeterminación, Vida y Dignidad – CAVIDA – contó que **“a pesar de todas las dificultades y ese dolor que hemos tenido que padecer durante estos 20 años, eso mismo nos ha dado fuerzas** y nos la seguirá dando fuerza para seguir luchando por mantener la dignidad dentro del territorio”.

La conmemoración inició el día 24 de Febrero en Turbo con una marcha de antorchas “fue muy bonito este acto donde participaron las comunidades y donde compartimos posteriormente cómo hemos podido avanzar a pesar de vivir en este mundo de impunidad” recalca Mena.

Comenzaba a salir el sol y hacia las 7:30 a.m. el 25 de Febrero, se dieron cita en el puerto de Turbo para salir por el Golfo de Urabá y tomar el Río Atrato hasta llegar a Cacarica **“esta vez la conmemoración o el festival de la memoria** fue en la Zona Humanitaria Nueva Esperanza en Dios. Para nosotros **fue algo muy alegre, motivador, porque a pesar de que seguimos con ese dolor y con esa memoria de lo que nos pasó hoy, después de 20 años, queremos dar un nuevo salto**” dice Mena.

Este salto, dicen las comunidades, no será fácil pues tendrán que luchar contra los poderes presentes en el territorio. Le puede interesar: [Mujeres de Cacarica: 20 años espantando la guerra](https://archivo.contagioradio.com/mujeres-de-cacarica-20-anos-espantando-la-guerra/)

La memoria que estuvo presente durante todo este festival fue lo que acompañó las actividades, para Mena uno de los momentos más significativos fue recordar a través de otras personas comprometidas con el proceso organizativo **“uno a uno los pasos y todas las cosas que hemos logrado con todas las dificultades**, entonces vemos que hemos tenido logros dentro de un mar de impunidad. Esa luz de la esperanza sigue allí".

### **Operación Génesis, la memoria y los jóvenes** 

Los jóvenes hoy, después de tanto tiempo, continúan manteniéndose en el territorio y han trabajado por guardar la memoria. **Casi el 95% de la juventud se mantiene y uno de los compromisos es empoderar a estos jóvenes para que mañana “ellos tomen las banderas** que vamos dejando las personas que pasamos a un segundo plano (…) porque los años van pasando” manifiesta Mena. Le puede interesar: [Con lista en mano paramilitares amenazan pobladores de Cacarica](https://archivo.contagioradio.com/paramilitares-amenazan-pobladores-cacarica-36208/)

Los jóvenes de Cacarica dicen que tienen las ganas de seguir luchando por su territorio “tenemos unos muchachos que tienen carisma, voluntad, deseos de continuar y que están dispuestos a dar el todo por el todo en la defensa de su territorio, pese a las dificultades que puedan afectarnos” relata Mena.

Además cuenta Mena, que contrario a lo que piensan muchas personas, para la comunidad de Cacarica, los jóvenes significan el presente “quienes fueron los que se encargaron y se empoderaron de esta conmemoración”.

### **Los retos 20 años después de la Operación Génesis** 

**“Me hace traer una sonrisa al rostro”** contesta Jhon Jairo Mena ante esta pregunta, pues según este líder se han visto enfrentados a "una pelea del huevo contra la piedra", huevo que no ha podido ser destruido y que por el contrario, **al pasar de los años ha contado con el apoyo de muchas organizaciones y personas que se han unido a esta causa**.

Pese a que existe una orden de la Corte Interamericana por cumplirse, esta ha sido ignorada por el Estado, pero las comunidades confían en que su trabajo será escuchado en algún momento “va a tomar un nuevo aire, no es nada fácil pero hoy tenemos la esperanza de que nuestra propuesta continuará tratando de romper las barreras”. Le puede interesar: [Cerca de 650 Paramilitares ejercen control territorial en Cacarica](https://archivo.contagioradio.com/650-paramilitares-ejercen-control-en-cacarica-35568/)

El fenómeno del paramilitarismo que sigue aquejando a las comunidades de Cacarica es otro de los retos a los que se enfrentan, estructuras que han ingresado al territorio y han realizado amenazas a líderes que "son la piedra en el zapato. Pero **a pesar de las dificultades, el ojo de la esperanza de las comunidades que están en el territorio sigue siendo la organización CAVIDA, entonces seguiremos más fuertes, seguiremos luchando**” concluye Mena.

<iframe id="audio_17276026" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17276026_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
