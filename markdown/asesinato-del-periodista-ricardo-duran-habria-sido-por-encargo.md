Title: Asesinato del periodista Ricardo Durán habría sido "por encargo"
Date: 2016-01-20 18:05
Category: El mundo
Tags: Ricardo Durán, Venezuela
Slug: asesinato-del-periodista-ricardo-duran-habria-sido-por-encargo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Ricardo-Duran.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Alba Ciudad 

<iframe src="http://www.ivoox.com/player_ek_10143733_2_1.html?data=kpWelpibd5Shhpywj5aaaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncaLnxtjW0MbYs4zYxtGY0srWrdDYytjhw5C2rcTV08ncjanZtoa3lIquk9OPrMLW04qwlYqliMKf1M7R0ZCJdpPkjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ana Teresa Pérez] 

###### [20 Ene 2015] 

Durante la madrugada del miércoles, apareció asesinado en su apartamento en Caracas, **Ricardo Durán, el periodista de 40 años,** que trabajó en la Venezolana de Televisión y además, fue director de Comunicación e Información de la Asamblea Nacional.

Aunque en un inicio se había asegurado que se trataba de un robo, **lo cierto es que Durán habría puesto resistencia y los sicarios habrían disparado,  y huyeron del lugar sin llevarse el carro** del periodista ni sus pertenencias.

Debido a la forma como se presentó el asesinato, y por su trabajo periodístico, las autoridades venezolanas afirmaron que la muerte del jefe de prensa del gobierno de Caracas habría sido **"por encargo", declarando tres días de duelo en Caracas.** Además, el Ministerio Público comisionó al fiscal 55° del área metropolitana de Caracas, Miguel Hernández, para investigar el asesinato del periodista.

El alcalde del municipio caraqueño Libertador, Jorge Rodríguez, indicó que "las primeras investigaciones señalan que resultó un asesinato por encargo, que no vengan ahora a tratar de enturbiar y obstaculizar las verdadera razones".

Ricardo fue periodista de Venezolana de Televisión y Director de Comunicación e Información de la Asamblea Nacional. También condujo varios programas en Radio Nacional de Venezuela (RNV), que lo hizo merecedor del Premio Nacional de Periodismo 2009, en la categoría Opinión mención Radio. Hace pocos días fue nombrado director de prensa del Gobierno del Distrito Capital. Además, participó en numerosos programas como "La Hojilla" junto a  Mario Silva.

Ana Teresa Pérez, amiga y periodista de Durán, asegura que su compañero murió en su zona de residencia en extrañas circunstancias, y pese a que sabía que su vida podía estar en riesgo debido a sus investigaciones y publicaciones, prefería no estar escoltado, pues **se caracterizaba por ser una persona “de a pie y muy sencillo”, dice Ana Teresa.**

“Ricardo era un periodista muy dateado en política, que **representaba un peligro** para muchos... era **muy comprometido y sin miedo”**, señala la amiga del periodista.
