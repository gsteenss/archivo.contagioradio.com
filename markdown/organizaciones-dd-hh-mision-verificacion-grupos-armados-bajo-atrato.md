Title: Organizaciones de DD.HH. piden misión de verificación ante incursiones de grupos armados en Bajo Atrato
Date: 2019-02-28 13:03
Category: Comunidad, DDHH
Tags: Bajo Atrato, Curvarado, Incursiones Paramilitares, Jiguamiandó
Slug: organizaciones-dd-hh-mision-verificacion-grupos-armados-bajo-atrato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### 28 Feb 2019 

**La Oficina Internacional de Derechos Humanos - Acción Colombia (OIDHACO)** expresó su preocupación frente a la presencia y enfrentamientos entre actores armados en las cuencas de **Jiguamiandó y Curvaradó, en Chocó.** Específicamente ante los sucesos de las últimas dos semanas en las que registraron múltiples incursiones de las Autodefensas Gaitanistas de Colombia (AGC) y del ELN en dichas Zonas Humanitarias del **Bajo Atrato**, amenazando y sitiando a las comunidades.

A través del comunicado, la organización advierte que aunque existen numerosas denuncias por parte de las comunidades rurales y de la **Comisión Intereclesial de Justicia y Paz (CIJP)**,  el Estado no ha ofrecido las garantía de seguridad y prevención a la población civil, lo que evidencia la expansión del control territorial de las AGC y del ELN en el Bajo Atrato; tal situación se da en medio de un proceso de restitución de tierras estancado.[(Le puede interesar: Paramilitares de AGC intentan asesinar a líder en Zona Humanitaria de Jiguaminadó, Chocó)](https://archivo.contagioradio.com/neoparamilitares-intentan-asesinar/)

### **La compleja situación del Bajo Atrato** 

La red, en la que están incluidas más de 30 organizaciones europeas e internacionales señala que las comunidades de **las cuencas de Jiguamiandó y Curbaradó enfrentan un mayor riesgo,** al no solo ser receptoras de excombatientes de las Farc, sino que paralelamente aportan con información y testimonios ante los mecanismos del Sistema Integral de Verdad, Justicia, Reparación y Garantías de No-Repetición. [(Lea también Paramilitares están retomando el Bajo Atrato)](https://archivo.contagioradio.com/paramilitares-estan-retomando-el-control-del-bajo-atrato/)

Ante dicha situación, la Oficina Internacional de Derechos Humanos **ha solicitado a la Unión Europea y en particular a las naciones de Suiza y Noruega instar al Gobierno para convocar una misión civil y humanitaria de verificación**, avanzar en el esclarecimiento e investigaciones de los crímenes cometidos en la región y darle celeridad a las denuncias con respecto a los planes de atentado en contra de integrantes de la CIJP y los líderes y lideresas de la región.

###### Reciba toda la información de Contagio Radio en [[su correo]
