Title: Huelga nacional carcelaria se extiende 10 días más
Date: 2016-02-09 16:55
Category: DDHH
Tags: crisis carcelaria, huelga nacional
Slug: huelga-nacional-carcelaria-se-extiende
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Crisis-carcelaria-e1482755978940.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radio Chile 

###### [9 Feb 2016] 

Prisioneros de diversas cárceles del país aseguran que **extenderán durante 10 días la Huelga Nacional que se desarrolla en los centros penitenciarios del país,** incluso se ha anunciado que a más de 10 cárceles y penitenciarias del país se han unido a esta jornada de desobediencia pacífica, hasta tanto no exista una respuesta integral frente a los acuerdos incumplimos por parte del gobierno nacional.

Durante el segundo día de protesta nacional y el día  veinte de la jornada que adelantan los internos del Eron Picota, se realizó una reunión con representantes autoridades institucionales como **la MAAP-OEA, Procuraduría, Consejo Superior de Política Criminal, USPEC- FidoFiducia, INPEC y** los voceros de los presos de La Picota, donde se expuso la situación que enfrentan los internos de las cárceles que viven en medio del hacinamiento y sin atención médica.

Tras la reunión se concretó un plan de acción programático  frente al tema de la salud de toda la población carcelaria del país. El próximo 8 de marzo se realizará una auditoría con la Corporación Solidaridad Jurídica, en donde se verificarán los avances en materia de atención en salud.

Desde la coordinación de la  Procuraduría se afirmó que “**es imposible que sigan muriendo seres humanos en las cárceles y no haya sanción disciplinaria o penal alguna para los funcionarios de las autoridades correspondientes”.**

Así mismo se programó otra reunión para el 16 de febrero, en la que se tratarán temas que tienen que ver con la situación de violación los Derechos Humanos,  tratos crueles inhumanos y degradantes,  así  como la corrupción al interior de los centros carcelarios.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
