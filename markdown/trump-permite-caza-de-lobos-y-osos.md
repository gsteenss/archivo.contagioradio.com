Title: Por orden de Trump crías de osos y lobos podrán ser cazados
Date: 2017-04-24 18:53
Category: Animales, El mundo
Tags: Donald Trump, Estados Unidos
Slug: trump-permite-caza-de-lobos-y-osos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/trump-e1493077662738.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### [24 Abr 2017] 

Donald Trump continúa dando pasos hacia atrás en la lucha contra el cambio climático y la conservación de la naturaleza. Esta vez los lobos y los osos son las víctimas, pues una firma del magnate da vía libre para que de ahora en adelante se pueda** cazar osos, lobos junto a sus crías en sus madrigueras, incluso cuando estén en época de hibernación.**

Se trata de una **iniciativa legislativa promovida por el congresista republicano Don Young,** representante de Alaska, y que fue aprobada por la Cámara y el Senado de Estados Unidos, con 225 votos a favor, contra 193.

"No es una cuestión de pequeños osos grises o polares, mucho menos de los animales que vemos en televisión. Esto tiene que ver con el derecho que posee el Estado para manejar ciertos problemas federales. Es el derecho del Estado para administrar los asuntos de su vida salvaje, y no permitir que el gobierno federal lo haga", argumentó el congresista republicano, Don Young.

Esta es una Ley que deroga los avances en esta materia que se habían logrado con la administración de Barack Obama en cuyo gobierno hace apenas un año se había prohíbido dicha actividad. Ahora Trump permitirá que los **cazadores puedan dispararles a estos animales desde helicópteros o aviones.** También se acepta el uso de cebo, las trampas de ajuste, la caza aérea y la matanza de osos, lobos y coyotes en su madriguera.

Para organizaciones ambientalistas y protectoras de los animales,  significa el final de la protección de estos animales en la reserva nacional de Alaska, ya que se desprotege la zona donde habitan estas especies, es decir, **las 16 reservas destinadas a garantizar la vida silvestre, una superficie de 300.000 km2. **

"Lo que la Casa Blanca hizo quedará grabado en la memoria de cada persona que ame a los animales. Si el Senado y el Presidente están de acuerdo, entonces **ya veremos familias completas de osos y lobos muertas y cazadas en sus propios refugios**", expresa Wayne Pacelle, representante de la organización Humane Society, quien agrega que ese uso de armas no está justificado por ningún motivo.

Cabe recordar que **los hijos de Donald Trump, no esconden su gusto por la caza de animales.** Así lo han demostrado las fotografías de Eric y Donald Jr posando al lado de cadáveres de elefantes, guepardos, antílopes o búfalos. “Mis hijos aman cazar. Son orgullosos miembros de la NRA”, había dicho Trump cuando fue precandidato republicano.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
