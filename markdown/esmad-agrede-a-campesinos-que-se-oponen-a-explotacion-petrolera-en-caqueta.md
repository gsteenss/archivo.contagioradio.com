Title: ESMAD agrede a campesinos que se oponen a explotación petrolera en Caquetá
Date: 2016-06-21 13:12
Category: Entrevistas, Nacional
Tags: Emerald Energy en Colombia, ESMAD, extracción petrolera Caqueta, petroleo
Slug: esmad-agrede-a-campesinos-que-se-oponen-a-explotacion-petrolera-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Valparaiso-Caquetá1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo ] 

###### [21 Junio 2016 ] 

Este lunes, efectivos del ESMAD agredieron con gases lacrimógenos a los campesinos que estaban en inmedicaciones de los municipios de Valparaiso, Milan y Morelia, en Caquetá, intentando desalojar a los funcionarios de la empresa 'Petroseismic Services', filial de la multinacional 'Emerald Energy', mientras **realizaban actividades de exploración petrolera sin la autorización de las comunidades**.

Según José Antonio Saldarriaga, poblador de la región, desde hace dos años, las familias se manifiestan pacíficamente contra la actividad petrolera que pretende implementarse en la región sin su autorización e impactando negativamente los ecosistemas, particularmente las fuentes hídricas. Durante este tiempo han realizado acciones como encadenamientos, hasta de dos meses.

"Defendemos el territorio, el agua y el futuro de las próximas generaciones (...) nos causa mucha tristeza que el 95% de nuestra población haya sido desplazada por la violencia y que ahora que regresamos, **nos quieran desplazar por los proyectos extractivistas de las multinacionales con la venía del gobierno nacional**, no estamos dispuestos a dejarnos (...) es una vergüenza que nos traten como terroristas cuando defendemos el territorio", afirma Saldarriaga.

"Nosotros somos dueños de los predios, tenemos títulos desde hace 50 años, no estamos invadiendo territorios, estamos con todos los derechos legales, pero el Gobierno quiere hacernos ver como ilegales para sacar los hidrocarburos", agrega el poblador, y asevera que entre mayo y julio de 2015, cuando estuvieron encadenados oponiéndose a la empresa, **el coronel Alexander Díaz, los tildó como terroristas por impedir el paso de una multinacional con titulación minera** en ocho municipios del departamento.

El 30 de junio del año pasado, los campesinos fueron reprimidos por la fuerza pública, en el hecho resultaron tres personas heridas de gravedad, al día siguiente f**uncionarios de la multinacional aseguraron a las comunidades que contaban con el dinero suficiente para pagar veinte muertos**. Esta agresiones y aseveraciones fueron remitidas al Ministro del Interior, quien aseguró que la implementación del proyecto de extracción petrolera no estaba en discusión por ser de interés nacional.

<iframe id="audio_11979222" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_11979222_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]][ ] 
