Title: Andrée Viana Garcés - Sucre: un mapa para elegir
Date: 2014-12-17 15:41
Author: CtgAdm
Category: Opinion
Slug: andree-viana-garces-sucre-un-mapa-para-elegir
Status: published

Sucre es un pueblo de 6000 habitantes, enclavado en una región en donde los procesos de base se hermanan entre ellos, pero duramente golpeada por todos los actores del conflicto, que han ejercido diversas presiones sobre su territorio, han permeado las dinámicas de poblamiento de algunas veredas, han abierto camino a prácticas económicas devastadoras, han implantado prácticas de desarticulación familiar, de sometimiento de género y desde luego han aterrorizado, matado y desplazado a su población.

Probablemente esa huella en la historia de la región ha determinado un ADN social específico, que ha dado varias generaciones de organizaciones y ciudadanos muy activos, pero ya cansados de las estrategias de la violencia, y en cambio expertos en fortalecimiento de redes y resistencias pacíficas.

Sucre tiene una rica historia en la gestión comunitaria del agua  Sus habitantes construyeron un acueducto artesanal con capacidad para llevar agua a 450 viviendas. Cuando en el año 1999, Sucre se convirtió en municipio, esas 450 familias conformaron un comité de usuarios para administrar el  acueducto municipal.

Desde entonces, la suma de esfuerzos ciudadanos e institucionales funcionó como una fórmula exitosa: el manejo comunitario se mantuvo sobre infraestructuras mejoradas con las inversiones públicas en redes y en la planta de tratamiento.

Veinte años después empezó el auge de la privatización del agua. Y en 2010 esa lógica llegó al Plan Departamental suscrito por todos los municipios del Cauca, con excepción de tres, dentro de los que estaba Sucre.  Ese gesto, en defensa del agua como bien público, fue el fruto de la presión ciudadana sobre el alcalde de turno.

Pero el éxito fue relativo. Desde la Alcaldía se inició una estrategia de desprestigio del acueducto comunitario sobre argumentos relacionados con la potabilidad del agua, la eficacia de la administración, y alertas sobre un manejo insalubre del recurso.  Así se fundamentó la decisión de entregar el sistema de aguas a una empresa de economía mixta por acciones.

La decisión fue cubierta con un manto de simulación participativa para evitar confrontaciones directas de la ciudadanía: el Alcalde emitió una cantidad determinada de acciones para que el Comité de Usuarios las comprara y así accediera a la administración del acueducto.  Desde luego, ni el comité, ni las familias que lo conforman, ni ningún campesino del municipio tenían capacidad económica para comprar acciones en una proporción que les permitiera verdadera incidencia en las decisiones de manejo.

En reacción, las dos organizaciones de base más fuertes del municipio (los bienandantes y el Comité de Usuarios) hicieron una larga y bien estructurada campaña para que el acueducto les fuera devuelto. Con el cambio de alcalde, el nuevo mandatario abrió una licitación que terminó adjudicándose al Comité de Usuarios. Desde 2013 son los mismos habitantes de Sucre los que manejan el agua del municipio y han ido asegurando la reforestación de la boca toma y la destinación del predio en que se encuentra.

Sucre no es, entonces, un pueblo de desentenidos. Ellos, como sus vecinos de la Vega, son campesinos activos y responsables en el ejercicio de su condición ciudadana. Conforman una sociedad civil con apuestas vitales en la gestión democrática de los recursos de que depende su pervivencia e identidad.

En esa línea, el pasado 7 de noviembre 300 líderes campesinos de más de 30 veredas se reunieron en Asamblea en el teatro del pueblo, para preparar otro ejercicio democrático de los que ya son tradición en esa región.  Otra vez de largo aliento pero quizás más complejo que el del acueducto.

La meta es la misma: la gestión democrática de los recursos.  Pero esta vez está en juego, no sólo la protección de los recursos naturales como bienes públicos, sino la salvaguarda del agua como derecho humano, la defensa de la soberanía alimentaria y el amparo del derecho al buen vivir.

El banderazo de salida es la alianza estratégica, natural en una democracia sana, entre el Alcalde y sus electores. La primera parte del recorrido es una consulta popular que defina la preferencia del pueblo entre dos opciones de futuro: de un lado, la opción de convertirse en un pueblo que albergue minas por varias décadas; y de otro lado la opción de conservar su hábitat bio-geográfico y mantener sus especificidades culturales como campesinos y agricultores.

El mapa de sucre está completamente traslapado por solicitudes de títulos mineros. Con su concesión se reconocerá a los titulares el derecho a iniciar la exploración sin más trámites ni condiciones. Es decir, la sola concesión trae consigo un riesgo inminente de generación de impactos sobre fuentes hídricas y ecosistemas inigualables.

Por todo, de Sucre es un caso que permitirá visibilizar la voluntad política del Gobierno de cumplir la sentencia C-123 de 2014.  Esa sentencia no ordena contarle o notificarle a los alcaldes las decisiones de adelantar proyectos mineros en sus territorios. Ordena, en cambio expedir un reglamento que debe construirse conjuntamente con ellos, para definir un proceso que garantice justamente su participación  en la adopción de las decisiones sobre la realización de proyectos mineros en su jurisdicción.

Ese proceso, según lo decidió la Corte, debe asegurar que gobierno central y local puedan celebrar acuerdos sobre temas como la protección de los recursos hídricos, la salud, la alimentación y la forma de vida de los habitantes, antes de decidir sobre la destinación del subsuelo de sus municipios.

Si los títulos solicitados en jurisdicción de Sucre se conceden sin tener en cuenta las realidades sociales, productivas y ecosistémicas, sin siquiera intentar un acuerdo con el Alcalde para proteger aguas, derechos, la fuerza democrática de ese pueblo caucano terminará reconducida a la frustración y el conflicto, y el buen vivir que han alcanzado a fuerza de organización terminará convertido en un episodio estorboso para el desarrollo que propone el modelo minero.

Ese desarrollo no sólo es extraño a los pobladores de Sucre, sino que tal como está planteado en Colombia, permite dinámicas perversas para la conservación de la diversidad social y cultural.

En efecto, como ya se ha vivido en municipios vecinos, el interés que se despierta con las solas solicitudes atrae prácticas que en la región se conocen como de mercenarios de la minería. Se trata de nuevos pobladores que se dedican a la minería ilegal y a aparentar lujos y comodidades ajenas a la vida campesina.  Cuando esas dinámicas empiezan a ser asumidas con normalidad, el camino a la gran minería queda abierto, y las posibles resistencias de base terminan neutralizadas por discursos individualistas de prosperidad y éxito.

Además, la industria minera ni genera empleo, ni ha logrado un modelo que permita que los pueblos que padecen sus impactos superen los niveles de pobreza, ni ha superado el paradigma de la invisibilidad política de las empresas que llevan treinta años incidiendo en el diseño de políticas públicas desde la perspectiva de intereses que están lejos de ser los del bienestar público; ni ha podido demostrar aún que tiene chance de ser ambientalmente sostenible.

La oportunidad que el caso de Sucre ofrece al gobierno central es inmensa.  Puede reinventar el modelo sólo respetando el principio democrático que soporta la autonomía territorial de los municipios, y por los cauces que la Corte ya le señaló.

Y el riesgo de hacerlo mal es igual o peor en proporciones: si ignora la orden de la corte, y sigue siendo sordo a los procesos de democracia participativa que reclaman una gestión territorial desde abajo y sostenible, entonces reconocerá que el Banco Mundial no se equivoca en predecir que los siguientes conflictos serán por el agua y los alimentos.
