Title: Fidel Castro encarnó la autocrítica necesaria de un revolucionario auténtico
Date: 2016-11-29 15:39
Category: El mundo, Otra Mirada
Tags: Cuba, Fidel Castro, Revolución Cubana
Slug: fidel-castro-encarno-la-autocritica-necesaria-de-un-revolucionario-autentico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/fidel-castro-iconica-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [29 Nov 2016]

“Hay dolor, mucha tranquilidad, pero pienso que la gente cree más en la resurrección que en la muerte. Fidel es un tipo que había resucitado. Su obra en diferentes órdenes asegura su permanencia cotidiana entre nosotros”, dice Joel Suárez, integrante del Centro Martín Luther King de Cuba.

Aunque mucho se ha dicho de la forma como **Fidel Castro trataba a los cristianos, Joel, asegura que una de las cosas que más resalta del ícono de la revolución cubana, fue la capacidad de reconocer que esa lucha necesitaba de todos**. En 1990 en Brasil, Castro evidenció su rechazo a este sector religioso, pero apenas un año después reconoció que se había equivocado, y la revolución cubana empezó a enriquecerse con los aportes de aquellos que desde la religión también luchaban por la revolución.

“El reconoce la **necesidad de rectificar la política de la revolución con relación a los creyentes**. Fidel estaba marcado por testimonio del cristianismo revolucionario de América Latina, y eso se evidencia por ejemplo en su amistad con la madre de Camilo Torres”, expresa Suárez.

En 1991 la gente no entendía por qué un partido comunista tenía cristianos. Fidel se da cuenta que el Partido Revolucionario de Cuba tenía que estar abierto a personas que asumían la revolución desde diferentes vertientes. Por eso, dos años después, la Constitución cubana de 1993 se transforma y refuerza la condena a la discriminación religiosa, lo que abre el camino a **una nueva etapa de relaciones entre cristianos y marxistas.**

La unidad es el segundo gran legado que el comandante Fidel le deja a los cubanos y al mundo. Suárez resalta que el líder revolucionario todo el tiempo **vivía proclamando que la felicidad del ser humano, debe pasar por la felicidad de todos**. La solidaridad por encima de la ganancia.

“Para los revolucionarios el testimonio de Fidel sobre la materialidad de la felicidad, **demostró que la revolución tiene que ascender hacia la solidaridad por un bien común.** Para él, la transición socialista es un proceso cultural de transformación de las relaciones con las personas para trabajar en nombre de la solidaridad”, indica el integrante del Centro Martín Luther King de Cuba.

En los últimos días Fidel se sostenía gracias a alimentos y productos naturales. Según Joel, se encontraba tranquilo y de vez en cuando salía de su casa. Hoy los cubanos se quedan con esa imagen que pudieron ver en la celebración su cumple años número 90. Aunque vienen muchos retos para Cuba, Joel Suárez asegura que estos los enfrentarán con base en **el legado del comandante Castro: la unidad y el amor solidario.**

<iframe id="audio_14301540" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14301540_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
