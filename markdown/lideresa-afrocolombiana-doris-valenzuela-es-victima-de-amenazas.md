Title: Lideresa afrocolombiana de Buenaventura es víctima de amenazas
Date: 2015-10-05 15:09
Category: DDHH, Nacional
Tags: buenaventura, Casas de pique, Doris Valenzuela líder afrocolombiana, paramilitares en Colombia, Sujetos Territoriales para la paz con Justicia Ambiental
Slug: lideresa-afrocolombiana-doris-valenzuela-es-victima-de-amenazas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### wp.me 

###### [05 oct 2015] 

En la mañana del domingo 4 de Octubre, **la líderesa afrocolombiana Doris Valenzuela,** recibió dos llamadas telefónicas amenazantes por parte de una mujer no identificada, en la que advierte conocer su ubicación actual.

En las comunicaciones, la mujer expresa textualmente “**soy tu peor pesadilla, no te podes esconder, donde sea los vamos a buscar, tu y yo tenemos una pelea casada por deslenguada, tenes que retirar la denuncia, así te escondan debajo de las piedras allá te voy a sacar”**, según dice la denuncia publicada por la Comisión de Justicia y Paz.

La oposición de la lideresa a la reclusión de menores por parte de paramilitares, entre quienes se encontraban su hija y varios niños de la comunidad, así como las diferentes denuncias y declaraciones, **le costaron la vida de su hijo Christian Aragón** a manos de las mismas estructuras paramilitares que pretendieron desmembrarlo en las llamadas Casas de Pique.

Las amenazas recibidas por Doris Valenzuela, se presentaron justo en el inicio de la Cátedra Abierta: **Sujetos Territoriales para la paz con Justicia Ambiental** que se realiza en la Ciudad de Bogotá.
