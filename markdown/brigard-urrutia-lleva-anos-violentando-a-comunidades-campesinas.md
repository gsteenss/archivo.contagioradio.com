Title: Brigard & Urrutia lleva años violentando a comunidades campesinas
Date: 2017-01-16 16:43
Category: DDHH, Entrevistas
Tags: Brigard y Urrutia, campesinos, Ley Zidres, Riopaila, Tierras baldías
Slug: brigard-urrutia-lleva-anos-violentando-a-comunidades-campesinas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Francisco-Uribe-Noguera-Colprensa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Red] 

###### [16 Ene. 2017]

Así lo aseguró el Ex-representante a la cámara por el Polo Democrático, Wilson Arias, frente al caso en el que está implicado el abogado Francisco Uribe quien hacía parte de dicha firma de abogados, y por el cual se aseguraba **habrían violado la ley al acumular a su favor los predios destinados a los campesinos sin tierra.**

**Este caso se remonta al año 2010 en el que varios congresistas del Polo Democrático denunciaran que la firma de abogados en la que Francisco Uribe Noguera era el socio principal durante los años en que se produjeron las compras**, había asesorado a estas empresas para apropiarse de tierras con antecedente de baldíos en la Altillanura.

**Firma que** según lo asegura un informe de investigación titulado *“Divide y comprarás” una nueva forma de comprar tierras baldías en Colombia*, realizado por Oxfam, **habría jugado un papel importante en la estrategia para asesorar a empresas que se quedaron irregularmente con terrenos baldíos. **Le puede interesar: [Cerca de 19 empresas son las que han acaparado más tierras en Colombia](https://archivo.contagioradio.com/cerca-de-19-empresas-son-las-que-han-acaparado-mas-tierras-en-colombia/)

Para Wilson Arias, quien sin una razón de peso le fue negado volver a acceder a este proceso, lo grabe de marginarle del mismo es que “de esta manera se está negando poder acceder a los contenidos de las sentencias para obrar contra ellos, para presentar recursos o al menos para que la sociedad conozca cuál fue el desenlace”.

Y es que según el Ex-representante **en este caso “hubo presiones enormes, grandísimas presiones en su oportunidad** (…) y el resultado es el secretismo y de reserva en mi opinión injustificada y los hechos tan graves que se presentaron, pero que fueron desestimados”.

### **¿Por qué este caso es importante?** 

Para el Ex-cabildante este caso es uno de los más “emblemáticos” de un acaparamiento irregular de tierras baldías de la nación y que podría tener dos implicaciones muy graves.

La primera, dice Arias “afecta el patrimonio de la nación. **Las tierras baldías le pertenecen a la nación y por ello hacen parte del patrimonio nacional**. De modo que cuando nos quitan tierras baldías de la nación irregularmente, se las están quitando a la totalidad de los colombianos y están ocasionando un detrimento patrimonial”.

Como segunda afectación esta que “**las tierras baldías de la nación tiene como único destinatario y lo dice la Constitución y la ley, el campesino pobre y sin tierra**. Así permitir el acceso a la propiedad de la tierra, a la gente sin tierra a los proletarios agrícolas y digamos que la normativa es democrática en el país”. Le puede interesar: [Persisten las amenazas contra el proceso restitución de tierras](https://archivo.contagioradio.com/persisten-las-amenaza-contra-el-proceso-restitucion-de-tierras/)

Sin embargo, pese a la normatividad, según Arias, **Riopaila y otras corporaciones han venido quedándose con esas tierras “en cantidades enormes** y eso coincide con un proceso de acaparamiento mundial de tierras que la hemos denominado extranjerización de la tierra”.

En los documentos presentados por el Ex-representante Arias, asegura que **al parecer Francisco Uribe Noguera fungió como una especie de “testaferrato”** dado que según él “Riopaila consiente de la gravedad de los hechos y no compró directamente las tierras sino que lo hizo a través de sociedades ficticias de papel que crearon en oficinas" y adiciona **"compraron 41 predios que sumaban 43 mil hectáreas y fueron compradas de manera sigilosa y con una especie de testaferrato por el señor Francisco Uribe Noguera”.**

Lo que sucedió después, según Arias, es que se entregaron esas tierras a Riopaila en Luxemburgo, un paraíso fiscal donde crean un holding por medio del cual se engloban las 43 mil hectáreas **“se hace todo un procedimiento como le llamó Uribe Noguera ‘muy sofisticado’ para terminar burlando la norma en Colombia”.**

Según la fuente, **esta decisión de la Judicatura podría deberse a lo influyentes que son personas como Francisco Uribe Noguera y su círculo cercano** “no podemos olvidar que Carlos Urrutia ex embajador de Colombia en Estados Unidos, padrastro de Uribe Noguera, se preciaba de la amistad con Santos, es decir es un asunto de vieja data”.

Para Wilson Arias, además de lo anterior este capítulo ha trascendido con lo acontecido con Rafael Uribe Noguera, hermano de Francisco que “violó a una niña indígena, completando un cuadro muy dramático y es que el bufete de abogados de su padre se quedaba con tierras campesinas y afectaba a las comunidades indígenas de los llanos orientales y un hermano de Francisco termina violando una niña indígena”. Le puede interesar: [La tristeza nos invade el corazón por el caso de Yuliana y de muchas mujeres en Colombia](https://archivo.contagioradio.com/la-tristeza-nos-invade-por-caso-de-yuliana/)

### **¿Qué queda por hacer?** 

Frente a la decisión de la Judicatura de absolver a Uribe Noguera en el caso de Riopaila lo que queda por hacer es una acción de denuncia y de solicitud de transparencia a la institución “**la idea es que sea algo así como que se ‘desclasifique’ el proceso, si valiera el término, que se permita el acceso y que sea de dominio público y el debate abierto de lo que actuación de la Judicatura y del bufete de abogados”.**

Adicionalmente, dice Arias debe trabajarse por buscar que se declare inexequible la Ley Zidres que permite acaparar las tierras baldías. Le puede interesar: [Ley  Zidres para  acaparadores de tierras afecta territorio colectivos](https://archivo.contagioradio.com/ley-zidres-para-acaparadores-de-tierras-afecta-territorio-colectivos/)

<iframe id="audio_16387888" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16387888_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).

<div class="ssba ssba-wrap">

</div>
