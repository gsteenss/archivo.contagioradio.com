Title: Más de cien mil investigaciones de la Fiscalía fortalecerán búsqueda de personas desaparecidas
Date: 2019-05-02 14:17
Author: CtgAdm
Category: Judicial, Paz
Tags: Desaparecidos en Colombia, Fiscalía general, Unidad de Búsqueda de personas dadas por desaparecidas
Slug: mas-de-cien-mil-investigaciones-de-la-fiscalia-fortaleceran-busqueda-de-personas-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Unidad-de-Búsqueda.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: : @UBPDBusqueda  ] 

Este jueves, la **Unidad de Búsqueda de Personas dadas por Desaparecidas (UBPD)** recibió de la Fiscalía 17 informes que incluyen 103.224 investigaciones de hechos relacionados con el conflicto armado en Colombia. Con  el acceso a esta información, se verá fortalecida la tarea que se ha propuesto la entidad para los próximos 20 años en

**Luz Marina Monzón, directora de la Unidad de Búsqueda** indicó que este será un elemento de gran importancia, en particular para "**responderle a miles de familias que buscan a sus seres queridos, que no los han podido encontrar",** destacando que uno de los grandes retos de esta nueva tarea será la consolidación de esta información como una base efectiva para la búsqueda de personas.

-   [(Lea también 2019 Unidad de Búsqueda de Desaparecidos llegará a 17 territorios)](https://archivo.contagioradio.com/unidad-de-busqueda-de-desaparecidos/)

> La [\#UBPD](https://twitter.com/hashtag/UBPD?src=hash&ref_src=twsrc%5Etfw) y la [@FiscaliaCol](https://twitter.com/FiscaliaCol?ref_src=twsrc%5Etfw) firmaron un convenio de colaboración para el acceso y gestión de información. [pic.twitter.com/toIqxP67Cm](https://t.co/toIqxP67Cm)
>
> — Unidad de Búsqueda UBPD (@UBPDBusqueda) [2 de mayo de 2019](https://twitter.com/UBPDBusqueda/status/1123976082124886016?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Según la Fiscalía, los 17 informes son una recopilación no individualizada y organizada por tipología delictiva **que incluye 99.114 investigaciones correspondientes a conductas delictivas atribuidas a la entonces guerrilla FARC y 4.110 sobre agentes del  Estado. Además relacionan a 84.723 víctimas y a 50.542 procesados**, incluyendo las siguientes categorías:

1\. Inventario del conflicto armado interno  
2. Retención Ilegal de personas por parte de las Farc-EP.  
3. Victimización a miembros de la Unión Patriótica por parte de agentes del Estado.  
4. Vinculación y utilización de niños, niñas y adolescentes por parte de las Farc-EP.  
5. Muertes ilegítimamente presentadas como bajas en combate por agentes del Estado.  
6. Violencia basada en género cometida por las Farc-EP.  
7. Violencia basada en género cometida por agentes del Estado.  
8. Retención y ocultamiento permanente de personas por parte de las Farc-EP.  
9. Retención y ocultamiento permanente de personas por parte de agentes del Estado.  
10. Fuentes y mecanismos de financiación de las Farc-EP.  
11. Victimización a líderes y defensores de derechos humanos por parte de agentes del Estado.  
12. Movilización forzada por parte de las Farc – EP.  
13. Movilización forzada por parte de agentes del Estado.  
14. Medios y métodos ilícitos de guerra utilizados por las Farc – EP.  
15. Muertes grupales y selectivas cometidas por las Farc- EP  
16. Muertes grupales cometidas por agentes del Estado  
17. Vínculos entre las Farc-EP e integrantes de las Fuerza Pública

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
