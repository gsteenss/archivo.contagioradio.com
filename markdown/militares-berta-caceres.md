Title: Dos militares están implicados en asesinato de Berta Cáceres en Honduras
Date: 2016-05-02 17:48
Category: Ambiente, El mundo
Tags: Berta Cáceres, CIDH, Copinh, honduras, militares
Slug: militares-berta-caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/hijos-de-berta-caceres-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: latribuna.hn] 

###### [2 May 2016] 

Según la información conocida hasta el momento dos integrantes de las fuerzas militares de Honduras fueron detenidos en la mañana de este lunes. Se trata del **mayor de infantería en servicio activo Mariano Díaz Chávez y el capitán de infantería en retiro,  Edilson Atilio Duarte Meza.**

Además fueron detenidos Douglas Geovanny Bustillo y Sergio Ramón Rodríguez, civiles **vinculados a la empresa DESA y concretamente al proyecto Agua Zarca** a cuya construcción se oponen  las comunidades y el Consejo de Organizaciones Populares e Indígenas de Honduras COPINH, el cual presidía Berta Cáceres, asesinada el pasado 3 de Marzo.

Sin embargo, la familia Cáceres emitió un comunicado en el que criticó duramente la acción de los organismos de investigación por haberlos excluido de[las investigaciones](https://archivo.contagioradio.com/gobierno-de-honduras-quiere-desviar-investigacion-del-asesinato-de-berta-caceres/) y al mismo tiempo afirmaron que con la captura de los integrantes de las fuerzas de seguridad se comprueba la [responsabilidad estatal](https://archivo.contagioradio.com/estado-bancos-y-empresas-estarian-detras-del-asesinato-de-berta-caceres-copinh/) en el asesinato de la líder indígena lo que sería **“razón suficiente para suspender el proyecto Agua Zarca de manera inmediata a y definitiva”.**

En el comunicado también hace énfasis en que se acepte la participación de expertos independientes de la Comisión Interamericana de DDHH para que continúe con la investigación y exigieron que las [pesquisas se dirijan también a los más altos responsables del asesinato](https://archivo.contagioradio.com/copinh-exige-que-bancos-internacionales-dejen-de-financiar-proyecto-agua-zarca/). Además resaltaron que ellos y ellas como familiares y allegados **deben enterarse de los avances en las investigaciones por los canales que por derecho les corresponden y no por los medios de información.**

Por otra parte, un conjunto de parlamentarios de Alemania liderados por Heike Haensel argumentaron que el gobierno de ese país debe[**exigir al gobierno hondureño que se permita la entrada de una comisión de investigación especial**](https://archivo.contagioradio.com/vaticano-pide-investigacion-imparcial-e-independiente-por-crimen-contra-berta-caceres/)como es el caso del CICIG en Guatemala y el CIEI en México.

La petición se hace unos días después de que el [presidente del Banco Mundial justificara el asesinato de la ambientalista](https://archivo.contagioradio.com/banco-mundial-justifica-el-asesinato-de-la-lideresa-berta-caceres/) y lo calificata como un "incidente" necesario en la aplicación de proyectos de gran embergadura.

#### *COMUNICADO:*

#### *Ante las actuaciones del Ministerio Público hondureño, exigimos inmediatamente la participación de la comisión internacional, independiente de expertos para la investigación del asesinato de nuestra Berta.*

#### *Olivia, Berta, Laura y Salvador, Zúniga Cáceres, hijas e hijo, Austra Bertha Flores madre, así mismo el Consejo Cívico de Organizaciones Populares e Indígenas de Honduras, COPINH, víctimas del asesinato de Berta Cáceres, recibimos con sorpresa la noticia de esta mañana acerca de capturas de personas involucradas en el vil crimen de nuestra Berta por parte del Ministerio Público y la Agencia Técnica de Investigación Criminal- ATIC.*

#### *La familia y el COPINH, al igual que toda la sociedad hondureña y la comunidad internacional, demandamos justicia y verdad en el asesinato de nuestra Berta.*

#### *Debido a que se nos ha excluido del proceso de investigación desde sus inicios, no tenemos forma de calificar si las capturas realizadas son producto de diligencias exhaustivas ni tampoco sabemos si estas comprenden la autoría intelectual en todos sus niveles.*

#### *Sin embargo, la noticia de la presunta participación de militares activos y retirados vinculados con la empresa pareciera demostrar el involucramiento de agentes estatales en el asesinato, lo cual debe ser profundizado, y es razón suficiente para suspender el proyecto hidroeléctrico Agua Zarca de manera inmediata y definitiva.*

#### *Lamentamos que las investigaciones realizadas por el Ministerio Público no se han desarrollado con nuestra participación, ni se hayan escuchado nuestras voces.*

#### *Hoy, a dos meses del asesinato de nuestra Berta, seguimos siendo revictimizados y debemos enterarnos de este tipo de noticias a través de los medios de comunicación y no por los canales que nos corresponden por derecho.*

#### *La desconfianza que todavía mantenemos en el Ministerio Público ha sido fomentada por sus propias actuaciones poco transparentes y que nos ignoran como víctimas.*

#### *Si el Estado realmente ha realizado una investigación exhaustiva, diligente, basados en pruebas científicas y respecto de todas las autorías (material e intelectual), hoy mismo debería aceptar la participación de la Comisión Interamericana de Derechos Humanos para que, a través de un grupo de personas expertas independientes se analicen las actuaciones hasta ahora realizadas y se establezca si las mismas son conformes con los mayores estándares internacionales de debida diligencia.*

#### *Si el Ministerio Público y su agencia han desarrollado de manera correcta su trabajo, no debe haber ningún temor en que esta comisión que la CIDH ya ha puesto a disposición, comience su investigación de manera inmediata.*

#### *Buscamos que haya verdad y justicia en el asesinato de nuestra Berta, si se logra, ganamos todos y todas.*

#### *¡Berta Vive!*

#### *Dado en La Esperanza, Intibucá a los 2 días del mes de Mayo de 2016*

#### *Olivia, Berta, Laura y Salvador Zúniga Cáceres, Austra Bertha Flores y el Consejo Cívico de Organizaciones Populares e Indígenas de Honduras, COPINH. Con el acompañamiento del Centro por la Justicia y el Derecho Internacional y el Movimiento Amplio por la Dignidad y la Justicia.*

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
