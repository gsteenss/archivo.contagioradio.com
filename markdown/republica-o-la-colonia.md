Title: República o la Colonia
Date: 2015-08-20 08:43
Category: Laura D, Opinion
Tags: Independencia de Colombia, Juan Manuel Santos, titulos mineros
Slug: republica-o-la-colonia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/bacteria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Imagen: Bacteria] 

#### **[Por Laura Duque - [~~@~~lavadupe](https://twitter.com/lavadupe)** 

[El 20 de julio, así como el 7 de agosto, muchos celebran la Independencia de Colombia; sin embargo me uno a esos que recordamos que desde el  Imperio español hasta nuestros días la Colombia ha sido dominada por colonos, locales y extranjeros. Lo que se dio en la primera etapa de este período “independentista” fue un desmembramiento de la patria –la Gran Colombia—, como una analogía de lo que sucedería cíclicamente hasta nuestros días.]

[Ha sido una farsa desde siempre; seguimos en la Patria Boba. Desde su gestación fue la independencia de los colonos nativos, los mismos que han perpetuado la cobranza desproporcionada de impuestos; los mismos que han vendido a granel el territorio nacional y cultural. ¡Esto pasa en frente de nuestras narices! Pero hemos estado noqueados por la violencia, la pobreza o el entretenimiento; o, en su defecto, por todo eso a la vez. ¡No nos engañemos!, la Colombia sigue padeciendo los síntomas de una vulnerable colonia.]

[Quise mencionar, a propósito de las recientes “celebraciones” –que prefiero llamar conmemoraciones—, el cómo se evidencian algunos signos malsanos de la herencia colonialista que han afectado la real consolidación de la República de Colombia.  ]

[A pesar de que el pueblo luchó por la anulación de exagerados tributos destinados a la corona española, aún hoy, nuestros dirigentes depositan en los ciudadanos –excepto en esa aristocracia criolla —cuantiosas cargas: impuestos directos e indirectos; impuestos nacionales, como el Impuesto al Valor Agregado –IVA—; impuestos departamentales y municipales, como la sobretasa a la gasolina, el Impuesto de Industria y Comercio, el Impuesto Predial Unificado, etc. Parece una “feria del impuesto” en la que año tras año pagamos por ver cómo nos roban el dinero que es de todos y para todos. ¡Eso sí!, impuestos inflados, pero salarios “moderados” (por no decir paupérrimos).]

[También la herida del saqueo mineral sigue abierta, y más profunda. Lo que antaño hicieron los españoles con nuestro oro y demás preciosidades; ahora lo disfrazan de “inversión extranjera”, sinónimo de progreso y desarrollo para todos, lo cual, por supuesto, es una falacia. De un tiempo para acá la fiebre minera llevó, sólo al gobierno Uribe, a otorgar cerca de 9.000 títulos mineros, sin respetar reservas naturales ni territorios étnico-culturales; y en el gobierno Santos, la infraestructura que se construye tiene el objetivo de acelerar el saqueo a cielo abierto y a gran escala. Hoy, 40% de los suelos están erosionados, principalmente, debido a la minería y a la deforestación. Una tragedia ambiental para el segundo país más diverso del mundo.        ]

[Como en los tiempos de la Colonia hay colombianos de todos los colores que día a día son explotados. Pese a que ya no es lícito azotar al otro, acá, empresas y multinacionales, amparadas por el Estado, han violado los derechos de los trabajadores: extensas y pesadas jornadas mal remuneradas, prohibición y persecución del libre derecho de sindicalización, despidos sin justa causa, el]*[boom]*[ de la tercerización y los contratos por prestación de servicios, maltrato… iniquidad.  Cambiaron la esclavitud por el salario mínimo.]

[Esos venidos del viejo mundo –según ellos de más alcurnia, linaje y dignidad— indicaron a los nuestros que había un ingrediente, ruin pero eficaz, para ejercer el poder y generar más réditos personales: la violencia, perpetuada por los “combos” de gobernantes locales; todos unos embajadores neocoloniales y neoliberales.]

[Hoy, como fósiles, reposan en Medellín los edificios de la Colonia invadidos por el comercio chino, gringo y europeo. Sólo en la emblemática carrera Junín, en el Edificio Antiguo Club Unión –Bien Inmueble de Interés Cultural, construido a finales del siglo XIX— se erige uno de los tantos símbolos del imperialismo contemporáneo: MacDonald´s, donde descaradamente  ponen en el individual de papel en el que sirven sus comidas rápidas: “Almuerzo colombiano” —no exijo que censuren la venta de hamburguesas, sólo pido que por favor no nos la vendan como un producto de la colombianidad, que ya de por sí es como una bruja; no existe, pero de que las hay, las hay—.]

[Quizá se conserva una mentalidad de vasallos, pues el hábito de comer lo que nos dan sigue más vigente que nunca: nuestro país produce 31,6 millones de toneladas de alimentos al año, exporta 4,4 millones, pero importa 10,3 millones de toneladas. Estamos desnudos ante los TLC y los gobiernos de turno nada que se animan a prestarnos el abrigo del Estado.]

[En la Colombia actual, 19 millones de hectáreas no son aptas para cultivar alimentos y se rumora que en 10 años el país sufrirá una crisis alimentaria.]

[Ojalá que como pueblo dejemos de actuar como corderos y mas bien nos convirtamos en los líderes y conductores de nuestra Colombia, como nos enseñó el entrañable Jaime Garzón. Y ojalá que esos “prohombres” no sigan asesinando la posibilidad de una República independiente y soberana.  ]
