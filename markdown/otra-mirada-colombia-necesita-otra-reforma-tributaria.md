Title: Otra Mirada: ¿Colombia necesita otra reforma tributaria?
Date: 2020-07-02 21:57
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: Ministerio de hacienda, Reforma tributaria, sistema tributario
Slug: otra-mirada-colombia-necesita-otra-reforma-tributaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/reforma-tributaria-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: acuda.org.co

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En diciembre del 2019 se adoptó una reforma tributaria y algunos de sus objetivos eran reducir la tributación a empresas y el cobro de impuestos a las personas naturales, sin embargo, esta reforma agravó la inequidad ya existente en el país. Teniendo esto en cuenta y debido al reciente anuncio por parte del Ministerio de Hacienda de la necesidad de una nueva reforma tributaria, las opiniones con respecto esta necesidad están divididas y muchos cuestionan si por el contrario, abrirá más la brecha de desigualdad. (Le puede interesar: [el 70% de municipios dependen de una red de salud publica iliquida y en pandemia](https://archivo.contagioradio.com/el-70-de-municipios-dependen-de-una-red-de-salud-publica-iliquida-y-en-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con el fin de entender qué representaría esta reforma para el país en medio de la crisis por la pandemia, invitamos a Juan Camilo Londoño, secretario del Consejo de Administración de ECOMUN y Exguerrillero de las FARC-EP, Consuelo Corredor, economista, profesora y miembro de la Academia Colombiana de Ciencias Económicas y Wilson Árias, senador de la República.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dado que las reformas tributarias se realizan pensando en reducir la desigualdad, unas de las preguntas a las que nuestros invitados dieron respuesta fueron a cuáles son los principios que deben regir esta reforma tributaria y si es posible que esta reforma sea una oportunidad para pensar y ayudar a las empresas emergentes o si por el contrario, aumentaría más el desempleo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, algunos de nuestros invitados generan prepuestas en cómo estar preparados para una reforma que podría estar permeada por la corrupción y tener como excusa la emergencia sanitaria para beneficiar a unos pocos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, manifiestan que la forma para evitar la toma de decisiones que respondan a ciertos intereses es acudir a la movilización popular y es preciso, llevar a discusión pública qué tipo de reforma necesita Colombia. (Si desea escuchar el programa del 1 de julio: [A dónde fueron a parar los recursos para combatir el COVID-19](https://bit.ly/2ZzWZLJ))

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F263395428289026%2F&amp;show_text=true&amp;width=734&amp;appId=289484308840495&amp;height=504" width="734" height="504" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
