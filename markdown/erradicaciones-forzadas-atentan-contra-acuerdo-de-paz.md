Title: Con erradicaciones forzadas se atenta contra Acuerdo de Paz: ASCSUCOR
Date: 2017-01-10 16:30
Category: Ambiente, Nacional
Tags: acuerdo de paz, Erradicaciones Forzadas, hoja de coca
Slug: erradicaciones-forzadas-atentan-contra-acuerdo-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Erradicación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [10 Enero 2017] 

La Asociación de Campesinos del Sur de Córdoba – ASCSUCOR- aseguró a través a un comunicado que desde el pasado 27 de diciembre **cerca de 100 soldados pertenecientes al Batallón Rifles están realizando erradicación forzada de cultivos de hoja de coca en el Sur del departamento de Córdoba.**

En la misiva manifiestan que de esta manera se está “desestimando la priorización del territorio en el marco de la implementación de los acuerdos firmados entre el Gobierno Nacional y las FARC-EP”.

**Para la Asociación de Campesinos, las erradicaciones forzadas desconocen el punto cuatro del Acuerdo de Paz (componente uno)** que “desde luego se conjuga con el acuerdo número uno: Reforma Rural Integral”. Le puede interesar: [Erradicaciones forzadas de coca no contribuyen a la paz](https://archivo.contagioradio.com/campesinos-del-catatumbo-dicen-no-a-erradicacion-forzada-de-coca/)

De igual modo, ASCSUCOR aseveró que los militares se han acantonado en el corregimiento de Tierradentro del Municipio de Montelibano, Córdoba con el mismo objetivo.

Los y las integrantes de **ASCSUCOR han recordado que muchas familias subsisten de este tipo de cultivo debido a “el abandono gubernamental, carencias socioeconómicas y falta de garantías de derechos humanos”** y añaden que “no se puede ser incoherente ante lo predispuesto en el acuerdo final, no se puede seguir degradando la Colombia rural y olvidada”. Le puede interesar: [Erradicaciones forzadas son otra manera de matar al campesinado](https://archivo.contagioradio.com/erradicaciones-forzadasotra-manera-de-matar-al-campesinado/)

Por lo anterior, los cultivadores y cultivadoras de hoja de coca han solicitado a las instituciones pertinentes, gesten los encuentros pertinentes para poder dialogar sobre este tema y llegar a acuerdos.

"Los cultivadores y cultivadoras de hoja de coca del sur de Córdoba están en total disposición de concertar los mecanismos  de implementación del acuerdo cuatro, componente uno, de sustitución voluntaria, concertada, con garantías y que desde luego se conjugue con el acuerdo número uno: Reforma Rural Integral”.

**Y concluyen responsabilizando al estado colombiano en cabeza del actual presidente Juan Manuel Santos de cualquier situación que pueda acontecer** en la región y que atente contra los derechos humanos de los líderes y personas que son parte de ASCSUCOR. Le puede interesar: [Erradicación de cultivos de uso ilicito van en contra de Acuerdo de Paz](https://archivo.contagioradio.com/erradicacion-de-cultivos-hecha-por-el-gobierno-iria-en-contra-de-acuerdo-de-paz/)

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
