Title: Las cárceles de Colombia: Un foco de Covid-19 olvidado por el Estado
Date: 2020-05-12 16:23
Author: AdminContagio
Category: Expreso Libertad, Nacional, Programas
Tags: #Cárcel #covid19 #IvánDuque #contagio #Crisis carcelaria, #Cárceles #Covid19 #Prisioneraspolíticas, #MovimientoCarcelario #Covid-19 #, carceles de colombia, Covid-19, Expreso Libertad
Slug: las-carceles-de-colombia-un-foco-de-covid-19-no-olvidado-por-el-estado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/COVID19-Prisons.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto por: Enterate24 {#foto-por-enterate24 .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

En este programa del **Expreso Libertad** María Cristina Molano, madre de la prisionera política Alejandra Méndez, denuncia el alto riesgo en el que se encuentra la población carcelaria de Picaleña en Ibagué de contraer el Covid 19, luego de que se conociera que 10 guardias del **INPEC** dieron positivo a la prueba.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

María Cristina alerta también sobre la situación de la prisionera política Lina Jiménez quien ya estaría en aislamiento por presentar sintomas. De igual forma el abogado Manuel García de la Comisión de Justicia y Paz y Uldarico Flores de la Brigada Eduardo Umaña manifestaron que hay una falta de cumplimiento por parte del Estado colombiano hacia la población reclusa.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por un lado debido a que cinco tutelas han fallado en favor a la población privada de la libertad en 4 cárceles del país, defendiendo el derecho a la salud y la vida, sin que hayan sido atendidas o resultas por el Ministerio de Justicia o el INPEC.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y por el otro porque la pandemia del Covid 19 pudo haber sido prevenida por parte del Estado en las cárceles, si hubiesen tomado las medidas y protocolos de bioseguridad correspondientes.

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/616457205885624/","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/616457205885624/

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:paragraph -->

[Ver mas: Programas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
