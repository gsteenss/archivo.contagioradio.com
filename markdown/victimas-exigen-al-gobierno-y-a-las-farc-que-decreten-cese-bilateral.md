Title: Víctimas exigen al Gobierno y a las FARC que decreten cese bilateral
Date: 2015-05-22 14:40
Category: Nacional, Paz
Tags: cese al fuego unilateral FARC-EP, cese bilateral, ELN, FARC, FFMM, Juan Manuel Santos, MOVICE
Slug: victimas-exigen-al-gobierno-y-a-las-farc-que-decreten-cese-bilateral
Status: published

###### Foto: contagioradio.com 

El Movimiento de Víctimas de Crímenes de Estado, emitió un comunicado público en el que resalta la urgencia de salvaguardar las conversaciones de paz decretando un cese bilateral de fuego. El **MOVICE** afirma que las víctimas en Colombia seguirán creciendo en número mientras persista la guerra y lamentaron la muerte de 26 integrantes de las FARC en Cauca, así como la muerte de soldados en el mismo departamento y en todas partes de Colombia.

El MOVICE recordó que, según el último informe del Frente Amplio por La Paz, entre febrero y abril del presente año se registraron 66 bombardeos en 14 departamentos del país, lo que arroja pérdida de vidas humanas, desplazamiento forzado, cultivos destrozados y pérdidas económicas, que pueden evitarse.

En el comunicado del Movimiento de Víctimas también exigen que se den los pasos necesarios para pactar un cese bilateral del fuego con la guerrilla del ELN, encaminado a facilitar el paso del proceso de conversaciones de paz que debería establecerse de manera directa y abierta con esa guerrilla.

 A continuación compartimos el comunicado de MOVICE:

*“Bogotá, 22 de mayo de 2015. El Movimiento Nacional de Víctimas de Crímenes de Estado (Movice) exige nuevamente al Gobierno Nacional, en cabeza de Juan Manuel Santos, y a las FARC-EP que decreten cese bilateral de las hostilidades !YA¡ para frenar el derramamiento de sangre en Colombia y evitar que se produzcan nuevos hechos que victimicen a la población colombiana.*

*El Movice lamenta el sufrimiento de cerca de mil pobladores del municipio de Guapí, Cauca, que en los últimos días se han visto obligados a desplazarse de su territorio a causa de la guerra, así como la muerte ayer de 26 insurgentes en ese departamento, a causa de bombardeos y ataque terrestre del Ejército.*

*Lamentamos también la muerte de nueve soldados profesionales y un suboficial el pasado 14 de abril en el Cauca, hecho por el que, en su momento, exigimos que una comisión independiente se encargara de esclarecer lo sucedido.*

*Las víctimas de crímenes de Estado hemos reiterado que la vía para alcanzar la PAZ es la negociación política del conflicto armado. El Gobierno Nacional ha insistido en negociar en medio de las hostilidades y confrontación bélica, lo que debilita la confianza de las partes en el proceso. Es importante recordar que, según el último informe del Frente Amplio por La Paz, entre febrero y abril del presente año se registraron 66 bombardeos en 14 departamentos del país, lo que arroja pérdida de vidas humanas, desplazamiento forzado, cultivos destrozados y pérdidas económicas, que pueden evitarse.*

*En el momento en que las FARC-EP anunciaron cese unilateral hicimos un llamado al Estado colombiano a responder de manera recíproca a esta decisión del grupo insurgente para que los diálogos cobraran coherencia y se generara un ambiente favorable de cara al proceso de refrendación de los acuerdos.*

*Hoy insistimos: urge  una tregua bilateral y un cese al fuego, que permitan el desescalamiento del conflicto, que cuente con efectivos mecanismos de verificación  y seguimiento pactados por las partes. Esta sería una muestra real por parte de los actores de su interés por poner fin al conflicto.*

*De otro lado, exigimos a la mesa de conversaciones de La Habana, Cuba, respeto por las propuestas presentadas por las delegaciones de víctimas que se reunieron con ellos, para quienes el CESE BILATERAL AL FUEGO fue una de las principales peticiones. No se puede continuar negociando en medio de las hostilidades, victimizando a la población civil y perdiendo vidas de compatriotas colombianos.*

*Es claro que la conformación de las delegaciones de víctimas evidenció el carácter bilateral del proceso de paz  (Dos partes del conflicto, que reconocen su responsabilidad) y la necesidad de incorporar en el proceso a las víctimas y sus propuestas. *

*Exigimos también que se busquen mecanismos para pactar un cese a las hostilidades con el ELN para generar un clima favorable al inicio de los diálogos con esta guerrilla.*

*Las víctimas de crímenes de Estado reiteramos nuestro apoyo al proceso de paz y nuestra exigencia a las partes de que, por ningún motivo, se retiren de la mesa sin concretar acuerdos y que asuman los compromisos para que se haga un cese bilateral al fuego !YA¡.”*
