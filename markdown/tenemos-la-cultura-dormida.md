Title: Tenemos la cultura dormida
Date: 2015-07-06 17:20
Category: Opinion, Schnaida
Tags: cultura ciudadana, Medellin, Metro, solidaridad
Slug: tenemos-la-cultura-dormida
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/422319605_1280x720.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FUNDEFAVA on Vimeo] 

#### [**[Ian Schnaida ](https://archivo.contagioradio.com/ian-schnaida/)- [~~@~~IanSchnaida](https://twitter.com/IanSchnaida) 

###### [6 Jun 2015] 

Para concienciar a la sociedad acerca de la solidaridad, no funcionan las charlas ni los regaños, lo que debemos hacer es emprender una campaña cívica con muñequitos, pancarticas en la calle y, obviamente, comerciales televisivos con actores de la farsándula criolla que pongan cara de serios y comprometidos. Eso sí que da resultado.

Creía que lo más grave que se había relajado en nuestra cultura era la moral, pero con ella se fue también la solidaridad, ambas están sentadas en unas poltronas con vista al mar mientras ven ahogar turistas.

Esta semana se montó una mujer en embarazo al Metro y como de costumbre no había puestos disponibles, así que se agarró de una de las varillas, apoyó suavemente su cabeza contra el aluminio frío, y entre una leve sonrisa y una leve tristeza, puso cara de musa para martir.

La mujer, que debió tener algún efecto sedante en su mirada, o quizá una estela de somníferos que expelía de su batola de rosas carmesí, ocasionó, apenas con cruzar las puertas, que cinco cabezas se cayeran dormidas sobre el respaldo de los asientos y el cristal; algunos, incluso, despegaron su mandíbula. Y qué pena despertarlos en solicitud de un asiento para la embarazada.

Yo, al ver que nadie hacía ánimos de nada y la mujer ya parecía que iba a tener allí al muchachito, aun cuatrimesino, me puse de pie y le hice una seña para que se sentara. Pues más me demoré en pararme que una señora en sentarse mientras se quejaba de la reuma y la artritis, al parecer muy comunes en las cuarentañeras de hoy en día.

Yo, atónito, mire a la mujer con mirada inquisitiva y le dije, sin que sonara grosero o alterado: "¡Señora, por favor, que me paré para darle el puesto a la mujer que está en embarazo". Ella, quejándose, como en su último hálito de vida, me respondió: "Es que estoy tan enferma". Yo no supe cómo proceder, pues no esperaba tal reacción.

Una mujer que estaba al lado, de unos 50 años, enmarcó sus cejas y se puso de pie. Le dijo a la embarazada que se sentara y se paró a mi lado con un típico: "¡Cómo le parece!".

Obviamente con el chisme desconcentramos a los pasajeros que dormían plácidamente, y todos abrieron los ojos como sapos estreñidos en la charca; pero aún así, ninguno cedió su puesto a la señora.

Este tipo de situaciones son pan de cada día y no solo en el metro, ni mucho menos solo en esta ciudad. A veces uno se monta a los buses con varios paquetes, o encartado con un par de bolsas, y el pensamiento de las demás personas es: "haber pagado taxi".

Al menos yo no me di cuenta en qué momento dejamos la cortesía y nos olvidamos de ayudar a los demás, aún en lo mínimo, que siempre resulta útil. Deberían resultar ilógicos este tipo de artículos donde se hace un llamado de atención a la gente por olvidarse de la cultura, del civismo, de la cordialidad. Habrá que considerar volver a enseñar en los colegios, universidades e iglesias, la empolvada “Urbanidad de Carreño”, que parecía tan anticuada.

Todos hemos acogido la cultura del "qué pesar, pero qué pereza", la de "¿por qué le tengo que ayudar yo si hay tantas personas acá?", y nos apoltronamos olvidando descaradamente el valor fundamental del servicio social, de la caridad, de la satisfacción de ayudar; pero que no se trate de nosotros en una situación que requiera ayuda, porque es OBLIGACIÓN de los demás socorrer.

La solidaridad es un tire y afloje que solo tira cuando la situación es de conveniencia propia. Es decir, la solidaridad es como cantar el happy birthday en una fiesta de cumpleaños: todos sabemos que es importante, pero pensamos que si los demás no lo hacen, no vamos a ser nosotros los únicos haciendo el oso.
