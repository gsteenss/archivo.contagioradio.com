Title: El mérito es para los campeones
Date: 2016-08-22 17:12
Category: Otra Mirada
Tags: Colombia olímpicos, Juegos Olímpicos Rio 2016, Medallas para Colombia
Slug: el-merito-es-para-los-campeones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/colombianos-con-medalla-olimpicos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

###### 21 Agos 2016

El desempeño de los atletas colombianos en los Juegos Olímpicos de Rio, que supera lo hasta ahora alcanzado en este tipo de competencias por una delegación nacional, ha permitido conocer varias de las historias que se convierten en ejemplos de lucha, disciplina y sacrificio para destacar en el deporte y vencer en muchos casos las adversidades.

La primera medalla de estos juegos llego gracias al empuje de **Oscar Figueroa**, pesista nacido en Zaragoza Antioquia hace 33 años, quien llegó a vivir muy joven junto a su familia a Cartago Valle, por la violencia del conflicto entre paramilitares y guerrilleros, y las pocas oportunidades laborales. Allí destacó por su fuerza física y contextura física que le ha permitido obtener títulos nacionales, panamericanos, centroamericanos y ahora Olímpicos.

Las sorpresas dentro de la delegación nacional se dieron por los lados del boxeo. Dos pugilistas una Caucana y un Antioqueño, **Ingrid Lorena Valencia** quien se colgó la medalla de bronce, cambio el trabajo en las minas de oro por los cuadriláteros para dar la batalla a la pobreza con la que llegó a vivir desplazada en una zona de invasión en el Jarillón del río Cauca en Cali.

La otra revelación olímpica provino del Urabá, **Yuberjen Martínez**, el hijo de un pastor cristiano que por la necesidad tuvo que trasladarse de su natal Turbo a Chigorodó y trabajar desde temprana edad en oficios como la venta de artesanías de caracol, mangos, lavando motos y arreglando bicicletas, en la construcción y en la industria bananera, antes de encontrar en el boxeo la persistencia y fuerza de voluntad que le valió la plata olímpica.

**Caterine Ibarguen**, ganadora del oro olímpico en salto triple, conquisto al país con su sonrisa y carisma, una luchadora que siendo niña quedó bajo el cuidado de su abuela Ayola Rivas, quién se encargo de sacarla a ella y a su hermano Luis Alberto adelante enviándola a estudiar al colegio San Francisco de Asis, institución en la que desarrollo su amor por el deporte por el que recibiría además la plata en Londres, y múltiple campeona de la Liga Diamante

Desde Jamundí, en el Valle del Cauca, **Yuri Alvear** siempre supo que el deporte podría ser una manera de apoyar a su familia para mejorar sus condiciones de vida. estudiante del Instituto Técnico Comercial de su municipio, inició su vida en el Judo, disciplina que la llevaría a ganar la presea de plata en Río, superando el bronce alcanzado hace cuatro años en los olímpicos de Londres.

En el mismo departamento, pero en la ciudad de Yumbo, **Luis Javier Mosquera** decidió seguir el ejemplo de sus hermanos, ambos levantadores de pesas. Sin embargo las cosas no fueron sencillas en su hogar desde el rompimiento de sus padres, situación por la que Luis Javier desde los 16 años se dedicó a la peluquería para aportar en su casa, oficio que combinaba con el duro entrenamiento. Gracias a todo el sacrificio hoy celebra, aún de manera inesperada, el obtener la medalla de bronce por la sanción impuesta al kirguiso Izzat Artykov.

En el cierre de la participación olímpica colombiana, el BMX guardaba aún dos medallas más para el país, dos más para Antioquia. Sin duda la más esperada desde antes del inicio de los juegos fue la de **Mariana Pajón**, campeona olímpica y primera en el ranking de la UCI en su deporte, una mujer que ha conquistado los títulos más importantes imponiéndose a los estereotipos de un deporte que en el país no tenía hasta su llegada mayores antecedentes para su género; y el bronce alcanzado por **Carlos Alberto Ramírez **quien a punta de tenacidad logró meterse en las finales y el el último suspiro alcanzar con su rueda a subirse en el tercer cajón del podio.
