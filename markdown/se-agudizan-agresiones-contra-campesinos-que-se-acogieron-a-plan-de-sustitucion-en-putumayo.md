Title: Se agudizan agresiones contra campesinos que se acogieron a plan de sustitución en Putumayo
Date: 2019-06-06 18:16
Author: CtgAdm
Category: DDHH, Nacional
Tags: Abuso de fuerza ESMAD, campesinos, ESMAD, Putumayo
Slug: se-agudizan-agresiones-contra-campesinos-que-se-acogieron-a-plan-de-sustitucion-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/ESMAD-Caquetá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

El pasado 3 de junio, **Deobaldo Cruz Montero**, presidente de la  Junta de Acción Comunal (JAC) de la vereda La Cumbre, en  **Puerto Asís, Putumayo perdió su ojo izquierdo al ser impactado en el rostro por un proyectil de gas lacrimógeno** disparado por el Escuadrón Móvil Antidisturbios (ESMAD), en medio de operativos para la erradicación violenta de los cultivos de uso ilícito en la región. Jóvenes campesinos también habrían sido detenidos y golpeados arbirtrariamente a manos de la Fuerza Pública.

[La denuncia, hecha por la Red de DD.HH del Putumayo, relata que campesinos dialogaban con personal de la Policía Nacional sobre el procedimiento de erradicación forzada que adelantarían los uniformados, **Deobaldo Cruz en calidad de presidente de la JAC** solicitaba al responsable de la unidad, el capitán Bonilla, que no los dejaran sin la manutención de sus familias. Sin provocación alguna, y tras la orden del capitán al mando, un miembro del ESMAD disparo con gas lacrimógeno, proyectil que impactó contra Deobaldo quien se encontraba a tan solo seis metros de distancia.]

El proyectil de gas hirió directamente al ojo izquierdo del campesino, posterior al suceso, se presentaron  disturbios entre el ESMAD y campesinos, pese a que Deobaldo fue llevado a un centro asistencial en Ecuador donde fue intervenido quirúrgicamente, el hombre perdió su ojo izquierdo. [(Le puede interesar: Esteban Mosquera, víctima del ESMAD en Cauca en el marco del paro estudiantil)2](https://archivo.contagioradio.com/esteban-mosquera/)

### "No defendemos la coca, defendemos los derechos del campesinado" 

Marco Rivadeneira, presidente de la Asociación Campesina de Puerto Asís señala que no se ha llegado a un acuerdo concertado con la comunidad y que **"el Estado únicamente erradica y deja a la deriva al campesinado"** resaltando la ausencia de proyectos productivos que prometió el Gobierno como parte del  Acuerdo de Paz.

"Si yo arranco eso de qué voy a vivir, busquemos otra alternativa, dialoguemos, pero el Gobierno hace caso omiso, viene a erradicar a la fuerza pero no propone alternativas", expresó Marco replicando la voz de las más de 2.500 familias que según la Fundación Ideas para la Paz se acogieron a la erradicación.

A su vez, empresas petroleras que hacen ejercicios de perforación en Puerto Asís tampoco han concertado con las familias campesinas y continúan explotando el territorio,  pues según Rivadeneira es precisamente en las zonas de erradicación forzosa donde se plantean las exploraciones por parte de las compañías, "debilitando la posición del campesino".

### Intimidación contra jóvenes campesinos 

Aquel mismo día, en horas de la tarde, mientras transitaban por el camino que conecta las veredas de La Cabaña y La Cumbre, tres jóvenes fueron detenidos por integrantes de la Policía y el ESMAD.** Uno de los jóvenes escapó mientras los otros fueron retenidos y llevados al campamento de los uniformados  donde fueron encapuchados y golpeados por cerca de media hora, según relata la denuncia.**

De igual forma, a los jóvenes les apuntaron con armas de fuego, amenazándoles de muerte sino respondían información sobre los campesinos que hacían parte de la protesta, la tortura se prolongó durante dos horas hasta que un teniente intervino, sin embargo nunca se presentó el capitán Bonilla. [(Lea también: ESMAD agrede a comunidades que se habían acogido a sustitución de cultivos de uso ilícito) ](https://archivo.contagioradio.com/esmad-agrede-a-comunidades-que-se-habian-acogido-a-sustitucion-de-cultivos-de-uso-ilicito/)

Durante el proceso también les fueron tomadas fotografías a los campesinos por lo que el comunicado alerta, existe la «posibilidad que juntos jóvenes puedan ser víctimas de algún tipo de montaje judicial», posteriormente los jóvenes fueron interrogados directamente por el capitán Bonilla, «mientras otro uniformado anotaba en un libro de contabilidad las respuestas», después de anotar sus nombres y documentos, los campesinos fueron liberados.

Los campesinos han pedido que se conforme una misión humanitaria para verificar lo denunciado y que los hechos sean investigados por la Fiscalía, Defensoría y Procuraduría. Asímismo, los días 4 y 5 de junio se prolongaron enfrentamientos entre ESMAD y campesinos; **pese a que no se han reportado más heridos, en el lugar de los hechos han sido hallados cartuchos de fusil y pistola.**

<iframe id="audio_36817961" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36817961_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
