Title: Camilo Torres en el Teatro "La Candelaria"
Date: 2015-07-08 16:34
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: camilo torres 50 años, obra de teatro, Patricia Ariza, teatro la candelaria
Slug: camilo-torres-en-el-teatro-la-candelaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/camilo-torres-teatro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Hernán Díaz ] 

##### <iframe src="http://www.ivoox.com/player_ek_4733605_2_1.html?data=lZyglZuUeY6ZmKiakpeJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLo087Qy8aPhdPd28aSlKiPqMrmxsjh0dfFb8XZjIqflKjFscrg0IqflJDSucbqwpDO0trJt9XVjMnSzpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Patricia Ariza, Directora de "Camilo"] 

###### [7 Jul 2015 ]

Una mezcla de teatro, danza, música en vivo, performance y video se integran en "Camilo", la nueva producción del tradicional teatro "La Candelaria" de Bogotá. Una obra que, lejos de ser un relato biográfico del sacerdote y sociólogo Camilo Torres Restrepo, es una exploración emotiva por algunos de los episodios que marcaron su vida, pensamiento y legado.

La piel del sacerdote se la pondrán 13 actores y actrices, entre experimentados y debutantes, que hacen parte del grupo de teatro del maestro Santiago García, en esta oportunidad bajo la tutela de la directora Patricia Ariza, en una construcción casi orgánica de momentos dramáticos recreados a partir del movimiento, toques de folclor y diálogos poéticos.

La compleja personalidad de Camilo Torres, bifurcada entre el cura y el guerrillero, sus decisiones y contradicciones, resulta atractiva de trasladar a las tablas, aún más cuando se presenta enriquecida por metáforas visuales en 13 versiones diferentes de un mismo ser humano, inspiración e influencia para jóvenes desde los años 60 hasta nuestros días.

Durante el proceso de investigación requerido para realizar el montaje de la obra, el equipo de trabajo acudió a documentos históricos entre los que se encontraron cartas, publicaciones escritas, conversaciones con personas cercanas al sacerdote, y material fílmico, tomandoles alrededor de un año en recopilar el material necesario para dar forma a la pieza teatral.

"El legado de camilo torres por un lado es la entrega total a lo que uno ama y en lo que uno cree, el hombre predicó la justicia social, el amor eficaz" una amor que "no basta con predicarlo sino que hay que salir a trabajar por un cambio social" asegura la directora de la obra que se estrena este 8 de Julio con funciones hasta el próximo 18 de Julio en el céntrico teatro bogotano.

Fundador de la sociologia en Colombia junto a Fals Borda, trabajó la investigación con acción participativa, otro de los grandes aportes que, de Camilo Torres resalta Ariza "no basta ser intelectual sino que accione, que entre en una relacion compleja con las personas".

"Camilo" resulta ser la primera de varias actividades culturales que se realizarán en el marco de la conmemoración de los 60 años de la muerte del sacerdote, ocurrida en su primera incursión al monte como guerrillero del ELN el 15 de febrero del año 1966, una apuesta cuyo mensaje según su directora "ayuda mucho a iluminar lo que esta pasando ahora y que de alguna manera desembocan en la necesidad de la paz con justicia social que es el anhelo que tenemos todas y todos en este momento" .

Las funciones están programadas de miércoles a sábado, 7:30 p. m.,  en el Teatro "La Candelaria", ubicado en la calle 12 n.° 2-59, Bogotá. El costo de las entradas es de 24.000 pesos.
