Title: Transportadores evalúan irse de nuevo a paro
Date: 2016-09-28 18:36
Category: Movilización, Nacional
Tags: paro camionero colombia 2016, paro transportadores colombia, paro transportes 2016
Slug: transportadores-evaluan-irse-de-nuevo-a-paro
Status: published

###### [Foto: Archivo Contagio Radio ] 

###### [28 Sept 2016] 

[Hace tres meses finalizó el paro que fue promovido por el gremio de los transportadores y que duró 47 días; hoy el sector plantea la posibilidad de cesar actividades por cuenta del **incumplimiento por parte del Gobierno frente a los 26 puntos que fueron acordados** y en los que se pactó que cada norma iba a ser consultada.]

[Los transportadores denuncian que el nuevo decreto aprobado acabará con su patrimonio pues plantea la abolición del uno a uno, la norma que permite que por cada vehículo de carga nuevo que entre al sector, salga otro. Para ellos el **Gobierno colombiano quiere beneficiar a multinacionales como Impala** con el aumento de la flota que actualmente es de 317 mil vehículos, que movilizan anualmente 600 millones de toneladas.]

[En lo pactado también se habían establecido mesas de interlocución en las que se evalúan las problemáticas del sector y que actualmente no están sesionando, pues para los transportadores **el que se haya aprobado una norma sin su consentimiento da cuenta que no hay garantías para concertar**.]

[El gremio asegura que necesita soluciones de fondo para el sector, pues desde las [[negociaciones con el Gobierno](https://archivo.contagioradio.com/gobierno-continuo-incumpliendole-a-los-transportadores/)] no han subido los fletes pero si ha habido un aumento en los peajes, **"en algunos casos del 200% cada 30 kilómetros"**, como asegura Jorge Salazar, secretario general de la Confederación Colombiana de Camioneros.]

![9fd166c2-8d26-41ef-924e-b2e280442e83](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/9fd166c2-8d26-41ef-924e-b2e280442e83-e1475102249722.jpg){.alignnone .size-full .wp-image-30022 width="500" height="889"}

<iframe id="audio_13111474" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13111474_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]][ ]
