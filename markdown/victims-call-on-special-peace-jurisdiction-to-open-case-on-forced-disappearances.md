Title: Victims call on Special Peace Jurisdiction to open case on forced disappearances
Date: 2019-11-14 08:09
Author: CtgAdm
Category: English
Tags: forced disappearances, Human Rights Everywhere, MOVICE, Special Peace Jurisdiction
Slug: victims-call-on-special-peace-jurisdiction-to-open-case-on-forced-disappearances
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Minga.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Photo: Asociación Minga] 

 

**Follow us on Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

[The organizations Human Rights Everywhere (HREV) and the National Movement of Victims of State Crimes (MOVICE) asked the Special Peace Jurisdiction (JEP) to revise the cases of at least 80,000 disappeared people during the armed conflict with the presentation of a new report this Thursday.]

[The report, “Cartagrophy of Disappearances,” denounces the impunity that has reigned in 99.5% of these cases. Although some cases that allude to victims of forced disappearances have already been opened in the transitional justice system, the organizations call on the JEP to open a case, specific to this crime. ]

[“There are other collective cases such as kidnapping and forced recruitment that include the case of forced disappearances, but because of the historical connotation this crime has had in this country, we think it is necessary to open a separate case,” said Érik Arellana, a journalist and co-author of the report. Her mother, a member of the M-19 urban guerrilla group, was disappeared in 1987.]

"We hope that the Jurisdiction will take a decision that respects the rights of the victims and the needs for justice," Arellana said.

### **More than 80,000 people disappeared**

"Until We Find Them," the latest report from the National Center on Historical Memory on forced disappearances, indicated that there are 82,998 people registered as disappeared during the armed conflict in Colombia, from 1970 to 2015. "Colombian families suffer the absence of their loved ones and the uncertainty that the lack of news or evidence of what happened to them provokes. Who took them? Why? Why so much indolence?" reads the report.

In 52% of these cases, paramilitaries were behind the crime. Some 10,360 cases are attributed to guerrilla rebels, 2,764 to post-demobilization groups and 2,484 to state agents. There is information about 42,471 of these cases. Arellana said that the magnitude of the crime is so large that the state has yet to consolidate precise statistics. Various institutions offer different numbers: the National Registry of Disappeared has found 28,755 victims, the Single Registry of Victims counts 47,762 and the Memory and Conflict Observatory states the number is 82,998. This reveals the lack of communication between organizations.

### "We want clarity for the motives behind the crimes"

For the journalist, justice in these cases goes beyond finding the culprits behind an individual one. "We want clarity for the motives behind the crimes and to know who are the macro responsible, which are the ones who have motivated and impulsed this practice in the extermination of Colombian society."

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
