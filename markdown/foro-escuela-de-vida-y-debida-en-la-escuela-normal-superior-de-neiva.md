Title: Foro “Escuela de Vida y Debida” en la Escuela Normal Superior de Neiva
Date: 2017-08-13 07:00
Category: Abilio, Opinion
Tags: acuerdo de paz, Neiva, Teologia
Slug: foro-escuela-de-vida-y-debida-en-la-escuela-normal-superior-de-neiva
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/“Escuela-de-Vida-y-Debida.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: José Alberto 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 24 Jul 2017 

[Al participar en el foro “Escuela de vida y debida” en Neiva, el 4 y 5 de agosto, pudimos sentir una simple y labrada construcción de amor. De ese que es arte,  que se aprende, que engendra, que como dice, Silvio Rodriguez, consigue encender lo  muerto. Las maestras y maestros del equipo que lo dinamizan, aman “la arcilla que está en sus manos”,  de la que  ellas y ellos se hacen, pero también con la que moldean la formación de maestras y maestros “con pertinencia rural, urbano marginal y para la población sorda... se construye desde la perspectiva de la pedagogía crítica y desde una concepción de currículo como el curso de la vida”, tal como la definen.]

Cada semestre el Programa de Formación Complementaria de la Normal Superior de Neiva, determina los proyectos de aula a desarrollar y sus resultados son compartidos en el Foro, al que concurren distintas instancias de la vida  académica de la ciudad. En  el Foro, los  y las estudiantes de cada uno de los semestres  en una puesta en escena, artística, dan cuenta del conocimiento construido. Luego presentan colectivamente la ponencia que  junto a sus profesores - pares académicos -  elaboran a partir del análisis de la problemática escogida y de la reconstrucción de las historias de vida de cada una y cada uno de los participantes. Los  temas: la  inclusión, la memoria de  los territorios y la corporalidad.

Son la inclusión, la tierra, los territorios y la corporalidad hechos tema en la elaboración teórica. Han  motivado  la movilización social para la exigencia de derechos  hasta alcanzar resultados tangibles que alimentan la esperanza. Por ejemplo, de la  inclusión de la población sorda que estudia en el Programa,   que al no contar con intérpretes levantó la movilización de los normalistas ante la Secretaría de Educación para exigir la asignación de éstos, hasta conseguirlo. Y quienes marcharon fueron estudiantes que pueden escuchar, mas no podían asistir a clase sin que sus compañeros sordos  estuvieran con ellos.

Es también la inclusión de estudiantes con  orientaciones sexuales diversas, que en la reconstrucción de las historias de vida, sintieron el  ambiente propicio para  compartir su orientación. Ante el machismo y la fuerza nociva de la religión alejada de la misericordia,  han sido capaces de generar escenarios de aceptación y respeto.

Es la pedagogía de la tierra, que desde  la reconstrucción de la memoria de los caminos,  se liga a las mujeres, a los hombres, a los animales que caminan para construirlos y que lo han andado de ida y de vuelta. Es también la de las plantas, árboles, frutos que desde  la orilla para dentro, lo reverdecen. Además es la memoria de la guerra, de la movilización de los desplazadores militares y paramilitares que llegaron hasta los caseríos para la masacre, la desaparición y el desplazamiento y que tuvieron que partir  por la fuerza de la represión.

Pero también la  memoria del combate y las acciones contra el derecho humanitario que en algún momento las insurgencias provocaron, en contra de los mandatos de sus propios manuales de ética revolucionaria. Y es  la memoria de las viejas tejedoras, indígenas, campesinas que se hacen presente en la Normal  a través de las y los herederos  que se especializan para formar a sus pueblos. Ellos y ellas  han defendido con éxito, por ejemplo,  la cuenca del río las Ceibas de la exploración petrolera ordenada por el gobierno central y que han denunciado la imposición de las semillas genéticamente modificadas por parte del ICA, Instituto Colombiano Agropecuario,  para la producción de arroz.

No es formación sobre la base de contenidos teóricos, abstractos. Es formación desde la vida de los sujetos que hacen parte del proceso pedagógico y para la construcción de relaciones sociales que se correspondan con la dignidad de las mujeres y los hombres.

En esta oportunidad, la Normal abrió las puertas al intercambio de saberes que  se inspiran, también  en las pedagogías críticas, emancipadoras,  de la esperanza. El profesor Stephen Haymes de la Universidad de San Vicente de Paul de Chicago, habló desde la pedagogía del Trabajo de Celestin Freinet; Catherine Bouley profesora en  Marcella, Francia, habló de la normalización del imaginario machista que reproducen los textos  infantiles, los tan aprendidos y reproducidos cuentos de  nuestro Rafael Pombo. Hablamos de la memoria de la tierra que pasa por la comprensión mestiza, indígena, afrodescendiente,  pero también por la comprensión y las prácticas impuestas por el modelo neoliberal que la convierte en objeto para la compra y la venta y que no se logró espantar de los acuerdos firmados entre el gobierno y las Farc-Ep en el teatro Colón de Bogotá.

Otro momento importante  que propició el Foro fue el encuentro con uno de los precursores  de la pedagogía crítica, el Canadiense-Estadounidense Peter McLaren que se dirigió al auditorio desde California, donde vive. Entablaron un diálogo de “tu a tu”, pues su pensamiento ha inspirado la formación de docentes por parte de la Normal, hasta el punto que uno de los bloques del campus lleva el nombre del pedagogo.

Valdría la pena que en la implementación del Acuerdo de paz,  del Teatro Colón, en lo concerniente a la educación, la Comisión de Implementación aprovechara el acumulado pedagógico de la Normal Superior para el desarrollo del “Plan Especial de Educación Rural”, sin miedo al pensamiento crítico que seguramente orientará el aporte de ésta Institución Educativa que con la fuerza del amor,  forma maestros y maestras para “tejer mundos posibles dignos, con educación digna para todas y todos” como lo expresaron en la presentación del Foro.

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)** 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio 
