Title: "Nosotros vamos a lograr que se pida perdón por parte de la Policía Nacional" Gustavo Trejos
Date: 2016-07-06 14:49
Category: Entrevistas, Judicial
Tags: “Trípido”, Asesinato de Diego Felipe Becerra
Slug: nosotros-vamos-a-lograr-que-se-pida-perdon-por-parte-de-la-policia-nacional-gustavo-trejos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/esferapública.org_.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: esferapublica.org] 

###### [6 Julio 2016 ]

Pese a que el juzgado 38 administrativo de Bogotá **condenó a la Nación, la Policía Nacional y al Ministerio de Defensa** por los daños causados a los padres de Diego Felipe Becerra, no le ordenó a ninguna de estas entidades dar unas disculpas públicas por cometer exceso de fuerza y abuso de autoridad en la muerte del grafitero de 16 años.

Este falló se da después de que el Juez confirme que en el asesinato de Diego Felipe Becerra hubo un[exceso de fuerza y abuso de autoridad](https://archivo.contagioradio.com/dilaciones-y-amenazas-caracterizan-el-proceso-por-el-asesinato-de-diego-felipe-becerra/), además de encontrar que **la escena del crimen fue alterada** y que posteriormente ante los medios de información, se desvío la investigación justificando el asesinato del joven por parte de la Fuerza Pública, sancionando a las instituciones con   la publicación del edicto de demanda en tres medios de información, la publicación en la página oficial de la Policía Nacional del falló durante un año y el pago de una sanción económica a la familia.

Condena que para la familia de Diego Becerra es **laxa y parcializada frente a la culpabilidad de las instituciones**  y que además, no tiene en cuenta las peticiones de la demanda en donde como medio de reparación se exigía un perdón público de las autoridades.

"**Mientras no existan sanciones verdaderas**, sanciones fuertes, los miembros de la policía van a seguir asesinando  civiles indefensos y van a seguir **abusando de la autoridad**, que es lo que acostumbran a hacer. Nosotros vamos a lograr que [se pida perdón y se pidan disculpas por parte de la Policía Nacional](https://archivo.contagioradio.com/policia-nacional-reconoce-que-altero-la-escena-del-crimen-de-diego-felipe-becerra/)"asevero Gustavo Trejos, padrastro de Diego Felipe Becerra.

En la actualidad, de  las 13 personas involucradas en este crimen, [8 de ellas lograron libertad por vencimiento](https://archivo.contagioradio.com/se-preparan-8-capturas-mas-por-el-crimen-de-diego-felipe-becerra/) de términos, [tres de ellas fueron condenadas](https://archivo.contagioradio.com/un-coronel-y-dos-subtenientes-seguiran-detenidos-en-caso-diego-felipe-becerra/) y uno de ellos le revocaron la medida de aseguramiento y esta en libertad. Por ahora, la familia seguirá en el proceso de apelación de sentencia en instancias locales.

<iframe src="http://co.ivoox.com/es/player_ej_12140340_2_1.html?data=kpeelpWXeJGhhpywj5WcaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5yncajp1NnO2NSPmNPZy9Tgh5enb9HVxdfSjanNqcjjjKfSxcrWtsKhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
