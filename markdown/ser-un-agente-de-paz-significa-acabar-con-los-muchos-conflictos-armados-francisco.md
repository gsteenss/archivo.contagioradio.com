Title: Ser un agente de paz significa acabar con los muchos conflictos armados"  Francisco
Date: 2015-09-24 15:20
Category: El mundo, Política
Tags: Congreso de Estados Unidos, Crisis ambiental, enmigrantes, Papa Francisco, tráfico de armas
Slug: ser-un-agente-de-paz-significa-acabar-con-los-muchos-conflictos-armados-francisco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/papaapapa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: surenio.com.ar]

###### [24 sep 2015]

Hoy el  Papa Francisco en su discurso ante el Congreso de Estados Unidos trató temas polémicos como la inmigración, el tráfico de armas y el cambio climático.

La  situación de los emigrantes y refugiados  es una de las problemáticas que  causan mayor preocupación al Sumo Pontífice, "...en este continente, las miles de personas que se ven obligadas a viajar hacia el norte en búsqueda de una vida mejor para sí y para sus seres queridos, en un anhelo de vida con mayores oportunidades. ¿Acaso no es lo que nosotros queremos para nuestros hijos?" .

 Respecto al cambio climático el Papa afirmó que esta lucha debe ser implementada como cultura ciudadana, para él en la actividad empresarial "la creación de puestos de trabajo es parte ineludible de su servicio al bien común”, sin embargo esa actividad debe “entrar en diálogo con todos acerca de nuestra casa común”

Agrega "que el desafío ambiental que vivimos, y sus raíces humanas, nos interesan y nos impactan a todos” y llama la atención de los grupos empresariales de EEUU para que amplíen el diálogo en el marco del respeto a los seres humanos y a la naturaleza.

Respecto al armamentismo Francisco afirma que “ser un agente de diálogo y de paz significa estar verdaderamente determinado a atenuar y, en último término, a acabar con los muchos conflictos armados que afligen nuestro mundo. Y sobre esto hemos de ponernos un interrogante: ¿Por qué las armas letales son vendidas a aquellos que pretenden infligir un sufrimiento indecible sobre los individuos y la sociedad? Tristemente, la respuesta, que todos conocemos, es simplemente por dinero; un dinero impregnado de sangre, y muchas veces de sangre inocente. Frente al silencio vergonzoso y cómplice, es nuestro deber afrontar el problema y acabar con el tráfico de armas”.
