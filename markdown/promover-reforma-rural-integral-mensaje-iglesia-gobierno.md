Title: Reforma rural integral, no aspersión aérea: El mensaje de la iglesia al Gobierno
Date: 2020-12-23 17:06
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Aspersión Aérea, iglesia católica, reforma rural integral, Sustitución de cultivos de uso ilícito
Slug: promover-reforma-rural-integral-mensaje-iglesia-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/EWa2UYSX0AE_Of1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Luis José Rueda arzobispo de la iglesia católica / cec.org.co

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A través de una reciente entrevista con el periodista Yamid Amat para El Tiempo, monseñor Luis José Rueda, arzobispo de Bogotá analizó la situación de violencia que vive el país y resaltó la importancia de promover la reforma rural integral y el desarrollo agrario del país en lugar de acudir a otras instancias como la fumigación aérea con glifosato, alternativas que ha sido demostrado tiene un efecto negativo sobre la salud de las comunidades, la sus cultivos y la flora y fauna de la región.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Frente a la postura de funcionarios y en general del Gobierno incluidos al ministro de Defensa, Carlos Holmes Trujillo y el defensor del Pueblo, Carlos Camargo quienes atribuyen la violencia al narcotráfico, monseñor expresó que la muerte de líderes sociales en el país "es una lamentable consecuencia de la combinación de fuerzas fratricidas" que priorizan la ética corrupta, la indiferencia social y la política del odio sobre la vida. [(Lea también: "La iglesia no puede guardar silencio frente a la situación del país")](https://archivo.contagioradio.com/la-iglesia-no-puede-guardar-silencio-frente-a-la-situacion-del-pais/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, señaló que no ve la fumigación con glifosato como la propuesta más adecuada para combatir al narcotráfico, "tenemos caminos con fundamento humano, social, ecológico y espiritual que son mejores que una rápida fumigación con glifosato". Agregó que se ha constatado en el pasado cómo la "fumigación no construye escenarios sociales sostenibles en las regiones que sufren este flagelo".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio del aislamiento preventivo durante los meses de abril y mayo, los obispos de Cauca y Nariño hicieron un nuevo llamado a las estructuras armadas que promueven el narcotráfico para que cesen su accionar violento y den prioridad a la vida de las comunidades en medio de la crisis humanitaria que vive el país. [(Le puede interesar: Comunidades del Pacífico exigen a Gobierno y grupos armados una inmediata salida negociada al conflicto)](https://archivo.contagioradio.com/comunidades-del-pacifico-exigen-a-gobierno-y-grupos-armados-una-inmediata-salida-negociada-al-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

De igual forma, otros representantes de la iglesia, como **Carlos Alberto Correa, vicario apostólico de Guapi, Cauca han expresado** con anterioridad que el Estado tiene la responsabilidad de atender las necesidades de las regiones que requieren empleo, la educación y el desarrollo social de los municipios. Algo que sugiere que más allá de los grupos que se dedican al narcotráfico es necesaria una presencia firme de las instituciones en los territorios.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La iglesia señala que la respuesta es promover un pacto humanitario

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Frente al creciente número de líderes sociales asesinados y que según organizaciones como el [Instituto de Estudios para el Desarrollo y la Paz (INDEPAZ)](http://www.indepaz.org.co/lideres/) se elevó a 300 personas, señaló que este es un símbolo de que "hay un grito desde las regiones que clama por la reconciliación social, lo cual requiere el empeño de todos los sectores sociales". [(Lea también: Obispos de Cauca y Nariño piden parar el flagelo del narcotráfico)](https://archivo.contagioradio.com/obispos-de-cauca-y-narino-piden-parar-el-flagelo-del-narcotrafico/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El arzobispo de Bogotá expresó que todos los sectores del país deben acoger un pacto humanitario **por la vida y la resolución dialogada de los conflicto**s y comprometerse en particular con las zonas rurales donde la atención al campo puede generar "empleo digno y una economía promotora de fraternidad y paz". [(Lea también: Hay una ruptura entre el poder y la autoridad: Dario Monsalve)](https://archivo.contagioradio.com/hay-una-ruptura-entre-el-poder-y-la-autoridad-dario-monsalve/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde septiembre de este año, al menos 140 organizaciones sociales, étnicas, y eclesiásticas del Chocó, Valle del Cauca, Cauca, Nariño y Putumayo se unieron para hacer un llamado a la sociedad colombiana a suscribir este Pacto por la Vida y la Paz como respuesta a las masacres, los asesinatos a líderes sociales y excombatientes, los desplazamientos forzados, y el abuso de la Fuerza Pública tanto en territorios rurales como urbanos. [(Lea también: Organizaciones sociales convocaron a un Pacto por la Vida y la Paz)](https://archivo.contagioradio.com/organizaciones-sociales-convocaron-a-un-pacto-por-la-vida-y-la-paz/)

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
