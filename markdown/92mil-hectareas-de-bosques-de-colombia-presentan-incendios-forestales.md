Title: 92mil hectáreas de bosques de Colombia presentan incendios forestales
Date: 2015-10-01 16:05
Category: Ambiente, Nacional
Tags: autoridades ambientales, bolivar, Cauca, Cerro negro, Conpaz, Defensoría del Pueblo, desabastecimiento de agua, Fenómenos de El Niño, Guachicono, incendios forestales Colombia, Río Mazamorras, Sucre
Slug: 92mil-hectareas-de-bosques-de-colombia-presentan-incendios-forestales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/DSC01282.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [cvcambiental.blogspot.com]

<iframe src="http://www.ivoox.com/player_ek_8732167_2_1.html?data=mZyglJaae46ZmKiak5WJd6KlmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRfZPhytGYysrHuIa3lIquk9fJpdSfxcqYxNTXtdbZ1JDRx5Cns83jzsfWw5DUtsbnxtPhw9OPrc%2FXxtOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Leidy Correa, CONPAZ] 

El fenómeno de **“El Niño”, ha generado que más de 92 mil hectáreas de bosques de Colombia** se vean afectadas por incendios forestales, entre ellas, **380 pertenecen al Cerro Negro, en el departamento del Cauca,** donde las autoridades no han atendido la situación pese al llamado de la comunidad.

De acuerdo con Leidy Correa, comunicadora de la Red CONPAZ y habitante del municipio de Sucre, en Cauca,  el Cerro Negro que está ubicado al oriente del municipio es el guardián de la comunidad, y ya **cumple 20 días cubierto en llamas sin que las autoridades departamentales, nacional, el ejército o la policía ayuden a apagar el incendio.** Frente a esa situación, mil personas de las comunidades cercanas son quienes han debido ir al cerro para intentar apagar el fuego.

Allí nacen las quebrada Morales y Milagrosa que constituyen el **Río Mazamorras,** del cual se abastecen varios pueblos. Sucre, ha sido el primero en verse afectado por esta situación, por lo que ahora **los habitantes deben suspender el uso de agua durante tres horas diarias, como medida de ahorro.**

Según la integrante de la Red CONPAZ, esta situación afecta a los municipios de Guachicono y  Bolívar que también se abastecen de agua por parte del rio Mazamorras. Además, en otras zonas del Cauca como **La Vega y Almaguer también hay incendios que no han atendido las autoridades.**

Cabe recordar, que la Defensoría del Pueblo aseguró que el progresivo deterioro de las cuencas hidrográficas; la desecación de ciénagas, pantanos y humedales; la ampliación de la frontera agrícola; las malas prácticas en la explotación de los recursos naturales; las altas tasas de deforestación y el incremento en el número de incendios forestales, ha producido que más de **312 municipios presenten riesgo de desabastecimiento de agua.**

##### [![6517529493\_c7e39c6a8c\_b-1024x768](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/6517529493_c7e39c6a8c_b-1024x768.jpg)
