Title: Piden a la ANLA publicar estudio de impacto ambiental en Páramo de Santurbán
Date: 2017-09-06 13:49
Category: Ambiente, Nacional
Tags: ANLA, empresa minesa, mineria a gran escala, Páramo de Santurbán, Santander
Slug: defensa-del-paramo-de-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/PARAMO-DE-SANTURBAN-e1461876510933.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Ola Política] 

###### [06 Sept 2017] 

El comité que defiende el Páramo de Santurbán ubicado en los departamentos de Santander y Norte de Santander, le ha pedido a la Agencia Nacional de Licencias Ambientales **que dé a conocer el estudio sobre el impacto ambiental** que va a tener el proyecto minero de la empresa árabe Minesa en ese territorio. Informaron que llevan 20 día pidiendo los resultados del estudio y no se los han entregado.

Según Dadán Amaya, miembro del Comité en Defensa del Páramo de Santurbán, “la empresa Minesa pretende realizar un proyecto de minería de oro a gran escala donde **la construcción de los túneles que se necesitan van a afectar las fuentes hídricas** que alimentan el acueducto metropolitano como los son el Río Suratá, el Río de oro y Río Frío”. (Le puede interesar: ["Ambientalistas exigen al Banco Mundial no financiar proyecto en Santurbán"](https://archivo.contagioradio.com/ambientalistas-exigen-al-banco-mundial-no-financiar-proyecto-santurban/))

Igualmente, indicó que **puede haber complicaciones adicionales para las comunidades y el territorio** como por ejemplo la contaminación del ecosistema con  aguas ácidas producto de la extracción de minerales y los riesgos de derrumbes en una zona que es inestable y que podría colapsar a causa de la remoción de rocas y materiales que componen el suelo.

El ambientalista dijo que **es preocupante que se tenga que dar el debate de la condición fundamental del agua para la vida de las comunidades,** “no puede haber una ciudad donde el suministro de agua se vea comprometido”. Hizo alusión a un estudio que realizó Green Peace sobre las afectaciones que deja la minería de carbón sobre las aguas del páramo de Pisba, donde las aguas ácidas han contaminado los ecosistemas.

Esto mismo podría suceder en los territorios santandereanos donde “se ha comprobado que **más de la mitad de** **cada tonelada de materiales que se sacan de la tierra puede ser azufre** y si se mezcla el agua y el aire se produce agua un millón de veces más ácida que la apta para el consumo humano”. (Le puede interesar: ["Minería en páramo de Santurbán no contó con estudios sobre impacto socio-ambientales"](https://archivo.contagioradio.com/mineria-en-paramo-de-santurban-no-conto-con-estudios-sobre-impactos-socio-ambientales/))

Los ambientalistas y las comunidades que habitan los santanderes han repetido en varias ocasiones los problemas que se desligan de los proyectos mineros a gran escala. Sin embargo, Amaya indicó que “**el Gobierno Nacional ha hecho declaraciones falsas diciendo que es minería responsable** y ha entregado licencias sin conocer los estudios ambientales”.

Igualmente, dijo que las alcaldías de la provincia de Soto Norte, donde se realizaría el proyecto minero, **“se han jugado su suerte con el proyecto minero,** pero hace unos meses se conoció la séptima cláusula de divulgación positiva del convenio que tienen los alcaldes con Minesa en donde las autoridades no pueden expresar ninguna opinión que afecte el nombre de la empresa o del proyecto”.

Finalmente, Amaya manifestó que no es correcto que los alcaldes “estén amordazados y que no se escuchen a las comunidades”. Dijo que la Universidad Industrial de Santander y el alcalde de Bucaramanga han expresado su apoyo en contra de la realización del proyecto e invitó a las comunidades para que participen en la **marcha del 6 de octubre para darle “el debate a Minesa** y al Gobierno Nacional con los argumentos técnicos necesarios”.

<iframe id="audio_20730736" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20730736_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
