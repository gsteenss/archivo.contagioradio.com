Title: Estado debe pedir perdón por el caso de Rosa Elvira Cely
Date: 2016-10-28 15:55
Category: Mujer, Nacional
Tags: genero, Ley Rosa Elvira Cely, mujeres, paz, Rosa Elvira Cely, violencia
Slug: estado-debe-pedir-perdon-por-el-caso-de-rosa-elvira-cely
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/rosacely-110506-10-de-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Oct de 2016] 

Ante el Juez 37 Administrativo de Bogotá, **se realizó la primera audiencia por la demanda que fue presentada en el caso de Rosa Elvira Cely**. Diversas organizaciones sociales han estado realizando un acompañamiento en solidaridad con la familia de esta mujer, que fue víctima de feminicidio hace 4 años.

Para Contagio Radio, Adriana Cely, hermana de Rosa Elvira y quien ha abanderado la lucha en el caso de su hermana, aseguró que **estos 4 años han sido épocas en las que han logrado fortalecerse como familia y trabajar por el perdón.** Pero también manifiesta que han sido años para “fortalecerse y poder entender que esta lucha y reparación es para todas las mujeres, no solo de la familia de Rosa Elvira, pues no ha sido la única mujer que ha sido violentada en nuestro país”.

Según lo ha denunciado la familia de Rosa Elvira, **existieron fallas enormes que, de no haberse dado, hubieran garantizado la atención a tiempo de Cely** “evidenciamos la falla en la línea de emergencia 123, de la policía, de salud – porque no fue atendida en los momentos requeridos -. También de la Fiscalía, porque este criminal que le hizo esto a mí hermana, ya había cometido unos actos por los cuales ya llevaba un proceso y estaba en la calle” afirmó Adriana.

Frente a este proceso que comienzan apenas a andar, Adriana Cely en representación de su familia, manifiesta que esperan que se haga justicia **“que el estado o las entidades acepten que tuvieron errores, que hubo negligencia”** asevera.

Además agrega Adriana,  **el estado debe “salir a pedir perdón por tantas víctimas que han muerto por culpa de no garantizar esas instancias y los derechos que tenemos”** y concluye diciendo que “lo más importante es lograr que Rosa Elvira Cely sea excluida de culpa por parte del juez en su caso”. Le puede interesar: [Rosa Elvira Cely: 6 veces victimizada](https://archivo.contagioradio.com/rosa-elvira-cely-6-veces-victimizada/)

<iframe id="audio_13522494" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13522494_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
