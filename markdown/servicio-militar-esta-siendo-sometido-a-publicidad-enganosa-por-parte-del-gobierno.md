Title: Servicio militar está siendo sometido a “publicidad engañosa” por el gobierno
Date: 2016-04-22 13:45
Category: DDHH, Entrevistas
Tags: militares, reclutamiento, servicio militar
Slug: servicio-militar-esta-siendo-sometido-a-publicidad-enganosa-por-parte-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/servicio-militar-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: radioipiales] 

###### [22 Abril 2016]

El gobierno ha dicho que está buscando eliminar el servicio militar obligatorio y la libreta militar como requisito para muchos trámites, sin embargo, **ha presentado al congreso dos proyectos que están en contravia de esa idea**, e incluso, ha hecho lobby para que el proyecto de Angélica Lozano, del Partido Verde que [sí elimina el pago del servicio militar](https://archivo.contagioradio.com/?s=servicio+militar), pierda el respaldo que había conseguido.

Uno de los proyectos presentado contempla que los jóvenes que logren conseguir un trabajo pero que no tengan la libreta militar, hagan un **acuerdo con el ministerio de defensa para que el pago de la libreta se difiera a 18 meses de plazo**. En la actualidad la libreta militar puede pagarse pero de contado.

El otro proyecto del gobierno plantea aumentar el tiempo del servicio de 12 a 18 meses, y además pone la libreta militar como requisito, no solamente para la vinculación formal laboral sino también para el trámite del pasaporte y la licencia de conducción. Según Angélica Lozano **estas propuestas van en dirección opuesta de lo que se dice y se hace públicamente por parte del gobierno.**

El gobierno está haciendo "publicidad engañosa", plantea Lozano, puesto que los proyectos de ley presentados por el gobierno aumentan y ponen requisitos para la libreta militar a pesar de que **el presidente se comprometió a [eliminar el servicio militar](https://archivo.contagioradio.com/jovenes-aportan-a-propuesta-de-servicio-social-para-la-paz/)** obligatorio, además la mayoría de los jóvenes que prestan el servicio militar son de estratos bajos que no tienen el dinero para pagar la libreta.

El proyecto de ley presentado por Angélica Lozano, por el contrario, **contempla que la [libreta militar sea gratuita](https://archivo.contagioradio.com/70-de-jovenes-reclutados-para-servicio-militar-son-de-estratos-1-y-2/)** y se consiguió en principio el respaldo de 80 congresistas, sin embargo el **vice ministro de defensa y 3 militares de alto nivel lograron voltear el apoyo hacia el proyecto de gobierno que cobra la libreta militar y aumenta el periodo** en que los jóvenes estarían en las filas de las FFMM o de policía.

Lozano, afirma que el gobierno está manejando un doble discurso, en el cual [se está engañando a los jóvenes más pobres](https://archivo.contagioradio.com/por-el-derecho-a-decir-no-al-servicio-militar-obligatorio/), es decir, una realidad de desigualdad. “uno no comprende que en momentos de un proceso de paz complejo (…) pero esperanzador en medio de la dificultad, **se promueva ampliar, profundizar el servicio militar obligatorio”.**

<iframe src="http://co.ivoox.com/es/player_ej_11269072_2_1.html?data=kpafmJ6Ue5Ohhpywj5WdaZS1k52ah5yncZOhhpywj5WRaZi3jpWah5yncaLiyIqwlYqlfc3dxMaYrtTepc%2FjjpCuzs7FstvVjLvS1MnJb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
