Title: EE.UU. pone a Colombia en la lista de vigilancia prioritaria por piratería, y de paso viola nuestra soberanía
Date: 2018-05-21 08:50
Category: Mision Salud, Opinion
Tags: colombia, EEUU, OCDE, piratería
Slug: ee-uu-colombia-lista-pirateria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/Dc2GQ4RW4Agc-L2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Misión salud 

###### 21 May 2018 

#### Por **[Misión Salud](https://archivo.contagioradio.com/opinion/mision-salud/)** 

*La Oficina de Comercio del Gobierno de EE.UU. (USTR) ha tenido a Colombia en su “lista negra de países piratas” desde hace años, pero ahora estará en la “lista prioritaria” porque, dice, no ha mostrado progresos significativos para implementar las obligaciones adquiridas en acuerdos comerciales en materia de propiedad intelectual.*

La lista negra de países piratas es un mecanismo de EE.UU., sin sustento en el marco jurídico internacional, usado para presionar a los países con los que tiene acuerdos comerciales vigentes. La lista se publica anualmente en lo que se denomina el Informe Especial 301 y en ella aparecen los países que, según la USTR, están incumpliendo los estándares de protección de la propiedad intelectual.

Organizaciones de la sociedad civil de Colombia hemos seguido este tema y hemos denunciado la ilegitimidad del mismo por años. Sorprende no solo que el mecanismo continúe, sino que, además, para Colombia aumentó a nivel prioritario. Desde nuestro punto de vista es una amenaza que Colombia debe ignorar ya que es un instrumento de matoneo que atiende esencialmente los intereses de las industrias de ese país.

De hecho, Chile, que se encuentra en esta lista, desde 2013 sostiene que es una herramienta ilegítima. Incluso se percibe esta clasificación como un ataque directo contra el Gobierno Santos, que, junto con las presiones que están ocurriendo a propósito del ingreso a la OCDE, se junta también con las informaciones recientemente divulgadas por la DEA sobre el caso Santrich, bombardeando el proceso de paz.

¿Por qué Colombia sigue siendo parte de esta lista?

Supuestamente, se justifica la inclusión de Colombia en esta lista porque:

● El país no ha implementado los compromisos que adquirió en el TLC firmado con EE.UU. con relación a una supuesta piratería en internet (especialmente a través de celulares y en páginas web hospedadas en el país, y en espacios físicos como los San Andresitos y las zonas francas).

● En el área de acceso a medicamentos, el país atentaría contra el sistema de innovación y de propiedad intelectual con el artículo 72 del Plan Nacional de Desarrollo. La verdad es que de lo que se trata es de complementar el procedimiento de registro sanitario de nuevos medicamentos ante el INVIMA, con la fijación de un precio con base en la evaluación hecha por parte del Instituto de Evaluación Tecnológica en Salud, con miras a proteger la sostenibilidad del sistema de salud.

¿Qué significa estar en la lista 301?

Colombia lleva años en esta lista negra, la novedad este año es que pasó de estar en una lista de países “bajo observación” a aumentar la “lista prioritaria” de países piratas. El obtener esta clasificación significa que EE.UU. quiere presionar más para lograr medidas que beneficien a sus industrias. La calificación no parece nada extraordinario, también son parte de este grupo China e India en Asia, Argentina, Chile y Venezuela en América Latina, e incluso un país desarrollado como Canadá.

Los acuerdos comerciales internacionales están convirtiéndose en importantes barreras para el desarrollo de políticas nacionales que combatan la desigualdad, no son neutrales y se discuten priorizando las agendas internacionales y no con base en las necesidades locales. Aún más, la experiencia en curso del proceso de ingreso de Colombia a la OCDE refleja cómo los países que desean entrar al “club de buenas prácticas” deben incurrir en malas prácticas, como reducir el acceso a medicamentos o pasar leyes de derecho de autor con procedimientos expeditos evitando un apropiado debate público, presionados de manera indebida por países miembros del “club”, concretamente Estados Unidos.

Colombia debe afirmar la ilegalidad del Informe Especial 301 y reafirmar como legítimas las acciones del país que han motivado su inclusión en el mismo, porque la construcción de leyes debe incluir un proceso amplio y público de participación ciudadana, y porque las acciones de interés público en materia de medicamentos cumplen el marco jurídico internacional y el ordenamiento jurídico interno, Es decir, el actuar del país debe verse simplemente como el ejercicio principio de soberanía nacional.

###### \*\*\* 

###### Documentos relacionados:  
● [Informe Especial 301/18 en inglés](https://ustr.gov/sites/default/files/files/Press/Reports/2018%20Special%20301.pdf).  
● Comentarios de sociedad civil sobre el informe 301 en [2011](https://www.google.com/url?q=http://karisma.org.co/%3Fp%3D611&ust=1526995200000000&usg=AFQjCNFOQc1NIHX0_3FAsMX5VGUNDz5huQ&hl=en&source=gmail), [2013](https://karisma.org.co/una-vez-mas-solicitamos-que-colombia-sea-retirada-del-informe-especial-301/), [2014](https://karisma.org.co/wp-content/uploads/2014/04/Radicado-Colombia.pdf), [2015](https://karisma.org.co/descargar/comentarios-al-informe-301-2015/), [2016](https://karisma.org.co/descargar/comentarios-al-informe-301-2015/)  
y [2017](https://karisma.org.co/descargar/comentarios-301-del-ano-2017/). 

###### Organizaciones firmantes  
Fundación Karisma (Colombia) -- diego@karisma.org.co  
Misión Salud (Colombia) -- comunicaciones@mision-salud.org  
Fundación Ifarma (Colombia) -- esanchez@ifarma.org 
