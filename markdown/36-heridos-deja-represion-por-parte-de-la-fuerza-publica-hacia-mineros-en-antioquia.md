Title: 36 heridos deja represión por parte de la fuerza pública hacia mineros en Antioquia
Date: 2016-09-26 14:26
Category: Movilización, Nacional
Tags: Gobierno, Mineria, movilización paro
Slug: 36-heridos-deja-represion-por-parte-de-la-fuerza-publica-hacia-mineros-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/minería-e1474917475262.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Christian Escobar M. 

###### [26 Sep 2016] 

La represión contra los mineros ancestrales que mantienen paro indefinido desde la semana pasada, ya deja los primeros heridos por cuenta de las acciones represivas por parte de la fuerza pública. El sábado en Segovia, agentes del ESMAD arremetieron contra los mineros dejando **36 personas heridas**, según denunció Conalminercol.

De acuerdo con Rubén Darío Gómez, secretario general de la Confederación Nacional de Mineros de Colombia, Conalminercol, al lugar conocido como la Electrificadora, donde se encontraban concentrados miles de mineros, llegaron **cerca de 60 integrantes del ESMAD para intentar desbloquear la vía, con gases lacrimógenos y golpeando a quienes se encontraban sobre la carretera.**

“Hubo gente lesionada, se recibieron disparos, eso se convirtió en una batalla campal cuando era una protesta pacífica”, dice el secretario de  Conalminercol, quien agrega que la inactividad en el transporte y el comercio, no se debe al taponamiento de vías, sino a que los **transportadores y el comercio también se unieron al paro** en protesta contra el abandono estatal de esta región del país.

Tras el incidente del sábado, ese mismo día se anunció que se llevará a cabo este martes sobre las 9 de la mañana, una reunión entre la mesa minera y el gobierno departamental para buscar alternativas para los mineros informales, que exigen que se derogue el **Decreto 1421 de 2016 con el cual se acaba con la minería artesanal y se dan beneficios para las multinacionales.**

Lo anterior teniendo en cuenta que según Rubén Darío Gómez, el gobierno ha recibido órdenes en materia legal, por parte de la Corte Constitucional y del Consejo de Estado, donde **se le exige al gobierno reformar o expedir un nuevo Código de Minas donde se incluya la mediana y pequeña minería,** reconociendo la ancestralidad esa actividad que se realiza en el 80% los municipios del país.

<iframe id="audio_13067667" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13067667_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
