Title: Fuero Penal Militar es un atentado
Date: 2014-12-26 15:58
Author: CtgAdm
Category: Otra Mirada
Slug: fuero-penal-militar-es-un-atentado
Status: published

Fuero Penal Militar es un atentado contra el Derecho Internacional Humanitario
==============================================================================

[Audio](http://ctgaudio.megadatesystem.com/upload/0/fsc7h4Pw.mp3)  
En la Cámara de Representantes se llevó a cabo este viernes 31 de octubre una audiencia de control político sobre los peligros que representan para los derechos de las víctimas de violencia sexual la ampliación del fuero penal militar, proyecto promovido por el ministerio de Defensa, que fue aprobado en segunda instancia esta misma semana.  
El proyecto de ley se hundió en el año 2013, porque la Corte constitucional encontró vicios de procedimiento. Sin embargo, para la Representante Ángela María Robledo, también existen vicios de contenido, en tanto permite el  no reconocimiento y la impunidad en los casos de violencia sexual y los llamados "falsos positivos" por parte de las fuerzas militares, representando un atentado contra el Derecho Internacional Humanitario al dejar en competencia de investigación de la justicia penal militar los crímenes en persona protegida.
