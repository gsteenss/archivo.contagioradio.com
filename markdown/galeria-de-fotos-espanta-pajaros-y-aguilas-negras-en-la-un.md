Title: Galería de Fotos: Espanta Pájaros y Aguilas Negras en la UN
Date: 2015-06-04 22:31
Category: Educación, Fotografia
Tags: acue, Aguilas Negras, estudiantes, fue, itayosara rojas, leopoldo manera, mane, mario hernandez, Universidad Nacional
Slug: galeria-de-fotos-espanta-pajaros-y-aguilas-negras-en-la-un
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/MG_8781-copy.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [4 Jun 2014] 

En la Universidad Nacional se construyeron espantapájaros, en una jornada simbólica de protección, como respuesta a las reiteradas amenazas de muerte contra profesores y estudiantes.

\

###### Fotografías de Carmela María 

 
