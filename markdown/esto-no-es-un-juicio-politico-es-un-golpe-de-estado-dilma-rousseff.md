Title: Esto no es un juicio político “es un golpe de Estado” Dilma Rousseff
Date: 2016-05-13 07:56
Category: El mundo, Política
Tags: Brasil, Dilma Rousseff, Impeachment
Slug: esto-no-es-un-juicio-politico-es-un-golpe-de-estado-dilma-rousseff
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/dilma-5-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elcomercio.pe] 

###### [12 May 2016] 

A la salida del palacio de gobierno en Brasilia, en el último discurso antes de separarse de su cargo, luego de la decisión tomada por el congreso de ese país en que se ordena el inicio del juicio político, la mandataria afirmó que no cometió ningún delito y que **lo que está haciendo la clase política es un golpe de Estado contra las ganancias de la clase trabajadora** y convocó a la [movilización constante](https://archivo.contagioradio.com/protestas-contra-golpe-de-estado-en-brasil-paralizan-vias-en-7-estados/) durante el tiempo de duración del juicio.

Rousseff también reiteró que se defenderá con todos los mecanismos legales existentes y que demostrará que no es culpable de los delitos de responsabilidad que se le pretenden imputar. En otro discurso reciente aseguró que le hacen el **juicio político porque nunca podrán llegar al poder por la vía electoral.**

Por su parte Fania Rodríguez, editora de Brasil de Fato, insistió en que el gobierno que instalará el vicepresidente Michelle Temer será absolutamente neoliberal “es hombre, blanco y viejo” lo cual es un indicador de las políticas conservadoras y retroceso para las conquistas alcanzadas. Según Rodríguez, **el gobierno de Temer es ilegítimo y por ello la movilización desde diversos sectores sociales se va a mantener.**

En su análisis Fania afirma también que algunos importantes cargos de gobierno que estaban siendo ocupados por mujeres pasarán a los hombres lo cual también es un golpe simbólico al papel de **Rouseff que es la primera mujer electa como presidenta en la historia de Brasil**. Otra de las preocupaciones gira en torno a que Temer anunció la creación de una secretaría de seguridad que estará al mando de un general.

Además se evidencia una crisis política puesto que por lo menos el **60% de los congresistas que aprobaron el impeachment están siendo investigados por corrupción**, acusaciones que son de extrema gravedad si se comparan con las acusaciones infundadas que pesan sobre Rouseff.

<iframe src="http://co.ivoox.com/es/player_ej_11510031_2_1.html?data=kpaik5WUd5Khhpywj5eaaZS1k5eah5yncZOhhpywj5WRaZi3jpWah5yncafVz87OjbfTqNPdyNrS1ZCRb6bYytnc1MaPhtPV1M7ZjcnJb6fV1dSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
