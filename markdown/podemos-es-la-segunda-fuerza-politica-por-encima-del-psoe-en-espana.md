Title: Podemos es la segunda fuerza política por encima del PSOE en España
Date: 2015-02-04 19:52
Author: CtgAdm
Category: El mundo, Política
Tags: Encuesta CIS Españá, intención de voto, podemos
Slug: podemos-es-la-segunda-fuerza-politica-por-encima-del-psoe-en-espana
Status: published

###### Foto:Vozpopuli 

Según la última encuesta del CIS (Centro de investigaciones sociológicas),** Podemos es la segunda fuerza política**, adelantando al partido socialista, PSOE, aunque la formación conservadora, Partido Popular, PP, continua como primera fuerza pero perdería la mayoría absoluta.

La primera encuesta con intención de voto del año pronostica al **PP como fuerza ganadora con el 27,3 %**, Podemos en segundo lugar con el 23,9% y el Partido socialista en claro receso como tercera fuerza con el 22,2%.

Es la primera vez que el PSOE ocupa el tercer puesto en intención de voto, **rompiéndose así, el tradicional bipartidismo** que ha marcado la política en el estado español durante los últimos 30 años.

Por otro lado está Izquierda Unida con el 5,2 %, UPyD , Ciutadans con el 3,1%, CIU con el 3% y ERC con el 1,6%.

Analistas señalan que sería posible un **pacto entre Izquierda Unida y Podemos**, que junto con otras fuerzas de carácter autonómico, agrupadas en grupo mixto podrían gobernar. En todo caso la llave  de la gobernabilidad quedaría en manos de la formación de la ex-socialista Rosa Díez, UPyD, quién podría pactar con los dos grandes partidos tradicionales.

Este año son elecciones municipales, autonómicas y generales, y que, dichas encuestas van a estar mediatizadas por las negociaciones entre la Troika y Syriza en Grecia con respecto a la negociación de la deuda.
