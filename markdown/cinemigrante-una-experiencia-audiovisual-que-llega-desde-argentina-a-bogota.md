Title: Cinemigrante, una experiencia Audiovisual que llega desde Argentina a Bogotá
Date: 2015-03-18 20:13
Author: CtgAdm
Category: 24 Cuadros, eventos
Tags: Cine Migrante, Cinemateca Distrital, Cultura
Slug: cinemigrante-una-experiencia-audiovisual-que-llega-desde-argentina-a-bogota
Status: published

######  Foto: cinemigrante 

#### *El evento con sede central en Buenos Aires ha convocado más de 23.400 personas en sus cinco ediciones pasadas.*

Desde hoy y hasta el próximo 26 de Marzo los bogotanos tendrán la oportunidad de disfrutar, de manera libre y gratuita, de la Quinta edición de *Cinemigrante*, un espacio para la difusión de cinematografía internacional de alta calidad creado con el objetivo de potenciar la acción expresiva que brinda el cine e invitar a la reflexión, el debate y el intercambio acerca de la movilidad humana y los derechos humanos de las personas migrantes.

Más de 54 títulos hacen parte de la selección de la Quinta muestra Internacional, que incluye además una retrospectiva del realizador francés Sylvain George, un Seminario de Cine Documental a cargo del director Javier Corcuera (España/Perú) y el Encuentro Internacional de Directores ”*Cine documental: Sujetos actuales y modos de representación en el cine documental contemporáneo*” con la participación de Sylvain George (Francia), Luis Ospina (Colombia) y Javier Corcuera (Perú).

<iframe src="https://player.vimeo.com/video/121621229" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Salas como la Cinemateca Distrital de Bogotá y Cine Tonalá serán el epicentro para esta experiencia de intercambio cultural, alternando con un circuito de proyecciones en espacios universitarios destinados a generar instancias de formación en derechos humanos como son la Plaza Central de la Universidad Nacional, Básicas (aire libre) de la Pontificia Universidad Javeriana, en la Universidad de Los Andes y en la Universidad Jorge Tadeo Lozano. Al mismo tiempo, contará con funciones nocturnas de cortometrajes que se proyectarán en el Centro Cultural La Aldea, punto de encuentro del Festival culminando cada jornada con una fiesta temática.

La Gala de apertura inicia a las 6:00 p.m. de hoy miércoles 18 de marzo en el Centro de Memoria, Paz y Reconciliación Cra 19B No. 24 – 82; con la presentación musical de “El Frente Cumbiero” acompañando el estreno de “La Salada” (Juan Martín Hsu, Argentina, 2013), premier que contará además con la presentación de las artistas del performance Jesusa Rodríguez de México y  Liliana Felipe  de Argentina.

<iframe src="https://player.vimeo.com/video/107093552" width="500" height="281" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

La quinta edición de la Muestra Internacional CineMigrante, es co-organizada por el Instituto Nacional de Cine y Artes Audiovisuales (INCAA), la Organización Internacional para las Migraciones (OIM Colombia), el Fondo de población de las Naciones Unidas (UNFPA Colombia), la Oficina de Relaciones Internacionales de la Alcaldía Mayor de Bogotá, e Instituto Distrital de las Artes (Idartes) con el apoyo de la Embajada de Francia y de la Universidad Nacional de Colombia, la Pontificia Universidad Javeriana, Universidad Jorge Tadeo Lozano y Universidad de los Andes.
