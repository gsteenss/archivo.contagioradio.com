Title: ¿Qué lograron incluir los pueblos afro e indígenas en los acuerdos de paz?
Date: 2016-08-26 13:53
Category: Nacional, Paz
Tags: Acuerdos de paz en Colombia, Diálogos de paz en Colombia, enfoque étnico acuerdos de paz, ONIC
Slug: que-lograron-incluir-los-pueblos-afro-e-indigenas-en-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Guardía-indígena.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio ] 

###### [26 Ago 2016 ] 

El pasado miércoles viajó a La Habana una delegación de indígenas y afrocolombianos con el objetivo de que fuera incorporado el enfoque étnico en los acuerdos de paz para proteger y promover los derechos reconocidos a estos pueblos en el marco de la implementación del acuerdo final. Propósito que se logró según afirma Luis Fernando Arias, Consejero Mayor de la Organización Nacional Indígena de Colombia ONIC, quien agrega que esta inclusión permitirá avanzar en la garantía de derechos en términos de autonomía, territorialidad y participación política, principalmente.

En el capítulo de reforma rural integral lograron acordar que los recursos del fondo de tierras sean destinados para la adquisición, constitución, ampliación, delimitación y saneamiento de los resguardos indígenas y territorios colectivos, y que los **programas de desarrollo se implementen teniendo en cuenta la consulta previa**, libre e informada la que tienen derecho los pueblos indígenas y afrodescendientes, bajo la perspectiva de fortalecer sus planes de vida.   ** **

En de los acuerdos también se incluyó el fortalecimiento de los escenarios de participación política de las comunidades étnicas en lo que van a ser los Consejos Territoriales de Paz que serán transitorios durante los próximos dos periodos electorales, afirma Arias y agrega que en el capítulo de [[cultivos de uso ilícito](https://archivo.contagioradio.com/las-condiciones-de-sustitucion-de-cultivos-ilicitos-segun-los-campesinos/)] **se consagró la importancia del uso tradicional y espiritual de la hoja de coca** y en ese sentido las dos partes asumieron el compromiso de devolver las tierras indígenas que han sido afectadas por este tipo de cultivos.

El Consejero insiste en que la [[incorporación de este enfoque](https://archivo.contagioradio.com/acuerdo-final-de-paz-entre-gobierno-y-farc/)] partió de la consideración de las condiciones de exclusión y violencia a las que históricamente han estado sometidas las comunidades étnicas en Colombia, y sobre esa base se fundamentó en la **protección de los derechos amparados en normativas nacionales e internacionales** como la Declaración de Naciones Unidas sobre los Derechos de los Pueblos Indígenas, el Convenio 169 de la OIT y la Convención contra la Discriminación Racial para que en ningún caso la implementación de los acuerdos implique la regresividad en derechos.

<iframe src="http://co.ivoox.com/es/player_ej_12677078_2_1.html?data=kpejmZyUe5mhhpywj5WaaZS1lZeah5yncZOhhpywj5WRaZi3jpWah5ynca3pytiYqMrWssLixdSYo9fNpdSZk6iYsbOth46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] 

 
