Title: La Fiscalía está montando pruebas: Defensa de jóvenes implicados en caso Andino
Date: 2018-08-31 20:42
Category: Judicial, Nacional
Tags: Andino, audiencia, ELN, Fiscalía, MRP
Slug: fiscalia-esta-montando-pruebas-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-24-a-las-5.34.20-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MantillaIgnacio] 

###### [21 Ago 2018] 

El proceso de los jóvenes implicados en el caso del atentado al **Centro Comercial Andino** tiene ahora una nueva acusación de la Fiscalía y el soporte de un testigo que vincula a los jóvenes con el **Movimiento Revolucionario del Pueblo (MRP)**, sin embargo, la bancada de la defensa reiteró que se está haciendo un montaje judicial y se están cometiendo irregularidades en el caso.

**David Uribe**, abogado de la bancada de la defensa de los jóvenes implicados en el caso del Andino,  aseguró que la Fiscalía hizo un montaje burdo, porque entre las pruebas presentadas en el nuevo caso que incluye unos atentados en la capital de Antioquia, se presentó a un testigo que habría sido cercano al grupo e identificó a las 7 personas sobre las que se solicita medida de aseguramiento como parte del MRP.

Sin embargo, para Uribe es evidente que el testimonio está montado, porque en la audiencia de acusación por el caso Andino, **el juez reconoció que los alias con los que se identificó a los jóvenes fueron puestos por los miembros de la Policía judicial** para identificarlos, en el marco de los seguimientos y vigilancia que les habían realizado.

El Abogado aclaró que el proceso se dividió en 2, puesto que tras la orden de libertad para los 10 jóvenes implicados en el caso del atentado al Centro Comercial Andino, **3 fueron puestos en libertad y re capturados, mientras los 7 restantes vivieron el mismo proceso pero con días de diferencia.**

Adicionalmente, Uribe señaló que el otro elemento que hace parte del caso es la relación entre el **MRP y el Ejército de Liberación Nacional (ELN).**En ese proceso, pese a que la Fiscalía en el caso del Andino tenía pruebas de la relación entre ambos grupos, había acusado a los jóvenes por concierto para delinquir, es decir, tratando el hecho como delincuencia común.

Pero en este nuevo proceso, se está buscando sindicarlos de pertenecer a estructuras urbanas del ELN, razón por la cual el tratamiento sería diferente. Para el abogado, el delito que se les buscaría imputar sería distinto o sería una falta a la Constitución, pero sí el caso no se tratara como delincuencia común, mostraría que es una investigación ficticia para impedir la libertad de las personas implicadas.

Para finalizar, el abogado confirmó que **hoy a las 4 de la tarde iniciará la audiencia en la que se sabrá si hay medida de aseguramiento para los 7 jóvenes,** y sostuvo que podrían ver presiones internas para que los funcionarios judiciales tomen determinaciones que están fuera de la ley. (Le puede interesar: ["Liberados y re capturados: ¿Qué pasa con los jóvenes implicados en el caso del atentado al CC Andino"](https://archivo.contagioradio.com/que-pasa-jovenes-caso-cc-andino/))

<iframe id="audio_28275762" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28275762_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
