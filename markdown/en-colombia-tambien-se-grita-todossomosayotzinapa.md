Title: En Colombia también se grita #TodosSomosAyotzinapa
Date: 2014-12-16 15:36
Author: CtgAdm
Category: Video
Slug: en-colombia-tambien-se-grita-todossomosayotzinapa
Status: published

\[embed\]https://www.youtube.com/watch?v=p7ZZFrHPdvE\[/embed\]  
"Vamos a mostrar la respuesta del pueblo organizado para terminar con estos abusos por parte del gobierno, para desear una vida más digna para todos nosotros. No podemos seguir permitiendo que se viole sistemáticamente la integridad física de las personas, únicamente por exigir mejores condiciones de vida, porque por eso desaparecieron a estos compañeros. El Estado fue el que los desapareció"
