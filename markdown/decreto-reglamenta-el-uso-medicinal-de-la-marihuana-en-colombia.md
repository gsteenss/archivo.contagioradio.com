Title: Decreto reglamenta el uso medicinal de la Marihuana en Colombia
Date: 2015-12-21 12:47
Category: Ambiente, Nacional
Tags: Juan Manuel Galán, Marihuana medicinal y científico, Ministerio de Salud, Reglamentación uso marihuana en Colombia, uso medicinal marihuana
Slug: decreto-reglamenta-el-uso-medicinal-de-la-marihuana-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/marihuana.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: [[agenciapacourondo.com.ar]

##### [21 Dic 2015]

El Ministerio de salud expidió el decreto que reglamenta el uso de la marihuana en Colombia con fines terapéuticos y científicos, norma que complementaría el proyecto de ley presentado por Juan Manuel Galán, que actualmente cursa en el Congreso de la República.

El documento establece los lineamientos para la tenencia y cultivo de semillas y plantas de cannabis, así como delimita los procesos de producción, fabricación, exportación, distribución, comercio, uso y tenencia de los productos derivados del proceso, que estén destinados únicamente al uso médico y científico.

Las personas, naturales o jurídicas, interesadas en cultivar y/o producir, deberán realizar el trámite de tres licencias: la primera para determinar el número de plantas ante el Consejo Nacional de Estupefacientes, la forma en que se procesarán las mismas en productos medicinales ante el Ministerio de Salud, y su exportación  ante la autoridad competente.

El decreto de 7 capítulos y 45 artículos. establece además que los cultivos no podrán ser de gran extensión, unicamente serán hidropónicos, y quienes reciban licencia por parte del Consejo Nacional de Estupefacientes, deben presentar un plan de cultivo anual y se debe especificar el monto de las inversiones que se llevarán a cabo durante el tiempo que le sea adjudicada la autorización.

Se espera que en los próximos días se relice la socialización del decreto por parte del Gobierno nacional.
