Title: Sube y baja de emociones por la paz
Date: 2016-10-10 09:16
Category: Opinion, Tatianna
Tags: paz, plebiscito por la paz
Slug: sube-y-baja-de-emociones-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-por-la-paz-contagioradio-e1476125455134.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por [Tatianna Riveros](https://archivo.contagioradio.com/tatianna-riveros-2/)- [@[tatiannarive]

10 Oct 2016

Después del dos de octubre ha ocurrido una serie de hechos que han sacudido al país. Un Nobel de Paz que nos confirma el apoyo internacional, marchas en todo el país exigiendo acuerdos ya, una reunión de un expresidente que después de seis años le dió la gana de sentarse a dialogar, un gerente de campaña contando la estrategia y cómo ganaron.

Hace unos días tenía una tristeza que parecía infinita pensando en lo que le habíamos hecho al país rural, a ese que votó SÍ. Hoy, estoy entre la rabia y la felicidad. Rabia porque la campaña de mentiras y la mezquindad no termina. Rabia porque nos tienen hablando de cómo engañaron a los adultos mayores diciendo que les quitarían el 7% de la pensión, rabia porque utilizaron los subsidios que recibe la gente de estratos bajos y se aprovecharon de eso para decir que guerrilleros desmovilizados recibirían lo mismo. Rabia porque utilizaron una supuesta ideología de género que no está en los acuerdos, y en cambio pusieron a las mismas mujeres a discutir sobre el enfoque de género. Parecen bobadas pero confirman el voto irresponsable que hoy nos tiene en el limbo. Siguen mintiéndole al país y ahora se victimizan para que nadie hable de los números que le faltan a su campaña.

Sí, los diferentes comités del NO (hoy representados por el uribismo) no han presentado todos los aportes recibidos para una campaña (la más barata y ruin de todas) que costó mil trescientos millones de pesos. Como siempre nos tienen hablando de otras cosas en donde el eje siguen siendo ellos.

¿Y si hablamos de las marchas? Esas me tienen llenan de felicidad, una ciudad como Medellín, madre de unos de los carteles más grandes del país, azotada por muchos años por grupos al margen de la ley, se atrevió a marchar, y no eran guerrilleros encapuchados ni estudiantes desocupados ni los que no salieron a votar. Al igual que en Bogotá, fue una marcha SIN politización, una marcha organizada por estudiantes PERO con asistencia de TODO el mundo: adultos, niños, perros, abuelos, las marchas han sido de Laura, Andrea , Felipe, Camilo, de todos aquellos que estamos convencidos de que la paz es nuestra y que un partido político no nos la puede embolatar hasta que consiga volver a poner un presidente.

Seguiremos insistiendo, seguiremos luchando por esa paz anhelada, por esa paz que queremos, por esa paz que buscamos. Seguimos apoyando cada iniciativa, enviaremos cartas a guerrilleros para decirles que los vamos a esperar, que creemos en ellos, seguiremos marchando, seguiremos proponiendo, seguiremos exigiendo.

------------------------------------------------------------------------

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. {#las-opiniones-expresadas-en-este-artículoson-de-exclusivaresponsabilidaddel-autor-y-no-necesariamente-representan-la-opinión-de-lacontagio-radio. dir="ltr"}
