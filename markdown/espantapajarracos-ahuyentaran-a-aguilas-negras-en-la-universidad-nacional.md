Title: "Espantapajarracos" ahuyentarán a Águilas Negras en la Universidad Nacional
Date: 2015-06-03 15:01
Category: Educación, Nacional
Tags: Aguilas Negras, Bogotá, Espantapajarracos, itayosara rojas, leopoldo munera, mario hernandez, Piedad Ortega, Plaza Che, Universidad Nacional de Colombia, Universidad Pedagógica Nacional
Slug: espantapajarracos-ahuyentaran-a-aguilas-negras-en-la-universidad-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/espanta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

##### <iframe src="http://www.ivoox.com/player_ek_4590283_2_1.html?data=lZqmkpecd46ZmKiakpmJd6KokZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYy91cbm0djFtsKfs9TXw9iRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
[Entrevista: Itayosara Rojas] 

##### [3 Jun 2015] 

El próximo **4 de Junio** se llevará a cabo una jornada de protección para la **Universidad Nacional de Colombia**, como respuesta a las reiteradas amenazas de muerte a los profesores [y algunos estudiantes.]

Profesores, estudiantes y trabajadores se encargarán de elaborar figuras tutelares llamadas **“espantapajarracos”,** que tienen la misión de proteger a la universidad y su libre ejercicio de la palabra, el debate, la expresión argumentada de las ideas y la defensa de la diferencia. Las figuras serán instaladas en un acto ritual a las 5 P.M. y a las 6 P.M. Los  y las estudiantes saldrán a rodear con **faroles y antorchas** toda la universidad.

**Itayosara Rojas**, de la facultad de sociología de la **Universidad Nacional de Colombia**, quien también ha recibido amenazas, habló acerca del evento, que empezará a las **2 P.M.** en la **Plaza Che** con tambores, grupos de música Andina y de Rock, “habrá una tarima para que el libre pensamiento y el arte se pueda expresar sin tenerle miedo a ningún águila, también se hará un ritual indígena para sacar las malas energías y los malos presagios de la universidad”.

La estudiante abre la invitación a todos quienes quieran hacer parte de esta jornada, simplemente deben llevar los materiales para los **“espantapajarracos”** y las velas. La iniciativa de los estudiantes y funcionarios de la universidad es una forma de protesta pacífica como forma de rechazo a las amenazas que puedan atentar contra la vida de cualquier persona que quiera expresarse libremente.

[![Espanta pararracos2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Espanta-pararracos21.jpg){.wp-image-9676 .aligncenter width="413" height="367"}](https://archivo.contagioradio.com/espantapajarracos-ahuyentaran-a-aguilas-negras-en-la-universidad-nacional/espanta-pararracos2-2/)
