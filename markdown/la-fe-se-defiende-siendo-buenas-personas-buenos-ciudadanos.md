Title: La fe se defiende siendo buenas personas y buenos ciudadanos
Date: 2019-07-18 11:48
Author: A quien corresponde
Category: Opinion
Tags: Fe, Jesus de nazaret, Teologia
Slug: la-fe-se-defiende-siendo-buenas-personas-buenos-ciudadanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/carretera.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

***Por culpa de ustedes, el nombre de Dios es denigrado entre las naciones***

[(Romanos 2,24) ]

[Bogotá, 6 de julio 2019]

*[Me gusta tu Cristo... No me gustan tus cristianos. ]*

*[Tus cristianos son muy diferentes a tu Cristo.]*

[ Mahatma Gandhi]

[Estimado ]

**Hermano en la fe**

[Cristianas, cristianos, personas interesadas]

[Cordial saludo, ]

[Retomo la comunicación, porque son muchos los temas de los que no hemos podido hablar respetuosamente, espero que en el futuro podamos hacerlo.  ]

[Hay expresiones o ideas que repites como verdades incuestionables y que utilizas para atacar y descalificar a quienes piensan distinto, “a los otros”, a quienes en la práctica niegas derechos por ser del otro partido, de otra religión, de otro grupo social, de otra visión de la vida...; cuando pregunto por las razones para tratarlos así, empiezas a repetir expresiones de]**“los tuyos”, te**[ pones de mal genio y terminamos en discusiones acaloradas, haciendo imposible un dialogo constructivo.]

[ Las siguientes frase o ideas las repites constantemente:]*[“Voy a defender la fe cristiana con todo, así me toque hacer lo que sea”]*[;]*[“Eso no es acuerdo de paz, es un acuerdo para entregarle el país a la guerrilla e imponer la ideología de género]*[”; “Ese proceso de paz ha sido un acuerdo escondido entre la guerrilla y “Farsantos” y sus cómplices de izquierda”;]*[“Hay una relación entre la extrema izquierda y la ideología de género para imponer la homosexualidad”; “el castro-chavismo, la ideología de género y el modernismo van a acabar con la familia tradicional, con las buenas costumbres, los valores tradicionales y el cristianismo”; “Nosotros si estamos a favor de paz, “paz sí pero no así”, pero con ese acuerdo no va a llegar la paz”,  “el cristianismo está siendo víctima de una ataque del demonio por medio del modernismo, del ateísmo, de la ciencia tendenciosa y de los que se las dan de progresistas en las iglesias”.]*[Hay muchas expresiones parecidas.  ]

[Hoy, solo me refiero a la expresión:]*[“Voy a defender la fe cristiana con todo, así me toque hacer lo que sea”]*

[Respeto la manera de vivir tu fe, que te definas como un cristiano  practicante y convencido, “fiel a la palabra de Dios”; tienes derecho a practicarla, a considerarla auténtica y verdadera. En eso no hay discusión. ]

[Pero la realidad es que no ha sido posible entendernos aunque seamos personas de fe, porque comprendemos de manera distinta a Dios, a Jesucristo, al Espíritu Santo, la Fe, la voluntad de Dios y la comprensión de la Sagrada Biblia. Considero muy importante reconocerlo. ]

[Esta comprensión divergente no solo pasa en el ámbito religioso, hay muchos aspectos de la vida y las relaciones humanas en los cuales, con las mismas palabras,  expresamos realidades distintas. ]

[Espero tener la oportunidad de  comprender ¿cómo y por qué llegamos a tener comprensiones tan distintas de Dios, Jesucristo, el Espíritu Santo, la fe, la voluntad de Dios, de la Sagradas Escrituras? ]

[Nuestro dialogo se hace imposible por la “manera” como defiendes tu fe: descalificando groseramente  a los “otros”; atacando a quienes no creen, no practican la fe o tienen otras maneras de ver la vida; tratando mal y mandando al infierno  a quienes piensan distinto en temas morales, políticos, religiosos, sexuales; usando la Biblia contra los “otros”, con lecturas parciales y desconociendo el tiempo, el ambiente humano y social en el que fue escrita; leyendo los textos bíblicos fuera del contexto de la misma página bíblica que citas. ]

[Defender la fe imponiéndola obligatoriamente, llamando malos a quienes no creen, afirmando que si no nos escuchan están rechazando a Dios, considerando “nuestra manera” de vivir la fe como la única, la verdadera, con miedo a lo distinto o con odio a quienes nos rechazan, va en contra del mandato del Señor Jesús:]*[“Ustedes han oído que se dijo: Amarás a tu prójimo y odiarás a tu enemigo. Pues yo les digo: Amen a sus enemigos, oren por sus perseguidores. Así serán hijos de su Padre del cielo, que hace salir su sol sobre malos y buenos y hace llover sobre justos e injustos” (]*[Mateo 5,43-45) ]

[Con todo respeto, pero con toda claridad, es muy difícil ver en personas como tu un discípulo de Jesús, creer que cumples la palabra de Dios como lo dices y aceptar que con tu manera de ser y de tratar a los demás, estás cumpliendo el mandato de Jesús:]*[“Les doy un mandamiento nuevo, que se amen unos a otros como yo los he amado: ámense así unos a otros. En esto conocerán que son mis discípulos, en el amor que se tengan unos a otros”]*[ (Juan 13,34-35).]

[Podemos hablar respetuosamente como creyentes con personas no creyentes, ateos, homosexuales, comunistas, izquierdistas, conservadores, personas LGTBI, feministas y ambientalistas, la inmensa mayoría  respetan nuestras creencias en la medida que sigamos el ejemplo de Jesús de Nazaret. ]

[Considero que defendemos la fe, cuando nuestra manera de ser y actuar en el  mundo, es reconocida como justa, respetuosa, sincera y sencilla. El discurso a Diogeneto, un documento del siglo II nos cuenta como reconocían a los cristianos en la antigüedad:]*[“Los cristianos, en efecto, no se distinguen de los demás hombres ni por la tierra ni por el habla ni por sus costumbres. Porque ni habitan ciudades exclusivas suyas, ni hablan una lengua extraña, ni llevan un género de vida aparte de los demás. A la verdad esta doctrina no ha sido por ellos inventada gracias al talente y especulación de hombres curiosos, ni profesan, como otros hacen, una extrañeza humana, sino que, habitan ciudades griegas o bárbaras, según la suerte que a cada uno le cupo, y adaptándose en vestido, comida y demás genero de vida a los usos y costumbres de cada país, dan muestra de un tenor particular de conducta admirable, y, por confesión de todos, sorprendente” (V 1-4).]*

[Fraternalmente, su hermano en la fe,]

**P. Alberto Franco, CSsR, J&P**

[[francoalberto9@gmail.com]](mailto:francoalberto9@gmail.com)[ ]
