Title: Palabras para inicio de semestre
Date: 2018-01-17 06:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: colegios, estudiantes, Universidades
Slug: palabras-para-inicio-de-semestre
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/inicio-de-semestre.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 17 Ene 2017 

[Algunos estudiantes padecen de dos síntomas producidos por el mismo virus. El primero de los síntomas es el de desarrollar lo que el Padre Camilo Torres llamaba un anticonformismo utópico, que los entrega a superar el estado de silencio y los impulsa a reconocer que existen cosas injustas, cosas que sin dudarlo desean cambiar ostensiblemente, y además, los hace soñar con ser parte de lo que podría ser ese cambio.]

[El anticonformismo utópico, produce tanta o igual conciencia como una propaganda de Coca-cola, y mantiene lugares comunes tales como, la convención sobre la muy famosa, pero en esencia debilucha idea de que la corrupción es el “gran” problema del país. O bien la unanimidad sobre el daño tremendo que produce la avaricia que habita en los políticos colombianos.]

[De allí para adelante, ese ímpetu orientado al cambio, esa excitación por las acciones contestatarias y apasionadas, va decayendo en relación al vislumbramiento de la complejidad que implica un grado, así sea mínimo, de organización política.]

[Sumado a ello, los parciales, la trasnochada luego de la larga revisión de redes sociales, el paso de los semestres, la cantidad alarmante de profesores (por supuesto no todos) dedicados a impartir la displicencia, el irrespeto y el conformismo, finalmente convierten a los estudiantes anticonformistas, en especímenes literalmente sin fe en la humanidad, en seres que transforman la utopía en distopía para así adecuarse mejor al sistema, para procurar conocer viendo series, pero no leyendo tanto y evadir la frustración de no haber sido capaces de luchar aunque sea por las ideas.]

[El segundo síntoma, se puede llamar caricaturescamente el]*[nimierdismo]*[. Es un estado comportamental, en el que so pretexto de que un individuo cree que “hace lo que quiere”, sin analizar las circunstancias, se deja a merced de la cultura de masas, de los medios de comunicación, de la moda, a tal punto que ni siquiera se da cuenta que tiene el mismo corte de cabello que todos o que anhela, en el mundo del mercado, lo que todos.  ]

[¿Entonces cuál es el virus? Sin duda el modelo y cultura neoliberales; que en conjunto podríamos llamarle sin cariño: “el sistema”. Un sistema cuya promesa de libertad individual la convierte en un empalagoso mensaje de “hazlo ahora, hazlo pronto”, fomentando una pereza absoluta en el empeño por algo, una pereza que conlleva implícita el inconformismo con todo aquello que recién se adquiere, o un aburrimiento instantáneo con todas las cosas que se hacen y que mantiene latente la necesidad de experimentar, por sobre la necesidad de pensar en lo que se hace y por qué diablos se hace.]

[Este “sistema”, ahogó a la mayoría de estudiantes en sus tentadoras y profundas redes. Estamos ante el esperpento de universidades plagadas de profesores que van a]*[dictar una clase]*[, y estudiantes que]*[van a clase]*[… y pare de contar. Juntos envueltos en el alter ego que produce un título profesional o una razón epistemológica considerada odiosamente como única y exclusiva.]

[¿Cómo contrarrestar semejante fuerza sistemática? ¿significa que estar deprimidos y ser dramáticos con la existencia, se convertirá en casi una descripción del quehacer revolucionario?  ]

[Un colegio, una escuela, una universidad, incluso un trabajo educativo autodidacta, son un semillero tremendo para componer una vida que basada en la dignidad, en la memoria histórica y en la justicia logre cultivar personas políticas, más analíticas que pasionales, menos intelectuales que pragmáticas, más conscientes que sabelotodo, más cultivadas que tituladas.  ]

[¿Por qué no creer? ¿acaso pensamos que la fuerza sistemática de hoy es superior a la fuerza sistemática que oprimía a otras sociedades en otros tiempos? ¿qué hizo creer a otras generaciones en el cambio?]

[Es que un desahogo en redes sociales desata nudos de garganta, ver una serie que te dice lo mierda que es el mundo se desaparece tan rápido como el humo de un cigarrillo que se expira, no obstante el virus continúa haciendo lo suyo, volviendo no errático sino crónico el pasaje por una nueva preocupación política de la cual solo sabemos quejarnos muy bien, pero no sabemos cómo comenzar a solucionarla, pues al final, ese “sistema” se nos metió al rancho y compuso sin la fuerza la organización de nuestro tiempo… nuestro tiempo libre, que hoy es solo]*[“tiempo libre”]*[ para ga\$\$\$tarlo en algo.  ]

[La cura para el virus, la portan estudiantes capaces de resistir a los deseos reprimidos de unos docentes que bien pueden denigrar de toda idea orientada al cambio político para evitar la frustración que les produce su historia juvenil, o también de otros que pueden burlarse de las ideas orientadas al cambio político porque esos burlones, a pesar de mostrarse “apáticos a la política” o “expertos conocedores de la realidad”, ignorando que el grado de su alienación es tan alto, no se dan cuenta que son funcionales al sistema detestando o contradiciendo toda posibilidad de cambio, es decir que teniendo en cuenta las condiciones de un país como Colombia, ¡¡finalmente no son funcionales, sino idiotas útiles!! (un término éticamente diferente)]

[La cura para el virus, la portan estudiantes y profesores capaces de comprender que a pesar de que la Universidad está mercantilizada, en los pasillos, en los salones, aún se vive una intimidad del pensamiento que convoca a la consciencia; aun pervive en medio de las más penosas adversidades, un instinto subversivo imparable, pues las cosas que ocurren en Colombia no dan para pensar más que sediciosamente.  ]

[La cura para el virus, radica en no permitir que solo el apasionamiento prime en la conclusión política, que no solo la lectura a medias de un par de libros, constituya fortalezas necias que finalmente quedan seducidas por el mercado y sus valores individualistas. La cura para el virus pasa por cuestionar la raíz de los problemas, y antes de matarnos por uno u otro candidato, podríamos pasar a pensar si ese podrido sistema electoral es una cosa inamovible, o si por el contrario, puede ser destruido de raíz para construir un mecanismo electoral que nos permita votar por algo y no por alguien.]

[La cura para el virus, se gestará en las universidades. Pues allí mismo nació el virus. Dicha paradoja del contexto no debe ser un pretexto para huir de la academia, pero mucho menos ahogarse en ella; debe simplemente endulzar la natural rebeldía de la juventud que anda, como nunca, algo deprimida hoy en día. Buen inicio de semestre.]

#### [**Leer más columnas de Johan Mendoza Torres**](https://archivo.contagioradio.com/johan-mendoza-torres/)

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
