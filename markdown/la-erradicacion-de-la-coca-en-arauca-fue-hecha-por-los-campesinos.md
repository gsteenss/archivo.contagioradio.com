Title: La erradicación de la coca en Arauca fue hecha por los campesinos
Date: 2018-04-16 15:06
Category: Nacional, yoreporto
Tags: antinarcóticos, Arauca, coca
Slug: la-erradicacion-de-la-coca-en-arauca-fue-hecha-por-los-campesinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/04/hoja-de-coca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [[Pixabay]

###### 16 Abr 2018 

### **¿Cómo era el contexto de Arauca cuando se cultivaba coca?** 

Este territorio fue colonizado en la década del 50 y 60 a raíz de la guerra conservadora-liberal, que emprendió el Estado contra el campesinado. Al crecer la población en Arauca, también crecían las necesidades insatisfechas por el Estado. En el 70, se empieza a cultivar la coca y marihuana en Arauca, creando una cultura mafiosa, del trago, prostitución y dejando de producir alimento, como también lse se dejó de lado la organización social, la exigencia al Estado por el respeto a los derechos de la comunidad. Al tiempo que crecía este cultivo crecía la represión y persecución estatal.

En el 2002, el régimen de Uribe declara a Arauca como zona de consolidación y rehabilitación, traducido en represión extrema contra el campesinado. Se judicializaron a líderes sociales, se asesinaron campesinos, se implementó la extinción de dominio como arma de desplazamiento contra el campesino tenía en su propiedad cultivo de coca. Todo esto ayudo a los campesinos para que tomaran la decisión de reemplazar la coca por alimentos.

Entre el 2007 al 2009, el campesinado araucano erradica la coca en los municipios de Saravena, Fortul, Arauquita y Tame, llegando a un 80% de erradicación. Aproximadamente había en Arauca 5.000 hectáreas de coca (según cifras oficiales de esa época hablaban de solo 2000 hectáreas de coca), que fueron objeto de erradicación del campesinado.

### **¿Cómo fue el proceso de las comunidades en la erradicación y reemplazo de la coca por alimentos?** 

La gente tenia temor de arrancar la planta, porque se decía que se moriría de hambre. Esta planta trajo el atraso al territorio, ya que no requiere de mayor infraestructura, por ejemplo, la gasolina la puede transportar en caballo, sin exigir carreteras o garantías de producción al Estado. Cuando las comunidades de Arauca, tomaron la decisión de erradicar nadie se murió de hambre, a tal punto de no saber de ese cultivo. Hoy en día, se han mejorado las condiciones de vida, en producción, investigación y técnica, como también se ha fortalecido la organización de los campesinos.

Actualmente, se tiene una economía estable con mejores condiciones de vida, dejando atrás el atraso que imponía la coca. Se han organizado los gremios como plataneros, yuqueros, cacaoteros, ganaderos, productores de leche, entre otros y estableciendo rutas de comercio de alimentos hacia el país.

### **¿Qué reacción tuvo el Estado cuando se dio este proceso?** 

El gobierno nacional habla de política antidrogas, antinarcóticos, pero al Estado le sirve que haya coca, es necesaria para justificar el sostenimiento del aparato militar que emprende y reproduce la guerra contra el pueblo. La coca es un sofisma que distrae al pueblo, y de esa manera ayuda a sostener la represión estatal. Cuando se erradicó la coca el Estado no conoció esta experiencia, ni ayudó a las comunidades.  La gente fue a la policía antinarcóticos, al Ministerio de Defensa, por el certificado de erradicación el cual nunca fue entregado por el establecimiento.

El riesgo de la coca, fue la política de desplazamiento a los campesinos de Arauca por parte del gobierno nacional, que junto al paramilitarismo, la judicialización y la persecución a líderes sociales, la fumigación de cultivos y la extinción de dominio a campesinos con propiedades buscaba sacarnos del territorio, por ser un área estratégica en riquezas naturales como el petróleo.

### **¿Qué ventajas o desventajas tiene la erradicación de coca?** 

La ventaja, es hacer que la tierra cumpla el papel de producir alimento, mejorar la economía, generar un espacio social, como también de proteger el medio ambiente. El campesino se enruta en el papel de mantener el territorio y protegerlo, saliéndose de la cultura mafiosa y camine hacia una sociedad mas justa, crítica, desarrollando la ciencia, la técnica y la tecnología en el territorio.

Los campesinos que cultivan coca, siguen en el atraso, son estigmatizados, la droga producto del cultivo de coca afecta a los jóvenes y demás personas que la consumen. Como pasa con los hijos de campesinos que llegan a la universidad, y por alguna razón conocen las drogas, que vienen del proceso del cultivo de la coca y es un ejemplo entre muchos sobre las consecuencias de cultivar este tipo de planta.

Otra ventaja de erradicar la coca, es la desaparición de los factores que contiene este cultivo, como la violencia, la descomposición social, la desorganización, la contaminación de la naturaleza, desdolarización. Por eso, se está a favor de la vida, el territorio, la cultura campesina, y se le exige al Estado que responda por los derechos del campesinado. Debemos generar una propuesta transitoria para dejar la coca, y proyectar un hombre y mujer nueva.

### **¿Qué proponen a las comunidades que no han erradicado la coca de sus territorios?** 

A estas comunidades, hay que construir un plan nacional para exigirle al Estado las garantías para producir alimentos y convertirnos en potencia agrícola nacional e internacional. Debemos hacer un balance en materia de consumo interno, para conocer que podemos producir internamente y que podemos exportar, porque Colombia puede ser una potencia en comida y no en guerra.

Apuntemos a la producción sana para el mundo, aprovechando nuestras riquezas naturales. Para crear una economía sustentable y sostenible a partir del trabajo organizado de los campesinos y campesinas.

#### [\*Entrevista realizada a Carlos Núñez, miembro de la Asociación Nacional Campesina José Antonio Galán Zorro - ASONALCA, proceso articulado al Coordinador Nacional Agrario.] 

#### **Secretaría de Comunicación y Formación del Coordinador Nacional Agrario** 
