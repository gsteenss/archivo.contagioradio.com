Title: En Catatumbo cambiaron los armados, pero continúa la guerra
Date: 2019-07-12 11:38
Author: CtgAdm
Category: Comunidad, Entrevistas
Tags: ejercito, ELN, EPL, Norte de Santander
Slug: en-catatumbo-cambiaron-los-armados-pero-continua-la-guerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Catatumbo-e1562885240294.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Seguimiento.co  
] 

El pasado 7 de julio, la Oficina en Colombia de las Naciones Unidas para la Coordinación de Asuntos Humanitarios (OCHA) denunció la situación de **desplazamiento y confinamiento de más de 800 personas en Catatumbo**. El hecho se presentaría debido a combates entre el ELN y el EPL, por disputas territoriales. Sin embargo, líderes de la zona afirman que este tipo de sucesos se presentan en toda la región de forma constante.

> \[EHP\] Flash Update N°2 - Desplazamiento masivo y confinamiento en La Playa de Belén (Norte de Santander) - <https://t.co/fREVDWetpg> [@GerardGomez59](https://twitter.com/GerardGomez59?ref_src=twsrc%5Etfw) [pic.twitter.com/qeFYqH5CF3](https://t.co/qeFYqH5CF3)
>
> — Ocha Colombia (@ochacolombia) [7 de julio de 2019](https://twitter.com/ochacolombia/status/1147938293117587456?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **En Catatumbo tienen lugar dos Conflictos Armados No Internacionales**

**Olmer Pérez, integrante de la Asociación Campesina del Catatumbo (ASCAMCAT),** afirmó que "todos los días se presenta algo en diferentes municipios, porque la región está bien azotada por el conflicto". Según lo advirtió el [Comité Internacional de la Cruz Roja (CICR)](https://archivo.contagioradio.com/colombia-conflictos-armados-cicr/), en la región se adelantan al menos dos de los cinco Conflictos Armados No Internacionales que hay en Colombia, con la confrontación entre el ELN, el EPL y las Fuerzas Militares.

En un primer momento, después de la firma del Acuerdo de Paz, las situaciones de violencia se presentaron producto de la confrontación y búsqueda por ganar territorio entre los dos grupos armados ilegales. Posteriormente, con la llegada de más pié de fuerza pública a la región, a partir de 2018, el conflicto no solo se ha profundizado, sino que "la violación de derechos humanos continúa con diferente actor".

El Líder señala que en un reciente recorrido por el territorio, por parte de organizaciones sociales, encontraron que hay un toque de queda en varias vías públicas por parte de las fuerzas militares, que no permiten movimientos de las comunidades después de las 6 de la tarde. Adicionalmente, "hemos detectado que la FUDRA 3 (Fuerza de Despliegue Rápido que opera en la región) acampa en fuentes hídricas donde los campesinos toman agua y destruyen los acueductos".

### **Es necesario buscar la paz con el ELN y cumplir el Acuerdo pactado con las FARC** 

Una de las razones por las cuales el territorio está en disputa es el control sobre los cultivos de uso ilícito, por esa razón, Pérez expresa que los campesinos en la región tienen interés en respaldar el **Plan Nacional Integral de Sustitución (PNIS)**; pero requieren que el Gobierno también camine en ese sentido, porque hasta ahora han visto que "no tiene voluntad de aplicar lo acordado".

En cuanto a las confrontaciones armadas, el Integrante de ASCAMCAT hace un llamado al Gobierno y el ELN para que retomen la mesa de diálogo, y así se evite situaciones de violaciones de derechos para las comunidades que habitan en Catatumbo. (Le puede interesar:["ONU registró 13 mil personas desplazadas durante 2018 en Norte de Santander"](https://archivo.contagioradio.com/personas-desplazadas-durante-2018-catatumbo/))

**Síguenos en Facebok:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38328370" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38328370_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
