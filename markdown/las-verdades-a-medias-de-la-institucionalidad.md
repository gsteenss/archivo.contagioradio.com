Title: Las verdades a medias de la institucionalidad
Date: 2015-05-07 11:42
Author: CtgAdm
Category: Fernando Q, Opinion
Tags: alias Diadema, Convivir, falsos positivos, Fernando Quijano, Fuerzas militares, Medellin, Oficina del Valle de Aburrá, Pacto del fusil, Policía Nacional, Rastrojos
Slug: las-verdades-a-medias-de-la-institucionalidad
Status: published

#### **[Fernando Quijano](https://archivo.contagioradio.com/fernando-quijano/) - [~~@~~FdoQuijano](https://twitter.com/FdoQuijano)

He venido revisando juiciosamente los continuos anuncios públicos que dan cuenta de las permanentes victorias militares que la institucionalidad, en cabeza de la fuerza pública, dice propinarle diariamente a la ilegalidad armada en Colombia, representada actualmente por las FARC, el ELN, el EPL, ‘Urabeños’ la Oficina del Valle de Aburrá, la Cordillera y los Rastrojos, entre otros.

Los anuncios, que  incluyen por supuesto la captura, muerte o “neutralización”  de los máximos jefes –aunque al otro día surjan un nuevos cabecillas-, reflejan que es en la lucha contrainsurgente donde el Estado está concentrando el mayor número de recursos económicos, logísticos, propagandísticos y humanos, especialmente para conseguir la derrota final de quien por más de 50 años ha puesto en riesgo el *statu quo.*

A diario dicen los generales, portando orgullosos los cuatro soles o estrellas, que ya inició el “**comienzo del fin”**, que se está a un paso de la victoria, y lo repiten constantemente hasta creérselo y hacerlo creer a la mayoría de ciudadanos, incluyendo al presidente de la república de turno.

Respetando a los miembros de la fuerza pública que a diario luchan **honestamente** en zonas rurales y urbanas, y no en cómodos escritorios de los centros de operaciones o en el recinto del congreso donde a punta de lobby se reparten privilegios, ascensos y prebendas, debe puntualizarse que la guerra pareciera que se estuviera ganando ante los medios de comunicación y no en campos y ciudades. Casi se podría decir que las constantes victorias anunciadas son más de nivel propagandístico y de juego de cifras que la realidad misma: 5700 ejecuciones extrajudiciales, conocidas como ‘Falsos positivos’,  así lo demuestran.

Este asunto propagandístico de éxitos policiales y militares, desvían la atención y no permiten que la opinión pública se concentre en otro asunto que cobra relevancia en la actualidad: la violencia y la criminalidad urbana.

Si en el escenario del conflicto armado, social y político se ve inconsistencias a la hora de avances o retrocesos del Estado, en las ciudades la cosa pinta peor. La estrategia de seguridad urbana parece diseñada cual juego de vídeo en el que gana quien lo juega pero no deja de ser ficción, nada más.

“Las verdades a medias terminan siendo grandes mentiras” y Medellín es un ejemplo palpable de esta práctica. Los “contundentes” resultados que a diario se anuncian del triunfo de lo institucional sobre lo criminal, tiene ejemplos que demuestran las mentiras que nos venden para mejorar la “percepción en seguridad” de la ciudadanía.

Como ejemplo se puede tomar lo afirmado frente a la supuesta desarticulación de las 35 ‘**Convivir**’: la policía solo reconoce la existencia de ocho de estos grupos y afirma su hipótesis luego de la captura de 23 de sus integrantes, de 19 policías rasos que les apoyaban, y de **alias Diadema**, antiguo jefe de estas agrupaciones que fue  relevado antes de ser capturado. Antes de que Diadema fuera apresado, lo remplazó alias Cejas, quien de forma silenciosa, ya que no estaba “caliente” con las autoridades, apoyó la reorganización de las Convivir, especialmente en la división técnica del trabajo criminal: el grupo criminal asumiría el control del territorio; prestación de la vigilancia; cobros menores de la ‘vacuna’, es decir, extorsión a la venta ambulante y prostitución, entre otros; y ejecuciones, especialmente de quienes no compartan o se nieguen a su dominio.

Por otro lado, el cobro al comercio, empresarios, dueños de casas y ollas de vicio, lo realizan los protagonistas del pacto del fusil: Urabeños y Oficina y lo hacen a través de una nueva modalidad: las casas de enlace o cobro, que no es otra cosa que la sistematización de la extorsión por medio de giros que permiten borrar cualquier rastro. Una verdadera innovación del crimen aunque no lo reconozcan el alcalde de Medellín, el Gobernador de Antioquia y la comandancia de la Policía Metropolitana del Valle de Aburrá.

Señores de la institucionalidad, ustedes tienen la palabra: ¿Estrategia de seguridad mediática, con el show que la acompaña, o estrategia de seguridad integral que realmente desmantele la criminalidad organizada?

**Píldora urbana:** Sería bueno que la institucionalidad reconociera frente al país y a su  jefe de Estado la verdad sobre la existencia del [Pacto del fusil](http://analisisurbano.com/?p=14712),  sólo así podrá saberse de una vez por todas a qué se debe esta “paz” que vive el Valle de Aburrá.
