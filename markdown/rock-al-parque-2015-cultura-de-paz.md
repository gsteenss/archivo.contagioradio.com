Title: Rock al parque 2015, Cultura de paz
Date: 2015-08-15 10:00
Category: Cultura, eventos
Tags: Cafe Tacvba, Coda, IDARTES, Los Cafres, Los Mentas, Los Pericos, P.O.D, Programación Rock al parque 2015, Rock al parque 2015, Sierra Leone’s Refugees All Stars, Vetusta Morla
Slug: rock-al-parque-2015-cultura-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/rockalparque_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [15 Ago 2015] 

20 años han pasado de la primera edición del **Festival Rock al parque**, el evento gratuito más grande de Latinoamérica, y el espíritu sigue intacto. Durante este fin de semana, los sonidos duros, el pogo y las melenas al aire se vuelven a vivir en los **tres escenarios del Parque Metropolitano "Simón Bolívar" y en la tarima de La Media Torta**.

Serán 3 días, para que los amantes del rock, y sus diferentes corrientes, disfruten con las propuestas de las **74 agrupaciones**, entre locales, nacionales e internacionales, que hacen parte del cartel del evento que en esta ocasión lleva como eslogan "**cultura de paz**".

La presencia de agrupaciones como **P.O.D.** (Estados Unidos), **I’ll Nino** (Estados Unidos), **A.N.I.M.A.L.**, **Los Pericos** y **Los Cafres **(Argentina); **Vetusta Morla** (España); **Coda** (México) y **Los Mentas** (Venezuela) entre otras, marcan la cuota internacional del Festival que en esta oportunidad acoge por primera vez a la mítica  agrupación reggae africana **Sierra Leone’s Refugees All Stars**.

A la 1:00 p.m de este **Sábado 15**, los amplificadores vibrarán con la presentación de la banda distrital Schutmaat Trio*, *hasta el cierre a las 9:00 p.m del próximo **lunes** a cargo de los siempre bienvenidos Cafe Tacvba. Esta es la programación del Festival, para que se agende durante estos **tres días de extrema convivencia**.

[![parrilla-sabado](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/parrilla-sabado.jpg){.aligncenter .size-full .wp-image-12225 width="600" height="1261"}](https://archivo.contagioradio.com/?attachment_id=12225)

[![parrilla-domingo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/parrilla-domingo.jpg){.aligncenter .size-full .wp-image-12226 width="600" height="1261"}](https://archivo.contagioradio.com/?attachment_id=12226)  
[![parrilla-lunes](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/parrilla-lunes.jpg){.aligncenter .size-full .wp-image-12227 width="600" height="1261"}](https://archivo.contagioradio.com/?attachment_id=12227)
