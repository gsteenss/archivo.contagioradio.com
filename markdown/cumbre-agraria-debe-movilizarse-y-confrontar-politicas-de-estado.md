Title: "Cumbre Agraria debe movilizarse y confrontar políticas de Estado"
Date: 2015-02-04 17:43
Author: CtgAdm
Category: DDHH, Movilización
Tags: colombia, congreso de los pueblos, Cumbre Agraria, izquierda, marcha patriotica, Seminario político
Slug: cumbre-agraria-debe-movilizarse-y-confrontar-politicas-de-estado
Status: published

###### Foto: Carmela María 

 <iframe src="http://www.ivoox.com/player_ek_4038490_2_1.html?data=lZWgmpmddI6ZmKiakp6Jd6KnmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bhytPO1M7Tb9HjzYqwlYqliNXdxNSYxsqPsMKfpNraxNfJb6Lb08bfy8aRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jhon Henry Herrera, Cumbre Agraria] 

La **Cumbre Agraria es "un espacio que debe continuar, fortalecerse y dar pasos significativos** en lo que debe ser la movilización y la confrontación de las políticas del Estado", señala Jhon Henry Herrera, del CNA, en el marco del primer seminario político de la Cumbre a un año de su lanzamiento como movimiento agrario y popular unitario.

Entre el 3 y el 5 de febrero, el proceso que reúne a organizaciones campesinas, indígenas y afro como la **ONIC, el PCN, el CNA, la MIA y la MUA**, espera adelantar un balance de la situación nacional, de los logros político organizativos de la Cumbre, y las perspectivas de consolidación y movilización social en el 1er seminario político.

Las organizaciones están de acuerdo en valorar de manera positiva el nacimiento del movimiento para la lucha colectiva de sectores rurales, y la construcción colectiva de un **pliego de peticiones que ha logrado ser plural, diverso y unitario.**

Sin embargo señalan que una de las principales trabas para la negociación ha sido que el **gobierno juega con el escenario de las conversaciones de paz y de la cumbre, frenando todos los posibles avances concretos que pueden darse en uno u otro espacio**.

La Cumbre, señala Herrera, representa una esperanza en el contexto de conversaciones de paz que se vive en Colombia, pues allí pueden discutir y proponer cambios en temas vitales para el país.

En el marco del seminario político, las organizaciones buscan dar vía a discusiones históricas que atañen a las organizaciones y movimientos sociales de izquierda: ¿Cómo resolver las tensiones entre región y nación? ¿Cómo armonizar las reivindicaciones de tipo táctico con la construcción de programas y estrategias políticas? ¿Cuál es la perspectiva política de las izquierdas que confluyen en la Cumbre Agraria? y finalmente, ¿Cuál es la perspectiva unitaria que tienen las organizaciones y los procesos sociales que confluyen en ella, para poder alcanzar los objetivos trazados?

Se espera que las discusiones decanten algunas de estas tensiones internas, mantenimiento el espíritu de trabajo colectivo unitario, y reconociendo que, **aunque se ha estado caminando por carriles diferentes, se va hacia un mismo lugar**.
