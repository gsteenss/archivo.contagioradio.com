Title: Comunidades del Guayabo en Santander recuperan sus tierras tras el desalojo
Date: 2018-01-15 13:14
Category: DDHH, Nacional
Tags: comunidades del guayabo, desalojo de campesinos, el guayabo, Santander
Slug: guayabo-recuperan-tierras-desalojados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/DTWxOJ0W4AUxkn-e1516036466633.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Trochando Sin Frontera] 

###### [15 Ene 2018] 

La Asociación Nacional Campesina en conjunto con la comunidad del Guayabo en Santander, enviaron un comunicado a la opinión pública para informar la **recuperación de su territorio.** Las comunidades recuperaron los predios de San Felipe y Altamira de los cuales habían sido desalojados el 13 de diciembre de 2017.

La recuperación de los predios se dio después de una **acción de reivindicación** el 12 de enero de este año realizada por las comunidades del Guayabo y Bellaunión y con el respaldo del Coordinador Nacional Agrario y la Comisión de Interlocución del sur de Bolívar, Centro y Sur del Cesar.

Cuando realizaron la acción evidenciaron **“la ocupación ilegal por parte de varios sujetos**, un arma de fuego de largo alcance y munición”. Denunciaron además que “uno de los sujetos huyó del lugar con una motocicleta en cual sacó del predio varias armas cortas”. (Le puede interesar: "[Sin techo pasarán la navidad 60 familias desalojadas del Guayabo en Santander"](https://archivo.contagioradio.com/60-familias-de-el-guayabo-fueron-desalojadas-en-puerto-wilches-santander/))

### **Desde hace 25 años estos corregimientos han sufrido la violencia** 

En el comunicado establecen que, tanto el Guayabo como Bellaunión, **han sufrido de manera permanente hechos violentos.** Indican que esto se debe a “la injerencia de grupos paramilitares, y desde el 2002 por el despojo de los predios mencionados del cual subsisten las familias afectadas”.

Ante esta situación, las comunidades han realizado los requerimientos correspondientes para **garantizar la posesión de la tierra** y también han denunciado las acciones violentas de la Policía y “del terrateniente y ocupante Rodrigo López Henao”. Manifestaron que se han presentado disparos y amenazas e incluso “el 5 de enero llegó el señor López Henao con un grupo de hombres armados amedrentando la comunidad”. De igual forma, el 8 de enero “hicieron presencia miembro del ejército y la policía quienes requisaron a los campesinos”.

### **Corregimiento del Guayabo es una zona estratégica para megaproyectos** 

En repetidas ocasiones las comunidades han manifestado que el Guayabo, al encontrarse en la rivera del río Magdalena, es una **zona estratégica para los megaproyectos** como el puerto multimodal de Barrancabermeja, el ferrocarril de Carare y la canalización del río Magdalena para aumentar su navegabilidad.

También se han aumentado en esta zona la ganadería extensiva, los monocultivos de arroz y palma de aceite. Debido a los proyectos extractivos que han llegado a estos corregimientos y que “pretenden explotar la riqueza petrolífera y aurífera de la zona”, desde el 2002 la comunidad del Guayabo ha sido **víctima de desalojos** constantes de tierras comunitarias como playones, ciénagas y humedales. (Le puede interesar:["En el día internacional indígena ordenan desalojo de un resguardo"](https://archivo.contagioradio.com/en-el-dia-internacional-de-los-pueblos-indigenas-ordenan-desalojo-de-un-resguardo/))

Finalmente, tanto las organizaciones sociales como las comunidades rechazaron la re victimización de los campesinos del Guayabo y **respaldaron los procesos de recuperación** **de tierras**. Ante esto, le exigieron al Gobierno Nacional la creación de una comisión de alto nivel que brinde garantías y soluciones a estas comunidades. Indicaron que van a continuar trabajando por el reconocimiento del campesinado como sujetos de derechos y la territorialidad campesina.

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
