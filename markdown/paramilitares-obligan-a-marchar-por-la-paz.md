Title: Paramilitares obligan a pobladores a marchar por la Paz
Date: 2016-11-11 12:57
Category: DDHH, Nacional
Tags: Caucasia, neoparamilitares, paramilitares, Presión Psicológica
Slug: paramilitares-obligan-a-marchar-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/paramilitares-Caucasia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Radio Canadá Internacional] 

###### [11 Nov. 2016] 

Según informaciones recientes de la Comisión Intereclesial de Justicia y Paz, po**bladores de Puerto Valdivia se vieron forzados en las últimas horas a participar en una Marcha por la Paz en Caucasia**. Dicha convocatoria fue hecha por neoparamilitares de las Autodefensas Gaitanistas de Colombia que se encuentran en esta región.

Según el comunicado, al lugar llegaron varios buses contratados por los armados para transportar a los pobladores de Valdivia hacia el acto central que tendrá lugar en Caucasia. Y según aseguran los pobladores, en cada uno de los buses se encuentra un neoparamilitar vestido de civil.

Informaciones recientes aseguran que durante los últimos días **los pobladores de esta subregión de Antioquia, han sido sometidos a presión psicológica por parte de los neoparamilitares para que participen hoy en la Marcha por la Paz.**

Según la misiva de esta organización los paramilitares han expresado “que la paz es con todos los guerreros, luego de intimidar a los pobladores han advertido las consecuencias pecuniarias a las que estarían sometidos de no participar”. Le puede interesar: [Paramilitares amenazan a organizaciones de derechos humanos del Valle del Cauca](https://archivo.contagioradio.com/organizaciones-ddhh-del-valle-del-cauca-amenazadas-por-paramilitares/)

**Los armados han advertido a todos los pobladores que no pueden faltar a la marcha convocada desde las 7:00 am.**  A las madres comunitarias les expresaron que debían cesar en sus actividades y llevar a y todos a la manifestación “los paramilitares han dejado el mensaje que era obligatorio salir de sus casas el viernes 11 de noviembre y llegar a Cacucasia a primera hora para la marcha por la Paz”.

Quienes se nieguen a participar de esta y otras actividades convocadas por los armados **deberán pagar una multa de \$500.000 es decir aproximadamente 170 dólares.**

Por su parte, la presencia armada del Estado en las localidades de Valdivia y Caucasia no han garantizado las libertades de los ciudadanos y han hecho caso omiso ante las denuncias de la comunidad y de la organización no gubernamental. Le puede interesar: [Amenazas paramilitares en zona campamentaria de las Farc](https://archivo.contagioradio.com/amenazas-paramilitares-en-zona-campamentaria/)
