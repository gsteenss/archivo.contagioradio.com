Title: JEP pide información de 9 brigadas implicadas en ejecuciones extrajudiciales
Date: 2018-08-02 17:43
Category: DDHH, Paz
Tags: Ejecuciones Extra Judiciales, falsos positivos, JEP
Slug: 9-brigadas-implicadas-en-ejecuciones-extrajudiciales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/militares-jep-falsos-positivos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio  Radio] 

###### [2 Ago 2018] 

La Jurisdicción Especial de Paz (JEP) solicitó, el miércoles 1 de Agosto, al Ministerio de Defensa información sobre el funcionamiento y los resultados de operación de la primera, segunda, cuarta y séptima división del Ejército Nacional. Esto ocurre en el marco de la apertura del caso 003 de la JEP sobre "Muertes ilegítimamente presentadas como bajas en combate por agentes del Estado", o los mal llamados, 'falsos positivos' que tuvo lugar el pasado 23 de julio.

La apertura de este caso se da a partir del informe 5 de la Fiscalía de la Nación en el que se identifica un total de 2.248 víctimas de ejecuciones extrajudiciales entre 1988 y 2014, reconociendo que hubo un alza en el número de homicidios desde el año 2002 y situando el punto más álgido del fenómeno entre los años 2006 y 2008.

Por estos asesinatos, y considerando que las divisiones del Ejército mencionadas podrían estar involucradas en casos de ejecuciones extrajudiciales, la JEP pidió que entregue información sobre los planes de operación de la desaparecida, Brigada Móvil 15, y las Brigadas 2, 4, 5, 7, 10, 16, 17 y 30 del Ejército Nacional. (Le puede interesar: ["Cinco Divisiones y diez Brigadas en la mira de la Corte Penal por 'Falsos Positivos'](https://archivo.contagioradio.com/cinco-divisiones-y-diez-brigadas-en-la-mira-de-la-cpi-por-los-llamados-falsos-positivos/)"

### **Brigada Móvil Nº 15**

Esta brigada fue creada a finales de 2005 y comienzos de 2006, tuvo su Sede de operación en Norte de Santander. Fue eliminada en el 2009 por el entonces Ministro de Defensa Juan Manuel Santos, debido a la cantidad de denuncias que se presentaban en la zona de presuntas violaciones a los Derechos Humanos de campesinos, así como su relación con ejecuciones extrajudiciales.

Un coronel y otros 20 integrantes de esta brigada ya fueron condenados por la desaparición y posterior ejecución de Víctor Fernando Gómez; Julio Cesar Mesa; Jhonatan Orlando Soto; Diego Alberto Tamayo; y Jader Andrés Palacio Bustamante, jóvenes  del municipio de Soacha, Cundinamarca.

De acuerdo con lo que se ha conocido de este caso, uniformados ofrecieron a los jóvenes del vecino Municipio de Bogotá ofertas de trabajo en el campo, para posteriormente asesinarlos y presentarlos como miembros de la guerrilla, muertos en combate en Ocaña, Norte de Santander. (Le puede interesar: "[Siguen las dilaciones en casos de ejecuciones extrajudiciales de Soacha"](https://archivo.contagioradio.com/siguen-las-dilaciones-en-ejecuciones-extrajudiciales-de-soacha/))

Tras la eliminación de esta Brigada, sus integrantes fueron reubicados, y se la reemplazó por la Brigada Móvil Nº23 que opera en el Municipio de El Tarra, Norte de Santander. Aunque la actual brigada no será objeto de revisión por parte de la JEP, sí tiene en su cuenta denuncias de [grupos de abogados](https://www.colectivodeabogados.org/Militares-de-la-brigada-movil-no) de la región por irregularidades como la retención ilegal de personas.

**Primera División del Ejército**

Esta división tiene jurisdicción en 5 departamentos del norte de Colombia y está compuesta por 2 unidades más pequeñas, la Brigada Nº2 con sede en Barranquilla, Atlántico, y la Brigada Nº10 con sede en Valledupar, Cesar

El Informe sobre las actividades de examen preliminar 2017, de la Corte Penal Internacional (CPI), señala que la brigada 10 "presuntamente cometió aproximadamente 146 homicidios conocidos como falsos positivos entre 2004 y 2008 en el departamento de Cesar".

### **Segunda División del Ejército** 

La Segunda división del Ejército tiene como sede la ciudad de Bucaramanga, Santander, y está compuesta por la Brigada Nº1 con sede en Tunja, Boyacá; la Brigada Nº5 con sede en Bucaramanga; la Brigada Nº30 de la Ciudad de Cúcuta, Norte de Santander; y la Fuerza de Tarea Vulcano que opera en el Catatumbo.  (Le puede interesar: ["¿Dónde estaba el Ejército durante la masacre de El Tarra?"](https://archivo.contagioradio.com/donde-estaba-ejercito-durante-masacre/))

Sobre la Segunda división, específicamente la desaparecida Brigada 15 y la Brigada 30, la Corte Penal expresó que presuntamente cometieron "aproximadamente 123 homicidios" o ejecuciones extrajudiciales en Norte de Santander y Magdalena entre los años 2002 y 2009.

**Cuarta División del Ejército**

El puesto de mando de esta división está en Apiay, Meta, y tiene jurisdicción en los departamentos de Meta, Guaviare, Vaupés y dos municipios de Cundinamarca. A la Cuarta división pertenecen la Brigada Nº 7 con sede en Villavicencio; la Brigada Nº22 con jurisdicción sobre el Guaviare; y la Brigada Nº 31, con sede Mitú, Vaupés.

Sobre la 7ª Brigada, la CPI señala que presuntamente cometió aproximadamente 224 ejecuciones extrajudiciales entre los años 2002 y 2008, en los departamentos de Meta, Casanare y Vichada. El informe incluye en esta división la Brigada Nº16, sin embargo, debido a una reorganización del Ejército, esta fue adscrita a la Octava División del Ejército. (Le puede interesar: ["VII Brigada Ejército y AUC hostigan a familias campesinas en Sumapaz"](https://archivo.contagioradio.com/vii-brigada-ejercito-y-auc-hostigan-a-familias-campesinas-en-sumapaz/))

**Séptima División del Ejército**

La Séptima División del Ejército entró en operación en el 2005, con sede en Medellín, Antioquia, y jurisdicción en los departamentos de Antioquia, Córdoba, Sucre y Chocó. Esta división se compone de la Brigada Nº4 con sede en Medellín; la Brigada Nº11 con sede en Monteria, Córdoba; la Brigada Nº 14 con sede en Puerto Berrio, Antioquia; y la Brigada Nº17 con sede en Carepa, Antioquia.

Sobre las Brigadas Nº 4, 11 y 14; la CPI dijo que entre 2001 y 2008, se presentaron 667 casos de ejecuciones extrajuidiciales en los departamentos de Antioquia y Córdoba a cargo presuntamente, de personal perteneciente a estas Brigadas.

Uno de estos casos que ya es materia de investigación por parte de la JEP, es la del Caso del General (r) Mario Montoya Uribe, quién estuvo a cargo de la Operación Orión en Medellín de la que resultaron, según cifras de organizaciones sociales, 95 casos de desaparición forzada documentados y 30 homicidios de líderes sociales. (Le puede interesar: ["Los casos por los que debería responder el Gral. Montoya ante la JEP"](https://archivo.contagioradio.com/casos-responder-montoya-ante-jep/))

### **Estas divisiones del Ejército concentran cerca del 60% de los casos de ejecuciones extrajudiciales** 

Según el comunicado sobre el caso 003 de la JEP, las ejecuciones extra judiciales tuvieron lugar en 29 de los 32 departamentos del territorio nacional; siendo Antioquia, Casanare, Cesar, Meta y Norte de Santander los lugares en los que se presentó el fenómeno con mayor intensidad.

Adicionalmente, el comunicado anuncia que conforme a la información entregada por la Fiscalía, estas brigadas de las que se pide información al ministerio de Defensa concentran cerca del 60% de los casos que involucran ejecuciones extrajudiciales. Sobre estos casos hay unos 2.100 procesos judiciales que presuntamente involucran además de soldados, a altos mandos de la organización militar.

[Comunicado 058 de la JEP sobre apertura de caso 003](https://www.scribd.com/document/385310138/Comunicado-058-de-la-JEP-sobre-apertura-de-caso-003#from_embed "View Comunicado 058 de la JEP sobre apertura de caso 003 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_92115" class="scribd_iframe_embed" title="Comunicado 058 de la JEP sobre apertura de caso 003" src="https://www.scribd.com/embeds/385310138/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-XCC0KLNOMEr8blxmXKaf&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe>

[Informe sobre las actividades de examen preliminar 2017 de la Corte Penal Internacional](https://www.scribd.com/document/385329798/Informe-sobre-las-actividades-de-examen-preliminar-2017-de-la-Corte-Penal-Internacional#from_embed "View Informe sobre las actividades de examen preliminar 2017 de la Corte Penal Internacional on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe class="scribd_iframe_embed" title="Informe sobre las actividades de examen preliminar 2017 de la Corte Penal Internacional" src="https://www.scribd.com/embeds/385329798/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-xvfhSxhQ1RHH2Fqw7Nfj&amp;show_recommendations=true" data-auto-height="false" data-aspect-ratio="0.707221350078493" scrolling="no" id="doc_69011" width="100%" height="600" frameborder="0"></iframe>  
 

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU) 
