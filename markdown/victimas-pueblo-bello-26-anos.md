Title: Víctimas en Pueblo Bello completan 26 años exigiendo verdad y justicia
Date: 2016-01-15 16:03
Category: DDHH, Nacional
Tags: Desaparición forzada, Pueblo Bello
Slug: victimas-pueblo-bello-26-anos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Fotografía-Pueblo-Bello-CCJ-4-e1452894695757.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pueblo Bello 

###### [15 Enero 2015]

Este 14 de enero se conmemoraron 26 años de la **desaparición forzada de 43 campesinos del corregimiento de Pueblo Bello, Antioquia.** Los familiares realizaron un acto de memoria y honra con el fin de resaltar el buen nombre de sus seres queridos, víctimas de la **“desprotección del Estado”**, como lo aseguraron las familias.

En el acto, recordaron cómo hace 26 años los habitantes de Pueblo Bello  escucharon la respuesta del entonces Capitán del Ejército, Fabio Enrique Rincón Pulido, cuando le preguntaron por el paradero de los 43 campesinos, quien en nombre de la Fuerza pública situada en San Pedro de Urabá respondió **“en Pueblo Bello cambiaron vacas por gente”,** haciendo referencia a un robo de ganado realizado meses atrás por la guerrilla a paramilitares de la finca “Las Tangas” en Córdoba. “Estas palabras todavía resuenan en nuestra mente y hacen mella en nuestro corazón a pesar del tiempo trascurrido”, expresaron los familiares.

Pese al acto de perdón de Juan Manuel Santos como ministro de defensa en el año 2009, que se realizó  de acuerdo a las medidas de reparación ordenadas por la Corte Interamericana de Derechos Humanos en el 2006, **han pasado 9 años y aun el Estado no cumple con lo estipulado por la CIDH para reparar a las víctimas debido a la falta de voluntad política.**

En ese sentido, denuncian que no se ha cumplido con la construcción del monumento en memoria de los 43 campesinos desaparecidos, entre otras medidas.

“La actual política del gobierno del Sr. Presidente  Dr. Juan Manuel Santos con las víctimas ha sido publicitar medidas de reparación-económicas, pero estos no es suficiente, es necesaria y urgente una reparación con verdad y justicia”, dicen los familiares, quienes pese al panorama de incumplimientos del gobierno, aseguran que seguirán luchando por la verdad y justicia.[![Fotografía Pueblo Bello - CCJ 5](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Fotografía-Pueblo-Bello-CCJ-5-e1452894635733.jpg){.aligncenter .wp-image-19226 .size-full width="800" height="534"}](https://archivo.contagioradio.com/victimas-en-pueblo-bello-completan-26-anos-exigiendo-verdad-y-justicia/fotografia-pueblo-bello-ccj-5/)

[![Fotografía Pueblo Bello - CCJ 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Fotografía-Pueblo-Bello-CCJ-1.jpg){.aligncenter .size-full .wp-image-19225 width="2000" height="1336"}](https://archivo.contagioradio.com/victimas-en-pueblo-bello-completan-26-anos-exigiendo-verdad-y-justicia/fotografia-pueblo-bello-ccj-1/)

[![Fotografía Pueblo Bello - CCJ 3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Fotografía-Pueblo-Bello-CCJ-3-e1452894668184.jpg){.aligncenter .wp-image-19224 .size-full width="800" height="534"}](https://archivo.contagioradio.com/victimas-en-pueblo-bello-completan-26-anos-exigiendo-verdad-y-justicia/fotografia-pueblo-bello-ccj-3/)
