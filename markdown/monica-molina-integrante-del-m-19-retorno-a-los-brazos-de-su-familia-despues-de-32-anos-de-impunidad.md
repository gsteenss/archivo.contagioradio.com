Title: Mónica Molina, integrante del M 19, retornó a los brazos de su familia después de 32 años de impunidad
Date: 2018-01-22 18:48
Category: DDHH, Nacional, Sin Olvido
Tags: Ejecucion extrajudicial, M-19, Mónica Molina, Palacio de Justicia, Víctimas de la Toma y Retoma del Palacio de Justicia
Slug: monica-molina-integrante-del-m-19-retorno-a-los-brazos-de-su-familia-despues-de-32-anos-de-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/WhatsApp-Image-2018-01-20-at-12.20.46-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Justicia y Paz] 

###### [22 Ene 2018] 

El pasado 20 de enero se hizo la entrega oficial de los restos de Mónica Molina, integrante del M-19 y víctima de desaparición forzada durante la toma y retoma del Palacio de Justicia. Para Patricia, hermana de Mónica Molina, una de las luchas más importantes durante estos largos 32 años, ha sido que el **Estado reconozca el crimen de lesa humanidad cometido contra los entonces integrantes de esta guerrilla**, con las ejecuciones extrajudiciales y asesinatos en estado de indefensión.

De acuerdo con Patricia, con la entrega de los restos de su hermana se abren más preguntas frente al accionar del Estado el 6 y 7 de noviembre de 1985, además, señaló que el paso que se ha dado, es producto de una sentencia de la Corte Interamericana de Derechos Humanos que [**falla a favor de las víctimas y le exige al estado su reparación inmediata.**] (Le puede interesar: "[El pacto del silencio" tras los 32 año de la toma y retoma del Palacio de Justicia](https://archivo.contagioradio.com/49372/))

De igual forma recalcó que tampoco hay respuestas sobre el asesinato del defensor de derechos humanos, Eduardo Umañana Medonza, que desde el principio apoyo a las víctimas del Palacio de Justicia “**aquí no nos han querido develar la verdad, por ejemplo, está el asesinato del abogado Eduardo Umaña Mendoza**, que logró un fallo para realizar las primeras exhumaciones de los cadáveres, fue asesinado por eso”.

### **32 años sin Mónica ** 

Para Patricia la búsqueda de su hermana **ha sido la lucha contra la estigmatización y el temo**r, debido a la militancia activa en el M-19 de su hermana y las implicaciones en términos de seguridad que implicaba encontrarla. Sin embargo, eso no impidió que reclamaran y exigieran información sobre Mónica.

“Al principio no podíamos hacer muy público esta búsqueda, por lo que significa en Colombia oponerse al Estado, que se ha caracterizado por ser criminal y cometer peores injusticias que en las dictaduras chilenas o argentinas, **hay más fosas comunes que en cualquier otra parte**” afirmó Patricia.

Patricia relata que Mónica tenía 19 años cuando fue víctima de desaparición forzada. Había terminado su bachillerato y se había trasladado a Bogotá. Además, señala que su hermana creció en un contexto bastante crítico con la realidad “parte de la familia compartimos una lucha fundamental contra este sistema desigual, injusto, a**rrasador de derechos y Mónica siempre tuvo esa semilla de rebeldía**”.

El paso siguiente para la familia Molina será continuar en la búsqueda de los demás resto de los huesos de Mónica, debido a que solo les fueron entregados una parte .En ese sentido, Patricia manifiesta que si bien ya tienen una parte de Mónica, ahora inician el camino para que el Estado les responda cómo fue asesinada y por qué. (Le puede interesar: ["32 años después Fiscalía sigue negando a los desaparecidos del Palacio de Justicia"](https://archivo.contagioradio.com/32-anos-despues-fiscalia-sigue-negando-a-los-desaparecidos-del-palacio-de-justicia/))

<iframe id="audio_23293079" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23293079_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
