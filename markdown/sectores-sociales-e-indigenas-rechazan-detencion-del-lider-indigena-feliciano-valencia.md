Title: Sectores sociales e indígenas rechazan detención del líder indígena Feliciano Valencia
Date: 2015-09-16 14:53
Author: AdminContagio
Category: Movilización, Nacional, Resistencias
Tags: ACIN, CRIC, Detención de Feliciano Valencia, Feliciano Valencia, Justicia Especial Indígena, Masacre del Nilo, Movimiento Indígena en Colombia, ONIC
Slug: sectores-sociales-e-indigenas-rechazan-detencion-del-lider-indigena-feliciano-valencia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Congreso-CRIC-10.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: arcoiris] 

###### [16 Sept 2015] 

<iframe src="http://www.ivoox.com/player_ek_8420717_2_1.html?data=mZmfkpyVe46ZmKiak5mJd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8bX1dTfx9iPt9DXysbZx9iPqYzdz8mSpZiJhaXbxtPO1ZDWqcTcwt%2FO0JDIqdXZz8jWh6iXaaOnz5DRx9GPcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

#### Aida Quilcue, líder indígena 

[Este martes en horas de la noche fue capturado por unidades del CTI el líder indígena Feliciano Valencia, acusado de secuestro simple y condenado a 16 años de prisión por el Tribunal Superior de Cauca. Los hechos con los que se relaciona su detención tienen que ver con el **ritual de castigo que, bajo la jurisdicción especial indígena, fue aplicado al cabo Jairo Danilo Chaparral  infiltrado en la minga indígena de La María, Cauca, en 2008**.]

[Luego de que los 10.000 indígenas presentes en la minga notaron que **Chaparral buscaba hacerse pasar por guerrillero y que portaba elementos militares para infiltrar la movilización**, las autoridades indígenas decidieron someterlo a un ritual de castigo que incluyó latigazos y la toma de un remedio.]

[Andrés Almendra Consejero Mayor del CRIC, asegura que **tiempo después el militar fue entregado a la Defensoría del Pueblo, conforme lo demanda la Constitución, en casos de aplicación de justicia especial indígena**.]

[La detención de Feliciano Valencia es entendida por la movilización nacional indígena como un hecho que se suma a la **campaña mediática y de persecución política por parte del Estado colombiano** para su aniquilación y **que desconoce las garantías constitucionales aprobadas en 1991 para la aplicación de su sistema especial de justicia**.      ]

[Por su parte Aida Quilcue lideresa del movimiento indígena caucano, denuncia que existe un desequilibrio en el sistema nacional judicial, pues tanto ella como Feliciano desde 2008 han tenido que **enfrentar más de cinco procesos judiciales en su contra, por delitos como lavado de activos**, mientras que las investigaciones adelantadas por la Masacre del Nilo en 2001 o por el asesinato de su esposo por unidades militares de la Brigada 29, no han arrojado mayores resultados.]

[En 2012 fueron condenados los autores materiales del atentado que acabó con la vida de su esposo, tiempo después fueron absueltas las condenas o rebajadas las penas y actualmente varios de estos militares continúan en el ejercicio de sus cargos, agrega Quilcue. Lo que demuestra que **la justicia sólo llegó a los ejecutores, sin reconocer ni sancionar al  aparato de poder organizado tras la comisión de este delito, y que lleva a la total impunidad en el caso**.]

[En el proceso adelantado contra Feliciano Valencia por secuestro simple **se han registrado varias irregularidades**. El curso de la denuncia inició mucho tiempo después del castigo impartido por las autoridades indígenas al cabo. Tras el fallo en primera instancia, que negó la comisión del delito por parte de Valencia, la Fiscalía apelo y **el proceso fue a una segunda instancia en la que no citaron a Feliciano ni a su abogado, quienes la última notificación que recibieron fue la de orden de captura**.  ]

[El Consejo Regional Indígena del Cauca se ha declarado en asamblea permanente, esperan adelantar reuniones para analizar el hecho y construir una hoja de ruta para los siguientes días y estiman en las próximas una rueda de prensa para comunicarse oficialmente. ]
