Title: "Serranía de las Quinchas lleva en sus entrañas el dolor de una guerra que nunca para"
Date: 2020-04-15 09:44
Author: CtgAdm
Category: Actualidad, Ambiente
Slug: la-serrania-de-las-quinchas-lleva-en-sus-entranas-el-dolor-de-una-guerra-que-nunca-para
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/ce9b350e-2318-477c-b6f4-2bffe2f5a89f.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-09-at-11.09.38-PM.mp4" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 9 de abril integrantes de Ejercito Nacional hallaron en el Parque Nacional Regional Serranía de las Quinchas una caleta con materiales suficientes para construir una laboratorio de cocaína al interior del PNR y sin tener consideración por el daño ambiental que provocarían destruyeron los insumos provocando una daño ambiental severo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según habitantes del territorio minutos después los uniformados procedieron a incinerar el material encontrando, Estefany Grajales líder ambiental de territorio señaló, *"lo militares debieron disponer de esto en otro lugar, y no en medio de un lugar sagrado afectando al ecosistema y llenado de pánico a los pobladores".*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"A todos los materiales hallados les dieron candela, había gasolina, químicos, microondas, tanques para el almacenamiento de agua entre otros elementos, **generando una gigantesca explosión y nubes de humo negro afectando la riqueza natural de esta región ubicada en el valle del magdalena medio**"*
>
> <cite> Estefany Grajales| Líder ambiental</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Según la defensora, la Serranía de las Quinchas *"lleva en sus entrañas el dolor de la guerra, una guerra que nunca para, si no que se transforma"*, haciendo referencia a la presencia de histórica de actores armados como guerrilla, paramilitarismo, narcotráfico, Fuerza Publica. Así como también la intervención de empresas[extractivas](https://www.justiciaypazcolombia.com/la-lucha-embera-contra-la-mineria-en-el-choco/).

<!-- /wp:paragraph -->

<!-- wp:video {"autoplay":false,"id":83122,"loop":false,"muted":false,"playsInline":false,"src":"https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-09-at-11.09.38-PM.mp4"} -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/WhatsApp-Video-2020-04-09-at-11.09.38-PM.mp4">
</video>
</figure>
<!-- /wp:video -->

<!-- wp:heading {"level":3} -->

### Las Quinchas un tesoro natural que debe ser protegido

<!-- /wp:heading -->

<!-- wp:paragraph -->

Grajales afirma que se cumplen 14 años desde la primer declaratoria de este lugar como parque natural, *"a la fecha la comunidad espera que se cumpla el acuerdo 0028 de diciembre de 2008, que habla de la administración, conservación y protección de las Quinchas"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cientos de campesinos y campesinas esperan aún alternativas económicas y proyectos de restitución de cultivos ilícitos, mecanismos que les garantice por parte del Gobierno una economía estable de la mano del aprovechamiento responsable de los recuerdos naturales.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Se ven obligados a talar el bosque para llevar alimentos a sus familias porque nisiquiera pueden cultivar la tierra para tener su pan coger (..)tampoco cuentan con títulos de propiedad, lo cual los hace mas vulnerables para quienes saben y manejan el negocio de la droga, convirtiéndose esta actividad en una de las únicas alternativas para obtener recursos económicos al igual que el aserrín de madera"*
>
> <cite> Estefany Grajales| Líder ambiental </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Por último Grajales afirmó que las entidades territoriales como Corpoboyacá, deben garantizar que *"campesinos se ganen la vida haciendo lo que ellos saben, protegiendo el ecosistema y no huyendo de las arremetidas del Gobierno "* ; esto frente a los casos donde la Fuerza Pública a judicializado a campesinos por talar, mientras el Gobierno otorgaba [licencias](https://archivo.contagioradio.com/jerico-no-necesita-de-mineria-para-su-sostenibilidad/) de explotación en zona ambiental protegida.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Los pobladores de la serranía de las Quinchas quieren permanecer allí, tienen toda la voluntad de aprender a conservar y convivir con la naturaleza",* agrega Grajales y señala que es necesario actuar ya, antes de que se de un caso similar al ocurrido en la Serrania de la Macarena. (Le puede interesar: <https://archivo.contagioradio.com/persecucion-a-pobladores-de-la-macarena-en-desarrollo-de-operacion-artemisa/>)

<!-- /wp:paragraph -->
