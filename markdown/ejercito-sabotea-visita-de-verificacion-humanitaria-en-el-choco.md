Title: Ejercito sabotea visita de verificación humanitaria en el Chocó
Date: 2015-06-16 15:17
Author: AdminContagio
Category: Paz, Resistencias, Uncategorized
Tags: bernardo vivas, cacarica, cavida, denuncia, Derechos Humanos, ejercito, Operacion genesis, paz
Slug: ejercito-sabotea-visita-de-verificacion-humanitaria-en-el-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Cacarica.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Amnesty 

###### 

<iframe src="http://www.ivoox.com/player_ek_4649277_2_1.html?data=lZuhm5ebe46ZmKiakpeJd6KpmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRiMbi1tPQy8aPh8LXwtfWxcaRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Bernardo Vivas, Representante de CAVIDA] 

#####  

###### [16 jun 2015] 

Habitantes de la cuenca del Cacarica, en el departamento del Chocó, denuncian que el Ejercito saboteó la visita de verificación que realizaría la cancillería colombiana al territorio, para iniciar un proceso de reparación integral que sentenció la Corte Interamericana de Derechos Humanos.

En el año 2013 la CIDH condenó al Estado Colombiano por el desarrollo de la Operación Génesis, ejecutada entre el 24 y el 27 de febrero de 1997, en que acciones conjuntas de Ejercito y paramilitares desplazaron a comunidades afrocolombianas de la cuenca del río Cacarica.

La sentencia, proferida en diciembre del 2013, dio un plazo de 1 año para que el Estado reparara integralmente a 535 víctimas. Desde entonces, las comunidades han intentado pactar con la Cancillería colombiana una visita para que se establezca el mecanismo de reparación, y agilice el proceso.

La visita que finalmente logró programarse para la 2da semana de junio fue torpedeada por el Ejercito, según afirma Bernardo Vivas, representante legal de las Comunidades de Autodeterminación y Vida Digna del Cacarica. El Comandante de las Fuerzas Militares de la región habría informado a la Comisión de Verificación que el trayecto duraba 8 horas y que el Ejercito no podían garantizar su seguridad.

"El trayecto sólo dura 1 hora y media. Para nosotros es de muy mal gusto que el comandante de la Fuerza Pública, que está en la zona desde el 2004, ahora le diga a la Comisión que no hay garantías, cuando ellos están por toda la región".

Para los beneficiarios de esta sentencia, la situación humanitaria es difícil, y aseguran que la única esperanza que tenían para solucionar parcialmente su calidad de vida era la visita de la Cancillería, y la reparación integral que esperan por parte del Estado.
