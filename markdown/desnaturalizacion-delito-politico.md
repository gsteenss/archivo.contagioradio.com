Title: Desnaturalización del delito político
Date: 2018-03-04 15:37
Category: Expreso Libertad
Tags: delito político, presos politicos
Slug: desnaturalizacion-delito-politico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/delito-politico.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Delegación de paz] 

###### [20 Feb 2018] 

En este programa del Expreso Libertad el abogado Fabio Díaz y el ex prisionero político Marbel Zamora, conocido también como Chucho Peña, hacen un recorrido histórico por la legislación colombiana y las diferentes modificaciones que ha sufrido para castigar con mayor severidad a los prisioneros y prisioneras políticas y como la tortura, el terrorismo y los delitos conexos, se convirtieron en la puerta de entrada a la violación de los derechos humanos y se fueron implementando, poco a poco, en Colombia hasta institucionalizarse como una práctica.

<iframe id="audio_24154994" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_24154994_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
