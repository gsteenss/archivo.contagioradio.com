Title: Nevado del Cocuy en riesgo por ecoturismo desmedido
Date: 2016-05-24 20:14
Category: Ambiente, Nacional
Tags: Ambiente, Cocuy
Slug: nevado-del-cocuy-en-riesgo-por-ecoturismo-desmedido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/cocuy-e1464138597822.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Solkes 

###### [24 May 2016] 

“El nevado es un territorio de vida no un territorio comercial, el agua es sagrada” es uno de los mensajes con los que la Asociación de Autoridades Tradicionales y Cabildos U´wa – ASOUWA, argumenta los más de dos meses que los indígenas llevan impidiendo la entrada de turistas al **Parque Natural El Nevado del Cocuy donde habitan estas comunidades y que hoy se está descongelando entre 3% al 5% según el Ideam.**

Por esta situación este martes se realizó en la Comisión Sexta del Senado, el debate de control político “Salvemos al Cocuy”, donde participaron representantes de la comunidad indígena afectada por el **desmedido turismo que se realiza en la zona, y que a su vez ha conllevado a un detrimento ambiental como** lo denuncia Aura Tegría, abogada e integrante de la comunidad U’wa.

La afectación ambiental en el Nevado del Cocuy se debe principalmente a que las autoridades ambientales no han hecho cumplir la normativa en materia de protección y conservación, y por lo tanto, no ha habido un buen manejo de las aguas negras como lo asegura Corpoboyacá, **contaminando las fuentes hídricas de los más de 7 mil indígenas asentados en la zona.**

Según cifras de la oficina del senador de la alianza Verde, Jorge Prieto, aunque la capacidad de carga del Parque del Cocuy es de 5.500 visitantes al año, en 2015, el Nevado tuvo que recibir a 19.239 visitantes. **Del 2013 al 2015 ingresaron al Parque 65.159 turistas, es decir cerca de 49 mil visitantes más de lo que se podría.**

Prieto, señala que  “hay es un turismo desorganizado, donde los que han pagado son los indígenas U’wa y los campesinos. Prohibieron los caballos, y ellos aceptaron, pero hay unas zonas de camping, y cabañas, que los administra el Estado, en donde no hay concesión de aguas ni licencias para construirlas”.

Teniendo en cuenta que esos territorios para los indígenas tienen un significado cultural y cosmológico, lo que exigen al gobierno es que **las áreas protegidas sean declaradas en coordinación con el saber ambiental de las comunidades,** de tal manera que se logre una verdadera conservación y protección de la naturaleza. A su vez se solicita que así como el Estado es estricto con la población indígena y campesina que allí vive, así mismo lo sean con las empresas que están sacando provecho económico de actividades como el ecoturismo en el Nevado del Cocuy.

<iframe src="http://co.ivoox.com/es/player_ej_11661548_2_1.html?data=kpajmJaZeJmhhpywj5WcaZS1lpyah5yncZOhhpywj5WRaZi3jpWah5yncaLp08aYtsrLtoa3lIqupsaPcYy1w9TUw8nFb7aZppeSmpWJfZrrwpKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
