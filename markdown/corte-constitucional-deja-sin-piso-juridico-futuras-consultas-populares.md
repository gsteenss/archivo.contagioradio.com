Title: Corte Constitucional deja sin piso jurídico futuras consultas populares
Date: 2019-02-15 16:55
Author: AdminContagio
Category: Ambiente, Judicial
Tags: Ambiente, consultas populares, Corte Constitucional, Rodrigo Negrete
Slug: corte-constitucional-deja-sin-piso-juridico-futuras-consultas-populares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/mineria-voto-popular-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: revistaccesos] 

###### [14 Feb 2019] 

La Corte Constitucional declaró inexequible el artículo 33 de la Ley 136 de 1994 que establecía que los municipios deberían realizar una consulta popular cuando se presentara un proyecto, ya sea minero, turístico u otro, que amenazara con crear un cambio significativo en el uso del suelo o diera lugar a una transformación en las actividades tradicionales de un municipio.

Según el abogado ambiental Rodrigo Negrete, el fallo de la Sala Plena este miércoles **deja sin piso jurídico futuras consultas populares dado que este artículo se había usado como soporte para convocar referendos en contra de actividades mineras o petroleras.** El abogado aseguró que cualquier iniciativa para realizar una consulta sería declarada invalida por los tribunales administrativos.

Esta decisión se suma a la sentencia 095 emitida el año pasado por la Corte Constitucional que declaró que los municipios no tienen la competencia para prohibir actividad minera o petrolera a través de la consulta popular porque éstas decisiones se deben acordar entre las autoridades nacionales y territoriales. [(Le puede interesar: "][[Comunidad de Cumaral llevaría fallo de la Corte Constitucional a instancias internacionales]](https://archivo.contagioradio.com/fallo-de-c-constitucional-sobre-consultas-populares-pasa-por-encima-de-voluntad-de-las-comunidades/)[")]

[Negrete sostuvo que éste fallo desconoce el precedente emitido por el mismo alto tribunal en el 2016 que determinó que las consultas populares sobre minería son legales y que los municipios tienen competencia para pronunciarse sobre los asuntos de minería. El abogado afirmó que éste cambio en posición en la Corte se debe a la llegada de nuevos magistrados que "claramente están en contra de la autonomía territorial y la participación ciudadana".]

Por tales razones, Negrete manifestó que los municipios tendrán que acudir al uso de acuerdos municipales para ejercer su derecho a la soberanía territorial, los cuales siguen vigentes a pesar de las decisiones contrarias de los tribunales administrativos. (Le puede interesar: [En riesgo acuerdos municipales contra la minería en Huila](https://archivo.contagioradio.com/riesgo-acuerdos-municipales-mineria-huila/))

<iframe id="audio_32557508" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32557508_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
