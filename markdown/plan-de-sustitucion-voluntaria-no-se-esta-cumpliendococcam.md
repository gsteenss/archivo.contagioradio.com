Title: "Plan de sustitución voluntaria no se está cumpliendo"COCCAM
Date: 2017-01-27 17:30
Category: Ambiente, Nacional
Tags: COCCAM, Erradicación Forzada
Slug: plan-de-sustitucion-voluntaria-no-se-esta-cumpliendococcam
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/campesino-e1460152084815.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Restitución de Tierras ] 

###### [27 Ene 2017] 

El vocero de **Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana **(COCCAM), César Jerez, expresó que saluda el comunicado de prensa suscrito entre el Gobierno Nacional y las FARC-EP sobre el plan de sustitución voluntaria de cultivos de uso ilícito, pero considera que **no se está cumpliendo debido a que continúan las erradicaciones forzadas en territorios del país**, por parte de Fuerzas Militares.

“Es un acuerdo y está bien, pero **deben hablar del documento y sus alcances con las comunidades**, sí  el gobierno y las FARC-EP no vienen estamos en más de lo mismo, porque acá están los protagonistas: los cocaleros y cocaleras del país” Le puede interesar: ["Erradicación forzada y sustitución no son compatibles con la sustitución: César Jerez"](https://archivo.contagioradio.com/erradicacion-forzada-y-sustitucion-no-son-compatibles-con-la-implementacion-cesar-jerez/)

Además, agregó que es un escenario paradójico porque **mientras se anuncia el plan de sustitución, el gobierno desata una campaña muy fuerte de erradicación**  bajo la resolución 3080 “la meta es erradicar aproximadamente 100.000 mil hectáreas a lo que la gente ha respondido: la erradicación se va a responder con movilización”.

En el plan de sustitución se menciona que la **meta es sustituir aproximadamente 50.000** hectáreas de cultivos de uso ilícito durante el primer año de su implementación, en más de 40 municipios de los departamentos más afectados, y que para **garantizar la participación de las comunidades, se creó un consejo de dirección permanente** en el que tendrán asiento el Gobierno Nacional, las FARC-EP y las comunidades, entre ellos una delegación de COCCAM.

Sin embargo, los c**ampesinos han venido denunciando erradicaciones forzadas en territorios del país como Catatumbo, Nariño, Putumayo y Córdoba**, a pesar de que hagan parte de proyectos de sustitución, acto que va en contra del punto 4, al igual que la determinación del Ministerio de Defensa de retomar las fumigaciones con glifosato. Le puede interesar: ["Comunidades del Putumayo insisten en plan de sustitución a pesar de radicación forzada"](https://archivo.contagioradio.com/comunidades-del-putumayo-insisten-en-plan-de-sustitucion-a-pesar-de-erradicacion-forzad/)

El encuentro de COCCAM  comenzó desde el 27 de enero e irá hasta el 29 de enero se espera que aproximadamente **5.000 campesinos se encuentren en el lanzamiento de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana**, que se lleva a cabo en la ciudad de Popayán en las instalaciones de la Universidad del Cauca, el día domingo se llevará a cabo una movilización por la ciudad que culminará con el acto político del lanzamiento oficial de la plataforma.

<iframe id="audio_16725987" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16725987_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
