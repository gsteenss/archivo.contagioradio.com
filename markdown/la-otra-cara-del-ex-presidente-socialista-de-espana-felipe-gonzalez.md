Title: La otra cara del ex-presidente "socialista" de España, Felipe González
Date: 2015-03-25 18:17
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Felipe González asesorará a opositores venezolanos, Felipe González la otra cara, Felipe gonzález y Gas Natural, Felipe gonzález y su relacion con derecha venezolana
Slug: la-otra-cara-del-ex-presidente-socialista-de-espana-felipe-gonzalez
Status: published

###### Foto:Huffingpost.com 

**Felipe González presidente de España desde 1982 a 1996** por el Partido **Socialista** Obrero Español, que logró bajo su mandato dos mayorías absolutas consecutivas, llevó a cabo todas las reformas en el Estado español en la conocida como transición de la dictadura franquista a una democracia neoliberal. Instauró el estado del bienestar, a España, en la **UE, la ONU y la OTAN** y participó junto con otros países en **guerras** como la del **Golfo** o la de descomposición de la antigua **Yugoslavia.**

Este primer párrafo hace alusión a un resumen sobre su biografía oficial, pero existe **otra cara de González no tan conocida.**

El expresidente español entró a gobernar publicitando el **rechazo a la entrada en la OTAN**, para luego, **introducir** **al país en ella**. En su gobierno sucedieron los peores escándalos de corrupción de la historia moderna de España. Casos como **FILESA, Mario Conde en Banesto, Flick, SEAT, Intelhorce**, o el cobro de comisiones ilegales por el **ex director de la Guardia Civil Luis Roldán,** como el caso más sonado.

Todos los casos de corrupción en sus gobiernos, más la mala gestión económica provocó dos huelgas generales de masivo seguimiento, dejando al país sumido en una crisis económica y una gran tasa de desempleo.

Pero la peor etapa de su gobierno con respecto a la violación de derechos humanos, fue cuando varios de los miembros de su gobierno se vieron implicados en la **guerra sucia contra ETA** o terrorismo de estado.

En su mandato operaron **los GAL, Grupos Armados de Liberación,** grupo paramilitar con conexiones con el propio gobierno y la extrema derecha, que asesinaron a más **de 74 personas entre civiles, activistas de la izquierda abertzale y miembros de ETA**. De lo que no cabe la menor duda, es que **González** fue al menos el **responsable político** de la etapa de la “Guerra sucia contra ETA”.

Después de su etapa como presidente del país, pasó a formar parte del C**onsejo Administrativo** de la gran multinacional española, **Gas Natural**, cobrando al año más de **127.000 euros**.

Cuando un presidente pasa de la política a una gran corporación se llama **puerta giratoria**, ya que por el mero hecho de haber ostentado dicho cargo accede a otro donde su **poder adquisitivo aumenta**.

En las últimas semanas, el ex presidente español ha anunciado que será **asesor jurídico en el caso de los opositores venezolanos presos**, acusados de **intentar planear un golpe de Estado** y de provocar la crisis de las **Guarimbas,** a lo que el presidente Nicolás Maduro ha respondido que "...**Felipe González es un aliado de los promotores del terrorismo y la violencia en Venezuela...**", acusándolo de fomentar una alianza para defender el terrorismo contra el propio gobierno venezolano.

González conoce bien la política venezolana, ya que mantuvo una **estrecha relación** con el **expresidente** **Carlos Andrés Pérez**, **destituido por la corte suprema** por malversación de fondos, así mismo, en su mandato fueron asesinados cientos de personas que protestaban contra su gobierno.

Además, aprovechando su situación de presidente de España vendió a Gustavo Cisneros, primera fortuna en Venezuela, y a la cadena de ropa Galerías Preciados. Es decir, siempre ha mantenido **negocios con el empresariado venezolano** y por lo que parece ahora actúa como **lobbista de multinacionales como Gas Natural** **con intereses en** **Sudamérica**.
