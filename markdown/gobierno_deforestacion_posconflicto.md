Title: "Gobierno no se preparó para impedir altas tasas de deforestación en el posconflicto"
Date: 2017-07-08 09:53
Category: Ambiente, Voces de la Tierra
Tags: deforestación, FARC, IDEAM, posconflicto
Slug: gobierno_deforestacion_posconflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/deforestacion-e1472679143710.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Radio Nacional 

###### [8 Jul 2017] 

Como “una crónica de una muerte anunciada” califica el exministro de ambiente Manuel Rodríguez Becerra, las alarmantes cifras que publicó esta semana el IDEAM respecto a las tasas de deforestación en Colombia. El año pasado **el país perdió 178.597 hectáreas de bosque, lo que representa un aumento del 44 % en comparación al 2015.**

Para Rodríguez lo sorpresivo no es que eso ocurra, ya que esa ha sido la experiencia internacional tras otros procesos de paz, lo increíble es que el Gobierno Nacional no haya evitado dicha situación cuando era algo predecible. Según explica el exministro, las fuerzas armadas del país eran las que debieron tomar rápidamente el control de las zonas que fueron escenario del conflicto colombiano, pero no ha sido así y hoy se ve el resultado.

“No se ha hecho nada, para preparar y tomar control de las áreas que estaban en poder de las FARC”, señala Manuel Rodríguez. Si bien, en países como Nicaragua, El Salvador, Angola se presentó ese mismo fenómeno, Colombia no tiene excusa ya que cuenta con uno de ejércitos de América Latina más grandes. “La pregunta es ¿Por qué no se ha usado esa fuerza donde estaba la guerrilla?”, cuestiona Rodríguez.[(Le puede interesar: Los riesgos para el ambiente en el posconflcito)](https://archivo.contagioradio.com/los-riesgos-ambiente-posconflicto/)

### Las cifras 

Desde 1998 diferentes expertos en materia ambiental en Colombia advirtieron que tras las negociaciones de paz, uno de los primeros efectos es la pérdida de bosques. De acuerdo con el informe del IDEAM, el 34 % de la deforestación de 2016 se presentó en Putumayo, Caquetá, Meta y Guaviare; puntualmente los municipios de Uribe, Mesetas, San Vicente del Caguán, Cartagena del Chairá, Puerto Guzmán y Puerto Leguízamo. Lugares en los que históricamente se ha desarrollado la guerra, y en donde hoy no es la guerrilla la que tiene el control, pero tampoco lo tiene el Estado.

El IDEAM indican que el **95 % de la tala de bosque se concentró en 30 municipios **y el 60,2 % corresponde a la región del Amazonas. Hasta el momento parece no haber mejoría. En el primer trimestre de 2017 se han identificado ocho núcleos activos frente a las Alertas Tempranas de Deforestación en Colombia. Nuevamente aparece la Amazonía, Norte de Santander, Nudo de Paramillo y Chocó. Las principales causas siguen siendo,** la ampliación de la frontera agrícola, la ganadería, la infraestructura vial y los cultivos ilícitos.**

### Identificar responsables 

El ambientalista manifiesta que es necesario conocer la responsabilidad de las FARC y demás grupos armados ilegales, respecto a la problemática ambiental. Uno de los casos más preocupantes es el del Parque Nacional Natural Sierra de la Macarena. Una zona que, pese a contar con una figura de protección ambiental, en la práctica era gobernada por las FARC. Aunque es una de los lugaes más importantes en materia de biodiversidad, se ha evidenciado que **durante el proceso de paz hubo grandes porcentajes de deforestación y siembra de coca. Actividades vinculadas con las FARC.**

Por otra parte, no es un secreto que la **tala de árboles también se presenta por cuenta de intereses empresariales.** Actividades como la ganadería extensiva “no la desarrollan unos pobres campesinos”, expresa el exministro. En Guaviare y Caquetá, se conoce que algunos empresarios han comprado veredas enteras para consolidar grandes extensiones de potreros.

Por otra parte, la salida de las FARC ha conllevado a que otros actores quieran apoderarse de territorios baldíos, como lo indica una investigación realizada por La Silla Vacía.  “Es un fenómeno sui generis, que está asociado al posconflicto (…) Ahora **hay más interés de los terratenientes en invertir en tierras muy baratas que están volviendo praderas** para ganadería extensiva con la expectativa de que puedan alegar posesión para que la Agencia Nacional de Tierras les titulen”.

Además, cabe recordar que estructuras de tipo paramilitar como el Clan del Golfo, que tienen presencia en Córdoba y el Urabá, han ampliado sus zonas de influencia a otras zonas de Colombia como en la parte norte de la región amazónica, donde hoy la deforestación es el pan de cada día. De acuerdo con la misma investigación de la Silla Vacía en el municipio de Uribe, cada día **se están tumbando entre a 2 y 3 hectáreas de bosque.**

### Existe la normativa 

“**El gobierno debe investigar, hay una operación burbuja para detener este proceso, y  hay toda una legislación**”, dice el experto. Muchos de esos lugares ambientalmente estratégicos están bajo figuras como reservas o parques nacionales. Incluso, explica el exministro, en Colombia está prohibido tumbar un bosque así esté en una finca privada, para eso debe hacerse una solicitud.

Lo preocupante hoy es que quienes han talado esos bosques han violado la Ley. Actualmente ni las fuerzas armadas hacen presencia, y tampoco las autoridades ambientales están haciendo seguimiento y dictando las debidas sanciones para ponerle freno a la tala de árboles y la fragmentación de ecosistemas. [(Le puede interesar: La Comisión de la Verdad y el esclarecimiento de crímenes ambientales)](https://archivo.contagioradio.com/crimenes-ambientales-marco-del-conflicto-armado/)

<iframe id="audio_19718599" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19718599_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
