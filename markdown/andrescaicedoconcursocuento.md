Title: 1er concurso de cuento para jóvenes "Andrés Caicedo"
Date: 2017-03-23 09:40
Category: eventos
Tags: Andrés Caicedo, concurso, cuentos
Slug: andrescaicedoconcursocuento
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Concurso-Caicedo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: cali.gov.co 

###### 23 Mar 2017 

Hasta el próximo 16 de julio estará abierta la convocatoria para participar en el **Concurso Nacional de Cuento para jóvenes "Andrés Caicedo"**, organizado en conmemoración a los 40 años de haberse publicado "Que viva la Música", novela insignia del escritor caleño.

De acuerdo con los organizadores, el objetivo de la competencia es que las nuevas generaciones **mantengan viva la obra de Caicedo**. “Queremos que sean jóvenes los que participen con sus narraciones, esa forma única de ver el mundo y de expresarse a través del relato de un cuento", manifestó Luz Adriana Betancourt, secretaria de Cultura de Cali.

**Jóvenes entre los 15 y 25 años de toda Colombia** y residentes en el exterior, podrán participar con **una obra inédita**, escrita en español y que no haya resultado ganadora en otro concurso. El ganador será anunciado en el mes de octubre durante la Feria Internacional del Libro de Cali.

**El cuento que ocupe el primer lugar ganará siete millones de pesos**, el segundo lugar dos millones y el tercer lugar un millón. El jurado lo integran tres figuras de la literatura nacional: Juan Gabriel Vásquez, Juan Esteban Constaín y Melba Escobar. **El cuento ganador y los finalistas serán publicados en un libro**.

Los escritos deben ser enviados por medio virtual al correo bibliotecacente@gmail.com, con **una extensión mínima de 5 hojas y máxima de 25**. Las bases del concurso están disponibles en www.cali.gov.co/cultura. Le puede interesar: [El pueblo u'wa y el origen de la poesía colombiana](https://archivo.contagioradio.com/uwapoesiaindigenas/).

   
 
