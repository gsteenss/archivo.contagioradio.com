Title: Con nuevo transfeminicidio en Cartagena son 29 los ocurridos durante 2020 en el país
Date: 2020-10-06 11:53
Author: CtgAdm
Category: Actualidad, LGBTI
Tags: Asesinatos, colombia, transfeminicidios
Slug: con-nuevo-transfeminicidio-en-cartagena-son-29-los-ocurridos-durante-2020-en-el-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_7847.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Contagio Radio /Carlos Zea*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Un nuevo **caso de transfeminicidio, se registró este 5 de octubre en la ciudad de Cartagena**, en dónde se encontró el cuerpo sin vida de Michell, una mujer que se encontraba trabajando en esta ciudad.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/LunariaFondo/status/1313124139713138689","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/LunariaFondo/status/1313124139713138689

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según la Red Comunitaria Trans, el homicidio se perpetró en la madrugada de este lunes en la capital de Bolívar, en donde **la comunidad afirmó qué encontró el cuerpo sin vida de Michel, con visibles heridas en el pecho causadas con un arma cortopunzante.** ([Señorita María, la historia de una mujer Trans en los andes colombianos](https://archivo.contagioradio.com/senorita-maria-la-historia-una-mujer-trans-los-andes-colombianos/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a que es poca la información que se conoce sobre la identidad de Michel, su caso se suma a los más de **10 transfeminicidios registrados en ciudades como Cali, Cartagena, Medellín y Santa Marta en contra de trabajadoras sexuales**, y el número 29 de agresiones contra mujeres integrantes de la comunidad LGBT durante el 2020 en Colombia.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/redcomunitariat/status/1313113879086497797","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/redcomunitariat/status/1313113879086497797

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Frente a este hecho la Red Comunitaria Trans indicó, ***"nuestra identidad nos está costando la vida. ¡No somos peligrosas, estamos en peligro"***, y exigieron a las autoridades nacionales en cabeza de la[presidencia de Colombia](https://archivo.contagioradio.com/crimenes-de-lesa-humanidad-de-la-policia-iran-a-la-corte-penal-internacional/) y las alcaldías locales, especialmente la de Cartagena, adelantar investigaciones que permitan no solamente esclarecer este transfeminicidio sino que además, ***"la muerte de Míchelle no quede en la impunidad".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En una acción para alzar la voz y exigir que se detenga las agresiones y se haga justicia ante el aumento impunidad en los casos de [asesinatos contra la comunidad](http://colombiadiversa.org/ddhh-lgbt/)LGBT, colectivos feministas como la Red Comunitaria Trans, Putamente Poderosas y las Viejas Verdes, resuenan con su llamado en la campaña, ***"Estamos putas, ¡Empútense con nosotras!**"*, acción que hoy convoca a las 6 de la tarde en el Parque Bolívar en la ciudad de Medellín ,a exigir los derechos y garantías laborales para las trabajadoras sexuales en Colombia.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/redcomunitariat/status/1313288523244601345","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/redcomunitariat/status/1313288523244601345

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78955} /-->
