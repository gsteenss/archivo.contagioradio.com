Title: En los primeros 9 meses de 2018 fueron asesinados 109 líderes sociales
Date: 2018-12-07 16:49
Author: AdminContagio
Category: DDHH, Entrevistas
Tags: 2018, Asesinatos, defensores de derechos humanos, Iván Duque, lideres sociales
Slug: en-los-primeros-9-meses-de-2018-fueron-asesinados-109-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/WhatsApp-Image-2018-08-06-at-3.26.09-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [07 Dic 2018] 

El pasado 6 de diciembre organizaciones defensoras de derechos humanos denunciaron, ante la CIDH, los altos niveles de impunidad que continúan registrándose frente a los actos de violencia en contra de líderes sociales, **que en lo corrido de los 9 primeros meses del 2018, cobraron la vida de 109 personas.**

En ese sentido las organizaciones denunciantes resaltaron que solo en el primer fin de semana de diciembre fueron asesinados 3 líderes indígenas del pueblo Awá, pertenecientes a la organización Camawari. (Le puede interesar: ["Aún no acaba el 2018 y ya son 34 indígenas Awá asesinados")](https://archivo.contagioradio.com/indigenas-awa-asesinados/)

De igual forma, la directora del programa Somos Defensores, Diana Sánchez, alertó que “desde el 23 de agosto cuando se firmó el Plan de Acción Oportuna en casos de defensores, del gobierno Duque, se han producido 67 asesinatos de personas defensoras", **hecho al que se suman un total de 309 amenazas a líderes sociales en lo corrido de este año**.

Las organizaciones señalaron que uno de los factores que ha aumentado el riesgo para las y los líderes sociales "ha sido la tendencia al negacionismo desde el Estado"; refiriendose a la insistencia en no reconocer que persiste el paramilitarismo, ni la sistematicidad en los asesinatos, "obstaculizando el avance de la justicia”.

### **Las investigaciones de la Fiscalía ** 

Otra de las situaciones que denunciaron las organizaciones, están relacionadas con las acciones por parte de la Fiscalía, considerando que **"existe una falta de transparencia sobre lo que comprende un hecho esclarecido** o la actividad investigativa realizada frente a las amenazas, atentados, entre otras agresiones contra personas defensoras de derechos humanos".

Adicionalmente, tampoco se estaría ofreciendo un análisis diferenciado con mirada de género sobre los ataques y la violencia sexual que afecta a las mujeres defensoras de forma particular. De igual forma, las organizaciones señalaron que las políticas públicas  anunciadas por el nuevo gobierno **no han sido concertadas con las organizaciones de la sociedad civil ni ampliamente difundidas,** hecho que genera más impedimentos en la protección de quienes ven amenazada su pervivencia.

Frente a este difícil contexto, Marco Romero de la organización CODHES afirmó que **"Una sociedad que no protege sus líderes sociales, tiene muy pocas posibilidades de consolidarse como democracia**. Lo importante es lograr efectividad de política de protección, por ello debe haber un diálogo Estado y sociedad civil”.

Se espera que tras la audiencia se establezcan medidas concretas por parte del Estado, que estén enfocadas en la creación de mecanismos efectivos para la protección de líderes sociales y defensores de derechos humanos, al igual que avances en las investigaciones de acciones violentas como amenazas y asesinatos.

###### Reciba toda la información de Contagio Radio en [[su correo]
