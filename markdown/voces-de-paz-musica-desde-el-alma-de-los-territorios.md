Title: Voces de Paz, música desde el alma de los territorios
Date: 2016-10-14 13:48
Category: Nacional, Paz
Tags: #PazALaCalle, acuerdo de paz, Acuerdos de La Habana, Conpaz, Música, paz, voces de paz
Slug: voces-de-paz-musica-desde-el-alma-de-los-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/IMG-20161014-WA0009.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 14 Oct 2016 

Luego de un intenso trabajo que duró más de un año, se realizó en la ciudad de Bogotá el lanzamiento del disco **Voces De Paz, música desde el alma de los territorios.**  Este trabajo que estuvo bajo la dirección de Corina Campos, directora de la Coral Leo de Cuba y del maestro Jun E. Ruanes, contó con la participación de 28 personas, 11 mujeres y 17 hombres que representaron a las comunidades de diversos territorios del país como el Naya (Valle del Cauca), Cacarica y Curvaradó (Chocó), Trujillo (Valle), Sucre (Cauca), Dabeiba (Antioquia) y Puerto Asís (Putumayo).

**Voces de Paz es la materialización de un sueño** que fue construido por hombres y mujeres, que revelan en sus letras las razones por las que a través de la música han decidido expresarse. Voces que reflejan risas, llantos, siembras, memoria, resistencias e historias gestadas desde las entrañas de los territorios del país.

**En entrevista, Corina Campos, manifestó** “trabajar con jóvenes, mujeres y hombres ha sido enriquecedor pero no es fácil entender las realidades de estas comunidades. **En mi anterior visita a Colombia cuando los conocí, lloré al ver todo lo que han tenido que vivir, pero ahora soy feliz de ver el avance artístico que han tenido”**.

**En esta producción pueden encontrarse diversos géneros musicales como el currulao, vallenato, balada, hip hop, tonadas andinas, entre otros** ritmos para los cuales fue necesario un trabajo intenso que al final arrojó muchas historias bellas de recordar “el trabajo más interesante fue con las indígenas que no sabían hablar español. Se hizo una labor con ellas que fue buscar un lenguaje para ellas y así poderse comunicar y que no se sintieran excluidas por no hablar español. Inclusive las cuidaban, porque vienen de tierra caliente. Es decir hubo un trabajo integrador” puntualizó Corina.

**Voces de Paz es una manera de hacer memoria desde la música, de lo que no se desea volver a repetir  y de lo que se puede reconstruir en una sociedad**, “yo creo que el arte en sentido general y la música en particular ayuda a cualquier conflicto, que todos son muy dolorosos…yo pienso que a través de estas canciones ellos (las comunidades) se sienten bien y podrán lograr sus objetivos” aseveró.

Este trabajo que realizó sus respectivas grabaciones entre abril y mayo de 2016 en Bogotá, dejó como resultado un disco pero también innumerables historias que se tejieron entre las comunidades y Corina “Yo aprendí también mucho de ellos, les pregunte cómo se toca este instrumento o aquel también cómo se baila eso. Y pues me fue bien por que Cuba y Colombia se parece en muchos ritmos” puntualizó.

**Voces de Paz** fue posible gracias a la financiación de la Agencia Vasca de Cooperación a través de la Fundación Mundubat y el apoyo de la agencia Christian Aid. Y **puede ser escuchado a través de la plataforma Spotify.**

<iframe id="audio_13324611" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13324611_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
\[embed\]https://www.youtube.com/watch?v=K\_sN-x4jkm0&feature=youtu.be\[/embed\]

Le puede interesar: [Inicia la tercera sesión de la cátedra abierta: sujetos territoriales de paz con justicia socio-ambiental](https://archivo.contagioradio.com/inicia-la-tercera-sesion-de-la-catedra-abierta-sujetos-territoriales-de-paz-con-justicia-socio-ambiental/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
