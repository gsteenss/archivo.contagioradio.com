Title: En condiciones degradantes se encuentran prisioneros políticos de Farc
Date: 2020-04-02 12:46
Author: CtgAdm
Category: Actualidad, DDHH
Tags: covid19, politicos
Slug: estamos-nadando-en-nuestras-propias-heces-denuncian-prisioneros-politicos-de-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/carcel-colombia-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Los cuatro prisioneros políticos de FARC, trasladados de forma arbitraria el pasado 22 de marzo a la cárcel de Coiba en Ibagué, denuncian que están siendo víctimas de tratos inhumanos. **"**E**stamos nadando en nuestras propias heces, no tenemos agua, nos tienen encerrados 23 horas al días"** afirman.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las cuatro personas son José Ángel Parra, quien padece de leucemia, enfermedad que podría agravarse en un centro de reclusión que no cuenta con las condiciones mínimas sanitarias. También se encuentran Moises Quintero, Oscar Rodríguez y Luis Franco.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El traslado de estos hombres de la cárcel la Picota, en Bogotá, a la de Coiba se hizo junto a otros reclusos, cuatro prisioneras políticas de la cárcel del Buen Pastor y cien prisioneras sociales de ese mismo centro penitenciario. (Le puede interesar:["El fin no ha sido fugarnos – Mujeres reclusas en El Buen Pastor Bogotá"](https://www.justiciaypazcolombia.com/el-fin-no-ha-sido-fugarnos/))

<!-- /wp:paragraph -->

<!-- wp:heading -->

"Estamos nadando en nuestras propias heces"
-------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

De acuerdo con Quintero, los hombres presentan **afectaciones respiratorias y grastrointestinales**. "Esto es una cloaca escondida del INPEC, son torres abandonadas y patios en donde abundan unos olores nauseabundos"afirma. (Le puede interesar: ["Prisioneros políticos en condiciones críticas luego de traslados"](https://archivo.contagioradio.com/se-conoce-paradero-de-prisioneros-politicos-en-condiciones-criticas/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, aseguran que "estamos nadando en nuestras propias heces", producto de que la cárcel no tendría desagües para el manejo de aguas negras.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, el centro penitenciario no contaría con los servicios de agua, energía. Tampoco estaría acondicionada para garantizar una cuarentena producto de la crisis del covid-19. Esos hechos sumados a las condiciones del lugar, **dejan a la población privada de la libertad en máximo riesgo de contagio.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a esta situación, tanto los prisioneros políticos como sociales están exigiendo medidas urgentes en torno a las violaciones de derechos humanos que están viviendo. "Pedimos con carácter humanitario, una comisión de paz del senado, de la Procuraduria y de derechos humanos internacional, para que vengan y se apersonen de esta situación inhumana, que viven todos los prisioneros en esta cárcel" afirman.

<!-- /wp:paragraph -->
