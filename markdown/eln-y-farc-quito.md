Title: ELN y FARC le están apostando a la paz: Victor de Currea
Date: 2017-10-24 15:23
Category: Nacional, Paz
Tags: ELN, FARC-EP, Proceso de paz en Quito
Slug: eln-y-farc-quito
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/farceln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: NCnoticias] 

###### [24 Oct 2017]

El encuentro entre las FARC y el ELN es un mensaje que ratifica la voluntad por construir un escenario de paz para el país, a través del cumplimiento por un lado de los acuerdos de paz, en el caso de las FARC y por el otro, del cumplimiento al cese bilateral pactado por el ELN con el gobierno, así lo afirmó Víctor de Currea, analista político.

“Estamos frente a dos propuestas político militares que le han apostado a la paz, que han cumplido con lo acordado y a las que el Estado les ha incumplido” señaló Currea, en el caso de las FARC, el analista manifestó que los incumplimientos se vienen dando desde el principio.

Entre ellos la falta de adecuación de las zonas veredales, el trámite de las amnistías que no se ha terminado y que mantiene en las cárceles a más de 1.400 integrantes de esta organización, la falta de voluntad política para implementar los acuerdos de paz desde el Congreso de la República y las modificaciones que se han intentado hacer para transformar el fondo del mismo acuerdo.

Para el analista estos incumplimientos a una guerrilla que ya acordó la dejación de armas, tiene consecuencias en el proceso de paz que se adelanta con el ELN, sin embargo, el analista manifestó que una ventaja que se tendría en este proceso **es la relevancia que se le da a la participación social y la posibilidad de ir implementando lo que se va acordando**.

### **Voluntad política para la paz en el 2018** 

Una de las preocupaciones de cara a las elecciones 2018 es la continuación de la implementación de los acuerdos de paz pactados con las FARC y del proceso con el ELN, en ese sentido Currea señaló que la voluntad política se refleja desde ya con recursos económicos y coherencia en el discurso “hay unos discursos de paz, pero en **la práctica hay un presupuesto que no apunta a la paz y unas figuras públicas que no favorecen**”.

En ese sentido, el analista político señaló que no es razonable que hablando de construcción de paz sea nombrado Néstor Humberto Martínez como Fiscal general, o que Ordoñez haya estado tanto tiempo en la Procuraduría. (Le puede interesar: ["Cambio Radical y Centro Democrático se unen para hacer trizas el acuerdo: FARC"](https://archivo.contagioradio.com/cambio-radical-y-centro-democratico-se-unen-para-hacer-trizas-los-acuerdos-farc/))

### **La movilización social una medida urgente** 

Frente a los incumplimientos por parte del gobierno, Currea afirmó que debe existir una coordinación y unidad al interior de las organizaciones y plataformas del movimiento social que actualmente no se ve reflejada en las acciones que se han emprendido desde estos sectores.

De igual forma, esta semana inician los espacios en donde confluirán diferentes organizaciones para exponer ante una comitiva de delegados del ELN y el gobierno Nacional los **mecanismos de participación que se podrían adoptar para este proceso de paz**. (Le puede interesar: ["Listo escenario para la participación social en conversaciones de Quito"](https://archivo.contagioradio.com/listo-escenario-para-la-partcipacion-social-en-conversaciones-de-quito/))

<iframe id="audio_21660740" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21660740_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **La declaración de Montecristi** 

"Ninguna dificultad- por más grande que se- nos hará desfallecer en la conquista de la paz" reza la declaración de las FARC y el ELN luego del encuentro realizado en Quito este 22 de Octubre. En la carta de 7 puntos**, destacan que este es un momento histórico excepcional en que debe ser clara la unidad de los sectores que buscan la paz.**

Aunque en la declaración, hacen especial énfasis en los problemas de asesinatos de líderes sociales y los retrasos en la implementación del acuerdo, señalan que es necesario mantenerse firmes y exigen del gobierno nacional cumplir su palabra, no solamente para lo que implica al acuerdo con las FARC, sino para dar confianza al diálogo con el ELN.

Esta es la declaración:

[Comunicado de Fuerza Alternativa Revolucionaria Del Común y El ELN](https://www.scribd.com/document/362515377/Comunicado-de-Fuerza-Alternativa-Revolucionaria-Del-Comun-y-El-ELN#from_embed "View Comunicado de Fuerza Alternativa Revolucionaria Del Común y El ELN on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_38585" class="scribd_iframe_embed" title="Comunicado de Fuerza Alternativa Revolucionaria Del Común y El ELN" src="https://www.scribd.com/embeds/362515377/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-YW5VP6kv3CpMXcsUJWVR&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
