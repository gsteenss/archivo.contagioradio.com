Title: Peligro de epidemias deben ser contrarrestadas con urgencia en Mocoa
Date: 2017-04-05 12:58
Category: DDHH, Nacional
Tags: avalancha, Mocoa
Slug: peligro-de-epidemias-deben-ser-contrarrestados-con-urgencia-en-mocoa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/el-heraldo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Heraldo] 

###### [05 Abril 2017] 

Con el paso del tiempo la situación en Mocoa se hace más difícil para las personas afectadas y sobreviviente a la avalancha del pasado sábado, en la madrugada. 5 días después, **los brotes de epidemias y el tratamiento a los heridos, son las preocupaciones que ahora deben afrontar tanto el gobierno** como las diferentes organizaciones que hacen presencia en este lugar.

De acuerdo con Yuri Quintero, diputa de la Asamblea Departamental, las condiciones en las que quedo la ciudad, como el **estancamiento de aguas, la falta de acueducto, sumado a la demora en la entrega de los cuerpos, que ya se están descomponiendo**, se han convertido en el foco para que brotes como la fiebre amarilla aparezcan.

Frente a esta situación ya se dispuso desde ayer, un cuarto frío en donde están los cuerpos mientras se realiza el proceso de entrega de los mismos. Sin embargo, las familias siguen denunciando que estas se entregan se están demorando demasiado, porque la **Fiscalía sigue haciendo el proceso de necropsia y no se adoptan medidas de urgencia adecuadas** para la situación actual. Le puede interesar: ["Demoras en entregas de cuerpos otra tragedia en Mocoa"](https://archivo.contagioradio.com/tragedia-mocoa-cuerpos-ayudas/)

Otra de las problemáticas que aqueja a las víctimas de Mocoa, **son los ladrones que han llegado a saquear lo poco que queda tanto en las viviendas como en los locales** comerciales, la Policía y el Ejercito prometieron presta una mayor vigilancia y aumentar las rondas para evitar estos actos, de igual modo, se están haciendo requisas constantes en el territorio. Le puede interesar: ["Corrupción también tuvo su parte de responsabilidad en avalancha en Mocoa"](https://archivo.contagioradio.com/corrupciontambienfueculpabledeavalnchademocoa/)

En cuanto a los albergues, Quintero señaló que cada vez son más las personas que están llegando a estos lugares, motivo por el cual hizo un llamado a que las donaciones de las personas **giren en torno a elementos como frazadas, colchonetas, carpas y medicamentos, para evitar que las enfermedades se propaguen.**

<iframe id="audio_17985921" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17985921_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

   
 
