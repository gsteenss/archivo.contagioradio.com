Title: Mas peligros que beneficios en el Nuevo Código Electoral
Date: 2020-12-23 16:01
Author: CtgAdm
Category: Actualidad, elecciones
Tags: código electoral, elecciones
Slug: mas-peligros-que-beneficios-en-el-nuevo-codigo-electoral
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/0001234149.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego de ser aprobada en la plenaria de la Cámara de Representantes el pasado 12 de diciembre, este jueves se aprobó en segundo debate el articulado del proyecto de ley por el cual se crea el **nuevo Código Electoral, una modificación que ha sido motivo de análisis de muchos sectores.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Con argumentos que tienen que ver inicialmente con la **conveniente rapidez de aprobación de los 271 artículos con los que cuenta el texto, y los 2 días en los que se dio la respuesta final**, expertos aseguran que se debió tener un mayor análisis en las sesiones ordinarias del Congreso, lo que genera **un gran impacto contra el Código, puesto que le quita legitimidad**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aun así luego de tres décadas, además de múltiples debates **Colombia asume un nuevo camino nacido in extremis** *«en el último momento»*, en el Congreso de la República, con lo que llamaron una homogeneización de los textos de Cámara y Senado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Lo bueno del nuevo Código Electoral**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para **Luis Guillermo Pérez, abogado e integrante del Consejo Nacional Electoral,** inicialmente **las modificaciones del Código Electoral apuntan a una perspectiva de Derechos Humanos, y *"de reconocer los derechos políticos como derechos esenciales****, que deben ser garantizados a toda la población sin exclusión, ni discriminación".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La adopción de medidas afirmativas en relación con la participación étnica en el proceso electoral y elaboración; así como **la ampliación de los puestos de votación para que estén en la Colombia profunda, la adecuación de transporte público gratuito el día de las eleccione**s, también conformarían este punto en garantías de Derechos. ([Se agota el tiempo para dar vida a las curules de la paz advierten al Consejo de Estado](https://archivo.contagioradio.com/se-agota-tiempo-para-dar-vida-a-las-curules-de-paz-advierten-al-consejo-de-estado/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El **Código incluye también, según Pérez una perspectiva de género que *"protege la población LGBTI+, y en donde también se sanciona la violencia política contra las mujeres"***. Además de una propuesta donde pretenden sancionar la propaganda política que difame a los adversarios.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Esta es una propuesta que surge frente a los grandes índices de violencia que hay en el país, que al ser regulados se abriría la puerta para que todos los partidos políticos y candidatos puedan hacer publicidad dedicada a difamar a los adversarios, lo cual **sería una responsabilidad de naturaleza criminal permitirlo**"*
>
> <cite>**Luis Guillermo Pérez, integrante del Consejo Nacional Electoral**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Aclarando que esta acción no tiene nada que ver con la censura, y al contrario va enfocado hacia los candidatos y candidatas; y no hacia los medios de comunicación y periodistas en general que ejerzan su análisis sobre las candidaturas políticas. ([Se hace viral video que critica a Sergio Fajardo](https://archivo.contagioradio.com/video-viral-contra-fajardo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La lista de cosas buenas continúa con los auxilios que recibirán las personas que por temas médicos no puedan desplazarse a un punto de votación, ***"con una copia de su historia clínica podrán solicitar a la Registraduría Nacional del Estado Civil votar desde sus casas, para ellos y ellas se crearán puestos móviles".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El integrante del CNE también enumeró otros puntos como **la actualización de las listas, que permitirá la disminución de la eliminación de votos, esto frente al delito de trashumancia.** Al tiempo indicó que este Código fortalecerá el proceso informático con auditorías permanentes que podrán desarrollar los partidos políticos junto en el CNE. ([Decir que en La Guajira no se compran o venden votos es querer tapar el sol con un dedo](https://archivo.contagioradio.com/la-guajira-compran-o-venden-votos-tapar-sol-con-un-dedo/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "El nuevo Código electoral presenta múltiples problemas así como unos desafíos enormes"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Para **Juliana Hernández integrante de la campaña Paridad ¡YA ! y directora de la Organización Artemisas**, el nuevo Código Electoral presenta múltiples problemas, así como unos desafíos enormes para las elecciones de 2022. [Procuraduría puede cambiar la balanza política y frenar la posibilidad de cambio](https://archivo.contagioradio.com/procuraduria-puede-cambiar-la-balanza-politica-y-frenar-la-posibilidad-de-cambio/)).

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"En las reformas que plantean para el sistema electoral no está claro cómo queda finalmente el Código, **hubo más de 500 proposiciones en lo que se dio este debate, además tramitado de urgencia por parte del Gobierno Nacional**; al igual que más de 200 artículos que se estaban discutiendo para reformar un Código que claramente ya no estaba en vigencia".*
>
> <cite>**Juliana Hernández integrante de la campaña Paridad ¡YA !**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Hernández indicó que hay tres puntos fundamentales analizados desde la fundación Artemisas en relación a las modificaciones del Código y también a las campañas en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**El primero es el voto electrónico**. El caso de Colombia no está exento de los debates sobre la efectividad del voto electrónico. Sin embargo por ley, se debe empezar a implementar. P**ara Hernández debería ser *" la oportunidad de desarrollar procesos electorales transparentes, abiertos y no dependientes a las mismas empresas que han venido monopolizando toda la contratación de los procesos electorales en los últimos lustros".***

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Por otra parte está la condición de seguridad nacional. No es lo mismo aplicar un nuevo código electoral en un país en el que el conflicto está desencadenado** *"una condición de seguridad que se da en un país que está llevado por la guerra con un proceso y Acuerdo de Paz que está en crisis en su implementación"*. ([Derechos Humanos: Un camino de afirmación de la vida](https://archivo.contagioradio.com/derechos-humanos-un-camino-de-afirmacion-a-la-vida/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y **como tercer argumento presentó la discusión pública en la que confluyen, candidatos, líderes políticos, opinadores y medios de comunicación. Según la directora de ARTEMISAS no está claro cuál es la frontera entre la censura y la posibilidad de debate dado que este punto no fue lo suficientemente discutido ni precisado.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*Otro de los problemas que tiene que ver con este tercer aspecto es la publicidad electoral "Esto afecta directamente a las mujeres, dado que ellas no tienen la misma cantidad de recursos financieros que tienen los hombres para defenderse de ataques y manifestar sus posiciones, así que este punto es muy nebuloso con respecto a las restricciones que va haber sobre publicidad en campaña electoral"*, **por tanto propone que este punto debe tener una especial veeduría para que se garantice el derecho a la libre expresión.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otros de los puntos que se deben analizar y hacer seguimiento dentro del nuevo Código Electoral, son por ejemplo el punto que indica, que **el Consejo Nacional Electoral ya no podrá rechazar la inscripción de candidatos que en el pasado hayan sido sancionados con fallos administrativos, disciplinarios o fiscales**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por tanto acciones interpuestas por la Procuraduría o Contraloría, tampoco serán una causal para suspender o destituir, de manera definitiva o temporal a un gobernador o alcalde. **Siendo así las únicas sanciones aplicables las emitidas por las cortes, tribunales y jueces de la República por aquello que se consideren como delito**, lo que evitaría las inscripciones de candidaturas o la suspensión de los cargos. ([CIDH condena al Estado en el caso de Gustavo Petro](https://archivo.contagioradio.com/cidh-condena-al-estado-por-la-destitucion-en-la-alcaldia-de-gustavo-petro/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esta acciones de revocatoria de cargos se suma una propuesta de la bancada del partido Cambio Radica, la cual indica que la acciones de recolecta de firmas ciudadanas para pedir la revocatoria debe estar justificada ante una audiencia pública donde se expongas las razones de peso para exigir esa derogación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y por último el documento también **dio luz verde a que candidatos y candidatas puedan financiar sus campañas con aportes a través de plataformas electrónica**s, y pese a que se presenta como un modelo regulado que tiene como objetivo ser más claros con los dineros que ingresan a las campañas, este mimo punto **exonera a los candidatos de cualquier responsabilidad que vincule los fondos de sus campañas** con financiadores que ejecuten actividades ilegales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Paridad, ¿un paso histórico?

<!-- /wp:heading -->

<!-- wp:embed {"url":"https://www.instagram.com/p/CI83WxiJo9K/?utm_source=ig_web_copy_link","type":"rich","providerNameSlug":"instagram","responsive":true,"className":""} -->

<figure class="wp-block-embed is-type-rich is-provider-instagram wp-block-embed-instagram">
<div class="wp-block-embed__wrapper">

https://www.instagram.com/p/CI83WxiJo9K/?utm\_source=ig\_web\_copy\_link

</div>

</figure>
<!-- /wp:embed -->

<!-- wp:paragraph -->

El tema de la paridad electoral ha sido cuestionado por diferentes sectores sobre si verdaderamente es un paso histórico, y si es o no una reforma estructural. Frente a esto Juliana Hernández indicó con claridad que *[**"esta no es una reforma estructural porque no plantea la alternancia en la conformación de las listas**]{}**haciéndolas más participativas incluyentes y diversas"***, y agregó que esto simplemente modifica la ley de cuotas y la incrementa del 30 al 50% en la participación de las mujeres.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**A pesar de esto indicó que es una acción histórica**, empezando con qué la paridad se lleva discutiendo y tumbando en en el Congreso desde hace años, *"**lo que se evidenció con esta modificación en el Código Electoral es el ruido y la incomodidad que genera una reforma que da paso a la paridad**, ¡y eso que sólo modifica las listas, ni siquiera las posiciones que ocupan las mujeres en estas!".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Frente a las modificaciones de fondo Hernández señaló que **el Código Electoral no está planteado para cambiar las cabezas políticas, debido a que esta no es una reforma política sino una al sistema electoral** y a las reglas del juego, *"eso representa un avance en materia de que van a haber nuevas caras muy obligadas, y es allí donde los partidos políticos van a tener la opción de apoyar a las mujeres que van disputarse el poder".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Permitiendo así demostrar la teoría en materia de elecciones latinoamericanas que indica que, las mujeres que ingresan a la política tienen un gran respaldo social detrás, a excepción de las herederas de poder, *"las mujeres hemos estado ocupando el espacio público desde la organización social comunitaria y del proceso de base, lo cual va a llamar la atención de los partidos políticos y también va hacer un acercamiento muy interesante de las mujeres al poder, **cambiando así el carácter machista y patriarcal de la política colombiana que habita no solamente los cuerpos de hombre sino también de mujeres**"*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Se requiere una reforma mucho más estructural y social para que dejen de seguirse reproduciendo los mismos modelos de roles y estereotipos tradicionales en espacios políticos"*
>
> <cite>**Juliana Hernández integrante de la campaña Paridad ¡YA !**</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente Hernández, llamó la atención a estar muy pendientes de los micos que van a salir de este Código Electoral, especialmente frente a la ampliación que tiene el poder y el registrador en las elecciones de 2022, **invitando a que los cargos que se creen como un ejercicio de veeduría, también deben desde la mirada de las mujeres y así garantizar que sus candidaturas cumplan y sean espacios de transparencia.**

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
