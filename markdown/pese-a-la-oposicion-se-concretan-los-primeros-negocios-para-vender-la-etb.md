Title: Pese a la oposición, se concretan los primeros negocios para vender la ETB
Date: 2016-07-15 11:52
Category: Economía, Nacional
Tags: ATELCA ETB, ETB, Plan de desarrollo Peñalosa, Venta de la ETB
Slug: pese-a-la-oposicion-se-concretan-los-primeros-negocios-para-vender-la-etb
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/cacerolazo-etb-23.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [15 Julio 2016]

Durante la primera semana de este mes, Beatriz Arbeláez, actual [Secretaria de Hacienda,  y Jorge Castellanos, gerente de la Empresa de Teléfonos de Bogotá, viajaron a Nueva York para "realizar una evaluación y sondeo del mercado de inversionistas y firmas interesadas en banca de inversión, con el fin de **estructurar el proceso que debe realizarse tendiente a la enajenación de las acciones de propiedad del Distrito en la ETB**", según el decreto 283 aprobado por el alcalde Enrique Peñalosa.]

[Esta acción, así como el cronograma que se ha establecido internamente para vender la compañía, son rechazados por la Asociación Nacional de Técnicos de Telecomunicaciones de la ETB, sindicato que interpuso **una demanda contra la aprobación de la venta de esta empresa y que se contempla en el plan de desarrollo** distrital, según afirma Alejandra Wilches, quien agrega que la situación laboral continúa muy tensa. ]

Durante los últimos meses, se han [[despedido irregularmente](https://archivo.contagioradio.com/3-trabajadores-despedidos-de-etb-permanecen-en-huelga-de-hambre-y-encadenados/)] y sin justas causas a por lo menos 25 trabajadores, cuyos sindicatos han interpuesto denuncias ante el Ministerio de Trabajo. Pero las decisiones erróneas no solamente han sido en términos laborales, según asevera Wilches la actual administración de la empresa ha puesto en marcha acciones para debilitar la compañía, por lo que **este viernes presentaran demandas ante entes de control**, mientras continúan en su labor de sensibilización sobre [[lo que implica vender la ETB](https://archivo.contagioradio.com/etb-generadora-de-riqueza-que-hay-que-saber-administrar/)].

<iframe src="http://co.ivoox.com/es/player_ej_12235738_2_1.html?data=kpeflZqbd5mhhpywj5WYaZS1k5yah5yncZOhhpywj5WRaZi3jpWah5yncaLgxs_O0MnWpYzLytHQysrXaZO3jLXfx9iSb6LIprGwo5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
