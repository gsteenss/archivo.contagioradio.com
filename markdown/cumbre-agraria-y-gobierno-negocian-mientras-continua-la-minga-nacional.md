Title: Cumbre Agraria y Gobierno negocian mientras continúa la Minga Nacional
Date: 2016-06-10 14:09
Category: Movilización, Nacional
Tags: Cauca, Cumbre Agraria, Gobierno Nacional, Minga Nacional, Paro Nacional
Slug: cumbre-agraria-y-gobierno-negocian-mientras-continua-la-minga-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/mesa-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Cumbre Agraria 

###### [10 Jun 2016] 

**La Cumbre Agraria anunció que desde hoy a las 9 de la mañana se desbloqueó la vía panamericana por 36 horas.** Durante ese mismo periodo de tiempo se inicia una ronda de negociación en Quinamayó, Cauca, mientras continúa la Minga en el resto del país donde a esta hora siguen los bloqueos en vías como la Ruta del Sol, y otros puntos en el departamento del Cauca.

**Pese a que el gobierno preparaba una arremetida en Quinamayó con el ESMAD y el Ejército,** sobre la una de la madrugada el Ministro del Interior, Juan Fernando Cristo aceptó la propuesta de la Cumbre de despejar por completo la vía Panamericana mientras se discuten los 8 puntos del pliego de peticiones incumplido por el gobierno desde el paro agrario del 2013.

En principio **se acordó dar total paso en la carretera por 36 horas, instalar la mesa de negociación y en la medida en que se desarrollan los puntos se evaluarán los avances**  y se procedería a un segundo desbloqueo temporal, incluso, según César Jeréz, uno de los voceros de la Cumbre Agraria, de acuerdo a cómo evolucione la discusión, la Panamericana podría ser totalmente desbloqueada.

Jeréz, puntualiza que el primer punto de negociación tiene que ver con las garantías para la movilización en materia de derechos humanos, pues pese a que se inició una mesa, este viernes continuaron las acciones de represión en diferentes puntos como La Lizama, en Norte de Santander, Ricaurte, en Pasto y en Valledupar, donde hay un campesinos desaparecido conocido como Nicolás Llerena.

"La gente está con la moral en alto. La Minga ha obtenido respaldo incluso de sectores urbanos, y se ha generado presión y solidaridad internacional. **El gobierno debe entender el peso político que tienen 3 indígenas asesinados, 2 bebés abortados y cientos de heridos.** No puede seguir reprimiendo hasta la muerte a las manifestaciones y reclamos de comunidades campesinas", expresa el vocero de la Cumbre.

<iframe src="http://co.ivoox.com/es/player_ej_11857349_2_1.html?data=kpall5yXeJqhhpywj5WdaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncaTZ1Mbfja_JtsbujIqfqJClkrvDs6iah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
