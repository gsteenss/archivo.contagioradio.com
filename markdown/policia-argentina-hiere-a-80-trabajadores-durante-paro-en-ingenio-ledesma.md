Title: Policía argentina hiere a 80 trabajadores durante paro en Ingenio Ledesma
Date: 2016-07-15 13:23
Category: DDHH, El mundo
Tags: Argentina, Ingenio Ledesma, Jujuy, Represión policía Argentina
Slug: policia-argentina-hiere-a-80-trabajadores-durante-paro-en-ingenio-ledesma
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/Ingenio-Ledesma.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Visión Sustentable ] 

###### [15 Julio 2016]

Por lo menos 200 trabajadores del Ingenio Ledesma se manifestaban en el marco del paro indefinido, decretado para exigir mejoras salariales y de las condiciones laborales, cuando fueron **violentamente reprimidos por efectivos policiales y miembros de seguridad privada** de la empresa, en la provincia argentina de Jujuy.

Ochenta empleados resultaron heridos con **balas de goma que fueron disparadas directamente a su cuerpo y a menos de 2 metros de distancia**. También se dispararon gases lacrimógenos y balas de plomo, según los rastros encontrados. Durante la arremetida capturaron a 13 personas que ya fueron liberadas.

Según el periodista Sebastian Fernández, la compañía es uno de los ingenios más grandes de América Latina, produce desde jugos hasta biodiesel, pero paga salarios menores que los del ingenio más pequeño de Jujuy, razón por la que los trabajadores se declararon en huelga desde el pasado mes de junio. Las condiciones laborales son tan negativas que hay casos en los que **trabajan todos los integrantes de una sola familia y reciben solo un salario**.

Los trabajadores denuncian que tienen que usar máquinas que por unidad remplazan a por lo menos 54 empleados, así mismo, tienen que cumplir con producciones diarias más allá de sus posibilidades reales. Para ellos las condiciones laborales son inadecuadas por lo que **se mantendrán en paro hasta que logren acuerdos con la empresa**, cuyo dueño está vinculado con procesos judiciales por crímenes de lesa humanidad cometidos durante la dictadura.

<iframe src="http://co.ivoox.com/es/player_ej_12235966_2_1.html?data=kpeflZqdepehhpywj5aUaZS1lZ2ah5yncZOhhpywj5WRaZi3jpWah5yncbTZw8bg1s6Jh5SZopbbjavJts-ZpJiSo5bSqMbuhpewjbXJtsrjxc7g1saRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
