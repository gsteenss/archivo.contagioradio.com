Title: Amenazan con levantar el Campamento Permanente por la Paz en la Plaza de Bolívar
Date: 2016-10-06 17:19
Category: Nacional, Paz
Tags: Acuerdos de paz en Colombia, Movilización Bogotá, Paz a la Calle
Slug: amenazan-con-levantar-el-campamento-permanente-por-la-paz-en-la-plaza-de-bolivar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/14492538_1773528862916176_948268934035075438_n-e1475791388898.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Paz A La Calle] 

###### [6 Oct 2016] 

Este miércoles, después de la multitudinaria marcha del silencio en la que hubo más de cien mil asistentes, **un grupo de personas instaló en la Plaza de Bolívar el Campamento Permanente por la Paz exigiendo la implementación de los acuerdos de forma inmediata.** En horas de la tarde del día de hoy, agentes del Estado amenazan con hacer el levantamiento del campamento.

Los participantes a través de redes sociales **piden a organizaciones defensoras de Derechos Humanos y medios de comunicación,** acompañamiento para que no se vulneren sus derechos y puedan mantener el campamento de forma indefinida hasta que se [reanude la implementación de los Acuerdos.](https://archivo.contagioradio.com/alcaldes-y-gobernadores-respaldan-el-caracter-humanitario-de-los-acuerdos-de-paz/)

Esta es otra acción colectiva que surge desde la iniciativa \#PazALaCalle, que desde el pasado lunes 3 de octubre plantea diversas propuestas creativas e independientes para continuar la construcción de Paz y promover la implementación de los acuerdos.

José Antequera uno de los integrantes de esta iniciativa afirmó que **la movilización de ayer “logró captar el espíritu de los acuerdos y fue una expresión muy democrática”** que demostró la capacidad de convocatoria y coordinación de los estudiantes universitarios y organizaciones sociales. Se logró que asistieran “personas que normalmente no se movilizan, fue una marcha intergeneracional en un momento pertinente” debido al anuncio de [terminación del cese bilateral.](https://archivo.contagioradio.com/cese-bilateral-plazo-para-que-no-retorne-la-guerra/)

Manifestó que lo más importante es persistir y mantener la presión y las movilizaciones, por ello **el día de hoy jueves a las 7:00pm se llevará a cabo la segunda Asamblea Ciudadana de \#PazALaCalle** en el monumento Almirante Padilla del Park Way.

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio [[bit.ly/1ICYhVU]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-bit.ly1nvao4u-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio-bit.ly1icyhvu .p1}
