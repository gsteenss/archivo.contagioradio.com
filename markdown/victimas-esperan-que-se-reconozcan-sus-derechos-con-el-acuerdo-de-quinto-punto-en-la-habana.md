Title: Víctimas esperan que se reconozcan sus derechos con el acuerdo de quinto punto en La Habana
Date: 2015-12-15 18:50
Category: Nacional, Paz
Tags: Alan Jara, Alfonso Mora, dialogos de paz, General Luis Herlindo Mendieta, Jineth Bedoya, Licinia Collazos, Luz Marina Bernal, María Soledad Garzón, paz, piedad cordoba, proceso de paz, víctimas, víctimas del conflicto armado, Wilfredo Landa, Yaneth Bautista
Slug: victimas-esperan-que-se-reconozcan-sus-derechos-con-el-acuerdo-de-quinto-punto-en-la-habana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/jineth.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:yahoo.com 

<iframe src="http://www.ivoox.com/player_ek_9736673_2_1.html?data=mpygmJubd46ZmKiakpmJd6KlkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRmoa3lIqupsjYrc7V1JDS1dXJtsLijNbix5DXqYzmxsjc0NTep8LijNji1ZDIqdPZxM3c1ZDHs8%2BfxtGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jineth Bedoya] 

###### [15 Dic 2015] 

Durante la firma del acuerdo del quinto punto del proceso de paz entre Gobierno y FARC en La Habana, la delegación de víctimas del conflicto armado en Colombia, expresó que aun y cuando en el momento de redacción del comunicado, desconocían el contenido del documento de víctimas, **apoyan el proceso de paz, pero que su respaldo no significa que renuncien a la justicia, a la reparación y “sobre todo a la verdad”**. Así lo manifestó Jineth Bedoya, la vocera del grupo.

Los firmantes expresaron que en su calidad de víctimas y como testigos de la firma del acuerdo, esperan que se abra el camino para reconocerlos después de varias décadas de impunidad: “reiteramos que así como hemos dejado nuestro dolor con testimonios, hoy reivindicamos nuestra dignidad y exigencia de que los acuerdos sean fieles a los reclamos de todas las víctimas”. Bedoya manifestó que el país debe entender que **solo mediante la vía del diálogo y la concertación, será posible llegar a la reconciliación y a “la materialización de esa palabra que tanto pronunciamos pero de la que poco sabemos: paz”**.

Por este motivo, la delegación sugirió que tanto las FARC como el Gobierno Nacional, deben multiplicar sus esfuerzos para hacer realidad la pedagogía de la paz. En ese sentido, se planteó la posibilidad de que sean las víctimas las que desarrollen esta labor. “Nuestro papel va más allá de ser un grupo de personas marcadas por la violencia: somos protagonistas sociales de un nuevo país”.

No obstante, en la lectura del comunicado, se denunció que varias de las víctimas que han participado en el proceso de paz, han recibido amenazas de muerte de las que aún no existe investigación alguna. “Ese debe ser el mayor ejemplo de las garantías de no repetición”, asegura.

El grupo integrado por Licinia Collazos, Wilfredo Landa, Alfonso Mora, Yaneth Bautista, Luz Marina Bernal, Alan Jara, María Soledad Garzón, Piedad Córdoba, el general Luis Herlindo Mendieta y Jineth Bedoya, sentenciaron que como víctimas “estamos dando la mejor muestra de generosidad al ofrecer nuestra voluntad de reconciliación, estamos creyendo en ustedes y queremos que el país crea en el acuerdo de paz, pero si ustedes fallan, no lo harán con nosotros, lo harán con la historia de Colombia”.
