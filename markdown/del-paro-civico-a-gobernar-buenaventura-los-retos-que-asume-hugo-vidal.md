Title: Del paro cívico a gobernar Buenaventura, los retos que asume Hugo Vidal
Date: 2019-10-29 20:54
Author: CtgAdm
Category: Comunidad, Política
Tags: buenaventura, Elecciones 2019, Paro Cívico de Buenaventura, Valle del Cauca
Slug: del-paro-civico-a-gobernar-buenaventura-los-retos-que-asume-hugo-vidal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Buenaventura.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

Las elecciones del pasado 27 de octubre, posicionaron en Buenaventura a **Victor Hugo Vidal Piedrahita**, líder del Paro Cívico de 2017 y candidato por la coalición 'Buenaventura digna', como nuevo alcalde electo. Una decisión que denota el interés de la población bonaverense de votar por opciones lejanas a las maquinarias políticas y que permite plantear los retos que tendrá que asumir Vidal en una región que ha sufrido un  abandono histórico por parte del Estado y que a través de acciones ciudadanas viene buscando una renovación.

### El reto de enfrentar la corrupción

Saulo Quiñones, Eliécer Arboleda Torres, José Félix Ocoró Minotta y Bartolo Valencia son cuatro de los alcaldes que en siete años han sido detenidos en medio de escándalos y cuestionamientos por malversación de recursos en el municipio, ante estos antecedentes, surge la inquietud de cómo garantizar que Víctor pueda cumplir su mandato a cabalidad sin verse inmerso en la corrupción que ha sido una de las más grandes problemáticas del departamento, a lo que Javier Valencia, líder del paro cívico de Buenaventura afirmó que Vidal actuará con transparencia.

"De lo que estoy seguro es que al venir acompañado de una procedencia ciudadanía y  de un pueblo,  no creo que sea su caso, va a  tramitar las cosas con transparencia, no permitiremos que suceda" asegura el líder cívico, quien resaltó la importancia de un pueblo que respalde y acompañe a sus líderes.

"Es la corrupción la que empuja a los alcaldes a ser llevados presos" manifiesta Víctor Vidal, alcalde electo, una afirmación que respalda Valencia al apuntar que este delito será uno de los retos a enfrentar durante su mandato, "el primer reto es la transparencia de cara al pueblo, hoy Colombia está cansada de líderes que se roban la voluntad del pueblo para sus propios intereses".

### La Buenaventura que encontrará Víctor Vidal 

Vidal expresó que es consciente del déficit financiero y la dificultad en temas como el recaudo de impuestos; sin embargo, señala que lo esencial es resolver cómo la estructura administrativa de la Alcaldía responderá a lo que requiere la comunidad en términos de educación y salud. [(Lea también: Si el Gobierno no cumple retomaremos el paro cívico: habitantes de Buenaventura) ](https://archivo.contagioradio.com/si-el-gobierno-no-cumple-retomaremos-el-paro-civico-habitantes-de-buenaventura/)

Por otro lado, algunos sectores han denunciado que se busca sabotear la tarea de la Comisión Escrutadora en Buenaventura, esto con la intención de favorecer otras candidaturas que ahora están pidiendo se realice un conteo mesa por mesa, al respecto Vidal manifestó que no existe ninguna posibilidad de que se haya orquestado un fraude, pero que si existen quejas, estas pueden ser presentadas de manera formal como dicta cada proceso.

**"Sabemos que el tema de seguridad es bravo, el domingo se partió la historia política de Buenaventura, y llega un alcalde que ha dicho y ha hecho cosas diferentes, somos conscientes de los riesgos"** afirmó Vidal que reiteró que llega a la Alcaldía sin compromisos con particulares y con el único interés de mejorar la situación de la comunidad.

Adicionalmente Valencia señaló que el reto también lo debe asumir la comunidad de Buenaventura para ejercer su derecho democrático, su soberanía y asegurarse que dentro del plan del Gobierno que ha expuesto Vidal, que a la final es el pliego de soluciones que se ofreció durante el paro, se cumpla.

> "Hay que pedirle al pueblo que tenga paciencia, que en 4 años no se va a cambiar el abandono histórico de 400 años, esto no lo va cambiar un mandatario sino un pueblo que sea veedor de las decisiones que toma" -  Javier Valencia

### El Comité sigue siendo un movimiento cívico 

Javier Valencia aseveró que la ambición del paro cívico nunca tuvo un propósito electoral y que siempre se ha buscado realizar veeduría ciudadana, "este proceso va a seguir siendo cívico, no perdemos la capacidad de manifestarnos, lo más importante es el deseo de que las cosas cambien en Buenaventura".

Al respecto, Vidal expresó que como alcalde tendrá que buscar la forma de articular el proceso del Paro Cívico en función a los acuerdos a los que se llegó con el Gobierno,  facilitar que se cumplan y mantener una relación muy buena con los demás líderes del movimiento, dejando claro que tanto la Alcaldía como el comité del Paro son instancias separadas.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
