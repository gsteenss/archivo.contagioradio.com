Title: Tortugas ¿Cuál es su importancia y que hacer para protegerlas?
Date: 2018-08-03 11:08
Category: Voces de la Tierra
Tags: Ambiente, Animales, protección
Slug: tortugas-marinas-peligro-extincion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/tortuga__1280x720-e1533245648620.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: National Geographic 

###### 2 Ago 2018 

Año tras año, las acciones humanas vienen poniendo en peligro la superviviencia de las tortugas marinas y de río en todo el mundo, **es así como especies que han viajado a través de los océanos del planeta por cerca de 150 millones de años, están ahora en riesgo de desaparecer,** desequilibrando de paso los ecosistemas en que habitan.

¿Cuál es su importancia en el ciclo de vida acuático? ¿Que hay que hacer para protegerlas? ¿Quienes trabajan en su preservación?. Para responder algunas de estas preguntas, en Voces de la tierra nos acompañaron desde Medellín **Karla Georgina Barrientos**,  Co-Fundadora y Directora Científica de la Fundación Tortugas del Mar y en el estudio **Diana del Pilar Ramirez Acosta,** Diseñadora Industrial, especialista en Educación y Gestión Ambiental y Co Fundadora de Fundación Tourtugas.

<iframe id="audio_27552266" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27552266_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
