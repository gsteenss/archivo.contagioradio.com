Title: Amenazas y extorsiones en municipios con influencia de Anglogold Ashanti
Date: 2015-11-13 15:32
Category: Ambiente, Nacional
Tags: Alerta humanitaria en Quindío, Alerta Humanitaria Temprana en defensa de la Vida, Anglogold Ashanti, el Agua y el Territorio, estructuras armadas del norte del Valle y los ‘Rastrojos’
Slug: amenazas-y-extorsiones-en-territorios-explotados-por-anglogold-ashanti
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Si-a-la-vida.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Salmon Urbano ] 

###### [13 Nov 2015] 

[Organizaciones campesinas, ambientalistas y diversos sectores sociales con presencia en el departamento de Quindío declaran **alerta humanitaria en 6 de sus municipios entre ellos Pijao, Buenavista, Córdoba y Génova**, en los que se han venido presentando **asesinatos**, **quemas de fincas**, **extorsiones y robos** que estarían relacionados con el accionar de la multinacional AngloGold Ashanti en alianza con fuerzas ilegales. ]

[En los municipios de **Pijao y Génova** desde hace 2 años se han reportado **asesinatos de campesinos en extrañas circunstancias**, de acuerdo con las viudas o sus familiares la razón puede atribuirse a la **negativa de vender sus propiedades**. En estos mismos municipios han sido quemadas cerca de 3 fincas y en Pijao otras 10 han sido abandonadas.]

[A la lista se suman las **continuas extorsiones** de las que han sido víctimas estas comunidades campesinas, quienes en medio del clima de zozobra contemplan la idea de irse, pues de acuerdo con las organizaciones sociales denunciante poco **importa la cuantía de la extorsión, resulta trascendental el generar miedo y desplazamiento**.]

[Los habitantes de estos municipios denuncian a su vez que el consumo de alucinógenos se ha incrementado de manera alarmante, así como la **presencia de hombres con armas de largo alcance en fincas que han sido expropiadas** a través del testaferrato por medio del que se ha aumentado la concentración de tierras en esta región.]

[Quienes se han atrevido a denunciar estos hechos frente a autoridades y medios de comunicación han sido presionados y revictimizados, presuntamente por integrantes de **grupos armados** que de acuerdo con algunas capturas realizadas están **vinculados con estructuras armadas del norte del Valle y los ‘Rastrojos’**.]

[Según las organizaciones estos hechos suceden en el marco de la entrega del territorio cordillerano a la multinacional **AngloGold Ashanti**, empresa que ha sido señalada en otras partes del mundo por su **vinculación con fuerzas ilegales**, por lo que exigen a la Defensoría del Pueblo hacer las investigaciones y obtener los informes necesarios para declarar una **Alerta Humanitaria Temprana en defensa de la Vida, el Agua y el Territorio**.]
