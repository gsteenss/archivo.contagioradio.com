Title: Ejército Nacional disparó contra Guardia Campesina en Arauca
Date: 2020-04-30 16:52
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Arauca, Ejército, guardia campesina
Slug: ejercito-nacional-agrede-a-guardia-campesina-en-arauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/ejercito.militares.militar.soldado.soldados.afp_0_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Organizaciones de Derechos Humanos en el departamento de Arauca denunciaron agresiones por parte del Ejército Nacional hacia integrantes de la Guardia Interétnica que hace control sanitario en el territorio, en la vereda Babaica, vía alterna al centro poblado El Botalón **municipio de Tame, Arauca**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la **Fundación de Derechos Humanos Joel Sierra**, sobre las 9:30 de la noche del 29 de abril miembros del [Ejército](https://archivo.contagioradio.com/ejercito-busca-incorporar-a-mas-de-14-mil-jovenes-a-sus-filas/)a**l mando del Sargento Mosquera del Batallón Navas Pardo**, llegaron disparando contra los integrantes de la Guardia. ( Le puede interesar: <https://www.justiciaypazcolombia.com/entrada-de-erradicadores-a-comunidades-de-la-zrcpa/)>

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

*"Afortunadamente los guardias se resguardaron en una casa campesina y salieron ilesos del ataque"*, señaló la Fundación; y agregó que el 28 de abril la comunidad también había denunciado intimidaciones a la Guardia por parte de hombres civiles y armados.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Patrullajes de civiles armados atemorizan a población

<!-- /wp:heading -->

<!-- wp:paragraph -->

*"Miembros de la guardia campesina en el punto de control sanitario de la vereda Puerto Miranda en ese mismo sector, **fueron intimidados por parte de hombres de civil y armados que se movilizaban en camionetas, quienes al parecer eran miembros de organismos de inteligencia de la fuerza pública**"*, denunció la Fundación por medio de un comunicado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A su vez hicieron un llamado para que estos hechos no queden en la impunidad y responsaron al Estado *"en cabeza de la fuerza pública Ejército Nacional por lo que les pueda pasar a los integrantes de la guardia y en general a los habitantes del Territorio Campesino Agroalimentario".*

<!-- /wp:paragraph -->
