Title: Sin desmonte del paramilitarismo no habrá Paz con legalidad: Ángela Robledo
Date: 2018-12-18 16:23
Author: AdminContagio
Category: Entrevistas, Nacional
Tags: Angela Maria Robledo, Asesinato a líderes sociales, Paramilitarismo, Paz con Legalidad
Slug: politica-paz-con-legalidad-huele-a-impunidad-angela-maria-robledo
Status: published

###### [Foto: Archivo Particular] 

###### [18 Dic 2018] 

La nueva política "Paz con Legalidad", para la implementación de los Acuerdo de Paz, fue firmada por el presidente Iván Duque, un plan a 10 años con el que, según el primer mandatario, hará la paz con quienes se encuentren en la legalidad. Sin embargo, para la representante a la Cámara Ángela María Robledo a esta política le hace falta lo más importante para garantizar un cambio en el país, **el desmonte de las estructuras paramilitares. **

De acuerdo con la representante, una implementación de los Acuerdos de paz pasa por un verdadero reconocimiento desde este gobierno, la Fiscalía General de la Nación, la Defensoría del Pueblo y la Procuraduría de **la sistematicidad en el asesinato a líderes y líderesas sociales**. Además señaló que las acusaciones que se han hecho desde Ministerios como el de Defensa, evidencian una estigmatización al legítimo derecho a la protesta y al trabajo que realizan quienes defienden los derechos humanos.

"Estamos en una vuelta a la violencia, lo ocurrido en los llanos Orientales ayer demuestra que las masacres están de vuelta y que no es verdad  lo que dice Duque que ha habido un esfuerzo de búsqueda de la paz con legalidad" afirmó Robledo. (Le puede interesar: ["Con nueva cúpula militar uribismo asume control de seguridad y defensa"](https://archivo.contagioradio.com/con-nueva-cupula-militar-el-uribismo-asume-control-de-seguridad-y-defensa/))

### **Paz con legalidad ** 

Robledo recordó que **Duque propuso, cuando era senador de la República, que la cúpula de la FARC se fuera a la cárcel**, en vez de plantear una salida dialogada al conflicto, hecho que para la representante es ejemplo de lo que significaría legalidad para el gobierno actual.

Además señaló que ya hay informes que encienden las alarmas sobre el resurgimiento del paramilitarismo en departamentos como Antioquia, Norte de Santander, Santander, Cauca y Nariño, en donde a pesar de la fuerte presencia militar, no se logra resolver los problemas de fondo, y **por el contrario aumentan las amenazas y hostigamientos a defensores de derechos humanos. **

"Hay estructuras económicas, paramilitares que están tras los asesinatos a líderes. En su primer momento la versión de la Fiscalía es que es un tema de sicariato, pero Aida Abella nos ha recordado que hace 30 años se decía lo mismo con el genocidio de la Unión Patriótica" afirmó la congresista.

<iframe id="audio_30886505" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30886505_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
