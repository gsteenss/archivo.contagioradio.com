Title: Elección de magistrado de la Corte Constitucional es clave para implementación del acuerdo
Date: 2017-05-31 11:14
Category: Judicial
Tags: acuerdo de paz con las FARC, Congreso, Corte Constitucional, Diana Fajardo, Fast Track, Iván Cepeda
Slug: eleccion-de-magistrado-de-la-corte-constitucional-es-clave-para-implementacion-del-acuerdo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/corte-suprema-fiscal-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: el informador] 

###### [31 May 2017]

Luego de la decisión de la Corte Constitucional que limitó el trámite del Fast Track en el congreso, la elección de un nuevo magistrado es clave para la implementación del acuerdo. Dependiendo de la filiación política de **la persona que sea elegida por el congreso se podrían prever nuevas trabas a dicho trámite por las demandas que cursan o por el contrario facilitarlo con decisiones afines.**

Una de las voces de alerta en torno a esta situación fue la de Iván Márquez, jefe de la delegación de paz de las FARC quién afirmó que si se cae la implementación de la Jurisdicción Especial de Paz se estaría poniendo en riesgo el acuerdo y no quedaría más que cárcel, extradición o “un tiro en la cabeza” para los integrantes de esa guerrilla en proceso de tránsito a la vida civil. [Le puede interesar: Corte Constitucional somete al proceso de paz a más obstáculos](https://archivo.contagioradio.com/corte-constitucional-somete-al-proceso-de-paz-a-mas-obstaculos/)

### **Una elección que repercutirá directamente en la implementación del acuerdo de paz** 

Ivan Cepeda, senador del Polo Democrático afirmó que la elección de mañana, en la que el congreso tendrá que definir entre Diana Fajardo, Álvaro Motta y Alejandro Ramelli, es de vital importancia y de mucha responsabilidad, dadas las implicaciones que tiene, no solamente a corto plazo en lo que tiene que ver con la JEP, **sino a largo y mediano plazo ya que se está en un periodo preelectoral** en el que los partidos buscan un equilibrio político de cara a un nuevo gobierno.

Sin embargo, Cepeda da un parte de tranquilidad y afirma que "**Estamos en un momento en el que todas las contradicciones y presiones se dan juntas**" lo cual no quiere decir que el proceso está en crisis, sino que se está atravesando un momento en el que no se debe caer en fatalismos "En situación de crisis me parece terrible caer en un fatalismo o asumir posición de que todo está bien" [Lea también: Corte Constitucional congeló el acuerdo de paz. Entrevista con Enrique Santiago](https://archivo.contagioradio.com/corte-constitucional-congela-acuerdo-de-paz-con-las-farc-enrique-santiago/)

Este proceso tendrá primero una jornada en la que los congresistas escucharán las propuestas de los candidatos y la candidata a la magistratura que se realiza este miércoles y un nuevo escenario que será este jueves 1 de Junio en que se registrará la votación que concluirá con la designación de un nuevo magistrado.

<iframe id="audio_19005403" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19005403_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
