Title: Entre vaivenes es aprobada JEP en Cámara de Representantes
Date: 2017-03-28 13:03
Category: Nacional, Paz
Tags: implementación acuerdos de paz, Jurisdicción Espacial de Paz
Slug: entre-vaivenes-es-aprobada-jep-en-camara-de-representantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/ausentismo-camara.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [28 Mar 2017] 

**Con 95 votos a favor y 6 en contra fue aprobada la Jurisdicción Especial para la Paz, en la Cámara de Representantes**, como parte de las medidas pactadas entre el gobierno y las FARC para cumplir con la implementación de los acuerdos, luego de esto la JEP deberá pasar a sanción presidencial .

Ángela María Robledo, representante a la Cámara por la Alianza Verde, señaló que ha existido una lentitud en puntos en que se ha pretendido re negociar los acuerdos de paz e incumplimientos con lo que se firmó en el Colón, y **agregó que existen reservas frente a las modificaciones de la JEP  y“como quedo finalmente esta reforma a la Constitución”.**

Las reservas a las que la representante se refieren, son la responsabilidad en la cadena de mano y la responsabilidad de terceros en el conflicto armado, en ambas situaciones Robledo y Alirio Uribe presentaron dos proposiciones, la primera **buscaba acotar y precisar la responsabilidad de mando de las Fuerzas Militares, que ha quedado difusa**, encendiendo las alertas de organizaciones defensoras de derechos humanos, la Oficina de Naciones Unidas y de la Corte Penal Internacional. Le puede interesar: ["Si Estado no juzga a empresarios y mandos militares que lo haga la CPI"](https://archivo.contagioradio.com/si-el-estado-no-puede-juzgar-empresarios-y-altos-mandos-militares-que-lo-haga-la-cpi/)

La segunda era sobre la manera en la que quedaron **blindados los terceros involucrados en el conflicto armado**, sin embargo, ninguna de las proposiciones pasó y tanto víctimas como organizaciones sociales han expresado la posibilidad de que la impunidad continúe. Le puede interesar: ["Abril es el plazo para tareas pendientes en Implementación de Acuerdos de Paz"](https://archivo.contagioradio.com/abril-el-mes-para-completar-la-implementacion-de-los-acuerdos-de-paz/)

No obstante, Robledo indicó que aún falta la revisión y aprobación del texto por parte de la **Corte Constitucional, que podría esclarecer estos dos punto**s y la importancia de una movilización social para que no le metan “conejos” a la JEP.

<iframe id="audio_17816841" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17816841_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
