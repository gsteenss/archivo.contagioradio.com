Title: Comenzó aporte de FARC al esclarecimiento de la verdad
Date: 2020-02-18 17:54
Author: CtgAdm
Category: Paz
Tags: comision de la verdad, FARC, Reconciliación, Sistema Integral de Justicia
Slug: aporte-farc-esclarecimiento-verdad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/FARC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: FARC/ @Carlozada\_FARC  

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este 18 y 19 de febrero, 7 de los integrantes del que fue el secretariado de las FARC revelan, ante la **Comisión para el Esclarecimiento de la Verdad**, temas sobre su participación en el conflicto que incluyen: **tierra y territorio, insurgencia, contrainsurgencia, bloques y frentes, su relación con la población civil, sus políticas y una autocrítica sobre su actuar durante la guerra.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Como puntualizó la comisionada Martha Ruíz, la CEV no solo escucha las diversas voces, sino que a sus vez las interroga para luego contrastar la información y llegar a una conclusión que será revelada en 2021 y que establecerá una verdad concreta e histórica que hable sobre hechos y patrones ocurridos en el marco del conflicto. [(Lea también: Diálogo entre antiguos actores armados abrió la puerta a la verdad y la reconciliación)](https://archivo.contagioradio.com/dialogo-entre-antiguos-actores-armados-abrio-la-puerta-a-la-verdad-y-la-reconciliacion/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta jornada de contribución hace parte de una serie de diálogos para abrir la participación en el proceso de esclarecimiento de la verdad, que dio inicio desde el pasado 12 de febrero, con el testimonio del secretario general del Partido Comunista Colombiano Jaime Caycedo, que entregó su versión del conflicto. [(Le puede interesar: El Gobierno respeta la Comisión, pero no la respalda: Francisco de Roux](https://archivo.contagioradio.com/el-gobierno-respeta-la-comision-pero-no-la-respalda-francisco-de-roux/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hemos venido aquí con nuestro compromiso de aportar a la verdad de lo que no nos debió haber pasado en Colombia, de este largo y extenso conflicto que dejó heridas tan profundas en la sociedad colombiana", expresó Rodrigo Londoño. [(Lea también: El Pato, el río que cambió para la paz)](https://archivo.contagioradio.com/el-pato-el-rio-que-cambio-para-la-paz-2/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este paso simboliza para el partido y sus integrantes un instrumento fundamental para iniciar un proceso de reconciliación y, a su vez, envía un mensaje a "los escépticos" del Acuerdo de Paz para que "se sumen a este compromiso y que todos los actores que estuvieron involucrados a este conflicto se acerquen y digan su verdad", afirmó el presidente de FARC. [(Lea también: Exintegrantes de actores armados piden perdón a menores víctimas del conflicto)](https://archivo.contagioradio.com/exintegrantes-de-actores-armados-piden-perdon-a-menores-victimas-del-conflicto/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La dificultad para acceder a la verdad de la Fuerza Pública

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Esta petición se da en medio de un contexto en el que se ha evidenciado por parte de comisionados como Alejandro Valencia, que los comparecientes y actores del conflicto armado no están revelando mayor información ni están aportando la verdad que requiere el país, un ejemplo de ello es el **del general (r) Mario Montoya frente a la Jurisdicción Especial para la Paz** quien en su versión voluntaria atribuyó los casos de ejecuciones extrajudiciales a sus subordinados a quienes tildó de no seguir las ordenes correctas por ser de un estrato más bajo.[(Lea también: Declaraciones de Mario Montoya ponen tierra de por medio entre él y soldados)](https://archivo.contagioradio.com/mario-montoya-jep/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez, la Comisión había alertado semanas atrás sobre la dificultad que existía para acceder a información militar reservada, **pese a que los protocolos estarían listos hace meses y la petición hecha desde hace un año,** el Ministerio de Defensa señala que no ha encontrado un espacio para realizar las firmas y el protocolo para la entrega de documentos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **F**ARC una de las piezas para armar el rompecabezas de la guerra

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Los comparecientes de FARC, fueron enfáticos en que, pese a que van a exponer unos temas en específico, no se abordarán casos puntuales. **"Fuimos un actor relevante del conflicto y lo que estamos haciendo hoy es aportar una ficha, la que nos corresponde a nosotros y a nuestra participación como insurgencia"**, agregó Carlos Lozada.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El padre Francisco de Roux, director de la CEV, destacó la voluntad de los integrantes del [Partido](https://twitter.com/PartidoFARC) para continuar con el trabajo de la Comisión y afirmó que su voz será escuchada como la de los diversos sectores que han participado o han sido víctimas del conflicto, "queremos entender la racionalidad de ellos, cuáles eran sus propósitos y así la Comisión va a llegar a tomar puntos concreto y a dar afirmaciones concretas sobre hechos muy duros de Colombia".

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
