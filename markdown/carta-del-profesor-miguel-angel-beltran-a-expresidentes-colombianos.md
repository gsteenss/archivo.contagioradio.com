Title: Carta del profesor Miguel Angel Beltrán a expresidentes colombianos
Date: 2015-05-03 06:58
Author: CtgAdm
Category: Nacional, Otra Mirada
Slug: carta-del-profesor-miguel-angel-beltran-a-expresidentes-colombianos
Status: published

Con motivo de la  "**Declaración de Panamá sobre Venezuela**", el sociólogo, historiador y actual **preso político colombiano, Miguel Ángel Beltrán**, responde a los ex Presidentes colombianos que aparecen firmando la declaración. El escrito titulado "Carta abierta de un preso político colombiano a los ex presidentes Belisario Betancur, Andrés Pastrana y Álvaro Uribe"  fue entregado en exclusiva a Contagio Radio para su amplia difusión.

Recordemos que el 10 de abril de 2015 y en el marco de la VII Cumbre de las Américas un grupo de ex Jefes de Estado dio a conocer el texto de la declaración en el que señalan estar "preocupados por el curso que toma la grave alteración institucional, política, económica y social que afecta sin distinciones al pueblo  venezolano."

[Carta de un Preso Político colombiano a los expresidentes Betacur, Pastrana y Uribe](https://es.scribd.com/doc/263690387/Carta-de-un-Preso-Poli-tico-colombiano-a-los-expresidentes-Betacur-Pastrana-y-Uribe "View Carta de un Preso Político colombiano a los expresidentes Betacur, Pastrana y Uribe on Scribd") by [Contagioradio](https://www.scribd.com/contagioradio "View Contagioradio's profile on Scribd")

<iframe id="doc_16070" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/263690387/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-bqF25D3WbDQmy4p6Hcf3&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>
