Title: Universidad pública: Otro escenario donde se vive el conflicto armado
Date: 2019-07-30 16:40
Author: CtgAdm
Category: DDHH, Educación
Tags: comision de la verdad, conflicto, JEP, Universidad pública
Slug: universidad-publica-conflicto-armado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/CONFLICTO-ARMADO-EN-LAS-UNIVERSIDADES.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ACEU UPN  
] 

Este martes fue presentado ante la Jurisdicción Especial para la Paz (JEP) el informe "Universidades Públicas Bajo Sospecha: Represión estatal a estudiantes, profesorado y sindicalistas en Colombia (2000 – 2019)" elaborado por la Asociación Jorge Adolfo Freytter Romero (AJAFR). En el documento se revelan las formas en que **la violencia avanzó sobre la universidad pública en Colombia,** afectando las distintas formas de expresión y generando persecución contra estudiantes, trabajadores y profesores.

### **Universidad Pública: Botín de guerra y bastión para políticos corruptos** 

**Martha Ruiz de la Comisión para el Esclarecimiento de la Verdad (CEV),** señaló que el documento referencia algunos hechos que son conocidos por la opinión pública: que las universidades han sido territorios donde el conflicto se ha expresado de muchas formas, viendo agudizada esta expresión entre la década de 1980-1990; en algunos centros de estudio el control de grupos paramilitares, especialmente en el Caribe, generó amenazas, exilios y asesinatos como el del profesor Jorge Adolfo Freytter Romero.

Según lo referenciado en el documento, entre 1996 y 2003, **"la infiltración paramilitar en diferentes centros de la Costa segó la vida de 17 profesores y 20 estudiantes"**; tomando en cuenta 3 casos de la Universidad del Magdalena, 10 de la Universidad de Córdoba, 5 en la Universidad Popular de Cesar y 19 en la Universidad del Atlántico. Adicionalmente, la Comisionada Ruiz sostuvo que gracias a procesos judiciales también se ha evidenciado que hubo una toma "incluso desde la política, porque las universidades también fueron botines de guerra"; esto, porque en ocasiones eran la entidad que más recursos manejaba en los departamentos.

Así como los campus no fueron ajenos del conflicto y la violencia que se vivía en el país, tampoco escaparon de la corrupción en la política; es así como el documento señala que "la Universidad del Atlántico en el periodo de 1998 al 2003 afrontó una intromisión fuerte en sus órganos administrativos". De acuerdo al testimonio del presidente de la Asociación Sindical de Profesores Universitarios del Atlántico **(ASPU-Atlántico), Walberto Torres Mármol,** "del Fondo de Pensionados **de la Universidad se extraviaron más de 156 millones de pesos** supuestamente destinados a subir auxilios, aniversarios y eventos internacionales".

### **Lo vivido fue un atentado a la democracia, la libertad de expresión y pensamiento**

En la nota de la CEV sobre el informe, el organismo resalta la idea de blindar las Universidades de la repetición de este tipo de hechos, pues como lo dijo la Comisionada, "esto fue en detrimento de la libertad de expresión y pensamiento", que es finalmente la esencia de la educación: la posibilidad de ser crítico. Sin embargo, reconoció que la violencia no se ha erradicado del país, y "hay muchos riesgos de repetición". (Le puede interesar: ["Universidad del Atlántico de nuevo bajo la mira del terror"](https://archivo.contagioradio.com/universidad-del-atlantico-mira-terror/))

Por lo tanto, declaró que es urgente que las universidades generen un clima de paz institucional; trabajen por lograr un diálogo social y político para crear pactos de convivencia entre los diferentes actores que tienen lugar en los campus. En esa medida, resaltó que es urgente que en estos espacios se puedan discutir todas las ideas, incluso las más radicales. (Le puede interesar: ["Con panfletos amenazan a líderes estudiantiles en Universidad de Antioquia"](https://archivo.contagioradio.com/con-panfletos-amenazan-a-lideres-estudiantiles-en-universidad-de-antioquia/))

### **Es necesario hacer un debate sincero y reflexionar sobre la vulnerabilidad de las Universidades**

Para avanzar en la construcción de esa nueva cultura política de convivencia, la Comisionada sostuvo que lo primero que se debe hacer es un "proceso de sinceramiento", para hablar de manera profunda sobre lo que ocurrió durante el conflicto. Ello invitaría a construir la verdad desde los estudiantes, maestros, trabajadores y quienes usaron la violencia, lo que debería llevar a la conclusión de que "la violencia no tiene nada que hacer en una universidad". (Le puede interesar: ["Universidad Pedagógica aparece con graffitis de las AUC"](https://archivo.contagioradio.com/universidad_pedagogica_grfitis_auc/))

En segundo lugar, Ruiz aseguró que se debía hacer una reflexión sobre la vulnerabilidad de las universidad, en términos de cómo los órganos directivos "facilitan la interacción con grupos armados"; a ello agregó que era necesario un planteamiento institucional que permita la libertad de pensamiento, 'cultivado' desde las propias instancias de gobierno de las universidades,  porque "en la medida que sean más pluralistas, eso contribuirá enormemente a legitimar el debate".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_39186774" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_39186774_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
