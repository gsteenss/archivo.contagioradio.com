Title: Con represión quieren detener al movimiento estudiantil
Date: 2019-10-03 21:44
Author: CtgAdm
Category: Estudiantes, Movilización
Tags: ESMAD, estudiantes, protesta, universidad
Slug: saldo-catastrofico-de-operacion-del-esmad-contra-protestas-y-universidades
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Unirse-contra-los-poderosos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @SemanarioVOZ  
] 

El pasado martes 1 de octubre la Red Universitaria Distrital de Derechos Humanos convocó a una rueda de prensa para referirse a la jornada de protestas desarrollada durante la última semana de septiembre en Bogotá, en la que se presentaron diferentes hechos donde se evidenció el exceso de violencia por parte de la fuerza pública. Sin embargo, también se resaltó la solidaridad que mostraron universidades públicas y privadas cuando tuvieron que enfrentar la represión.

### **Universidades de Cundinamarca y Minuto de Dios en Soacha** 

En la rueda de prensa estuvieron delegados de derechos humanos de las principales Universidades afectadas en el centro del país; la primera que vivió las primeras instituciones donde se evidenciaron los excesos de la policía fue en la Universidad de Cundinamarca y la Universidad Minuto de Dios de Soacha, Cundinamarca. La delegada recordó que el pasado 4 de septiembre los estudiantes de la Universidad de Cundinamarca salieron a manifestarse, y la policía intervino.

Ante la necesidad de buscar refugio, algunos de ellos llegaron a la sede de la Universidad Minuto de Dios en este municipio, donde los uniformados llegaron e intentaron ingresar de forma irregular al campus, ante lo que "el rector se ve obligado a cerrar las puertas" de la Institución**. En el lugar se hizo presente el Grupo de Operativos Especiales (GOES) de la policía, unidad que no está ideada para actuar en el marco de la protesta social**; y tras los hechos, se presentó un herido de bala en la pierna.

### **Universidad Distrital, corrupción y represión** 

Desde el lunes 23 de septiembre se plantearon protestas para manifestarse contra los casos de corrupción investigados por la Procuraduría, para ello, se realizaron plantones que fueron reprimidos por el ESMAD. Posteriormente, a lo largo de la semana se presentaron distintas formas de movilización en las sedes de la Universidad en las que integrantes de la Red evidenciaron el uso de elementos no convencionales por parte de la fuerza pública, la violación a la autonomía universitaria en la sede Vivero, y el uso desmedido de la fuerza en la Macarena, lo que causó heridas graves en el ojo de un estudiante.

(Le puede interesar: ["Estudiantes de la Universidad Distrital tienen razones de sobra para protestar"](https://archivo.contagioradio.com/estudiantes-de-la-universidad-distrital-tienen-razones-de-sobra-para-protestar/))

### **Universidades Javeriana y Colegio Mayor de Cundinamarca** 

Tras ser testigos de la represión sufrida por los estudiantes de la Universidad Distrital, integrantes de la comunidad académica de la Pontificia Universidad Javeriana (PUJ) llamaron a concentraciones sobre la carrera 7ma el 23 y 24 de septiembre como forma de solidarizarse, tras las que se presentaron intervenciones del ESMAD. La delegada de DD.HH. de la PUJ denunció que a lo largo de estas jornadas se evidenció "un abuso de autoridad y uso desproporcionado de la fuerza, así como el uso de material no convencional lanzado contra estudiantes".

La delegada además señaló que los días 25 y 27 de septiembre se presentó el ingreso de uniformados al campus de la institución. Producto de la intervención del ESMAD se presentaron más de **10 personas heridas y 7 detenidas**, aunque todas fueron dejadas en libertad "dadas la ilegalidad de la detención, pero también la ausencia de material probatorio". (Le puede interesar: ["A la calle, universidades en Bogotá se unen por el desmonte del ESMAD"](https://archivo.contagioradio.com/a-la-calle-universidades-en-bogota-se-unen-por-el-desmonte-del-esmad/))

El jueves 26 de septiembre se manifestaron estudiantes de la Universidad Colegio Mayor de Cundinamarca en el centro de Bogotá, en rechazo a las acciones del ESMAD con los estudiantes de la Universidad Distrital y la Javeriana. Tras la realización de un plantón el Escuadrón atacó a los jóvenes, dejando un saldo de 4 personas detenidas, que fueron dejados en libertad pero fueron golpeados por los uniformados. Según integrantes de derechos humanos: **"partieron una silla contra la espalda de uno de ellos que era menor de edad"**, y de acuerdo al informe médico realizado por funcionarios de la Cruz Roja, a 2 de los 4 estudiantes les causaron traumas en tabique y rodillas.

### **La Universidad Nacional: Una nueva agresión contra la misión médica** 

El mismo día jueves 26 de septiembre, estudiantes de la Universidad Nacional realizan una movilización en el interior del campus de la institución que intentó convertirse en plantón sobre la calle 26, pero es inmediatamente atacada por el Escuadrón. Tras la intervención, se genera una confrontación en medio de la cual los estudiantes denunciaron que los agentes del ESMAD agredieron con gases y bombas aturdidoras una ambulancia que atendía a heridos en el lugar.

### **Universidad Pedagógica Nacional: Epicentro de un atentado contra los estudiantes** 

El delegado de DD.HH. denunció que las explosiones ocurridas el miércoles 25 de septiembre fue "un atentado contra los y las estudiantes de la Universidad Pedagógica Nacional (UPN)". Ese día, sobre las 4:30 de la tarde, "dos personas que no habían sido vistas en la movilización se acercan con una bolsa transparente a una de las canecas que estaba prendida (en llamas) y suele ser usada para resguardarse de los gases, e introducen una bolsa pidiendo a las personas que estaban alrededor que se muevan del lugar porque 'esto va a estallar'".

Las personas se retiraron del lugar pero la bolsa no explota inmediatamente, **instantes después, las personas se acercan y explota la bolsa, "utilizando la caneca como metralla contra los estudiantes".** Minutos antes, "un artefacto explosivo fue lanzado desde afuera de la Universidad, desde el lugar donde el ESMAD contiene las manifestaciones". (Le puede interesar: ["Explosión en la Pedagógica habría sido por artefactos lanzados desde afuera"](https://archivo.contagioradio.com/explosion-en-la-pedagogica-habria-sido-por-artefactos-lanzados-desde-afuera/))

Ambas explosiones dejaron dos heridos que tuvieron serias afectaciones en sus miembros inferiores y superiores, adicionalmente, cerca de la noche del mismo miércoles se presentó el ingreso del Cuerpo Técnico de Investigación (CTI) de la Fiscalía, y la fuerza pública al campus. "Un ingreso que no fue autorizado por el rector", y por lo tanto, para los estudiantes pone en duda la legalidad y credibilidad de las investigaciones producidas por este cuerpo investigativo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
