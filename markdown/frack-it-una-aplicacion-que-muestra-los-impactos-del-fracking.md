Title: Frack it, una aplicación que muestra los impactos del Fracking
Date: 2016-01-18 14:20
Category: Ambiente, Nacional
Tags: Frack it aplicación movil, fracking, Fundación Heinrich Böll México
Slug: frack-it-una-aplicacion-que-muestra-los-impactos-del-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/frack-it.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [18 Ene 2016]

En búsqueda de nuevas alternativas para informar a la gente, en particular a los más jóvenes,  sobre  los procesos del Fracking, sus impactos sobre el ambiente y las personas, integrantes de la filial de la Fundación Heinrich Böll en México, desarrollaron la aplicación para dispositivos móviles “Frack it”.

Los materiales informativos elaborados por la fundación, tales como videos, libros y cartillas entre otros, serán complementados por la aplicación diseñada con todas las características de un juego que incluye tarjetas informativas y un cuestionario que ayudara al usuario a comprender con mayor facilidad como se realiza la extracción y la afectación que ocasiona en las comunidades.

El aplicativo, disponible en las tiendas Android y iOS para celular y Tablet de manera gratuita, permite al usuario obtener puntos con los que irá avanzando en la medida en que se explote un terreno, utilizando la técnica con mayor velocidad, incrementando gradualmente los daños en el ambiente, el agua y las personas.

<iframe src="https://www.youtube.com/embed/V0wj5Y2YDu4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Las iniciativas contra el uso del fracking, apoyadas y promovidas por la Fundación, son consecuentes con los pronunciamientos que alrededor del mundo diversas organizaciones vienen realizando y que han tenido eco en regiones donde la técnica es implementada, así como en aquellas que aún se encuentra en fase exploratoria. (Lea también E[n Cop 21 Organizaciones sociales piden que se prohíba el Fracking](https://archivo.contagioradio.com/en-cop21-organizaciones-sociales-piden-que-se-prohiba-el-fracking/)).

El equipo de trabajo de “Frack it” está compuesto por Caroline Schroeder y Dolores Rojas en la parte de investigación y contenido, Jorge Aurelio Alvarez en el diseño y la dirección creativa, Diana Karina Ortiz y Juan Carlos Vargas Martínez en la programación con asistencia de Jonathan Mauricio Montaño y en el sonido Jaime Hiram Cuevas.

**Sobre el Fracking**

<u>D</u>urante el ciclo de extracción, procesamiento, almacenamiento, traslado y distribución de los hidrocarburos no convencionales extraídos vía fracking, se libera metano que, como gas de efecto invernadero, es 87 veces más activo que el dióxido de carbono en un margen temporal de 20 años, lo que provoca mayor calentamiento global, contaminación del agua,  y afectaciones directas en la salud humana.
