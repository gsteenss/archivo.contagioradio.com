Title: El mensaje de despedida del diputado gay amenazado en Brasil
Date: 2019-01-26 14:41
Author: AdminContagio
Category: El mundo, Otra Mirada
Tags: bolsonaro, Brasil, LGBTI
Slug: amenazas-diputado-gay-brasil
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/FB_IMG_1548463892081.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 

###### 25  Ene 2019 

Luego de recibir amenazas en su contra, **el primer diputado abiertamente homosexual de Brasil Jean Wyllys, aseguró que no asumira su tercer periodo** como representante por el Estado de Río de Janeiro, en un contexto de persecución contra la comunidad LGBTI que se ha agudizado **desde la llegada de Jair Bolsonaro a la presidencia**.

Wyllys, quien hace parte del partido PSOL-RJ, escribió en sus redes sociales "**Preservar la vida amenazada es también una estrategia de la lucha por días mejores, hemos hecho mucho por el bien común, y haremos mucho más cuando llegue el nuevo tiempo. Gracias a todas y todos ustedes, de todo corazón, Axé”.**

El diputado no se presentará para asumir funciones el próximo 1 de febrero, según anunciaron fuentes cercanas **será remplazado por el concejal carioca David Miranda**. A través de sus redes personales Wyllys publicó una carta dirigida a su circulo cercano y quienes lo acompañaron en su causa por la defensa de la comunidad LGBTI en Brasil.

**Carta de despedida**

**“Queridas compañeras y queridos compañeros, me dirijo hoy a ustedes, con dolor y profundo pesar en el corazón, para comunicarles que no tomo posesión en el cargo de diputado federal para el que fui elegido el año pasado. (…) Tengo conciencia del legado que estoy dejando al partido y al Brasil, especialmente en lo que se refiere a las llamadas “pautas identitarias” (en realidad, las reivindicaciones de minorías sociales, sexuales y étnicas por ciudadanía plena y estima social) y de vanguardia, que están contenidos en los proyectos que presenté y en las banderas que defiendo; le cuento con ustedes para dar continuidad a esta lucha en el Parlamento”**, escribió Wyllys en la carta de despedida, fechada el 23 de enero de 2019. **“No dejo el cargo de manera irreflexiva. Fue una decisión pensada, ponderada pero sufrida, difícil. Pero el hecho es que llegué a mi límite. Mi vida está, desde hace mucho, por la mitad; quebrada por las amenazas de muerte y la pesada difamación que sufre desde el primer mandato”**.

El ahora exdiputado manifestó que las amenazas se han extendido a su familia y amigos y tienen sustento para ser tomadas en serio: **“Tuve la convicción de que no podía-para mi salud física y emocional y de mi familia- continuar viviendo de manera precaria y por la mitad, fue la semana en que las noticias comenzaron a desnudar la planificación cruel e inaceptable de la brutal ejecución de nuestra compañera y mi amiga Marielle Franco. Vean, compañeras y compañeros, estamos hablando de sicarios que viven en Río de Janeiro, estado donde vivo, que asesinaron a una compañera de luchas, y que mantiene vínculos estrechos con personas que se oponen públicamente a mis banderas e incluso a la propia existencia de personas LGBT. Ejemplo de ello fue el aumento en los últimos meses del índice de asesinatos de personas LGBT en Brasil”**.

(Le puede interesar: [Hijo de tigre... las cuestionadas acciones de Flávio Bolsonaro](https://archivo.contagioradio.com/relaciones-salpican-flavio-bolsonaro/))
