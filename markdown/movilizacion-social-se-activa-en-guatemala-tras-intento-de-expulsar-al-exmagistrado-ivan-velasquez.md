Title: Hay más de 100 investigaciones por corrupción que adelanta Iván Velásquez en Guatemala
Date: 2017-08-28 20:56
Category: DDHH, El mundo
Tags: Guatemala, Iván Velásquez, Jimmy morales
Slug: movilizacion-social-se-activa-en-guatemala-tras-intento-de-expulsar-al-exmagistrado-ivan-velasquez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/cicig-e1503953023205.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [PeriódicoelMirador]

###### [28 Ago 2017] 

*“*Iván se queda” ha sido una de las arengas con las que miles de personas en Guatemala desde el pasado jueves vienen exigiendo que el exmagistrado y titular de la Comisión Internacional Contra la Impunidad en Guatemala (Cicig), Iván Velásquez no sea expulsado del país, y con ello se continúen las investigaciones por corrupción, que **ya suman más de 500 capturas y otras 100 citaciones pendientes entre las que se encuentran diputados y el propio presidente Jimmy Morales.  **

“Declaro non grato al señor Iván Velásquez (...) Ordeno que abandone inmediatamente la República de Guatemala", anunció la semana pasada el presidente Morales. Ante ello, Claudia Samayoa, defensora de Derechos Humanos de ese país, considera que lo que se busca es detener las investigaciones que hace dos años promovieron una gran protesta nacional que acabó en la renuncia del entonces presidente, Otto Pérez Molina, y su exvicepresidenta Roxana Baldetti, después de que el Congreso les retirara la inmunidad y emitiera una orden de captura en su contra.

Hoy esas investigaciones señalan, según Samayoa, que pese a que Morales “subió al poder diciendo que no era corrupto, ni ladrón **las investigaciones desatan que casi el 80% de los gastos de su campaña no fueron reportados”.**

Con esta nueva crisis, la movilización social en Guatemala se ha reactivado, y en la mañana de este lunes, se contaban al menos seis bloqueos en diferentes vías del país. “Quieren remover a Velásquez creyendo que este proceso realmente se detiene (…) lo que ha habido es una estigmatización pero la población se está movilizando por la decisión constitucional que impediría la expulsión del comisionado”, manifiesta la defensora de DDHH.

Morales pretende expulsar al colombiano asegurando que **la Cicig violó la "presunción de inocencia de ciudadanos guatemaltecos"** y el exmagistrado "se inmiscuyó en asuntos internos que competen con exclusividad al Estado de Guatemala" e "intentó presionar a los diputados para la aprobación de reformas constitucionales".

Las reacciones

El portavoz del Departamento de Estado, Heather Nauert de EE.UU defendió la labor del exmagistrado indicando que “ha sido un líder efectivo de la Cicig en su lucha contra la corrupción en Guatemala", y añadió que "**La Cicig ha jugado y debe seguir juntando un importante papel en el fortalecimiento de las instituciones guatemaltecas** y en el derribo de la corrupción”.

Por su parte el portavoz de las **Naciones Unidas en Nueva York, Stéphane Dujarric,** en un comunicado dijo que el secretario general ONU pide respeto para Velásquez que “ha trabajado incansablemente para promover una cultura que defienda el imperio de la ley y rechace la corrupción".

En ese sentido, **Annabella Morfín, Procuradora General de la Nación** guatemalteca, instó al presidente Jimmy Morales, a reflexionar sobre la posible expulsión de Iván Velásquez, y pidió al mandatario "cumplir con las resoluciones emanadas por los órganos jurisdiccionales", ya que la Corte Constitucional tienen la capacidad suspender ese tipo de decisiones.

Finalmente cabe resaltar que esta situación además de reactivar la movilización social, ha ocasionado un conflicto entre la iglesia católica y cristiana. De hecho, Claudia Samayoa afirma que el movimiento cristiano ha intentado detener las movilizaciones, respaldado a Jimmy Morales, que aún como presidente sigue siendo pastor. **Mientras que Conferencia Episcopal ha declarado su rotundo apoyo a al exmagistrado.** El obispo de la Diócesis de Huehuetenango, Álvaro Ramazzini, pidió explicaciones al presidente y lo responsabilizó por la crisis que ha generado en el país, cuestionando los intereses reales que le llevaron a tomar la decisión de expulsar al comisionado Velásquez.

<iframe id="audio_20573920" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20573920_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
