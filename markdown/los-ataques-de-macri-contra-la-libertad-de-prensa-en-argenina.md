Title: Los ataques de Macri contra la libertad de prensa en Argentina
Date: 2015-12-30 09:02
Category: El mundo, Otra Mirada
Tags: Argentina, Ley de Medios, Mauricio Macri
Slug: los-ataques-de-macri-contra-la-libertad-de-prensa-en-argenina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Macri-2-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: radio.uchile.cl 

###### [30 Dic 2015]

El pasado 24 de diciembre el mandatario argentino ordenó la intervención de la **Autoridad Federal de Servicios de Comunicación Audiovisual** (AFSCA) y la Autoridad Federal de Tecnologías de la Información y la Comunicación (AFTIC), entes que regulan las telecomunicaciones y los medios audiovisuales de Argentina y han posibilitado la puesta en marcha de la[Ley de medios](https://archivo.contagioradio.com/ley-de-medios-amordazada-en-argentina/), decretada durante los 12 años de gobierno Kishnerista.

Adicionalmente el decreto también permite que el actual director de la AFSCA, **Martin Sabbatella, sea imputado por no dejar su cargo de manera inmediata a pesar de que su cargo estaba asignado hasta 2017**. Sabatella afirmó que “*lo que está sucediendo es un delirio, una locura, quieren cerrar el organismo y clausurar el funcionamiento de la Ley de Medios*”.

El pasado 22 de Diciembre el presidente **Macri ordenó la cancelación del programa de análisis político “6,7,8”** uno de los más reconocido de Argentina que se emitía a través de la Tv Estatal "*Hernán Lombardi administrará todos los medios públicos. Tendrá rango de ministro y se va a ocupar de todo el sistema de medios. No va a haber 6,7,8 ni 8,7,6*", dijo Macri.

El 26 de Diciembre **Macri ordenó la cancelación de la televisión del Senado** que transmitía las sesiones de la cámara alta de ese país, pero además aportaba en el funcionamiento del canal público “**Parlatino TV**” lo que ha sido considerado por varios congresistas latinoamericanos un ataque a la libertad de información a la que tienen derecho todos los ciudadanos en América Latina. La creación de este canal internacional de todos los congresos de la región, se dio apenas hace unos meses.

Diversos analistas en América Latina y el mundo han hecho eco de las voces de los propios ciudadanos y ciudadanas argentinas que consideran que las medidas de Mauricio Macri por vía Decreto de “necesidad y urgencia” son antidemocráticos puesto que **ninguno de los elementos de dicho decreto fue consultado con el parlamento** y representan importantes retrocesos en materia de derechos.

Lea tambien: *[Las cuentas pendientes de Mauricio Macri ante la justicia Argentina](https://archivo.contagioradio.com/las-cuentas-pedientes-de-mauricio-macri-ante-la-justicia-argentina/)*
