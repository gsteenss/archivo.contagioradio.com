Title: Gobierno y Congreso hacen conejo a Consulta Anticorrupción
Date: 2018-10-24 13:22
Author: AdminContagio
Category: Nacional, Política
Tags: Alberto Carrasquilla, Consulta anticorrupción, Katherine Miranda, Moción de Censura
Slug: consulta-anticorrupcion-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/consulta-anticorrupcion.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @PartidoVerdeCol] 

###### [19 Oct 2018] 

Recientemente dos hechos relevantes han ocurrido en la Cámara de Representantes: por una parte, el veto a la palabra que tendrían algunos congresistas para intervenir en el debate por la Moción de Censura contra el ministro Alberto Carrasquilla; y por otra parte, la votación negativa del proyecto anticorrupción, que limitaba el número de veces que un ciudadano puede ser elegido  como congresista.

### **Moción de Censura contra Alberto Carrasquilla iniciará su curso en el Congreso** 

Este miércoles, a las 5 de la tarde iniciará la moción de censura impulsada por un sector del Congreso contra el ministro de hacienda **Alberto Carrasquilla**; durante la sesión, representantes a la cámara esperan respuestas por parte del funcionario sobre los hechos que lo involucran con los llamados 'Bonos del Agua', sin embargo, **no todos tendrían asegurada la posibilidad de intervenir en el debate.**

Según la representante por la **Alianza Verde, Katherine Miranda**, en el debate de esta tarde solamente los voceros de los partidos tendrían asegurada su intervención en el evento, hecho que excluiría a los demás representantes que también quieren manifestar sus inquietudes  frente al ministro Carrasquilla. (Le puede interesar: ["Ernesto Macías está censurando a la oposición: Jorge Robledo"](https://archivo.contagioradio.com/macias-censurando-oposicion/))

Aunque Miranda y otros congresistas no firmaron la carta para que iniciara el proceso de Moción de Censura, asegura que sí están pendientes del mismo; por tanto **esperaban que esta tarde pudieran formular preguntas al Ministro**, procurando que la decisión que tomen la próxima semana cuando se vote la Moción, sea informada. (Le puede interesar: ["El que la hace la paga no aplica para el ministro Carrasquilla"](https://archivo.contagioradio.com/paga-ministro-carrasquilla/))

En consecuencia, la Representante a la cámara está a la expectativa que durante esta tarde se brinden las debidas garantías a la oposición, como para quienes defienden al ministro, de tal forma que él pueda responder a todas y cada una de ellas; incluso, si el debate debe llegar hasta altas horas de la noche. (Le puede interesar: ["La reforma tributaria de Carrasquilla afectaría al 90% de los colombianos"](https://archivo.contagioradio.com/carrasquilla-afectara-colombianos/))

### **"Le están haciendo conejo a 11 millones de personas" **

Este martes **fue hundido en la Cámara de Representantes el  Proyecto de Acto Legislativo que buscaba limitar a el periodo de congresistas a solo 3 ocasiones en cada cámara**; sobre está situación, Miranda señaló que sus colegas "no le están haciendo conejo a Claudia López, ni al Partido Verde, sino que le están haciendo conejo a 11 millones de personas que votaron la Consulta Anticorrupción".

La Representante explicó que no se están respetando las mesas técnicas que se formaron con el Ministerio de Interior y miembros de todos los partidos políticos; puesto que en esos espacios se llega a acuerdos pero cuando se debe votar el proyecto, los congresistas votan de otra manera. Sin embargo, **Miranda anunció que la iniciativa será presentada nuevamente en el Senado de la república**, donde esperan un mayor apoyo de todos los partidos políticos.

Para concluir, la Representante a la Cámara sostuvo que para apoyar las iniciativas anticorrupción que hacían parte de la consulta "necesitamos voluntad del Gobierno Nacional para dar mensaje de urgencia a los proyectos, voluntad de los partidos políticos para que nos apoyen en esta reforma, y el **acompañamiento de la ciudadanía para presionar el apoyo de los congresistas a los proyectos**".

<iframe id="audio_29589323" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29589323_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
