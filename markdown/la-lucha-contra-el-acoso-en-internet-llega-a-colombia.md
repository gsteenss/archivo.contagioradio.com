Title: Se lanza en Colombia plataforma para combatir el acoso en internet
Date: 2019-02-05 10:28
Author: AdminContagio
Category: DDHH, Nacional
Tags: Buen uso de Internet, Difusión no consentida de contenido íntimo, Fundación Karisma
Slug: la-lucha-contra-el-acoso-en-internet-llega-a-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/image-20141210-6060-1jrkhqe-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Confidential 

###### 4 Feb 2019

En el marco del Día Mundial de la Internet segura, el cual se celebra cada 5 de febrero, la Fundación Karisma ha decidido lanzar Acoso.Online, una plataforma virtual que ya funcionaba en diversos países como **Barbados, Argentina. Brasil, Chile, México, Perú y Paraguay** como una guía y apoyo para quienes hayan sido víctimas de pornografía no consentida y que ahora llega a Colombia.

Amalia Toledo, [coordinadora de proyectos de la Fundación Karisma afirma que dentro de la plataforma, sus usuarios podrán encontrar información sobre qué ]hacer en caso de ser víctimas de la difusión no consentida de imágenes con contenido o material sexual o erótico, a qué entidades acudir y qué esperar. Además de aprender a cómo denunciar dicho material en redes sociales **también encontrarán información legal de cada uno de los países que trabajan en el proyecto y que en la actualidad suman 14 naciones.**

[Según la información recolectada en ‘Pornografía no consentida: ¿Cómo responden las plataformas privadas de internet a las usuarias de América Latina?’ texto que podrán encontrar en la nueva web][[(https://acoso.online/co/]](https://acoso.online/co/)[[)](https://acoso.online/co/) **cerca de 3000 sitios** **web publican "pornografía de venganza" a diario,** material que es a su vez distribuido por  redes sociales, blogs, emails y mensajes de texto.]

[La coordinadora de proyectos advirtió que el gran problema que existe en Colombia es que no hay una política pública que atienda este tipo de situaciones, ni formas de sensibilizar a la población que al compartir dichas imágenes sin consentimiento se convierten en replicadores de este tipo de violencia.]

Aunque las denuncias van en aumento, estas solo representan un pequeño porcentaje de todos los casos que existen, pues aún no hay la suficiente confianza para hablar de ello particularmente en las mujeres, quienes son las que más sufren este tipo de violencia al difundirse este tipo de contenido, “ante un suceso como este sienten que no pueden denunciar, porque la sociedad las culpa, sienten que esto afectará su reputación, su vida personal y profesional, hay mucho que hacer, hace falta cambiar la cultura” señala Toledo.

<iframe id="audio_32239775" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32239775_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo]
