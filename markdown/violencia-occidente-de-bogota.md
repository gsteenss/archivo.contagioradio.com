Title: ¿Qué está detrás de los hechos de violencia al occidente de Bogotá?
Date: 2019-07-24 12:14
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: Aguilas Negras, Engativá, Panfletos, Para Policía, Paramilitarismo
Slug: violencia-occidente-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Occidente-de-Bogotá-e1563916496367.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Wikipedia Commons  
] 

En las últimas semanas fueron encontrados el cuerpo desmembrado de una mujer en Engativá, y un hombre en Fontibón, a ello se sumó la aparición de **panfletos firmados por las Águilas Negras,** advirtiendo sobre una limpieza social en el occidente de Bogotá. En su momento, el experto en seguridad Néstor Rosanía, sostuvo que la situación de violencia en Engativá se debe a rencillas entre pandillas, pero otros expertos advierten sobre la existencia de fuerzas parapoliciales en la Capital.  (Le puede interesar:["Hallazgo de cuerpo en Engativá, ¿a qué obedecen estas prácticas de violencia?"](https://archivo.contagioradio.com/hallazgo-de-cuerpo-en-engativa-a-que-obedecen-estas-practicas-de-violencia/))

El profesor y analista de temas de conflicto **Carlos Medina Gallego** afirmó que las Águilas Negras son una razón social "que utilizan fuerzas parapoliciales y paramilitares", y cuentan con servicios de inteligencia que les proporcionan nombres de los líderes sociales en las zonas; ello les permite realizar amenazas y panfletos en distintos lugares del país. En el mismo sentido se han pronunciado diferentes[organizaciones](https://archivo.contagioradio.com/18-grupos-narcoparamilitares/) y estudiosos de temas de seguridad, quienes han señalado el hecho de que las Águilas Negras son, en realidad, un nombre usado por distintas organizaciones para intimidar a determinados sectores de la población.

### **¿Paramilitares o parapolicías en el occidente de Bogotá?**

Medina explicó que en Bogotá se presentan hechos de violencia recurrentes contra población juvenil, "consumidores de droga, contra prostitutas, contra población LGBTI" y han intentado hacer "limpieza social"; de igual forma, se presentan intentos de amedrentar a los líderes sociales que trabajan por sus comunidades. A este fenómeno de violencia contra un sector específico de la población, se suma al aumento de percepción de inseguridad por el incremento en los hurtos o robos.

El analista sostuvo que la presión para mejorar esa percepción de seguridad, unida a la idea de que los grupos juveniles y las poblaciones anteriormente mencionadas son causantes de los hechos de violencia en la ciudad, **estarían permitiendo la aparición de grupos parapoliciales.** La intención de estos sería **"tratar de controlar las situaciones de inseguridad en distintos barrios"**, de acuerdo al académico.

No obstante, advirtió que se debe tener cuidado al tratar este tema, pues se puede generar temor en las personas que habitan la localidad, y ello "permite imponer un sistema de seguridad que no garantiza las libertades que deben tener los ciudadanos para acceder a los espacios públicos". (Le puede interesar: ["Sur de Bogotá: Condenado a recibir la basura de la ciudad por 37 años más"](https://archivo.contagioradio.com/sur-bogota-basura/))

### **Fortalecimiento de lo comunitario e investigación de las amenazas**

Sin embargo, no sería un problema exclusivo de Bogotá; de acuerdo a estudios sobre seguridad adelantados en otras ciudades de latinoamérica por el Profesor, "este fenómeno se viene dando en todos los países y en todas las ciudades de América Latina". Pero en el caso de la capital de Colombia, donde se han emitido al menos cuatro alertas tempranas por presencia de grupos armados y riesgos de la población civil, Medina sostuvo que se debían investigar estas alertas y avanzar en el fortalecimiento comunitario, que es lo que se busca atacar.

A propósito de este tema, la candidata al Concejo de Bogotá por la Unión Patriótica Heidy Sanchez, apoyó lo dicho por Medina, en el sentido de "fortalecer la seguridad entre las mismas comunidades"; y **apoyar las propuestas que impulsan los líderes sociales para dar respuesta a la violencia**. (Le puede interesar: ["Violencia política contra Juntas de Acción Comunal aumentaría al acercarse elecciones regionales"](https://archivo.contagioradio.com/violencia-politica-contra-juntas-de-accion-comunal-aumentaria-al-acercarse-elecciones-regionales/))

Para concluir, Medina aclaró que esta ola de violencia no obedecería al paramilitarismo, pues lo paramilitar "tiene que ver con la lucha contra la subversión y obtener el control político" de la población, su objetivo es el control de organizaciones políticas; en cambio, la violencia parapolicial "son fenómenos de limpieza social y amenazas a los jóvenes, que en visión de estos grupos, son perturbadores de la tranquilidad ciudadana". (Le puede interesar: ["Según CINEP en 2018 fueron victimizados 98 líderes cívicos y comunales"](https://archivo.contagioradio.com/en-2018-fueron-victimizados-98-lideres-civicos-y-comunales-cinep/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38878846" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38878846_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
