Title: En render se quedó la Bogotá de Peñalosa
Date: 2019-05-09 19:23
Author: CtgAdm
Category: Entrevistas, Política
Tags: Metro, Peñalosa, Séptima, Transmilenio
Slug: render-quedo-bogota-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Diseño-sin-título-3.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Este miércoles se desarrolló en el Congreso de la República, el debate de control político al alcalde Enrique Peñalosa, citado por los represenantes a la Cámara Inti Asprilla y Germán Navas,  sobre el sistema de transporte. Sin embargo, el evento fue la oportunidad para hacer una evaluación de la Administración en la Capital y cuestionar el cumplimiento de las promesas de campañas hechas por el Alcalde, que al parecer, siguen en render.

### **Metro sí, pero no así**

En el evento también participaron los congresistas David Racero, Maria José Pizarro y la líder de la oposición Ángela María Robledo, quien reiteró los riesgos y dificultades que se habían anunciado sobre el Proyecto Metro: "es más corto, más costoso, tiene menos estaciones y dejaría a Bogotá con un serio deterioro"; quien también sostuvo que el Alcalde abriría la licitación para el Metro sin los estudios suficientes, y en la medida en que el proceso que actualmente mantiene suspendida la construcción del Transmilenio por la carrera Séptima, sea una decisión definitiva, habría que volver a pensar en esta vía como un corredor para el metro.

Adicionalmente, la líder de la oposición recordó que Peñalosa tiró a la basura los estudios del Metro propuestos por la Alcaldía de Gustavo Petro,  sin tener en cuenta la importancia de este Proyecto para la Capital; ni una de las recomendaciones hechas por el Banco Interamericano de Desarrollo (BID), en donde se sugería una construcción subterránea, por tal motivo, Robledo manifestó que lo correcto era no correr en su licitación y analizar la situación con cabeza fría. En conclusión y en pocas palabras, "metro sí, pero no así".

> En el debate de hoy [@EnriquePenalosa](https://twitter.com/EnriquePenalosa?ref_src=twsrc%5Etfw) me dijo mentiroso.  
> Pues se equivoca. En el Congreso hasta mis contradictores reconocen que yo hago debates con datos y argumentos. Le doy la evidencia del documento del BID que sí existe y usted niega.  
> Se equivoca Alcalde, no soy como usted [pic.twitter.com/M4u6XxeysU](https://t.co/M4u6XxeysU)
>
> — David Racero ?? (@DavidRacero) [8 de mayo de 2019](https://twitter.com/DavidRacero/status/1126219652802256899?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
### **Una es la Bogotá Virtual que el Alcalde sueña, y otra la que tenemos**

Recientemente, nuevas decisiones judiciales han detenido proyectos que para Peñalosa eran claves: El Parque Ecológico del Embalse San Rafael, Transmilenio por la Séptima y la intervención a la Reserva Thomas Van Der Hammen. Para Robledo, estos frenos de la justicia muestran que hay una ausencia en los procesos de planificación y errores en el estudio de la normatividad legal en el uso del suelo por parte de la Alcaldía. (Le puede interesar:["Peñalosa no atiende recomendaciones académicas sobre calidad del aire en Bogotá"](https://archivo.contagioradio.com/penalosa-no-atiende-recomendaciones-academicas-calidad-del-aire-bogota/))

A estos hechos también podrían sumarse la demanda contra la licitación del Metro elevado, y la acción popular instaurada contra la nueva flota de articulados de Transmilenio; que como lo recordó la integrante de la Colombia Humana, no cumple con los estándares establecidos en el acuerdo de París que suscribió Colombia. (Le puede interesar:["Frenar el cambio climático en Bogotá no será posible con la nueva flota de Transmilenio"](https://archivo.contagioradio.com/nueva-flota-de-transmi/))

En esa medida, y tomando en cuenta los proyectos de la Alcaldía en términos de movilidad y  la construcción de otro tipo de obras, Robledo criticó que solo se haya cumplido en un 50% con las metas planteadas por esta Administración en sus 4 años de gestión. Situación que para la líder política evidencia la poca gobernabilidad de Peñalosa, "esta es una ciudad de render, si uno quiere saber cómo es la Bogotá virtual entra a los sitios web de la Alcaldía y encuentra el render de la séptima, del Parque San Rafael y de la Avenida 68... pero la diferencia en esa ciudad digital que sueña Peñalosa y la real es amplia".

### **¿Se usa a la justicia para impedir el desarrollo de proyectos del Alcalde?**

Tras el debate, en diferentes declaraciones Peñalosa manifestó que se estaba haciendo uso de los estrados judiciales como un instrumento para hacer política, en referencia a las diferentes decisiones jurídicas que tienen suspendidas las obras de proyectos relevantes de su administración. Sobre esa afirmación, Robledo sostuvo que el Alcalde "detesta a los políticos, pero cada que necesita aspirar a un cargo los busca".

Adicionalmente, dijo que en lo concerniente a asuntos de la ciudad, se trata siempre de temas políticos, porque involucra pensar el desarrollo de una ciudad: el uso del espacio público, el sistema de educación, el sistema de transporte, las obras sociales que se impulsan. (Le puede interesar: ["Peñalosa gastó 99 mil millones de pesos en Publicidad entre 2016 y 2017"](https://archivo.contagioradio.com/99-mil-millones-penalosa-publicidad/))

> Conclusiones del debate:  
> \[Abro hilo\]  
> I. Peñalosa NO respondió a ninguno de nuestros cuestionamientos.[@intiasprilla](https://twitter.com/intiasprilla?ref_src=twsrc%5Etfw) [@angelamrobledo](https://twitter.com/angelamrobledo?ref_src=twsrc%5Etfw) [@PizarroMariaJo](https://twitter.com/PizarroMariaJo?ref_src=twsrc%5Etfw) [@CamaraColombia](https://twitter.com/CamaraColombia?ref_src=twsrc%5Etfw) [@PoloDemocratico](https://twitter.com/PoloDemocratico?ref_src=twsrc%5Etfw) [pic.twitter.com/0NiB1kv8E3](https://t.co/0NiB1kv8E3)
>
> — Germán Navas Talero ? (@GNavasTalero) [9 de mayo de 2019](https://twitter.com/GNavasTalero/status/1126295193928511488?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
