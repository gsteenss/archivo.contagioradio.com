Title: Colombia y sus dos presidentes: análisis de Fernando Giraldo
Date: 2018-08-08 12:15
Category: Entrevistas, Política
Tags: Centro Democrático, Iván Duque
Slug: colombia-y-sus-dos-presidentes-analisis-de-fernando-giraldo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/ivan-duque.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter Iván Duque] 

###### [08 Ago de 2018] 

El analista y politólogo Fernando Giraldo manifestó que durante la posesión presidencial quedó en cuestión la gobernabilidad del poder ejecutivo en el país, con dos discursos, por un lado, el de Iván Duque y por el otro, el de Ernesto Macías, presidente del senado, **“como si hubiese dos copresidentes de Colombia: el presidente y el copresidente”**.

### **El Copresidente de Colombia** 

Para Giraldo el discurso de Macías, integrante del Centro Democrático, estuvo caracterizado por ser “desubicado, desobligante, insultante y agresivo”. Sin embargo, señaló que la intervención fue la expresión de una de las tendencias al interior de su bancada, y posiblemente la más radical**, liderada por Álvaro Uribe Vélez.**

Hecho que a su vez, comprobó que la posición mediadora de Duque, podría ser una pequeña minoría al interior del partido y que, en realidad, la gran mayoría de esta bancada **“no es simpatizante”**  con el nuevo presidente. (Le puede interesar:["Uribe esta haciendo una tenebrosa campaña contra la Corte Suprema: Iván Cepeda")](https://archivo.contagioradio.com/uribe-esta-haciendo-una-tenebrosa-campana-contra-la-corte-suprema-ivan-cepeda/)

En ese sentido, Giraldo aseguró que el discurso “le marco el terreno a Iván Duque y le dijo: usted haga, si quiere borre la hoja, pero nosotros estamos acá para hacer otra cosa”, afirmación que daría por entendido que el Centro Democrático tramitaría** la gobernanza del país desde el Congreso** y que Duque podría ser un prisionero de su propio partido.

### **Duque presidente** 

De acuerdo con Girlando, las posibilidades que tendrá Duque para hacer efectiva su presidencia se verán sujetadas a la capacidad que tenga para acercarse a líderes político de otros partidos como Cambio Radical, el partido Conservador, entre otros, que le permitan desmarcarse de las intenciones del Centro Democrático.

Asimismo, su gabinete de magistrados tendrá que afrontar, algunos por primera vez, los juegos de la política, y según Giraldo, debido a que la conformación de su equipo de trabajo se hizo a partir de responder a las expectativas de su partido, intentar que no todos fuesen radicales en sus posturas y tratar que fuesen proclives a él, su unidad tendrá que **afrontar tanto los llamados de control político por parte de la bancada de oposición, como los condicionamientos de su propia bancada** y finalmente presiones del mismo que terminen en cambios de ministros.

No obstante, Giraldo recordó que el Centro Democrático solo es el 16% del Congreso de la República y que no se puede creer que son “la primera fuerza” política del país, razón por la cual necesitarán obligatoriamente alianzas con otros para llegar a ser mayoría, y dejar de lado la "arrogancia" con la que han actuado hasta el momento.

De igual forma, aseveró que es la primera vez en la historia del país que los sectores de izquierda tienen tantas **oportunidades para gestar un bloque de unidad que tenga la capacidad de disputarse un proyecto de país**.

<iframe id="audio_27703452" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27703452_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
