Title: ¿Protesta social sin vías de hecho?
Date: 2016-06-15 12:33
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: Minga Nacional, Paro Agrario, paro campesino
Slug: protesta-social-sin-vias-de-hecho
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/paro-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Germán Osorio Arias 

#### **[Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

###### 15 Jun 2016 

El cargo de procurador general de la nación, es un cargo que requiere el menor sesgo político, moral e ideológico posible. Es un cargo que no puede ejecutarse, actuando contundentemente contra unos, pero siendo permisivo o buscando excusas llenas de palabrería legalista con los otros.

[La última con la que salió el procurador, a propósito del paro y minga agrarios, fue la siguiente: “]*[para eso existe el Esmad, para que actúe, tiene límites y controles, debe actuar prudentemente sin afectar derechos, pero no se puede negociar la presencia del Esmad, debe estar presente y ser instrumento de la legalidad para garantizar los derechos de los ciudadanos]*[”.]

[El gran problema es que el Esmad, ha estado implicado en varios asesinatos de ciudadanos, utilización de armamento hechizo (recalzadas) uso desproporcionado y asimétrico de la fuerza a la hora de disolver una marcha, etc. Cuando un supuesto instrumento de la legalidad, se constituye en un instrumento de la criminalidad sistemática, entonces la gente tiene todo el derecho de no considerar legítima dicha legalidad, hasta tanto no se exprese coherentemente. Ordoñez, no obstante, continuaba diciendo “]*[la protesta tiene límites y los límites están trazados por el interés general, el bien común, los derechos de los otros ciudadanos]*[”.]

[¿De qué límites a la protesta hablará Ordoñez? ¿acaso se referirá a esos límites en los que la gente marcha o se reúne y no es atendida por ninguna entidad o funcionario estatal? ¿ese es el límite permitido señor Ordoñez? ¿el límite donde no se afecte la tranquilidad de aquellos que no conocen cómo llega una fruta al supermecado? ¡¡¡De qué interés general podrá estar hablando Ordoñez, cuando él, como procurador, no representa el interés general de ni siquiera la totalidad de los funcionarios del Estado, sino solamente el de aquellos que compartan en gran medida su ideología!!!]

[El interés general en Colombia, el bien común de los colombianos, es por principio obvio, el derecho a estar vivos para tener la barriga llena, ¡es comer! y la comida no “nace” en los supermecardos, la comida sale de la tierra, del cultivo de verduras y frutas, de la cría de animales, del esfuerzo de muchos y muchas en los campos del país. Por lo tanto, el interés de los campesinos e indígenas, es sin lugar a dudas la representación directa e indirecta del interés general en Colombia.]

[Entonces, la protesta es un derecho, pero no hay derecho a que se incumplan los acuerdos que hayan surgido en ocasiones anteriores. La protesta es un derecho, pero no hay derecho a que un ministro de agricultura ¡¡¡un funcionario público como Aurelio Iragorri!!! le dijera en una ocasión a los manifestantes, que él por allá no iba, que sí querían negociar mejor fueran a su oficina… la gente le llegó a la oficina, se tomó el edificio del ministerio… y ahí sí, los medios masivos de siempre hablaron incluso de secuestro.]

[La protesta es un derecho, pero en Colombia para nadie es un secreto que, sin las vías de hecho, los funcionarios públicos no aparecen, se pasan la pelota los unos a los otros, no le responden a la gente las invitaciones al diálogo, a la negociación; pero cuando se les bloquea una vía, cuando se bloquea la entrada a un campo petrolero, a un ingenio azucarero, ahí sí apelan, a hablar del interés general, de jurisprudencia, como si no sospecháramos de que en Colombia las leyes van por un lado, y el interés común por otro.]

[No hay seguridad pública sin la seguridad alimentaria garantizada. Por eso, a quienes producen los alimentos en Colombia no les queda de otra que protestar, pues el latifundio sigue instalado como forma tradicional de tenencia de la tierra, fracasó el TLC, fracasó el modelo económico que en el marco de su teoría neoliberal ve al mercado como un fetiche y se olvida de la cadena productiva monetarizando la vida social, ignorando las demandas de la gente y buscando la manera para desacreditar la protesta.]

[El acto de interés general más importante que solicita hoy Colombia, es que el Esmad no exista más, el bien común no es lo que el procurador dice, incluso, lastimosamente las leyes, parece ser, que no representan el interés general del país, así que ¿por qué vendría a hablarnos de interés general un tipo como el procurador Ordoñez que apoya el uso de glifosato, pero que obviamente nunca permitiría que un avión lo esparciera en el jardín de su hermosa casa?  ¿por qué viene a hablarnos de bien común un tipo que revoca la sanción de los oficiales del Esmad en el caso de Nicolás Neira? ¿por qué viene a hablar de “legalidad” un tipo que destituye a Piedad Córdoba, pero no destituye a Álvaro Uribe Vélez?  De seguro el procurador dirá que no está ideologizado, y que sólo habla en nombre de la ley… ¡¡¡pero es que aquí la ley es el foco indiscutible de la desconfianza que cualquier ciudadano común y corriente tiene frente al Estado!!!]

[La presencia del Esmad, sí es negociable. Porque dicha fuerza policial, NO constituye un instrumento de la legalidad, despejar una vía no es atender al bien común, atender al bien común es defender los intereses generales del país, en este caso, defender a los campesinos e indígenas del país que así bloqueen o no bloqueen una vía, están alimentándonos a todos.]

[Ordoñez, todos los alcaldes y gobernadores, saben que en Colombia, si no hay vías de hecho, no hay probabilidad de escenarios para la negociación, ¡ellos lo saben! por eso se afanaron en hacer la ley que lo prohibiera. Por eso, por ejemplo, las expresiones más radicales de la lucha política se han forjado a sangre y fuego su camino al diálogo en la Habana. Por eso, los movimientos sociales más fuertes están en el Cauca, en el Catatumbo, porque es gente que apela a las vías de hecho. Por eso aquí parece que un diálogo, una negociación, son sucesos que vienen después de que el pueblo demuestra de qué está hecho, después de que se presentan vías de hecho, porque la gente común y corriente, sabemos que la ley en Colombia puede ser la ley, pero en muchas ocasiones no tiene nada que ver con la justicia.]

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
