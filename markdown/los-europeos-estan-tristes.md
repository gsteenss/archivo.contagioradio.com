Title: Los europeos están tristes
Date: 2015-02-19 15:14
Author: CtgAdm
Category: Eleuterio, Opinion
Tags: Europa
Slug: los-europeos-estan-tristes
Status: published

###### Foto: losidealesdelgol 

Por [[**Eleuterio Gabón  **](https://archivo.contagioradio.com/eleuterio-gabon/)]

Tras una jornada de trabajo en una comunidad donde se vive la revolución diaria, los zapatistas dedican un rato a jugar al fútbol. También hay que divertirse, es fundamental para que la vida revolucionaria funcione.

En Europa nos divertimos a cada rato. La sociedad del bienestar tiene el ocio como uno de sus pilares principales. Deporte, cine, televisión, fiestas temáticas, fiestas porque sí, videojuegos, conciertos, conciertitos, conciertatazos, shoping, sexo, drogas, rock and roll. Sin embargo los y las europeas estamos tristes. Quizá resulte engañoso, tal vez a primera vista no lo parezca, es una tristeza interiorizada, colectivizada pero individual. Bien es sabido que es, a priori, uno de los lugares más seguros del mundo, donde, a priori denuevo, mejor se vive.

Comparado con el caos del que nos cuentan nuestros telediarios que gobierna el resto del planeta, la ingobernable África, la violenta Latinoamérica, el inestable Medio Oriente, en Europa vivimos en el mejor de los mundos posibles. Civilización es nuestra palabra insignia. La civilización parece mayor a medida que uno se dirige hacia el norte. Todo está más ordenado, más calmado, la rutina define el ambiente, la espontaneidad es una rareza, todo está programado, todo está bajo control.

Tal vez ese “estar bajo control” tiene mucho que ver con la tristeza. No vayan a creer que tantas maravillas como se predica que Europa tiene son a cambio de nada. Para disfrutar de la estabilidad europea uno debe adaptarse a su forma de vida, debe ser un buen ciudadano, ese es el trato. Hay que[ ]{.Apple-converted-space}asumir los discursos, cumplir las normas y sentirse feliz por ser un privilegiado. Quien se salta alguna de estas condiciones queda excluido, peor aún, debe ser castigado o reinsertado, según su gravedad. De este enorme engranaje de represión física y mental nos hacen responsables a todos. Europa es libertad, es seguridad, ustedes la han elegido, uestedes la permiten y la construyen día a día. Eso no es así pero se nos dice y se nos repite entre líneas a cada paso.

Hay pequeñas rebeliones dirarias contra este modelo de vida. Los hay que toman antidepresivos, incapaces de escapar a la rutina que teje su destino, quienes beben para llorar con apariencia de euforia la prohibición de sus sueños o van más allá quemando cajeros en busca de la venganza contra un enemigo invisible. El denominador común de todos ellos es un profundo deseo de alegría, de sentir una alegría de veras, no virtual; la alegría de vivir, ese derecho que no recoge la carta de derechos humanos.

Dicen que no hay mal que por bien no venga. Las crisis económicas ponen en crisis los varoles sociales más estandarizados. Son muchos los que saben que la autoproclamada vanguardia europea no es la de la cultura y las libertades sino, en gran medida, la de una tecnología y una burocracia para el control social. A pesar de las apariencias de la pretendida sociedad del bienestar, la tristeza interior de sus ciudadanos la delata. Europa no es el modelo a seguir, urge un cambio, también aquí urge una revolución.

Mientras tanto en Chiapas siguen jugando después de las tareas de resistencia. Allí hay tristeza pero no están tristes. No son dueños de nada, salvo de sus propias vidas.
