Title: Amenazan de muerte a sacerdote de López de Micay en Cauca
Date: 2017-05-10 16:55
Category: DDHH, Nacional
Tags: Amenazas a defensores de Derechos Humanos, Conpaz
Slug: amenazan-de-muerte-a-sacerdote-de-lopez-de-micay-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/paramilitares-cordoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [10 May 2017] 

El pasado 8 de Mayo, el sacerdote Killian Cuero, quién ha venido trabajando en la defensa de los Derechos Humanos de los habitantes de esa zona del departamento del Cauca, **fue amenazado de muerte por un hombre que además le dio la orden de abandonar el municipio**. Los hechos se produjeron en el casco urbano de la población hacia las 5:40 pm según la denuncia de la Comisión de Justicia y Paz.

La denuncia y la amenaza se dieron después de un evento convocado por la gobernación del departamento del Cuaca y el programa de las Naciones Unidas para el Desarrollo PNUD. **En dicho evento el padre Killian denunció la difícil situación que afrontan las comunidades de la zona**, debido a la presencia de paramilitares que pretenden el control de los territorios.

En concreto **el sacerdote denunció que los grupos de neoparamilitares sostienen un accionar en toda la región pacífica** y que dicha presencia y control tendría que ver con las rutas del narcotráfico y el desarrollo de operaciones de minería ilegal.  Le puede interesar:["Fuerte Arremetida Paramilitar en el Nordeste Antioqueño"](https://archivo.contagioradio.com/40171/)

El padre Killian Cuero, **quién también es integrante de las Comunidades Construyendo Paz en los Territorios, CONPAZ**, ha respaldado las acciones de paz de los habitantes de la región y ha hecho eco de las denuncias que también se han realizado por parte de las familias que habitan la zona rural del municipio. Le puede interesar:["En el Bajo Atrato paramilitares reclutan jóvenes para control social"](https://archivo.contagioradio.com/paramilitares-con-armas-cortas-controlan-territorios-del-choco/)

###### Reciba toda la información de Contagio Radio en [[su correo]
