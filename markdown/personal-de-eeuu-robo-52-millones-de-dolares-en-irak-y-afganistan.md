Title: Personal de EEUU robó 52 millones de dólares en Irak y Afganistán
Date: 2015-05-11 15:39
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 132 condenados de EEUU por robar dinero en Afganistán e Irak, 52 millones de dólares robados por militares en Irak y Afganistán, Afganistán, Estados Unidos, Guerra del golfo, Invasión Irak y Afganistán, Irak
Slug: personal-de-eeuu-robo-52-millones-de-dolares-en-irak-y-afganistan
Status: published

###### **Foto:HispanTV.com** 

[Al menos **52 millones de dólares han sido robados** por militares, contratistas y funcionarios Estadounidenses desde 2005 en **Afganistán e Irak**. Los fraudes se cometieron gracias al **caos financiero y burocrático** vivido durante las **invasiones** y el tiempo en que EEUU tuvo máxima presencia militar en los países. Y**a han sido identificados 115 integrantes** de las delegaciones Estadounidenses en los dos países invadidos.]{lang="ES"}

[Según **Stuart Bowen** delegado de EEUU para la reconstrucción de Irak ]{lang="ES"}"El **fraude total** cometido por personal militar estadounidense y contratistas **va a ser mucho más grande de lo que somos capaces de perseguir".**

Uno de los **fraudes** más comunes ha sido con el **combustible de las bases armamentísticas,** los estafadores traían más combustible de lo que declaraban en los informes, para luego vender este en el mercado negro y así poder lucrarse.

**Stephanie Charboneau y Jonathan Hightower,** ambos contratistas de EEUU han sido **condenados respectivamente a siete años** de prisión por fraude con el combustible.

Según los condenados esto ha sido posible gracias a la **permisividad del ejercicio militar** y a la inexistencia de operadores de control en operaciones como el traslado de combustible.

Actualmente hay **327 investigaciones abiertas y estas podrían implicar a más de 31 militares de alto rango de EEUU**.

Además la publicación del **reportaje** sobre la corrupción en las intervenciones militares Estadounidenses realizado por el **magazine *Slate*** ha provocado un aluvión de críticas dentro del país y fuera por parte de organizaciones sociales que acusan a los militares y funcionarios de lucrarse a costa de la guerra.
