Title: Día de la afrocolombianidad: una fecha para reafirmar la lucha contra el racismo
Date: 2019-05-21 12:44
Author: CtgAdm
Category: Comunidad, Nacional
Tags: ACONC, Día de la Afrocolombianidad, PCN
Slug: dia-de-la-afrocolombianidad-una-fecha-para-reafirmar-la-lucha-contra-el-racismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/afrocolombianidad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

Como cada 21 de mayo desde 2001, las comunidades negras de Colombia conmemoran la abolición de la esclavitud en el país celebrando el Día de la afrocolombianidad, una fecha que se redimensiona en la actualidad para luchar contra el racismo y la discriminación latente en el territorio nacional.

**Charo Mina, integrante del Proceso de Comunidades Negras (PCN) ** explicó qué actividades realizarán para este día, entre ellas su participación en el Congreso, donde expusieron cuál es la situación de la población afrodescendiente en Colombia con relación a sus derechos, el territorio, el desarrollo y la protección de sus líderes y lideresas, pues "a pesar de nuestras luchas y nuestros avances seguimos en  una situación de exclusión que nos distancia de las aspiraciones de vida digna que en general tiene este país", afirma.

### Continúa el racismo estructural 

Mina señaló que durante este periodo y en particular con el nuevo Gobierno,  las manifestaciones de racismo son más evidentes, especialmente con la implementación de los acuerdos y el Plan Nacional de Desarrollo (PND) de los que fueron excluidos, pues este ha demostrado ser un Estado que ha dado ciertos pasos "pero que no implementan ni respeta la legislación de los pueblos".

La lideresa afirma que los avances son pocos pues a pesar de sostener "una lucha centenaria que ha generado algunos frutos" continúan prácticas como el racismo, las condiciones de violencia y despojo en el territorio,  "una violación de derechos que tiene una sola razón de ser: el racismo estructural", apunta.

A propósito de esta fecha, organizaciones de comunidades afro enviaron una carta al presidente Duque en la que reafirman su voluntad para continuar trabajando por la  Reparación Histórica y exigiendo la implementación del capítulo étnico del acuerdo de paz y así cumplir con la defensa de los derechos territoriales y "la protección colectiva, especialmente de las mujeres y  los territorios ancestrales".

### Avances de las comunidades afrodescendientes 

Charo Mina destaca que gracias a su lucha han logrado avances a nivel legislativo y constitucional, "hemos obtenido nuestro reconocimiento como pueblo, procesos territoriales organizados y más de seis millones de hectáreas tituladas", resalta. [(Le puede interesar: "Muchos nos ven como una piedra en el zapato por defender el territorio" ACONC)](https://archivo.contagioradio.com/muchos-nos-ven-como-una-piedra-en-el-zapato-aconc/)

Como parte de esta conmemoración, en los territorios se realizarán diferentes actividades, conservatorios, exposiciones artísticas y reuniones comunitarias, es una fecha en la que hay bastante movilización pues también se conmemora el día del Panafricanismo, "hay remembranza de todas esas contribuciones que como diáspora africana y como miembros de esa herencia hemos hecho", declaró Mina.

> [\#DiaDeLaResistenciaNegra](https://twitter.com/hashtag/DiaDeLaResistenciaNegra?src=hash&ref_src=twsrc%5Etfw)  
> Hoy más que el día de la afrocolombianidad, conmemoramos el día de la resistencia y dignidad negra, un día para honrar el legado de los ancestr@s, un día para recordar la responsabilidad de seguir pariendo la libertad para nuestro pueblo.
>
> — Francia Márquez Mina (@FranciaMarquezM) [21 de mayo de 2019](https://twitter.com/FranciaMarquezM/status/1130793765017477120?ref_src=twsrc%5Etfw)

### Mujeres que hacen historia de resistencia 

En el marco del Día de la Afrocolombianidad, el PCN también hizo el lanzamiento de **'[No hay otra opción que resistir: Lideresas del proceso de comunidades negras'](https://renacientes.net/wp-content/uploads/2019/05/Memoir-Full-PDF.pdf)**,  un proyecto que esperan sea un motivante para reconocer a su género "dentro de los procesos organizativos, dentro de la vida de las comunidades del movimiento negro y del país" y  quienes han sido pioneras en la producción de conocimiento y el avances de la población negra en Colombia.

<iframe id="audio_36159923" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_36159923_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
