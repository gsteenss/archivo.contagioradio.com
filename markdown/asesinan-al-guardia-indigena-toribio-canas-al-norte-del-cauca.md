Title: Asesinan al guardia indígena Toribio Canás al norte del Cauca
Date: 2019-10-14 12:49
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asesinato de indígenas, Cauca, CRIC
Slug: asesinan-al-guardia-indigena-toribio-canas-al-norte-del-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/55949702_1676960492406329_5701197755452489728_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CRIC ] 

El Consejo Regional Indígena del Cauca (CRIC) denunció el asesinato del guardia indígena, Toribio Canas Velasco, la noche del pasado 13 de octubre en el casco urbano de Tacueyó del municipio de Toribio. Según información de la Oficina del Alto Comisionado de las Naciones Unidas, en 2019 se han registrado más de 36 homicidios contra  integrantes de pueblos indígenas al norte del departamento, al menos 53 amenazas de muerte y 8 atentados sin que exista una respuesta efectiva ante esta problemática por parte del Estado, más allá de la militarización.

Los hechos, según el comunicado del CRIC, ocurrieron cuando el guardia se encontraba en un establecimiento junto a sus amigos y siendo las 10:30 pm, hombres encapuchados llegaron hasta el lugar, disparando contra Toribio causando su muerte. El hombre de 53 años, era  a su vez, esposo de la coordinadora de salud del plan de vida de la zona. [(Lea también: Tortura contra coordinador indígena José Camayo retrata la aguda crisis del Cauca)](https://archivo.contagioradio.com/tortura-contra-coordinador-indigena-jose-camayo-retrata-la-aguda-crisis-del-cauca/)

### Ejercer el control territorial del Cauca les está costando la vida 

Las autoridades indígenas señalan que pese a que aún no se conoce si Toribio fue objeto de amenazas,  el ejercicio de control que viene realizando la guardia es el motivo principal por el que **grupos armados tanto legales como ilegales,** continúan intimidando y asesinando a autoridades, lideres y jóvenes.  [(Lea también: Asesinan a Gersain Yatacue, coordinador y guardia indígena de Toribio, Cauca)](https://archivo.contagioradio.com/asesinan-a-gersain-yatacue-coordinador-y-guardia-indigena-de-toribio-cauca/)

El CRIC denuncia a su vez que en horas de la mañana de ese mismo día,  en el Resguardo indígena de Pioya en Caldono, fue hallado un panfleto en el que quienes se identifican como el Cartel Sinaloa **amenazan al Consejero de la asociación Ukawesx Nasa Cxhab Fredy Campo, a los gobernadores Alfonzo Díaz, Ovidio Hurtado y a la guardia indígena.**

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
