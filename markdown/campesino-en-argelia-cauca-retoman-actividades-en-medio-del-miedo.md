Title: Campesinos en Argelia, Cauca, retoman actividades en medio del miedo
Date: 2015-11-23 17:44
Category: Movilización, Nacional
Tags: Bombardeos en Argelia Cauca, Campesinos heridos en Argelia Cauca, erradicación de cultivos ilícitos, Fuerzas Militares en Argelia, proceso de paz
Slug: campesino-en-argelia-cauca-retoman-actividades-en-medio-del-miedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Militares-en-Argelia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: ASCAMCAT 

###### [23 Nov 2015 ]

[Tras la **incursión de cerca de 2 mil miembros de las FFMM junto a** 319 erradicadores de cultivos ilícitos y efectivos del ESMAD en el municipio de Argelia, Cauca, el pasado jueves, dejando como **resultado por lo menos 6 campesinos heridos y una persona muerta**, las comunidades reanudan sus actividades en medio del miedo y la desconfianza frente a los acuerdos pactados con el Gobierno nacional.]

[“El tema de las escuelas inicia a media marcha pues quedan las huellas de la guerra que se vivió… **no es fácil de borrar el tema de los gases, del plomo, de los helicópteros ametrallando**, eso queda por un tiempo, eso no va a ser de la noche a la mañana... queda uno con ese trauma… nos duele la situación de los campesinos no sólo de Argelia, sino de otras partes del país” afirma el líder campesino Orlando Bolaños.]

[Bolaños asegura que en la reunión del pasado sábado con delegados de Gobierno nacional, departamental y municipal, se acordó una mesa de diálogos que sesionara desde el próximo 3 de diciembre y que contempla la **consolidación de un plan de sustitución gradual de cultivos ilícitos**, pese a la polarización que existe frente a este tema.  ]

[Las comunidades persisten en su negativa a que continúe la erradicación, “nosotros no queremos erradicación, **queremos sustitución, pero no una sustitución impuesta, sino una sustitución negociada**, que nosotros miremos que realmente nos conviene, porque no nos pueden salir con engaños como siempre lo han hecho” afirma Bolaños, pero el Estado asegura que persistirá continuar con la erradicación manual pese a los últimos hechos.]

[“La verdad nosotros no creemos en el Gobierno, no creemos en las Fuerzas Militares, no creemos en la Policía, de lo que del Gobierno venga no creemos en nada porque **lo pasó ahorita fue una traición al proceso de paz**, una traición al espacio que estábamos ganando con las Fuerzas Militares… ahora vuelven y tiran a la basura todo, entonces es difícil volver a creer cuando **en el día nos dan la mano y por la tarde nos están dando garrote, plomo, gases y de todo**”, agrega el líder. ]

[Frente a los Diálogos de paz, Bolaños afirma que “días atrás se sacó el Ejército del Sinaí con el fin de cuidar el tema del proceso de paz, para respetar la zona donde hay presencia guerrillera con el fin de evitar incidentes con armas, pero **el Ejército aprovechó esa tensa calma y nos invadió de la noche a la mañana".**]

[“El tema del proceso de paz nos ha tenido muy ilusionados porque **hace un tiempo no escuchábamos ametrallamientos, no escuchábamos plomo**, hasta ahorita que vuelve y se da… sí estamos hablando de un proceso respeten, porque nosotros queremos que nos respeten… sea de parte y parte el que rompa el acuerdo está irrespetando a la comunidad, porque **los combates se dan en medio de la población civil y es lo que nosotros no queremos**”, indica el campesino de Argelia.]

[Al finalizar la reunión del sábado fueron retiradas las tropas militares de Argelia, pero de acuerdo con versiones de campesinos el día de **ayer se registró la presencia de militares en municipios aledaños como Cristales, El Mango y La Primavera** en los que “la militarización es el diario vivir” concluye.]
