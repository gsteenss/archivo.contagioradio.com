Title: Tejedoras de Mampuján piden gestos de paz a todos los actores armados
Date: 2016-09-29 17:29
Category: Nacional, Paz
Tags: conflicto armado, FARC, Gobierno, mujeres, paz
Slug: tejedoras-de-mampujan-piden-gestos-de-paz-a-todos-los-actores-armados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Tejedoras-Mapuján-e1475187934866.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carolina Corredor, U. Externado 

###### 29 Sep 2016

En una subregión del Caribe colombiano, entre Sucre y Bolívar, los Montes de María fueron testigo de crudos capítulos de la guerra. Tiempo después, u**n grupo de mujeres campesinas se unieron para superar los traumas,** por medio de la representación de sus vivencias en figuras de tela.

El grupo de mujeres visita esta semana la capital colombiana para instalar la exposición **“Mampuján entretejido: un camino estético para la paz”,** una muestra que se extenderá hasta el 30 de noviembre de 2016 en la biblioteca de la Universidad Externado de Colombia.

Allí, las tejedoras de Mampuján, galardonadas con el Premio Nacional de Paz, se refirieron a la histórica decisión que tomarán los colombianos en las urnas el próximo domingo, para refrendar el acuerdo de paz alcanzado entre el Gobierno y las FARC en Cuba.

Juana Alicia Ruiz, líder de las tejedoras, hizo un llamado al Gobierno a que, de ganar el sí, cumpla lo acordado “pues dependerá cómo se lleven las cosas con este grupo (FARC), para que sea más fácil que los demás actores crean en el proceso y se desmovilicen”.

Sin embargo aseguró como víctima que “**es tiempo de que nos den la oportunidad de vivir en una nueva Colombia,** que le den la oportunidad a nuestros hijos de que vivan en una Colombia en paz, porque no hemos parido hijos para que vayan a la guerra”.

Las telas narran desde la vida del cimarrón en África hasta la masacre de 12 campesinos de la vereda de Las Brisas, corregimiento de San Cayetano, y el desplazamiento de cientos de personas de la población de Mampuján, por parte del bloque Héroes de los Montes de María comandados por Úber Bánquez en el año 2000.

A su turno **Gledys López pidió a los ciudadanos que acudan masivamente a las urnas.** “Mi llamado es que por favor votemos al sí para que Colombia cambie en su vivir. No pensemos en nosotros, pensemos en nuestros hijos”, manifestó.

Y aunque algunos indecisos con el acuerdo alcanzado, **manifiestan que aún quedan temas pendientes con los grupos neoparamilitares y el ELN,** asegura que “en la medida que tengamos un enemigo menos, tenemos más motivos para seguir luchando por la paz”.

Los bordados, de gran formato, constituyen un minucioso trabajo femenino, que no solo narra la memoria de los dolorosos hechos asociados al conflicto armado colombiano, sino que devela un valioso patrimonio cultural y natural, que emerge como refinadas y pequeñas piezas que representan animales, flores, arquitectura y prácticas ancestrales de la población del caribe colombiano.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
