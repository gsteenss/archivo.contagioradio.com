Title: Comunidades étnicas denuncian exclusión de implementación de acuerdos de paz
Date: 2017-09-07 12:44
Category: Nacional, Paz
Tags: acuerdos de paz, CSIVI, implementación de los acuerdos, Pueblos étnicos
Slug: comunidades-etnicas-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/pueblos-etnicos-e1504805616561.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Marcha Patriótica] 

###### [07 Sept 2017] 

La Comisión Étnica para la Paz y la Defensa de los Derechos Territoriales, **alertó sobre la exclusión de las comunidades étnicas de la implementación de los acuerdos de paz**. A través de un mensaje, le manifestaron al Gobierno que acudirán a la movilización si "continúan con la actitud racista y excluyente de los pueblos étnicos".

Le recordaron al Gobierno Nacional que tiene la responsabilidad de garantizar **"la implementación de lo acordado"** mediante la construcción de un Plan Marco de Implementación. (Le puede interesar: ["Se creará instancia con pueblos étnicos para implementación de los acuerdos"](https://archivo.contagioradio.com/se-creara-instancia-con-pueblos-etnicos-para-implementacion-de-acuerdos-de-paz/))

Indicaron que ha habido una exclusión con **"graves consecuencias para los derechos conquistados, en particular los derechos territoriales"**. Para ellos, hay un riesgo de que la Comisión de Seguimiento, Impulso y Verificación (CSIVI), apruebe un Plan Marco sin indicadores específicos que identifiquen y midan los avances en políticas públicas que respondan a las necesidades de los pueblos étnicos.

Esto supondría una falta de inclusión de los **"resultados estructurales para los pueblos étnicos, que hemos sufrido los impactos desproporcionados del conflicto".** También significa que la implementación en sus territorios se quedaría sin los recursos necesarios para garantizar la reparación de las víctimas. (Le puede interesar: ["Comisión étnica pide participación en tribunal para la paz"](https://archivo.contagioradio.com/comision-etnica-pide-participacion-en-tribunal-para-la-paz/))

Manifiestan que, en el proceso de diálogo entre el Gobierno y las FARC, **ya había habido una exclusión**, solo al final lograron incluir el Capítulo Étnico. Además, "como ha ocurrido con el progreso legislativo vía fast track que nos ha excluido y violado el derechos a la consulta previa, libre e informada, el gobierno ejerce una especie de apartheid frente a lo étnico en la implementación del acuerdo".

Finalmente indicaron que "**el llamado post acuerdo incrementará el trato desigual y discriminatorio que han sufrido** nuestro pueblos, lo cual nos revictimiza y viola nuestros derechos ". Instaron a los pueblos a declararse en estado de alerta y a estar preparados para "demandar cumplimiento y respeto por parte del Gobierno colombiano".

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

   
 
