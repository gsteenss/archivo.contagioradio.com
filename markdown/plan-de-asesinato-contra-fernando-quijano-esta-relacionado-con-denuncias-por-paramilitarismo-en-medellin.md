Title: Plan de asesinato contra Fernando Quijano está relacionado con denuncias por paramilitarismo
Date: 2015-03-17 16:50
Author: CtgAdm
Category: DDHH, Nacional
Tags: Comuna 10 de Medellín, Convivir, Fernando Quijano, Medellin, Nuevo Jerusalen, Paramilitarismo
Slug: plan-de-asesinato-contra-fernando-quijano-esta-relacionado-con-denuncias-por-paramilitarismo-en-medellin
Status: published

###### Foto: lasdosorillas 

Según el propio **Fernando Quijano** hay un plan que se viene orquestando desde hace mucho tiempo, e incluso habría un alto oficial de la policía vinculado con este plan. La denuncia ya se había presentado a la fiscalía y según Corpades, tiene que ver con la denuncia de la **actuación de las CONVIVIR y su estrecha relación con sectores de la policía.**

Los detalles que se conocieron por parte de fuentes confiables, según Quijano, son tan específicos como que el plan se está realizando en un bar de Barbacoas, en la comuna 10 de **Medellín** y en el atentado participarían motos “gemeliadas”; es decir con placas falsas o que pertenecen a otro vehículo similar; además el atentado se perpetraría cerca del lugar de residencia de Quijano, sitio que habría sido identificado como de alta vulnerabilidad.

En los últimos días, el equipo de seguridad del defensor de Derechos Humanos, detectó movimientos sospechosos en las inmediaciones de la vivienda y lograron grabar los movimientos de algunas personas lo cual es altamente preocupante según el propio Quijano.

Para el presidente de **CORPADES** las denuncias que se hacen desde su organización golpean de manera directa las estructuras criminales que operan en Medellín y golpean también a personas comprometidas con lo que se conoce como la **"protección oficial"** a esos sectores criminales.

Una de las denuncias que se resaltan es acerca de las **35 estructuras de CONVIVIR** **que continúan funcionando** a pesar de la actuación oficial, que, dice Quijano, son paños de agua tibia. Otras de las denuncias de “Análisis Urbano” son la situación actual en Nuevo Jerusalén, la vinculación de un alto mando de la policía y la nueva cadena de mando en la “Oficina de Envigado”. Además hay un alto oficial que se mueve alrededor de las estructuras criminales del sector "El Raudal".

Quijano también denuncia que en **Nuevo Jerusalén** hay estructuras paramilitares que hace cobros por tarifas de servicios públicos. La cuota por casa asciende a los 35 mil pesos y de esto también tienen conocimiento las autoridades y hasta el momento no hay acciones eficaces para desmontar el paramilitarismo.
