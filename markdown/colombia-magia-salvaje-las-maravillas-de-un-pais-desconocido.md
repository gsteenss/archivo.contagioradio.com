Title: Colombia, Magia Salvaje: Las maravillas de un país desconocido.
Date: 2015-08-30 11:00
Author: AdminContagio
Category: 24 Cuadros, Ambiente
Tags: Cine Colombia, Colombia magia salvaje, Mike Slee
Slug: colombia-magia-salvaje-las-maravillas-de-un-pais-desconocido
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/unnamed-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Póster Película Magia Salvaje.] 

A partir del 10 de septiembre, llega a las salas de cine del país "Colombia, Magia Salvaje", un ambicioso proyecto que busca enamorar al público nacional e internacional con la belleza natural del paisaje colombiano. Un film para el que se recolectó material que involucró “85 locaciones, 150 horas de grabación, 38 especies filmadas, 640 horas de edición 20 ecosistemas documentados”.

<iframe src="https://www.youtube.com/embed/43gK9f_Pai0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>  
[Con viajes por los paradisíacos paisajes, bosques ríos y océanos de la Nación, la película cautiva con la biodiversidad existente en Colombia que pareciera ignorarse o desconocerse.]

[![Parque natural Sierra de Chiribiquete. Foto: Colombia Magia Salvaje.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/unnamed.jpg){.aligncenter .size-full .wp-image-12992 width="1177" height="785"}](https://archivo.contagioradio.com/colombia-magia-salvaje-las-maravillas-de-un-pais-desconocido/unnamed-9/)

###### [Parque natural Sierra de Chiribiquete. Foto: Colombia Magia Salvaje] 

[En el largometraje se expone que la explotación indiscriminada de los recursos y la despreocupación por la conservación del medio ambiente, hacen que todo el paraíso natural de la Nación se encuentre en peligro.]

[[![Mineria](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/unnamed-11.jpg)

###### [Foto: confederacionminera.blogspot  ] 

[Bajo la dirección de Mike Slee (]*[¡Bichos! 3D, El Vuelo de las Mariposas),]*[se encuentran figuras como Richard Kirby en la fotografía (]*[Wildest Africa]*[) y el compositor David Campbell (]*[Annie, El Hombre Araña 2, Guerra Mundial Z]*[), quien convocó a la Orquesta Sinfónica de Colombia, Carlos Vives, Aterciopelados, ChoqQuibTown, Walter Silva, Andrés Castro, Juanes y Fonseca para la interpretación de la música en el largometraje, la producción también promete un despliegue de diferentes ritmos y géneros musicales.]

[Con el [auspicio](http://www.magiasalvaje.org/aliados) de entidades públicas, privadas y el apoyo de diversas ONG’s, la película logró rodarse con tecnología de punta y un equipo humano reconocido mundialmente.]

[El precio de cada boleta, tendrá un valor de \$4.500 al solicitarse un cupón en cualquier almacén Éxito.]
