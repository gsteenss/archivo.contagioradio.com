Title: FARC espera que Santos haga valer sus facultades para sacar adelante acuerdos de paz
Date: 2017-05-18 07:15
Category: Nacional, Paz
Tags: Corte Constitucional, paz
Slug: corte_constitucional_fast_track
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/corte-e1495109388589.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: AFP 

###### [18 May 2017] 

[El Congreso de la República ahora podrá modificar el acuerdo de paz firmado entre el gobierno y las FARC. Así lo ha establecido la Sala Plena de la Corte Constitucional que tumbó dos puntos del 'fast track', con los que se blindaba lo pactado, de manera que]**sólo podía ser modificado siempre y cuando se ajustara al contenido del Acuerdo Final.**

[Los numerales H y J del acto legislativo para la paz, son los que tumbó el alto tribunal, tras el análisis de una demanda que presentó el Centro Democrático. **“Se tumban un par de instrumentos importantes, pero el asunto del 'Fast Track' es más profundo**”, asegura el Ministro del Interior, Juan Fernando Cristo.]

[Así las cosas, el 'Fast Track' es aceptado por la Corte, pero sin aprobarse el apartado 'H' del artículo primero decía que "los proyectos de ley y de acto legislativo solo podrán tener modificaciones siempre que se ajusten al contenido del Acuerdo Final y que cuenten con el aval previo del Gobierno nacional". Uno de los aspectos que más cuestionaba el Centro Democrático.]

[Desde muchos sectores se afirma que se le ha dado un duro golpe al proceso de paz. Sin embargo Juan Fernando Cristo, ha manifestado que los elementos esenciales que le dan seguridad jurídica al tránsito de las FARC ya fueron tramitados y ampliamente aceptados por el Congreso. No obstante, **señala que la decisión de la Corte dificulta la implementación de los acuerdos y además la  hace más lenta.**]

[Por su parte, Iván Márquez, Jefe de la Delegación de Paz de las FARC-EP,  dijo en twitter “**Confiamos en que el presidente haga valer las facultades que le otorga la Constitución para sacar adelante este proceso de paz**”.]

[Este jueves el Luis Guillermo Guerrero el presidente de la Corte Constitucional, explicará los alcances de la decisión del alto Tribunal.  ]

**Plantón para que "no se enrede la paz"**

David Flórez, vocero del movimiento Marcha Patriótica, asegura que así como se pudo superar la derrota del plebiscito se puede superar ese obstáculo, llamando a las dos armas que fueron en ese momento una pedagogía muy fuerte a la sociedad colombiana y la movilización ciudadana.

Adicionalente, Florez afirma que ante esta situación se requiere una mayor vigilancia por parte de la ciudadanía sobre la actividad del congreso, retomando esfuerzos como la iniciativa "Ojo a la paz" redoblarlos. Para las 4 p.m. se convocó a un plantón en exigencia de "no enredar la paz" en la céntrica Plaza de Bolívar de Bogotá.

<iframe id="audio_18785341" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18785341_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
