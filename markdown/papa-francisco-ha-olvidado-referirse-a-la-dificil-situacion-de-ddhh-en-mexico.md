Title: Situación de derechos humanos no ha estado en la agenda del Papa en México
Date: 2016-02-16 16:57
Category: DDHH, El mundo
Tags: Papa Francisco en México, Visita Papa México
Slug: papa-francisco-ha-olvidado-referirse-a-la-dificil-situacion-de-ddhh-en-mexico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Papa-México.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Desde La Plaza] 

<iframe src="http://www.ivoox.com/player_ek_10467001_2_1.html?data=kpWhmJyUdJKhhpywj5WdaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5yncbTd1drOxc6Jh5SZo5jbjcnJb8XZ08rQytTXb8npzsbb0diPstCfycaYx9jYpcXjjMrbjdHFb8LbxtPRj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alina Duarte, Revolución 3.0] 

###### [16 Feb 2016 ]

Desde el pasado viernes el Papa Francisco visita distintas ciudades de México en las que se presentan altos índices de pobreza, violencia, narcotráfico, muerte y desaparición forzada, y en las que **organizaciones de víctimas han demandado del Sumo Pontífice intervenciones más contundentes en temas relacionados con la vulneración de derechos humanos**, como la desaparición forzada de los 43 estudiantes de Ayotzinapa o los siete feminicidios diarios que se presentan en municipios como Ecatepec.

Según afirma la periodista Alina Duarte, las **familias de las víctimas exigieron reunirse con el Papa Francisco con el fin de que sus casos sean más visibilizados a nivel internacional**, "para que el Gobierno mexicano no dijera que es lo más importante a seguir en la agenda, sino que fuera el mismo Papa el que pudiera pronunciarse al respecto"; sin embargo, el sumo Pontífice ha asegurado que no se reunirá con varias de ellas.

Además de las [[prácticas de limpieza social denunciadas](https://archivo.contagioradio.com/lo-que-no-quieren-que-el-papa-vea-en-su-visita-a-mexico/)], Duarte asevera que ha habido **"un derroche de dinero impresionante para tratar de ocultar este México de las altas cifras de horror y sangre"**, que los grandes monopolios de la comunicación han ocultado en sus cubrimientos a los eventos en los que además, los medios alternativos no han podido hacer presencia. Frente a estos alarmantes índices de violencia a los que se suman los asesinatos de periodistas se espera que el Papa Francisco se pronuncié.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
