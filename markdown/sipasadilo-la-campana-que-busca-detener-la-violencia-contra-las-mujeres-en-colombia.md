Title: #SiPasaDilo la campaña que busca detener la violencia contra las mujeres en Colombia
Date: 2020-11-20 15:25
Author: AdminContagio
Category: Actualidad, Mujer
Tags: acoso, mujeres, Violencia contra las mujeres, Violencia de género en Colombia
Slug: sipasadilo-la-campana-que-busca-detener-la-violencia-contra-las-mujeres-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/WhatsApp-Image-2020-11-19-at-3.40.45-PM.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

*Foto: Christina Noriega*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Previo al **Día Internacional de la Eliminación de la Violencia contra la Mujer este 25 de noviembre**, diferentes organizaciones en defensa de los derechos de las mujeres y niñas, se unieron en la campaña **\#SiPasaDilo** que tiene como objetivo visibilizar y analizar los casos en aumento de violencia intrafamiliar y acoso sexual contra las mujeres.

<!-- /wp:paragraph -->

<!-- wp:heading -->

\#SiPasaDilo, es momento de hablar
----------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

La campaña **\#SiPasaDilo es organizado por la Red Nacional de Mujeres y cuenta con el apoyo de ONU Mujeres y la Agencia de los Estados Unidos para el Desarrollo Internacional** *-U.S Agency for Internacional Development-*, (USAID) por sus siglas en inglés-.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RNMColombia/status/1329428007728066571","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RNMColombia/status/1329428007728066571

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Al mismo tiempo ésta pretende alertar sobre el aumento de la violencia en Latinoamérica en contra de las mujeres y especialmente en Colombia, **una cifra que entre enero y septiembre del 2020 registra 45 feminicidios en el país, según el Observatorio Feminicidios Colombia.** ([Comité de Impulso presentó informe sobre Resolución 1325](https://archivo.contagioradio.com/comite-de-impulso-presento-informe-sobre-resolucion-1325/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Dentro de las acciones que encabezan esta la realización de un foro virtual denominado; ***"La justicia en el acoso sexual: del escarnio público, la denuncia legal y otras vías"*, que se llevará a cabo de manera gratuita este lunes 23 de noviembre a las 4:00 de la tarde** y se transmitirá por el Facebook y el canal de YouTube de la Red Nacional de Mujeres de Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este espacio virtual se responderán preguntas cómo, ¿De qué se trata el acoso sexual?, ¿Por qué las mujeres no denuncian?, ¿Es legítimo el escarnio público?, ¿Es legítima la denuncia anónima?, ¿De qué manera opera la justicia en los casos de acoso sexual?; entre otros puntos que se pondrán sobre la mesa de análisis.

<!-- /wp:paragraph -->

<!-- wp:heading -->

En septiembre de 2020 se registraron 86 feminicidios en Colombia
----------------------------------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El[Observatorio Feminicidios Colombia](https://observatoriofeminicidioscolombia.org/), una plataforma que se ha encargado de hacer seguimiento y divulgación de las violencia que enfrentan las mujeres en todo el territorio nacional, evidencia que **tan sólo en el mes de septiembre de este año se registraron 86 feminicidios y 25 feminicidios en grado de tentativa, lo cual da un total de 111 mujeres víctimas de violencia feminicida.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FeminicidiosCol/status/1324761303873933314","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FeminicidiosCol/status/1324761303873933314

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Dentro de este mismo informe dan cuenta qué **septiembre es el mes con más casos de violencia en contra de la mujer** en comparación al mes de agosto, en donde se registraron 7 casos, al mismo tiempo evidencia que en el departamento del **Valle del Cauca se registra el mayor número de feminicidios con 16, seguido** **por Antioquia con 14** feminicidios. ([Violencia contra la mujer durante la pandemia](https://archivo.contagioradio.com/violencia-contra-la-mujer-durante-la-pandemia/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, durante esta pandemia según el DANE 4,2 millones de personas perdieron su empleo, afectando en su mayoría a las mujeres, quienes tuvieron que retornar a sus hogares a enfrentarse en muchos casos con sus agresores; ante esto la Línea Púrpura y la Casa de Igualdad Para Las Mujeres, informa que durante este año **recibieron cerca de 10.403 llamadas, de estas el 48% corresponden a violencia psicológica, 27% a física, 15% a económica, 3% a sexual, 5% a patrimonial y 2% a violencia verbal.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A esto se suma que según Sisma Mujer, *"durante el periodo de aislamiento preventivo obligatorio ocasionado por el covid-19, entre el 25 de marzo y el 10 de septiembre **se incrementaron las llamadas por violencia intrafamiliar en un 121,7%**"*, una cifra qué pasa de 6.561 llamadas en el mismo periodo del 2019, a 14.545 en el 2020.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En un panorama internacional, **estudios dan cuenta que cada 33 minutos una mujer es agredida sexualmente,** un acto de violencia que según la ONU se reconoce como *"**una de las vulneraciones a los Derechos Humanos más extendidas, persistentes y devastadoras del mundo**"*, pese a ello, también es una de las que menos se informa de manera oportuna, esto debido a los altos índices de impunidad en contra de los agresores, sumado al silencio que asumen las víctimas producto de la estigmatización y vergüenza de reconocer el abuso.

<!-- /wp:paragraph -->

<!-- wp:core-embed/instagram {"url":"https://www.instagram.com/p/CH1BemjFnhK/?igshid=fmhfwgfmlql8","type":"rich","providerNameSlug":"instagram","className":""} -->

<figure class="wp-block-embed-instagram wp-block-embed is-type-rich is-provider-instagram">
<div class="wp-block-embed__wrapper">

https://www.instagram.com/p/CH1BemjFnhK/?igshid=fmhfwgfmlql8

</div>

</figure>
<!-- /wp:core-embed/instagram -->

<!-- wp:paragraph -->

En respuesta a a estos casos y la violencia silenciosa que viven cientos de mujeres en Colombia, presentan la campaña **\#SiPasaDilo, que tiene como objetivo visibilizar las cifras de violencia contra la mujer y realizar una labor de pedagogía sobre los tipos de violencia** para identificarlos y posteriormente tener presente las rutas de atención respectiva para denunciarlos.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
