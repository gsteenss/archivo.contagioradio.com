Title: Aumenta temor por amenaza de Autodefensas Gaitanistas a defensores de DDHH en el Valle
Date: 2016-04-12 11:36
Category: DDHH, Nacional
Tags: amenazas a líderes, Autodefensas Gaitanistas, MOVICE
Slug: aumenta-temor-por-amenaza-de-autodefensas-gaitanistas-a-defensores-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/defensore-de-derechos-humanos-asesinados.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [12 Abril 2016 ]

Un nuevo panfleto de las [['Autodefensas Gaitanistas de Colombia'](https://archivo.contagioradio.com/?s=autodefensas+gaitanistas)] pone en **riesgo la vida e integridad** de miembros de organizaciones defensoras de los derechos humanos en el departamento del Valle del Cauca.

[Se trata de Marta Giraldo, secretaria técnica del MOVICE; Walter Agredo, miembro del 'Comité de Solidaridad con Presos Políticos'; Hernán Arciniegas, Julián Lozano y Ariel Díaz, integrantes de la CUT; Rodrigo Vargas, subdirector del Comité Permanente Por la Defensa de los Derechos Humanos; José Milciades Sánchez miembro Sintraunicol, Jorge Iván Vélez y Albert Quintero integrantes de Sintramecali; además de destacados líderes en la defensa de los derechos humanos como Antonio Gutierrez, Edinson Mendez y Carlos Murcia. ]

En el panfleto, las Autodefensas Gaitanistas declaran a estas personas "objetivo militar" en el marco del "exterminio" que han venido desplegando a nivel nacional, departamental y municipal, e insisten en que todo aquél que esté cerca de los líderes también podría "llevar plomo". Amenazas similares, proferidas también **por grupos paramilitares en 2012 y 2015 contra varias de estas personas**, fueron denunciadas ante las autoridades, en exigencia de medidas de seguridad.

Distintas organizaciones sociales, entre ellas el MOVICE, extienden su denuncia sobre "el aumento del accionar paramilitar en todo el país" y reiteran que, "para alcanzar una paz estable y duradera, el Estado debe reconocer que el paramilitarismo es un fenómeno que sigue vigente y que se ha ido consolidando en una nueva fase con la **complicidad, anuencia y tolerancia de estructuras institucionales**, no sólo de la fuerza pública, sino también de poderes políticos y de sectores económicos del país".

En el más reciente comunicado, el MOVICE, asegura que para lograr el **desmonte real y definitivo del paramilitarismo** es necesaria la consolidación de una política coherente con la participación de las víctimas, organizaciones defensoras de los derechos humanos y expertos, que caracterice el problema y logre una reforma institucional para las garantías de no repetición, especialmente para las víctimas del fenómeno paramilitar.

Conozca el panfleto:

[![Panfleto AGC](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Panfleto-AGC.jpg){.aligncenter .size-full .wp-image-22490 width="720" height="1280"}](https://archivo.contagioradio.com/aumenta-temor-por-amenaza-de-autodefensas-gaitanistas-a-defensores-de-ddhh/panfleto-agc/)

##### Reciba toda la información de Contagio Radio en [s[u correo]](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
