Title: 22 Hospitales de Bogotá estarían en riesgo por recorte presupuestal del 40%
Date: 2016-01-22 12:14
Category: Nacional
Tags: Alcaldía de Bogotá, crisis de la salud, Enrique Peñalosa
Slug: 40-recorte-presupuestal-bogota-hospitales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/hospitales-e1453482398104.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Revista Mire Lea 

<iframe src="http://www.ivoox.com/player_ek_10166480_2_1.html?data=kpWemJuYfJGhhpywj5aVaZS1lpWah5yncZOhhpywj5WRaZi3jpWah5yncZOmjK3c1dXNuMLgxtiYxsqPhtDb0NmSpZiJhZKfxtjhw9eJh5SZoqnO0JDJsozmysrgydSPtNDmjNfSxdTWuI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [María Doris González] 

###### [22 Ene 2016]

Desde la [Alcaldía de Bogotá](https://archivo.contagioradio.com/?s=Bogot%C3%A1) se anunció un **recorte presupuestal del 40% en los gastos de funcionamiento de la red de hospitales del distrito**, a la que en el 2015 le fueron destinados \$2,2 billones de pesos, lo que implicaría que el recorte para este año podría ser de más de \$800.000 millones.

La secretarías de Hacienda y Salud enviaron una circular a los gerentes de la red de hospitales públicos de la cuidad conformada por 22 centros, donde se informa sobre la medida, dándoles **hasta el 25 de enero para presentar el nuevo reajuste presupuestal de cada hospital.**

“Nos parece una irresponsabilidad, parece que los secretarios no tienen conocimiento de cómo trabajan los hospitales de la ciudad, **es imposible que reduciendo en un 40% no se vallan a desmejorar servicios y a despedir trabajadores”** asegura María Doris González, presidenta del Sindicato Nacional de de salud y seguridad social, quien agrega que los trabajadores de este sector  se encuentran muy preocupados por el anuncio.

González, señala que cada año sin ningún tipo de orden desde la Alcaldía, se está reduciendo el presupuesto de los centros hospitalarios, porque el gobierno pretende que la entidades de salud lleguen a ser productivas como un operador privado, “lo poco que queda público lo va a entregar a privados”.

**Los 22 hospitales se verían afectados por la medida.** Los casos más graves, serían el **Hospital Meissen, Hospital San Blas, Hospital del Tunal, Hospital Centro Oriente, Hospital Simón Bolívar, y el Hospital Santa Clara,** teniendo en cuenta que hace algunos días, el propio secretario de Salud, **Luis Morales, había indicado que la mayoría de** los centros médicos de mayor complejidad de la ciudad, públicos y privados, presentan situación de hacinamiento y falta de especialistas, además explicó que el número de camas para pacientes enfermos con los que cuenta hoy el Distrito es casi el mismo de hace diez años.

“El Hospital San Blas, de segundo nivel, sería uno de los más afectados, ya que tiene un déficit de 27 mil millones de pesos por las deudas de las EPS, y ahora con lo poquito que tiene deberá reducir su presupuesto en un 40%”,  ejemplifica María Doris González, afirmando que a “esta administración no le interesa que la red pública de hospitales siga siendo pública”.

Frente a esa situación, los sindicatos del distrito han definido una coalición de diversas organizaciones que se unirán para defender las entidades públicas que Peñalosa ha pensado privatizar. Así mismo, se podrán en la tarea de informar a la ciudadanía sobre los efectos de este recorte presupuestal, de manera que las personas se movilicen para exigir un “**basta ya del paseo de la muerte y de la intermediación de las EPS”,** concluye González.

###### [[Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). ]] 
