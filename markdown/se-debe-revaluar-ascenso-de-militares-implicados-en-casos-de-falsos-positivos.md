Title: Militares implicados en “falsos positivos” no deben ser ascendidos: HRW
Date: 2016-11-10 11:38
Category: DDHH, Nacional
Tags: Ascenso de militares, Ascensos en las FFMM, falsos positivos, justicia, madres de soacha, militares, Ocaña y falsos positivos, Victimas falsos positivos
Slug: se-debe-revaluar-ascenso-de-militares-implicados-en-casos-de-falsos-positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Jose-Miguel-Vivanco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: En País Zeta] 

###### [10 Nov. De 2016] 

En un comunicado la organización internacional Human Rights Watch – HRW- manifestó que es necesario que se **suspendan los ascensos que se tienen pensados realizar a militares**, en su mayoría generales y coroneles que han estado **implicados en el tema de las ejecuciones extrajudiciales, conocidas como “falsos positivos”.**

Según la misiva, el listado que dio a conocer el Ministerio de Defensa tiene los nombres de al menos 5 generales o coroneles que han sido salpicados por este escándalo que fue revelado en el gobierno del ahora senador Álvaro Uribe Vélez.

El director de esta organización, José Miguel Vivanco, manifestó que **el Senado colombiano debería examinar las hojas de vida cuidadosamente y rechazar los ascensos de militares contra quienes existan evidencias creíbles de abusos y afirmó que “de lo contrario, se reforzaría el mensaje de que en Colombia los oficiales no deben rendir cuentas por sus crímenes”.**

El listado de miembros del Ejército que se conoció y que generó este tipo de comunicación de parte de HRW, fueron los brigadieres generales **Emiro José Barrios Jiménez y Jorge Enrique Navarrete Jadeth** y los coroneles **Marcos Evangelista Pinto Lizarazo, Edgar Alberto Rodríguez Sánchez y Adolfo León Hernández Martínez**.  En contexto: [Militares vinculados con falsos positivos podrían ascender a generales](https://archivo.contagioradio.com/militares-vinculados-con-falsos-positivos-podrian-ascender-a-generales/)

Dicha comunicación, hizo que los senadores de la Comisión Segunda y el ministerio de Defensa se reunieran de emergencia para poder dialogar sobre este tema y tomar algún tipo de medida al respecto.

**¿Quiénes son los posibles ascendidos?**

Al brigadier general **Emiro José Barrios Jiménez, la Fiscalía lo investiga** por su rol en “falsos positivos” durante su comandancia de la Octava Brigada del Ejército durante el 2007, 2008 y 2009. Las investigaciones son **por al menos 19 muertes presuntamente perpetradas en 2008 por soldados de la brigada que el comandaba.**

**Jorge Enrique Navarrete Jadeth**, es brigadier general actualmente. **Navarrete se encuentra bajo investigación penal por homicidios cometidos por la Octava Brigada, así como por su presunta cooperación con grupos paramilitares.** Human Rights Watch ha manifestado que el Brigadier pagó de 2 millones de pesos a un informante por “la muerte en combate de dos terroristas”.  Le puede interesar: [Durante comandancia del general Montoya se documentaron 4.300 falsos positivos](https://archivo.contagioradio.com/durante-comandancia-del-general-montoya-se-documentaron-4-300-falsos-positivos/)

**El coronel Marcos Evangelista Pinto Lizarazo** estuvo entre diciembre de 2007 y julio de 2008 al mando del Batallón Magdalena de la Novena Brigada, al cual **le han abierto investigaciones por al menos 19 muertes, que según la Fiscalía fueron ejecutadas por soldados de dicho Batallón que estaba a su mando.**

En julio de 2011 **cinco miembros del Batallón Popa de la Décima Brigada fueron condenados por un tribunal de Valledupar por asesinar a un civil y entregarlo como un guerrillero del ELN. El coronel Adolfo León Hernández Martínez, que está en la lista de posibles ascendidos estuvo al frente de dicho Batallón.** Adicionalmente en 2013, fueron condenados otros cinco militares de este mismo Batallón por el asesinato de dos civiles en el municipio de Pueblo Bello y presentarlos como N.N. muertos en combate.

Según cifras de la ONG HRW **cerca de 800 integrantes del Ejército de Colombia han sido condenados por ejecuciones extrajudiciales. **Le puede interesar: [Denuncian 2 nuevos ‘falsos positivos judiciales’ en Córdoba](https://archivo.contagioradio.com/denuncian-2-nuevos-falsos-positivos-judiciales-en-cordoba/)

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
