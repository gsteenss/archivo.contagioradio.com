Title: Daupará, Muestra de Cine y Video Indígena abre sus convocatorias
Date: 2017-07-14 15:00
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Cultura, indigena
Slug: daupara-cine-indigena
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/cine-indígena-600x407.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Daupará 

###### 13 Jul 2017 

"Renaciendo en Bakatá" es el slogan con el que este año la muestra de cine y video indígena Daupará, convoca a los realizadores para que envíen sus producciones y hagan parte del evento que en 2017 cumple 9 años de existencia resignificándose desde el sentir ancestral del territorio Muisca.

Para la presente edición, el objetivo de los organizadores es visibilizar producciones audiovisuales exclusivamente colombianas, para lo cual han creado dos categorías: "Nuestras miradas" obras que provengan del pensamiento indígena, sean escritas, dirigidas o realizadas por por autores pertenecientes a grupos indígenas y "Miradas que acompañan" realizadas por quienes se sintonizan con la voz de los pueblos indígenas.

En cuanto al contenido, algunas de las condiciones para paticipar son que las producciones aporten al fortalecimiento de la identidad y las culturas de los pueblos indígenas, promuevan el diálogo intercultural, resalten las expresiones culturales en situaciones de desaparición, presenten cualidades estéticas y aporten al lenguaje audiovisual del cine indígena y que planteen perspectivas originales y la búsqueda de narrativas propias en sintonía con el pensamiento de los pueblos indígenas.

La fecha de recepción de los trabajos será hasta el 31 de agosto y los resultados de la convocatoria serán publicados el 8 de octubre. La duración y el formato de las piezas audiovisuales es libre en cualquiera de los géneros: ficción, documental, animación, experimental, entre otros. (Le puede interesar: [Territorio, resistencia y cine en el Distrito de Aguablanca](https://archivo.contagioradio.com/42904/))

Para las obras que se encuentran habladas en lenguas originarias se requiere que contengan subtítulos en castellano. Los organizadores hacen además la claridad que no se tendrán en cuenta producciones que de manera directa promuevan partidos políticos, instituciones gubernamentales, no gubernamentales y movimientos religiosos.

![daupara](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/daupara.png){.alignnone .size-full .wp-image-43637 .aligncenter width="1610" height="783"}

Los interesados en participar pueden diligenciar la ficha de inscripción[on line](https://docs.google.com/forms/d/1oqHFANjSy7DtKeO1P7RyPA8t-%20hWMh7DJblTH1XK26VU/edit), adjuntando el link al video (en youtube, vimeo, mega) con sus respectivas claves de acceso. Quiénes no cuenten con un enlace de visionado, pueden enviar 2 copias dvd en físico, en un sobre cerrado con el título de “sin valor comercial, destinado a un evento cultural” a Daupará - Muestra de Cine y Video Indígena de Colombia Calle 11A \# 78D.

La Muestra de cine y video indígena Daupará tendrá lugar en Bogotá del 21 al 25 de noviembre en la ciudad de Bogotá. Para mayor información escriba a convocatoriadaupara@gmail.com
