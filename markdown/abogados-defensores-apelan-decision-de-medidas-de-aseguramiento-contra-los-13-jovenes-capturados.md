Title: Abogados defensores apelan decisión de medidas de aseguramiento contra 13 jóvenes capturados
Date: 2015-07-28 15:18
Category: DDHH, Nacional
Tags: 20 de mayo, Atentados en Bogotá, Congreso de loa Pueblos, Fiscalía, Jóvenes capturados, medida de aseguramiento, protestas estudiantiles, Universidad Nacional
Slug: abogados-defensores-apelan-decision-de-medidas-de-aseguramiento-contra-los-13-jovenes-capturados
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/11791108_10153353885220020_1515040779_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

Esta mañana, **la juez 72 de Bogotá dictó medida de aseguramiento contra los 13 jóvenes detenidos por las manifestaciones estudiantiles** del pasado 20 de mayo en la Universidad Nacional. Sin embargo, la audiencia continúa debido a que la bancada de la defensa **apeló esta decisión.**

A esta hora se da paso a que cada abogado de a conocer los argumentos por los cuales no se está de acuerdo con la medida de aseguramiento, ya que existirían varias irregularidades (ver [¿Cuáles son los argumentos de los abogados para que los 13 jóvenes no vayan a la cárcel?](https://archivo.contagioradio.com/cuales-son-los-argumentos-de-los-abogados-para-que-los-13-jovenes-no-vayan-a-la-carcel/)).

La juez argumentó, que “si los dejamos en libertad no solo podrían entorpecer la investigación sobre una serie de atentados en Bogotá, tratando de influir sobre testigos, sino que además podrían poner en peligro a la Universidad Nacional”.

Ante la posibilidad de que los jóvenes sean un peligro para la sociedad, uno de los abogados defensores de los jóvenes, dijo esta mañana en los micrófonos de Contagio radio : “cuando se habla de peligro para la comunidad, de qué comunidad se está hablando, si es la comunidad movilizada que está exigiendo la libertad; o la comunidad internacional, que está pidiendo garantías judiciales y la comunidad académica que ha respaldado el trabajo político y académico de algunas de las personas detenidas… **entonces la gran pregunta es ¿qué peligro para la sociedad es el que se está argumentando?**”, señaló  David Uribe.

Aun así, la juez negó la medida de privación domiciliaria solicitada por los abogados defensores, **lo que para ellos implica una violación a la defensa.**

Algunos de los argumentos de la defensa es que **se está aplicando una condena social por hechos no probados**, además las medidas de aseguramiento no pueden ser colectivas, deben ser individuales. Por otro lado, aseguran que **no se debería tomar esta decisión por una protesta estudiantil.**

Cabe recordar que la Fiscalía señala a los 13 jóvenes por delitos como **terrorismo y rebelión,**  violencia contra servidor público
