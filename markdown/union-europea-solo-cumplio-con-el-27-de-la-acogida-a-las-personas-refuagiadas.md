Title: Unión Europea solo cumplió el 27% en cuota de acogida a personas refugiadas
Date: 2017-09-26 15:21
Category: DDHH, El mundo
Tags: Refugiados, Unión europea
Slug: union-europea-solo-cumplio-con-el-27-de-la-acogida-a-las-personas-refuagiadas
Status: published

###### [Foto: Sumadiario.com] 

###### [26 Sep 2017] 

Se terminó el plazo para que la Unión Europea acoja a 160.000 refugiados como lo había prometido en el año 2015, pero los países incumplieron lo pactado. Así lo evidencian las cifras donde se muestra que solo se acogió a **44.300 personas, es decir, apenas se cumplió con el 27% del acuerdo alcanzado** en julio de 2015.

En un primer momento los [**países que hacen parte de la UE acordaron dar asilo a**] **32.256 s**olicitantes de Italia y Grecia. Una cifra que se aumentó 120.000 más tras la trágica imagen de Aylan, el niño que apareció muerto en una playa de Turquía. Sin embargo, luego Hungría se salió de la UE y su compromiso de acoger a 54.000 refugiados quedó sin ningún responsable.

"**La Unión Europea es incapaz de cumplir sus compromisos de mínimos **y apenas mueve un dedo para que el Mediterráneo deje de convertirse en una fosa común cada vez más grande", denuncia el Comité Español de Ayuda al Refugiado, (CEAR) que ha trabajado especialmente en España, donde apenas se cumplió con un 11%, es decir solo brindaron asilo a 1980 personas de las 17.337 a las que se habían comprometido.

### **Los obstáculos** 

En 2016 murieron 7.927 refugiados en el mundo en las rutas migratorias, pero este año la cifra no parece desacelerarse, **a agosto de 2017, ya se contaban 4.002 personas muertas, ** según informó la Organización Internacional para las Migraciones.

Sin embargo, en medio de ese panorama,  este lunes, la Comisión Europea rechazó la idea de referirse a la política de ayudas a los refugiados como un "fracaso", pues el comisario europeo de Migración, Dimitris Avramopoulos afirmó que "Desde nuestro punto de vista, el programa de reubicaciones ha sido un éxito".

[Pese a esa visión de la Comisión Europea, Patricia Bárcena, directora del CEAR Euskadi, explica una situación muy diferente. "Nosotros decimos desde hace muchos años que el derecho de asilo es una carrera de obstáculos: el primero es salir de tu país, el segundo es llegar a un país seguro, y el tercero es pedir la protección en un sistema jurídico que tiende a denegar la protección, por unos parámetros de exclusión que hacen que sea casi imposible acceder a ese derecho".]

### **¿Qué se debe hacer?** 

Las organizaciones defensoras de derechos humanos del mundo hacen dos solicitudes concretas. Por un lado, que la sociedad en general y particularmente los **medios de comunicación dejen de alimentar el discurso del miedo y el terrorismo,** pues se ha generado un rechazo a las comunidades de migrantes presentes en los países europeos, y por tanto, se han fortalecido ideas para impedir la llegada de refugiados que huyen de sus países por las múltiples guerras y carencias.

En la otra vía, CEAR por ejemplo, ha pedido que "se **reimpulsen los programas de reubicación y reasentamiento** con una revisión inmediata de los criterios, como el de que sólo son reubicables personas de tres nacionalidades (siria, eritrea e iraquí), dejando fuera a otras personas que huyen de la guerra en sus países y que también tendrían derecho a protección internacional", como lo señala el periódico español El Mundo.

Asimismo, desde CEAR piden que haya una mejor **coordinación** **entre la agencia europea de asilo, los diferentes gobiernos y las autoridades italianas y griegas** para agilizar todas las fases del proceso de acogida y declaración de asilo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
