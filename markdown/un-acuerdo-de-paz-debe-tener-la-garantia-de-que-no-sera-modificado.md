Title: Un acuerdo de paz debe tener la garantía de que no será modificado
Date: 2015-09-19 17:16
Category: Entrevistas, Paz, Política
Tags: Asesinato de Luiber Arroyo, Congreso de Colombia, Diálogos de La Habana, justicia transicional en colombia, Mecanismo de justicia transicional proceso de paz en Colombia, Paramilitarismo, Polo Democrático, Proceso de paz en Colombia, Senado, Senador Ivan Cepeda
Slug: un-acuerdo-de-paz-debe-tener-la-garantia-de-que-no-sera-modificado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: panampost.com] 

<iframe src="http://www.ivoox.com/player_ek_8463097_2_1.html?data=mZmjlZWde46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRlNDg0JCxx9LTp9OZpJiSo5bYrcTjjMnS1cbUttbZw8aYz8rHpc%2Fd1NLcjcnJb9PZx9fS0MnFp8qZpJiSpJjScYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ivan Cepeda, Polo Democrático] 

###### [18 Sept 2015] 

[Tras la radicación del acto legislativo para la paz en el Congreso, el Polo Democrático declaró que no participará de los debates, pues según el senador Iván Cepeda **la discusión de un proyecto de ley  de esta envergadura debe darse cuando sea abordado el sexto punto de la agenda de negociación**.]

[El proyecto, radicado en el Congreso el pasado martes, contempla la **creación de una comisión legislativa especial** integrada por congresistas de las comisiones primeras del Senado y Cámara, 19 senadores y 35 representantes, así como por otros 12 parlamentarios elegidos por la mesa directiva del Congreso, **para implementar los acuerdos de paz que se espera sean firmados en La Habana**.]

[Al respecto, Cepeda asegura que el Polo Democrático no se opone a la creación de un mecanismo legislativo para que los acuerdos sean refrendados, pero insisten en que **los acuerdos a los que se lleguen deben ser pactados entre la delegación del gobierno nacional y las FARC**, sólo cuando ello se dé será posible discutir su forma de implementación.]

[El Senador insiste en que la negativa del Polo, se centra en el conocimiento de experiencias pasadas, “sería un gravísimo error que una vez que se hayan llegado a acuerdos en La Habana se dé vía libre al Congreso para que legisle en la materia”, pues se estima que puede haber retrocesos en lo ya adelantado.]

[El Senador reiteró que hechos como el reciente asesinato de concejal del Polo, Liuber Arroyo, en el municipio del Charco, Nariño, demuestran que pese a los avances en La Habana, **el paramilitarismo y la represión a los sectores sociales y de izquierda, continúan.**]

[Por lo que se debe exigir a las partes en negociación, que **no finalicen los diálogos hasta que no haya garantías reales de desmonte del paramilitarismo**, pues la persistencia de su accionar podría poner **en riesgo la integridad de quienes decidan dejar las armas y articularse en un partido político**. ]
