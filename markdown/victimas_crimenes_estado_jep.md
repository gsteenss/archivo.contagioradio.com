Title: Víctimas de crímenes de Estado exigen celeridad en investigaciones de la JEP
Date: 2018-03-15 15:50
Category: Nacional, Paz
Tags: crímenes de estado, JEP, justicia especial para la paz, MOVICE
Slug: victimas_crimenes_estado_jep
Status: published

###### [Foto: MOVICE] 

###### [15 Mar 2018] 

Mediante una carta, el Movimiento de Víctimas de Crímenes de Estado, le manifiesta a la presidenta de la Jurisdicción Especial para la Paz, Patricia Linares, sus expectativas antela puesta en marcha de ese escenario de justicia de cara a las víctimas, pero a su vez piden que exista un **trato igualitario para todos aquellos afectados por el conflicto**.

"Nosotras, las víctimas de crímenes de Estado, entramos al sistema ubicadas en un lugar desigual. La Fiscalía tiene investigaciones de la macrocriminalidad sobre casos de la guerrilla, ya tiene casos ordenados, pero no tiene nada organizado sobre crímenes de Estado para investigar. Pedimos un trato justo y simétrico como víctimas con respecto a las demás", dice el documento dirigido a Linares.

Señalan por ejemplo que la JEP inicia su tarea este 15 de marzo con **un total de 7.392 postulados, de los cuales solo el 24,6% son agentes del Estado**: 1.824 militares y policías, procesados y condenados en un alto porcentaje por ejecuciones extrajudiciales.

También preguntan sobre las razones por las cuales se definió la manera como se distribuyó el personal para investigar los crímenes en el marco de la guerra.  Habrá "**7 Fiscales ante el Tribunal de paz para investigar y acusar a las FARC; 1 para investigar y acusar a terceros y agentes del Estado civiles y tan sólo 3 para investigar y acusar a miembros de la Fuerza Pública.** Le preguntamos a Usted y a todos los directivos de la JEP, ¿Cuáles son las razones de esta diferencia?", expresan.

### **Las exigencias del MOVICE** 

Uno de los elementos que las víctimas de crímenes de Estado exigen que se tenga en cuenta, tiene que con que los miembros de la Fuerza Pública postulados ante la JEP fueran no sólo los responsables por acción, sino también por omisión, teniendo en cuenta que muchos de los hechos victimizantes por parte de los grupos paramilitares sucedieron ante los ojos de Fuerza Pública.

Por otro lado, destacan la importancia que la JEP funcione a nivel regional y no sea centralizada, sin embargo, afirman que **solo once fiscales adscritos a la Unidad de Investigación, son muy pocos si se quiere lograr una verdadera **presencia territorial.

Asimismo, exigen mejores garantías de participación, pues manifiestan que **"deben ser incluidas expresamente las facultades de las víctimas como sujeto interviniente especial dentro de la JEP",** y por tanto, esperan que la Ley de procedimiento presentada la semana pasada al presidente Juan Manuel Santos, contemple esa disposiciones, y a su vez, dicho proyecto de Ley consensuado con las víctimas.

Además indican como "fundamental que al momento de abordar los casos por parte de las respectivas salas se excluyan de la JEP aquellos casos en los que se compruebe el ánimo de obtener enriquecimiento personal ilícito".

### **La impunidad actual** 

**Según el MOVICE, el 99.9% de los crímenes de Estado continúan en la impunidad debido a que las políticas estatales siguen siendo ineficientes** para dar con la verdad sobre estos hechos,  sumado a ellos, las investigaciones no avanzan significativamente para hallar con los responsables intelectuales.

En ese sentido subrayan que **la Fuerza Pública es responsable directa de la comisión de crímenes que generaron más de 62.373** **víctimas**: 171 masacres que generaron 968 víctimas; 51.676 personas desplazadas; por lo menos 2.484 personas desaparecidas forzosamente; 206 mujeres violadas sexualmente y 7.039 personas ejecutadas extrajudicialmente.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
