Title: Caso de Dilan Cruz es enviado a la Justicia Penal Militar
Date: 2019-12-18 11:56
Author: CtgAdm
Category: DDHH, Judicial
Tags: Consejo Superior de la Judicatura, Dilan Cruz, ESMAD, Justicia Penal Militar
Slug: caso-de-dilan-cruz-es-enviado-a-la-justicia-penal-militar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Dilan-Cruz-publimetro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Publimetro  
] 

La Sala Jurisdiccional Disciplinaria del Consejo Superior de la Judicatura resolvió el conflicto de competencias entre la Jurisdicción Penal Ordinaria y la Jurisdicción Penal Militar en el caso del homicidio de Dilan Cruz, dejando el caso en esta última Jurisdicción. De esta forma, **el capitán que disparó contra la humanidad de Dilan sería juzgado por una instancia que en el pasado ha sido cuestionada desde organismos internacionales como la Organización de Estados Americanos (OEA),** por no garantizar el derecho a la justicia de las víctimas al carecer de independencia.

### **Los actos del uniformado "se dieron en el marco de un acto del servicio que cumplía un agente del ESMAD"** 

El conflicto de competencias por el caso del homicidio de Dilan Cruz se planteaba entre el conocimiento del caso que tendría el Juzgado 189 de Instrucción Penal Militar de Bogotá y la Fiscalía 298 Seccional Unidad de Vida de Bogotá. En este caso, el Consejo Superior de la Judicatura, organismo idóneo para resolver este conflicto de jurisdicciones, decidió que la competencia del caso le correspondía al Juzgado 189, **en tanto los hechos en los que resultó herido Dilan "se dieron en el marco de un acto del servicio que cumplía el agente del ESMAD involucrado en los mismos".**

En el comunicado de la decisión, la Sala Jurisdiccional Disciplinaria del Consejo señala que la decisión no significa que habrá impunidad con Dilan, pues el hecho de remitir su caso a la justicia castrense no significa que "en el mismo no existirá una verdadera y cumplida administración de justicia". (Le puede interesar: ["Dilan, Brandon y Juan David: rostros de la ausencia de garantías para los jóvenes en Colombia"](https://archivo.contagioradio.com/dilan-brandon-y-juan-david-rostros-de-la-ausencia-de-garantias-para-los-jovenes-en-colombia/))

### **¿Es un acto de servicio lo ocurrido con Dilan?** 

Un abogado penalista consultado por Contagio Radio señaló que la determinación había sido tomada por el ente idóneo para decidir sobre este asunto, no obstante, **cuestionó que efectivamente sea un acto del servicio disparar contra la humanidad de Dilan**, tomando en cuenta que para hacer dichas intervenciones el ESMAD cuenta con protocolos claros de actuación que no se cumplieron. (Le puede interesar: ["ESMAD usó material no convencional y violó protocolos en movilizaciones estudiantiles"](https://archivo.contagioradio.com/esmad-uso-material-no-convencional-y-violo-protocolos-en-movilizaciones-estudiantiles/))

Adicionalmente, el especialista añadió que se podría intentar acudir a la acción de tutela, revisando si la decisión desconoce derechos fundamentales preservados en la jurisprudencia, para intentar que el uniformado sea juzgado por la Justicia Penal Ordinaria. (Le puede interesar: ["Así avanza la (in)justicia en el caso de Nicolás Neira"](https://archivo.contagioradio.com/asi-avanza-la-injusticia-en-el-caso-de-nicolas-neira/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
