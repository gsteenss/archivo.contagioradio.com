Title: Más de 600 campesinos se movilizan en Córdoba por su derecho a la educación
Date: 2015-02-27 17:04
Author: CtgAdm
Category: Movilización, Nacional
Tags: Campesinos de Córdoba, Derecho a la eduación en Colombia, Movilización Comunitaria
Slug: mas-de-600-campesinos-se-movilizan-en-cordoba-por-su-derecho-a-la-educacion
Status: published

###### Foto: aliriouribe.com 

<iframe src="http://www.ivoox.com/player_ek_4143638_2_1.html?data=lZahlZuXfI6ZmKiakpyJd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmptjhw8nTb8LX0Nffw9HFb8LgjMjOz9XJt8ri0JDQ0dOPuc%2FVjMnSjdjZt4zV09LO1ZDQqdXVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Andrés Chica, Asociación de Campesinos del Sur de Córdoba] 

**Más de 600 campesinos y campesinas de Soledad, Rojero, Mutata, Tierra Adentro, San Juan, San José, La Rica, en el departamento de Córdoba**, se movilizan este viernes 27 de febrero para exigir al gobierno nacional, **colegios para que sus hijos e hijas puedan estudiar.**

Para **Andrés Chica**, defensor de Derechos Humanos miembro de la Asociación de Campesinos del Sur de Córdoba, el analfabetismo en la región es de aproximadamente el 40%. Hace más de 17 años, el sur del Departamento no cuenta con maestros ni infraestructura adecuada para la educación de niños, niñas y adolescentes, motivo por el cual, generaciones de abuelos, padres, hijos e hijas no saben leer ni escribir, y se dedican a lo único que han aprendido por generaciones: sembrar coca.

"Los niños, niñas y adolescentes están tomando la guerra como opción de vida, porque no estudian, no hay garantías de trabajo, y no hay producción agropecuaria, y todo esto es resultado de no educar a los campesinos y las campesinas", señaló Chica. "El terrorismo de Estado acorrala al campesino con una de sus armas letales: la no educación".

Desde las 3 de la mañana del viernes 27 de febrero, campesinos y campesinas de toda la región, organizados en juntas de acción comunal y asociaciones campesinas, se trasladaron hacia el casco urbano de Puerto Libertador en una gran movilización, exigiendo al gobierno nacional los escuche, y brinde las respuestas que la alcaldía municipal y departamental no pudieron, o no quisieron cumplir.

De no recibir respuesta por parte del Ministerio de Educación Nacional, los campesinos y campesinas iniciarían un proceso de paro en el Sur de Bolívar.
