Title: Que Duque no nos obligue a volver al clóset: Caribe Afirmativo
Date: 2018-06-29 17:11
Category: LGBTI, Nacional
Tags: Día del Orgullo Gay, Iván Duque, JEP, proceso de paz
Slug: duque-no-nos-obligue-a-volver-al-closet-caribe-afirmativo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/marcha-lgbti.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [ Diego Cambiaso](https://www.flickr.com/photos/djc/9191821827/in/album-72157634449856358/)] 

###### [29 Jun 2018] 

A propósito de la celebración del **Día del Orgullo Gay, William Castañeda, Director de la **Corporación Caribe Afirmativo**,** señalo que este año las conmemoraciones estarán enmarcadas por dos elementos relevantes: la elección de un presidente que no tiene ningún compromiso con la agenda **LGBT** y que por el contrario, tiene personas cercanas en su campaña que atacan los derechos de las diversidades sexuales; y que es la segunda marcha que se realiza en el país tras la firma del acuerdo de paz.

Sobre el primer punto, Castañeda pide "que no se retroceda en materia de derechos a la población LGBT", pues aunque en su discurso de posesión, **Iván Duque** dijo: "Nosotros no vamos a despojar a nadie de los derechos que han conseguido en nuestro país", lo cierto es que hasta el momento se contradice, muestra de ello es la exclusión del enfoque de género que hizo el **Centro Democrático, en el Proyecto de Ley de Procedimiento de la JEP.**

Hecho que para Castañeda representa, "rechazar que hubo violencia basada en el género durante el conflicto armado; no solo contra las mujeres sino contra las personas que viven en otra forma de construcción de la matriz sexo-género". **Lo que implica que el Centro Democrático quiere desconocer a las personas que integran la diversidad sexual como víctimas del conflicto. **(Le puede interesar: ["Aún se puede defender el espíritu de la JEP"](https://archivo.contagioradio.com/aun-se-puede-defender-la-jep/))

### Que el gobierno de Duque no nos obligue a volver al clóset. 

Adicionalmente, la movilización es un llamado a la implementación del acuerdo de paz, porque las cifras que la Corporación Colombia Diversa presentará el próximo jueves en su informe anual, muestran que **el proceso de paz ha significado una reducción de la violencia contra personas LGBT** en los territorios en los que antes había conflicto armado.

Pese a ello, hay dos cosas que preocupan a Castañeda: Se siguen presentando actos de violencia por cuenta de la construcción que han de las feminidades y masculinidades quienes pertenecen a la comunidad LGBT, y que se sigue manteniendo la impunidad en casos de violencia que afecta a esta comunidad. (Le puede interesar: ["La deuda de Colombia con la población LGBTI"](https://archivo.contagioradio.com/la-deuda-de-colombia-con-la-poblacion-lgbti/))

Por eso la invitación de Castañeda es a: "Primero, tener incidencia en la política, tanto local como nacional; segundo, lograr una coalición ciudadana en defensa de los derechos LGBT, así como de los derechos sexuales y reproductivos y de las mujeres; y tercero, trabajar por una cultura ciudadana que respete la diversidad y la diferencia".

Al nuevo Gobierno de Iván Duque le pide que "no nos obligue a volver al clóset".

### ¿Por qué se realizan marchas por el Orgullo Gay? 

Esta marcha, que conmemora la golpiza que recibieron un 28 de junio de 1969, un grupo de personas LGBT en Nueva York -en su mayoría latinoamericanos indocumentados-  a manos de policías, se celebra hace cerca de 20 años en Colombia. Castañeda aclara que dicha golpiza generó una ola de marchas reivindicatorias que continúan hasta el día de hoy, y en el marco de esas marchas hay dos hechos recientes por los que salen a marchar particularmente en Colombia:

En primera instancia que "llego a la presidencia de la república un candidato que lamentablemente no tuvo ningún compromiso con las agendas LGBT y por el contrario, en un debate planteó quitar un derecho adquirido por nosotros y nosotras como el matrimonio igualitario". Adicionalmente, Castañeda aseguró que en su campaña tuvo al lado enemigos históricos de las reivindicaciones de género como el ex-procurador Alejandro Ordoñez y la senadora Viviane Morales.

Así mismo, manifestó que es la segunda marcha que se hace durante la implementación del acuerdo de paz, y es una movilización para que se continue y profundice dicha implementación del acuerdo "**porque si la paz no avanza en Colombia, la igualdad no será una realidad."**

![infografia lgbti colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/infografia-lgbti-colombia-532x800.png){.alignnone .size-medium .wp-image-54412 .aligncenter width="532" height="800"}<iframe id="audio_26805870" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26805870_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada] [por ][Contagio Radio](http://bit.ly/1ICYhVU). 
