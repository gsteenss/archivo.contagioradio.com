Title: Ellas y ellos son los ganadores del Premio Nacional a la defensa de Derechos Humanos 2018
Date: 2018-09-05 12:34
Author: AdminContagio
Category: DDHH, Nacional
Tags: Defesores de Derechos Humanos, Diakonia, lideres sociales
Slug: estos-son-los-y-las-ganadores-de-la-septima-version-del-premio-nacional-a-la-defensa-de-derechos-humanos
Status: published

###### [Foto: Contagio Radio] 

###### [05 Sept 2018] 

Se llevó a cabo **la séptima versión del Premio Nacional a la Defensa de los Derechos Humanos Diakonia **que otorga un reconocimiento público en 4 categorías que buscan visibilizar el aporte de los y las defensoras de derechos humanos a la construcción de democracia y paz en Colombia.

Los ganadores de este año hacen parte de procesos históricos en defensa de derechos en el país como la Comunidad de Paz de San José de Apartadó, el Movimiento Ríos Vivos, la Junta de Gobierno de la Comunidad Negra/Afrodescendiente de Alto Mira y Frontera y el liderazgo de la matriarca María Lligía Chaverra, en el territorio Colectivo del Curvarado.

### **Categoría 1: "Defensor o Defensora del Año** 

En la categoría de Defensor o Defensora del año, el premio fue otorgado a Germán Graciano Posso, perteneciente al Consejo Interno de la Comunidad de Paz de San José de Apartadó, en el Uraba Antioqueño. Posso, **conformó y organizó frentes de trabajo para realizar un proyecto de vida junto con más de 500 personas**. Su trabajo ha estado centrado en la defensa del territorio y el bienestar para su comunidad.

\[caption id="attachment\_56315" align="alignnone" width="800"\]![Contagio radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/WhatsApp-Image-2018-09-05-at-11.45.20-AM-800x451.jpeg){.size-medium .wp-image-56315 width="800" height="451"} Contagio radio\[/caption\]

### **Categoría 2A: "Experiencia o Proceso Colectivo del Año (Proceso Social Comunitario)"** 

Junta de Gobierno de la Comunidad Negra de Alto Mira y Frontera, en Tumaco- Nariño, fue la que se llevo el reconocimiento a la Experiencia o Proceso Colectivo del Año. Este proceso se ha encargado de visibilizar las luchas de las comunidades afro en este territorio, las violaciones a derechos humanos y denunciar el asesinato a líderes sociales y la detención por parte del Estado de sus integrantes, como son los casos **particulares de Tulia Maris Valencia y Sara Quiñones.**

\[caption id="attachment\_56316" align="alignnone" width="800"\]![Contagio Radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/WhatsApp-Image-2018-09-05-at-11.55.32-AM-800x451.jpeg){.size-medium .wp-image-56316 width="800" height="451"} Contagio Radio\[/caption\]

**Categoría 2B: "Experiencia o Proceso Colectivo del Año (Modalidad de ONG)"**

En esta categoría, el Movimiento Ríos Vivos se llevo este reconocimiento. Esta plataforma, que agremia a varias comunidades afectadas por el proyecto Hidroituango, en el sur occidente del departamento de Antioquia, ha promovido la defensa del territorio y el ambiente perjudicado por la construcción de la represa. Además han defendido el derecho de las víctimas de desaparición forzada, **que estaría en fosas comunes ubicadas justo en donde se esta desarrollando este proyecto.**

\[caption id="attachment\_56318" align="alignnone" width="800"\]![Contagio Radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/WhatsApp-Image-2018-09-05-at-12.05.46-PM-800x451.jpeg){.size-medium .wp-image-56318 width="800" height="451"} Contagio Radio\[/caption\]

### **Categoría 3: Reconocimiento a "Toda una Vida"** 

María Ligia Chaverra, , representante de la organización Zona Humanitaria Las Camelias, fue la ganadora al reconocimiento por "Toda una Vida" en la defensa de derechos humanos. Chaverra encarna a quienes han sido víctimas de la violencia del conflicto armado en el país, que con su liderazgo ha logrado **resistir, defender y reivindicar los derechos humanos en el territorio Colectivo del Curvaradó,** en el Bajo Atrato.

Heredera de las luchas ancestrales de las comunidades afrodecendientes, la matriarca ha sabido afrontar las múltiples amenazas de la que ha sido víctima y la criminalización por parte del Estado.

![Contagio Radio](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/09/WhatsApp-Image-2018-09-05-at-12.17.18-PM-800x451.jpeg){.alignnone .size-medium .wp-image-56322 width="800" height="451"}

[**Homenaje a Jesús María Valle**]

De igual forma este año se hizo un homenaje especial al abogado **Jesús María Valle, defensor de derechos humanos asesinado el 27 de febrero de 1998.** Debido a la trascendencia de su trabajo era conocido como el Apóstol de los derechos humanos. Jesús María Valle nació en el corregimiento de El Aro, en el municipio antioqueño de Ituango, el 28 de febrero de 1943.

Frente al trabajo del abogado, su sobrina Luz Adriana Valle hizo un llamado a los demás líderes sociales y defensores de derechos humanos para "persistir y nunca desistir, a no declinar y a no dejarse vencer por el mal" en el arduo trabajo que realizan día a día.

El Programa Colombia de Diakonia en conjunto con la Iglesia Sueca, ha otorgado este premio desde el año 2012 **como un mecanismo de respaldo al trabajo que realizan los defensores de derechos humanos y del territorio en el país**.

Como en años pasados, el reconocimiento a los hombres, mujeres y organizaciones que contribuyen a la democracia y la paz se hará como parte de un mecanismo de protección ante la alarmante situación de riesgo en la que viven estas personas en Colombia. (Le puede interesar: ["21 líderes sociales han sido en lo corrido del Gobierno Duque"](https://archivo.contagioradio.com/durante-duque-asesinados-21-lideres/))

###### [Reciba toda la información de Contagio Radio en [[su correo] 
