Title: Se afianza presencia paramilitar a pesar de existencia de Fuerza Pública en Rio Sucio, Chocó
Date: 2018-04-02 11:00
Category: DDHH, Nacional
Tags: amenazas de paramilitares, comunidades de chocó, Jiguamiandó, paramilitares, Río Sucio
Slug: se-afianza-presencia-paramilitar-a-pesar-de-existencia-de-fuerza-publica-en-rio-sucio-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Primicia Diario] 

###### [02 Abr 2018] 

Las comunidades del municipio de Rio Sucio en el Chocó, denunicaron que el lunes 26 de marzo “neoparamilitares” de las Autodefensas Gaitanistas de Colombia amenazaron con asesinar a **“sapos, viciosos y violadores”**. Esto en la vía que comunica a la vereda de Pavarandó con Llano Rico. Denunciaron también que estos grupos actúan con el conocimiento de la Fuerza Pública en ese territorio.

A través de la Comisión Intereclesial de Justicia y Paz, se supo además que el martes 27 de marzo a la 1:00 pm el mismo grupo de 80 hombres paramilitares “obligó a los pobladores a realizar **una reunión en el lugar conocido como La Laguna**”en el territorio colectivo de Jiguamiandó. (Le puede interesar:["Neoparamilitares amenazan la zona humanitaria de Nueva Vida en Cacarica, Chocó"](https://archivo.contagioradio.com/neoparamilitares-amanezan-la-zona-humanitaria-de-nueva-vida-en-cacarica-choco/))

Allí, los hombres armados le indicaron a los integrantes de los concejos locales de Arrastradero y el Vergel “**que ellos iban a resititur las tierras**”. Sin embargo, afirmaron que quienes hubieran vendido tierras nos las iban a devolver y que las comunidades “debían dejar trabajar a los empresarios Darío Montoya, William Ramírez y Antonio Lopera” argumentando que “es gente de bien que beneficia la a la zona”.

Además, el miércoles 28 de marzo, los integrantes de las AGC “bajaron en dos motocicletas a dos civiles en el punto que conduce a la finca de La Rueda”. Allí, les **amarraron las manos a las personas**, los golpearon y los amenazaron para después conducirlos por el camino de la propiedad conocida como La Esmeralda. La organización denunció que se desconoce lo que sucedió con las dos personas.

### **Paramilitares declararon objetivo militar a quienes los denuncien** 

A los hechos que ocurrieron se suma que los grupos armados paramilitares que operan en la región amenazaron con **asesinar a las personas que los denuncien** “en Bogotá”. Las comunidades recordaron que se encuentran en un estado de vulnerabilidad a pesar de haber activado el sistema de alertas. Argumentaron que tienen el derecho constitucional a la tierra pero las amenazas por la lucha por el territorio continúan. (Le puede interesar:["Nuevas amenazas de paramilitares contra FARC y líderes sociales"](https://archivo.contagioradio.com/nuevas-amenazas-de-paramilitares-contra-farc-y-lideres-sociales/))

Finalmente, la organización recordó que las estructuras armadas ilegales de las AGC “continúan operando en lugares **donde hay presencia policial y militar**”. Esto podría indicar la tolerancia o la ineficiencia de la Fuerza Pública para detener a estos grupos armados pues “se están constituyendo como la autoridad real para realizar una devolución de tierras” y  han asegurado la tierra a los beneficiarios del despojo paramilitar. Todas estas acciones se están llevando a cabo ante la ausencia de diligencia del Estado colombiano y con el “consentimiento de la brigada 17” del Ejército Nacional.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
