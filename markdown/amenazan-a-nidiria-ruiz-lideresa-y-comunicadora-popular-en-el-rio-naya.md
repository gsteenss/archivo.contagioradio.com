Title: Amenazan a Nidiria Ruíz, lideresa y comunicadora popular en el Río Naya
Date: 2019-05-24 10:27
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Amenaza a líderes sociales, buenaventura, Mujeres AINI, Río Naya
Slug: amenazan-a-nidiria-ruiz-lideresa-y-comunicadora-popular-en-el-rio-naya
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Nidiria-Ruíz.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

En horas de la mañana de hoy, viernes 24 de mayo, en el **Corregimiento de Puerto Merizalde, Territorio colectivo del Río Naya**, Buenaventura, en la entrada de la vivienda de la **lideresa Nidiria Ruíz Medina** fue hallado un panfleto amenazante con el mensaje "Bala para Nidiria Ruíz" realizado recortes de letras de periódico o revista.

> [\#SinPueblosEtnicosNoHayPaz](https://twitter.com/hashtag/SinPueblosEtnicosNoHayPaz?src=hash&ref_src=twsrc%5Etfw)  
> Amenaza a líderesa Nidiria Ruiz de [@CONPAZ\_](https://twitter.com/CONPAZ_?ref_src=twsrc%5Etfw) sigue demostrando que es vital rescatar La Paz en Peligro en Colombia ?? [@Contagioradio1](https://twitter.com/Contagioradio1?ref_src=twsrc%5Etfw) [@Justiciaypazcol](https://twitter.com/Justiciaypazcol?ref_src=twsrc%5Etfw) Sr presidente [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) cúmplale a la vida [@EmilioJArchila](https://twitter.com/EmilioJArchila?ref_src=twsrc%5Etfw) [@DefendamosPaz](https://twitter.com/DefendamosPaz?ref_src=twsrc%5Etfw) <https://t.co/YEsvsFd7Py>
>
> — CENPAZ COLOMBIA (@CENPAZ\_) [24 de mayo de 2019](https://twitter.com/CENPAZ_/status/1131935038134738956?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Nidiria es integrante de la **Asociación de Mujeres AINI en el Río Naya** y es vocera nacional y comunicadora popular de la red de Comunidades construyendo paz en los territorios CONPAZ, además lidera el proceso de pedagogía de los acuerdos de paz al interior de su comunidad así como la construcción del Plan de Desarrollo con enfoque Territorial PDET para el Pacífico Medio.

La lideresa quien también apoya el proceso de sustitución de los cultivos de uso ilícito y la reconstrucción de la memoria histórica para el esclarecimiento de la verdad sobre los desaparecidos del Naya, e**s beneficiaria de medidas cautelares de la Comisión Interamericana de Derechos Humanos desde enero del año 2002.**

Las mujeres AINI fueron galardonadas por la Gobernación del Valle en reconocimiento a su labor como mujeres emprendedoras y constructoras de paz en marzo de 2019. [(Lea también: "Todas reunidas no vamos a olvidar, vamos a reconciliarnos”: Mujeres del Río Naya](https://archivo.contagioradio.com/todas-reunidas-no-vamos-a-olvidar-vamos-a-reconciliarnos-mujeres-del-rio-naya/)

 
