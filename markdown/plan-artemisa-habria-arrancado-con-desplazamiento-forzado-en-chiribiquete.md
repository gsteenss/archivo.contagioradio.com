Title: Plan "Artemisa" habría arrancado con desplazamiento forzado en Chiribiquete
Date: 2019-04-30 18:54
Author: CtgAdm
Category: Ambiente, Comunidad
Tags: Parque Nacional Natural Chiribiquete
Slug: plan-artemisa-habria-arrancado-con-desplazamiento-forzado-en-chiribiquete
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/PNN-Chiribiquete-2.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/PNN-Chiribiquete.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/PNN-Chiribiquete-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Suministrada] 

Comunidades de San José del Guaviare (Guaviare) denunciaron que el Ejército Nacional, en desarrollo de la política de gobierno "Artemisa", desplazó de manera forzada a **11 personas, entre ellos niños y personas de la tercera edad,** por el supuesto delito de ocupar ilegalmente una zona de colonización campesina en el Parque Nacional Natural Chiribiquete.

Estas familias, que vivían desde hace 30 años en sus predios, fueron  detenidas el pasado 25 de abril por hombres que se identificaron como miembros de Antinarcóticos de la Policía por presuntos delitos ambientales y trasladadas a las instalaciones de unidades Seccionales de Investigación Criminal en San José del Guaviare, donde fueron interrogadas y presionadas a aceptar los cargos.

Según la denuncia, a las familias les dieron un plazo de tres días para desocupar sus fincas y al regresar a sus viviendas en la vereda Alta Angoleta descubrieron que el Ejército Nacional, con la autorización de la fiscal 17 especializada de Medellín para derechos humanos y delitos ambientales, había tumbado sus casas con motosierras, quemado sus pertenencias y herramientas de trabajo e incluso, dinamitado un puente construido por la comunidad.

Este operativo haría parte de la primera acción de la nueva iniciativa del Presidente Iván Duque para, supuestamente, frenar la alta tasa de deforestación en los parque naturales del país. La ofensiva "Artemisa" fue anunciada por el primer mandatario el pasado 27 de abril en La Macarena, Meta, donde declaró que en los últimos días había logrado **capturar 10 personas que ocupaban 120 hectáreas del PNN Chiribiquete.**

Según Sammy Sánchez, profesional técnica de organizaciones campesinas que habitan los Parques Nacionales, la vereda Alta Angoleta fue integrada a las zona protegida del Parque Chiribiquete en octubre de 2018 tras la decisión del expresidente Juan Manuel Santos de ampliar el parque natural por 1’486.676 hectáreas. A estas familias, se les había prometido su reubicación por la Agencia de Tierras, sin embargo hasta la fecha este compromiso no se ha cumplido.

[Sánchez señala que "**militarizar la gestión y el manejo de las áreas protegidas no va ser la solución a la alta tasa de deforestación**, que está trayendo como consecuencia el desplazamiento de familias campesinas y el acaparamiento de  tierras por sectores políticos". Además, se debe prioritarizar el diálogo con los campesinos de la zona para la creación de política pública para generar economías sostenibles que a su vez ayuden a conservar el bosque de la Amazonía.]

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/PNN-Chiribiquete-1-300x225.jpeg){.aligncenter .size-medium .wp-image-66130 width="300" height="225"}

###### 

###### [<iframe id="audio_35277393" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_35277393_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
