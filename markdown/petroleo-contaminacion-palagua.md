Title: Petróleo por vida, los daños ambientales en la Ciénaga de Palagua
Date: 2018-08-27 16:38
Category: Voces de la Tierra
Tags: palagua, petroleo, quinchas
Slug: petroleo-contaminacion-palagua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/web_paro_big_tp.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Vanguardia 

###### 27 Ago 2018 

La **Serranía de las Quinchas** es uno de los ecosistemas más importantes del Magdalena Medio. Una zona declarada hotspot, donde la biodiversidad que existe allí no existe en otro lugar de Colombia o del mundo, y es el lugar donde nacen diferentes cuerpos de agua que alimentan la **Ciénaga de Palagua** y el rió Magdalena.

Hasta hace 20 años, la Ciénaga de Palagua, mantenía su economía local basada en la pesca, pero la contaminación del espejo de agua causada por la explotación petrolera viene acabando con la vida y el sustento de las comunidades, sin que la autoridad ambiental y las instituciones del Estado tomen las medidas necesarias, y por el contrario permitan ampliar los perímetros de perforación.

En este Voces de la tierra, **Stefany Johana Grajales**, Coordinadora del Movimiento de mujeres defensoras del territorio y la naturaleza de Boyacá y parte de Colombia libre de fracking, expone la difícil situación ambiental y social que se vive en ambos ecosistemas de Puerto Boyacá.

Además, **Juan Pablo Soler**, ambientalista integrante del Movimiento Ríos vivos, habla de lo que sigue ocurriendo con las comunidades afectadas por el proyecto hidroeléctrico Hidroituango en Antioquia.

<iframe id="audio_28110865" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28110865_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
