Title: El 6 de junio los docentes de Colombia se toman Bogotá
Date: 2017-06-03 10:10
Category: Educación, Nacional
Tags: fecode, Paro de docentes
Slug: el-6-de-junio-los-docentes-de-colombia-se-toman-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/movilización-fecode.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [02 Jun 2017] 

FECODE, a través de su vocero Over Dorado, expresó que el próximo 6 de junio docentes de todo el país llegaran a la Capital, a la Plaza de Bolívar, para exigirle al Ministerio de Educación que **destine fondos para superar la desfinanciación de la Educación pública del país y de prontas propuestas que permiten retomar las clases en los colegios**.

Para Over Dorado, si bien es cierto que han logrado llegar a puntos en común con el Ministerio de Educación, aún no hay ninguna propuesta que subsane los 600 mil millones de pesos que hay de déficit en el Sistema de Educación. Le puede interesar:["Gobierno está repartiendo la pobreza pero no aumenta el presupuesto: FECODE"](https://archivo.contagioradio.com/gobierno-esta-repartiendo-la-pobreza-pero-no-aumenta-presupuesto-fecode/)

“Hay posibilidad de entrar a hablar más en detalle y de acercarse a las propuestas de FECODE en términos de bonificación, nivelación salarial y del sistema general de participación, **para nosotros es importante esto, pero la voluntad también debe verse en la exigencia a la financiación de la educación**” afirmó Dorado.

La Asociación de Padres de Familia manifestó que tiene la intensión de instaurar una demanda contra el Ministerio de Educación y FECODE, por afectaciones a los estudiantes que cumplen casi un mes sin asistir a las aulas de clase. Referente a esta situación Dorado afirmó, **“lo que está haciendo FECODE es buscar las condiciones necesarias para que los estudiantes y docentes tengan garantías dignas de educación**”.

La toma a Bogotá de las y los docentes de Colombia, se está organizando desde diferentes regiones del país, que llegarán a la capital para marcha por 3 vías importantes, el Monumento a los Héroes, La Sevillana y la Av. Boyacá, que se juntarán en la carrera séptima y terminarán en la Plaza de Bolívar. Le puede interesar:["Secretaría de Educación ya habían advertido grave situación de Educación en Colombia"](https://archivo.contagioradio.com/secretarias_educacion_paro_educadores/)

###### Reciba toda la información de Contagio Radio en [[su correo]
