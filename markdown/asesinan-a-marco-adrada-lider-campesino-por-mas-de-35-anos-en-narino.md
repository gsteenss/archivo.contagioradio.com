Title: Asesinan a Marco Adrada, líder campesino por más de 35 años en Nariño
Date: 2019-04-28 20:58
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: junta de acción comunal, Líderes campesinos asesinados, nariño
Slug: asesinan-a-marco-adrada-lider-campesino-por-mas-de-35-anos-en-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Líder-Marcos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Fensuagro] 

Este domingo 28 de abril a las 3:00 de la tarde, fue asesinado con arma de fuego **el líder campesino Marco Antonio Adrada Viana** mientras se desplazaba por la vía  que conduce del municipio de Leiva, Nariño hasta el corregimiento Las delicias a unos 200 metros del casco municipal,  Marco, quien ocupaba el cargo de fiscal al interior de la Junta de Acción Comunal de El Sauce sería víctima de un grupo armado de la región.

Los hechos sucedieron mientras Marco Antonio se desplazaba junto a su familia hacia su lugar de residencia después  de hacer mercado en la vereda El Sauce, de donde era oriundo y considerado un reconocido  líder por la comunidad debido a que **desde hacia 35 años hizo parte de de la Junta de Acción Comunal ocupando diferentes cargos.** Su esposa y su hijastro, menor edad salieron ilesos del ataque.

La denuncia que llegó hoy Domingo, pasadas las seis de la tarde, no agrega más detalles del hecho, sin embargo, tanto la región como los integrantes de FENSUAGRO y sus filiales han sido víctimas de decenas de asesinatos a lo largo del territorio nacional. [(Lea también: Continúa el asesinato de líderes sociales en Nariño)](https://archivo.contagioradio.com/nuevamente-un-lider-social-fue-asesinado-en-narino/)

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
