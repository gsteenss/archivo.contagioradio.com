Title: La protesta como derecho en tiempos de pandemia
Date: 2020-08-19 14:38
Author: CtgAdm
Category: Columnistas, Opinion
Tags: cuarentena, cuarentena en colombia, Movilización, Movilización social, OMS, pandemia, prevención, Protesta social, Revolucion
Slug: la-protesta-como-derecho-en-tiempos-de-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Marcha-5.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Andrés Zea - Contagio Radio

<!-- wp:paragraph -->

Las recomendaciones de la OMS y de la comunidad científica sobre la Covid-19, apuntan a la adopción de medidas de protección para disminuir los riesgos asociados al contagio que han hecho colapsar los sistemas de salud e incrementar de manera grave la afectación de vidas humanas. Dentro de estas medidas, las cuarentenas, el confinamiento y la distancia social restringen la libertad de reunión y de movimiento y, en consecuencia, el derecho a la protesta, pero no los suprimen. Los Estados democráticos tienen el deber de proteger y garantizar los derechos, aún en circunstancias excepcionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las medidas de prevención y contención que los gobiernos han adoptado para mitigar la propagación de la pandemia deberían ser compatibles con la protección de los derechos humanos y el Estado de derecho. Las restricciones y limitaciones sólo se justifican si tienen una base legal, son estrictamente necesarias según evidencias científicas, proporcionales con el objetivo que se busca (relativo a la protección de la salud de las personas en este caso) y estén limitadas en el tiempo. Tampoco pueden acarrear discriminación en su aplicación o atentar contra la dignidad humana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

¿Por qué, si el Covid-19 cabalga todavía sin freno en nuestras ciudades, hemos visto protestas en Colombia durante todo este periodo de emergencia, unas en el espacio público, otras desde terrazas, ventanas y balcones, y otras más en redes sociales? Hay al menos dos realidades que lo explican.
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Primero**, como se ha visto en el país y el mundo, esta pandemia puso de nuevo en primer plano las grandes desigualdades sociales, la exclusión de la población más pobre y vulnerable de los beneficios del desarrollo y de las mieles que preveían los doctos del mercado que, de alguna manera, llegarían a todas y todos por igual. Presenciamos la crisis de un modelo capitalista de mercado que despojó al Estado del manejo de asuntos tan cruciales como la salud pública y de asegurar el bienestar para toda la población y, en especial, para los sectores y grupos más vulnerables y maginados de la renta y el capital. Sin ir más lejos, la agenda social que movilizó a la ciudadanía desde finales de 2019 en Colombia está hoy más vigente. Por eso se protesta.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**En segundo lugar**, hay que dar un vistazo al derecho a la salud. De acuerdo con el Comité de Derechos Económicos, Sociales y Culturales de las Naciones Unidas (CDESC) este derecho está estrechamente vinculado con el ejercicio de otros derechos y depende de ellos, como un bloque integral. Son ellos los derechos a la alimentación, la vivienda, al trabajo, la educación, la dignidad humana, la vida, la no discriminación, la igualdad, a no ser sometido a torturas, a la vida privada, al acceso a la información y a la libertad de asociación, reunión y circulación.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pues bien, la mayoría, si no todas las protestas que han ocurrido en el país desde que se decretó la emergencia sanitaria y en medio de los decretos de confinamiento obligatorio, tienen que ver precisamente con estos derechos: además del acceso a los servicios de salud pública y de la seguridad para las personas que trabajan en este sector, se protesta por alimentos, trabajo, ingresos y vivienda. Pero también por la vulneración de derechos que se han incrementado durante la pandemia, como el hacinamiento en las cárceles, la violencia intrafamiliar, los feminicidios disparados o el creciente asesinato de líderes y lideresas sociales, y jóvenes. Los reclamos y demandas ciudadanas se han hecho más visibles y evidentes cuando se declara un estado de emergencia que requiere medidas de aislamiento social para su contención pero que ignora las desigualdades sociales y que sin duda afecta la economía e incrementa la población desempleada.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De allí que se requiera un Estado que garantice mecanismos y canales adecuados para que la población pueda contribuir a superar los problemas que se enfrentan en medio de la pandemia. Y eso pasa, en un Estado democrático, por asegurar las garantías necesarias para afrontar las restricciones que se derivan de la prevención del contagio, en un marco de dignidad humana, que amparen la salud y la vida en un contexto especialmente excepcional. La negación de estos requisitos básicos motiva las protestas de muchos sectores de la población afectados duramente por la crisis, porque ni sus opiniones ni su situación fueron consideradas por las decisiones gubernamentales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Si bien la pandemia requiere de una respuesta rápida y eficaz del Estado, la participación de la sociedad civil en el diseño y la evaluación periódica de las estrategias que se adopten puede hacer que las medidas sean mejores, tanto en su diseño como en su aplicación. De esta forma se podría garantizar la inclusión de las necesidades de la población marginada y se atenderían las medidas de confinamiento con más información, compresión y confianza ciudadana.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La respuesta de un Estado democrático debe orientarse hacia disposiciones que, por ejemplo, aseguren a los hogares de bajos ingresos, que viven de la informalidad y a los sectores más vulnerables, con un ingreso básico digno que les permita acogerse a las medidas de cuarentena o confinamientos, además de atención médica y apoyo al trabajo de cuidado. O puede, como medida autoritaria –ignorando la ruta democrática– militarizar las zonas más empobrecidas de las ciudades, cercar barrios, imponer multas o detenciones por infringir la cuarentena, que en nada contribuyen como medida de salud pública y tienden a ser contraproducentes, discriminatorias y arbitrarias. Las medidas coercitivas, aupadas por el miedo y por la fuerza, rara vez son proporcionadas. En cambio, las medidas voluntarias de autoaislamiento tienen más probabilidades de inducir la cooperación y proteger la confianza pública, cuando son construidas con la ciudadanía y toman en cuenta las condiciones de vida de los sectores más desprotegidos y vulnerables en un estado de excepción.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En todo caso, siempre debe quedar intacto el derecho a disentir, a oponerse a las decisiones gubernamentales y a expresar el desacuerdo en espacios públicos acudiendo, como en un contexto de pandemia, a medidas de autocuidado y protección para evitar la transmisión del virus. La pandemia no puede esconder las demandas ciudadanas, anular la voz de la disidencia o de la indignación ante la iniquidad que se deriva de medidas que ignoran las desigualdades agravadas en un contexto de crisis sanitaria, pues las decisiones terminan siendo discriminatorias con los sectores marginados y, en no pocos casos, respaldadas por el uso desmedido de la fuerza policial, cargadas de arbitrariedad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El derecho a protestar no desaparece en tiempos de pandemia. Sigue vigente y la sociedad civil transforma sus métodos para hacer visibles sus demandas e inconformidad por sobre el aislamiento físico. Al Estado democrático le corresponde proteger también este derecho en forma consistente con las necesidades de salud pública, y evaluar constantemente las restricciones a las protestas en las calles para determinar si siguen siendo necesarias y proporcionadas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La Fundación Foro Nacional Por Colombia**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":6} -->

###### ·  Es un Organismo Civil no Gubernamental sin ánimo de lucro, creado en 1982, cuyos objetivos son contribuir al fortalecimiento de la democracia en Colombia. Desarrolla actividades de investigación, intervención social, divulgación y deliberación pública, asesoría e incidencia en campos como el fortalecimiento de organizaciones, redes y movimientos sociales, la participación ciudadana y política, la descentralización y la gestión pública, los derechos humanos, el conflicto, la paz y las relaciones de género en la perspectiva de una democracia incluyente y efectiva. Foro es una entidad descentralizada con sede en Bogotá y con tres capítulos regionales en Bogotá (Foro Región Central), Barranquilla (Foro Costa Atlántica) y Cali (Foro Suroccidente).

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### **Contáctenos: 316 697 8026\_ 031282 2550 /Comunicaciones@foro.org.co**

<!-- /wp:heading -->

<!-- wp:separator -->

------------------------------------------------------------------------

<!-- /wp:separator -->

<!-- wp:heading {"level":6} -->

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.

<!-- /wp:heading -->

<!-- wp:paragraph -->

[Le puede interesar: Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
