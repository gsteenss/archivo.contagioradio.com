Title: Gobernador del Tolima desconoce rechazo ciudadano a la "dictadura minera"
Date: 2017-12-06 17:07
Category: Ambiente, Nacional
Tags: Ibagué, San Agustín, Tolima
Slug: gobernacion_tolima_renzo_garcia_mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/consulta-ibague.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las 2 Orillas] 

###### [5 Dic 2017] 

[El gobierno nacional logró tumbar el acuerdo que impide la actividad minera en Ibagué por medio de un recurso interpuesto por el gobernador del departamento. La medida había sido tomada por el concejo de la ciudad dadas las trabas que se impusieron a la consulta popular que había sido convocada por la ciudadanía.]

El Tribunal Administrativo del Tolima decidió aceptar las objeciones formuladas por la gobernación del departamento**. Óscar Barreto Quiroga, gobernador del Tolima, señaló en una demanda que el concejo municipal no tenía la potestad** de prohibir actividades de prospección, exploración, construcción, montaje, explotación y transformación minera.

[Renzo García,  vocero del Comité Ambiental del Tolima, señala que la comunidad se ha enterado de dicha decisión mediante un comunicado que emite la oficina de defensa jurídica del Estado, y aunque aún no se conocen detalladamente los argumentos del tribunal para negar el acuerdo municipal, asegura que la comunidad seguirá en pide de lucha para blindar su territorio de la minería contaminante.]

García asegura que, contrario a lo que ha dicho el tribunal, es la propia **Corte Constitucional a través de la Sentencia T-445 de 2016,** la que manifiesta que para proteger el patrimonio ambiental, tanto concejos municipales como los alcaldes tienen la autoridad para impedir el desarrollo de proyectos mineros.

### **Colombia no está en una "dictadura minera"** 

Además, señala que al día de hoy hay "una especie de choque de trenes" ya que, mientras se conoce este fallo en Tolima**, el Tribunal Administrativo del Huila dejó en firme un acuerdo municipal en San Agustín,** reconociendo el principio de autonomía territorial, "de manera que no entenderíamos los argumentos jurisprudenciales de la sala de Tolima".

Frente a ello, desde el Comté Ambiental, se señala que el gobernador está desconociendo el rechazo masivo a la imposición de una "dictadura minera", por parte de los habitantes de Ibagué, y en general, del departamento.

Lo que se está buscando desde el movimiento ambientalista en Colombia es seguir fortaleciendo la capacidad de organización y movilización de la ciudadanía y con ello mantener y defender los mecanismos de defensa territorial. Asimismo, siguen proponiendo que el gobierno vea **la superficie del suelo colombiano como el lugar donde se encuentra la verdadera riqueza,** a través de la cual se pueden soportar otros ejercicios productivos, como el turismo y la agricultura.

<iframe id="audio_22482681" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22482681_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
