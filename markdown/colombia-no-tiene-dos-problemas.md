Title: Colombia no tiene dos problemas
Date: 2015-02-18 15:58
Author: CtgAdm
Category: Opinion, superandianda
Tags: alvaro uribe velez
Slug: colombia-no-tiene-dos-problemas
Status: published

**[[Por [Superandianda ](https://archivo.contagioradio.com/superandianda/)]]**

Lo que las FARC fueron para el gobierno de Álvaro Uribe Vélez es hoy Álvaro Uribe Vélez para Colombia.

Mientras todo el discurso del gobierno de la seguridad democrática se centro en "acabar con las FARC" tomándola como único referente del inicio y fin de todas las falencias y miseria social del sistema político, que mucho antes ya gobernaba, de esa misma manera parece que Colombia se centra en el tema de Álvaro Uribe Vélez. El personaje mencionado, curiosamente, ha logrado, no solo en el pasado, poner el tema “problema” (FARC)[ ]{.Apple-converted-space}sino él mismo ponerse como el problema actual. A la vez que el antiuribismo exige la justicia frente a crímenes que aún siguen impunes, el uribismo[ ]{.Apple-converted-space}ha formado una religión que no critica y a la que siguen y defienden ciegamente. La[ ]{.Apple-converted-space}cuestión es que todos seguimos hablando de lo mismo[ ]{.Apple-converted-space}desde diferentes posiciones: Alvaro Uribe Vélez y las FARC.

En el debate constante de buscar culpables de nuestro pésimo sistema corrupto y sin justicia social, hemos olvidado que el pueblo es el principal culpable, que no existe una preparación consciente para elegir gobiernos, para promover líderes, para evaluarlos, exigirles y en la que la memoria no alcanza para repudiar crímenes de estado.[ ]{.Apple-converted-space}Mientras unos toman a las FARC como único culpable del terror y de la violencia, otros[ ]{.Apple-converted-space}toman[ ]{.Apple-converted-space}a Álvaro Uribe Vélez como único culpable del terror e impunidad del paramilitarismo.

Indirectamente el mismo discurso, desde cualquier posición, [ ]{.Apple-converted-space}ha servido para que las mismas maquinarias[ ]{.Apple-converted-space}políticas se cuelen, sigan ocupando los mismos cargos y se mantengan en sus puestos sin realmente dar soluciones y propuestas efectivas a problemas emergentes que vienen desde gobiernos pasados hasta el actual. Parece ser que nuestros representantes olvidan que participar en política no es solo quedarse en la crítica, repetir lo que ya todos sabemos, denunciar sin comprometerse. El ex presidente regala a sus simpatizantes y opositores los argumentos perfectos que necesitan para seguir ineptamente en el poder. Defendiéndolo o atacándolo siguen durmiendo a un pueblo que día a día sufre mayor desigualdad y comercialización de sus derechos.

Los problemas de Colombia van mucho más allá de dos personajes: siguen vendiendo nuestros recursos al mejor postor extranjero, el sistema carcelario y de salud está colapsando, la educación pública amenazada por la mercantilización, carecemos de medios de información que permitan crear en la ciudadanía una verdadera mirada crítica a su sistema político.

No podemos negar que el gobierno de Álvaro Uribe Vélez se caracterizo por sus beneficios a sectores privados y al paramilitarismo pero tampoco podemos dar patrocinio al gobierno de Juan Manuel Santos, el “traidor” que continua con el mismo sistema que no hace nada para corregirlo ni castigarlo. Firmando la paz y capturando a Alvaro Uribe Vélez no se acaba con la ignorancia, desigualdad y[ ]{.Apple-converted-space}miseria. Si no estamos preparados, si no tenemos organización política, si no vemos más allá de los dos únicos protagonistas que nos muestran, de nada sirve una firma o una cárcel.

No critiquemos por criticar ni marchemos por marchar. La vida, el respeto, la igualdad son temas universales que por ende se deben defender, no podemos seguir alzando la bota del pantalón, sacando el lápiz y salir a gritar sin tener una[ ]{.Apple-converted-space}visión del cambio social al que queremos llegar. ¿Qué viene después de las marchas? ¿Qué nos proponen después de la crítica? ¿Qué viene después de la paz? Son los post frente a los cuales nadie sabe una respuesta clara y real.

Logrando la salida política al conflicto armado se puede dar paso a un nuevo periodo político para el país[ ]{.Apple-converted-space}y esclareciendo los crímenes de la seguridad democrática se avanza hacia un camino para que muchas de las víctimas no queden impunes en el silencio del estado, pero no son los dos únicos factores que harían de Colombia un país "feliz y justo" si no trabajamos por una nueva estructura social.
