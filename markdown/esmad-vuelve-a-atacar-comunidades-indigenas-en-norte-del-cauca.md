Title: ESMAD vuelve a atacar comunidades indígenas en Norte del Cauca
Date: 2015-03-04 20:04
Author: CtgAdm
Category: Movilización, Nacional
Tags: ESMAD, Indigenas del Norte del Cauca, liberación de la madre tierra, víctimas ESMAD
Slug: esmad-vuelve-a-atacar-comunidades-indigenas-en-norte-del-cauca
Status: published

###### Foto: ONIC 

Según la denuncia del senador Alberto Castilla, este 4 de marzo, hacia las 11:00 am, el ESMAD ingresó de manera abrupta a los predios cercanos a la finca Miraflores en Corinto, Cauca, quemando las carpas y arremetiendo con gases lacrimógenos. Según el Consejero Mayor de la Asociación de Cabildos Indígenas (ACIN) Héctor Fabio Dicué, existe una alta preocupación por la presencia de un número mayor de agentes del ESMAD. Sumado a lo anterior, el día de ayer, en el resguardo de La Agustina, también fue atacada una chiva que intentaba trasladar a las y los indígenas, hiriendo en el ojo a una comunera.

Tras la visita de la Comisión de Verificación este 2 de marzo, compuesta por parlamentarios del Polo Democrático, la ONIC, el Congreso de los Pueblos y la Marcha Patriótica,  se instaló una mesa de interlocución entre una  comunidad Indígena del Norte del Cauca y representantes del Gobierno Nacional, con apoyo de la gobernación de ese departamento.
