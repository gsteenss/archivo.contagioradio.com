Title: Se cumple una semana de paro nacional y así se moverá la ciudadanía este 28N
Date: 2019-11-28 10:55
Author: CtgAdm
Category: Movilización, Paro Nacional
Tags: 28N, cacerolazo, Duque, Gobierno, manifestación, Marchas, Movilización, paquetazo, Paro Nacional
Slug: paro-nacional-28n
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Movilización_Paro_Nacional_28N.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Paro-Nacional-Cacerolazo.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: En Instagram @thewhisky.ph] 

[Al cumplirse ocho días de Paro Nacional y tras la jornada de diversas manifestaciones que se vivieron el pasado miércoles en diversas ciudades, se esperan movilizaciones en distintos puntos de la capital. Organizaciones y colectivos han establecido puntos de encuentro y recorridos específicos, uniendo la protesta con la expresión artística para este 28 de noviembre.]

[Ese es el caso de **“Tambores a las calles**”, donde distintas agrupaciones musicales como **La Beat Machine, La Sonora Mazurén y Morfonía** invitan a tomarse la plazoleta de la calle 85, al frente del Carulla, de la mano de la percusión. La cita es desde las 5:00 pm en el norte de la ciudad. Se espera que los asistentes lleven instrumentos propios como tambores y redoblantes, al igual que cacerolas.][(Le puede interesar: Paro Nacional: una expresión ciudadana de contundente movilización).](https://archivo.contagioradio.com/paro-nacional-una-expresion-ciudadana-de-contundente-movilizacion/)

[Por otro lado, en el monumento de Los Héroes se espera una movilización que se dirigirá hasta el Parque Cedritos, en la carrera 9 con calle 146. Bajo la consigna **\#ParoDesdeTemprano** los manifestantes buscan tomarse las calles todo el día. ]

### En el paro continúa la movilización pacífica

[Desde el sur también hay un llamado a marchar. Los manifestantes iniciarán su recorrido desde el Portal del Sur hasta la Plaza de BolÍvar, uniéndose con varios marchantes que saldrán de puntos cercanos desde las 6:00 am.]

[En la Avenida El Dorado con carrera 66, al frente de la Secretaría de Educación de Bogotá, se convocó a un plantón y una ofrenda floral por “la vida, la paz y los estudiantes caídos”, como lo informa la Asociación Distrital de Trabajadores y Trabajadoras de la Educación (ADE). Esta actividad se desarrollará entre las 10:00 am y las 2:00 pm.]

[Por último, un **“cacerocletazo”** se ha convocado por redes para que los manifestantes que se movilizan en bicicleta por la ciudad acudan al roundpoint de la calle 100 con carrera 15 a las 5:00 pm. Desde allí los manifestantes irán “cacerola y bici en mano” hasta el **Parque de los Hippies**, en la localidad de Chapinero, donde cerrarán con un acto de circo cultural. ]

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
