Title: ESMAD dispara contra estudiantes, jóvenes reporteros y periodistas
Date: 2015-06-10 21:03
Category: Educación, Nacional
Tags: brutalidad policial, Cristian Andrés Pulido, ESMAD Universidad del Tolima, Jessica Garcia
Slug: estudiantes-de-la-universidad-del-tolima-denuncian-graves-agresiones-por-parte-del-esmad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/IMG-20150610-WA0003.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [Fotografía: Natalia, Astracatol] 

<iframe src="http://www.ivoox.com/player_ek_4624281_2_1.html?data=lZuflpecdY6ZmKiakpyJd6KklJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjsbn1M7Qw5ClsMbewtPR1MaPi8LmxIqwlYqliMKZk6iYy9PYqcjmwtPhx5DIqYzgwpCu1dTHrcLXyoqwlYqmd8%2Bhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Jessica García, integrante de la Asociación colombiana de Estudiantes Universitarios] 

###### [10 jun 2015] 

De acuerdo con la denuncia pública presentada por la **Asociación Colombiana de Estudiantes Universitarios y la Corporación para la Defensa y Promoción de los Derechos Humanos REINICIAR** regional Tolima, estudiantes de la Universidad de ese departamento **resultaron gravemente heridos como consecuencia del uso desproporcionado de la fuerza por parte de integrantes del Escuadrón Móvil Antidisturbios ESMAD.**

Durante los hechos ocurridos el pasado lunes 9 de junio, en el marco de la conmemoración del día Nacional del Estudiante, **Cristian Andrés Pulido**, quien cursa la Licenciatura en Ciencias Sociales, **recibió un impacto con gas lacrimógeno en su rostro**, causándole un grave trauma que lo mantiene en cuidados intensivos en la Clínica Federico Lleras Acosta de Ibagué, con un alto riesgo para su vida.

**Jessica Alejandra García**, compañera de Facultad de Cristian Pulido, asegura que la agresión por parte de los uniformados **“atenta contra los Derechos Humanos y con el código institucional por tratarse de un disparo frontal a menos de 20 metros de distancia”**.

El estudiante, quien **fue intervenido quirúrgicamente en la noche del lunes **por la gravedad de su situación, podría resultar **“sufriendo algún tipo de parálisis”** por un coágulo de sangre y continúan a la espera ante la posibilidad **“**que **pierda uno de sus ojos”,** según manifiesta Jessica García, quien se ha mantenido atenta al estado de salud de su compañero.

Además de la agresión contra Pulido, t**res estudiantes más resultaron lesionados tras el encuentro con el ESMAD**: el menor de edad, **Diego Rincón** de la Facultad de Ciencias Exactas**,** herido en su pierna derecha, **David Camilo Sánchez** de la Facultad de Derecho con heridas en la parte superior de su cabeza y **Miguel Góngora** de la misma Facultad, impactado en diferentes partes de su rostro por recazadas.

Según denuncian los periodistas y reporteros que se encontraban en el lugar, el ESMAD disparó gases y balas de pimienta en contra de las cámaras que buscaban registrar los hechos, hiriendo en la cara y manos a los jóvenes, y dañando sus implementos de trabajo. Es el caso de Natalia Romero, a quien el ESMAD le disparó directamente a la cara mientras ella tomaba fotografías, dañándole su implemento de trabajo. Cabe recordar que bajo el mismo modus operandi fue herido gravemente el reportero Fred Nuñez, quien cubría las protestas del Catatumbo del año 2013, y el 19 de agosto del mismo año fueron agredidos comunicadores de la Red de Medios Alternativos y Populares en Cali, quienes cubrían el paro agrario en esa zona del país.

\[caption id="attachment\_9959" align="alignright" width="316"\][![La última foto que tomó la cámara de Natalia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Camara.jpg){.wp-image-9959 width="316" height="387"}](https://archivo.contagioradio.com/estudiantes-de-la-universidad-del-tolima-denuncian-graves-agresiones-por-parte-del-esmad/camara/) La última foto que tomó la cámara de Natalia\[/caption\]

Jessica García asegura que **“hasta ahora las autoridades no han hecho ningún tipo de pronunciamiento al respecto, lo único que hicieron fue venir a tratar de señalar a los compañeros que habían sido heridos como si ellos hubiesen estado encapuchados”**, enfatizando además que su compañero **“se encontraba realizando labor de derechos humanos razón por la cual tomaba fotografías para luego hacer el denuncio por la arremetida del ESMAD**”.

Tras los hechos represivos extendidos hasta el día de hoy, **los estudiantes continuarán tomando medidas en cuanto a la denuncia pública y otras dirigidas a la Instituciones como la Policía, la Personería y la Defensoría del Pueblo**, exigiendo el respeto por sus derechos fundamentales y en particular el derecho a la protesta y la libre expresión, esperando contar con el apoyo y acompañamiento por parte de las Directivas de la Universidad.

En el marco de la conmemoración del día del Estudiante, también se presentaron manifestaciones en la Universidad de Antioquia, en Medellín.

<p>
<script src="//e.issuu.com/embed.js" async="true" type="text/javascript"></script>
</p>

