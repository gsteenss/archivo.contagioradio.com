Title: Egipto condena a muerte al líder de los Hermanos Musulmanes
Date: 2015-04-13 13:18
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: Egipto, Egipto condena a muerte a líder Hermanos Musulmanes, Hermanos Musulmanes, Hosni Mubarak, Karim Abdel Radi, Mohamed Badie, Mohammed Badie
Slug: egipto-condena-a-muerte-al-lider-de-los-hermanos-musulmanes
Status: published

###### Foto:Marqaanweyne.com 

###### **Entrevista con [Ignacio Álvarez-Ossorio], experto en el mundo árabe y profesor de la Universidad de Alicante (España):**

<iframe src="http://www.ivoox.com/player_ek_4351596_2_1.html?data=lZiik5qdeo6ZmKiakp6Jd6KnmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRicjd0dncjcjTssXZz8aYw5DRucbm1cqYw9GPsIa3lIqupsnJtozYxpDZ0diPjMbmzsbb0diPkdbn1tGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El juez Egipcio Nagi Shehata, ha **condenado a muerte** **a Mohammed Badie, líder supremo de los Hermanos Musulmanes** y a otros **14 de sus miembros**, acusándolos de pertenecer a una organización terrorista con fines de desestabilizar el país.

Después de que en **Julio de 2013 fuera derrocado el presidente Morshi**, la **represión** se **desató** **contra** el grupo político conocido como “**Los Hermanos Musulmanes**”, calificándolo de terrorista, ilegalizándolo y deteniendo a toda su cúpula.

Egipto alude qué el grupo organizó una **estructura para atentar contra el estado** y provocar los fuertes disturbios ocasionados cuando la destitución del último presidente.

En total han sido **más de 100 detenidos políticos** de la agrupación islamista, 14 de ellos condenados a muerte, 37 a cadena perpetua y otros 64 se encuentran acusados de pertenecer a agrupaciones terroristas.

Para los abogados de las acusaciones particulares las **irregularidades que se están cometiendo en los juicios son flagrantes violaciones de DDHH.**

“…El objetivo de las autoridades es el de **deshacerse de la oposición política** o, como mínimo, mantenerla bajo presión…” **afirma Karim Abdel Radi abogado de parte de los acusados y miembro de la organización social de defensa de DDHH árabe** “Red árabe para la información de los DDHH”.

Las distintas fuerzas políticas en la oposición y voces críticas del país han calificado la condena de peligrosa, ya que al no dejar espacio político para el islamismo, se **corre el riesgo de que las posiciones se radicalicen**, dejando vía libre a grupos armados extremistas como el Estado Islámico.

Desde la **caída** del dictador Hosni **Mubarak**, en las protestas han **muerto ya más de 3.000 personas y más de 40.000 han sido encarceladas**. La represión ha acabado con la oposición política islamista, pero también con quienes tumbaron la dictadura.

Según **Ignacio Álvarez-Ossorio**, analista político del mundo árabe de la universidad de Alicante, la noticia de la sentencia a muerte es un **grave riesgo ya que dotará de justificación a grupos islamistas extremistas para su propia cooptación**, así como un paso más en la represión desplegada por Egipto contra la oposición.

Ossorio afirma que **no hay vínculos entre los Hermanos Musulmanes y el yihadismo**, ni si quiera afinidad política, ya que estos rechazan la violencia y actúan a través de la vía parlamentaria.

Por último el analista señala que este es un **intento del gobierno de quitarse de encima a la oposición islamista** y que es posible que en los próximos días se rebajen las penas para evita una explosión popular debido a que el partido islámico goza de mucha popularidad entre los sectores más pobres  de la población.
