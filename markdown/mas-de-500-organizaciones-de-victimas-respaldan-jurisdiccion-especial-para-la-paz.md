Title: Más de 500 organizaciones de víctimas respaldan Jurisdicción Especial para la Paz
Date: 2016-10-20 15:32
Category: Nacional, Paz
Tags: acuerdos de paz ya, Jurisdicción Especial de Paz, Red CONPAZ
Slug: mas-de-500-organizaciones-de-victimas-respaldan-jurisdiccion-especial-para-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_171.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [20 Oct 2016] 

Más de quinientas organizaciones de víctimas y Derechos humanos, representadas en distintas plataformas, emitieron un comunicado dirigido a las delegaciones negociadoras de los acuerdos, **reafirmando su respaldo a la Justicia Transicional construida en La Habana como contribución a la lucha contra la impunidad.** Resaltan que su derecho a la verdad no es negociable y que son las víctimas el sector con más potestad para decidir cómo se deben pagar las condenas.

María Mosquera, integrante de la organización CONPAZ, señaló que **“son muchos años de impunidad en que la justicia ordinaria no permite conocer la verdad de los hechos** (…) las condenas tampoco ayudan a subsanar lo que pasó, son pequeñas condenas y dan tratos diferenciales de privilegios a los culpables”.

Mosquera resalta que **“las cárceles no son la solución, necesitamos nuevos mecanismos para que haya justicia** (…) nos vemos reflejados en el punto 5, se debe respetar y fortalecer porque es un sentir desde las víctimas, una oportunidad para que el país cambie y realmente hayan garantías de no repetición”. Añade que para que ello se cumpla es fundamental que **sean investigados empresarios y demás actores involucrados en la explotación de territorios y violaciones de Derechos Humanos.**

Por otra parte, Alberto Yepes abogado de la Coordinación Colombia-Europa-Estados Unidos, manifestó que la Jurisdicción Especial para la Paz “va mas allá que la justicia ordinaria, que colapsó por los altos niveles de impunidad, pues **es una justicia parcializada concentrada en el juzgamiento de sólo uno de los actores”.**

Afirma que el aparato normativo colombiano tiene un “papel débil frente a la responsabilidad de particulares y agentes estatales”. Según el abogado, es allí donde radica el temor de algunos sectores, pues la Jurisdicción Especial “contempla mecanismos efectivos de rendición de cuentas (…) **hay sectores que tienen pánico que la Comisión de la Verdad saque a flote verdades que afecten a personajes involucrados en crímenes de estado**, que son cercanos al centro democrático”. Le puede interesar: [“Jurisdicción Especial de Paz enfrentará la impunidad”: Comisión Ética.](https://archivo.contagioradio.com/jurisdiccion-especial-de-paz-enfrentara-la-impunidad-comision-etica/)

Además, resalta que “el eje principal de ataque a los acuerdos por parte del centro democrático es la eliminación de la Jurisdicción Especial (…) muchos generales, agentes estatales y terceros que **financiaron el paramilitarismo contarían la verdad y darían a conocer nombres de otros que hasta ahora han estado ocultos”**.

Yepes, hace un especial énfasis en la importancia que merece la defensa de los logros del punto cinco, apunta que “ese fue el punto que más se demoro debido a que tuvo una alta participación de víctimas, sentires y enfoques, étnico, diferencial, de género y territorial” y añade que **es fundamental la movilización social para hacer frente a la desinformación que promueven algunos medios.** Le puede interesar: [Las 6 respuestas a las víctimas sobre la jurisdicción especial de paz.](https://archivo.contagioradio.com/victimas-y-jurisdiccion-especial-de-paz/)

Por otra parte, indica que la Jurisdicción Especial para la Paz “en sus diseños fue examinada por distintas instancias internacionales como la Comisión Interamericana de Derechos, también fue avalada por la Fiscalía de la Corte Penal Internacional (…) por eso **sustraer la responsabilidad de los que deben rendir cuentas desequilibra la verdad, la justicia y la reparación”.**

Las organizaciones firmantes son: Coordinación Colombia – Europa- Estados Unidos (CCEEU), Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE), Mesa Ecuménica por la Paz, Comunidades Construyendo Paz en los Territorios (CONPAZ), Diálogo Intereclesial por la Paz (DiPaz), Plataforma Colombiana de Derechos Humanos, Democracia y Desarrollo y la Coalición de Movimientos y Organizaciones Sociales de Colombia (COMOSOC).

<iframe id="audio_13406747" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13406747_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe id="audio_13406813" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_13406813_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo [[bit.ly/1nvAO4u]
