Title: Obispos de Cauca y Nariño piden parar el flagelo del narcotráfico
Date: 2020-04-20 19:05
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Cauca, ELN, FARC, nariño
Slug: obispos-de-cauca-y-narino-piden-parar-el-flagelo-del-narcotrafico
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/conflicto.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Cauca conflicto/ MBS

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Ante el aumento de la violencia y la persecución contra el campesinado en pleno confinamiento y que se ha evidenciado en el asesinato dlíderes sociales en menos de tres días en Cauca; obispos de **Popayán, Pasto, Tumaco, Guapi, Ipiales y Tierradentro** hicieron un nuevo llamado a las estructuras armadas que promueven el narcotráfico en la región para que cesen sus actividades y den prioridad a la vida de las comunidades.  
  
"Es una realidad que podemos ver todos los días cuando salimos a nuestro ejercicio pastoral", afirma **Carlos Alberto Correa, vicario apostólico de Guapi, quien además acompaña desde su labor a los municipios de Lopez de Micay, Timbiquí en Cauca e Iscuande en Nariño**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Para el religioso, los enfrentamientos en estos municipios son una amenaza fruto de "la codicia humana" que además de verse reflejada en la minería ilegal, se ve en el accionar de grupos armados que promueven la coca, la tala indiscriminada y atacan directamente a quienes promueven la sustitución voluntaria.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Monseñor Correa señala que **hay una disputa por el territorio por la siembra de la coca** **entre las disidencias y el ELN, grupo que se fortaleció tras el cese de actividades de las FARC-EP**, sin embargo "hay personas que se están reforzando y ahora parece que ese grupo de las disidencias que se está fortaleciendo está en una persecución contra el ELN, incluso en medio del cese al fuego que decretó esa guerrilla hasta el 1 de mayo y del que advirtieron, estaría condicionado, si existían hostigamientos en su contra.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aclara que las distancias geográficas no permiten conocer el estado actual de cada comunidad, sin embargo es enfático en que **"quienes se atreven a confrontar a los grupos armados y exigir que se les deje por fuera de la guerra, siguen siendo una minoría y finalmente son asesinados"**.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En tan solo tres días, Andrés Cansimanse Burbano, Teodomiro Sotello y Mario Chilhueso campesinos que han sido asesinados en El Tambo y el Naya sonu reflejo de la violencia ejercida contra líderes que promueven el desarollo de sus comunidades. [(Le recomendamos leer: Teodomiro Sotelo, segundo líder asesinado en El Tambo, Cauca en un mes](https://archivo.contagioradio.com/teodomiro-sotelo-segundo-lider-asesinado-en-el-tambo-cauca-en-un-mes/))

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"Hay muchas sombras entorno a esta situación" afirma el vicario pues **aunque las comunidades quieren permanecer ajenas al conflicto, quedan confinadas en medio. Monseñor sugiere que sin tener a dónde más acudir, los armados estarían viviendo entre las comunidades como resultado de la cuarentena,** "las comunidades no pueden hacer absolutamente nada, ni decir sí ni decir no". [(Lea también: Puerta a puerta armados persiguen a excombatientes y líderes en Argelia, Cauca)](https://archivo.contagioradio.com/puerta-a-puerta-armados-persiguen-a-excombatientes-y-lideres-en-argelia-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Agrega que a este problema se suma el reclutamiento de jóvenes en las veredas y corregimientos, algo que silencia más a las comunidades, "que empiezan a ser involucradas cuando se llevan a sus hijos y fuera de eso las chicas se involucran sentimentalmente con los armados, lo que se vuelve una cadena muy fuerte que termina por inmiscuir a la población". [(Lea también: Fue asesinado Mario Chilhueso, líder campesino del Naya)](https://archivo.contagioradio.com/fue-asesinado-mario-chilhueso-lider-campesino-del-naya/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El Estado debe buscar la manera de solucionar la precariedad en Cauca

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Al respecto afirma que es necesario apelar a la conciencia y darse cuenta que se están creando **"eslabones de muerte"**, además reiteró la responsabilidad del Estado de atender las necesidades de un departamento como el Cauca donde hay precariedad en el empleo, la educación y el desarrollo social de los municipios, "como iglesias estamos muy preocupados por esta situación, los obispos hemos hecho un llamado a que cese la codicia que es fruto del dinero y de las armas".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque **Cauca es uno de los departamentos más militarizados,** monseñor Correa afirma que pese a la abundante presencia de la infantería de marina, no tienen la potestad de actuar sin el consentimiento de los consejos comunitarios que estarían siendo coaccionados por las estructuras ilegales. Agrega además que el pie de fuerza de la Policía es inferior por lo que afirma, se trata de un problema estructural que no ha tenido respuesta.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El problema estructural del departamento

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El problema del Narcotráfico, no es el único en el departamento y la muestra de ello ha quedado evidenciada, con sus sombras, en los últimos días.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La proliferación de los grupos armados que se disputan el control de los territorios, los problemas estructurales en torno a la presencia del [Estado](https://twitter.com/infopresidencia)con salud y educación, la falta de oportunidades de sostenimiento al margen del narcotráfico, la presencia de las grandes empresas azucareras, la concentración de la tierra, son solamente algunos de los problemas que agudizan los problemas del departamento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Todo ello sugiere, que más allá de una decisión de los grupos dedicados al narcotráfico, también hace falta una decisión del Estado de tener una presencia firme en este territorio, con respeto por la autonomía de las comunidades indígenas y negras, pero con grandes focos de inversión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
