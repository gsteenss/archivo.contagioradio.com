Title: La ANUC lidera una expedición por Colombia a favor de la paz
Date: 2016-07-10 07:00
Category: Nacional, Paz
Tags: ANUC, Diálogos de paz Colombia, Diálogos Gobierno - FARC, Dialogos gobierno eln
Slug: la-anuc-lidera-una-expedicion-por-colombia-en-favor-de-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Comunidades-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ] 

###### [8 Julio 2016]

La Asociación Nacional de Usuarios Campesinos ANUC ha dado pasos importantes para la promoción del proceso de paz en Colombia y en esta ocasión promueve una **expedición por 26 departamentos con el propósito de entablar diálogos con las comunidades** e invitarlas a que voten a favor del plebiscito. Los líderes que promoverán esta expedición también interlocutarán con alcaldes, funcionarios y empresarios para la ejecución de proyectos con los que se superen las desigualdades en el campo, como causa del conflicto armado, según afirma Luis Jiménez, presidente de la Asociación.

"Hay un tema que preocupa mucho y es que las administraciones municipales, por una parte, asumen posturas muy unilaterales, poco consultadas con las comunidades, y eso hace que la gestión administrativa resulte igualmente poco pertinente, y esa es una dificultad seria que hay que superar; que **los alcaldes asuman una postura de mayor diálogo con comunidades para que su acción se fortalezca** (...) sabemos de las dificultades financieras en los municipios, pero la participación de la comunidad hace más eficiente el uso de los recursos", asegura Jiménez.

De acuerdo con el líder, las organizaciones que integran la ANUC siempre han sostenido que **en la medida en que el proceso necesite fortalecerse, todos los actores tendrán que ofrecer condiciones**, es decir, no cerrarles la puerta a los combatientes de las FARC-EP que se reintegren a la vida civil en las zonas de concentración, entre otras, porque eso no significaría reconciliación. En tal sentido, llaman también a las delegaciones de paz del ELN y del Gobierno a que sumen esfuerzos para que la mesa avance.

<iframe src="http://co.ivoox.com/es/player_ej_12164786_2_1.html?data=kpeemJmbfJehhpywj5aXaZS1lpuah5yncZOhhpywj5WRaZi3jpWah5ynca3pytiYrM7Rqc_Z24qfpZClkra3joqkpZKns8_owszW0ZC2pcXd0JCah5yncZU.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
