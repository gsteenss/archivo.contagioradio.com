Title: La polémica detrás del proyecto de Ecopetrol en Guamal, Meta
Date: 2017-04-12 08:28
Category: Ambiente, Nacional
Tags: Ecopetrol, Meta
Slug: la-polemica-detras-del-proyecto-ecopetrol-guamal-meta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/ecopetrol.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Negocios y Petróleo 

###### [11 Abr 2017]

Tras la denuncia de líderes de la comunidad de Guamal en el Meta, sobre las irregularidades y falta de garantías de participación para los pobladores de la vereda Pio XII, donde se busca desarrollar exploración petrolera por parte de Ecopetrol, la empresa habló con Contagio Radio y asegura **estar a disposición de la comunidad para dialogar sobre las preocupaciones de las personas sobre el proyecto petrolero.**

Eduaro Uribe, Vicepresidente de sostenibilidad de Ecopetrol, afirma que lo que se busca con el proyecto es conocer las dimensiones de unas posibles reservas petroleras que puede haber en la zona. Asimismo agrega que las socializaciones se han hecho como ordena la Ley y que el proyecto cuenta con los estudios necesarios que demostrarían que no habría ningún tipo de impacto ambiental para este tipo de actividad y por ende **no habría incompatibilidad alguna entre la vocación agrícola de la región y la actividad petrolera.**

“Sobre toda la altillanura existen acuíferos que son los que aprovechan algunas comunidades. Los pozos se perforan a 1, 2 o 3 kilómetros de profundidad. No hay ningún contacto con las aguas pues son pozos revestidos de cemento y que son herméticos, de manera que no hay contaminación. Todo esto lo hemos explicado en detalle”, sostiene el funcionario de la compañía petrolera.

Por otro lado, cabe recordar que en una entrevista pasada, Edgar Cruz, habitante de la comunidad, explicó que **en febrero del 2014 la empresa había asumido la refinanciación de estudios técnicos en la zona para verificar posibles impactos ambientales** con expertos sugeridos por las comunidades, pero certificados profesionalmente, sin embargo, asevera que estos no se han hecho. [(Le puede interesar: Comunidades de 8 municipios del Meta rechazan proyecto petrolero)](https://archivo.contagioradio.com/comunidades-8-municipios-del-meta-rechazan-proyecto-petrolero-ecopetrol/)

“Se ha desarrollado una campaña mediática donde dicen que todo está concertado cuando a los habitantes de vereda  Pio XII no los han tenido en cuenta”, denuncia Cruz, quien agrega que existe una sentencia de la Corte Constitucional que obligó a que la actividad en el pozo Lorito 1 fuera detenida desde 2013.

Esa orden indica que se debe proteger zonas de recarga hídrica, y en esa área justamente fue donde se construyó la plataforma de Ecopetrol, donde existen **1032 hectáreas de reserva subterránea de agua dulce**. No obstante, se denuncia que pese a la sentencia, se piensa construir una nueva plataforma a 300 o 400 metros de distancia con base en **los mismos estudios y sobre la misma reserva de agua.**

Por su parte, el funcionario de Ecopetrol explica que no son todas las personas las que rechazan la realización del proyecto, debido a los beneficios que según la empresa, este tipo de actividades puede traer a la región, en materia educativa, de insfraestructura, de salud e incluso ambiental, pues la empresa debe usar el 1% de su inversión en planes de restauración ecológica, como lo indica la Ley.

De hecho, según el funcionario, Ecopetrol ya compró terrenos alrededor del río Humadea para iniciar un proyecto de ecoturismo y revitalizar la zona ambientalmente, **“Uno de los retos más grandes de Ecopetrol es demostrar que la prosperidad tiene que ser local,** tenemos que ser capaces de que las comunidades vean que podemos traer beneficios”.

En esa misma línea asegura que la compañía ya ha invertido entre mil y 2 mil millones de pesos en el centro de adulto mayor, un centro cultural para el municipio, pavimentación de carreteras, y además dice que de encontrarse reservas de crudo con la actividad de exploración, los beneficios para la región serían aún mayores. [(Le puede interesar: Sigue en firme la defensa del Río Humadea en Guamal)](https://archivo.contagioradio.com/sigue-firme-la-defensa-del-rio-humadea-en-guamal/)

Un estudio de la Universidad Industrial de Santander, concluye que **por cada barril de petróleo se usan 13 de agua,** algo que confirma Eduardo Uribe, quien explica que existen unas plantas de tratamiento de agua que permiten que parte de esas aguas después puedan reutilizarse para la irrigación de cultivos de palma.

Sin embargo, pese a las declaraciones de la empresa a Contagio Radio, las preocupaciones expresadas por líderes de la comunidad como Edgar Cruz siguen vigentes, ya que señalan que tanto el gobierno como la alcaldía y la gobernación, se encuentran a disposición de la actividad petrolera, pues la misma gobernadora Marcela Amaya, quien, de acuerdo con Cruz, quiere consolidar el departamento Meta como productor de crudo, aunque eso no fue lo que prometió a las comunidades durante su campaña. Además **“No se ha hablado de la participación de la comunidad, lo que quiere el gobierno es bloquear a la movilización** de las comunidades”, concluye. [(Le puede interesar: Policía desaloja campamento de campesinos en Guamal Meta)](https://archivo.contagioradio.com/policia-desaloja-campamento-campesinos-guamal-meta/)

<iframe id="audio_18113810" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18113810_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
