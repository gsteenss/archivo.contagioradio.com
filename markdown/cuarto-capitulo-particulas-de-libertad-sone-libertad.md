Title: Cuarto capítulo Partículas de Libertad: soñé libertad
Date: 2020-11-03 15:53
Author: AdminContagio
Category: Expreso Libertad, Programas
Tags: Expreso Libertad, montaje judicial, montajes judiciales, radionovela, soñelibertad
Slug: cuarto-capitulo-particulas-de-libertad-sone-libertad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/PARTICULAS-DE-LIBERTAD.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 27 de octubre se estrenó el cuarto capítulo de la radionovela **Partículas de Libertad, titulado: soñé libertad.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este episodio, las cinco jóvenes deben confrontar a Alfonso, quién les tendió el montaje judicial, en los estrados. Allí su abogado defensor intentará develar cómo el policía infiltrado creó un plan para inculpar a los estudiantes de pertenecer a un supuesto grupo armado.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_60124872" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_60124872_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

En este lanzamiento del Expreso Libertad, también nos acompañó Carlos Alberto Rincón, compañero de Érika Flores, que denuncia haber sido víctima de un montaje judicial y su abogado defensor Eduardo Matías.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_58640405" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_58640405_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

[Mas de Expreso Libertad](https://archivo.contagioradio.com/programas/expreso-libertad/)

<!-- /wp:paragraph -->
