Title: Derechos de la comunidad LGTBI en las cárceles de Colombia
Date: 2018-09-06 17:36
Author: AdminContagio
Category: Expreso Libertad
Tags: carceles de colombia, lgtbi
Slug: derechos-de-la-comunidad-lgtbi-en-las-carceles-de-colombia
Status: published

###### Foto: Cuerpos en Prisión 

###### 28 Ago 2018 

Una tutela y cinco años de espera, fue el tiempo que necesitó la Comunidad Trans de las cárceles del país, para que se respetaran sus derechos a ser libremente. Éste ha sido uno de los mayores logros de las luchas de la comunidad LGTBI al interior del sistema penitenciario por defender la diversidad.

En este Expreso Libertad conozca más de cerca los avances de la protección del derecho a la diferencia en los centros de reclusión de Colombia.

<iframe id="audio_28280169" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28280169_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
