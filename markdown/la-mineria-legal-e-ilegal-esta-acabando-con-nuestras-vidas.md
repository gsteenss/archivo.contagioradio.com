Title: "La minería legal e ilegal está acabando con nuestras vidas"
Date: 2016-01-27 10:29
Category: Movilización, Nacional
Tags: bloqueos en Sogamoso, minería en Boyacá, protestas en Boyacá
Slug: la-mineria-legal-e-ilegal-esta-acabando-con-nuestras-vidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Minería-Boyacá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Que es. ] 

<iframe src="http://www.ivoox.com/player_ek_10217230_2_1.html?data=kpWfk5yWd5Ghhpywj5WdaZS1kZmah5yncZOhhpywj5WRaZi3jpWah5yncYamk7HOjdLNssbmhqigh6aopYzgxszOzpDJb8rgxszOzpDJt9WZpJiSo5aPpcTVw8bbxtSPp9DijNPix9jYtsLnjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gustavo Guzmán, campesino] 

###### [26 Ene 2016. ] 

Continúan las [[movilizaciones en varios puntos del departamento de Boyacá](https://archivo.contagioradio.com/campesinos-de-boyaca-estan-cansados-de-comer-tierra/)] por cuenta del inconformismo que las comunidades campesinas han manifestado frente a los **altos niveles de contaminación, la destrucción de viviendas, la proliferación de distintos tipos de cáncer y enfermedades respiratorias y la destrucción de acueductos comunitarios**, como parte de los impactos negativos de la explotación minera en sus territorios.

Gustavo Guzmán líder campesino y habitante de una de las veredas del municipio de Sogamoso, asegura que la remoción de tierra producida por la “avalancha de mineras” es la principal causante de que los tubos de los acueductos comunitarios se perforen y generen desperdicios de agua, que ponen **en riesgo la supervivencia de por lo menos tres comunidades en las que escuelas y centros de salud se han visto en la obligación de declarar emergencia sanitaria**.

“La minería tanto legal como ilegal está acabando con nuestras casas, con nuestras vidas y con nuestro derecho a vivir dignamente como campesinos” asevera Guzmán, e insiste en que por cuenta de las licencias otorgadas a empresas mineras que no son de la región **“Sogamoso se ha convertido en un corredor completamente contaminado”**, en el que pese a las prohibiciones se está explotando carbón en el páramo localizado en inmediaciones de las veredas Las Cintas y Las Cañas.

Así mismo los pobladores tienen conocimiento de una **concesión que sería entregada a una empresa multinacional** para la explotación de roca fosforita en un extensión de 400 hectáreas entre los municipios de Iza y Sogamoso, un hecho frente al que las comunidades denuncian correr "el **riesgo de un desplazamiento y de que se acabe con el territorio ancestral**", como afirma Guzmán.

“Hacemos un llamado a las autoridades para que se abstengan de entregar más títulos mineros, que **frenen la minería porque nosotros estamos siendo desplazados**, nuestras casas se están derrumbando. **Somos campesinos pacíficos que exigimos que nos dejen protestar por nuestros derechos**”, asegura Guzmán y agrega que la respuesta por parte del alcalde de Sogamoso es que la atención de estas problemáticas es de competencia nacional. “Exigimos la presencia del gobernador y del ministro de medio ambiente porque **no queremos ver a nuestros campesinos sufrir más**”, concluye Guzmán.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
