Title: PND y Ley Estatutaria de Salud no son compatibles
Date: 2015-02-16 21:17
Author: CtgAdm
Category: DDHH, Nacional
Tags: Juan Manuel Santos, Ministerio de Salud, Ministro Alejandro Gaviria, Salud, tutela
Slug: pnd-y-ley-estatutaria-de-salud-no-son-compatibles
Status: published

##### Foto: [www.moir.org.co]

Este lunes la **Ley Estatutaria de Reforma a la Salud** se firmó por el Presidente Juan Manuel Santos**.** De acuerdo al Gobierno, desde ahora se considerará constitucionalmente el derecho a la salud como un derecho fundamental. Pese a esa conclusión, de acuerdo a Clemencia Mayorga, vocera nacional de la **Mesa Nacional Por el Derecho a la Salud**, es claro que **el Plan Nacional de Desarrollo, (PND) 2014 – 2018 no es compatible con la Ley**, lo que pondría en riesgo el estricto cumplimiento que requiere.

Anteriormente, el senador del Polo Democrático, Jorge Robledo, había denunciado que el presidente Juan Manuel Santos y el Ministro, Alejandro Gaviria, **habían aprobado una [ley estatutaria de salud en favor de las EPS](https://archivo.contagioradio.com/entrevistas/plan-nacional-de-desarrollo-empezo-cojo-jorge-robledo/),** sin embargo, la **Corte Constitucional** reformó la Ley y no permitió que derechos ciudadanos como el de la tutela, se vulnerarán por cuenta de la propuesta del gobierno.

Para Mayorga, el PND y la Ley Estatutaria deben “ir en la misma vía”  y afirmó que la “Corte Constitucional dijo que **el derecho a la salud no está supeditado a sostenibilidad fiscal”,** aunque es crucial que para que la Ley se cumpla, "los colombianos deben empezar a exigir su derecho a la salud”.

Entre los lineamientos más importantes que se estipularon en esta reforma, están que fortalece el **control de precios sobre los medicamentos,** generando mayor acceso; establece el concepto de **Integralidad**; se elimina el Plan Obligatorio de Salud **(POS)**; **no existirán regiones desatendidas y la tutela se mantendrá** como mecanismo para hacer valer los derechos de salud de los colombianos y las colombianas.
