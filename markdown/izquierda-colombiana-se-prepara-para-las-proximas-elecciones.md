Title: Izquierda colombiana se prepara para las próximas elecciones
Date: 2016-01-25 14:53
Category: Nacional, Política
Tags: elecciones 2018, izquierda colombiana, Movimiento social
Slug: izquierda-colombiana-se-prepara-para-las-proximas-elecciones
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/CZVSt9XWIAAs0gl.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Twitter. 

<iframe src="http://www.ivoox.com/player_ek_10193934_2_1.html?data=kpWem5idd5Whhpywj5aUaZS1lZyah5yncZOhhpywj5WRaZi3jpWah5ynca3VjM7n09rNqdPYwpDQ0dHTscPdwtPOjcfZt8TVjMjc0NvJtsjZz8jWw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alirio Uribe, Polo Democrático.] 

###### [25 Ene 2015.] 

El pasado viernes, diferentes lideres y lideresas de la izquierda colombiana se reunieron con el fin de construir una ruta de convergencia y unidad del movimiento social en Colombia, **con miras a las próximas elecciones al congreso y presidencia del 2018.**

De acuerdo con el representante del Polo Democrático Alternativo, Alirio Uribe, para esa convergencia de la izquierda, es necesario llenar el vacío que existe en  materia de comunicaciones, **para lo que propusieron crear una red de medios que permita contrarrestar la información oficial**, ya que “la comunicación es política”, dice Uribe Muñoz.

Otro de los temas a fortalecer tiene que ver con **la defensa de lo público**, teniendo en cuenta las políticas del gobierno nacional y distrital en las que se evidencia que el fin es privatizar las empresas de los colombianos.

Finalmente, se analizaron algunas estrategias electorales para el 2018, evaluando lo sucedido en las últimas elecciones, en las que la izquierda perdió, entre otras, la alcaldía de la ciudad. **“Hemos cometido muchos errores, y se han cobrado, hemos estado divididos, y no hemos podido mandar un mensaje comunicacional** que salga a conquistar a otras personas afectadas por el modelo económico”, y que “permitan mandar un mensaje al país de que queremos unidad”, dice el representante del Polo Democrático Alternativo.

“¿La izquierda tienen la posibilidad de tener un candidato único? ¿Somos capaces de lograr ese proceso de convergencia? ¿Tenemos la fuerza para tener un candidato? ¿tenemos la capacidad de poner un programa político? ¿Cuál sería el mecanismo o vehículo de unidad?”, son algunas de las preguntas que deja en el debate el congresista, quien concluye que “la unidad no se construye con las cúpulas, se construye a nivel local, de manera que son las bases son las que tienen que presionar a sus dirigentes”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
