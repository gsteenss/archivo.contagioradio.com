Title: Víctimas exigen garantías de seguridad para excavaciones en "La Escombrera"
Date: 2015-08-04 17:25
Category: DDHH, Otra Mirada
Tags: Corporación Jurídica Libertad, desaparecidos colombia, La escombrera, Medellin
Slug: victimas-exigen-garantias-de-seguridad-para-excavaciones-en-la-escombrera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/escombrera2-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: pulzo 

###### [4 Agosto 2015]

Se prevé que este miércoles 5 de Agosto se de la **entrada de maquinaria para iniciar las labores de búsqueda, luego del proceso de identificación y delimitación de la zona**. Según Adriana Arboleda, abogada de la Corporación Jurídica Libertad, la fiscalía parece estar muy comprometida con el trabajo que se está realizando y se han acogido tanto las recomendaciones de las víctimas y de sus acompañantes como sus exigencias.

En este proceso se conoció que los grupos de paramilitares que están presentes en la Comuna 13 de Medellín han afirmado que no van a permitir que se cierre la escombrera, según algunos analistas, estas amenazas, aunque no “oficiales” obedecen a dos factores como la presencia de policía y ejército, la **disposición de 2 cordones de seguridad alrededor del sitio de trabajo**, pero también a la pérdida de entradas económicas derivadas del cobro de impuesto de entrada al sitio por parte de empresas constructoras o transportadores de escombros.

Aunque ni las víctimas, ni los organismos de investigación ni otras entidades han recibido información directa de estas amenazas, tampoco les extrañan los rumores que crecen en ese sentido puesto que es evidente la presencia y el control paramilitar de las comunas alrededor de la escombrera. Arboleda afirma que a pesar de ello se han realizado varias acciones para garantizar la seguridad.

### **Organizaciones de víctimas y de Derechos Humanos realizan recomendaciones de seguridad** 

1.  Quienes custodian la zona no pueden haber trabajado en ese sector, debido a las reconocidas relaciones o actuaciones conjuntas entre paramilitares y policías o ejército.
2.  El personal que realice trabajos de búsqueda no puede tener ninguna investigación judicial pendiente ya que podrían poner en riesgo los hallazgos.
3.  Es necesario que haya veeduría internacional y de acompañamiento al campamento de las víctimas.

Aunque estas solicitudes no garantizan al 100% la seguridad de las personas y de los trabajos, si pueden significar un **compromiso político real por parte de las autoridades del Estado comprometidas en estas tareas.**

### **El compromiso de la Fiscalía** 

Además de cumplir las recomendaciones de las víctimas y las organizaciones de DDHH, la tarea va más allá de las garantías de seguridad, pasan por el diálogo con las víctimas, la resolución de las inquietudes, la instalación de infraestructura para los campamentos de víctimas y de policía, y la adecuación de luz para tener iluminación las 24 horas.

Según la **Corporación Jurídica Libertad** se debe entender que el trabajo que se está realizando en “la escombrera” es abrir un camino y establecer unos pasos para la búsqueda de las cerca de 50 mil víctimas de desaparición forzada en Colombia y se debe asumir como una tarea histórica.

Hasta el momento, según Adriana Arboleda, hay muestras de voluntad, pero el proceso es largo. (Vea [Búsqueda de desaparecidos en la escombrera debe ser una labor integral](https://archivo.contagioradio.com/busqueda-de-desaparecidos-en-la-escombrera-debe-ser-una-labor-integral/))

### **La verdad que no se ha podido desaparecer en “La Escombrera”** 

El 29 de Mayo de 2003 se desarrolló la primera etapa de la **Operación Orión**, que según el paramilitar extraditado “Don Berna” fue planificado y ejecutado de manera conjunta entre los paramilitares y la IV Brigada del ejército comandada por el General Mario Montoya. Según cifras de organizaciones de DDHH durante los días de la operación se produjeron **335 detenciones arbitrarias, 39 civiles heridos y 7 personas desaparecidas.**

Pero ni esas operación, ni esas cifras son toda la historia. El control paramilitar derivado de la operación Orión permitió que sobre los escombros que más de 300 volquetas llevan a diario, se siguiera enterrando la verdad, allí hay hasta desechos químicos que podrían afectar lo poco que se puede encontrar después de 12 años. Además las organizaciones de DDHH han logrado establecer que **hay 92 personas, pero la cifra real es más espantosa.**

El lugar de las excavaciones ocupa cerca de una cuarta parte de todo el botadero de desechos que se extiende en 15 hectáreas en la comuna 13, esos otros sectores siguen recibiendo escombros de toda la ciudad a diario. (Vea también [En la escombrera se removerá la impunidad](https://archivo.contagioradio.com/en-la-escombrera-no-solo-deberan-removerse-escombros-sino-tambien-la-impunidad/))
