Title: Vaticano pide investigación imparcial e independiente por crimen contra Berta Cáceres
Date: 2016-03-28 12:14
Category: El mundo, Judicial
Tags: asesinato Berta Cáceres, Berta Cáceres, Copinh, Vaticano
Slug: vaticano-pide-investigacion-imparcial-e-independiente-por-crimen-contra-berta-caceres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Cáceres-Papa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alaintet] 

###### [28 Mar 2016] 

A través de un comunicado, el cardenal Peter Turkson, actual presidente del 'Pontificio Consejo para la Justicia y la Paz del Vaticano', pidió una **"investigación independiente e imparcial" frente al asesinato de la líder indígena** [Berta Cáceres](http://a%20fin%20de%20que%20esclarezca%20este%20horrendo%20crimen%20a%20la%20mayor%20brevedad%20possible,%20y%20sea%20protegida%20la%20integridad%20física%20de%20los%20testigos%20,%20compañeros%20y%20familiares%20de%20las%20víctimas) "a fin de que esclarezca este horrendo crimen a la mayor brevedad possible, y sea protegida la integridad física de los testigos , compañeros y familiares de las víctimas".

La comunicación fue enviada a familiares, amigos y compañeros de Cáceres a quienes el cardenal les extendió sus "más sentidas condolencias", manifestándoles que había podido "conocer de primera mano su extraordinaria labor en defensa de la hermana nuestra madre tierra y en la reafirmación de los derechos de los pueblos originarios", por lo que su asesinato lo sintió "como **un ataque a todos los que luchamos por un mundo más justo**, con tierra, techo y trabajo para todos, un medio ambiente sano y la pacífica convivencia de los pueblos".

La carta enviada por el cardenal, se suma a las [[peticiones de distintas organizaciones](https://archivo.contagioradio.com/mision-internacional-pide-acelerar-investigacion-por-crimen-de-berta-caceres/)] sociales hondureñas y de otras parte del mundo para que el crimen de Cáceres sea esclarecido y sancionado, concluye pidiendo **"que una situación semejante no vuelva a suceder"** y rogando que sean protegidos todos aquellos que defienden "la Madre Tierra y los derechos sagrados de los pueblos".

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]
