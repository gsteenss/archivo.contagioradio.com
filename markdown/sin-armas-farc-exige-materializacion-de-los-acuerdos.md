Title: Las preocupaciones de las FARC luego de la dejación total de armas
Date: 2017-06-27 13:21
Category: Nacional, Paz
Tags: acuerdos de paz, Acuerdos de paz con las FARC, Dejación de armas, FARC, Jesús Santrich
Slug: sin-armas-farc-exige-materializacion-de-los-acuerdos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/las-preocupaciones-de-las-farc-luego-de-la-entrega-de-armas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Farc] 

###### [27 Jun 2017]

En el acto de dejación de las armas en Mesetas, Meta, las FARC ratificó la entrega a la Misión de la ONU de 7.132 armas. **Desde hoy pasarán a consolidar el partido político que los representará en la vida civil.** Sin embargo, hay escepticismo por parte de esa guerrilla frente al cumplimiento de los acuerdos por parte del gobierno.

A pesar de los retrasos en las instalaciones de las Zonas Veredales y Puntos de Transición y Normalización, la guerrilla más antigua del continente Latino Americano y según Rodrigo Londoño, **“ha cumplido con el punto 3 de los acuerdos de la Habana, el fin del conflicto”**. De igual forma, hoy le han pedido al Gobierno y a la sociedad en general que “se deje a un lado la mentalidad mezquina y militarista que ha imperado en Colombia”. (Le puede interesar: "[Así vivieron los habitantes de la Elvira en Cauca la dejación de armas de las FARC](https://archivo.contagioradio.com/asi-vivieron-los-habitantes-de-la-elvira-en-cauca-la-dejacion-de-armas-de-las-farc/)")

Para Jesús Santrich, miembro del secretariado de las FARC, “hoy es un día de reflexión y no de celebración. Hay promesas que necesitan ser materializadas por parte del gobierno quien desde el principio no cumplió con la adecuación de los campamentos de transición ”. Así mismo, manifestó **“las FARC hemos demostrado nuestra voluntad de paz** y ahora el Gobierno Nacional tiene que cumplir con la totalidad de los compromisos”.

**Es momento de la materialización de los acuerdos**

En lo que concierne al paso siguiente de la dejación de armas, Santrich recordó que **“los acuerdos de paz no son para las FARC sino para toda Colombia,** hoy le estamos dando valor a la palabra para que se escuchen las voces de los oprimidos”. Para esta guerrilla el paso siguiente es que “el gobierno afronte el problema del paramilitarismo para proteger a los líderes sociales, a los campesinos y a los indígenas”. (Le puede interesar: "[Dejación de armas de las FARC marca un cambio en la historia del país](https://archivo.contagioradio.com/42169/)")

Según ellos, “el Gobierno tiene la capacidad de solucionar los problemas de las comunidades y tiene el deber de cumplir con la totalidad de los acuerdos”. **Jesús Santrich hizo énfasis en los guerrillero que se encuentran en las cárceles colombianas**; “los prisioneros políticos aún no han salido de la cárcel, eso es un acuerdo al que se llegó y que el gobierno no ha cumplido”.

Por su parte, el representante a la Cámara, Alirio Uribe, destacó como **una decisión valiente de las FARC el hecho de haber entregado sus armas**. Manifestó que “la dejación es una forma de acallar a los enemigos de la paz que decían que las FARC no iban a firmar, no se iban a concentrar y que no iban a entregar las armas”.

Sin embargo, hizo énfasis en que los **miembros de esa guerrilla aún tienen muchas preocupaciones sobre lo que será su futuro en la sociedad**. “Los guerrilleros empiezan un cambio de vida con inquietudes en su seguridad física y política y las condiciones de educación, salud y trabajo una vez hagan el tránsito a la vida civil”. (Le puede interesar: "[FARC inicia dejación de armas "sin pesar ni llanto"](https://archivo.contagioradio.com/el-20-de-junio-las-farc-habra-entregado-todas-sus-armas/))

Si bien con el acto de la dejación de armas se ratificó el compromiso de las FARC por el cumplimiento de la totalidad de los acuerdos, **aún quedan deberes recíprocos para lograr que los acuerdos de paz se materialicen.** Para Alirio Uribe, “la sociedad colombiana y el gobierno tiene que garantizar que los acuerdos se cumplan para hacer el tránsito completo hacia una vida en paz”.

<iframe id="audio_19499744" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19499744_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
