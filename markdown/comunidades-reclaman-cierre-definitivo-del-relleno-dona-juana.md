Title: Comunidades reclaman cierre definitivo del relleno 'Doña Juana'
Date: 2016-02-15 13:30
Category: Movilización, Nacional
Tags: Doña Juana, Relleno sanitario Doña Juana
Slug: comunidades-reclaman-cierre-definitivo-del-relleno-dona-juana
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Audiencia-Popular-Doña-Juana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Javier Reyes ] 

<iframe src="http://www.ivoox.com/player_ek_10443493_2_1.html?data=kpWhlpiYfZShhpywj5WcaZS1kpaah5yncZOhhpywj5WRaZi3jpWah5yncaTjztrby8nFqMbnjNfSxdHFscLijMjWx9fWqYzYxsvW0M7YrdfjjMnSzpDWqc3gxtPcjYqWe6Xjhqigj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Julián Arana] 

###### [15 Feb 2016 ] 

Desde las siete de la mañana de este lunes cerca de ochenta habitantes de las localidades de Ciudad Bolívar y Usme, áreas de influencia del relleno sanitario 'Doña Juana', se manifestaban pacíficamente en la entrada de este vertedero, como parte de las estrategias acordadas en la Audiencia Popular que busca el **cierre definitivo de este basurero y el pago de la deuda social con las comunidades afectadas por los derrumbes y las crisis sanitarias** que han provocado sus veintiocho años de funcionamiento.

De acuerdo con Julian Arana, integrante de la Mesa Ambiental [‘[No le saque la piedra a la montaña’]](https://archivo.contagioradio.com/el-ultimo-bosque-semi-seco-de-colombia-bajo-amenaza-minera/), **pese a que en el plantón no se había presentado ningún brote de violencia** por parte de los manifestantes, en su mayoría adultos mayores, y de que se había acordado impedir el paso de los camiones de la basura por cuarenta y cinco minutos, pasado apenas un cuarto de hora, **"llegó el ESMAD y la Policía, atropelló a la comunidad y se llevó a uno de los líderes"**, sin generar ningún tipo de interlocución y sin que estuviera presente algún equipo de derechos humanos de la Alcaldía.

Según indica Arana esta acción se había acordado en la Segunda Audiencia Popular realizada el pasado sábado en la que se acordó con la Unidad Administrativa Especial de Servicios Públicos Uaesp, instalar una mesa de negociación para determinar los **incumplimientos del 'Centro de Gerenciamiento de Residuos Doña Juana S.A. E.S.P', así como sus pasivos ambientales y sociales, para revertir su contrato** y poner en marcha un sistema integral de manejo y aprovechamiento de residuos, en el que las comunidades participen activamente.

Son **por lo menos seis mil las toneladas de basura que diariamente se vierten en el relleno sanitario** y de acuerdo con la comunidad y algunos estudios académicos realizados, el mal manejo de estos residuos ha **provocado enfermedades respiratorias y reacciones alérgicas en los pobladores de los ocho barrios** que limitan con 'Doña Juana' y que rememoran dos episodios de derrumbes por acumulación de lixiviados, el primero ocurrido en 1997 y el segundo en octubre del año pasado, por lo que **exigen que este modelo de recolección de residuos se transforme.  **

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
