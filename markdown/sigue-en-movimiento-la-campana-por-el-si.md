Title: Sigue en movimiento la campaña por el sí
Date: 2016-09-28 10:55
Category: Paz
Tags: Acuerdos de paz en Colombia, Conversaciones de paz con las FARC, Diálogos de paz Colombia
Slug: sigue-en-movimiento-la-campana-por-el-si
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/pazsi.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Súmate al Sí] 

###### [28 Sep 2016] 

Después de la firma del acuerdo final para la terminación del conflicto este lunes en Cartagena, esta semana continúan los eventos a nivel nacional **para impulsar el sí en el plebiscito**. Las campañas [[Súmate al SÍ](https://archivo.contagioradio.com/crecen-seguidores-de-campanas-como-sumate-al-si-en-colombia/)] y [[La Paz SÍ es contigo](https://archivo.contagioradio.com/colombia-esta-de-fiesta-con-la-paz-si-es-contigo/)], siguen creciendo, organizaciones de jóvenes, mujeres y ambientalistas, por mencionar algunas, siguen nutriendo estas campañas con propuestas desde el arte, la música y la danza.

Queda poco menos de una semana para que la ciudadanía se acerque a los distintos puntos de votación dispuestos en Colombia y fuera del país, para refrendar los acuerdos de La Habana. Teniendo en cuenta que hay quienes aún no se deciden, las **organizaciones que vienen promoviendo la socialización de los acuerdos** y las respuestas de por qué dar el sí, han agendado para esta semana algunos eventos en Bogotá y otras ciudades.

En la Universidad Santiago de Cali sede Palmira se llevará a cabo este miércoles 28, a partir de las 9:30 de la mañana, el foro "La Paz es Joven", que contará con la participación de docentes especializados en el tema de conflicto colombiano y juventud.

![14468202\_1209090745821726\_8181587545336338993\_o](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/14468202_1209090745821726_8181587545336338993_o.jpg){.alignnone .size-full .wp-image-29962 width="2358" height="1113"}

En el Colegio San Bartolomé La Merced se llevará a cabo una Cena por la Paz, convocada por RedePaz y un Millón de Mujeres por la Paz, este jueves 29 a partir de las 6:00pm.

El viernes 30 de septiembre en Bogotá, la organización Común Acuerdo realizará el evento "Vamos a bailar por la paz", una Bailatón en el parque el Virrey desde las 4:30pm y estará abierto para todo público.

![1044694\_10154662419762845\_1215422230509648991\_n](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/1044694_10154662419762845_1215422230509648991_n.jpg){.size-full .wp-image-30029 .aligncenter width="477" height="296"}

Adicionalmente en parques de algunas localidades de Bogotá y en Universidades, organizaciones estudiantiles y juveniles realizarán tomas culturales y ollas comunitarias para compartir con gente de todas las edades [[puntos importantes del acuerdo](https://archivo.contagioradio.com/acuerdo-final-de-paz-entre-gobierno-y-farc/)] y lo que implica el sí a la paz.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
