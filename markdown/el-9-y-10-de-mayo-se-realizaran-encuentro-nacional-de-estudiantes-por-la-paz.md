Title: Encuentro Nacional de Estudiantes por la Paz
Date: 2015-05-04 12:30
Author: CtgAdm
Category: Educación, eventos, Nacional, Paz
Tags: encuentro, estudiantes, estudiantil, luis fernando, mane, mesa amplia, universidad pedagogica, vega
Slug: el-9-y-10-de-mayo-se-realizaran-encuentro-nacional-de-estudiantes-por-la-paz
Status: published

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_4444388_2_1.html?data=lZmhlpicfI6ZmKiakpyJd6KllpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic2fmpDmjZaUb8XZjNLO29SPt8af08rOzs7epdOZpJiSo5bSb6bixNrS0NnWs4zCwsjW0dPFsIzYxpCah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luis Fernando Vega, Estudiante Universidad El Rosario] 

Estudiantes universitarios y de secundaria se reunirán el sábado 9 y domingo 10 de mayo en la Universidad Pedagógica Nacional, en Bogotá, para debatir cuál es su papel en la construcción de la paz en Colombia y hacerse participes de los procesos que adelanta el gobierno actualmente con las guerrillas de las FARC y el ELN.

"Queremos que converjan diversas expresiones estudiantiles de la educación, en procura de construir insumos en la discusión nacional de la paz", señala Luis Fernando Vega, estudiante de la Universidad El Rosario, y que hace parte del equipo de estudiantes que organiza el evento.

En este sentido, los y las estudiantes analizarán la educación y la paz en 3 campos: La comprensión de los diálogos de paz en el marco histórico, que contará con la participación de los miembros de la Comisión Histórica del Conflicto y sus Víctimas; El análisis de las medidas que busca implementar el gobierno nacional en materia educativa, mediante el análisis del Plan Nacional de Desarrollo 2014-2018; y ubicar el papel de los y las estudiantes en la construcción de la paz, mediante la articulación de iniciativas y propuestas a nivel local y nacional.

El encuentro iniciará con la acreditación, el sábado 9 de mayo de 7:00 a 10:00 de la mañana, y culminará el domingo con un concierto en la Plaza Lourdes de la Cr 63 con Cll 13 a las 3 de la tarde. Las personas interesadas en asistir podrán inscribirse al correo electrónico encuentroestudiantespaz@gmail.com y conocer toda la programación.
