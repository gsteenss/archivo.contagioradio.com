Title: ESMAD se puso al servicio de empresa minera en el Sur de Bolívar: AHERAMIGUA
Date: 2020-06-25 21:28
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Bolívar, desalojo de comunidades, ESMAD, Megamineria
Slug: esmad-se-puso-al-servicio-de-empresa-minera-en-el-sur-de-bolivar-aheramigua
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/ESMAD-Caquetá.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: ESMAD/ Imagen de Referencia

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Desde el pasado 23 de junio, comunidades rurales de Mina Walter, en Montecristo, Bolívar denuncian ataques por parte del Escuadrón Móvil Antidisturbios (ESMAD) contra los mineros artesanales y sus familias que durante generaciones han trabajado y habitado en el sector y que han visto su permanencia amenazada desde la llegada de la Cooperativa Coopcaribona que busca desalojar a los pobladores para el desarrollo de sus actividades mineras.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Mina Walter es una comunidad compuesta por una colectividad de al menos 2.000 personas** en 387 núcleos familiares, sujetos de especial protección constitucional al ser en su mayoría víctimas del conflicto armado, con niños y niñas, aldultos mayores, madres cabeza de familia, personas en situación de discapacidad que dependen en su totalidad del oficio de la explotación artesanal de oro.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Pese a ello, desde que la **Cooperativa Multiactiva Minera Del Caribona (Coopcaribona)** logró el título de explotación minera de todo ese territorio en 2016, "la comunidad se siente vulnerada y la cooperativa ha utilizado tanto vías legales como ilegales para desplazar a la comunidad que desarrolla sus actividades tradicionales" explica abogado Camilo Villamil quien ha acompañado a la comunidad.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Llegada del ESMAD a Montecristo

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Según videos difundidos en redes sociales, desde el martes 23 hubo sobrevuelos de aeronaves y se registró la llegada del ESMAD quienes se alojaron en la sede de **Coopcaribona** "por lo que parecía que se tratara de un escuadrón de mercenarios al servicio de la cooperativa".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Aunque desde la **[Asociación de Hermandades Agroecológicas y Mineras de Guamocó- Aheramigua](https://twitter.com/AHERAMIGUA), la Asociación de Mineros de Mina Walter- Asomiwa y el consejo comunitario del Alto Caribona** se ha emprendido siempre la vía del diálogo por lo que junto a organizaciones defensoras de DD.HH. formaron una comitiva para mediar con la Fuerza Pública. Sin embargo la respuesta a los problemas manifestados fue la agresión por parte del ESMAD.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AHERAMIGUA/status/1275931341029347330","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AHERAMIGUA/status/1275931341029347330

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Villamil asegura que con el paso del tiempo **Coopcaribona** ha intentado hacer montajes en contra de líderes sociales que han defendido el territorio y quienes han sufrido persecuciones judiciales, **"han entrado y salido de la cárcel por montajes judiciales, se les ha amenazado a través de grupos armados",** por lo que le resulta peculiar la llegada de la Fuerza disponible del ESMAD al corregimiento.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**"Es particular que la Fuerza Pública llegue y ese hospede en las instalaciones de una organización económica como Coopcaribona, que lo que ha hecho es hacerle daño a la comunidad"** además asegura que tras comunicarse con la Gobernación del departamento y la Acaldía, se concluyó que no se tenía conocimiento de ningún tipo de operativo del ESMAD en la zona..

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La comunidad de Mina Walter ha desarrollado una lucha por vías jurídicas y a través de la movilización social, en defensa de su territorio, logrando que **desde el 8 de agosto de 2019, la Sección Quinta de la Sala de lo Contencioso Administrativo del Consejo de Estado amparara los derechos fundamentales de las comunidades,** ordenando que se realizara una consulta previa junto a los habitantes con relación a la adjudicación del título minero además de detener toda actividad que vaya en contra de la comunidad, sin embargo a un año de la decisión, no se ha acatado dicha orden.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AHERAMIGUA/status/1275893347769634818","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AHERAMIGUA/status/1275893347769634818

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph {"align":"justify"} -->

Villamil denuncia que en medio de los hechos también fue agredida una defensora de DD.HH. que hace parte de de AHERAMIGUA, sobreviviente de la Unión Patriótica quien además cuenta con medidas de protección por parte de la Unidad Nacional de Protección, "la lideresa fue retenida por el ESMAD y a sus escoltas les quitaron los chalecos y armas de dotación, los retuvieron durante algunass horas señalándola a ella de hacer parte de la insurgencia. [(Lea también: AHERAMIGUA denuncia el asesinato de Víctor Manuel Trujillo, líder del Bajo Cauca)](https://archivo.contagioradio.com/aheramigua-denuncia-el-asesinato-de-victor-manuel-trujillo-lider-del-bajo-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

"En todos los videos que hemos publicado se ve que el ESMAD no solo apunta a las comunidades sino a las casas, lo que quieren es desocupar ese territorio," afirma el abogado. Cabe resaltar que estas acciones de desalojo no contemplan ningún plan, ni medida de atención social para las familias que han vivido de la minería artesanal durante años.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
