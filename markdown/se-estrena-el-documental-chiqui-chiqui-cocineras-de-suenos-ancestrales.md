Title: Se estrena el documental “Chiqui chiqui, cocineras de sueños ancestrales”
Date: 2015-03-13 21:50
Author: CtgAdm
Category: Mujer, Nacional
Tags: Cocineras tejiendo sueños, Guajira, Tabaco, Wayuu
Slug: se-estrena-el-documental-chiqui-chiqui-cocineras-de-suenos-ancestrales
Status: published

###### Foto: ojoalsancocho.org 

Chiqui chiqui, es una historia que narra las resistencias de la comunidad de Tabaco en el sur de la Guajira a través de la comida, la expresión de su cultura y la intensa lucha por mantener su soberanía alimentaria ante la amenaza de empresas extractivistas.

Con la intención de mostrar la fuerte violencia que deja marcada la extracción minera y sus consecuencias de derroche y pérdidas de semillas de la zona, la obra audiovisual se desenvuelve alrededor de la receta autóctona de maíz morado o cariaco.

El colectivo "Cocineras de sueños ancestrales", da estreno al documental luego de presentarse el premio Nacional a las Cocinas Tradicionales Colombianas, del Ministerio de Cultura, ocasión en la que es oportuno mostrar la situación social, cultural y ambiental que se vive en el sur de la Guajira, adoptando de manera creativa métodos que dan a conocer al público la riqueza y valor de la tierra y sus formas de vida desde la ancestralidad.

Mujeres como Inés y Norys, relatarán a lo largo de la historia, la lucha que han forjado desde que fueron desplazadas de sus territorios junto con sus familiaspor parte de transnacional de Cerrejón en la Guajira, trabajando con dignidad y manifestando la unión de las mujeres que, además de ser blanco de rechazos por modelos patriarcales, también desde su género juegan un rol transformador.

Su estreno se llevará a cabo en Bogotá el 13 de marzo en la sede de la Academia Colombiana de Historia, Calle 10 \#8-95, a partir de las 5:00 pm.
