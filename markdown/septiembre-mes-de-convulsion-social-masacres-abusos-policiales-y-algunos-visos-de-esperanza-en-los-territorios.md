Title: Septiembre: Mes de convulsión social, masacres, abusos policiales y algunos visos de esperanza en los territorios
Date: 2020-12-31 11:12
Author: CtgAdm
Category: Actualidad, Especiales Contagio Radio, Resumen del año
Tags: Abuso de fuerza ESMAD, Abuso policial, abusos Fuerza Pública, comunidades, Javier Ordóñez, Masacres, Presidente Ivan Duque, protesta, Protesta social, Protestas contra abuso policial, Regreso de las masacres, Salvatore Mancuso, tutela
Slug: septiembre-mes-de-convulsion-social-masacres-abusos-policiales-y-algunos-visos-de-esperanza-en-los-territorios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-1-4.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas-5.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Luego del asesinato de Javier Ordóñez en Bogotá, las protestas ciudadanas en todo el país no se hicieron esperar. Cerca de 45 puestos de policía en los barrios recibieron la rabia de la ciudadanía que ya había denunciado más de 1000 hechos de abuso policial. Lastimosamente la respuesta de los organismos de seguridad fue peor que la enfermedad, durante las noches del 8 y 9 de septiembre se reportaron 13 asesinatos de manifestantes a manos de la policía y más de 123 personas heridas, hechos que siguen en la impunidad pues hasta el momento solo se ha conocido la condena de la procuraduría en contra de los oficiales de policía que asesinaron a Javier Ordóñez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
En este mes la organización Transparencia Internacional hizo una denuncia que preocupa para este 2021 y que tiene que ver con la concentración del poder en el ejecutivo, pues instancias como la Fiscalía, la Procuraduría, la Defensoría del Pueblo y varias magistraturas, sobre todo de la Corte Constitucional, están quedando en manos de personas muy cercanas, incluso amigas del gobierno y del uribismo. Procesos como los que se siguen por los delitos de las FFMM y la Policía están en riesgo y podrían incluso precluir en los primeros meses del 2021.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Sin embargo, si la justicia no avanza por las vías institucionales las víctimas y las organizaciones sociales si están dando pasos firmes. En setiembre se conoció de un primer encuentro entre víctimas y comunidades con Salvatore Mancuso cuy resultado fue conocer una parte de la verdad sobre el asesinato de Kimy Pernía Domicó. Para las comunidades en los territorios este paso de encuentro en la verdad es muy importante pues abre las puertas a una verdadera reparación integral.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

  
Adicionalmente las comunidades, luego de una serie de 9 cartas lograron llevar a los tribunales, a través de una tutela, su derecho a la paz. Cerca de 200 comunidades de país siguieron insistiendo en la necesidad de un Acuerdo Humanitario Global que les permitiera enfrentar la pandemia sin el riesgo de las balas en sus territorios. Sin embargo el control paramilitar y la inacción de las FFMM siguen degradando su derecho a vivir en paz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### 127 heridos y 13 asesinadas en medio de represión a protestas contra abusos policiales

<!-- /wp:heading -->

<!-- wp:paragraph -->

Localidades como Suba, Ciudad Bolívar, Bosa y Kennedy reportan hasta el momento 127 personas heridas y 13 personas asesinadas producto del accionar de la Fuerza Pública. Los hechos se registraron durante las protestas en rechazo al asesinato de [Javier Ordóñez, producto del abuso de violencia por parte de dos agentes de la Policía.](https://archivo.contagioradio.com/el-abogado-javier-ordonez-fue-asesinado-por-la-policia-afirman-sus-familiares/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Mas de esta nota: [127 heridos y 13 asesinadas en medio de represión a protestas contra abusos policiales](https://archivo.contagioradio.com/mas-de-12-personas-heridas-y-dos-personas-asesinadas-en-medio-de-protestas-contra-accionar-de-fuerza-publica/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Transparencia Internacional alerta sobre concentración del poder en gobierno Duque

<!-- /wp:heading -->

<!-- wp:paragraph -->

La organización Transparencia Internacional y su capítulo en Colombia alertó sobre una concentración del poder de la rama ejecutiva en el país. Cabe señalar que a lo largo del 2020 han sido elegidas personas cercanas al Gobierno de Iván Duque para encabezar diferentes organismos de control como la Fiscalía, la Procuraduría y la Defensoría del Pueblo, lo que podría poner en riesgo la independencia que deben tener dichos órganos externos de control frente al poder ejecutivo. [(Le puede interesar: En dos años del Gobierno Duque se ha derrumbado institucionalidad)](https://archivo.contagioradio.com/en-dos-anos-del-gobierno-duque-se-ha-derrumbado-institucionalidad/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La organización, advierte sobre la capacidad que adquirió el Gobierno en medio de la emergencia de la pandemia por Covid-19 y que le dio facultades como legislador transitorio. Tan solo en el primer estado de emergencia para el mes de mayo, Duque decreto 72 actos legislativos, de los que al menos 6 fueron declarados inconstitucionales.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Transparencia Internacional alerta sobre concentración del poder en gobierno Duque](https://archivo.contagioradio.com/transparencia-internacional-alerta-sobre-concentracion-del-poder-en-gobierno-duque/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Así fue encuentro de diálogo entre víctimas y Salvatore Mancuso

<!-- /wp:heading -->

<!-- wp:paragraph -->

Un grupo de víctimas de crímenes de lesa humanidad y crímenes de guerra hombres y mujeres, afrocolombianos, indígenas, mestizos, habitantes rurales del Urabá antioqueño, Urabá córdobés, Norte del Chocó y Bajo Atrato en la ciudad de Apartadó, sostuvieron un diálogo virtual extrajudicial con varios solicitantes ante el Sistema Integral Transicional con responsables del sector empresarial y excombatientes responsables de su desplazamiento y despojo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Así fue encuentro de diálogo entre víctimas y Salvatore Mancuso](https://archivo.contagioradio.com/asi-fue-encuentro-de-dialogo-entre-victimas-y-salvatore-mancuso/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Juez podría dictar cárcel contra presidente Duque por desacato a tutela

<!-- /wp:heading -->

<!-- wp:paragraph -->

El Tribunal Administrativo de Cundinamarca, Sección Segunda, Subsección “F” tiene en sus manos un incidente de desacato a la acción de tutela presentada por un grupo de más de 100 comunidades negras, mestizas e indígenas en el país, **que exigen acuerdos humanitarios regionales y un cese al fuego para aliviar el conflicto en sus territorios.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La** tutela, que fue fallada a favor de las comunidades el pasado 20 de Agosto **ordena que se de respuesta a la solicitud de las comunidades en torno a la orden de un Cese al Fuego. [(Lea también: Tribunal ordena a Duque responder a comunidades que exigen cese al fuego en sus territorios)](https://archivo.contagioradio.com/tribunal-ordena-a-duque-responder-a-comunidades-que-exigen-cese-al-fuego-en-sus-territorios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Juez podría dictar cárcel contra presidente Duque por desacato a tutela](https://archivo.contagioradio.com/juez-dictar-carcel-contra-presidente-duque-desacato-tutela/)

<!-- /wp:paragraph -->
