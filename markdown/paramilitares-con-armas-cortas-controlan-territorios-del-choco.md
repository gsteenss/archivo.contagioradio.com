Title: En Bajo Atrato paramilitares reclutan jóvenes para control social
Date: 2017-04-26 13:06
Category: DDHH, Nacional
Tags: cacarica, Chocó, paramilitares, Truandó
Slug: paramilitares-con-armas-cortas-controlan-territorios-del-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo  
] 

###### [26 Abr. 2017] 

Comunidades de Cacarica en el Chocó denunciaron que la **presencia de paramilitares, autodenominados como Autodefensas Gaitanistas de Colombia (AGC) continua**. El hecho más reciente se presentó en Truandó, sobre el río Atrato, donde fueron hostigados por 3 hombres, 2 de ellos con armas cortas, en el marco de una reunión que sostenía la comunidad. Así mismo, se conoció de la creación de una oficina de las AGC desde donde se emiten ordenes para asesinar a pobladores.

El hecho más reciente se presentó este 19 de Abril cuando **3 hombres, dos de ellos armados, hostigaron, limitaron la movilidad de las personas** e impidieron su derecho a la libertad de expresión sobre temas territoriales en una reunión que sostenían.

Los hechos se conocieron a través de **una denuncia entregada por la Comisión de Justicia y Paz**, en la que agregan que los hombres permanecieron en dicha reunión "como informantes", sin respetar el espacio de encuentro entre los pobladores. Le puede interesar: [Enfrentamientos entre paramilitares y ELN desplazan 300 familias en Chocó](https://archivo.contagioradio.com/desplazamiento-choco-enfrentamientos/)

Asegura la Comisión de Justicia y Paz que este no es el único lugar donde  hacen presencia los paramilitares, y de los cuales han tomado el control **sin ninguna respuesta por parte de la Fuerza Pública que hace presencia en el departamento**.

"A unos 200 metros del corregimiento La Travesía (Chocó), se encuentra de manera permanente un puesto del batallón fluvial y unos kilómetros aguas abajo en el punto conocido como Tumaradó, jurisdicción de Acandí, cuatro hombres de las AGC vestidos de civil están permanentemente" dice el comunicado de la organización. Le puede interesar: [Persiste la presencia de paramilitares sobre el Río Tamboral en el Chocó](https://archivo.contagioradio.com/persiste-la-presencia-de-estructuras-neoparamilitares-sobre-el-rio-tamboral-en-el-choco/)

Según la Comisión de Justicia y Paz, **los paramilitares tienen presencia desde 2006, año en el que realizan "operaciones de control** del transporte de personas de Cacarica, extorsivas, y las propias del tráfico de drogas, así como las que aseguran movilidad de las unidades de la estructurs armada por el río Atrato y el Parque Nacional de los Katíos".

Adicionalmente cuenta la organización que tres defensores de derechos humanos que hacen parte de la Comisión de Justicia y Paz , este 21 de abril en el marco de sus labores, se detuvieron en el sitio conocido como Tumaradó, jurisdicción de Acandí, momento en el cual llamaron la atención de los AGC entre los que se encontraba alias "Damián" quienes vigilaron sus movimientos.

### **Los paramilitares tienen oficina de despacho** 

Este 21 de Abril se conoció de la existencia de una oficina de despacho de las autodenominadas AGC, ubicada a las afueras del casco urbano del municipio de Ríosucio, así lo aseguró la Comisión de Justicia y Paz quien añade **desde allí que "como es de público conocimiento, indígenas, afrocolombianos y afro mestizos, se ven obligados a ir para proteger la vida** de personas que aparecen en listas de personas que definen los paramilitares serían asesinadas".

Además de esta oficina, en el corregimiento de Bijao, los paramilitares decidieron ubicar otro **lugar para "aclarar el listado de personas a asesinar si no salen de la región",** lo que ha obligado a los campesinos de Cacarica a ir "para evitar el asesinato de amenazados por las AGC".

Muy cerca de esa comunidad del territorio colectivo hacen presencia unidades militares regulares desde el 24 de febrero, sin que se haya tomado acciones al respecto, que garanticen la vida, integridad y permanencia de las comunidades en sus tierras. Le puede interesar: [Comunidades indígenas de Jiguamiando, Chocó asediadas por paramilitares](https://archivo.contagioradio.com/pparamilitares_jiguamiando_choco/)

Sumado a las "oficinas de control social", dice la denuncia de la organización que entre el 21 y el 26 de Abril, en el municipio de Turbo, más exactamente en los barrios La Playa, Gaitán y Obrero **los paramilitares han "continuado con el reclutamiento de integrantes de pandillas y bandas.** Haciendo que los jóvenes se vean obligados a someterse a sus ordenes o serían asesinados. Los pagos van desde 100 mil pesos hasta un máximo de 300 mil. Algunos de los jóvenes vinculados se encuentran operando en las áreas rurales de Cacarica".

Por último, la Comisión reitera su preocupación ante la ausencia de respuesta en materia de protección y derechos sociales de parte del Estado dado el control social que están ejerciendo las AGC en Chocó.
