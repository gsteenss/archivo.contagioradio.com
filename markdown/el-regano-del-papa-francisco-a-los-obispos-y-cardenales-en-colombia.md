Title: Animador regaño del Papa Francisco a obispos y cardenales
Date: 2017-09-07 17:56
Category: Nacional, Paz
Tags: Cardenales, iglesia católica, Obispos, Papa Francisco
Slug: el-regano-del-papa-francisco-a-los-obispos-y-cardenales-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/papafrancisco.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: el impulso] 

###### [07 Sept 2017]

En uno de los más largos mensajes del Papa Francisco y durante su corta permanencia en el palacio Arzobispal en Bogotá, se dio un fuerte llamado de atención a la acción de los obispos y cardenales en Colombia. La humildad, la sencillez y la frase “no son políticos, son pastores” son algunas de las palabras que se resaltaron en la intervención del Sumo Pontífice.

Un primer llamado fue a no temer a empobrecerse “en la conciencia de ser ustedes sacramento viviente de esa libertad divina que no tiene miedo de salir de sí misma por amor, que no teme empobrecerse mientras se entrega”

### **Romper con los poderosos de turno y no plegarse a la dictadura presente** 

También hizo un llamado a no confiar la misión de la iglesia a los poderosos de turno, en una alusión a las alianzas que en algunos sectores de la iglesia se hacen con gobernadores, alcaldes o escenarios que no favorecen a los más desprotegidos.

“Dios nos precede, somos sarmientos, no somos la vid. Por tanto, no enmudezcan la voz de Aquel que los ha llamado ni se ilusionen en que sea la suma de sus pobres virtudes, la de ustedes, o los halagos de los poderosos de turno quienes aseguran el resultado de la misión que les ha confiado Dios, acercarse a Jesús dejando atrás «lo que fuimos, para que seamos lo que no éramos»”

"¿Qué otro futuro podemos perseguir? ¿A qué otra dignidad podemos aspirar? No se midan con el metro de aquellos que quisieran que fueran solo una casta de funcionarios plegados a la dictadura del presente."

**¿Una división en la Iglesia por las agendas encubiertas?**

En una clara alusión a la situación de la iglesia en la que pugnan por el poder eclesial una postura conservadora y seguidora del legado de Benedicto XVI y la postura del actual pontificado Francisco interrogó y pidió por favor “busquen con perseverancia la comunión entre ustedes. No se cansen de construirla a través del diálogo franco y fraterno, condenando como peste las agendas encubiertas, por favor.”

También hizo un llamado a acabar con los monopolios que muchas veces toman decisiones trascendentales al interior de la iglesia colombiana “Construyan una Iglesia que ofrezca a este País un testimonio elocuente de cuánto se puede progresar cuando se está dispuesto a no quedarse en las manos de unos pocos.”

### **La paz y el desafío de la reconciliación** 

En torno al momento histórico que atraviesa Colombia, el papa instó a potenciar la reconciliación y a dejar de lado las alianzas que no sirven al evangelio “La palabra de la reconciliación.  Muchos pueden contribuir al desafío de esta Nación, pero la misión de ustedes es singular. Ustedes no son técnicos ni políticos, son pastores (…) No sirven alianzas con una parte u otra, sino la libertad de hablar a los corazones de todos.”

### **Un llamado a respetar a afrodescendientes e indígenas y los derechos de la naturaleza** 

En alusión a las raíces afrodescendientes afirmó que es necesario recuperarlas y valorarlas “Reserven una particular sensibilidad hacia las raíces afro-colombianas de su gente, que tan generosamente han contribuido a plasmar el rostro de esta tierra.”

También instó a defender los derechos de la naturaleza, haciendo un llamado a aprender de las comunidades indígenas de la amazonía “me pregunto si somos aún capaces de aprender de ellos la sacralidad de la vida, el respeto por la naturaleza, la conciencia de que no solamente la razón instrumental es suficiente”

Además llamó a que la iglesia de la amazonía sea también el amigo “el otro brazo” de la iglesia a nivel nacional “Colombia no la puede amputar sin ser mutilada en su rostro y en su alma.”

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
