Title: "Don Berna" vincula con las AUC a Del Río, Plazas Acevedo, Santoyo, Narváez y Hernández
Date: 2015-09-07 16:00
Category: DDHH, Judicial, Nacional
Tags: Comisión Colombia de Juristas, das, Don Berna, Eduardo Umaña, General Plazas Acevedo, Jaime Garzon, Jesús María Valle, José Miguel Narváez, Julio Cesar Santoyo, Operaciones criminales del DAS, rito alejo del rio, Santiago Escobar
Slug: don-berna-vincula-a-3-militares-con-crimenes-y-funcionamiento-de-las-auc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/diego_fernando_murillo_alias_don_berna_foto_archivo_efe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Bluradio 

###### [Sep 7 2015] 

Desde la cárcel en  Miami donde se encuentra recluido, "Don Berna" declaró por el crimen del humorista Jaime Garzón, del defensor de derechos humanos Eduardo Umaña, el crimen de Jesús María valle, abogado defensor de Derechos Humanos, y Francisco Villalba testigo de la masacre del Aro.

En su denuncia "Don Berna" evidenció que estos asesinatos tenían un modus operandi en donde **las ordenes venían desde las altas esferas, luego estas contactaban con las Autodefensas quienes planeaban el crimen y miembros de la banda “La Terraza” ejecutaban estas órdenes**.

El Coronel Rito Alejo del Río, el General Plazas, el General Hernandez , José Miguel Narváez del DAS y el del senador Álvaro Uribe Vélez fueron algunos de los nombres que dio el exjefe paramilitar, quien además indicó que cada uno de estos crímenes fueron de lesa humanidad.

Respecto del **General Santoyo, "Don Berna" declaró que era una de las personas determinantes en varios de los crímenes y que Rito Alejo del Río era prácticamente una parte orgánica de las AUC.** En su declaración "Don Berna" pidió perdón a la familia de Jaime Garzón por su asesinato, también indicando que **este fue un crimen de lesa humanidad**.

Sobre lo que puedan significar estas declaraciones en el avance de las investigaciones el abogado integrante de la **Comisión Colombia de Juristas, Santiago Escobar**, indicó que en el caso de Narváez podría usarse como una prueba superviviente, en vista de que ya se está acabando el proceso contra él, sin embargo, no es seguro que las acepten.

En cuanto al caso de Plazas Acevedo indicó que la juez quitó la potestad para que las víctimas tuvieran participación, impidiendo que sean presentadas estas pruebas.
