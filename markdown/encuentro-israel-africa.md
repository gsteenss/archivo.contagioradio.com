Title: Cancelado encuentro entre países africanos e Israel debido a presiones pro palestinas
Date: 2017-09-14 16:48
Category: Onda Palestina
Tags: Apartheid, BDS, Israel, Palestina
Slug: encuentro-israel-africa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/encuentro.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: www.jpost.com 

###### 12 Sep 2017 

El gobierno israelí decidió cancelar el encuentro Israel/África programado para el siguiente mes en el país de Togo. Esto se dió luego que varios países africanos decidieran boicotear el evento; según el Jerusalem Post varios países árabes solicitaron la cancelación, aunque el ministro de relaciones exteriores de Israel afirmó que el evento solo ha sido postpuesto sin hablar de una fecha alternativa.

Según las palabras dadas por el embajador de Sudáfrica en el Líbano, al diario The Citizen, este país no participaría en este encuentro porque “lo ve como un paso a la normalización de las relaciones entre los Estados de África y un Estado que mantiene una ocupación militar”. Asimismo, la ministra de relaciones exteriores de Sudáfrica afirmó que: “No podemos hacer como si no viéramos que los esfuerzos de Israel para obtener apoyo en África, así como el resto del mundo, son una forma de atacar a la causa palestina”

En esta emisión de Onda Palestina estaremos en sintonía con las campañas de Boicot, desinversiones y sanciones a Israel desde Canadá y Sudáfrica, donde recientemente han tenido gran éxito y resonancia internacional; además de la inauguración oficial del Museo de Palestina con su primera exhibición: ¡Jerusalén vive! . Trataremos temas asociados a la visita de Netanyahu de la mano de dos panelistas los cuales estuvieron presentes en el encuentro académico del día de ayer sobre el TLC Colombia-Israel, y finalmente realizaremos una discusión alrededor de las temáticas que fueron discutidas en ese panel.

<iframe id="audio_20885738" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20885738_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Escuche esta y más información sobre la resistencia del pueblo palestino en [Onda Palestina](https://archivo.contagioradio.com/programas/onda-palestina/), todos los Martes de 7 a 8 de la noche en Contagio Radio. 
