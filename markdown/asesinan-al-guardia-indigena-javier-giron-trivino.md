Title: Javier Girón Triviño, indígena Nasa asesinado en Santander de Quilichao
Date: 2020-02-03 09:56
Author: CtgAdm
Category: Nacional
Tags: Asesinatos líderes, ONIC
Slug: asesinan-al-guardia-indigena-javier-giron-trivino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Javier-Girón-Triviño.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","fontSize":"small"} -->

Foto: Eduin Mauricio Capaz

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A través de su cuenta de Twitter la **Organización Nacional Indígena de Colombia (ONIC),** denunció el asesinato de **Javier Girón Triviño**,  integrante del Pueblo Nasa y guardia indígena,  el hecho sucedió en la vereda El Jagüito en zona rural de Santander de Quilichao, Cauca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El comunero de 54 años fue asesinado cuando se encontraba en un local de comercio público hacia las 6 a.m., momento en el que un hombre entró al sitio y disparó en tres ocasiones contra la humanidad de Javier Girón.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Él hacia parte de la comunidad indígena del resguardo Nasa Kiwe Tech Ksxa, uno de los 22 territorios indígenas agrupados en la Asociación de Cabildos Indígenas de Norte de Cauca (ACIN). Desde hace más de 20 años hacia parte de la Guardia Indígena 'Kiwe Thegnas'. No se habían denunciado amenazas contra el guardia indígena.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver: [Colombia inicia el año con aumento de violencia contra líderes sociales](https://archivo.contagioradio.com/colombia-inicia-el-ano-con-aumento-de-violencia-contra-lideres-sociales/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

ONU- Derechos Humanos Colombia, exigió a través de su cuenta de Twitter una pronta "investigación, juzgamiento y sanción del homicidio del guardia indígena Javier Girón"

<!-- /wp:paragraph -->

<!-- wp:html -->

> ?[\#Hilo](https://twitter.com/hashtag/Hilo?src=hash&ref_src=twsrc%5Etfw) Rechazamos el homicidio del integrante del Pueblo Nasa y guardia indígena Javier GIrón Triviño. Estamos dando seguimiento a este crimen de acuerdo con nuestro mandato [@Albrunori](https://twitter.com/Albrunori?ref_src=twsrc%5Etfw) [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw) [@CRIC\_Cauca](https://twitter.com/CRIC_Cauca?ref_src=twsrc%5Etfw) [pic.twitter.com/zAg8LINlzY](https://t.co/zAg8LINlzY)
>
> — ONU Derechos Humanos Colombia (@ONUHumanRights) [February 3, 2020](https://twitter.com/ONUHumanRights/status/1224128491546914818?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
  
<!-- /wp:html -->

</p>
<!-- wp:paragraph -->

<!-- /wp:paragraph -->
