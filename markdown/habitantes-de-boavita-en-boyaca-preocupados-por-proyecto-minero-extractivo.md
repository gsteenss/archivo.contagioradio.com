Title: Termoeléctrica de carbón afectarían 5 ecosistemas de Boyacá
Date: 2017-07-12 13:32
Category: Ambiente, Voces de la Tierra
Tags: Ambiente, Boavita, Boyacá, carbón, Mineria
Slug: habitantes-de-boavita-en-boyaca-preocupados-por-proyecto-minero-extractivo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/12-nevado-del-cocuy-e1499884308128.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía de Boyacá] 

###### [12 Jul 2017] 

Los habitantes de Boavita en Boyacá, han manifestado su preocupación ante la posible construcción de una termoeléctrica de carbón, sacando un promedio de  **20 mil toneladas del mineral al mes **que serían quemadas en las centrales, aumentando la deforestación contaminado el aire, el agua y el suelo de la Sierra Nevada del Cocuy, el páramo de Güina y el páramo de Carmen.

Según un habitante de Boavita, “este proyecto, que es minero extractivo, se basa en que **las empresas vienen a sacar el carbón de las montañas para utilizarlo en las termoeléctricas**”. Indicó que la empresa que construiría esta termoeléctrica es Innercol quienes trabajan en la industria de generación de energía en el país. Corzo manifestó que "esta empresa tiene un capital enorme por lo que puede haber una empresa multinacional detrás queriendo entrar al municipio".  (Le puede interesar: ["Sibaté prepara consulta popular contra la minería"](https://archivo.contagioradio.com/sibate_consulta_popular_mineria/))

La gran preocupación es que la Sierra Nevada del Cocuy se encuentra a 30 kilómetros de distancia del lugar donde estarían las centrales termoeléctricas. Esta sierra es la segunda más grande del país y tiene las mayores alturas de la cordillera oriental. Con el proyecto minero, **las deforestaciones en las inmediaciones de la sierra aumentarían** pues los habitantes han afirmado que “para poder sostener los socavones de carbón mineros necesitan poner madera”.

### **Corpoboyacá estaría estudiando la posibilidad de otorgar licencias ambientales** 

Los Boavitas manifestaron que al tratarse de un proyecto en donde “**las termoeléctricas tienen la capacidad de producir 90 mega vatios de energía** y no más, el encargado de otorgar las licencias es Corpoboyacá”. Esta corporación, en audiencia público ambiental, está estudiando la posibilidad de otorgar 2 licencias que han sido solicitadas para la realización de este proyecto energético. (Le puede interesar: ["595 personas se gradúan de diplomado que busca fortalecer la lucha contra la minería"](https://archivo.contagioradio.com/415_personas_graduan_diplomado_tolima/))

Además, “**a los habitantes de Boavita no nos han mostrado el nivel de las afectaciones ambientales para los páramos y los nevados**”. Igualmente, ellos y ellas tampoco saben cuál será el impacto que tendrán estas actividades en las comunidades cuando queden materiales como plomo, mercurio y arsénico principalmente en las fuentes hídricas.

<iframe id="audio_19768594" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19768594_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
