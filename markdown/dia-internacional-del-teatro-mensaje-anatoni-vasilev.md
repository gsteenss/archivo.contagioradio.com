Title: "El teatro puede decirnos todo" Anatoli Vasíliev en el  Día Internacional del teatro
Date: 2016-03-27 09:36
Category: Cultura, Otra Mirada
Tags: teatro
Slug: dia-internacional-del-teatro-mensaje-anatoni-vasilev
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/4478936_orig.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: revistaelsotano 

###### 27 Mar 2016 

Teatro, palabra que en griego significa "lugar del contemplar" celebra un año más de actuación, de cultura, de vida. Un día como hoy del  año 1961 es creado El día mundial del Teatro por el instituto Internacional del Teatro (ITI), con el objetivo de mantener viva  la actividad escénica en el desarrollo de la cultura.

Durante 55 años  directores, directoras de [teatro](https://archivo.contagioradio.com/?s=teatro) y dramaturgos  han enviado un mensaje para celebrar esta fecha, Anatoli Vasíliev, director ruso y uno de los más representativos protagonistas de la escena teatral europea expresa:

#### [¿Necesitamos teatro?] 

Esa es la pregunta que surge en miles de profesionales del teatro decepcionados y en millones de personas cansadas de él.

#### [¿Qué necesitamos de él?] 

Hoy en día la escena es tan insignificante, en comparación con las ciudades y estados donde se juegan auténticas tragedias de la vida real.

#### [¿Qué es él para nosotros?] 

Galerías y balcones, bañados de oro y plata; en las salas, sillones de terciopelo, actores de voces bien pulidas o viceversa, algo que puede lucir aparentemente diferente: cajas negras, manchadas de barro y sangre, con un montón de cuerpos desnudos rabiosos al interior.

#### [¿Qué está dispuesto a decirnos?] 

¡Todo!

#### [El teatro puede decirnos todo.] 

Cómo los dioses habitan en el cielo, y cómo los presos languidecen en cuevas subterráneas, olvidadas, y cómo la pasión nos puede elevar, y cómo el amor puede destruir, y cómo nadie necesita una buena persona en este mundo, y cómo reina la decepción, y cómo la gente vive en apartamentos, mientras que los niños se marchitan en campos de refugiados, y cómo todos tienen que volver de nuevo al desierto, y cómo día tras día nos vemos obligados a apartarnos de nuestros seres amados, el teatro puede decirnos todo.

#### [El teatro siempre ha estado y siempre permanecerá.] 

Y ahora, en estos últimos cincuenta o setenta años, es particularmente necesario. Porque si usted ve todas las artes públicas, puede observar de inmediato que sólo el teatro nos da una palabra de boca en boca, una mirada de ojo a ojo, un gesto de mano en mano y de cuerpo a cuerpo. No necesita ningún intermediario para trabajar entre seres humanos, constituye el lado más transparente de la luz, no pertenece más al sur, o al norte o al este u oeste, oh no, es la esencia de su propia luz, brillando desde todos los rincones del mundo, inmediatamente reconocible por cualquier persona, ya sea hostil o amigable hacia él.

#### [**Y necesitamos teatro que permanece siempre diferente, necesitamos teatro de muchas formas diferentes.**] 

Aún así, creo que entre todas las formas de teatro posibles, sus formas arcaicas demostrarán ahora ser las de mayor demanda. El teatro de formas rituales no debe oponerse artificialmente al de naciones "civilizadas". La cultura secular está siendo cada vez más castrada, la llamada "información cultural" sustituye gradualmente y suplanta entidades simples, así como nuestra esperanza de cumplirles un día.

Pero puedo ver claramente ahora: el teatro está abriendo sus puertas de par en par. Entrada gratuita para todos y cada uno.

Al diablo con aparatos y dispositivos, ¡ir al teatro!, ¡ocupar filas de butacas en las galerías, escuchar la palabra y mirar imágenes en vivo! El teatro está frente a usted, no lo descuide y no se pierda la oportunidad de participar en él, tal vez la más preciosa oportunidad que tenemos en nuestras vanas y apresuradas vidas.

#### [Necesitamos cada forma de teatro.] 

Sólo hay un teatro que seguramente no es necesario para nadie, me refiero al teatro de juegos políticos, un teatro de políticas "ratoneras", un teatro de políticos, un inútil teatro de políticos. Lo que sin duda no necesitamos es un teatro de terror cotidiano, ya sea en lo individual o colectivo, lo que no necesitamos es la escena de cadáveres y sangre en las calles y plazas de las capitales o provincias, un teatro falso sobre los enfrentamientos entre religiones o grupos étnicos...

**Anatoli Aleksándrovich Vasíliev - traducido por César Muñoz **
