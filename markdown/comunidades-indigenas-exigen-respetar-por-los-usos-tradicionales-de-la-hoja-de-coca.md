Title: Impedir uso y comercialización de productos de hoja de Coca es un "Apartheid"
Date: 2019-02-07 17:33
Author: AdminContagio
Category: Comunidad, Nacional
Tags: agresión a indígenas, Productos de Hoja de coca
Slug: comunidades-indigenas-exigen-respetar-por-los-usos-tradicionales-de-la-hoja-de-coca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/30391220415_b03f0f23c3_k-e1550619349489-770x400-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Coca Nasa 

###### 07 Feb 2019

La Organización Nacional Indígena de Colombia convocó a una rueda de prensa para denunciar la persecución que están ejerciendo entidades como la Secretaría de Salud de Bogotá y el INVIMA al incautar y destruir productos a base de coca creados en los resguardos, a pesar de contar con el debido registro sanitario y actuar en contra vía de los derechos de las comunidades indígenas.

Según **David Curtidor, cofundador de la empresa Coca Nasa, una iniciativa productiva proveniente del resguardo Calderas en Tierradentro, Cauca**, el registro sanitario de dichos productos fue expedido en acuerdo con las comunidades indigenas del Cauca y con autorización del Instituto Nacional de Vigilancia de Medicamentos y Alimentos (INVIMA) de los años 2002 y 2005,  posteriormente en 2010 el INVIMA emitió una alerta sanitaria donde decían que dichos registros no servían para toda Colombia.

Dicha alerta sanitaria fue demandada ante el Consejo del Estado el cual, en 2015, falló a favor de Coca Nasa, y estableció que los registros sanitarios son vigentes en toda Colombia, una instrucción que el INVIMA y la Secretaría de Salud han ignorado al recalcar que no está permitido comerciar productos tales como bebidas energizantes, galletas, té, vinos y pomadas en todo el país.

### **Se busca privatizar el uso de la hoja de coca** 

El cofundador de Coca Nasa afirma que "hay un desquiciamiento del estado social de derecho en materia de protección de los derechos de los pueblos indígenas" pues se está intentando privatizar el uso de la hoja de coca con destino a farmacéuticas para la elaboración de medicamentos, razón por la que han presentando acciones legales nuevamente ante el Consejo de Estado y  solicitarán medidas cautelares ante la Comisión Interamericana de Derechos Humanos.

**"Esta es definitivamente la declaración del Apartheid, impedirle a comunidades indígenas que puedan hacer ejercicio de la recreación de su identidad cultural y vida económica en cualquier lugar del territorio"** afirma Curtidor al señalar el daño que se está ocasionando a las comunidades indígenas al atentar contra sus saberes ancestrales y su autonomía.

###### Reciba toda la información de Contagio Radio en [[su correo]
