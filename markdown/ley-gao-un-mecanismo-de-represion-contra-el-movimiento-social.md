Title: Ley GAO un mecanismo de represión contra el Movimiento Social
Date: 2020-09-09 07:29
Author: AdminContagio
Category: Expreso Libertad, Programas
Tags: carceles de colombia, Expreso Libertad, ley gao, Movimiento social
Slug: ley-gao-un-mecanismo-de-represion-contra-el-movimiento-social
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/09/Guerrillas_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto de: El Nuevo Siglo {#foto-de-el-nuevo-siglo .has-text-align-right .has-cyan-bluish-gray-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde la firma del Acuerdo de Paz, una de las leyes que más rechazo generó en el Movimiento Social de Colombia, es la **Ley 1908 o ley de Grupos Armados Organizados (GAO)**. De acuerdo con las denuncias, esta ley no solo violenta los derechos al debido proceso, sino que se convierte en un mecanismo para perseguir y encarcelar líderes del movimiento social.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En este programa del **Expreso Libertad** los abogados Uldarico Florez, Luisa Niño, ambos defensores de derechos humanos, señalaron los peligros de esta normativa jurídica, en donde se encuentra incluso una extensión en el tiempo en cárcel, sin que haya iniciado el proceso judicial de 6 años.

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe id="audio_57015869" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_57015869_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Asimismo, Laura Escobar, familiar de Yeison Franco, quien fue capturado durante las protestas del 21N y que denuncia un montaje judicial, señala que actualmente se encuentra procesado por esta Ley aunque la misma no debería ser aplicada, debido a que estas leyes solo entran en legislación dos años después.

<!-- /wp:paragraph -->

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fwatch%2F%3Fv%3D1386520531551855%26extid%3DGXRbBcGCtfqpb8Ti&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

<!-- wp:heading {"level":5} -->

##### [Vea mas programas del Expreso libertad](https://archivo.contagioradio.com/categoria/programas/expreso-libertad/)

<!-- /wp:heading -->
