Title: Feminicidios son sólo la punta del iceberg: Limpal
Date: 2020-06-26 06:26
Author: CtgAdm
Category: Actualidad, Mujer
Slug: feminicidios-son-solo-la-punta-del-iceberg-limpal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/mujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 24 de mayo conocimos dos nuevos casos de agresión hacia la mujer, uno de ellos por parte del **director de cine Ciro Guerra**, que **registra 8 denuncias de acoso sexual** en su contra y **una por abuso sexual**, información revelada por las investigadoras Catalina Ruiz-Navarro y Matilde de los Milagros Londoño .

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A este hecho se suma la **violación de una menor de 12 años de la comunidad del resguardo indígena Embera Katio** en Pueblo Rico Risaralda**,** **por parte de 7 militares** pertenecientes al batallón de San Mateo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONIC_Colombia/status/1275906273851183104","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONIC\_Colombia/status/1275906273851183104

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por otro lado el **[Observatorio de Violencia Contra la Mujer](http://observatoriofeminicidioscolombia.org/index.php/seguimiento/noticias/428-vivas-nos-queremos-dossier-de-feminicidios-en-cuarentena)** de la Fundación [Feminicidios](https://archivo.contagioradio.com/justicia-restaurativa-puede-contrarrestar-violencia-genero/) Colombia, presentó esta misma semana un alarmante registro de al menos **102 casos de feminicidios en el 2020; 50 solo en el marco del Estado de emergencia**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además agregan que por lo menos **26 mujeres en el periodo del 20 de marzo al 23 de junio de este año**, han sufrido diferentes tipos de agresiones, las cuales se clasifican como **tentativas de feminicidio**, ataques menos visibilizados pero igual de validos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Diana Salcedo, Directora de la Liga Internacional de Mujeres por la Paz y la Libertad ([LIMPAL](https://www.limpalcolombia.org/es/informate/noticias/vida-digna/52-rechazo-a-las-continuas-violencias-que-sufren-las-mujeres-de-manera-sistematica-en-manos-de-sus-parejas)),** señaló que , *"el feminicidio es sólo la punta del iceberg",* agregando que **la gran mayoría de asesinatos de mujeres se dan con arma de fuego**, abriendo así la discusión a otro tipo de debates como es el manejo de las armas.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *[**"Aquí no nos interesa si es una o 100, sino que este Estado y sociedad aprueba y esto hace que**]{}**los feminici******dios sigan en aumento"***
>
> <cite>Diana Salcedo, Directora LIMPAL</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

  
Salcedo señaló también que en los últimos meses ha aumentado la modalidad de asesinato a las mujeres por medio de sicarios, *"están mandando a matar a las mujeres a través de terceros, y **esto tampoco queda registrado, porque lo que vende en muchos medios de comunicación es decir que son crímenes pasionales"**.*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### [Nos están matando, ¿y la culpa es nuestra?]{.has-inline-color .has-very-dark-gray-color} {#nos-están-matando-y-la-culpa-es-nuestra .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Señalando que, *"las mujeres no aparecemos muertas, cómo aparecen las flores en el campo a las mujeres nos están matando"*, la Directora destacó que este tipo de hecho se dan como mecanismos para generar un escarnio público, **utilizando el cuerpo de las mujeres para mostrar poder e intimidar a las comunidades.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que **es irresponsable decir que cualquiera es un posible feminicida, cuándo no hay investigaciones por parte de los organismos de control, ni tampoco denuncias válidas** que permitan entender los procesos legales que respalden estas afirmaciones, y enfatizó que acciones como la cadena perpetua solo evitan acciones reales.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"vivid-cyan-blue"} -->

### ["Nosotras no somos las responsables de transformar las masculinidades"]{.has-inline-color .has-very-dark-gray-color} {#nosotras-no-somos-las-responsables-de-transformar-las-masculinidades .has-vivid-cyan-blue-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Organizaciones feministas como Limpal, trabajamos de la mano de los hombres en distinto proyectos, razón que Salcedo ve como una razón más hacer de este una responsabilidad conjunta, *"no seremos también las responsables de pedagogizar una sociedad de por sí ya es violenta, y sumar una carga más a una sociedad inequitativa con el rol de la mujer"*

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"El feminicida no es que haya despertado un día con ganas de matar a alguien está es una estructura que se ha construido por años en medio de un sistema patriarcal, y que genera estos hechos violentos y muchas veces también los aplaude"***
>
> <cite>Diana Salcedo | Directora LIMPAL</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

La Directora también señaló que estas violencias se agudizan en medio de un sistema legal patriarcal, en el cual se enmarca toda la sociedad, *"**no es solo cambiar los hogares también generar cambios en el sistema legal el cual en pocas palabras culpa a la mujer por su agresión o violación**, con preguntas absurdas, inhumanas y que desconoces las múltiples feminidades"*.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/redcomunitariat/status/1274290773647937536?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1274290773647937536%7Ctwgr%5E\u0026ref_url=https%3A%2F%2Fwww.eltiempo.com%2Fbogota%2Ftrabajadoras-sexuales-trans-denuncian-violencia-de-la-policia-en-el-centro-de-bogota-509300","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/redcomunitariat/status/1274290773647937536?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1274290773647937536%7Ctwgr%5E&ref\_url=https%3A%2F%2Fwww.eltiempo.com%2Fbogota%2Ftrabajadoras-sexuales-trans-denuncian-violencia-de-la-policia-en-el-centro-de-bogota-509300

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Entre estas expresiones de ser mujer, Salcedo hace referencia específicamente a los transfeminicidios, *"un feminicidio no esta ligado estrictamente a una connotación biológica y a una identidad de género, **poco hablamos las mujeres de las mujeres trans, pero miles de ellas se enfrentan a agresiones, maltratos y asesinatos, y es una lucha que debemos asumir como nuestra también**".*

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3,"textColor":"very-dark-gray"} -->

### **El reinventarse no es una posibilidad para quienes sufren feminicidios y ataques** {#el-reinventarse-no-es-una-posibilidad-para-quienes-sufren-feminicidios-y-ataques .has-very-dark-gray-color .has-text-color}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Salcedo enfatizó que ante los múltiples casos de violencia contra la mujer, ***"el miedo no debe paralizarnos sino al contrario debe motivar muchas más y luchas para defender en la vida,** y gritar a la sociedad que los feminicidios no pueden ser una respuesta ante las relaciones de poder desiguales que hay en el país"*.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó que Colombia tiene que hacer cumplir el sin número de pactos internacionales que ha firmado en torno a la protección de la vida de las mujeres, *"garantizando los derechos humanos y especialmente la vida de las mujeres y niñas* *y dejando de ver este fenómeno con números, donde cada día la cifra es mas grande".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y concluyó diciendo que en medio de los cambios que ha causado a la sociedad de la pandemia y en donde, *"la palabra de moda es reinventemonos, pero para la violencia, y las agresiones hacia la mujer no hay forma de reinvención, porqué el machismo no se transforma se elimina".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph {"textColor":"cyan-bluish-gray"} -->

*(Le pude interesar : Otra Mirada \#VivasNosQueremos" , vea el programa completo y conozco más sobre este tema)*

<!-- /wp:paragraph -->

<!-- wp:html -->  
<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F473187150189431%2F&amp;show_text=false&amp;width=734&amp;height=413&amp;appId" width="734" height="413" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowtransparency="true" allow="encrypted-media" allowfullscreen="true"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
