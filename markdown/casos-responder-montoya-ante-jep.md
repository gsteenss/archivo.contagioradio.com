Title: Los casos por los que debería responder el Gral. Montoya ante la JEP
Date: 2018-07-18 12:48
Category: DDHH, Paz
Tags: Ejecuciones Extra Judiciales, falsos positivos, General Mario Montoya, JEP
Slug: casos-responder-montoya-ante-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/general_mario_montoya.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [18 Jul 2018] 

Tras la firma del acta de sometimiento del General Mario Montoya Uribe ante la JEP, algunas víctimas tienen sentimientos encontrados. De una parte, la llegada del alto mando militar al tribunal de paz les resulta importante porque les permitiría acceder al sistema de Verdad, Justicia, Reparación y Garantías de No Repetición; pero, de otra parte, sienten incertidumbre sobre la verdad que pueda aportar **el mando militar, quien se ha mostrado reacio a aceptar su participación y responsabilidad en los hechos de los que se lo acusa**, lo que les impediría conocer los autores intelectuales de esos acontecimientos.

El Sometimiento ante la JEP del General Montoya será por omisión en el asesinato de 3 jóvenes en Soacha; sin embargo, la lista de hechos que podrían pasar a estudio de la justicia de paz es mas larga. De acuerdo a los documentos recopilados por la Corporación Jurídica Libertad, **el General más condecorado del ejército, estaría involucrado en varios casos de ejecuciones extra judiciales y otras violaciones a los Derechos Humanos.**

### **Montoya debe responder en omisión ejecución extra judicial de tres jóvenes de Soacha** 

Según la Fiscalía, el General Mario Montoya quien fue comandante del Ejército Nacional entre 2006 y 2008, tiene responsabilidad en el asesinato de Daniel Pesca Olaya, Eduardo Garzón Páez y Fair Leonardo Porras, víctimas de ejecuciones extra judiciales en Soacha, Cundinamarca. (Le puede interesar: ["El historial de siete generales que las víctimas le presentaron a la CPI"](https://archivo.contagioradio.com/generales-victimas-cpi-falsos-positivos/))

Aunque la defensa del uniformado se ha empeñado en mostrar que no podía tener conocimiento de estos casos, la Fiscalía asegura lo contrario, dado que estaba en capacidad de evitar los resultados, lo que se puede demostrar con el número de muertes cuestionadas durante su periodo de comandancia, e incluso después de esa época.

En ese sentido Human Rights Watch manifestó que durante los casi 3 años que Montoya estuvo al mando del Ejército **"al menos 2.500 civiles habrían sido víctimas de ejecuciones extrajudiciales"**. Por esa razón, la Fiscalía ordenó medida de aseguramiento en su contra, por emitir directrices que presionaban resultados, así como omitir los controles para evitar que se cometieran las ejecuciones extra judiciales.

### **El General Montoya y la Operación Orión** 

Adicionalmente, Montoya tendría que responder por violaciones a los derechos humanos cometidas durante la operación Orión, en la Comuna 13 de Medellín que dejó un saldo de por lo menos 100 personas desaparecidas durante el gobierno de Álvaro Uribe Vélez. (Le puede interesar: ["Fiscalía General de la Nación pedirá medida de aseguramiento contra el General (r) Mario Montoya"](https://archivo.contagioradio.com/fiscalia-general-de-la-nacion-pedira-medida-de-aseguramiento-contra-el-general-r-mario-montoya/))

Sergio Arboleda, Abogado de la Corporación Jurídica Libertad, asegura que existe registro en **la Comuna 13 de "95 desapariciones forzadas documentadas y 30 homicidios a líderes sociales en el marco de la operación Orión"**. Según el abogado gracias a información de diferentes organizaciones defensoras de Derechos Humanos, es posible afirmar que en el país hay más de 10 mil casos de ejecuciones extra judiciales entre 2002 y 2010; de estas víctimas, cerca del 50% ocurrieron mientras Montoya fue Comandante en Medellín.

Montoya además de participar en la operación Orión, también tuvo injerencia en las operaciones Meteoro, Mariscal y Marcial, que pudieron causar un sin número de víctimas. (Le puede interesar: ["Durante comandancia del General Montoya se documentaron 4 300 falsos positivos"](https://archivo.contagioradio.com/durante-comandancia-del-general-montoya-se-documentaron-4-300-falsos-positivos/))

A la negativa del General de reconocer su responsabilidad en casos de ejecuciones extra judiciales, las víctimas también sienten incertidumbre por las modificaciones hechas a la [Ley de Procedimiento de la JEP](https://archivo.contagioradio.com/aun-se-puede-defender-la-jep/) por congresistas del Centro Democrático, entre las cuales está l**a creación de una Sala Especial para integrantes de la Fuerza Pública, lo que podría retrasar tanto el proceso del General Mario Montoya Uribe,** como el de los militares, policías y miembros de la armada que ya se sometieron ante ese tribunal de paz.

[![Captura de pantalla 2018-07-18 a la(s) 11.18.53 a.m.](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-18-a-las-11.18.53-a.m.-451x635.png){.size-medium .wp-image-54873 .aligncenter width="451" height="635"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/07/Captura-de-pantalla-2018-07-18-a-las-11.18.53-a.m..png)<iframe id="audio_27124247" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27124247_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### **[Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU)** 
