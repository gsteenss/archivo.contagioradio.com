Title: Representantes pedirán que se revoque licencia para desviar el Arroyo Bruno
Date: 2016-05-26 16:06
Category: Ambiente, Nacional
Tags: Ambiente, arroyo Bruno, comunidades, Guajira
Slug: representantes-pediran-que-se-revoque-licencia-para-desviar-el-arroyo-bruno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/Arroyo-Bruno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Minuto 60 

###### [26 May 2016] 

**La Comisión segunda de la Cámara de Representantes pedirá que se revoque la licencia ambiental para el [desvío del arroyo Bruno](https://archivo.contagioradio.com/comunidades-buscan-que-se-suspenda-definitivamente-desviacion-del-arroyo-bruno/)** en el departamento de La Guajira, que la Agencia Nacional de Licencias Ambientales, ANLA, le adjudicó a la empresa minera El Cerrejón.

Esta semana la Comisión Segunda de la Cámara de Representantes visitó el municipio de Albania en La Guajira dónde se llevó a cabo un debate sobre los [proyectos del Cerrejón frente a la desviación del Arroyo Bruno.](https://archivo.contagioradio.com/13-razones-para-no-desviar-el-arroyo-bruno/) Allí también fueron invitados el director de la ANLA, de Planeación Nacional, del Instituto Colombiano de Bienestar Familiar, el Procurador General de la Nación y los ministros de Salud, Interior, Educación, Hacienda, Ambiente, Vivienda y Agricultura. **La única que asistió fue Cristina Plazas del ICBF, mientras que las demás autoridades no asistieron y volvieron a incumplirle a las comunidades**, como ha sucedido reiteradamente.

Según Angélica Ortíz, vocera de la organización Fuerza Mujeres Wayúu, **el mes de suspensión que se dio para realizar una consulta previa no es suficiente pues son varias las comunidades a las que se debería aplicar este mecanismo**, de manera que lo que se exige desde las comunidades es que se revoque la licencia ambiental que autorizó la ANLA para que no haya posibilidad de que se seque [otra fuente más de agua.](https://archivo.contagioradio.com/con-el-arroyo-bruno-serian-27-las-fuentes-de-agua-que-el-cerrejon-ha-secado-en-la-guajira/)

Finalmente, las comunidades expusieron a los representantes que las soluciones a la problemática de la sequía no pueden seguir siendo inmediatistas, y en cambio deben ser soluciones estructurales a través de las cuales no solo se garantice el agua sino también su descontaminación, pues **esa situación ha producido la muerte de 28 niños y niñas durante este año.**

<iframe src="http://co.ivoox.com/es/player_ej_11678962_2_1.html?data=kpajmZ2depOhhpywj5aaaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5yncaLiyMrZy8jFb7Dm1c7njZKPitbZ09%2FOjbLZrsbmxtiYucbdaaSnhqeu15KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
