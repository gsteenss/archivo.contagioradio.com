Title: Gobierno debe atender estructuralmente crisis humanitaria del Chocó
Date: 2016-02-04 16:16
Category: DDHH, Entrevistas
Tags: Chocó, Crisis humanitaria, Río Atrato
Slug: gobierno-debe-atender-estructuralmente-crisis-humanitaria-del-choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Choco-e1454620412930.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Nataly Ramírez 

<iframe src="http://www.ivoox.com/player_ek_10319112_2_1.html?data=kpWgk56VdZOhhpywj5WUaZS1kZWah5yncZGhhpywj5WRaZi3jpWah5yncajjw87S1NPTb8XZw8qYw9nJssXZ05DS1dnWucTo1tfOztLJstXZjMjfy9jNt4zc1tLO0M7YpdOhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Ximena González, Tierra Digna] 

###### [4 Feb 2016] 

Acciones de las empresas legales e ilegales y el olvido estatal, tienen gravemente afectados a los más de 700 mil  afrocolombianos e indígenas que viven en la cuenca del río Atrato, departamento del Chocó, donde **esta semana se contabilizó que en menos de dos años han muerto 37 niños.**

Tras la inspección judicial que realizó una comisión delegada por la Corte Constitucional del 27 al 30 enero, los consejos comunitarios del departamento esperan que el **fallo que deberá emitir la Corte en el mes de marzo, beneficie a las comunidades y  se minimicen los daños ambientales causados en esos territorios.**

“La comunidad espera que se emita un fallo estructural que permita la articulación de diversas instituciones estatales para dar solución al problema”, dice Ximena González, abogada de Tierra Digna, organización que le ha hecho el seguimiento al caso.

Durante la inspección, se recolectaron las pruebas necesarias para evidenciar la grave situación de contaminación en la que se encuentra río Atrato y las problemáticas ambientales y sociales que esto ha generado en la población que vive del afluente. González asegura que **“La explotación minera,  forestal y la ausencia de servicios básicos, hace que se presente una situación crítica para la población** que debe vivir en medio de la contaminación del río, generando problemas de salubridad y la deforestación de la selva del departamento”.

En el Chocó, existe una explotación minera informal en la que se utiliza mercurio, y además, en el bajo Atrato empresas como Maderas de Darién, que realizan explotación forestal de manera legal, tienen a la población en la actual crisis.

De acuerdo con la abogada, preocupa que pese a que a que Defensoría del Pueblo dijo que había una crisis humanitaria en el departamento, no se han realizado las investigaciones pertinentes para determinar la causa de las muertes de los niños, pues siempre se le atribuye a los efectos del agua y alimentos contaminados con mercurio, “pero hay que analizar el problema a profundidad”, pues según ella, **“hay una problemática estructural y el gobierno ha abandonado históricamente la región”.**

Las comunidades dependen de una doble situación, por un lado, económicamente se sostienen de la minería, pero por otro,  son conscientes de lo perjudicial que es esta para el ambiente y para la salud, de manera que desde el estado debe haber una respuesta en la que se enfrente los asuntos ambientales con **soluciones económicas y productivas para la población.**
