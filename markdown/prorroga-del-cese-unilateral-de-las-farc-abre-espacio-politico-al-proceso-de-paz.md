Title: Prorroga del Cese Unilateral de las FARC abre espacio político al proceso de paz
Date: 2015-08-20 16:19
Category: Nacional, Paz
Tags: Camila Moreno, carlos medina gallego, cese bilateral del fuego, cese unilateral de las FARC, Conversaciones de paz de la habana, ELN, ICTJ, Proceso de conversaciones de paz de la Habana
Slug: prorroga-del-cese-unilateral-de-las-farc-abre-espacio-politico-al-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/cese-unilaterla-farc-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: josecardenas.com 

###### [20 Ago 2015] 

La directora del ICTJ en Colombia (Centro Internacional para la Justicia Transicional), Camila Moreno, afirma que “**esta decisión de las FARC es muy importante en vista de que reafirma el compromiso con la búsqueda de una solución al conflicto armado**” en referencia al anuncio de prolongar el Cese Unilateral por parte de esa guerrilla.

Frente a las denuncias de las FARC en cuanto a ametrallamientos y bombardeos en el Putumayo y otros departamentos, Moreno indica que lamentablemente los hechos no corresponden a las decisiones que se toman en la mesa para un desescalamiento del conflicto, este paso de las FARC “*Es muy importante y es una muestra clara de su compromiso con este proceso*”.

Por otra parte se refirió a que los medios de comunicación también deben generar una opinión favorable hacia el proceso de paz, con decisiones como estas que en las comunidades más afectadas por el conflicto son acogidas como más que una esperanza y en la ciudad en donde a pesar que el conflicto no es tan visible, es tomado como un mensaje positivo.

### **El debate continúa frente a los pasos para aprobar acuerdos de la Habana**

Frente a los mecanismos para aprobar los acuerdos en la Habana el profesor Medina Gallego afirma que el anuncio del Presidente Juan Manuel Santos del "Congresito" es una forma de ambientar la discusión en el país de cuáles son los mecanismos a seguir para aprobar los acuerdos en la Habana.

Moreno llamó la atención sobre el papel de los medios de información “*una cosa son los mecanismos que se deben poner en marcha, legales, institucionales, qué necesita hacer el estado colombiano de su legislación, las reformas constitucionales para aceptar lo que pase en la Habana*” y afirma que es necesario generar claridades y no confusión.

Sin embargo el debate sigue abierto frente a cuales son los pasos y mecanismos que se deben tomar para aprobar e implementar los acuerdos de paz en la Habana, ya que este es otro de los temas que también genera mucha incertidumbre en el país.

**El acuerdo con el ELN debe ser paralelo al de las FARC**

Otro de los temas que trataron los analistas fueron los acercamientos con el ELN, quienes han tenido algunos acercamientos con el gobierno, para la instalación de una posible mesa pública en donde se adelante un proceso.

Para que la paz sea completa, t**anto Gallego como Moreno indican que este proceso debe incluir lo más pronto posible a esta guerrilla**, para que las conversaciones se desarrollen paralelamente, principalmente ahora que se discute el punto de justicia y reparación.

Este es un tema que el gobierno debe pensar también como una propiedad en la medida en que cuanto se demore la instalación de esta mesa “hasta que punto se logra un ambiente político dos veces, con lo difícil que es” indica Moreno, además que hace un llamado a poner todos los esfuerzos en este momento para la búsqueda de la paz.
