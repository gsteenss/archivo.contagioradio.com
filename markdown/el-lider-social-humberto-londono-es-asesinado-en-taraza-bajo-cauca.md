Title: El líder social Humberto Londoño, es asesinado en Tarazá Bajo Cauca
Date: 2019-12-08 12:10
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: asesinato, Bajo Cauca, Lider social
Slug: el-lider-social-humberto-londono-es-asesinado-en-taraza-bajo-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/ELMdXbGW4AAerYz-e1575825182634.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:]@GarantiasPSG 

Este 6 de diciembre en horas de la noche fue asesinado el líder social Humberto Londoño en la vía que conduce de Tarazá al corregimiento de la Caucana, según el reporte del Proceso Social de Garantías (PSG), el líder f**ue atacado por hombres armados quienes le dispararon reiteradas veces** hasta causar su muerte. (Le puede interesar: [Grupos ilegales se disputan el Bajo Cauca Antioqueño ante la ausencia del Estado](https://archivo.contagioradio.com/grupos-ilegales-se-disputan-el-bajo-cauca-antioqueno-ante-la-ausencia-del-estado/))

Londoño era coordinador del Comité de Conciliación de la vereda El Socorro- Cañón de Iglesias, y también hacia parte de la Asociación de Campesinos del Bajo Cauca (Asocbac). S**u muerte incrementa a 24 los homicidios registrados en Antioquia en 2019.** (Le puede interear:[En el Bajo Cauca Antioqueño la violencia se está tomando el poder](https://archivo.contagioradio.com/bajo-cauca-antioqueno-violencia-gobierno/))

Hasta el momento se desconoce la identidad de los agresores, sin embargo La Caucana es conocido por la presencia de dos frentes  paramilitares quienes se encuentran en continuos enfrentamientos por el territorio, al ser un punto geográfico crucial  para el negocio del narcotráfico  al ser un corredor que comunica al  sur de Córdoba, con el Urabá Antioqueño.

<div class="css-1dbjc4n r-18u37iz r-thb0q2 r-1mi0q7o" data-testid="tweet">

<div class="css-1dbjc4n r-1iusvr4 r-16y2uox r-1777fci r-5f2r5o">

<div class="css-1dbjc4n r-18u37iz r-1wtj0ep r-zl2h9q">

<div class="css-1dbjc4n r-k200y r-18u37iz r-1h0z5md r-1joea0r">

<div class="css-18t94o4 css-1dbjc4n r-1777fci r-11cpok1 r-1ny4l3l r-bztko3 r-lrvibr" tabindex="0" role="button" aria-haspopup="true" aria-label="Más opciones" data-focusable="true" data-testid="caret">

<div class="css-901oao r-1awozwy r-1re7ezh r-6koalj r-1qd0xha r-a023e6 r-16dba41 r-1h0z5md r-ad9z0x r-bcqeeo r-o7ynqc r-clp7b1 r-3s2u2q r-qvutc0" dir="ltr">

<div class="css-1dbjc4n r-xoduu5">

<div class="css-1dbjc4n r-sdzlij r-1p0dtai r-xoduu5 r-1d2f490 r-podbf7 r-u8s1d r-zchlnj r-ipm5af r-o7ynqc r-6416eg">

</div>

</div>

</div>

</div>

</div>

</div>

</div>

</div>

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
