Title: En Medellín piden 3 millones de deseos por la vida y la paz
Date: 2017-09-21 15:06
Category: DDHH, Nacional
Tags: deseos por la vida, Medellin, No Matarás
Slug: en-medellin-pediran-3-millones-de-deseos-por-la-vida-y-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/DKQhF00XoAAnJ4h.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twitter] 

###### [21 Sept. 2017] 

Luego de haber coloreado varias fuentes de la ciudad con anilina roja vegetal, como forma de rechazo y para llamar la atención por el aumento de los asesinatos en Medellín, los jóvenes impulsores de esta causa decidieron hacer una colecta de dinero para poderle devolver a la ciudad el dinero invertido para limpiarlas, llamada 3.070.133 deseos por la vida. Luego de 24 horas de abierta la campaña lograron recolectar esa suma de dinero.

Por ello, **este 21 de septiembre en el marco del Día Internacional de la Paz en el Parque San Antonio a las 6 p.m. se darán cita** todas las personas que quieran lanzar estas monedas recolectadas pidiendo 3.070.133 deseos por la vida y contra el asesinato. Le puede interesar: [Un aporte a la paz urbana desde Castilla, Medellín](https://archivo.contagioradio.com/un-aporte-a-la-paz-urbana-desde-castilla-medellin/)

Maira Duque una de las integrantes de esta iniciativa dijo que “después de la acción vamos a recoger las monedas y **el viernes en la mañana la vamos a llevar donde un notario para que él nos ayude a contarlas** y certifique que ahí está la totalidad del dinero que le devolvemos a la ciudad”. Posteriormente, se dirigirán a la Alpujarra para entregarle el dinero al Alcalde de Medellín.

Aunque es muy difícil pronosticar cuántas personas estarán presentes en este acto, dice Duque que sí ha tenido muy buena acogida “igual \#NoMatarás ha tenido mucho apoyo en otros momentos por parte de muchas personas, por ejemplo, cuando nos entregamos en el Teatro Pablo Tobón porque el Alcalde nos estaba buscando fuimos alrededor de 200 personas a entregarnos, entonces esperamos que esta vez vayan muchas personas”.

### **Control de paramilitares sigue estando presente en Medellín** 

Pese a las cifras entregadas por el Alcalde de Medellín, Federico Gutiérrez y las autoridades quienes dicen que los homicidios han disminuido en la capital de Antioquia, la situación de violencia se sigue agudizando en los territorios. Le puede interesar: [30 años después los claveles rojos vuelven a marchar en Medellín](https://archivo.contagioradio.com/marcha-claveles-rojos/)

“En la ciudad es una realidad que muchas bandas criminales siguen controlando muchos barrios y durante muchos años hemos logrado tasas de homicidio relativamente bajas frente a lo que recibió Medellín en los años 20, estos actores se han logrado consolidar muy fuertemente en los barrios”.

Hoy el 50% de los asesinatos en Medellín son precisamente por disputas entre bandas criminales, lo que se traduce en que sigue haciendo falta presencia institucional en los territorios. Le puede interesar: [En Medellín pedirán 3 millones de Deseos por la Vida](https://archivo.contagioradio.com/en-medellin-pediran-3-millones-de-deseos-por-la-vida/)

“Hay un estudio de EAFIT que encontró que había una alta correlación entre las personas que eran asesinadas y su edad, generalmente son jóvenes. El 49% de los asesinados en Medellín son jóvenes entre 18 y 30 años y quienes tienen mayores niveles de analfabetismo e ingresos económicos bajos, además son quienes viven en las zonas más periféricas de la ciudad”.

Concluye diciendo que si bien el homicidio es un tema muy grave que acontece en Medellín, tienen claro como colectivo de ciudadanos que esa realidad es tan solo la punta del iceberg de muchos factores y de condiciones sociales que hacen que los patrones de criminalidad se sigan reproduciendo “hay que hablar de las brechas sociales”.

<iframe id="audio_21021866" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21021866_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

<div class="osd-sms-wrapper">

</div>
