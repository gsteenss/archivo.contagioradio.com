Title: Walter Scott, otra víctima afrodescendiente de la policía de  EEUU
Date: 2015-04-08 13:30
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: asesinatos raciales EEUU, ferguson, Michael Brown, Michael Slager, North Charleston, Policía blanco mata a negro en Carolina del Sur, Policía de Estados Unidos, Walter Scott
Slug: walter-scott-otra-victima-afrodescendiente-de-la-policia-de-eeuu
Status: published

###### Foto:Elconfidencial.com 

El **sábado 4 de abril en Carolina del Sur**, EEUU, un **policía blanco asesinó a tiros a un hombre negro desramado, por la espalda**, la secuencia quedó grabada, creando gran indignación entre la comunidad afrodescendiente.

**Impactante video publicado hoy por el abogado de la familia de Scott muestra toda la escena del crimen y atestigua el brutal asesinato.**

Todo sucedió en la localidad de **North Charleston** (Carolina del Sur), cuando el **agente** de raza blanca **Michael Slager**, **detuvo el coche** de **Waltter Scott** por tener rota una de las luces de su auto.

Video del brutal asesinato: **(imágenes fuertes)**

\[embed\]https://www.youtube.com/watch?v=XKQqgVlk0NQ\[/embed\]

El ciudadano de raza negra había sido **detenido hasta 10 veces por no poder pagar la manutención de sus hijos**, con lo que al ser detenido el coche huyó por miedo a represalias similares.

 Fue entonces cuando el policía **disparó por la espalda cuatro veces a Scott**, hasta que cayó abatido en el suelo. **Malherido y desangrándose el policía lo inmovilizó poniéndole las esposas**, es en este momento cuando se ve al agente **dejar un objeto al lado del cuerpo** aún con vida.

Después de lo sucedido **Slager ha sido acusado de asesinato** y de tentativa de modificar la escena del crimen en beneficio propio. El agente alega que le disparó al intentar quitarle el teaser, se presume que en las imágenes, es cuando el policía deja caer un objeto sobre el cadáver después de esposarlo,  intentando así falsear lo que realmente ocurrió.

Según alcalde de la localidad de North Charleston, Keith Summey, donde el **47% de la población es afrodescendiente**  "…Cuando te equivocas, te equivocas. Cuando tomas una mala decisión, no importa si eres un oficial o un ciudadano de la calle, tienes que afrontar esa decisión…".

Este no es el primer caso, de hecho desde que en Agosto de 2014 muriera en las mismas circunstancias **Michael Brown en Ferguson**, provocando **fuertes disturbios y protestas** de la comunidad afrodescendiente, se han sucedido al menos tres muertes más en distintos estado.

Después del **informe emitido por la justicia** de EEUU, acusando al estado de Ferguson de utilizar un **patrón racista en las detenciones y judicializaciones de individuos de raza negra**, el **comisario** **general de la policía** de Ferguson, junto con **dos** cargos **políticos** más **dimitieron**.

Tras la elección de Obama, resurgieron las protestas raciales, agravadas por los escandalosos casos de asesinatos de ciudadanos de raza negra desarmados a manos de policías blancos.

Según los **informes** de las organizaciones sociales de **DDHH**, las cifras indican que en comparación con la época de la segregación racial, la **situación no ha mejorado** y tiende a agravarse debido a la **exclusión social** en la que se encuentra la **comunidad afro** en **EEUU**.
