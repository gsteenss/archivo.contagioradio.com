Title: Este es el balance en materia de DDHH en Colombia para 2015
Date: 2015-12-11 13:35
Category: DDHH, Entrevistas
Tags: Asociación Minga, Balance de DDHH en Colombia para 2015, Comisión Intereclesial de Justicia y Paz, Día internacional de los DDHH, Observatorio de Derechos Humanos de la Coordinación Colombia-Europa-Estados Unidos, Plataforma Colombiana de Derechos Humanos Democracia y Desarrollo
Slug: este-es-el-balance-en-materia-de-ddhh-en-colombia-para-2015
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/DDHH-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colectivo de Abogados José Alvear Restrepo ] 

###### [[10 Dic 2015] ] 

[La Oficina en Colombia del Alto Comisionado de las Naciones Unidas para los Derechos Humanos en articulación con la Fiscalía General de la Nación, consolidaron un registro de **729 homicidios de defensores de Derechos Humanos entre 1994 y 2015**, un promedio anual de 33 asesinatos casi todos ellos en la impunidad, lo que indica que para este año el balance en la materia no resulta positivo. ]

<iframe src="http://www.ivoox.com/player_ek_9675315_2_1.html?data=mpukl5iVeY6ZmKiakpyJd6KkkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibTIppCytZCpkIy2orGusKipb6bCjLKutqq2jaKfpaqYpqmsjIy5r5CwsbGzkaO9opC9o7elb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alberto Yepes, Coordinación Colombia-Europa-Estados Unidos] 

[Alberto Yepes, coordinador del ‘Observatorio de Derechos Humanos de la Coordinación Colombia-Europa-Estados Unidos’, asegura que el 2015 “ha sido un año trágico… estamos asistiendo, sobre todo en los últimos tres meses, a un robustecimiento de la **presencia paramilitar** en muchas regiones del país… dedicándose a una **actitud ofensiva contra comunidades reclamantes de tierra y defensores de Derechos Humanos**”. Pese a ello, estas acciones “tienen como correlato la **esperanza del acuerdo de paz que toda la sociedad colombiana **está esperando se concrete prontamente" para lograr mayores garantías en el ejercicio de la movilización social.]

<iframe src="http://www.ivoox.com/player_ek_9675515_2_1.html?data=mpukl5qVeY6ZmKiakpmJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibTIppCytZCpkIy2orGusKipb6bCjLKutqq2jaKfpaqYpqmsjIy5r5CwsbGzkaO9opC9o7elb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yessica Hoyos, Plataforma Colombiana de DDHH Democracia y Desarrollo] 

[Yessica Hoyos, abogada y vocera de la ‘Plataforma Colombiana de Derechos Humanos Democracia y Desarrollo’ indica que **actualmente los defensores de DDHH se enfrentan a estigmatizaciones muy fuertes que les pueden ocasionar amenazas y asesinatos**. El balance en materia de derechos humanos resulta complejo, el contexto de los 'Diálogos de Paz' se contrasta con lo que sucede en los territorios donde persisten los robos de información, las amenazas, los hostigamientos y las persecuciones. Lo que indica que **el país debe enfrentarse el próximo año al gran reto de materializar la paz a través de garantías reales para todas aquellas personas que defienden la vida**. ]

<iframe src="http://www.ivoox.com/player_ek_9675856_2_1.html?data=mpukl52Zeo6ZmKiakp2Jd6KmkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibTIppCytZCpkIy2orGusKipb6bCjLKutqq2jaKfpaqYpqmsjIy5r5CwsbGzkaO9opC9o7elb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Fabio Arias, Central Unitaria de Trabajadores] 

[Fabio Arias, secretario general de la ‘Central Unitaria de Trabajadores’, indica que los 18 homicidios, sumados a los atentados y las amenazas contra sindicalistas en lo que va corrido del 2015 muestran que la “violencia antisindical en Colombia se mantiene, no es un tema del pasado, sigue siendo del presente... **el Gobierno está judicializando la protesta social**… para que los dirigentes sindicales no puedan hacer uso de sus derechos legítimos”. No obstante "le estamos apostando a que el otro año en medio de los acuerdos que se deriven con las guerrillas el **Gobierno revise la normatividad que ha venido legalizando contra la movilización social**... como una oportunidad para que demuestre su voluntad política en el tratamiento civilista y democrático a organizaciones legales como los sindicatos".  ]

<iframe src="http://www.ivoox.com/player_ek_9673485_2_1.html?data=mpuklZmceY6ZmKiakpeJd6Kol5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidToxpDS1ZDJsIzWwtHO0MjJb8bijNLO1srWrcKfxcqYpqmsjIzZz5Cw0dHTscPdwpDdw9fFb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Abilio Peña, Comisión de Justicia y Paz ] 

[Abilio Peña, defensor de derechos humanos e integrante de la ‘Comisión Intereclesial de Justicia y Paz’, manifiesta que **no son equiparables los avances de la Mesa de Diálogos con lo que está sucediendo en nuestro país**, "como organización estamos documentando a diario presencia paramilitar en muchas zonas, agresiones de parte de militares, ocupaciones de tierra y ausencia de respuestas efectivas por parte del Gobierno frente a decisiones de la Corte Interamericana de Derechos Humanos, que nos muestran que no es nada alentador el balance” en el panorama de los DDHH durante este año.]

<iframe src="http://www.ivoox.com/player_ek_9675716_2_1.html?data=mpukl5yVeo6ZmKiakpuJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibTIppCytZCpkIy2orGusKipb6bCjLKutqq2jaKfpaqYpqmsjIy5r5CwsbGzkaO9opC9o7elb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Javier Marín, Asociación Minga] 

[Javier Marín, integrante de la ‘Asociación Minga’, afirma que la organización social en las comunidades se ha fortalecido a tal punto de llevarlas a asumirse y ser reconocidas por la institucionalidad colombiana como sujetos de derechos, frente a un **aparato estatal que muchas veces ha actuado contra la materialización de tales garantías**. Así mismo, llama la atención sobre el panorama del actual Proceso de Paz, en el que se disputan distintos proyectos de país, pues en ese marco **el Gobierno ha visto en el protagonismo de la sociedad civil una amenaza poniendo en marcha mecanismos y políticas de corte represivo** para quienes se oponen a megaproyectos de infraestructura o minería en las regiones.]

<iframe src="http://www.ivoox.com/player_ek_9675920_2_1.html?data=mpukl56WdI6ZmKiakpyJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRibTIppCytZCpkIy2orGusKipb6bCjLKutqq2jaKfpaqYpqmsjIy5r5CwsbGzkaO9opC9o7elb46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Luz Marina Monzón, abogada defensora de derechos humanos] 

[Luz Marina Monzón, abogada defensora de derechos humanos, concluye afirmando que la violación de derechos han significado para las victimas momentos dramáticos frente a los cuales **defensoras y defensores de DDHH están llamados a cumplir un rol fundamental como actores políticos**, en un momento como el del actual proceso de paz que “significa una **oportunidad para la construcción de un país distinto y una nueva sociedad” basada en la verdad** y que implique el reconocimiento de las experiencias de quienes han sido víctimas y de las condiciones que están alrededor de la violación a sus derechos humanos.]
