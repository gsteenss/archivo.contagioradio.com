Title: Estos son los 11 finalistas al Premio Nacional a la Defensa de los Derechos Humanos
Date: 2019-08-21 17:01
Author: CtgAdm
Category: eventos, Líderes sociales
Tags: Derechos Humanos, Diakonia Colombia, Iglesias Suecas, Lideres, lideresas, Premios
Slug: finalistas-del-premio-a-la-defensa-de-los-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/DSC9111.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Sin-título-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio] 

La VII edición de los premios que reconocen el trabajo de **defensores y defensoras de Derechos Humanos** en Colombia, galardonará cuatro categorías que hicieron parte de una selección inicial de 45 nominaciones y de la que finalmente quedaron **11 finalistas,** entre ellos se encuentran; asociaciones, colectivos, ONG, líderes y lideresas, de distintas comunidades alrededor del país, que trabajan por los territorios, el crecimiento de las comunidades y en general  su compromiso por  los derechos humanos.

Este premio  ha reconocido desde sus orígenes a 14 defensores y defensoras, también 17 organizaciones que han sido destacadas en las diferentes categorías, para **Karen Cárdena, coordinadora del Premio, “los galardonados son personas y organizaciones  extraordinarias, que han logrado salir del anonimato y volverse parte indispensable de sus comunidades por eso vale la pena reconocerles su valentía”.**

### **Los Nominados que defienden los derechos humanos** 

Las categorías que se premian son; **Defensores o Defensoras** del año, presentado como finalistas; **Isabel Cristina Zuleta**, socióloga, líder ambientalista y feminista, quien ha ganado su reputación a través de movimiento “Ríos Vivos”, **Victor Hugo Moreno Mina**, economista, quien ha dedicado gran parte de su vida a la defensa de los derechos étnicos y territoriales del pueblo negro, y **Clemencia Carabalí Rodallega**, con más de 30 años en la defensa de territorios ancestrales de las comunidades negras, es la presidenta y cofundadora de “Mujeres Afrodescendientes del Norte de Cauca”(ASOM) ( Le puede interesar:[Los acuerdos son para cumplirlos Comunidades negras del Cauca al Gobierno](https://archivo.contagioradio.com/los-acuerdos-son-para-cumplirlos-comunidades-negras-del-cauca-al-gobierno/))

En la categoría "**Procesos Sociales de Base Comunitaria"** los nominados son la **Asociación Campesina de Catatumbo (ASCAMCAT)** que desde 2005 trabaja por el ambiente y los derechos y las causas campesinas en Norte de Santander, en esta misma se encuentra la **Asociación Campesina del Valle del Río Cimitarra (ACVC)**, fundada en 1996, trabaja por el desarrollo social comunitario y político, cambiando la mirada de los megaproyectos por alternativas que potencien esta zona, y finalmente la **Asociación de Desplazados de Ocaña (Asodepo)**; nacida en 2002, que enfrentan la situación de muchas víctimas de conflicto que han sido olvidadas por el estado.

La categoría que premia la **Experiencia o proceso colectivo** del año nominó al **Comité Permanente por la Defensa de los Derechos Humanos** (CPDH), trabajando directamente en comunidades de Arauca y la vulneración de sus derechos en espacios de reconciliación y paz; **La Consultoría para los Derechos Humanos y Desplazamiento** (CODHES), quienes monitorean la situación de DDHH en Colombia y finalmente la **Comisión Intereclesial de Justicia y Paz**, con un trabajo de más de 30 años por comunidades rurales y urbanas, resaltando que no hay olvido y devolviendo la integridad de las victimas mediante la búsqueda de la verdad y la justicia.

> ##### *["Esta nominación no es solo para nosotros, es para las comunidades, negras y mestizas a la cuales hemos prestado nuestros servicios en estos 31 años, esperamos que este homenaje a la vida siga siendo un reconocimiento a las victimas ..." Danilo Rueda Defensor de derechos de la Comisión de Justicia y Paz.]* 

Los premios los cierra el homenaje **“A toda una vida”,** donde se seleccionan personas que lleven un prolongado periodo de tiempo trabajando por los derechos humanos en Colombia; los nombres que entran en esta categoría son; **Claudia Julieta Duque**, periodista defensora de derechos humanos y libertad de prensa, **Ricardo Esquivia Ballestas**, defensor por más de 40 años de los DDHH y gestor en la recuperación de víctimas, y **Teresa del Niño Jesús Gaviria Urrego**, quien ha dedicado gran parte de su vida a luchar contra la desaparición forzada en Colombia. [(Le puede interesar: No voy a permitir que me silencien, es un tema de principios: Claudia Julieta Duque)](https://archivo.contagioradio.com/no-voy-a-permitir-que-me-silencien-es-un-tema-de-principios-claudia-julieta-duque/)

### **Los Jurados ** 

Este año los ganadores de los premios serán seleccionados bajo el criterio de **jurados,** donde se puedes destacar periodistas, sociólogos, defensores cada uno desde su campo  de los derechos humano, algunos de ellos son Monseñor Héctor Fabio Henao, Marta Nubia Bello, Doris Marcela Hernández**,** Mario Morales,Gloria Yaneth Castrillón, ademas jurados que desde sus diferentes campos de acción han trabajado por la dignificación de las comunidades en Colombia,  países como Uruguay, Paraguay, Estados Unidos, Suiza, Reino Unido y Alemania.

### ** Los organizadores** 

Como cada año vale la pena destacar el trabajo de quienes hacen posible este galardón, entre ellos  ACT Iglesias Suecas, que es la unión de varias iglesias protestantes, evangélica protestante conformado por actores comunitarios que trabajan por el desarrollo de su país y la cooperación internacional, y Diakonia, organización de cooperación internacional que nace de las iglesias libres de Suecia.

Este evento como lo dice Karen Cárdenas coordinadora del premio, “es un reconocimiento que se queda corto frente  a la ardua labor de no solo los que están nominados, sino múltiples líderes y lideresas que trabajan por los derechos humanos en Colombia”, vincula a muchas comunidades y los ubica en el foco de organizaciones y medios de comunicación que quieres destacar su trabajo. La entrega de los galardones se dará este 4 de septiembre en en el marco de la Semana por la Paz y la celebración del día de los Derechos Humanos. (Le puede interesar: [Ya estamos listos para la Semana por la Paz)](https://archivo.contagioradio.com/listos-semana-por-la-paz/)

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
