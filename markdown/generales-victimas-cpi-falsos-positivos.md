Title: El historial de siete generales que las víctimas le presentaron a la CPI
Date: 2017-09-20 15:50
Category: Otra Mirada
Tags: Ejecuciones Extrajudiciales, Ejército Nacional, falsos positivos
Slug: generales-victimas-cpi-falsos-positivos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/FALSOS-POSITIVOS-e1500995272586.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Pilon] 

###### [19 Sep 2017] 

[En el marco de la reunión entre la fiscal de la Corte Penal Internacional con organizaciones sociales, integrantes de organizaciones de DDHH que acompañan a algunas de las  víctimas de ejecuciones extrajudiciales, presentaron los nombres de algunos generales del ejército implicados en los mal llamados “falsos positivos”.]

[Y es que si bien cerca de 800 soldados o suboficiales han sido condenados, apenas unos pocos coroneles y generales han rendido cuentas ante la justicia por estos casos, en cambio las investigaciones no avanzan o son demasiado lentas. Además hay que tener en cuenta que esta práctica obedeció a la directiva 029 de 2005 del Ministerio de Defensa avalada por el presidente de ese entonces en que se autorizaba el pago de 3'800.000 pesos por cada guerrillero muerto. (Le puede interesar:[CPI solicita que 29 coroneles y generales sean juzgados por ejecuciones extrajudiciales](https://archivo.contagioradio.com/corte_penal_internacional_falsos_positivos/))]

[Estas directivas propiciaron las alianzas criminales paa fabricar cadáveres y recibir por ellos una recompensa económica. Ante ello, específicamente son siete los generales y coroneles implicados en estos crímenes según lo han denunciado ante la CPI. ]

### **General (r) Óscar González Peña** 

[ De acuerdo con un informe de Human Right Watch publicado en junio del 2015, durante el período en el cual el General (r) Óscar González Peña fue comandante del Ejército Nacional de 2008 a 2010; se cometieron mínimo 113 presuntas ejecuciones extrajudiciales perpetradas por soldados de la Cuarta Brigada. De acuerdo con las palabras de Jose Miguel Vivanco, director de HRW, en una entrevista a Semana.com, se han encontrado evidencias creíbles que evidencian que el general estuvo implicado en estos casos.]

[González Peña fue comandante del Comando Conjunto del Caribe, y luego pasó a ser el reemplazo del General Mario Montoya, luego de que este presentara su sorpresiva renuncia al entonces presidente Álvaro Uribe Vélez.  Lo hizo luego de que se empezara a investigar los primeros casos de ejecuciones extrajudiciales, por lo cual fueron separados de sus cargos 27 oficiales, entre ellos tres generales, y suboficiales.]

[Para ese momento, Uribe designó a González, por tener “eficacia, transparencia y eficiencia” como “la base para que una política de seguridad democrática sea creíble y por ende sostenible”. (Le puede interesar: [Juez Novena de Garantías se burla de las víctimas de los "falsos positivos](https://archivo.contagioradio.com/juez-noveno-de-garantias-se-burla-de-las-victimas-de-ejecuciones-extrajudiciales/)")]

### **General (r) Henry Torres Escalante** 

Torres Escalante fue jefe del Comando Conjunto Número 2 del Suroccidente, es decir que tuvo a su cargo los departamentos de Valle del Cauca, Cauca y Nariño. Realizó un curso básico de Ascenso, Estado Mayor y de Altos Estudios Militares y fue titulado como Ingeniero de Obras Civiles y Militares, en Gerencia de Proyectos Integrales y cursó un seminario acerca del Narcotráfico en la Universidad de la Sabana.

[El mismo informe de HRW recuerda que este general estuvo en la Décimo Sexta Brigada, que presuntamente fue responsable de 113 de los casos de "falsos positivos" entre diciembre de 2005 y junio de 2007.  Contra Torres Escalante existen expedientes donde reposa uno de los casos en los que se documenta el asesinato dos hombres.]

[Se trata de un caso que ocurrió el 16 de marzo de 2007 en la vereda El Triunfo, en Casanare. Esa fecha, algunos militares adscritos a la Brigada XVI que dirigida Torres, asesinaron al campesino Daniel Torres y a su hijo menor de edad Roque, y luego ambos fueron presentados como guerrilleros del ELN.]

[Por este crimen el general habría sido]detenido y en su medida de aseguramiento se documentaban las pruebas contra el general entre las que se incluye 41 inspecciones judiciales, nueve pruebas periciales, 207 pruebas documentales y 86 declaraciones en el acervo probatorio.

### **General (r) Mario Montoya Uribe** 

Según la Fiscalía General de la Nación, como comandante del Ejército Nacional de 2006 a 2008; en el gobierno de Uribe Vélez,  Montoya tiene responsabilidad  en el asesinato de tres jóvenes de Soacha, Daniel Pesca Olaya, Eduardo Garzón Páez y Fair Leonardo Porras. La investigación apunta a que el general implementaba "una política de presión por resultados", por lo que debía responder por al menos 10 casos de ejecuciones extrajudiciales.

Aunque Montoya Uribe lo niega, la Fiscalía asegura que era imposible que el general no tuviera conocimiento, ya que “Estaba en capacidad de evitar los resultados, lo cual puede inferirse a partir del número de muertes cuestionadas durante el periodo de su comandancia y posterior a ella".

Por otra parte, de acuerdo con el informe de HRW del 2015, se dice que a[l menos 44 presuntas ejecuciones extrajudiciales perpetradas por soldados de la Cuarta Brigada, se presentaron durante el período en el cual el General estuvo al mando. No obstante, ]{lang="ES"}de acuerdo con organizaciones defensoras de derechos humanos, no se trata únicamente de esos casos, sino de los más de 4.300 'falsos positivos' documentados cuando Montoya fue comandante de la Cuarta Brigada de Medellín. (Le puede interesar:[13 militares y un condenados por ejecuciones extrajudiciales](https://archivo.contagioradio.com/13-militares-y-un-paramilitar-fueron-condenados-por-ejecuciones-extrajudiciales/))

"Lo que hemos sostenido las organizaciones de derechos humanos, es que  no son solo 7 casos sino 4.300 que se han documentado cuando del 2001 al 2003 Montoya se desempeñó como máximo mando de la Cuarta Brigada, mientras se llevaron a cabo la operaciones como Orión, Meteoro, Mariscal, Marcial, que arrojan un sin número de víctimas, que no son hechos aislados sino que son una política de Estado, encargada de convertir a las fuerzas militares en una empresa criminal”, señala Sergio Arboleda, abogado de las víctimas e integrante de la Corporación Jurídica Libertad.

### **General Jorge Enrique Navarrete Jadeth** 

Pese a las advertencias de HRW Navarrete, en noviembre fue ascendido el brigadier general Jorge Enrique Navarrete Jadeth. Este militar fue el segundo comandante de Barrios Jiménez en la Octava Brigada en 2008, y  por su cargo y el tiempo en el que estuvo en ese cargo, es investigado por las ejecuciones extrajudiciales cometidas por la Octava Brigada, así como por su presunta cooperación con grupos paramilitares.

Human Rights Watch señala que las investigaciones evidenciarían que en 2008 él y el General Emiro José Barrios Jiménez firmaron el pago de 2 millones de pesos a un informante por “la muerte en combate de dos terroristas”.

### **General Emiro José Barrios Jiménez** 

Además de la presunta evidencia sobre el pago de 2 millones de pesos para los informantes. Al brigadier general Emiro José Barrios Jiménez, la Fiscalía lo investiga por su rol en “falsos positivos” durante su comandancia de la Octava Brigada del Ejército durante el 2007, 2008 y 2009. Las investigaciones son por al menos 19 muertes presuntamente perpetradas en 2008 por soldados de la brigada que el comandaba.

### **General Ricardo Andrés Bernal Mendiola** 

[Fue excomandante de la Primera División del Ejército. Bernal es investigado por su presunta participación en las ejecuciones extrajudiciales de dos personas a manos de militares en Norte de Santander. Bernal Mendiola es recordado por su participación en la operación Jaque, a él un subalterno lo acusó de participar en la masacre de 12 personas  que luego fueron presentadas como guerrilleros muertos en norte de Santander.]

### **General Jorge Salgado** 

[Salgado Restrepo fue director de la Escuela Militar de Cadetes José María Córdova y ascendió al grado de general en 2012. Además fue c[omandantes de la Décima Primera Brigada y su resposabilidad en casos de ejecuciones extrajudiciales sería investigada en los años 2007 y 2008.]

[La Fiscalía le archivó un proceso por homicidio en persona protegida tras encontrar que sus acciones al llegar a la Brigada 11, con sede en Montería, evitaron que se cometieran más ejecuciones de civiles, pues habría nombrado a un asesor jurídico para impulsar las investigaciones disciplinarias y las denuncias empolvadas contra sus hombres, reforzó el control de las operaciones militares y descubrió la máquina criminal del coronel Luis Fernando Borja.]

###### Reciba toda la información de Contagio Radio [en su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio ](http://bit.ly/1ICYhVU) 
