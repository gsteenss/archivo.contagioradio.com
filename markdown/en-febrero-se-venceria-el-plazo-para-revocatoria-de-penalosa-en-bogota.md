Title: En febrero se vencería el plazo para revocatoria de Peñalosa en Bogotá
Date: 2018-01-24 14:55
Category: Movilización, Nacional
Tags: CNE, Consejo Nacional Electoral, Revocatoria a Peñalosa, Unidos revocaremos a Peñalosa
Slug: en-febrero-se-venceria-el-plazo-para-revocatoria-de-penalosa-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Enrique_Peñalosa-e1488922778412.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Wikimedia Commons] 

###### [24 Ene 2018] 

Con la resolución a la recusación que solicitaba un impedimento para que el magistrado Novoa participara en la discusión sobre la revocatoria a Peñalosa, se reanudaría el proceso en el Consejo Nacional Electoral. Sin embargo, debido a las múltiples trabas del CNE la **revocatoria podría quedar en nada si no se convoca en febrero, razón por la cual, los promotores a la revocatoria ahora exigen la renuncia del Alcalde**.

De acuerdo con Carlos Carrillo, vocero Unidos Revocaremos Peñalosa, la Registraduría necesita como mínimo dos meses para convocar la Revocatoria, por lo tanto, el CNE debería tomar la decisión antes de estos dos meses, **es decir antes de marzo, para que puedan realizarse las elecciones atípicas**.

“Ellos han venido demorando esto durante medio año, esto es nuevamente una estrategia de dilación y a menos que ellos emitan el certificado inmediatamente, pues la revocatoria no va a poderse dar” afirmó Carrillo. (Le puede interesar:["La revocatoria es en las calles": Comité Unidos Revocaremos a Peñalosa)](https://archivo.contagioradio.com/resistraduria-avala-firmas-para-revocatoria-de-penalosa/)

Frente a esta situación, Carrillo aseguró que que la otra ruta exigir la renuncia de Peñalosa y movilizarse en las calles “**hay que apuntar a obligar al alcalde a que renuncie y eso solo se logra con movilizaciones masivas de gente en las calles exigiéndoselo**”.

De igual forma, el candidato a la cámara por la lista de la Decencia, José Cuesta interpuso un incidente de desacato, que de tener éxito podría llevar al arresto de los magistrados a los que se les compruebe no haber acatado el fallo del Tribunal de Cundinamarca que ordenaba destrabar el proceso de revocatoria al alcalde de Bogotá.

<iframe id="audio_23345082" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23345082_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="osd-sms-wrapper">

</div>
