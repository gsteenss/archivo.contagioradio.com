Title: Colombia acogerá el evento más importante de resistencia al fracking en Latinoamérica
Date: 2018-08-27 17:48
Category: Ambiente, eventos
Tags: Alianza Colombia Libre de Fracking, Colombia Resistencia, fracking
Slug: colombia-acogera-resistencia-fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-27-a-las-5.17.01-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @CarlosSantiagoL] 

###### [27 Ago 2018] 

La **Jornada Internacional Territorios Frente al Fracking en América Latina** se llevará a cabo desde este martes 28 de agosto hasta el sábado 1 de Septiembre en diferentes regiones del país. El evento contará con 16 expertos y expertas de Estados Unidos, México, Brasil, Bolivia, Perú, Chile y Argentina quienes hablarán sobre los efectos de la explotación de Yacimientos No Convencionales (YNC).

**Carlos Andres Santiago, miembro de la Alianza Colombia Libre de Fracking,** con el evento buscan sensibilizar sobre los efectos del fracturamiento hidráulico, que están avalados por "investigaciones, evidencias científicas" que prueban los riesgos del uso de esa técnica, asociados a la salud pública, la contaminación del agua y los impactos sociales.

Adicionalmente, espera que sirva poner al Fracking en el centro del debate de la opinión pública, como un impulso al Proyecto de Ley radicado a principios de mes, con el apoyo de 30 congresistas de diferentes partidos, con el que se pretende prohibir la explotación de YNC en Colombia. (Le puede interesar:["Radicado proyecto de Ley que prohibiría el fracking en Colombia"](https://archivo.contagioradio.com/listo-proyecto-de-ley-que-prohibiria-el-fracking-en-colombia/))

El evento iniciará con una reunión privada de la Alianza Latinoamericana frente al Fracking, que es el **"escenario regional de articulación más importante de oposición"** ante esta técnica. El miércoles se desarrollará un desayuno con 28 congresistas de 10 partidos políticos para hablar sobre el tema, y en la tarde se realizará un foro abierto en que los expertos contarán las experiencias de la explotación de YNC en diferentes partes del mundo.

Para finalizar la jornada, el viernes y sábado la agenda se trasladará a Boyacá y al Magdalena medio, en los que esperan hablar sobre los procesos de resistencia a las explotaciones no convencionales desde los territorios. (Le puede interesar: ["No es posible hacer Fracking de forma responsable"](https://archivo.contagioradio.com/no-es-fracking-responsable/))

<iframe id="audio_28111014" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28111014_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU). 
