Title: Organizaciones proponen acciones para fortalecer la restitución de tierras
Date: 2016-04-15 13:13
Category: DDHH, Nacional
Tags: Ley 1448, Red de Observatorios sobre la restitución de tierras y derechos territoriales, Restitución de tierras
Slug: organizaciones-proponen-acciones-para-fortalecer-la-restitucion-de-tierras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/restitucion-de-tierras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [15 Abril 2016 ]

Organizaciones integrantes de la 'Red de Observatorios sobre la restitución de tierras y derechos territoriales', comunicaron en rueda de prensa su respaldo general a la política pública para restituir territorios despojados en el marco del conflicto armado, así como a las legítimas reclamaciones de las víctimas a través de la Ley 1448; la cual, pese a sus limitaciones, representa un **avance estatal importante para la garantía de los derechos de las víctimas**, razón por la que afirman debe continuar y no deslegitimarse.

"Antes que una oposición frontal que conduzca a abandonar la política, la discusión pública debe insistir en proponer su mejora y asegurar la sostenibilidad de la reparación integral de las víctimas", aseveran las organizaciones e insisten en que el "ataque" a la Ley 1448, que han manifestado funcionarios públicos como el Procurador Alejandro Ordóñez y la Representante del Centro Democrático María Fernanda Cabal, así como Félix Lafauire, evidencia la **"reacción de los sectores que se vieron mayoritariamente beneficiados por el despojo"**, con implicaciones políticas, judiciales y en materia de derechos humanos.

En la Procuraduría, en el Congreso y en medios de comunicación se ha dicho que la Ley 1448 "es la principal amenaza para los derechos de propiedad de los campesinos vulnerables hoy en Colombia", un argumento que no sólo puede representar atraso en los procesos judiciales, sino que ha tenido **implicaciones en la integridad de las familias que reclaman la restitución de sus predios**.

En los [[municipios de San Angel y Chibolo](https://archivo.contagioradio.com/persisten-las-amenaza-contra-el-proceso-restitucion-de-tierras/)], se ha denunciado la presencia de actores armados que buscan a los líderes de la restitución y advierten represalias en su contra, **"un ambiente antirestitución que podría justificar nuevos escenarios de violencia"**, cómo aseguró Claudia Erazo de la Corporación Jurídica Yira Castro.

De acuerdo con las organizaciones, los principales obstáculos que ha enfrentado la Ley 1448 para su implementación, tienen que ver con la heterogeneidad en la ocupación de los predios solicitados para restitución, pues se han identificado cadenas de ocupantes, tanto en condición de vulnerabilidad como de invulnerabilidad, y de allí parte la necesidad de que ante los jueces y magistrados ellos **demuestren que su predio fue adquirido de buena fe y con exención de culpa**.

Otro de los elementos que han dificultado la implementación de la Ley están relacionados con el atraso catastral de por lo menos 200 años que enfrenta Colombia, a través del cual se ha profundizado la **informalidad de la propiedad, haciendo muy complicado saber de quién es la tierra legalmente**. La falta de microfocalización de los territorios a restituir, sumada a la inversión presupuestal en la rama judicial que se encarga de estos procesos, y la desarticulación de las tres ramas del poder público, han representado serios [[obstáculos para la restitución](https://archivo.contagioradio.com/?s=restitucion+de+tierras+)].

Ante estas limitaciones y dificultades, la Red de Observatorios propone la realización de un catastro nacional multipropósito, que permita superar las falencias en informalidad de la propiedad territorial; **mayor inversión presupuestal para aumentar la cantidad de jueces y magistrados en los procesos jurídicos de restitución**; mayor coordinación entre las instituciones del orden nacional y territorial, con el fin de que se reconstruya la ciudadanía de las víctimas, empezando por la garantía de sus derechos a la propiedad territorial.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 

 
