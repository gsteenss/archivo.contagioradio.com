Title: Fernando Quintero Mena y la paz que no llegó a Catatumbo
Date: 2020-01-27 10:01
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: acuerdo de paz, Fernando Quintero, Lider social
Slug: fernando-quintero-mena-y-la-paz-que-no-llego-a-catatumbo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/01/Fernando-Quintero.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:audio -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/junior-maldonado-integrante-ascamcat-sobre-situacion-en_md_47010514_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Junior Maldonado | ASCAMCAT

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:paragraph -->

El domingo 27 de enero la Asociación Minga denunció el asesinato del líder social **Fernando Quintero, quien se desempeñaba como presidente de la Junta de Acción Comunal (JAC)** de la vereda Guasiles Norte en Convención, Norte de Santander. Según habitantes del territorio, el asesinato de Quintero es otra muestra de lo que ha significado la no implementación del Acuerdo de Paz.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **"A Catatumbo no llegó la paz"**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Junior Maldonado, integrante de la Asociación Campesina del Catatumbo (ASCAMCAT), Fernando Quintero era exconcejal de Convención, **un líder destacado por su actividad en el territorio** y un convencido de poder transformar la región para lograr el bienestar en las comunidades. (Le puede interesar:["Asociación campesina del Catatumbo sin garantías para participar en paro nacional"](https://archivo.contagioradio.com/asociacion-campesina-catatumbo-sin-paro-nacional/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Maldonado señaló que tras la dejación de armas y entrada en implementación del Acuerdo de Paz, en el Catatumbo se intensificó una disputa armada entre el Ejército Popular de Liberación (EPL), el Ejército de Liberación Nacional (ELN) y el Ejército. A esta confrontación se sumaron grupos de excombatientes de las FARC que regresaron a las armas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El líder manifestó que "**a Catatumbo no llegó la paz**", y por eso se sigue dando un escenario en el que los actores armados se nutren de rentas ilegales como el narcotráfico. Esta situación pone en riesgo a los liderazgos sociales, "la mayoría estamos comprometidos con la implementación del Acuerdo, creemos que hay salidas a los Planes de Desarrollo con Enfoque Territorial (PDET) y Plan Nacional Integral de Sustitución (PNIS), esto le está costando la vida", aseguró Maldonado.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Celiar Martínez, líder social desaparecido en la región**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Desde septiembre de 2019 la Asociación denunció la desaparición de Celiar Martínez, integrante de ASCAMCAT en Teorama. Caso sobre el que Maldonado dijo que estaban trabajando para lograr su liberación pero que hasta el momento no han logrado siquiera una prueba de supervivencia. (Le puede interesar: ["Líder social del Catatumbo, Celiar Martínez completa cinco días desaparecido"](https://archivo.contagioradio.com/lider-social-del-catatumbo-celiar-martinez-completa-cinco-dias-desaparecido/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/ONUHumanRights/status/1171830657217179648?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1171830657217179648\u0026ref_url=https%3A%2F%2Fnoticias.canalrcn.com%2Fnacional%2Fbuscan-lider-social-celiar-martinez-desaparecido-en-catatumbo-346954","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/ONUHumanRights/status/1171830657217179648?ref\_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E1171830657217179648&ref\_url=https%3A%2F%2Fnoticias.canalrcn.com%2Fnacional%2Fbuscan-lider-social-celiar-martinez-desaparecido-en-catatumbo-346954

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Maldonado también recordó el asesinato de Tulio Cesar Maldonado, coordinador del comité de campesinos de La Silla en Tibú; hechos que revelan los riesgos que enfrenta el liderazgo social en Catatumbo y que para los integrantes de ASCAMCAT **revela la sistematicidad en los homicidios. ** (Le puede interesar: " ["En Catatumbo cambiaron los armados, pero continúa la guerra"](https://archivo.contagioradio.com/en-catatumbo-cambiaron-los-armados-pero-continua-la-guerra/) )

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78980} /-->
