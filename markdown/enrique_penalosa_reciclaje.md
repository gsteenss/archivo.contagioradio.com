Title: Administración Peñalosa habría disminuido índices de reciclaje de un 15% a un 3% en 2016
Date: 2017-08-17 16:49
Category: Ambiente, Nacional
Tags: Bogotá, Doña Juana, Enrique Peñalosa, reciclaje
Slug: enrique_penalosa_reciclaje
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/Relleno-Doña-Juana-1200x630-e1503006517453.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: [Correo Confidencial] 

###### [17 Ago 2017] 

No solo respecto al tema de los cerros, la Reserva Thomas van Der Hammen, o la ALO, se rajaría el alcalde Enrique Pañalosa con los ambientalistas, también lo estaría haciendo ahora con los resultados respecto a los bajos niveles de reciclaje que evidencia un reciente informe de Calidad de Vida elaborado por **'Bogotá Cómo Vamos', en el que se revela que la ciudad redujo de un 15% a un 3% dicha actividad.**

"Los reportes para el porcentaje de aprovechamiento de los residuos sólidos muestran un aumento desde el año 2012 hasta el año 2014. En el año 2015 se mantiene el porcentaje por encima del 15% pero **para el año 2016 el porcentaje disminuye drásticamente hasta 3%"**, dice el informe. Sin embargo, tras hacerse pública esa información la alcaldía, y concretamente la Secretaría del Hábitat señaló que dicho número es erróneo, y que incluso durante el primer año del alcalde se logró recuperar el 14% de los residuos.

![reciclaje](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/reciclaje.png){.alignnone .size-full .wp-image-45456 width="675" height="274"}

"La cifra usada en el informe **no tiene en cuenta el trabajo de los 21.000 recicladores que existen en la ciudad,** los cuales han seguido haciendo su labor durante nuestra administración”, aseguró Eduardo Ojeda, subdirector de servicios públicos de la Secretaría del Hábitat, de hecho, añadió que la administración distrital se ha planteado una meta final del  20% de residuos reciclados.

No obstante, aunque la alcaldía señale que se trata de una cifra equivocada, por no tener en cuenta el trabajo de los recicladores, que también hace parte de las anteriores alcaldías, podría pensarse, que algún déficit si podría haber en esa materia, pues se compara con los datos de años anteriores.

Ante la advertencia que hace el informe, recomienda "aumentar el porcentaje de aprovechamiento de residuos sólidos, ya que ha caído considerablemente".

### Relleno Doña Juana 

La baja nota que obtiene la alcaldía respecto al reciclaje, se publica precisamente en medio del debate alrededor del relleno 'Doña Juana', en donde actualmente los vecinos de la zona exigen respuestas al problema de salubridad. La situación para la alcladía se complica, porque pretende alargar la vida del vertedero hasta el 2022, pero, **de ser ciertas las cifras del documento, esto no sería posible, si en lugar de disminuir los desechos que llegan al botadero más bien estarían aumentando, pues no se estaría reciclando.** [(Le puede interesar: Habitantes del sur de Bogotá protestan contra relleno Doña Juana)](https://archivo.contagioradio.com/habitantes-de-mas-de-100-barrios-del-sur-de-bogota-protestan-contra-relleno-sanitario-dona-juana/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
