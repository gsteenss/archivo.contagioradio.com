Title: En zona de Hidroituango habría más de 2 mil personas desaparecidas
Date: 2018-01-23 15:54
Category: DDHH, Nacional
Tags: Antioquia, Cañón del Río Cauca, Desaparición forzada, Hidroituango, personas desaparecidas
Slug: en-el-canon-del-rio-cauca-afectado-por-hidroituango-hay-mas-de-2-mil-personas-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/hidroituango-e1516739322374.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Ene 2018] 

La Misión de observación y verificación sobre las condiciones de búsqueda de personas desaparecidas a lo largo del Cañón del Río Cauca en Antioquia, constató que hay más de 2 mil personas desaparecidas por lo que el proyecto violaría los derechos de las víctimas y de la sociedad en general por conocer la verdad y por ello sería necesario un proceso de búsqueda antes de adelantar la siguiente fase del proyecto de EPM.

La Misión presentó su informe tras la observación de la situación que atañe a la **construcción de la hidroeléctrica Hidroituango** que prevé la inundación de 79 KM a lo largo del Cañón del Río Cauca.

De acuerdo con las cifras de la Unidad para la Atención y Reparación Integral de Víctimas, en los 12 municipios antioqueños que afecta el proyecto hidroeléctrico, se han registrado **2.238 víctimas de desaparición forzada hasta 2017**. Además, han sido exhumadas entre 2014 y 2018, 159 cuerpos de los cuales sólo se han identificado 84.

De acuerdo con Verónica Heredia, integrante del grupo que viajó al territorio y abogada argentina, **“la desaparición forzada es el peor de los crímenes** porque todos los días la persona está desaparecida y todos los días los familiares buscan a esa persona”. Indicó que en Colombia este fenómeno está presente desde hace varias décadas y se desconoce la magnitud real del fenómeno debido a las discrepancias de la información.

### **¿Qué sucede con el Hidroituango en el Cañón del Río Cauca?** 

A través de diferentes investigaciones, Contagio Radio ha documentado el problema que afrontan miles de víctimas de los **12 municipios que se ven afectados por la hidroeléctrica** que está en construcción. Los familiares de las víctimas desaparecidas argumentan que, durante el conflicto armado, “las personas eran asesinadas y lanzadas al río para que se perdieran”. (Le puede interesar:["Memoria y resistencia en el Cañón del Río Cauca"](https://archivo.contagioradio.com/memoria-y-resistencia-en-el-canon-del-rio-cauca/))

Por esto, han declarado el Río Cauca como un cementerio y los campesinos y barequeros aseguran que han **recogido cuerpos en el río** por lo que los han enterrado en diferentes partes del bosque geo localizándolos por la ubicación de los árboles. Sin embargo, como parte del proyecto de construcción, la empresa EMP ha venido realizando actividades de tala por lo que se pierde la oportunidad de encontrar los cuerpos enterrados.

Esto ha sido denunciado en repetidas ocasiones por el Movimiento Ríos Vivos que está compuesto en gran medida por **víctimas del conflicto armado.** Una de sus representantes, Isabel Cristina Zuleta, ha manifestado que la empresa ha re victimizado a estas personas que además han sufrido amenazas contra su vida por la labor que realizan.

### **Observaciones de la Misión** 

En el marco de la visita que se realizó al territorio, los integrantes de la Misión evidenciaron que **“hay un atropello e irrespeto hacia las víctimas** debido a los procedimientos de búsqueda adelantados en la región”. Esto teniendo en cuenta que es una labor que debe realizar el Estado y no la empresa y se deben respetar e incluir a las comunidades y sus tradiciones culturales.

Además, rectificaron que entre 1990 y el 2000 **“hubo un tránsito diario de cuerpos en diferentes sectores del río Cauca”** y eran personas que fallecieron de manera violenta. Las personas “por motivos espirituales y éticos inhumaron los cuerpos y siempre lo hicieron en la ribera”. También observaron que hay un temor por denunciar estos casos teniendo en cuenta que persiste el conflicto armado en esta región.

Una preocupación grande está relacionada con el hecho de que **hay un acuerdo entre la Fiscalía General de la Nación y la empresa EPM** para recolectar la información sobre la presencia de cuerpos sin identificar en la zona de la inundación del proyecto donde la Fiscalía ha realizado 159 exhumaciones. (Le puede interesar:"[62 masacres en los 12 municipios donde se desarrolla el proyecto Hidroituango](https://archivo.contagioradio.com/62-masacres-los-12-municipios-donde-se-desarrolla-proyecto-hidroituango/)")

Les preocupa que la empresa tenga la competencia de **concertar la exhumación y trasladar los cuerpos** presentes en cementerios de la zona a ser inundada pues se ha convocado a una licitación con diferentes entidades para realizar esta labor y no se ha tenido en cuenta a las comunidades y las familias desconocen lo que sucede con los cuerpos de sus familiares.

### **Recomendaciones hechas por la Misión** 

Como ya lo ha realizado el Movimiento Ríos Vivos, le solicitaron al Estado Colombiano que **“despliegue las medidas de prevención y protección** de los territorios que se requieran para garantizar el derecho a la verdad individual y colectiva”. Además, le pidieron que realice la documentación, investigación y dignificación de las víctimas por las graves violaciones a los derechos humanos.

A la sociedad colombiana le pidieron que se **solidarice con la situación que padecen las víctimas de desaparición forzada** en el departamento de Antioquia en la medida que es necesario, para la consecución de la paz, saber la verdad de los hechos sucedidos. Afirmaron que la movilización social es vital para garantizar los derechos de no repetición.

Finalmente le hicieron un llamado a la empresa EPM para que se abstenga de desarrollar actividades que **“afecten la memoria, la verdad y la justicia** a través de la recuperación, identificación y entrega de cuerpos inhumanos en la ribera del río Cauca o que puedan afectar la investigación, juzgamiento y sanción de hechos violatorios de los derechos humanos”.

<iframe id="audio_23318146" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23318146_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
