Title: Primer laboratorio para fortalecer el audiovisual en Bogotá
Date: 2015-10-15 15:24
Author: AdminContagio
Category: 24 Cuadros
Tags: Cinemateca al parque, Cinemateca Distrital, Cinemateca rodante, Cinematecalab, David Zapata asesor Cinemateca, IDARTES
Slug: primer-laboratorio-para-fortalecer-el-audiovisual-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/cinemateca-e1444940760658.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_8984739_2_1.html?data=mZ6llpyXfY6ZmKiakpuJd6Kok5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8rixtLO1srHpc3Vw4qfpZDZssKfytPWxc7FuMrqwpDdw9fFb9Hj1crbxc7FtozZzZDO18nNs9fdjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [David Zapata, Asesor Cinemateca Distrital] 

###### [15 Oct 2015]

En busca de modelar y fortalecer los proyectos de creación, circulación y formación audiovisual en la ciudad de Bogotá, la Cinemateca Distrital, Gerencia de artes visuales de IDARTES, tiene abiertas las inscripciones para "**Cinemateca Lab" 2015**.

David Zapata, Asesor de Formación y Convocatorias de la Gerencia de Artes Visuales, asegura que la motivación para proponer este nuevo espacio surge de la necesidad de "**fortalecer la formulación de las propuestas** que se presentan anualmente en las convocatorias del portafolio de la Cinemateca Distrital".

Entre el 17 y el 21 de Noviembre, expertos en diversos campos y áreas de la producción audiovisual, acompañarán y asesorarán los proyectos presentados por personas y colectivos de ciudadanos, en las **tres modalidades** estipuladas por la organización de la convocatoria.

Para la modalidad de **creación**, 12 serán los proyectos seleccionados repartidos así: 3 cupos para animación, 3 para documental, 3 para argumental y 3 para tres para relatos que utilicen plataformas transmediales. 4 proyectos de **formación** audiovisual para creadores y 8 proyectos de **circulación** repartidos por mitad entre Festivales, cineclubes y salas asociadas.

La meta es que aquellas personas vinculadas al programa "**Cinemateca Rodante**" o que tengan la intención de presentarse a las convocatorias que integran el portafolio para 2016, puedan inscribirse al laboratorio. Zapata indica que lo ideal sería que los proyectos participantes "**se encuentren en una fase de formulación o desarrollo**".

Los interesados en participar deberán diligenciar antes del 20 de octubre, el formulario de pre-inscripción correspondiente a las modalidades en las que desee participar, en el cual debe especificar algunas de las **características que componen su proyecto**, valoraciones fundamentales para la evaluación a realizar por parte del equipo seleccionador.

Para conocer las bases detalladas, así como los formatos de inscripción, le invitamos a ingresar en el siguiente vínculo [Cinemateca Lab](http://www.idartes.gov.co/attachments/article/4268/Cinemateca%20Lab-Convocatoria%202015.pdf) , recordando que la participación en la convocatoria es totalmente gratuita.
