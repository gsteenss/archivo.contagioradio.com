Title: Plan Zonal del Norte afectaría gravemente Franjas de Conectividad
Date: 2017-02-21 14:48
Category: Ambiente, Nacional
Tags: Alcaldía Enrique Peñalosa, María Mercedes Maldonado, Plan Zonal del Norte, Reserva Thomas Van der Hammen
Slug: plan-zonal-del-norte-afectaria-gravemente-franjas-de-conectividad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Torcahumedal.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Noticias UN] 

###### [21 Feb 2017] 

La conectividad entre los Cerros Orientales, los humedales de Torca y Gaymaral, la Reserva Thomas van der Hammen y el Río Bogotá, se vería gravemente afectada de llevarse a cabo el decreto de ampliación de la capital del país, **llamado Plan Zonal del Norte, que incluye nuevas autopistas y proyectos de urbanización como Lagos de Torca,** impulsado por la alcaldía de Enrique Peñalosa.

María Mercedes Maldonado abogada y defensora ambiental, advirtió que si bien el proyecto de urbanización no es directamente en la Van der Hammen, está en la franja de conectividad, restauración y protección, **dispuesta de tal forma por el Ministerio de Ambiente mediante la resolución 475 del año 2000.** ([Le puede interesar: Enrique Peñalosa desconoce reservas en los Cerros Orientales](https://archivo.contagioradio.com/enrique-penalosa-sigue-desconociendo-reservas-forestales-en-los-cerros-orientales/))

### ¿Por qué el Plan Zonal del Norte? 

La abogada señala que hay una cierta intencionalidad de Peñalosa en presionar la aprobación del Plan Zonal pues así **“toma un atajo para evadir todos los tramites que debe hacer”, normas de obligatorio cumplimiento y de esa forma afecta la conectividad** ecológica y funcional entre ecosistemas “para después decir que la Van der Hammen no sirve para nada”.

Maldonado indica que el Plan Zonal “se llevaría todo el Norte”, pues esta zona de la ciudad esta dividida en zona de expansión urbana, con unas 2000 ha, zona de reserva con 1395 ha y una zona rural que cuenta con 1700ha aproximadamente, explica que para que la alcaldía Peñalosa “cumpla con su meta de las 1800 ha, pues va a urbanizar todas esas zonas”.

La abogada, manifiesta que **“no están obedeciendo las resoluciones que siguen vigentes y son obligatorias” en las áreas de protección de los humedales** y en un área de conectividad como lo es la de Torca- Guaymaral y el cerro de La Conejera, lo que podría generar cambios irreversibles en los ecosistemas a causa de “un alcalde que se salta las leyes y la conservación ambiental”.

### ¿Quedan alternativas? 

Las organizaciones ambientales y las comunidades que habitan en estas zonas, podrían tener un chance para frenar este proyecto, la abogada resalta que las **veedurías ciudadanas, la movilización en redes y calles y acciones dentro del derecho constitucional** como las acciones ciudadanas, acciones de cumplimiento y acciones populares, son “buenas oportunidades”.

Por último, la abogada explica que algunas acciones como las acciones de nulidad, demoran varios años, pero “la acción de cumplimiento es similar a la tutela, podemos exigirle a un juez, por muchas razones clarísimas de incumplimiento, **que ordene echar atrás la decisión arbitraria que esta tomando Peñalosa”.** ([Le puede interesar: Peñalosa obligado a responder por urbanización en Reserva van der Hammen](https://archivo.contagioradio.com/penalosa-van-der-hammen/))

<iframe id="audio_17143136" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17143136_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
