Title: Documentales sobre el conflicto armado abren la VIII Semana de la Memoria
Date: 2015-09-23 12:14
Author: AdminContagio
Category: 24 Cuadros, Sin Olvido
Tags: Cantos de Alabaos, Centro Nacional de Memoria Histórica, Cinemateca Distrital, Graffitour, informe “¡Basta ya! Colombia. Memorias de guerra y dignidad”, masacre de El Salado, Masacre de Trujillo, Memorias del conflicto armado en Colombia, Muestra documental memoria, orregimiento de El Placer, Parque Monumento, Salón del Nunca Más, VIII Semana de la memorí
Slug: documentales-sobre-el-conflito-armado-abren-la-viii-semana-de-la-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/salado1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotograma: "Salado: Rostros de una masacre"] 

###### [23 Sept 2015] 

Como preámbulo de la "**VIII Semana de la memoría**", el Centro Nacional de Memoria Histórica, en asocio con la Cinemateca Distrital de Bogotá, presentan los días 24, 25, 26 y 27 de septiembre la Muestra/Foro "**Memorias del conflicto armado en Colombia**".

"No hubo tiempo para la tristeza", de Mario Betancour; "El Salado: Rostros de una Masacre", dirigido por Tony Rubio; "Trujillo: una tragedia que no cesa", de Freddy Gusgüen Perilla; "Rostros de las memorias", de Camilo Pérez y "El placer: mujeres tras las huellas de la memoria" de María Libertad Márquez, son las producciones que componen la muestra.

Los documentales se proyectarán a las 9:00 de la mañana hasta las 12 m en la sede de la Cinemateca Distrital Cra. 7 \#22-79, Bogotá, con entrada libre previa inscripción  en www.centrodememoriahistorica.gov.co/museo/, hasta completar el aforo.

**"Hubo un tiempo para la tristeza" (Dir. Jorge Mario Betancur)**

[![no hubo tiempo para la tristeza](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/no-hubo-tiempo-para-la-tristeza.jpg){.wp-image-14488 .alignright width="352" height="198"}](https://archivo.contagioradio.com/documentales-sobre-el-conflito-armado-abren-la-viii-semana-de-la-memoria/no-hubo-tiempo-para-la-tristeza/)

Un relato construído a partir del informe **“¡Basta ya! Colombia. Memorias de guerra y dignidad”**, elaborado por el Centro Nacional de Memoria Histórica, que busca responder el por qué el país ha sido escenario de un conflicto armado durante más de 50 años y cómo los ciudadanos han sobrevivido a este largo periodo de violencia.

El documental incluye testimonios de hombres y mujeres que desde La Chorrera, Bojayá, San Carlos, las orillas del río Carare, Valle Encantado y Medellín dicen que Colombia no puede permitir que la atrocidad de la que ellos fueron testigos se repita.

[**Septiembre 24, Hora 9:00 a.m**]

**"Rostros de las memorias" (Dir.  Camilo Pérez)**

![rostros de las memorias](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/rostros-de-las-memorias.jpg){.wp-image-14492 .alignleft width="322" height="200"}Presenta algunas de las iniciativas de memoria desarrolladas por comunidades y organizaciones de víctimas en todo el país, profundizando en cuatro experiencias: **Graffitour** de la Comuna 13 en Medellín, **Cantos de Alabaos** de Bojayá, Chocó, **Parque Monumento** en Trujillo, Valle del Cauca y **Salón del Nunca Más** en** **Granada, Antioquia.

Rostros de las Memorias, recoge una muestra de la creatividad de los colombianos y las colombianas quienes relatan sus historias de resistencia a través de las artes, la cultura, los espacios y las exposiciones.

[**Septiembre 24, Hora 10:30 a.m**]

**"Salado: Rostros de una masacre" (Dir. Tony Rubio) **

![salado1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/salado1.jpg){.wp-image-14493 .alignright width="251" height="189"}Documental del Grupo de Memoria Histórica - CNRR que cuenta, mediante una serie de entrevistas con sobrevivientes y testigos, la versión de las víctimas de la **masacre de El Salado** ejecutada por paramilitares bajo el mando de Salvatore Mancuso y Rodrigo Tovar Pupo, alias Jorge 40, en febrero de 2000.

[**Septiembre 25, Hora 9:00 a.m**]

**"Trujillo; Una tragedia que no cesa" (Dir. Freddy Gusgüen Perilla)**

Entre 1988 y 1994, en Tru![portada\_trujillo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/portada_trujillo-e1443027181299.jpg){.alignleft .wp-image-14494 width="295" height="230"}jillo, al norte del departamento del Valle del Cauca, 342 personas fueron brutalmente torturadas, masacradas y desaparecidas. A estos hechos monstruosos en la historia reciente de Colombia se les conoce como: . Aunque el Estado reconoció su responsabilidad en los hechos, cientos de familias aún hoy esperan conocer la verdad y que el gobierno los repare por años de violencia y sangre.

[**Septiembre 26, Hora 9:00 a.m**]

**"El placer: Mujeres tras las huellas de la memoria" (Dir. María Libertad Márquez )**

**![mujeres tras las huellas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/muj.jpg){.wp-image-14495 .alignright width="208" height="278"}**

Expone los efectos que tuvo el paso del terror paramilitar en la vida de los habitantes del corregimiento de **El Placer**, en el departamento del Putumayo. Un recorrido por estas dos décadas de violencia impartida por las FARC y las AUC en esta región del país, donde se revela, también, los esfuerzos de resistencia de la población, promovidos, en especial, por las mujeres, quienes aún hoy persisten en un profundo anhelo por hacer memoria y romper con los estigmas que han marcado a su pueblo.

[**Septiembre 27, Hora 9:00 a.m**]
