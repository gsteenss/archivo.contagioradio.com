Title: Indígenas Wounaan exigen garantías para retornar a sus tierras
Date: 2017-02-16 18:08
Category: DDHH, Nacional
Tags: Comunidad Wounaan desplazada en Buenaventura, Paramilitarismo
Slug: entrevista-indigenas-nonam
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/9493540445_aa634e5ba4_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [16 Feb 2017] 

Las 31 familias indígenas Wounaan desplazadas por operaciones paramilitares del Bajo Calima, en Valle del Cauca **le han expresado a la Fiscalía General de la Nación, a la Defesoria del Pueblo y Fuerzas Militares, que la única forma de reto**rnar a sus territorios es que se garantice la vida de los indígenas y del disfrute de su territorio.

Durante reuniones que han sostenido los indígenas Wounaan con estas instituciones, **han denunciado que la presencia de paramilitares se ve en veredas como el Carra, en Valle del Cauca en donde hay un punto de control de la Marina a 15 minutos**, sin que exista alguna acción por parte de las autoridades.

A su vez, le están exigiendo a las autoridades que **conformen lo más pronto posible, una comisión de verificación, con acompañamiento de organizaciones internacionales,** que viaje a la región para constatar la situación de violación de derechos humanos hacia las comunidades.  Le puede interesar: ["31 familias Wounann son desplazadas por paramilitares en el Bajo Calima" ](https://archivo.contagioradio.com/31-familias-wounaan-desplazadas-paramilitares-calima/)

Igualmente le **piden a la Fiscalía que de prontos resultados sobre investigación y capturas a los responsables de las torturas al  indígena José Cely Chamapurro**, de la comunidad el 4 de febrero de este año y una atención digna en alimentación y condiciones para las 31 familias que llegaron al Puerto de Buenaventura. Le puede interesar: ["Paramilitares torturaron a indígena en el Bajo Calima"](https://archivo.contagioradio.com/paramilitares-torturaron-a-indigena/)

<iframe id="audio_17060008" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17060008_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
