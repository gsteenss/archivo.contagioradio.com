Title: Amerisur pone en riesgo la pervivencia del pueblo ZioBain
Date: 2016-11-22 15:57
Category: Movilización, Nacional
Tags: Comunidad Indígena Siona, despojo de tierras, empresas extractivas
Slug: amerisur-pone-riesgo-la-pervivencia-del-pueblo-ziobain
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/siona1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ElCampesino.co] 

###### [22 Nov 2016] 

Autoridades de del pueblo ZioBain del Resguardo Indígena Buenavista en el Putumayo, se trasladaron a la capital para **denunciar la grave situación de vulnerabilidad en que se encuentran** a causa de la amenaza que representa el ingreso al territorio de la empresa Amerisur.

La situación es tan grave que en pocos años la comunidad ZioBain ha reducido el número de sus integrantes a la mitad. Según el gobernador Mario Erazo, el pueblo hace **10 años la comunidad contaba con cerca de 5000 integrantes, hoy solo son 2400** porque la colonización que vino con la guerra y la presencia de la petrolera los "está acabando".

Dicha multinacional **pretende adquirir el bloque Putumayo 12 para desarrollar extracción de petróleo con sísmica**, a pesar que en consulta previa del año 2014 la comunidad dio un rotundo no al proyecto.

### **¿Cuáles son las denuncias de la comunidad?** 

Las autoridades indígenas señalan en un comunicado que el ingreso de la multinacional junto al incremento de presencia y accionar militar en la zona, significa el exterminio físico y cultural de la comunidad. Manifiestan que las **fuerzas militares han hecho disparos de granadas y roquets de mortero, ráfagas desde helicópteros, hostigamientos continuos** por medio de detenciones, registros fotográficos, requisas a moradores del resguardo, robo de animales y alimentos e ingreso a domicilios.

Dicho accionar ha **impedido el desarrollo pleno de actividades propias de subsistencia de la comunidad** como lo es la caza, la pesca, la recolección de alimentos y medicina en chagras y la libre movilización por tierra y río entre comunidades vecinas. También se han presentado situaciones de **desplazamiento forzado de familias del resguardo que hasta la fecha no han retornado.**

Además, las comunidades resaltan que el conjunto de estos hechos han afectado la realización de actividades ceremoniales **“que son el sostén espiritual, de gobernanza y direccionamiento del Pueblo ZioBain”**.

Los hostigamientos y sobrevuelos nocturnos interrumpen las ceremonias de toma de yagé y “originaron que la población tuviera miedo de asistir a las mismas; generando una **desarmonización en la comunidad, incremento de enfermedades, conflictos intracomunitarios, pérdida de protección del territorio y la disminución de actividades de cacería y pesca”.**

**“Nuestra espiritualidad es nuestra vida, y cuando la afectan nos dañan la manera propia de vivir y de pensar en armonía** con el contexto de nuestro Mai yija, Nuestro territorio y Mai rotaye, Nuestro pensamiento”.

Otra de las denuncias tiene que ver con la **contaminación de las fuentes de agua por un derrame de crudo en el año 2011 y otro en el presente año,** lo cual afectó una quebrada ubicada en la vereda La Rosa que desemboca al rio Putumayo, vulnerando los derechos a la vida, la salud y alimentación.

En el 2014 la comunidad del Resguardo Buenavista realizó un proceso de consulta previa para el programa de adquisición sísmica 2D dentro del bloque Putumayo 12, el cual concluyó sin acuerdos entre las partes y con **la determinación por parte de la comunidad de no permitir el desarrollo del programa.**

### **La respuesta de la empresa** 

A través de Maribel Escobar relacionadora comunitaria y Marco Tulio González jefe  de Relaciones Institucionales, la multinacional ha **asediado vía telefónica al Gobernador Mario Erazo insistiendo en que debe reunirse con la empresa.**

En el comunicado, las autoridades señalan que el día 9 de Noviembre Amerisur remitió una carta al Gobernador para solicitar una reunión de información de inicio de actividades del proyecto de adquisición sísmica en el bloque, **“lo que evidencia una continua presión sobre la determinación comunitaria y un nuevo intento por doblegar las decisiones del resguardo”.**

De igual forma, desde febrero de 2015 hasta la fecha funcionarios de la empresa han insistido en hacer contacto con la comunidad para lograr su consentimiento; desde el mes de agosto de 2016 han intentado acercamientos a la comunidad a través de Edwin Bravo, promotor de asuntos indígenas del Municipio de Puerto Asís, **buscando la coordinación de reuniones entre los Gobernadores de Piñuña Blanco, Buenavista y cabildo Bajo Santa Elena y Amerisur.**

### **¿Qué exigen las comunidades?** 

En el mismo comunicado las autoridades hacen hincapié en que el Estado Colombiano debe dar **cumplimiento al auto 004-09 de la Corte Constitucional**, el cual dispone la protección de derechos fundamentales de campesinos, afro e indígenas desplazados por el conflicto armado, **acate el plan de salvaguarda Siona y en general haga efectivas sus obligaciones constitucionales** para garantizar el respeto a los derechos humanos y protección de la vida en este territorio colectivo y sagrado.

Exigen también una **respuesta oportuna por parte de la dirección de consulta previa del Ministerio del Interior y la Defensoría del Pueblo**, han solicitado de manera urgente soluciones frente a la situación de militarización y desabastecimiento, sin tener respuestas concretas a la fecha.

“Requerimos que la primera acción sea garantizar que **ni la empresa Amerisur ni ninguna otra empresa desarrollen el programa de adquisición sísmica 2D dentro del bloque PUT–12**, ni ninguna otra actividad de exploración o explotación de recursos no renovables”.

Por ultimo hacen un llamado a la comunidad nacional e internacional, a los medios de comunicación y a las organizaciones de derechos humanos “para que **acompañen, se solidaricen y realicen seguimiento de los hechos que denunciamos, a que dirijan comunicaciones de preocupación por los hechos aquí denunciados** y por aquellos que sigan surgiendo en la medida que se pueda seguir recaudando y difundiendo mayor información”.

[A004-09](https://www.scribd.com/doc/55225670/A004-09-CONLICTO-ARMADO#from_embed "View A004-09 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_54187" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/331985798/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-CEsPC0Fn2RdPNlB0AhZ5&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.6545718432510885"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU) {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .p1}
