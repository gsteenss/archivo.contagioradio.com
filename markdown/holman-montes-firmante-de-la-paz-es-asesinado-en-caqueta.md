Title: Holman Montes, firmante de la paz es asesinado en Caquetá
Date: 2020-02-28 12:23
Author: AdminContagio
Category: DDHH, Nacional
Tags: Caquetá, FARC, San Vicente del Caguán
Slug: holman-montes-firmante-de-la-paz-es-asesinado-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/FARC-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: FARC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El Partido Fuerza Alternativa Revolucionaria del Común (FARC) denunció el asesinato del excombatiente Holman Fabio Montes Sánchez de 38 años de edad en la vereda el Sinaí, en el municipio de San Vicente del Caguán, Caquetá. Tanto el partido como organizaciones de DD.HH coinciden en que **los asesinatos contra firmantes de la paz desde la firma del acuerdo ascienden a más de 187 casos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Según información compartida por [Rodrigo Londoño](https://twitter.com/TimoFARC/status/1233391650350780416), presidente de la colectividad, el excombatiente fue asesinado con arma de fuego. En lo que van del 2020 son 14 los reincorporados asesinados. [(Le puede interesar: Garantías de vida y seguridad para excombatientes se quedaron en el papel)](https://archivo.contagioradio.com/garantias-de-vida-y-seguridad-para-excombatientes-se-quedaron-en-el-papel/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo...

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
