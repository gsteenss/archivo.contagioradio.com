Title: "La batalla de Chile" el antes, durante y después del 11 de Septiembre de 1973
Date: 2015-09-11 15:47
Author: AdminContagio
Category: 24 Cuadros, DDHH
Tags: Chris Marker cineasta francés, Documentales golpe de Estado en Chile, Golpe de estado en Chile 1973, La batalla de Chile Documental, Patricio Guzmán Documentalista, Salvador Allende
Slug: la-batalla-de-chile-el-antes-durante-y-despues-del-11-de-septiembre-de-1973
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/salvador.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

"La insurrección de la burguesía", "El golpe de Estado" y "El poder popular", trilogía documental que compone "L**a batalla de Chile**", resulta ser tal vez el registro fílmico más completo y contundente de la convulción social y política vivida en el país Austral entre los años 1970 y 1973, período que comprende el gobierno de la Unidad Popular en cabeza del inmolado líder Salvador Allende.

Reutilizando algunos conceptos e imágenes de sus producciones "El primer año" (1972) y "La respuesta de Octubre" (1973), Patricio Gúzman, director y guionista de los documentales, logró capturar casi de manera de noticiosa a blanco y negro, escenas que aportan a la construcción de la memoria histórica chilena.

La colaboración activa del cineasta francés Chris Marker, quien se unió a Guzmán luego de ver su trabajo en "El primer año", fue clave en la realización y finalización de las producciones, sumandose al equipo conformado por el camarógrafo Jorge Müller, el sonidista Bernardo Menz, el ayudante de dirección José Bartolomé, el productor Federico Elton y el montajista Pedro Chaskel.

El trabajo requerido para que "La batalla de Chile" saliera a la luz no fue para nada sencillo. Filmados en 16mm en las calles de Santiago y editados en una pequeña sala de montaje del Instituto Cubano de Artes e Industria Cinematográfico, fueron estrenados a medida que cada uno fue terminado entre los años 1975 y 1979.

Tras el golpe de estado, el destino de varios integrantes del equipo de producción estuvo marcado por el exilio: Elton y Bartolomé debieron salir del país, al igual que Menz y Chaskel, quienes salvaguardaron el material filmico enviandolo a Cuba, mientras que el camarógrafo Jorge Müller fue secuestrado por la DINA en noviembre del 74 y es hasta hoy una de los miles de desaparecidos por el régimen militar.

La gran acogida demostrada por el público en más de 35 países de Europa, América, Asia y Australia, con reconocimientos internacionales como los recibidos en los Festivales de Grenoble, Bruselas, Benalmádena, La Habana y Leipzig; contrasta con la poca trasendencia que ha tenido en su país, donde la censura no ha permitido que "La batalla de Chile" halla sido ampliamente difundida.

**“La Insurrección de la Burguesía”**

<iframe src="https://www.youtube.com/embed/ldfQmR9Wmdk?list=PLt45OMvRJBd3WKgb-MIG7z1uN-x0JZLRo" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

#### **Sinopsis:** Salvador Allende pone en marcha un programa de profundas transformaciones sociales y políticas. Desde el primer día la derecha organiza contra él una serie de huelgas salvajes mientras la Casa Blanca le asfixia económicamente. A pesar del boicot --en marzo de 1973-- los partidos que apoyan a Allende obtienen el 43,4 por ciento de los votos. La derecha comprende que los mecanismos legales ya no les sirven. De ahora en adelante su estrategia será la estrategia del golpe de estado. “La Batalla de Chile” es un fresco que muestra paso a paso estos hechos que conmovieron al mundo. 

 

**“El golpe de Estado”**

<iframe src="https://www.youtube.com/embed/W-1LZ9L6dhQ?list=PLt45OMvRJBd3WKgb-MIG7z1uN-x0JZLRo" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

 

#### **Sinopsis:** Entre marzo y septiembre de 1973 la izquierda y derecha se enfrentan en la calle, en las fábricas, en los tribunales, en las universidades, en el parlamento y en los medios de comunicación. La situación se vuelve insostenible. Estados Unidos financia la huelga de los camioneros y fomenta el caos social. Allende trata de llegar a un acuerdo con las fuerzas de la Democracia Cristiana, sin conseguirlo. Las propias contradicciones de la izquierda aumentan la crisis. Los militares empiezan a conspirar en Valparaíso. Un amplio sector de la clase media apoya el boicot y la guerra civil. El ll de septiembre Pinochet bombardea el palacio de gobierno. 

 

**“El Poder Popular”**

<iframe src="https://www.youtube.com/embed/JQ5nbu1zuPU?list=PLt45OMvRJBd3WKgb-MIG7z1uN-x0JZLRo" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

 

#### Al margen de los grandes acontecimientos que narran los filmes precedentes ocurren también otros fenómenos originales, a veces efímeros, incompletos, que recoge la tercera parte. Numerosos sectores de la población y en particular las capas populares que apoyan a Allende organizan y ponen en marcha una serie de acciones colectivas: almacenes comunitarios, cordones industriales, comités campesinos, etc., con la intención de neutralizar el caos y superar la crisis. Estas instituciones, en su mayoría espontáneas, representan un “estado” adentro del Estado. 

 
