Title: OtraMirada: ¿Qué implica la restitución de derechos por violación a mujeres indígenas?
Date: 2020-07-01 23:14
Author: AdminContagio
Category: Otra Mirada, Otra Mirada, Programas
Tags: Abuso sexual por Ejército, Comunidades Indígena Embera, Restitución de derechos, Violaciones a mujeres indígenas, violaciones sexuales Colombia
Slug: otramirada-que-implica-la-restitucion-de-derechos-por-violacion-a-mujeres-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/siona.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Indígenas Siona

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Tras haberse conocido el caso de violación de niña Embera por ocho integrantes del Ejército Nacional y de las posteriores declaraciones por parte de las instituciones del Estado, empezaron a surgir preguntas a nivel nacional ¿qué está pasando en los territorios con las mujeres indígenas y cómo está funcionando la restitución de los derechos de las víctimas indígenas en el país? (Le puede interesar: [Agresion sexual del ejercito contra menor embera es un ataque a los 115 pueblos indígenas del pais: ONIC](https://archivo.contagioradio.com/agresion-sexual-del-ejercito-contra-menor-embera-es-un-ataque-a-los-115-pueblos-indigenas-del-pais-onic/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para dar respuesta a estas preguntas y entender los mecanismos que las comunidades indígenas requieren para restituir eficazmente los derechos de las mujeres indígenas invitamos a Milena Margoth Mazabel, indígena del pueblo Kokonu (Abogada y defensora de DDDHH), Dora Saldarriaga Concejala de Estamos listas y docente investigadora en derecho constitucional, Fany Kuiru Castro coordinadora de Mujer, Niñez y Familia de la Organización Nacional Indígena de los Pueblos Indígenas de la Amazonia Colombiana - OPIAC y por último Nazareth Cabrera, Sabedora indígena y lideresa Araracuara (Florencia, Caquetá).

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Así es la atención diferencial que aplican comunidades indígenas a las mujeres

<!-- /wp:heading -->

<!-- wp:paragraph -->

De igual manera, este espacio permitió conocer la forma en que las instituciones han hecho el abordaje y cuáles son los procedimientos y trámites respecto a los casos de violación y abuso sexual. También, qué se está llevando a cabo desde las organizaciones para mejorar la efectividad de las rutas de denuncia con el fin de que estas aumenten y que hayan garantías para las victimas. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otro lado, ya que la sociedad colombiana ha mostrado gran indignación, nuestras invitadas expresan **que si se puede hacer a la sociedad participe de estos procesos, sin vulnerar a las comunidades. Además, explican qué el daño se ve reflejado en la comunidades, y desde el conocimiento tradicional realizan el tratamiento para apoyar emocionalmente tanto a la víctima, como al agresor, a la familia y a su comunidad. **

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ultimo, este también es un llamado al Estado y a las autoridades para generar mecanismos que no solo garanticen un proceso de acompañamiento en caso de abusos sexuales sino también la protección de las niñas y mujeres para que estos hechos no sigan ocurriendo.  (Si desea escuchar el programa del viernes 26 de junio: [Resistimosconorgullo](https://bit.ly/2BSHCWA))

<!-- /wp:paragraph -->

<!-- wp:html /-->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
