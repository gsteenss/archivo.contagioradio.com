Title: Delegación de paz del ELN rechaza captura de Juan Carlos Cuellar, Gestor de paz
Date: 2019-12-30 13:51
Author: CtgAdm
Category: Paz, Política
Tags: ELN, paz
Slug: delegacion-de-paz-del-eln-rechaza-captura-de-juan-carlos-cuellar-gestor-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/comunicado-eln-gestor-de-paz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

A través de un comunicado la **Delegación de Paz del** [**Ejérito de Liberación Nacional** (ELN) rechazó el hecho sucedido en la tarde del domingo en la ciudad de Cali, cuando fue detenido **Juan Carlos Cuellar,** Gestor de Paz y quien ha participado en conversaciones de paz con diferentes gobiernos entre ellos con el de Juan Manuel Santos y Duque. (Le puede interesar: ["ELN entrega tres menores, y denuncia que estos fueron reclutador por el Ejército"](https://archivo.contagioradio.com/eln-entrega-tres-menores-y-denuncia-que-estos-fueron-reclutador-por-el-ejercito/))]

De acuerdo con el comunicado **Juan Carlos Cuellar**, asumió la interlocución con la [Oficina del Comisionado de Paz y oficinas del gobierno Duque pese a la ruptura de los diálogos en el mes de enero del presente año, igualmente]ha participado en [negociaciones con los gobiernos de Álvaro Uribe, Juan Manuel Santos y con el actual gobierno desde agosto de 2018.]

["Él asumió el riesgo de mantener la comunicación directa entre el Gobierno y el ELN, pese a que su condición de Gestor de Paz estaba suspendida. Los emisarios gubernamentales le habían planteado a Juan Carlos que podía seguir en estas gestiones, sin que fuera nuevamente encarcelado, por esto su captura significa otro golpe más, propio de un régimen que 'hace trizas la paz'”, señala el comunicado. Para la Delegación de Paz este hecho genera un clima de desconfianza, y se suma a los incumplimientos de este Gobierno, en el que recuerdan la violación a los protocolos firmados con los países garantes del diálogo.]

Para finalizar, la guerrilla manifiesta su disposición al diálogo y al restablecimiento de la [Mesa de Conversaciones, igualmente, expresa la apertura a un cese bilateral "sin exigencias unilaterales y en el marco de la bilateralidad ya establecida; nos disponemos a un cese bilateral para cesar en las acciones ofensivas de parte y parte, que le cree un mejor clima al Proceso de Paz, mientras esto no se acuerde, entendemos que se mantiene la operatividad de las dos partes".]

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/comunicado-eln-gestor-de-paz.jpg){.aligncenter .size-full .wp-image-78705 width="794" height="816"}  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
