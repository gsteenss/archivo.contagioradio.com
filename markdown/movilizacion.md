Title: Movilización
Date: 2014-11-25 15:43
Author: AdminContagio
Slug: movilizacion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/movilización-popular-en-guatemala-piden-renuncia-de-otto-pérez-molina-y-roxana-baldetti.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Movilización-Sogamoso.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Movilizaciones-ADE-e1456416787217.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Movilización-Hidroituango.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Movilizaciones-USA.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/movilización-fecode.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-19-770x400.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/Captura-de-pantalla-90.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/EE8HmaEWkAIXZ16.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Uni.-Atlántico.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/11/Universidad.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/07/Movilización-social-Marcha-por-la-Dignidad.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

MOVILIZACIÓN
------------

[](https://archivo.contagioradio.com/papa-negociacion-eln/)  

###### [Organizaciones sociales piden al Papa que intervenga en negociación con ELN](https://archivo.contagioradio.com/papa-negociacion-eln/)

[<time datetime="2019-01-28T18:23:29+00:00" title="2019-01-28T18:23:29+00:00">enero 28, 2019</time>](https://archivo.contagioradio.com/2019/01/28/)Organizaciones sociales, iglesias y comunidades piden al Papa que nuevamente intervenga para que sea posible la paz en Colombia[Leer más](https://archivo.contagioradio.com/papa-negociacion-eln/)  
[](https://archivo.contagioradio.com/escoltas-unp/)  

###### [Por malas condiciones laborales escoltas de la UNP cesarían actividades](https://archivo.contagioradio.com/escoltas-unp/)

[<time datetime="2019-01-28T16:51:32+00:00" title="2019-01-28T16:51:32+00:00">enero 28, 2019</time>](https://archivo.contagioradio.com/2019/01/28/)Los integrantes de la Unidad Nacional de Protección (UNP) reclaman mejora de condiciones laborales, así como garantías para cuidar a sus protegidos[Leer más](https://archivo.contagioradio.com/escoltas-unp/)  
[](https://archivo.contagioradio.com/caminantes-de-la-sierra/)  

###### [1.000 km por la educación: Llegan los Caminantes de la Sierra a Bogotá](https://archivo.contagioradio.com/caminantes-de-la-sierra/)

[<time datetime="2019-01-23T15:42:18+00:00" title="2019-01-23T15:42:18+00:00">enero 23, 2019</time>](https://archivo.contagioradio.com/2019/01/23/)Luego de 48 días y 1.198 km de recorrido, llegan los Caminantes de la Sierra a la Capital; una braza para avivar el fuego de la lucha estudiantil[Leer más](https://archivo.contagioradio.com/caminantes-de-la-sierra/)  
[](https://archivo.contagioradio.com/activistas-paz-jueves/)  

###### [Activistas por la paz se dan cita este jueves para hablar sobre negociación con ELN](https://archivo.contagioradio.com/activistas-paz-jueves/)

[<time datetime="2019-01-23T12:40:43+00:00" title="2019-01-23T12:40:43+00:00">enero 23, 2019</time>](https://archivo.contagioradio.com/2019/01/23/)Los activistas por la paz se reunirán en el ParkWay para encaminar acciones que permitan impulsar una salida negociada al conflicto con el ELN[Leer más](https://archivo.contagioradio.com/activistas-paz-jueves/)
