Title: Perspectiva al sur, cine documental con ojos de mujer
Date: 2016-11-08 15:11
Author: AdminContagio
Category: 24 Cuadros
Tags: Cine, Documental, Festivales, Latinoamérica, mujer
Slug: festival-cine-documental-mujer-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/14632909_360627124278216_5872379548941982017_n.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto:Achiote cocina audiovisual 

##### 8 Nov 2016 

Desde hoy y durante 4 días se desarrolla el **festival "Perspectiva al sur"**, una cita con el cine documental cuyo objetivo es **propiciar el intercambio audiovisual y académico entre países de América Latina**, y que en Bogotá encuentra un espacio en la Cinemateca Distrital.

Para esta segunda edición, e**l festival esta dedicado a las documentalistas latinoamericanas**, con una selección de [17 producciones](http://www.achioteaudiovisual.org/blog/2016/10/25/peliculas-perspectiva-al-sur-2016/) en largo, medio y largometraje, donde están plasmadas temáticas como **la identidad, la memoria, los derechos humanos y experiencias organizativas y de resistencia de las mujeres**.

Documentales como la coproducción chileno-mexicana **Allende mi abuelo Allende**, de Marcia Tambutti Allende, L**a madre (Dayipapara)** de Marta Hincapié y **Buen Pastor, una fuga de mujeres** de Lucía Torres de Colombia y **Verde Carne, Tierra muerta** de Cristina M.Olivé y Andrea Bilbao de Guatemala-Honduras, son algunas de las producciones que hacen parte de la programación del evento.

Las proyecciones estarán acompañadas de **foros y debates**, en compañía de las realizadoras y otras invitadas con las que se abrirán las conversaciones a profundidad sobre cada uno de los temas, en **horarios de 3:00, 5:00 y 7:00 p.m**.

![programación](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/Programación_PalSurII3.jpg){.size-full .wp-image-31887 .aligncenter width="1000" height="1950"}

### **Acción desde el documental** 

Perspectiva sur, es organizado por **Achiote, cocina Audiovisual**, un colectivo que busca **promover y realizar acciones, producciones y encuentros en torno al audiovisual, enfocadas principalmente en la realización documental**, en alianza con organizaciones sociales y comunitarias, que sirva para dar a conocer sus experiencias mediante herramientas audiovisuales, y en abrir y fortalecer espacios que contribuyan a consolidar el sector cinematográfico colombiano y latinoamericano.
