Title: Empresa canadiense demandaría a Colombia por fallo que protege páramo de Santurbán
Date: 2016-03-16 14:57
Category: Ambiente, Nacional
Tags: Corte Constitucional, delimitación de páramos, Eco Oro Minerals Corp
Slug: empresa-canadiense-demandaria-a-colombia-por-fallo-que-protege-paramo-de-santurban
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Paramo-de-Santubán-e1458158028827.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Blog Una idea, una razón 

###### [16 Mar 2016] 

Luego de que la Corte Constitucional fallara a favor de una tutela interpuesta por congresistas del Polo Democrático Alternativo, con la que se prohíbe realizar actividades mineras en zonas de páramo, **la empresa canadiense Eco Oro Minerals Corp anunció que demandará al Estado Colombiano ante instancias internacionales** pues pretende explotar **la mina Angostura en el páramo de Santurbá**n.

La empresa anunció un posible arbitraje internacional por las medidas adoptadas por el país para proteger los páramos, argumentando que es posible demandar al Estado de acuerdo con el capítulo de inversión del Tratado de Libre Comercio entre Canadá y Colombia. **La empresa argumenta haber sido afectada por la delimitación de ese ecosistema y aseguró que buscará “compensación monetaria por los daños”**.

Frente a esta situación, diferentes organizaciones en defensa del ambiente se oponen a este anuncio de Eco Oro Minerals Corp, y llaman a la empresa a desistir de esta amenaza de llevar al Estado a arbitraje internacional, temiendo que otras empresas con intereses en el este páramo y otros del país sigan ese ejemplo.

Carlos Lozano Acosta, abogado de la Asociación Interamericana para la Defensa del Ambiente, AIDA, explica que “Desde el comienzo de Angostura era claro que la Constitución y normas aplicables protegen los páramos, que el proyecto podría afectar Santurbán y que por ende podría no ser autorizado. **Los Estados no deben ser sancionados por proteger sus fuentes de agua,** cumpliendo las obligaciones nacionales e internacionales”.

Cabe recordar, que en 2011, **el Ministerio de Ambiente de Colombia negó la licencia ambiental a la mina Angostura,** demostrando la inviabilidad del proyecto. Lo que significa que el fallo de la Corte Constitucional estaría “reafirmando que el derecho al agua y la protección de los páramos prima sobre las expectativas económicas de quienes desarrollan proyectos mineros en esos ecosistemas”, asegura Miguel Ramos, integrante del Comité por la Defensa del Agua y el Páramo de Santurbán.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/). 
