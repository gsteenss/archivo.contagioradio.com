Title: Amenazado de muerte defensor de derechos humanos en Popayán
Date: 2017-09-26 13:34
Category: DDHH, Nacional
Tags: Amenazas a defensores de Derechos Humanos, defensores de derechos humanos, Popayán
Slug: amenaza-a-defensor-de-derechos-humanos-en-popayan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/sms2-e1506450825105.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: PcWorld] 

###### [25 Sept 2017] 

La Red de Derechos Humanos del Suroccidente Colombiano “Francisco Isaías Cifuentes”, **denunció una amenaza de muerte contra un defensor de derechos humanos,** que hace parte de la Red. Los hechos se presentaron en la ciudad de Popayán, Cauca el domingo 24 de septiembre de 2017.

De acuerdo con la información brindada por la Red de DDHH, **la amenaza la recibió vía mensaje de texto** el defensor de derechos humanos Deivin Hurtado. A su celular fueron enviadas imágenes en las cuales los delincuentes manifestaron su intención de hacerle daño.

En el comunicado indicaron que, desde el número de celular utilizado para realizar la amenaza, **también habían sido amenazados en el pasado** los defensores Huber Ballesteros, Mayerli Hurtado Motta y Victoria Ballesteros. (Le puede interesar:["Entre 2009 y 2016 han sido asesinados 458 defensores de DDHH en Colombia"](https://archivo.contagioradio.com/entre-2009-y-2016-han-sido-asesinados-458-defensores-de-dd-hh-en-colombia/))

Ante estos hechos, el movimiento de defensa de los derechos humanos **responsabilizó al Gobierno Nacional y al Ejército Nacional** por la falta de responsabilidad en la protección a los derechos de los defensores y líderes sociales.

Los responsabilizaron además **“por las violaciones al Derecho Internacional de los Derechos Humanos** cometidas por integrantes de fuerzas armadas irregulares, en la zona de operaciones de las unidades militares y policiales que dirigen”.

De igual forma, exigieron que el Estado Colombiano **asuma la responsabilidad frente a la protección de los derechos** a la vida, la seguridad personal, la libre movilidad, la integridad física y psicológica y la intimidad personal y familiar de Deivin Hurtado. (Le puede interesar: ["Stop Wars, paren la guerra contra los defensores"](https://archivo.contagioradio.com/stop-wars-paren-la-guerra-contra-defensores-as/))

Finalmente, le solicitaron a la Oficina del Alto Comisionado de las Naciones Unidas para los Derechos Humanos que “preste toda su gestión para que las actuaciones del Estado Colombiano **se apeguen a las normas internas y externas** que se ha comprometido a respetar y que se inicien las investigaciones a que haya lugar por el desconocimiento de las mismas”.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
