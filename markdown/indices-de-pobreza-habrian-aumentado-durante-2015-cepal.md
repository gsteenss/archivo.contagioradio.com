Title: Indices de pobreza aumentaron durante 2015: CEPAL
Date: 2016-03-23 10:00
Category: Economía, Nacional
Tags: Informe CEPAL, Pobres en Latinoamerica
Slug: indices-de-pobreza-habrian-aumentado-durante-2015-cepal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/pobreza_desigualda.jpg_1718483346.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Telesur 

##### [22 Mar 2016]

En su informe anual 'Panorama social de América Latina', la Comisión Económica para América Latina y el Caribe CEPAL, hace un llamado a los gobiernos de la región para que protejan la estabilidad alcanzada en materia social entre los años 2013 y 2014, e impidan un retroceso ocasionado por el inminente crecimiento que habrían tenido las cifras de pobreza e indigencia durante 2015.

El documento, presentado en Santiago de Chile por la Secretaria Ejecutiva de la CEPAL Alicia Bárcena, establece que las tasas de pobreza e indigencia medidas por ingresos se mantuvieron estables durante 2014 respecto al año anterior, situándose en 28,2% y 11,8% de la población de la región respectivamente, con un leve aumento en la cantidad de pobres no indigentes.

De acuerdo con la estadística, la cantidad de pobres alcanzó a ser de 168 millones en 2014, de las cuales 70 millones se encontraban en situación de indigencia, cifras que se elevarían considerablemente de confirmarse las proyecciones hechas para 2015 de 175 millones que vivirían en situación de pobreza por ingresos, 75 millones de las cuales estarían en situación de indigencia.

El documento, lanzado la mañana de hoy, analiza también la evolución en la distribución del ingreso y las desigualdades que se manifiestan en el mercado laboral, reportando una leve disminución de la desigualdad por ingresos entre 2013 y 2014, en el promedio de los países que cuentan con información reciente. Pese a este descenso, en 2014 el ingreso per cápita de las personas del 10% de mayores ingresos fue 14 veces superior que el del 40% de menores ingresos.

Por ello, la CEPAL pidió proteger los avances logrados en años recientes e impedir retrocesos sociales ante un escenario de menor crecimiento económico “Si queremos lograr el primer Objetivo de Desarrollo Sostenible, que llama a poner fin a la pobreza en todas sus formas, América Latina debe generar más empleos de calidad, con derechos y protección social, cautelar el salario mínimo y proteger el gasto social, que muestra una merma en su ritmo de crecimiento”, sostuvo  Bárcena.

[Panorama social en América Latina CEPAL](https://es.scribd.com/doc/305659356/Panorama-social-en-America-Latina-CEPAL "View Panorama social en América Latina CEPAL on Scribd")

<iframe id="doc_46146" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/305659356/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>
