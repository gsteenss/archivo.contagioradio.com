Title: Organizaciones sociales realizan balance de la implementación de los acuerdos de paz
Date: 2017-07-05 16:30
Category: Nacional, Paz
Tags: acuerdos de paz, encuentro nacional comun acuerdo, implementación de los acuerdos
Slug: organizaciones-sociales-realizan-balance-de-la-implementacion-de-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Paz-e1499290199642.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Cháchara.co] 

###### [05 Jul 2017]

Las organizaciones sociales de 26 departamentos del país realizarán durante tres días un balance de la implementación de los acuerdos de paz. Para esto tendrán en cuenta la finalización de la primera parte del Fast Track, **donde se crearon 30 normas expedidas por el Congreso y por el uso de las facultades extraordinarias del presidente**,  y la dejación total de las armas por parte de las FARC.

Para Antonio Madariaga, director de la Corporación Viva la Ciudadanía, **“vamos a hacer un balance del proceso de creación de normas y rutas que se han ido creando** y que culminaron con la primera parte del Fast Track”. Manifestó además que “esto se hará en términos de reconocimiento de los derechos, instrumentos y mecanismos que se desprenden de las normas expedidas y que sirven para la implementación de los acuerdos”. (Le puede interesar: ["Implementación de los acuerdos requiere movilización social" Iván Cepeda"](https://archivo.contagioradio.com/implementacion-de-los-acuerdos-requiere-movilizacion-social-ivan-cepeda/))

De cara a la implementación de los acuerdos de paz, en el II Encuentro Nacional Común Acuerdo, los participantes buscarán **construir un plan de acción que le permita a las organizaciones participar de manera efectiva en la construcción de una paz territorial**. Según Madariaga, “el desarrollo de la implementación requiere de la participación de la sociedad civil para lograr, por ejemplo, la ampliación y profundización de la democracia contenida en los acuerdos”.

Madariaga se refirió a la situación de los presos políticos de las FARC argumentado que ha sido uno de los obstáculos que tiene que hacerse evidente para ser resuelto lo antes posible. “Lo que ocurre con los presos políticos es un error procesal que dificulta la implementación de la ley de amnistía acordada y la sociedad en general debe exigir celeridad en estos procesos”.

Así mismo manifestó que el Gobierno debe considerar este tipo de obstáculos y **“reconocer que esto no debería estar pasando y hacer los esfuerzos para que sociedad incida en que el acuerdo** y la amnistía sea implementado”. (Le puede interesar: ["Investigación destaca aporte de las mujeres en el proceso de paz de la Habana"](https://archivo.contagioradio.com/?p=43151&preview=true))

Finalmente, Madariaga hizo referencia a la importancia de que la **Corte Constitucional avale el acto legislativo que establece el sistema de verdad, justicia y reparación**. Según él, “lo que está sucediendo en la audiencia de la Corte Constitucional es un debate sobre uno de los aspectos medulares del acuerdo que es el sistema integral de verdad, justicia y reparación.” Por esto recalcó la importancia de la manifestación de las víctimas quienes son las más interesadas en que este sistema pueda ser implementado.

**En el encuentro participarán 200 organizaciones civiles y se realizará desde el 05 de julio hasta el 07 de julio en el Hotel Dann Carlton de Bogotá.** Durante el Miércoles y el Jueves se hará un balance de la implementación de cada uno de los puntos del acuerdo para cerrar el viernes con un balance general, discusiones sobre la paz territorial y la construcción de la agenda de movilización nacional para la implementación.

<iframe id="audio_19647963" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19647963_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

###### 
