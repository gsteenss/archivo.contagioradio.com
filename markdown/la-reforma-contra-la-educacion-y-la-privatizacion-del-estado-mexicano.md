Title: La reforma contra la educación y la privatización del Estado mexicano
Date: 2016-08-12 09:39
Category: Eleuterio, Opinion
Tags: educacion, mexico, Privatización
Slug: la-reforma-contra-la-educacion-y-la-privatizacion-del-estado-mexicano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/privatizacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:DesInformemonos 

#### Por **[Eleuterio Gabón ](https://archivo.contagioradio.com/eleuterio-gabon/)** 

###### 12 Ago 2016 

[El pasado mes de junio las movilizaciones del sindicato magisterial en contra de la reforma educativa, fueron duramente reprimidas por el gobierno de Enrique Peña Nieto. Los duros enfrentamientos que se produjeron en varios estados del país como Chiapas, Guerrero o Oaxaca dejaron detenciones arbitrarias, varios heridos y al menos 9 muertos. El ejército y la policía federal trataron de imponerse por la fuerza a los cortes de carreteras y a las movilizaciones de protestas de los maestros y las comunidades que les apoyan en su lucha.]

[La criminalización del movimiento magisterial en los medios de comunicación y su defensa a ultranza de la posición del gobierno en el conflicto, nos lleva a buscar las voces de algunos de los maestros y maestras en lucha para que nos den su visión de lo que está ocurriendo. Un grupo de maestros de Veracruz remontan el comienzo de la lucha magisterial tres años atrás. “Entonces la cámara de senadores aprobó la nueva ley y desde ese momento comenzó también nuestra lucha para derogar esta reforma educativa. El 13 de septiembre de 2013 fuimos desalojados del Zócalo Capitolino en Ciudad de México, tras resistir durante 87 días volvimos a nuestros estados para seguir luchando contra la reforma desde allí. Desde entonces hasta día de hoy hemos venido resistiendo desde los diferentes estados del país pero al centralizar el gobierno el control del magisterio a nivel federal, los estados han perdido su capacidad legal de respuesta.” Ahora al intentar aplicar la reforma al 100% las movilizaciones son a nivel nacional y la represión se ha recrudecido.]

[Hablamos de una reforma laboral más que educativa, ya que no tiene un carácter pedagógico sino que está orientada al control laboral de los maestros de primaria y secundaria. La reforma conlleva una evaluación no para medir el nivel educativo de los maestros sino para facilitar su despido. Un despido que supone la pérdida de las prestaciones, la plaza base y la jubilación, la pérdida de la pensión y la seguridad laboral. Si la reforma llega a implementarse los padres deberán hacerse cargo de pagar la educación de sus hijos. La educación deja de ser pública y gratuita, hasta ahora el Estado tenía la obligación de dar el 100% del presupuesto educativo. Ahora manejan un nuevo concepto que llaman “autonomía de gestión”, según el cual los padres deben conseguir el presupuesto para mantenimiento y materiales para las escuelas de sus hijos. Dos maestras de primaria nos cuentan cómo afecta esto en su región. “Nosotras trabajamos en municipios muy pobres en Chiapas. Tenemos hijos de padres campesinos que tienen un sueldo de 60 pesos el jornal (3€) en familias con 7 u 8 miembros, con lo que resulta imposible hacerse cargo de esa “autonomía de gestión”.]

[Los maestros seguirán en la lucha, conscientes de que no es sólo la educación lo que está en juego en el futuro de México. “Es un proceso complicado porque sabemos que la reforma educativa está dentro de un paquete de 12 medidas estructurales que afectan al país en materia energética, fiscal, política, de salud... por lo que lograr un cambio en la reforma educativa abriría el camino para derogar las otras reformas que se están llevando a cabo; creemos que el país va hacia ese camino. A veces hay cosas que no alcanzamos a comprender nosotros mismo pero sabemos que estamos en el camino de la lucha.”]

Por último las maestras chiapanecas nos hablan de su trabajo, del que realizan día a día, ese que quieren seguir llevando a cabo y que no es otro que el de educar a la juventud de su país. “Trabajamos con los pueblos campesinos de Chiapas y lo hacemos no sólo con contenidos formales sino también sobre la lucha social y política, sobre la realidad social que estamos viviendo. Tratamos de educar en la pedagogía crítica, educar para emancipar como decía Freire. Queremos formar niños y adolescentes libres, críticos e independientes, no queremos educar para obreros y sumisas y sometidas del sistema. Queremos que la gente sea capaz de generar conocimiento por ellos mismos.”

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio. 
