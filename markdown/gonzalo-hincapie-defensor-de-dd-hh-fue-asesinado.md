Title: Gonzalo Hincapié, defensor de DD.HH. fue asesinado
Date: 2020-11-04 14:38
Author: AdminContagio
Category: Actualidad, Líderes sociales, Nacional
Tags: Antioquia, defensores de derechos humanos, El peñol, Gonzalo Hincapié
Slug: gonzalo-hincapie-defensor-de-dd-hh-fue-asesinado
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/muerte-de-lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: @FunSumapaz

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El martes 3 de noviembre se reportó que el defensor de derechos humanos y presidente de la Junta de Acción Comunal de la vereda La Cristalina, **Luis Gonzalo Hincapié fue asesinado en El Peñol , en el departamento de Antioquia.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la información que se ha dado a conocer, el hecho ocurrido cuando hombres armados llegaron hasta la finca donde residía el defensor en zona rural y le propinaron varios impactos de arma de fuego. (Le puede interesar: [Denuncian allanamiento “irregular” al defensor de DD.HH. Jassir Olivares](https://archivo.contagioradio.com/denuncian-allanamiento-irregular-al-defensor-de-dd-hh-jassir-olivares/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/Indepaz/status/1323833486415974401","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/Indepaz/status/1323833486415974401

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Por el momento, las autoridades continúan buscando a los responsables de este hecho e investigan los motivos que habrían conducido al asesinato de Gonzalo. Además, Carlos Camargo Assis, **Defensor del Pueblo, pidió a autoridades regionales hacer seguimiento al caso ya que en la región se han conocido reiteradas amenazas contra líderes sociales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con el [Instituto de estudios para el desarrollo y la paz -Indepaz-](http://www.indepaz.org.co/) con Gonzalo Hincapié, ya son 251 los defensores de DD.HH asesinados en el 2020 y 656 durante la presidencia de Iván Duque Márquez.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A la vez que se conocía el asesinato de Hincapié, también se denunciaba el asesinato del líder social Jorge Solano en Ocaña, Norte de Santander y una nueva masacre en la que fueron asesinadas 4 personas en el corregimiento de Bijagual, municipio de Nechí, en la región del Bajo Cauca antioqueño. Ante estos hechos, la ciudadanía denuncia que el asesinato de líderes sociales es sistemático y no hay acciones por parte del Gobierno Nacional. (Le puede interesar: [Es asesinado Jorge Solano, reconocido líder social de Ocaña](https://archivo.contagioradio.com/es-asesinado-jorge-solano-lider-social-de-ocana/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
