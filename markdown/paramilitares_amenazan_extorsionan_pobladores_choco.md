Title: Paramilitares extorsionan y amenazan a pobladores de Murindó, Chocó
Date: 2017-03-04 13:44
Category: DDHH, Nacional
Tags: Chocó, paramilitares
Slug: paramilitares_amenazan_extorsionan_pobladores_choco
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Primicia Diario 

###### [4 Mar 2017] 

Los integrantes del Consejo Comunitario Local de la Isla de Los Rojas, vienen siendo víctimas desde febrero de la amenaza paramilitar que continúa consolidándose en lugares como Murindó, en el departamento del Chocó. Esta vez, la comunidad fue objeto de un engaño para **extorsionar a algunos de los pobladores, con el objetivo de financiar grupos paramilitares. **

Así  lo asegura el Consejo Comunitario Mayor de la Asociación Campesina Integral del Atrato, COCOMACIA. Según la denuncia el pasado 2 de febrero un miembro de Junta saliente de la comunidad recibió una llamada de una persona que **se identificó como Carlos Jiménez, quien dijo ser funcionario del Ministerio de Agricultura**. Jiménez informó que el consejo comunitario había sido beneficiario de un proyecto de siembra de plátano, y por lo tanto, necesitaban con urgencia un lista de 15 beneficiarios.

Ante esa información la junta del Consejo definió quienes serían las 15 personas beneficiarias de dicho proyecto, aportando nombre completo y número telefónico. Supuestamente con esa información se gestionaría la puesta en marcha del proyecto de la siembra de plátano. Sin embargo, apenas tres días después, una de las personas designadas por la comunidad como beneficiaria del proyecto, **recibió una llamada en  la que le obligaban a pagar un millón de pesos para la compra de camuflados** y amenzándolo para que no avisara a las autoridades.

La denuncia, señala que las intimidaciones y extorsiones continuaron. Cuatro supuestos beneficiarios del proyecto también fueron amenazados y les dijeron que si no tenían el dinero, deberían vender algún bien de su propiedad. De lo contrario les quitarían sus bienes, o tenían que desocupar la zona, "porque esa era la orden del **Clan del Golfo de Urabá".**

Además de las extorisiones, la comunidad viene siendo víctimas de una posible "limpieza social”. La denuncia señala que **"los recursos que estaban solicitando eran a nombre de los paramilitares que tienen el control en la zona de Urabá y Murindó** y que su intención era hacer una limpieza social a través, de un plan pistola a los sapos, marihuaneros y ladrones que existen en la zona". Los recursos solicitados eran para la compra de proyectiles, uniformes y botas, y quien se negara sería asesinado junto a su familia.

Esa situación mantiene en alerta a la población. COCOMACIA hace un llamado a las autoridades locales, regionales y nacionales para que actúen frente a la amenaza paramilitar que afecta a las 124 comunidades afro del área de influencia de COCOMACIA, ubicadas en la cuenca de medio Atrato.

###### Reciba toda la información de Contagio Radio en [[su correo]
