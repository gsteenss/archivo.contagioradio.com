Title: Comunidades del Putumayo denuncian persecución judicial
Date: 2017-07-28 17:29
Category: DDHH, Nacional
Tags: medio ambiente, Perla Amazónica, Persecución Judicial, petroleo, zonas de reserva campesina
Slug: comunidades-del-putumayo-denuncian-persecucion-judicial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/reserva-putumayo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [28 Jul 2017] 

Los presidentes de las Juntas de Acción Comunal, del sector 4 de la Zona de Reserva Campesina de la Perla Amazónica, ZRCPA, **fueron citados a diligencia judicial el 4 de agosto por la Fiscalía 12 Especializada**. Ellos manifestaron que se encuentran ante una posible persecución judicial.

Según la Comisión Intereclesial de Justicia y Paz, “La fiscalía 12 fue creada en el marco de un convenio de cooperación de 2001 entre la Fiscalía General de la Nación, Policía Nacional, Ecopetrol y el Fondo Rotatorio de la Policía Nacional. La misma tiene sede dentro de la infraestructura del batallón de Ingenieros Mecanizados No 27 General Manuel Castro y **opera en coordinación con las fuerzas regulares del ejército**, Gaula y unidades antiterrorismo”. (Le puede interesar:"[Campesinos del Putumayo logran acuerdos de sustitución de cultivos"](https://archivo.contagioradio.com/campesinos-del-putumayo-logran-acuerdos-de-sustitucion-de-cultivos/))

### **Citación judicial se da en el marco de movilizaciones por protección del ambiente** 

La citación judicial sucede cuando las comunidades se están movilizando desde el pasado 10 de julio por **los incumplimientos del gobierno ante los  preacuerdos de Sustitución de Cultivos de Uso Ilícito** firmado entre las organizaciones de la región, el Gobierno Nacional y las FARC. Igualmente, ha habido movilizaciones por frenar las acciones petroleras que están causando daños ambientales.

Jani Silva, lideresa de ANZORC, manifestó que los presidentes llamados a citación judicial hacen parte de las Juntas de Acción Comunal que se encuentran en el **área de influencia de la empresa petrolera Amerisur**. “Estas personas estuvieron en movilización y han denunciado en varias ocasiones contaminación de la extracción petrolera”. (Le puede interesar:["En Putumayo el Estado ha dilatado plan de sustitución de cultivos de uso ilícito"](https://archivo.contagioradio.com/en-putumayo-el-estado-ha-dilatado-el-plan-de-sustitucion-de-cultivos-de-uso-ilicito/))

De igual manera, Silva dijo que en la comunidad hay una preocupación porque **“no es la primera vez que reciben citaciones judiciales** que se dan siempre durante las movilizaciones cuando las comunidades han salido a reclamar sus derechos a un sano ambiente, al trabajo y a la no erradicación forzada”.

Finalmente, Silva expresó que al no ser clara la citación **los presidentes de las juntas de acción comunal no se van a presentar**.  Para ellos estos llamados de La Fiscalía son  un intento de persecución judicial para impedir la protección de los derechos territoriales y ambientales de las Zonas de Reserva Campesina.

<iframe id="audio_20054054" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20054054_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
