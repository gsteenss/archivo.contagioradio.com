Title: No repetición: El caso de paramilitares y mercenarios
Date: 2016-01-16 18:01
Category: Opinion, Ricardo
Tags: militares, paramilitares, Paramilitarismo, paz
Slug: paramilitares-y-mercenarios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/mercenarios.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: primiciadiario ] 

#### **[Ricardo Ferrer Espinosa](https://archivo.contagioradio.com/ricardo-ferrer-espinosa/)  - @ferr\_es** 

###### 16 Ene 2016

#### **Romper el anonimato (parte 1)** 

[Si se consolida la salida política al conflicto colombiano, la sociedad civil querrá que se cumpla la promesa de no repetición:]**Que los gobernantes dejen de importar mercenarios y disuelvan las fuerzas paramilitares que actúan contra la oposición política y los movimientos sociales**[.  ]

El pueblo colombiano ha visto y sufrido durante décadas la acción de empresas y organizaciones de mercenarios que han sido a su vez gestoras de organizaciones paramilitares y han desviado a las Fuerzas Armadas de Colombia de sus obligaciones constitucionales. \[1\]

Presidentes de Colombia, gremios económicos, políticos y mandos militares, en su decisión de guerra, permitieron y facilitaron el accionar de tropas extranjeras que a su vez entrenaron y entrenan ejércitos privados. A su vez, las fuerzas paramilitares ejecutaron sistemáticamente a  sindicalistas y opositores políticos para poder implantar su modelo económico.

¿Qué puede hacer el ciudadano colombiano para que esto no se repita? Qué puede hacer la comunidad frente a sicarios extranjeros que vienen a  delinquir, espiar, asesinar, masacrar, desplazar población?

**Lo primero es retomar el debate público del tema**[. Las actividades mercenarias y paramilitares son parte de la globalización sicarial, donde todo vale para obtener ganancias: Usurpan tierras, asesinan y desplazan a sus dueños; el sindicalista es neutralizado, el político es comprado, y finalmente el contrato de la obra millonaria se asigna a los corruptos. La presión armada hace parte del “paquete normal” de la actual economía: el resultado es la privatización de bienes públicos, licitaciones amañadas, intimidación a periodistas y control de territorios.  ]

**El mercenario y el paramilitar necesitan anonimato, la garantía de su impunidad**

**La primera acción es delatar a las empresas que contratan mercenarios. Podemos delatar a las firmas que operan en Colombia.**[ Ya están identificados más de 20 mil empresarios que tienen responsabilidad directa por su patrocinio a los escuadrones de la muerte y fuerzas paramilitares. En muchos casos esas mismas empresas “importaron”  asesores israelíes, sudafricanos, irlandeses, ingleses, argentinos y norteamericanos. El trabajo es identificar a cada uno de estos sicarios internacionales, conseguir sus fotografías, sus nombres, su país de origen, la empresa que lo contrató. Proponemos que se abra la “galería de la fama”, que los mercenarios queden en evidencia y que lo sepa cada vecino en su país de origen.  ]

Estos datos no pueden quedarse en los sumarios, no pueden ser de uso exclusivo de la Fiscalía, deben hacerse públicos y deben ser difundidos a los cuatro vientos.

**Las empresas bananeras de Colombia, pagaron millones de dólares a los escuadrones de la muerte durante el cogobierno AUV – AUC**[. En los medios de difusión nunca se nombran las compañías de seguridad que pagaron  a los paramilitares por el trabajo sucio. Corresponde a las víctimas estimular la memoria y conseguir los nombres de quienes agenciaron la muerte.]

Recordamos hoy que el gobierno de los Estados Unidos desplegó sus Boinas Verdes (Séptimo grupo de Operaciones Especiales) en Colombia.  Un año especialmente sangriento fue 1997, cuando estas tropas norteamericanas acompañaron la Operación Génesis, la masacre de Mapiripán y la guerra en el sur de Colombia. \[2\] Estos criminales de guerra siguen impunes, anónimos y es urgente ponerles rostro.

Es necesario que recordemos. La memoria hace parte de la reconstrucción del tejido social y nos orienta para que estos hechos nunca más se repitan en la historia de Colombia.

###### [\[1\]][[http://mercenariosencolombia.blogspot.com]](http://mercenariosencolombia.blogspot.com) 

###### [\[2\] “Nuestra guerra ajena”, Germán Castro Caicedo, Planeta, Bogotá, 2014.] 

######  
