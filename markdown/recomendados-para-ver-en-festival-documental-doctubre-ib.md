Title: Documentales y cortos recomendados del "Doctubre IB"
Date: 2015-10-06 12:16
Author: AdminContagio
Category: 24 Cuadros, eventos
Tags: Carolina Platt, Cinemateca Distrital de Bogotá, Dir. Adriana Trujillo, Festival de cortometraje y documental, Festival Doctubre, Gonzalo Gerardín &amp; María Paula Trocchia, Octubre
Slug: recomendados-para-ver-en-festival-documental-doctubre-ib
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/La-hora-de-la-siesta.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Lahoradelasiesta.mx 

###### [6 Oct 2015] 

Del 7 al 9 de Octubre, la Cinemateca Distrital de Bogotá, se une a uno de los festivales más importantes de Cine Documental mexicano: "Doctubre IB", un espacio de encuentros y desencuentros entre naciones, culturas, tradiciones, costumbres e idiomas existentes en la región.

La muestra que se realiza desde el año 2013, llega la capital colombiana con una programación compuesta por 15 documentales y cortometrajes de directores en su mayoría iberoamericanos, con funciones de 12:30, 3:00, 5:00 de la tarde y 7:00 de la noche.

**Recomendados:**

### **Autoficciones de un traficante- Dir. Adriana Trujillo** 

Félix es un actor y traficante de personas en Tijuana, México, quien ha vivido la transformación de la frontera que divide nuestro país de Estados Unidos. Este pintoresco personaje ha contado su historia en una multitud de películas de bajo presupuesto, en las cuales revela sus estrategias y métodos para cruzar ilegalmente a cientos de personas.

<iframe src="https://www.youtube.com/embed/0JMNBLt_hAE" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

 

### **Enseguida anochece - Dir.  Gonzalo Gerardín & María Paula Trocchia** 

«Cada uno está solo sobre el corazón de la tierra traspasado por un rayo de sol: y enseguida anochece»

<iframe src="https://www.youtube.com/embed/_ZSRcSX2phY" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

**La hora de la siesta-  Dir.Carolina Platt**

“Atravesamos los mares del recuerdo y la ausencia para contar las historias de Emilia y Yeyé, dos de los 49 niños que perdieron la vida en la Guardería ABC, en un incendio que pudo ser evitado y que mostró las terribles consecuencias de la corrupción y el tráfico de influencias dentro del sistema de guarderías subrogadas por el gobierno en México. “La hora de la Siesta” es una elegía visual a la memoria de estos niños y a la valiente lucha de sus familias por retomar la vida” Carolia Platt, directora.

<iframe src="https://www.youtube.com/embed/0N23MRQJgU4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

### [[Programación Cinemateca Octubre](https://es.scribd.com/doc/283846011/Cinemateca-programacio-n-octubre "View Cinemateca programación octubre on Scribd")] 

<iframe id="doc_15201" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/283846011/content?start_page=1&amp;view_mode=scroll&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="undefined"></iframe>  
 

 

 
