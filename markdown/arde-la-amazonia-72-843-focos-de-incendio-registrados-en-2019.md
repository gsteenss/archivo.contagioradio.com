Title: Arde la Amazonía: 72.843 focos de incendio registrados en 2019
Date: 2019-08-21 22:04
Author: CtgAdm
Category: Ambiente, Entrevistas
Tags: Amazonía, Incendios en la Amazonía, jair bolsonaro
Slug: arde-la-amazonia-72-843-focos-de-incendio-registrados-en-2019
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Amazonía.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Página 12] 

Desde inicios del mes de agosto, parte de la selva de la Amazonía, de la que dos tercios se encuentran en Brasil, esta siendo consumida por las llamas, llegando a registrarse un total de **72.843 incendios en lo que va del 2019,** equivalente a un aumento del 80% en comparación con el mismo periodo de 2018 y la mayor oleada de fuegos de los últimos siete años.

Aunque los incendios forestales son comunes durante la estación seca de agosto a octubre, ambientalistas culpan al presidente Jair Bolsonaro por la situación de la Amazonía, señalando al mandatario de alentar a sectores  madereros y agricultores a quemar la vegetación para "despejar" la tierra.  Según cifras del INPE, tan solo en 2018 se perdieron cerca de 7.500 kilómetros cuadrados de bosque, lo que representa un área 65% más grande que la perdida en 2017.

### Políticas de Bolsonaro afectan la Amazonía

Por su parte, Bolsonaro insinuó que los responsables tras estos incendios, serían organizaciones no gubernamentales "como venganza por el recorte de los fondos que el Gobierno les entregaba", sin embargo estadísticas señalan que desde su llegada al poder, **los incendios en la Amazonia se han incrementando un 83% según el Instituto Nacional de Investigaciones Espaciales de Brasil (INPE).**

Desde el inicio de su mandato, las políticas de Bolsonaro se han orientado a impulsar los intereses económicos y flexibilizar la industria maderera y las explotaciones mineras, incluyendo las que están ubicadas en territorios indígenas que habitan el bosque tropical que produce el 20% del oxígeno en la atmósfera terrestre.  [( Le puede interesar: Siguen talando los derechos de la Amazonia)](https://archivo.contagioradio.com/siguen-talando-los-derechos-la-amazonia/)

En el Amazonas habitan 350 grupos indígenas, además alberga 600 especies y cerca de 10 son únicas de esta región, de igual forma habitan el 20% de las aves del mundo y cubre cerca de 6.7 millones de kilómetros cuadrados de bosque.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
