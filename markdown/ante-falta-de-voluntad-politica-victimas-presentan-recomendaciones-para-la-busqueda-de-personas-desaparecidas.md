Title: Más de 300 recomendaciones para la búsqueda de personas desaparecidas
Date: 2016-02-17 14:19
Category: Nacional, Paz
Tags: Desaparición forzada, Diálogos de La Habana, Fundación Nydia Erika Bautista
Slug: ante-falta-de-voluntad-politica-victimas-presentan-recomendaciones-para-la-busqueda-de-personas-desaparecidas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/desaparecidos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

<iframe src="http://www.ivoox.com/player_ek_10473011_2_1.html?data=kpWhmZiUdZKhhpywj5WdaZS1k5qah5yncZOhhpywj5WRaZi3jpWah5yncaLi1cqYyMbQuMKfxcqY2NTQuc%2FowsmY0tTQaaSnhqax1s7HpYzqhqigh6aop9XdzsbgjdXWqdTZz9nO0JDWqY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Yaneth Bautista] 

###### [17 Feb 2016 ]

**Durante dos meses más de ochenta organizaciones de víctimas provenientes de diferentes regiones del país se dieron cita en la Mesa de Trabajo sobre Desaparición Forzada** de la red 'Coordinación Colombia-Europa-Estados Unidos', con el fin de construir recomendaciones que harán llegar a mediados de marzo a las delegaciones de paz reunidas en La Habana, para la implementación del acuerdo humanitario dado a conocer en el comunicado 62 de octubre del año pasado.

De acuerdo con Yaneth Bautista, directora de la Fundación Nidya Erika Bautista, lo que llegará a la Mesa de Diálogos es el resultado de un proceso de varias etapas. En la primera **buscaron que la Comisión de Búsqueda de Personas Desaparecidas rindiera cuentas a las víctimas** sobre los avances logrados hasta el momento; sin embargo, **por falta de voluntad política** de la Fiscalía, la Procuraduría, el Ministerio de Defensa, el Instituto de Medicina Legal, la Consejería Presidencial para los Derechos Humanos y la Defensoría del Pueblo, quienes conforman esta Comisión, **el intento fue fallido**.

Bautista asegura que **el 90% de las audiencias regionales sobre desaparición forzada que se han realizado han sido un fracaso**, "resultaron en revictimización (...) porque no dieron respuesta a lo que las víctimas de desaparición forzada necesitamos que es saber el paradero de nuestros seres queridos y cómo van las investigaciones", lo que en su opinión **continúa impidiendo el acceso al derecho a la verdad al que tienen todas las víctimas**.

Más de 300 recomendaciones fueron construidas con base en la sabiduría lograda por familiares en "la búsqueda frustrada de sus seres queridos", según afirma la directora, "**el saber que han recopilado durante todos los años pidiendo justicia, pidiendo verdad**. Sabiduría popular combinada con saber académico de expertos de organismos de derechos humanos", que se articula para trabajar en pro de que los acuerdos a los que llegue la Mesa de Diálogos sean implementados sea cuál sea el mecanismo de implementación.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
