Title: Acciones de pre campaña son el ingrediente perfecto para la corrupción: MOE
Date: 2017-11-30 17:16
Category: Nacional, Política
Tags: campañas electorales, candidatos presidenciales, elecciones, MOE
Slug: acciones-de-pre-campana-son-el-ingrediente-perfecto-para-la-corrupcion-moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/11/pol_firmas_campuzano_0-e1512080137540.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Meridiano 70] 

###### [30 Nov 2017] 

Si bien no ha comenzado la campaña electoral, la Misión de Observación Electoral ha alertado sobre las diferentes acciones que se consideran **“pre campaña”** y que han venido realizando los diferentes candidatos a la presidencia y al Congreso de la República. La MOE indicó que estas acciones son el ingrediente ideal para la corrupción.

La mayor preocupación está asociada al descontrol sobre la des financiación de las campañas electorales en la medida que esto es “el ingrediente perfecto para la corrupción”. De acuerdo con Camilo Vargas, coordinador del observatorio político y la democracia de la MOE, cuando los candidatos invierten dinero, que no se sabe de dónde sale, y llegan a ocupar un puesto público, **“tienen que retribuir toda esa plata y lo hacen mal versando los gastos públicos”**. Por esto, dijo que es necesario que se controle la financiación de las campañas para así evitar que haya actos de corrupción.

Explicó que  “la campaña a la presidencia no ha empezado, estamos en la pre campaña y preocupa que las consultas partidistas y la recolección de firmas, **empiezan a gastar dinero sin reportar los gastos**”. Dijo que a la MOE han llegado reportes de qué tan legales fueron las vallas para la consulta del partido liberal. (Le puede interesar: ["Las trampas de Vargas Llegas al Sistema de Control Electoral en Colombia"](https://archivo.contagioradio.com/vargas-lleras-campana-cne/))

### **Candidatos que recogen firmas deberían reportar gastos** 

Esto preocupa porque **“no es claro en dónde quedan reportados esos gastos o qué tan legal es el gasto”**. Adicionalmente, los casi 50 candidatos que están recogiendo firma para lanzarse a la presidencia, “incurren en gastos grandes y a veces recogen las firmas con la propaganda de partido políticos o realizan eventos de campaña mientras recogen las firmas”.

Vargas enfatizó en que, en la **legislación** **hay un vacío** en la medida que el Estado no exige que los candidatos que recogen firmas, reporten el dinero que utilizan por lo que la MOE y otras organizaciones le han pedido al Consejo Electoral “que exija rendición de cuentas y hemos invitado a las campañas a reportar de dónde sacan el dinero y en qué lo utilizan”.

<iframe id="audio_22404059" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22404059_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

######  
