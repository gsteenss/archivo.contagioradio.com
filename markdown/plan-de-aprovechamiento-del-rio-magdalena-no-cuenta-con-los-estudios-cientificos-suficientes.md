Title: Plan de Aprovechamiento del Río Magdalena no sería viable social y ambientalmente
Date: 2015-04-15 14:03
Author: CtgAdm
Category: Ambiente, Nacional
Tags: Ambiente y Sociedad, Carlos Andrés Núñez, Cormagdalena, Corporación Autónoma Regional del Río Grande de la Magdalena, Foro Nacional Ambiental, Fundación Fescol, Margarita Flórez, Movimiento Ríos Vivos, Navelena, Plan de Aprovechamiento del Río Magdalena, Río Magdalena
Slug: plan-de-aprovechamiento-del-rio-magdalena-no-cuenta-con-los-estudios-cientificos-suficientes
Status: published

##### Foto: Quimbo.com.co 

<iframe src="http://www.ivoox.com/player_ek_4357961_2_1.html?data=lZiimZ6adY6ZmKiak5aJd6KpkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRitDm0JCuz8fNqc%2FowtGYsMbHrdDiwtGY1dTGtsafsdHO0JDIqYzV0dfc2MrHrMLhysrb1tSPqI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Margarita Flórez, directora de la organización, Ambiente y Sociedad.] 

Esta semana se llevó a cabo el, donde se habló sobre del Plan de Aprovechamiento del Río Magdalen. De acuerdo a Margarita Flórez, directora de la organización, Ambiente y Sociedad, la conclusión a la que ella y otros ambientalistas llegaron tras el evento, es que no existen los estudios científicos suficientes para determinar la realización del proyecto, debido a que hay **muy poco conocimiento sobre el funcionamiento de la cuenca.**

El foro denominado “Riesgos y desafíos ambientales del proyecto de navegabilidad del Río Magdalena”, organizado por la Fundación Fescol y el Foro Nacional Ambiental, contó con la participación del director de la Corporación Autónoma Regional del Río Grande de la Magdalena (Cormagdalena), Carlos Andrés Núñez, quien afirmó que **el proyecto se encuentra en la primera etapa desde el año pasado, es decir la preconstrucción.**

Los ambientalistas y las organizaciones sociales, iban con varios interrogantes respecto a los impactos ambientales y la consulta con las comunidades, sin embargo, estos no fueron resueltos, pues según Flórez, “**la presentación de Cormagdalena sobre los riesgos fue muy y deficiente** y se desarrolló en términos de defensa del contrato”.

El contrato ya no cuenta con la participación de la compañía China, sino con el consorcio **Navelena,** perteneciente a una firma brasilera, que tiene a su cargo las obras de **construcción y mantenimiento de 908 kilómetros de canal navegable**, entre los que se encuentra la creación de ocho nuevos puertos, con una inversión privada de 2.000 millones de dólares.

Los expertos en el río Magdalena, señalaron que **no hay más de 40 o 50 documentos que revelen cómo funciona el río**. En cambio, como lo denuncia la ambientalista, “donde están los procesos de licenciamiento ambiental, se dice que hay más de 700 documentos científicos sobre el río”, lo cual sería una mentira, si se contrasta esa información con la de los científicos.

“Lo que se ha dicho es que **el proyecto obedece a una vía más rápida y más económica para sacar el carbón y el petróleo**”, indica la directora de Ambiente y Sociedad, quien agrega que los pocos documentos que hay,  no señalan ningún  tipo de información  que evidencien cuáles serían los impactos socio-ambientales a los que conllevaría el Plan.

Flórez, afirma que las comunidades **han sido objeto de socialización pero nunca de consulta,** lo que ha dejado a los ciudadanos en un estado de indefensión, ya que no se sabe si la información será pública, y además, hasta el momento no si vallan a existir audiencias públicas para monitorear el tema ambiental.

Cabe recordar que el Plan Maestro de Aprovechamiento del río Magdalena planea la construcción de **17 represas para el año 2020, las cuales implicarían la reubicación de varias comunidades indígenas, campesinas y pesqueras**, además de los daños ambientales que este proyecto podría ocasionar. Es por eso, que las comunidades continúan movilizándose en resistencia al Plan, por lo que la siguiente manifestación empezará en agosto en La Dorada (Caldas) y concluirá en Barranquilla el 12 de octubre. [(Ver nota relacionada).](https://archivo.contagioradio.com/?s=r%C3%ADo+magdalena)
