Title: Joven de 20 años muere por impacto de fusil de las FFMM en Argelia
Date: 2015-11-19 13:23
Category: Movilización, Nacional
Tags: Argelia, Cauca, cultivos ilícitos, Derechos Humanos, erradicación de coca, hoja de coca, proceso de paz, Radio derechos Humanos
Slug: 2000-efectivos-de-la-fuerza-publica-atacan-a-civiles-en-argelia-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Miller-Bermeo-Acosta-marcha-argelia-cauca-erradicacion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

######  

<iframe src="http://www.ivoox.com/player_ek_9443857_2_1.html?data=mpmhlZ2Ze46ZmKiakpiJd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMLWytnO0NnJb8XZjKrZjbLFssjjhpewjajFucTVjNLix9fJb9Hj05DWz9XFp9XjjMnSjcvZt8rgjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Orlando Bolaños, habitante "El Mango"] 

###### [19 Nov 2015]

En el municipio de Argelia, Cauca y otras poblaciones aledañas, la comunidad denuncia que **soldados del Ejército Nacional, ESMAD y policía llegaron con un grupo de erradicadores de coca que en total suman más de dos mil agentes de la fuerza pública, frente a este hecho la comunidad se movilizó para evitar la invasión de sus territorio lo que causa el uso de la fuerza desmedida por parte de la los agentes del estado que han utilizado sus armas de fuego contra la población. **

Aproximadamente dos mil civiles se encuentran atemorizados por cuenta de los ametrallamientos que proviene de tres helicópteros. Los ataques de la fuerza pública acabaron   con la vida de **Miller Bermeo Acosta** de 20 años, integrante del **Movimiento Marcha Patriótica,**  así mismo las acciones han dejado hasta este momento seis personas heridas por armas de fuego. Pobladores reportan que el ejercito a llegado a las casas intimidando a los pobladores diciendo que salgan de sus casas.

### **Las personas heridas por armas de fuego son: ** 

-   Edward Sanchez
-   Carlos Andres Galindez
-   José Yordan
-   Ivan Mosquera
-   Eduar Sanchez
-   Johny Males Gómez

Según explica Orlando Bolaños, habitante del municipio, **el propósito con el que llegaron los efectivos de la fuerza pública es erradicar cultivos de hoja de coca. **La comunidad denuncia que desde **hace meses con la tregua ofrecida por las FARC, el ejercito tomó posesión del territorio y se “incrementaron los operativos militares”**, como asegura Bolaños.

El habitante de Argelia, expresa que la comunidad no entiende cómo pueden estar viviendo estos ataques por parte de los militares, con la excusa de la erradicación, sin tener en cuenta que **en el marco de los acuerdos en la Habana** el presidente Juan Manuel Santos había ordenado la suspensión de la erradicación de estos cultivos.

De acuerdo al relato de Bolaños, sobre las dos de la madrugada las fuerzas militares se ubicaron en las veredas del Encanto, el caserío de la Playa, la Mina, Sinai y Cristales, cerraron la vía que conduce hacia la cabecera municipal de Argelia, e iniciaron los ataques contra la población. En este momento **"están tirando plomo"** expresa el habitante, quien advierte **"si el Gobierno no atiende la situación, habrán muchos muertos en Argelia"**.

Por su parte la comunidad ha tomado la decisión de no dejar erradicar estos cultivos, pese a que los militares han indicado que “la coca está patrocinando a la guerrilla” y que “los campesinos de acá somos terroristas”, dice Orlando, quien aclara que la población no tiene ningún tipo de vínculo con grupos guerrilleros.

**"En pleno Proceso de Paz, las fuerza militares se han desplegado por todo el municipio de Argelia, mostrado su odio contra la población (...) si esta situación continúa no nos podemos quedar”**, afirma Bolaños.

<iframe src="http://www.ivoox.com/player_ek_9444458_2_1.html?data=mpmhlpmZfI6ZmKiakp6Jd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjMLWytnO0NnJb8XZjKrZjbLFssjjhpewjajFucTVjNLix9fJb9Hj05DWz9XFp9XjjMnSjcvZt8rgjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

La comunidad solicita de manera urgente la presencia de autoridades nacionales e internacionales para que se acompañe a la población y se detengan los ataques.

En desarrollo...
