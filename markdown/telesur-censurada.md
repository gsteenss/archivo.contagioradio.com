Title: Gobierno argentino censura a Telesur “a favor de CNN” Adolfo Pérez
Date: 2016-03-29 18:39
Category: El mundo, Política
Tags: Argenitna, Mauricio Macri, Telesur
Slug: telesur-censurada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/macri-telesur-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: caraotadigital] 

###### [29 Mar 2016] 

Las reacciones ante el anuncio por parte del gobierno argentino, de retirar su participación en la cadena Multiestatal “Telesur”, no se han hecho esperar. Adolfo Pérez, integrante del consejo asesor de la cadena y premio nobel de paz, afirmó que la decisión del gobierno de Macri es una “censura a favor de CNN”.

La fuerte crítica se suscitó a raíz del anuncio del pasado Domingo en que Hernán Lombardi y el secretario de comunicación, Jorge Grecco argentino, publicaron un acuerdo de dar por finalizada  su participación  con la cadena televisiva, de la cual, el gobierno de ese país, tiene cerca del 16% de participación. Según la información del diario “La Nación” Grecco y Lombardi ya habrían comunicado la decisión sin haber acudido a los canales diplomáticos como está establecido.

Por otro lado, el analista Atilio Borón, señala que la información internacional será proporcionada por un par de cadenas televisivas como CNN y TNT24, predominando los intereses políticos y económicos de una clase dominante, alejando el pluralismo mediático e informativo que tanto exige el actual gobierno de Macri.

[TeleSur](https://archivo.contagioradio.com/decision-de-grupo-clarin-es-discriminatoria-patricia-villegas-directora-telesur/) señala que este tipo de decisiones exponen la parcialidad que se maneja en la administración del presidente argentino, ya que no solo se retira de la emisión de la plataforma  estatal, sino que su inclusión en los cables privados deja de ser obligatoria.

Mientras tanto, ALER (Asociación Latinoamericana de Educación Radiofónica), frente al abandono del gobierno argentino de la señal informativa multiestatal, considera que las declaraciones expuestas en un medio conservador como  LA NACIÓN, pretenden defender las posturas del gobierno liderado por Mauricio Macri.

Esta cadena multiestatal quedaría integrada por Venezuela, Cuba, Ecuador, Nicaragua, Bolivia y Uruguay, reafirmando que en el defender el pluralismo mediático se acude a la censura.
