Title: El Vuelo del Alcaraván, memorias de lucha en Arauca
Date: 2017-03-21 15:59
Category: Cultura, yoreporto
Tags: Arauca, Documentales, Fundación Joel Sierra
Slug: vuelo-alcaravan-documental
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/el-vuelo-del-alcatraz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El vuelo del Alcaraván 

#### **Por Andrés Gamboa - Yo Reporto ** 

###### 21 Mar 2017 

Arauca, es una región selvática donde los colonizadores se hicieron a una tierra y la volvieron productiva sin tener centros de salud, escuelas ni carreteras. Joel Sierra fue campesino trabajador, organizado en la Asociación Nacional de Usuarios Campesinos ANUC y defensor de derechos humanos.  Siendo un referente social, fue asesinado por el Estado junto con otros cuatro lideres sociales de una forma macabra.

La fundación de derechos humanos Joel Sierra, “nace de la necesidad de las comunidades y de las organizaciones sociales de Arauca, por defender el acumulado social, frente a las diferentes agresiones que de parte del estado se implementan en el territorio, entendidas estas en la estigmatización, ejecuciones extra judiciales, implementación del paramilitarismo y el despojo de los territorios para la explotación petrolera. En ese sentido se hace necesario crear un instrumento que sirva para la defensa y protección de los derechos humanos” asegura Marcela Cruz, abogada de la fundación .

Como Fundación, planteamos las problemáticas que a diario viven las comunidades y las vulneraciones de derechos que existen en la región. Por lo tanto las comunidades se reconocen en el trabajo que hace la fundación para visibilizar, denunciar e interlocutar con el estado y con las demás organizaciones a nivel internacional para garantizar la vida.

**El vuelo del Alcaraván**

"El vuelo del Alcaraván", es una coproducción entre el periódico Trochando Sin Fronteras (TSF) y la productora audiovisual Agit Prop, que busca narrar las memorias de las luchas campesinas en esta región desde el trabajo de la Fundación Joel Sierra. Un proyecto que actualmente se encuentra en la etapa de post-producción y que próximamente tendrá su estreno oficial.

\[embed\]https://www.youtube.com/watch?v=iZTwnHYjC2E&t=7s\[/embed\]

"Desde los medios de comunicación populares y alternativos, asumimos el papel de trabajar junto a esas comunidades, buscando presentar esa voz que se contrapone a los intereses de las multinacionales, esa sensibilidad y terquedad nos han permitido juntarnos hace 8 años como periódico". afirmó Eduardo Sogamoso, director del periódico TSF, hemos aprendido a ser amigos, compañeros y luego están nuestros medios de comunicación.

Por esto hoy presentamos el proyecto “El vuelo del alcaraván” como una oportunidad para exponer la dignidad de las personas en los territorios,  de  las apuestas de paz que se están construyendo, de soberanía que la gente construye, obras que las comunidades hacen pero que muchas veces están in-visibilizadas y es importante que se conozcan en Colombia

En aras que se genere esa solidaridad y hermandad entre los pueblos, de esa manera nos hemos lanzado con nuestro ser, con nuestro conocimiento a aportar a esas comunidades. Estuvimos recorriendo varios municipios del departamento de Arauca, estuvimos donde están los campesinos recuperando su territorio en un complejo petrolero, estuvimos recorriendo los paisajes maravillosos de este territorio.

Allí se han construido propuestas que son dignas de conocer, por eso nos atrevemos a desarrollar esta labor aún en las dificultades que se presentan: la estigmatización y señalamientos. En este sentido, aseguró Eduardo, "Nos preocupa muy profundamente la violencia contra líderes, lideresas y defensores y defensoras de derechos humanos. La persistencia de esta violencia pone en riesgo el propósito común de la paz en el país", señaló Martín Santiago, coordinador residente de Naciones Unidas en Colombia” 1

"La invitación es a ver el vuelo del alcaraván" en las palabras del director de este proyecto David Guevara, ya que este es  un documental que suena a arpa llanera y encuadra vida, que convoca a la memoria y reafirma que el país que estas comunidades han soñado está aún en disputa, que a la gente la han matado y la siguen matando, así que lo que uno siente al hacer visibles estas historias es que está haciendo un poco de justicia, que uno está haciendo un pequeño aporte por exponer sus historias.

1.  ###### *<http://www.semana.com/nacion/articulo/onu-preocupada-por-asesinato-de-lideres-sociales/518741>*


