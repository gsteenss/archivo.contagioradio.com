Title: ¿Qué ha pasado con la participación ciudadana en Colombia?
Date: 2020-02-07 17:47
Author: Foro Opina
Category: Opinion
Tags: FARC, Foro opina, Paro Nacional, participación ciudadana, participación política
Slug: que-ha-pasado-con-la-participacion-ciudadana-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/73458765_1261216460731425_8181636639857049600_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El pasado 21 de noviembre marcó un hito en la vida social y política del país. Varias organizaciones hicieron un llamado a la ciudadanía para salir a la calle ese día a manifestar su inconformidad con el gobierno de Duque. Protestaron por su plan de desarrollo, su reforma tributaria, su saboteo al Acuerdo de paz con las **FARC** y su intención de poner en marcha varias reformas que afectan el bolsillo y la dignidad de muchos colombianos y colombianas, angustiados por el acelerado deterioro de sus condiciones de vida.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La movilización del 21N, que llevó a las calles en todo el país a sectores organizados y a personas que espontáneamente participaron en la marcha o decidieron hacer sonar sus cacerolas durante varias noches, no es un acontecimiento aislado. Hace parte de una oleada de protesta que ha venido experimentado el país desde hace más de una década. Y que seguramente no va a parar. Es la expresión de disgusto, de desesperanza, de rabia y –en algunos casos- de impotencia que invade el ánimo de muchas personas que solo quieren vivir dignamente y que no ven cómo.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La protesta se convirtió en la forma más efectiva de hacerse escuchar. Y ha mostrado resultados. Ha obligado a las autoridades públicas a sentarse a negociar y a modificar, así sea muy tímidamente, su actitud frente a las justas demandas ciudadanas. Y, en buena parte, la gente ha optado por esa vía porque las instituciones participativas que fueron creadas hace un poco más de tres décadas no fueron efectivas. En los años ochenta, cuando fueron reglamentados los primeros mecanismos de participación, la gente los vio con ilusión. En un país que pasaba por una aguda crisis política, la apertura de espacios para interactuar con las autoridades generó una alta expectativa, que fue ratificada luego de la aprobación de la nueva Constitución en 1991.  

<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->

#### Sin embargo, pronto llegó el desencanto.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los dispositivos de participación fueron perdiendo alcance y credibilidad. Por varias razones: porque ni el Gobierno nacional ni las autoridades departamentales y municipales hicieron lo necesario para que operaran como medios para hacer que la gente participara en las decisiones públicas. Pero también porque la gente los desconocía o no tenía interés en hacer uso de ellos, bien por indiferencia, bien porque les resultaba mejor acudir al intermediario político para que les resolviera sus problemas a cambio de su voto en las elecciones. O porque la mayoría de esas instituciones fueron diseñadas para consultar, no para concertar ni decidir.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante estas falencias de la institucionalidad participativa y el deterioro de la calidad de vida, varios sectores organizados comenzaron a protestar: hombres y mujeres, los jóvenes, las víctimas del conflicto armado, las comunidades locales en rechazo a la explotación de los recursos naturales, los pobladores urbanos ante la mala calidad de los servicios públicos, los usuarios del transporte público, los indígenas y las comunidades negras, los trabajadores del campo, los pequeños mineros, por mencionar algunos sectores.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La movilización ciudadana se convirtió en el pan de cada día y el gobierno –en muchos casos- se la pasó apagando incendios, haciendo promesas que sabía que no podría cumplir.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hoy la participación se expresa así, en las calles, en las movilizaciones demostrativas de sectores ciudadanos que no solo quieren decir: “aquí estamos”, sino que aspiran a que las decisiones públicas beneficien a las mayorías, que la corrupción cese y que la dirigencia política obre en función del bien común. Las instituciones participativas siguen operando, pero la acción colectiva es hoy la forma más expedita de participar.   

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las autoridades son reacias a dialogar, mucho más a llegar a acuerdos con la ciudadanía. Se han sentado a la mesa, no por iniciativa propia, sino porque se han visto obligados a hacerlo. La democracia funciona hoy en las calles. Y los gobiernos tendrán que entender -tarde o temprano- que ello es así. Han gobernado a sus anchas durante décadas. Pero ese círculo de poder se estrecha cada vez más. La participación ciudadana, más por la vía de la movilización que a través de canales institucionales, comienza a ser efectiva. ¿Será que las autoridades públicas y, en particular, el gobierno de Duque, mantendrán su misma actitud de autismo ante los reclamos ciudadanos?

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La **Fundación Foro Nacional por Colombia** es una organización civil no gubernamental sin ánimo de lucro, creada en 1982 por iniciativa de intelectuales colombianos comprometidos con el fortalecimiento de la democracia y la promoción de la justicia social, la paz y la convivencia. El propósito de nuestro trabajo es crear las condiciones para el ejercicio de una ciudadanía activa con capacidad de incidencia en los asuntos públicos. El pluralismo, la participación ciudadana, la concertación democrática, la corresponsabilidad y la solidaridad son la base para el desarrollo de nuestra misión, con un enfoque diferencial (de género, generación y etnia). Desde sus inicios, Foro rechazó la violencia como forma de acción política. Por ello cobijó la propuesta de una salida negociada al conflicto armado y del fomento de una cultura democrática. Luego de la firma del Acuerdo entre el Gobierno y las FARC, Foro ha orientado su quehacer hacia el objetivo estratégico de contribuir a la construcción de la paz y la convivencia en Colombia. Foro es una entidad descentralizada con sede en Bogotá y con tres capítulos regionales en Bogotá (Foro Región Central), Barranquilla (Foro Costa Atlántica) y Cali (Foro Suroccidente). 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

[Lea mas columnas de Foro Opina  
](https://archivo.contagioradio.com/author/foro-opina/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Contáctenos: 316 697 8026 –  (1) 282 2550

<!-- /wp:paragraph -->
