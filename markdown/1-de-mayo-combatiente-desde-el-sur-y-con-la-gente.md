Title: Así se vivió el 1° de Mayo en el Sur de Bogotá
Date: 2015-05-02 11:19
Author: CtgAdm
Category: Movilización, Nacional
Tags: 1 de Mayo Bogotá, 1 de Mayo Bogotá Bosa, 1 de Mayo Bogotá Ciudad Bolívar, 1 de Mayo Bogotá moviliza el Sur, 1 de Mayo Bogotá Sur, 1 de Mayo Bogotá USME
Slug: 1-de-mayo-combatiente-desde-el-sur-y-con-la-gente
Status: published

###### Foto:Contagioradio.com 

**1° de Mayo combatiente desde el sur y con la gente**, fue el lema más coreado durante todas las marchas convocadas desde las distintas **localidades del sur de Bogotá**, que confluyeron en Kennedy.

Desde las **8 de la mañana en las localidades de Usme, Ciudad Bolívar, Bosa y Kennedy,** y en distintos puntos de encuentro cientos de manifestantes comenzaron a marchar por las localidades en un tono festivo pero combativo explicando e informando a los vecinos sobre la movilización del 1° de Mayo.

Distintas **expresiones artísticas** acompañaban los **grupos informativos barriales** como performances reivindicativas de los derechos de los trabajadores y denunciando la manipulación mediática de los medios de comunicación de masas en el país.

**Alrededor de las 2 de la tarde todas las marchas confluyeron en Kennedy** donde realizaron un último recorrido hasta llegar al parque de Villas Torres, donde se encuentra el **centro social juvenil Conspiracción**, para poder confluir leer el manifiesto conjunto, almorzar y celebrar el día de los trabajadores con distintos conciertos de bandas de las localidades.

En todo momento la marcha discurrió en un **ambiente pacífico**, liderado por el movimiento social Congreso de los Pueblos y distintos colectivos sociales de las localidades del sur.

Los principales lemas hacían referencia a los **derechos de los trabajadores** y en concreto la gran mayoría se solidarizaban con las **movilizaciones de la educación** y por el aniversario del **asesinato de Nicolás Neira**.

Galería de imágenes de la movilización del 1° de Mayo desde el sur de Bogotá:

\
