Title: Con acuerdo municipal Urrao le dice 'no' a la minería
Date: 2017-08-30 15:00
Category: Ambiente, Nacional
Tags: Antioquia, Mineria, Urrao
Slug: mineria_urrao
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/mineriaypaz.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Gabriel Galindo] 

###### [30 Ago 2017] 

Urrao se ha convertido en el tercer municipio antioqueño que mediante un acuerdo del concejo se ha decidido prohibir la minería de metales. Así lo decidieron los concejales del el pasado sábado al aprobar el acuerdo 009 “por medio del cual se dictan medidas para la defensa del patrimonio ecológico y natural del municipio de Urrao, Antioquia y se adoptan otras determinaciones”.

Así las cosas, la prohibición de actividades extractivistas ya sea por consultas populares o por acuerdos municipales continúa en aumento. En el caso de Urrao no es un tema nuevo. **Desde el año 2012 se viene cuajando la iniciativa pero el Tribunal Administrativo de Antioquia no permitió que se consolidara la medida,** a partir de esa negativa se fortaleció la movilización social logrando que finalmente se prohibiera dicha actividad.

### ¿Por qué? 

Los pobladores temen por la naturaleza, la vocación agrícola y ecoturística de su municipio que se caracteriza por la disponibilidad de los recursos hídricos, donde además abundan los cultivos de **aguacate, papa, yuca, granadilla, tomate y su principal producto, el café clasificado como de la mejor calidad.**

En materia ambiental también cuentan con ecosistemas de gran importancia como el Páramo del Sol y forma parte del Parque Natural Las Orquídeas que junto a los municipios vecinos de Frontino y Abriaquí, lo integran en su totalidad presentando especies endémicas en su zona.

Sin embargo, esas riquezas, según los habitantes se encuentran en amenaza debido al asedio de empresas que buscan explorar y explotar principalmente oro, plata y platino. De acuerdo con información del municipio existen cinco títulos adjudicados para minería, que no se han hecho válidos por falta de la licencia ambiental expedida por Corpourabá. Por esta situación la comunidad se ha movilizado en contra de la explotación minera en el municipio, se han realizado marchas pacíficas buscando el mecanismo de consulta popular para hacer valer la participación de la comunidad en su territorio, para ello **se formó el grupo “No a la Minería en Urrao”.**

La lucha lleva más de 6 años, pero al fin ha conseguido unirse a las decisiones de Jericó y Támesis, con lo que ya se cuentan tres municipios antioqueños que han prohibido la actividad minera mediante ese mecanismo. **Jericó lo hizo el pasado 28 de mayo al impedir la extracción de oro, y Támesis el 29 de mayo, impidiendo** el avance de proyectos de exploración de oro, plata, cobre y molibdeno, entre otros minerales.

Asimismo cabe recordar que Tolima es otro de los departamentos que se ha sumado a la misma lucha, y ha logrado concretar ocho acuerdos autónomos negándole la entrada a las empresas minero-energéticas.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
