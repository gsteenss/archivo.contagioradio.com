Title: ¿Se agota la esperanza de paz de Iván Marquez y 'El Paisa'?
Date: 2018-10-02 18:11
Author: AdminContagio
Category: Paz, Política
Tags: acuerdo de paz, carta, El Paisa, Iván Márquez, JEP
Slug: esperanza-paz-de-ivan-marquez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/ivan-marquez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: primiciadiario.com] 

###### [2 Oct 2018] 

Según **Camilo González Posso, director de INDEPAZ, la carta de Marquez es un grito desesperado que puede tener doble efecto**: desmoralizar al conjunto del país y las bases de la FARC sobre la implementación del Acuerdo de Paz; pero también puede llamar la atención a los miembros del Senado para establecer correctivos, "porque aunque hay exageraciones, es cierto que hay puntos en los que se debe tomar nota para evitar que se haga trizas lo acordado en La Habana".

Para Gonzalez, hay un espíritu derrotista en la carta que parece afirmar que todo está perdido; no obstante, debería reconocerse que "hay una dinámica de renegociaciones y confrontaciones, pero que es dinámica", y ello permite pensar que no todo está perdido, porque **hay fuerzas impulsando la implementación del Acuerdo de paz ante las cuales no se puede ser derrotista.**

El analista destacó que aún hay muchos procesos que dependen de iniciativas parlamentarias, y es una de las alertas que se puede desprender de la carta, pero indicó que **esa alarma serías más efectiva "si hubiera una iniciativa política más activa y propositiva de parte de Iván Marquez**, que ha tenido tanto peso en el Acuerdo de Paz y en el partido político".

### **La inseguridad jurídica: El primer argumento de Marquez** 

En su comunicación, Marquez señala que hay 3 hechos que ponen en riesgo la paz de Colombia: **"la inseguridad jurídica, las modificaciones al texto original de lo convenido y el incumplimiento de aspectos esenciales del Acuerdo",** refiriéndose en su primer argumento al proceso que afronta Jesús Santrich, por presunto tráfico de coca. (Le puede interesar: ["La paz en Colombia pende de un hilo: Caravana Internacional de Juristas"](https://archivo.contagioradio.com/paz-en-colombia-pende-de-un-hilo/))

En declaraciones recientes sobre ese proceso**, el Fiscal General de la Nación reconoció que no tiene pruebas sobre el caso de Santrich,** lo que para Gonzalez significa que la extradición del exjefe guerrillero es "un acto de fe ciega, y una maniobra construida desde Estados Unidos, bajo la cual no puede operar una justicia soberana y transicional" como es la Jurisdicción Especial para la Paz (JEP).

Sin embargo, el Director de INDEPAZ sostuvo que **todos los actos que signifiquen suspender la relación de Marquez y Montero con la JEP, son negativos para el proceso de paz** y para los excombatientes. Aunque reconoció que el tribunal de paz ha sido eficiente y cauteloso al solicitar la colaboración de la FARC en su desarrollo judicial. (Le puede interesar: ["Hoy se vence el plazo para que Fiscalía entregue pruebas en caso Jesús Santrich"](https://archivo.contagioradio.com/hoy-se-vence-el-plazo-para-que-fiscalia-entregue-pruebas-en-caso-jesus-santrich/))

### **"La realidad nos confronta a que hay que pelear por la paz": Victoria Sadino** 

Para la senadora por la FARC, Victoria Sandino, todo lo que tiene que ver con la implementación del Acuerdo tiene dificultades, ello incluye "los 13 planes de atención entre los que se encuentra el plan nacional de salud rural que no se han puesto en marcha"; de igual forma **los Programas de Desarrollo con Enfoque Territorial (PDET), que la senadora recordó, "eran de implementación inmediata".**

Por lo tanto**, la discusión no se trata de si la carta es optimista o pesimista, sino de un tema de realidad.** Según Sandino, "la realidad es que nosotros le hemos apostado a la construcción de la paz y a eso no renunciamos, tenemos muchísimas dificultades, y esas las tenemos que seguir denunciando". En razón de está visión, la Senadora sostuvo que **este contexto que los confronta, los invita a seguir peleando por la paz.**

### **La misiva de Marquez no es una renuncia al partido político** 

González recordó que en el pasado Marquez ya había manifestado diferencias con posturas asumidas desde el partido FARC, por lo tanto, en la carta "puede haber novedades de detalles y cirustancias" pero son discrepancias conocidas. Adcionalmente, resaltó que al interior de la colectividad hay opiniones encontradas que se resuelven mediante procedimientos internos, por lo que **la misiva no es una renuncia al partido.**

El experto concluyó que **la carta tampoco tendrá efecto en la percepción que tiene el ELN del proceso de paz que adelanta con el Gobierno,** porque el 'congelamiento' de las conversaciones se debe a las definiciones políticas que tanto Iván Duque como la guerrilla han asumido para sentarse en la mesa de diálogos. (Le puede interesar: ["Organizaciones sociales piden gestos de paz al Gobierno y al ELN"](https://archivo.contagioradio.com/piden-paz-gobierno-eln/))

[Carta de Ivan Marquez y "El Paisa"](https://www.scribd.com/document/389980817/Carta-de-Ivan-Marquez-y-El-Paisa#from_embed "View Carta de Ivan Marquez y "El Paisa" on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_12988" class="scribd_iframe_embed" title="Carta de Ivan Marquez y " src="https://www.scribd.com/embeds/389980817/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-LGNdgceqa46pX8uhqdmH&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>  
<iframe id="audio_29072074" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29072074_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 
