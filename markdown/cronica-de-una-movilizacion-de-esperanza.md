Title: Crónica de una movilización de esperanza
Date: 2019-07-27 17:12
Author: CtgAdm
Category: DDHH, Movilización
Tags: 26 de julio, lideres sociales, Minga Nacional por la Vida, Movilización
Slug: cronica-de-una-movilizacion-de-esperanza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/14.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/15.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/20.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/11.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/22.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/23.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/27jpg.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

**Por: Cristian Mora J**

[El cielo parece nublado mientras los primeras personas que vienen a unirse a la movilización por los líderes y lideresas sociales de este 26 de julio comienzan a llegar al Centro Nacional de Memoria Paz y Reconciliación. **Yeison Mosquera, líder proveniente de Ríosucio, Chocó** alza la vista al cielo con semblante preocupado, “Dios no va a permitir que hoy haga un mal día”, afirma con convicción. ]

[A tan solo algunos metros **Virgelina Chará, tejedora, líder comunitaria**. defensora de los derechos humanos e incluso nominada al Nobel de paz en 2005 coordina los últimos detalles junto a los integrantes de su organización antes de unirse a la comitiva que caminará hasta la Plaza de Bolívar.]

[“Más que marchar, yo vine a exigir, porque las movilizaciones son acciones de exigencia y de visibilidad ante lo que está pasando, y en Colombia está pasando algo” sostiene Virgelina quien desde hace muchos años viene trabajando con la Fundación ASOMUJER junto a mujeres víctimas del desplazamiento y  familiares de desaparecidos. ]

\[caption id="attachment\_71350" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/11-1024x682.jpg){.wp-image-71350 .size-large width="1024" height="682"} Movilización a la altura de la calle 26, frente al Centro de Memoria, Paz y reconciliación\[/caption\]

[Las personas continúan llegando en este atardecer de viernes al centro de la ciudad y como si Yeison Mosquera hubiese predicho el clima, pese a los nubarrones y el agua que a veces parecería intenta desbordarse, en el cielo se forma un arcoiris que cruza de lado a lado el horizonte, como un presagio de la jornada de hoy. ]

\[caption id="attachment\_71347" align="aligncenter" width="682"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/15-682x1024.jpg){.wp-image-71347 .size-large width="682" height="1024"} Réplica de ‘El Grito’ de Edvard Munch\[/caption\]

[La movilidad se paraliza y las personas comienzan a caminar a lo largo de la calle 26, entre la multitud, destacando por su altura, una réplica de aquella famosa obra, ‘El Grito’ de Edvard Munch surge entre los asistentes como el símbolo de una voz, que pese a las amenazas, pese a los asesinatos, no puede ser acallada. ]

 

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/20-682x1024.jpg){.aligncenter .size-large .wp-image-71349 width="682" height="1024"}

[La comitiva avanza y esta se vuelve más colorida y llamativa a medida que la noche empieza a caer, banderas ondean en todas las direcciones y entre bastones de mando, extendidos se pueden ver los colores de la bandera indígena que en defensa del territorio también se ha unido a esta movilización. (Le puede interesar: ["Minuto a minuto de la movilización por la vida de las y los líderes \#ElGrito"](https://archivo.contagioradio.com/minuto-a-minuto-de-la-movilizacion-por-la-vida-de-las-y-los-lideres-elgrito/))]

[Es precisamente la población indígena una de las más afectadas por esta problemática, después de la firma del Acuerdo de paz han sido asesinados 142 líderes indígenas, a lo que se suma la estigmatización, a la que han sido expuestos por parte del Gobierno que pese a las frecuentes amenazas y situaciones de asedio por parte de grupos armados, les acusan de estar vinculados a estas organizaciones. ]

[ "La invitación a unir nuestras voces, a unir nuestras fuerzas y nuestras luchas y es la defensa de los líderes" manifestó Luis Kankui, consejero Mayor de la Organización Nacional Indígena de Colombia quien se unió a proclama junto a líderes y lideresas de diferentes partes del país en la Plaza de Bolívar. (Le puede interesar: ["Movilización de este 26 de Julio, un respaldo para líderes sociales"](https://archivo.contagioradio.com/movilizacion-26-de-julio-respaldo-para-lideres-sociales/))]

Pero no solo entre los colores la gente avanza, de todas partes llegan la algarabía, las canciones, los coros cargados de energía y de fuerza:  saxofones, tamboras, clarinetes y flautas unen a la gente en una mezcla de ritmos y música que se funden al son de una misma balada: la de la vida.

\[caption id="attachment\_71346" align="aligncenter" width="1024"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/14-1024x682.jpg){.size-large .wp-image-71346 width="1024" height="682"} La música hace vibrar los corazones de quienes marchan y entonan un canto por la vida.\[/caption\]

A su vez, se abren paso las banderas del partido FARC, la rosa roja entre banderas blancas ondea mientras se puede leer en una gran pancarta el mensaje que muchos comparten y apoyan: "Colombia debe dejar atrás definitivamente los días de la guerra", pero que otros aún no han comprendido.

**Prueba de ello son los más de 130 excombatientes que han sido asesinados desde 2016.** Pese a esta alarmante cifra, la esperanza y el sueño de ver una implementación sólida del Acuerdo, no decaen y hoy son más de 8.000 hombres y mujeres quienes le apuestan a la paz, edificando una nueva vida: desde la comunicación, desde la agricultura, desde el turismo, desde la confección, desde las Juntas de Acción Comunal

Algunos insisten en distinguir y separar los asesinatos de excombatientes de los líderes sociales, pero si esta determinación de volver a comenzar de nuevo, de empoderarse en las veredas y en los Espacios Territoriales de Capacitación y Reincorporación (ETCR), no son un sinónimo del mismo empuje y valentía que demuestran los líderes en sus comunidades, entonces habría que empezar a replantearse esa idea.

\[caption id="attachment\_71351" align="aligncenter" width="682"\]![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/22-682x1024.jpg){.wp-image-71351 .size-large width="682" height="1024"} Movilización a la altura de la Calle 26 con Carrera 10\[/caption\]

En algún lugar de la movilización también marcha **Elizabeth Cometa, lideresa de Villavicencio, Meta**, quien se encuentra en Bogotá desde hace varios días pidiendo medidas de protección para ella y para su familia, afirma que ya ha sido víctima de un atentado y que le han dado ocho días para que deje su territorio, pero de igual forma se mantiene firme en su consigna de denuncia, "hay que ir a los territorios y ver lo que está pasando", expresa.

[Durante la movilización también estuvo presente la Comisión de la Verdad, encabezada por el padre Francisco de Roux, quien previamente invitó a la marcha, a este grito también se sumaron las diferentes Casas de la Verdad, en las que la Comisión viene trabajando como parte de su mandato. “La fractura del ser humano en Colombia ha sido muy profunda y lo importante de esto es encontrarnos en las distintas expresiones políticas, culturales y de género” manifestó el presidente de la Comisión.]

 

Sí, al final llovió, pero no por mucho tiempo, porque así lo supo desde un principio Yeison Mosquera quien fue de los primeros en encabezar la marcha y quien al llegar a la Plaza de Bolívar se dirigió a la muchedumbre en una sentida proclama que leyó junto a varios líderes y lideresas, pese a que no se la aprendió, como nos confesó horas antes, sus palabras, acompañadas de las de Luis Kankui y Mayerlis Angarita, lideresa de 'Narrar para Vivir', fueron motivo de aplausos entre la multitud.

> "Estos crímenes son particularmente graves, sintomáticos de una enfermedad muy arraigada en Colombia, la de querer descabezar, desanimar, eliminar, asustar o exterminar a cualquiera que quiera levantar cabeza, a cualquiera que quiera denunciar una injusticia o proponer una reforma, una solución, una reivindicación popular necesaria y justa".

Así, entre el coro de 'Para el Pueblo lo que es del Pueblo' de Piero, entre las múltiples imágenes de don Temístocles Machado, de María del Pilar Hurtado, Emilsen Manyoma, Gilberto Valencia, Dimar Torres y los 486 defensores y defensoras de DD.HH. asesinados desde la firma del Acuerdo, entre las velas, las pancartas y los nombres de tantas vidas apagadas, poco a poco todos regresan a casa, emocionados, llenos de euforia, pero en el fondo, en silencio entre temor y esperanza, deseando que el día de mañana, no tengamos que registrar un nuevo nombre a esa lista que parece no tener fin.

\

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
