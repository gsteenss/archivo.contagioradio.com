Title: Dos años después continúan capturando campesinos por Paro Agrario
Date: 2015-07-16 13:52
Category: DDHH, Nacional
Tags: David Florez, Defensoría del Pueblo, Ejecuciones Extrajudiciales, Esmer Montilla, Falsos Positivos Judiciales, Fundación DHOC, impunidad, la Oficina del Alto Comisionado de Derechos Humanos de la ONU, la Secretaria de la Gobernación del Meta, marcha patriotica
Slug: dos-anos-despues-continuan-capturando-campesinos-por-paro-agrario
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/unnamed.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Carmela María 

<iframe src="http://www.ivoox.com/player_ek_4872723_2_1.html?data=lZ2klJyWd46ZmKiakp6Jd6Klk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRidThxteYr9TSuMrgzcaSlKiPs9Xm0JDQw9jTb8XZjIqflMvFsNTj1JDd0djNuMrq0NiYzNrIrcTdwtGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [David Flórez, Marcha Patriótica y Fundación DHOC] 

###### [16, Julio. 2015] 

**Esmer Montilla, presidente de la Fundación de Derechos Humanos del Oriente Colombiano**, DHOC, fue detenido este miércoles en horas de la tarde en Villavicencio, situación que el movimiento Marcha Patriótica califica como **un nuevo caso de “falsos positivos judiciales” contra las organizaciones sociales** en Colombia.

“Es otro absurdo judicial donde se quiere criminalizar al movimiento social y donde hay una clara estrategia de represión contra líderes sociales” expresa David Flórez, vocero de Marcha Patriótica e integrante de la Fundación DHOC.

Los cargos por los que se le acusa a Montilla, **son rebelión y secuestro simple durante el paro agrario de 2013,** según relata Flórez, en hechos que corresponden a la decisión de los campesinos manifestantes de sacar a un grupo de policias infiltrados en la movilización y entregarlos a la Defensoría del Pueblo

En el despacho de dicha entidad,** se firmó un acta** por parte de la Oficina del Alto Comisionado de Derechos Humanos de la ONU, la Secretaria de la Gobernación del Meta y la Fundación DHOC. Adicionalmente el gobierno se comprometió a dar garantías a los líderes sociales del paro agrario, asegurando que cesaría la criminalización contra los movimientos sociales. Sin embargo el gobierno sigue incumpliendo y en su lugar responde con “falsos positivos judiciales” como lo denuncia el vocero de MP.

**Esmer Montilla es un Líder campesino de 65 años,** que ha sido desplazado en múltiples ocasiones, motivación para convertirse en un defensor de derechos humanos. La fundación que lidera, **adelanta casos de ejecuciones extrajudiciales** y lleva investigaciones contra algunos generales y un coronel.

Según David Flórez, hasta el momento solo se conoce el texto que avaló la captura, pero esta no se ha legalizado y no se ha realizado la imputación de cargos.

Cabe resaltar que desde su creación a la fecha, ya **se cuentan  96 integrantes de MP asesinados,  con 100% de impunidad en cada uno de los casos.**
