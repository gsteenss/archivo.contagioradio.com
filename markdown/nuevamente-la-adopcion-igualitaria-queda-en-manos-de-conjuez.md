Title: Nuevamente la adopción igualitaria queda en manos de conjuez
Date: 2015-04-07 15:41
Author: CtgAdm
Category: LGBTI, Nacional
Tags: adopcion gay, Adopción igualitaria, Angélica Lozano, Convención de Derechos del Niño, Corte Constitucional, Jorge Iván Palacio, LGBTI, Sergio Estrada
Slug: nuevamente-la-adopcion-igualitaria-queda-en-manos-de-conjuez
Status: published

##### Foto: Pulzo.com 

<iframe src="http://www.ivoox.com/player_ek_4320631_2_1.html?data=lZifkpuXdY6ZmKiak5yJd6KllZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRktbZ18bax9PYqYzX0NPX18reb8XZxM7Ry9eJh5SZopaY1dTGtsaf1drhx9HFb9LpxpDdx9fRrdXd04qwlZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Angélica Lozano, Representante a la Cámara Partido Verde] 

Este martes, terminó **en empate la votación en la Sala Plena de la Corte Constitucional** sobre la posibilidad de las parejas del mismo sexo puedan adoptar. Tras la demanda interpuesta por el abogado y docente Sergio Estrada, frente al derecho que tiene los niños y niñas a tener una familia, conformada ya sea por una pareja heterosexual u homosexual.

Debido al empate que se presentó, **se nombrará un conjuez para que se tenga un fallo definitivo frente a la demanda. Jaime Córdoba,** expresidente del alto tribunal, será quien estudie el caso y pronuncie un concepto sobre el tema.

Por su parte, el magistrado Jorge Iván Palacio afirma que **no existen argumentos jurídicos argumentos que impidan que las parejas homosexuales** puedan brindarle un hogar a un niño o niña.

De acuerdo a la ponencia de Palacio,“la Corte Constitucional concluye que la adopción de niños por personas con orientación sexual diversa, en general, y por parejas del mismo sexo, en particular, no afecta por sí misma el interés superior del menor, ni compromete de manera negativa su salud física y mental o su desarrollo integral”.

Cabe recordar que la tutela del profesor Estrada no se basa en los derechos de la comunidad LGBTI, sino en el **derecho de la niñez a tener una familia**. “Hoy se trata del derecho de los niños a la familia el amor y el cuidado, y muchos niños carecen de eso”, afirma la representante a la cámara Angélica Lozano y agrega que la Corte constitucional debe recordar que en el 2012, determinó por unanimidad que “las familias conformadas por parejas homosexuales son familias, y por lo tanto tiene los mimos derechos de cualquier familia”.

Así mismo la representante por el Partido Verde, asegura que hay miles de niños/as que "están creciendo como hijos de nadie", en cambio los adversarios a la adopción igualitaria, que defienden que los niños deben tener una familia con padre y madre, no se han puesto a pensar que **el 58% de los niños y niñas, según DANE, no tienen una familia conformada de esa manera.**

Por lo tanto, para Lozano, el papel de la sociedad en este momento es crucial, y se debe pensar “**si prima el prejuicio o el derecho de la niñez a tener una familia”.**

Cabe recordar, que la demanda del abogado, está en contra de los artículos 64, 66 y 68 de la Ley 1098 de 2006, conocida como la **Ley de Infancia y Adolescencia**, debido a que el debate esta vez  gira en torno a que los niños y niñas tienen el derecho fundamental a una familia, de manera que se tiene en cuenta la ley 12 de 1991, donde se incorpora la** Convención de Derechos del Niño,** allí se argumenta que en la adopción no se debe considerar el sexo de los padres.
