Title: Por una Comisión de Verdad Ambiental en Colombia
Date: 2016-06-21 06:00
Category: CENSAT, Opinion
Tags: Ambiente, Campesina, comision de la verdad, Cumbre Nacional Agraria, río vivos
Slug: por-una-comision-de-verdad-ambiental-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Comisión-de-Verdad-Ambiental-en-Colombia.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Prensa Rural 

#### **Por [CENSAT Agua Viva](https://archivo.contagioradio.com/censat-agua-viva/) - [~~@~~CensatAguaViva](https://twitter.com/CensatAguaViva)

###### 21 Jun 2016 

Indudablemente, la intención de paz, más allá del carácter neoliberal y corporativo que le imprime el gobierno de la prosperidad democrática, ha dado apertura a la disputa por la verdad, dando cabida a elaboraciones diversas que relativizan verdades absolutas que en el caso del conflicto armado han sido construidas por quienes han hecho la guerra, a través de cerramientos ideológicos y mediáticos de las totalizaciones históricas.

En el avance hacia la superación del conflicto armado con las insurgencias, con adelantos significativos en las negociaciones entre el gobierno de Juan Manuel Santos y las Fuerzas Armadas Revolucionarias de Colombia -FARC-, e incipientes acercamientos con el Ejército de Liberación Nacional -ELN-, surgen inquietudes sobre la posibilidad de reconstruir verdad. La de las causas estructurales que dieron origen al conflicto armado; también la relativa a aquellos que patrocinaron y financiaron los conflictos, y a su vez la verdad que reclaman las víctimas humanas, que en la mayoría de los casos no conocen el paradero de sus seres queridos, ni las razones del despojo de tierras cuando fueron desarraigados de sus lugares de creación y reproducción de la vida.

También resuenan las voces que persiguen la construcción de verdad desde otros lugares enunciativos,  frente a los crímenes que se cometieron en contra del ambiente, y que entienden el reconocimiento de la naturaleza como escenario y víctima del conflicto armado como condición necesaria para la restauración y reparación integral de las víctimas -no humanas y humanas- de la guerra.

Respecto a la búsqueda de verdad, ha surgido en Colombia, desde el Movimiento Colombiano en Defensa del Territorio y Afectados por Represas Ríos Vivos y CENSAT Agua Viva, la propuesta de una Comisión de Verdad Ambiental, que está integrada en el pliego único de negociación presentado al gobierno Santos por la Cumbre Nacional Agraria, Campesina, Étnica y Popular. La Comisión integra y proyecta el reconocimiento de la naturaleza como sujeto de derecho, y entre otras cosas pone en consideración la constitucionalidad y la legalidad vigentes, y su respectiva aplicación, frente al respeto y cuidado del patrimonio ambiental.

La Comisión, en tanto ámbito de (re)producción de verdad ambiental, incluye, como una de sus tareas iniciales, la reconstrucción de la memoria ambiental y ecológica del país, y con ella la búsqueda de verdad sobre el origen, el desarrollo y las consecuencias de los conflictos socio – ambientales asociados a la confrontación armada, en los que la naturaleza fue escenario y botín de la guerra -incluidos los territorios y los cuerpos de los hombres y las mujeres. De esta manera, es necesario reconocer que los daños ambientales, la muerte de animales, la destrucción de territorios en el marco del conflicto armado no fueron producto sólo de bombardeos y fumigaciones, sino de la militarización de los territorios que cedieron importantes beneficios a sectores empresariales.

Como es de suponerse, parece imposible reconstruir la memoria histórica de cada uno de los conflictos pasados o existentes, por lo que, tal y como en el caso de las víctimas humanas, debe priorizarse la reconstrucción y búsqueda de verdad frente a casos emblemáticos, por ejemplo el emplazamiento de la represa de Urrá I, la palmicultura en Chocó y Tumaco, las fumigaciones en Putumayo y Catatumbo, los derrames petroleros en Arauca, el batallón de Alta Montaña en Sumapaz, la desecación de ciénagas en el Caribe, entre otros.

El análisis de casos permitirá definir: de una parte, los patrones de acción a emprender para la restauración de ciclos naturales y territorios afectados por la confrontación armada, y, de otra, la generación y aplicación de nuevos modelos de reparación social en el proceso de posacuerdo. Además, de definir responsabilidades para que los actores se comprometan en su no repetición. Esto para los casos con características similares a los que sean reconstruidos, analizados y trabajados con rigor y a profundidad.

En síntesis, los hallazgos de la Comisión deberán permitir que dentro de los procesos de ordenamiento territorial se garantice el respeto a los ciclos y procesos de intercambio orgánico entre la naturaleza humana y no humana,  en perspectiva de construcción de paz territorial.

La Comisión de Verdad Ambiental estará integrada por organizaciones, procesos y movimientos sociales que históricamente han realizado trabajo por la defensa de la naturaleza y de los derechos territoriales, y contará con la presencia de delegados de comunidades que han padecido la guerra.  Incluirá también una veeduría internacional para garantizar que los resultados del trabajo de la Comisión en busca de verdad ambiental sean tenidos en cuenta para la reparación integral de las víctimas humanas y en la restauración de la naturaleza no humana.

Así pues, la propuesta apunta a la restauración y reparación integral de la naturaleza en una concepción amplia de la misma, en tanto sujeto de derecho , y persigue la justicia ambiental y el esclarecimiento de verdad en los crímenes y delitos que seis décadas de confrontación armada arrojaron a los territorios urbanos y rurales colombianos.

###### Amigos de la Tierra – Colombia 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de la Contagio Radio.
