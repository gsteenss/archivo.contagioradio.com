Title: "Siempreviva" anuncia fechas de estreno y presenta trailer oficial
Date: 2015-08-26 12:29
Author: AdminContagio
Category: 24 Cuadros, Cultura
Tags: Ana Piñeres, Andrea Gómez, Andrés Parra, Cine Tonalá, Clara María Ochoa, CMO Producciones, Enrique Carriazo, Festival de Cine del Mundo de Montreal, Latido Films, Siempreviva película, Siempreviva trailer
Slug: siempreviva-anuncia-fechas-de-estreno-y-presenta-trailer-oficial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Siempre-viva.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [26 Ago 2015] 

"*Siempreviva*" adaptación cinematográfica de la obra teatral homónima del dramaturgo Miguel Torres, tendrá su premier internacional durante el **Festival de Cine del Mundo de Montreal**. La ópera prima del director **Klych López**, producida por CMO, será proyectada el **6 de septiembre**. (Ver ["Siempreviva", una película ambientada en la toma del Palacio de Justicia](https://archivo.contagioradio.com/siempreviva-una-pelicula-ambientada-en-la-toma-del-palacio-de-justicia/))

La cinta ambientada en los días anteriores y durante la **toma y retoma del Palacio de Justicia** en Bogotá, ocurrida el 6 de noviembre de 1985, presentó la noche del martes su trailer oficial en cine Tonalá de la capital colombiana, anunciando su **estreno en salas colombianas** para el próximo **1 de Octubre**.

<iframe src="https://www.youtube.com/embed/1EFokEx31u4" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Con la amplia experiencia de **Clara María Ochoa** y **Ana Piñeres** en la producción, sumada al reparto encabezado por **Andrea Gómez**, **Andres Parra**, **Enrique Carriazo** y **Fernando Arévalo** entre otros grandes actores, "*Siempreviva*" promete ser un " un documento a la memoria histórica y de patrimonio audiovisual" de gran calidad. El film tendrá distribución internacional por parte de la empresa española **Latido Films**, la misma de la ganadora del Oscar "El secreto de sus ojos", y "Violeta se fue a los cielos".

**Sinopsis:**

La economía no va bien y en unos meses Lucía perderá la casa hipotecada que comparte con sus dos hijos, don Carlos el dueño de la compraventa, Sergio, payaso de día, mesero de noche, y su esposa Victoria. Julieta, la hija menor, es la salvación, acaba de graduarse como abogada, pero la mañana del 6 de noviembre de 1985 Julieta sale a su trabajo en el Palacio de Justicia y nunca regresa.

Hay testigos que dicen que la vieron salir con vida después de que el Palacio se consumió en llamas por una toma guerrillera, la vida de los habitantes de la casa no volverá a ser la misma.

Basada en hechos reales ocurridos en Colombia en 1985.
