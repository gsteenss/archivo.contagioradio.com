Title: "Políticas de migración de la UE pasan por encima de los DDHH", CEAR
Date: 2015-09-03 17:41
Category: El mundo, Entrevistas, Política
Tags: CEAR, Europa, migración, muertes de migrantes, Unión europea
Slug: politicas-de-migracion-de-la-ue-pasan-por-encima-de-los-ddhh-cear
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/7443325-11471000-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: [lejournalinternational.fr]

<iframe src="http://www.ivoox.com/player_ek_7907316_2_1.html?data=mJ6dmZiVeo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaZS6hqifh6eltNDghqigh52ouMrXwtiYxsqPscrb08bQy4qnd4atlNOYxsqPsMKf1sqY0sbXpc%2Bf0dTfjcrSp8rhwpDRj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Nuria Díaz,  CEAR] 

###### [3 sep 2015] 

En los últimos días han muerto cientos de inmigrantes intentado atravesar el mar Mediterráneo para llegar a Europa, debido a que por un lado, deben salir huyendo por la situación de guerra y pobreza en la que viven en sus naciones, y por otro, las medidas de Unión Europea no son suficientes para atender a los más de 60 millones de refugiados.

Nuria Díaz, quien hace parte de la Comisión Española de Ayuda al Refugiado, CEAR, asegura que es necesario que la Unión Europea realice un cambio urgente de sus políticas de migración, ya que estas no están siendo suficientes para impedir que “se dejen perder vidas humanas en el mediterráneo”.

“Las políticas de migración de la Unión Europea están centradas en el refuerzo y control de las fronteras pasando por encima de los derechos humanos”, asegura Díaz, quien añade que los Estados no se ponen de acuerdo para resolver este problema, causado por la guerra, pobreza y condiciones económicas y políticas en las que viven las personas de países como Siria, Afganistán, Eritrea, Nigeria, Somalia entre otros.

De acuerdo al “Informe 2015: Las personas refugiadas en España y Europa”, Días, afirma que hace falta voluntad política para enfrentar la grave crisis de migración, además, otro de los problemas es que algunos medios de información y otras figuras políticas están generando que se desarrolle un sentimiento de de xenofobia en los europeos, que muchas veces ven como “plagas” a los migrantes.

Para la integrante del CEAR, es claro que los países de la Unión Europea no se han logrado poner de acuerdo para tomar medidas eficientes para ayudar a los inmigrantes, es por eso que desde el CEAR “se exige un cambio de enfoque, poner más vallas, más muros y prácticas ilegales en las fronteras, no va a frenar la situación”, dice Díaz.

Por ahora las medidas que se deben tomar son  a corto plazo, la principal es que todos los países de la UE, velen para que no sigan ocurriendo catástrofes en donde se pierden cientos de vidas, como el naufragio que hubo este lunes en donde 37 personas perdieron la vida.

   
   
   
   
 
