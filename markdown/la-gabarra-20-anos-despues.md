Title: La Gabarra, 20 años después
Date: 2019-08-26 13:24
Author: CtgAdm
Category: Especiales Contagio Radio, Nacional
Tags: La Gabarra, memoria, Norte de Santander, reportajes
Slug: la-gabarra-20-anos-despues
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_0600.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_0630.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1231.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1283-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1283.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1307.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2039.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2265.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2278.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2316.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2343.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2265-1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/la-gabarra-20-años.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/la-gabarra-20-años-despues-javier-sule.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### reportajes

LA GABARRA, 20 AÑOS DESPUÉS
---------------------------

La localidad del Norte de Santander conmemoró el vigésimo aniversario de la masacre paramilitar sin haber sido reparada y obligada a seguir viviendo de la coca como forma de subsistencia.

Texto y fotos: Javier Sulé

###### 26 Agosto 2019

Pedro Josías nació en el Cocuy, iba para sacerdote y tras conocer La Gabarra en un paseo se quedó aquí para siempre. Formó su familia y lleva 31 años trabajando como profesor en este corregimiento del municipio de Tibú, en la subregión del Catatumbo, en el Norte de Santander. “**Me  enamoré del campo, de la selva, del río y de su gente**”, dice. Además de dar clase, la música y el deporte son sus otras dos pasiones. Le compuso canciones a la región y ha enseñado a cientos de niños a jugar a fútbol como entrenador en la escuela que creó para practicarlo.

Cuando llegó en el 88, la coca todavía no había arraigado en la localidad. Cinco años después, la gente sí empezó a sembrarla y se expandió por todo el territorio. *“En las veredas donde estuve de profesor terminaron por cultivarla porque veían que todo el mundo hacía plata. Lo malo es que tal como la ganaban se la gastaban en tomar trago y divertirse. La bonanza cocalera fue muy visible. Aquí en La Gabarra llegó a haber 15 discotecas y bailaderos, y traían conjuntos vallenatos. Unos dicen que la coca llegó con las FARC y otros que fueron grandes narcotraficantes los que la trajeron*”, recuerda Josías.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2265-1-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2265-1.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2265-1.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2265-1.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2265-1.jpg?resize=370%2C247&ssl=1 370w"}

###### Pedro Josías lleva 31 años como profesor en La Gabarra 

El 21 de agosto de 1999, los paramilitares llegaron al pueblo. “Yo estaba tomando una gaseosa con un compadre cuando vino un pelao diciendo que llegaron unos hombres vestidos de camuflado que no sabía si eran paramilitares, elenos o las FARC. De pronto la gente empezó a correr para todos lados. Yo me fui para mi casa, me encerré y tranqué la puerta. Al poco rato empezó la plomacera”, recuerda Josías. **Carmen García, hoy vocera de la Mesa de Víctimas de Tibú**,  tenía 15 años y trabajaba en una cantina vendiendo cerveza. Ese mismo día, horas antes, hubo una discusión entre narcotraficantes compradores de pasta base de coca al frente de su cantina que acabó con la muerte a tiros de dos de ellos.

>  “Aquello ya fue un trauma para mi porque nunca había habido muertos en La Gabarra. La guerrilla existía pero no le hacían daño a nadie. Con la muerte de los muchachos, echamos candado. Ya por la noche, entraron los paras, se fue la luz y tiraron una bengala que pareció que se hiciera de día. Al momento, oímos que tocaron la puerta de la residencia de al lado y se escucharon una retahíla de disparos. Cuando amanecimos y salimos, empezamos a ver los muertos por todos lados. Sólo en la residencia donde le dije mataron a 17 personas. Total, fueron 38 los muertos”.

Los paramilitares habían pasado antes por el modesto rancho de tablas de Doña Gloria Puente.  “Pasaron por aquí que parecían caballos y en el pueblo dejaron un reguero de muertos”, recuerda. Lo peor para ella estaba por llegar. Doña Gloria ha sacado adelante a 11 hijos, pero desde hace 19 años no volvió a saber nada de dos de ellos. Están desaparecidos. Tenían 15 y 17 años. “Esta es la fecha y no han llegado. Un mes después de la masacre, se fueron juntos a trabajar de raspachines a una vereda y no volvieron, pues yo se que los mató la violencia cuando estaban las Autodefensas. Todavía estoy preguntando donde están, qué pasó con ellos. La fiscalía me tomó muestras de sangre, de saliva y estamos en eso de la reparación. Es triste recordar a mis hijos, eran todavía unos niños. Esa gente vino a hacer daño, mataron a muchas personas inocentes, demasiadas. El único delito mío ha sido parir chinos y estar pendiente de ellos”, dice con lágrimas en los ojos.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1307-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1307.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1307.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1307.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1307.jpg?resize=370%2C247&ssl=1 370w"}

###### Las calles de La Gabarra no están pavimentadas 

El accionar paramilitar tuvo la connivencia y el beneplácito absoluto del Ejército. Así lo confesó el propio Mancuso y los tres generales que fueron condenados por las masacres de La Gabarra y de Tibu al demostrarse su maridaje con las AUC y que reconocieron que los paramilitares no hubieran podido asentarse en el Norte de Santander sin el apoyo de la fuerza publica. La noche de los hechos, los militares se acuartelaron temprano en la base cercana al pueblo y se inhibieron sin salir en defensa de los pobladores. Sólo al día siguiente aparecieron recogiendo los muertos.

El Bloque Catatumbo fue uno de los frentes paramilitares más crueles que actúo en la región. Mancuso y su ejército montaron su mando operativo en La Gabarra. Las masacres, desapariciones y los actos violentos continuaron durante los cinco años que duraron en el pueblo, hasta diciembre de 2004 que se desmovilizaron. La situación produjo un éxodo masivo de personas y el despojo y abandono forzado de tierras. “Eramos unos 95 profesores y quedamos como siete, todos se fueron.  Las fincas cocaleras se las robaron casi todas. Las FARC se habían replegado en Venezuela, venían a ratos a enfrentarlos y luego se perdían”, recuerda Josías. En este tiempo, los paramilitares regularon la vida del pueblo y lo controlaban todo.

> “Vivíamos humillados. Recuerdo un tal Gacha, un paramilitar que se paseaba a caballo por las calles y cuando se lo cruzaba uno había que bajar la cabeza”.

En la memoria de Carmen también están muy presentes las cosas terribles que hicieron los paramilitares como meterle una pulla por la vagina hasta la cabeza a una mujer que estaba en embarazo. “Vivíamos sometidos. Tenían una camioneta que llamábamos la última lágrima porque a quien montaban ahí, ese era el camino a su muerte”. Un día Carmen y otras mujeres dijeron basta: “En todos los negocios ellos se tomaban las cosas sin pagar. Por suerte, había algún comandante de ellos que se dejaba hablar. Les hicimos un paro, cerramos los negocios, nadie les lavaba, organizamos un comité de mujeres e hicimos venir a Mancuso. Yo tenia 17 años y le hablé de frente a él de todo lo que hacían sus hombres. Él sentenció que paramilitar que se tomase una cerveza o utilizara una mujer y no la pagase, lo mataban y que cuidado si me llegase a pasar algo a mi. Empezaron a pagar y me dejaron quieta un tiempo, pero ya me la tenían jurada. Solo esperaban a que diera papaya para sacarme de en medio. Un día atentaron contra mi vida cuando iba por el río. Nos lanzaron una canoa por encima para matarnos. Yo estaba embarazada  y perdí el bebé. Mi mano quedó destrozada”, recuerda.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2316-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2316.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2316.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2316.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2316.jpg?resize=370%2C247&ssl=1 370w"}

###### Doña Gloria Puente sacó adelante a 11 hijos. Desde hace 19 años, dos de ellos, menores de edad, fueron desaparecidos por la violencia paramilitar 

Vivir de la coca
================

Hoy, veinte años después, La Gabarra sigue viviendo directa e indirectamente de la coca. “Mis hijos son raspachines. Aquí mucha gente vive de la raspa. Lo que hace falta es que haya mejores trabajos para el campesino”, reconoce Gloria Puente. Según la Oficina de Naciones Unidas contra la Droga y el Delito (UNODC), el municipio de Tibú, al que pertenece La Gabarra, tiene unas 13.000 hectáreas de coca cultivadas y seria la segunda zona con más coca de Colombia. En el Catatumbo, se estima que hay unas 13.000 familias cocaleras, de las cuales solo 3.000 suscribieron el Programa Nacional de Sustitución de Cultivos PNIS. 93 de estas familias viven en las veredas de los alrededores del Espacio Territorial de Reincorporación de las FARC en Caño Indio donde se impulso uno de los planes piloto de sustitución que no cumplió las expectativas por los retrasos e incumplimientos del gobierno. Caño Indio debía haber sido un espejo para toda la región y no ha funcionado. Muchos campesinos podrían verse obligados a sembrar nuevamente. En la zona de La Gabarra, además, muchos líderes que propugnaban el PNIS fueron amenazados por los grupos armados.

**Maria carvajal, de la Asociación Campesina del Catatumbo** (ASCAMCAT), considera que si tuvieran las necesidades básicas resueltas no habría coca. “El campesino sembró por necesidad,. Si el gobierno quisiera acabar la coca solucionaría los problemas sociales que tenemos en el territorio. Nosotros no necesitamos militares que nos vengan a reprimir, necesitamos soluciones. Necesitamos vivienda, médicos, una educación mejor, carreteras y proyectos productivos en los que podamos transformar la materia prima, que ya no vendiéramos maiz ni cacao sino harina de maiz y chocolate”, señala.

> Desde el campesinado ha habido propuestas de soluciones pero el gobierno nunca ha cumplido con los compromisos.

Según Carvajal, los Campesinos si quieren salir de la coca y tiene la voluntad de hacerlo. “Desde el campesinado ha habido propuestas de soluciones pero el gobierno nunca ha cumplido con los compromisos.  En el 99, teníamos un mandato apoyado por seis mil familias cocaleras en el que se proponía cómo debía ser la sustitución y que decía que debía ser gradual, concertada finca a finca, voluntaria y a realizarse en un plazo de 10 años. Es decir, a medida que el gobierno fuese cumpliendo, el campesino iba cumpliendo porque siempre nos han engañado y necesitábamos garantías porque la coca no se puede arrancar de un día para otro porque sino de qué va a comer la gente si eso es la única economía que hay en el Catatumbo. Sin embargo, la solución del Estado fue la arremetida paramilitar que acabó con la mayoría de líderes y el tejido social del Catatumbo, La gente tuvo que salir por miles hacia Venezuela” explica esta lideresa social superviviente con una dura historia detrás de desplazamiento y violencia.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1283-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1283.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1283.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1283.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1283.jpg?resize=370%2C247&ssl=1 370w"}

###### Típicas embarcaciones de transporte en el embarcadero de la Gabarra 

Veinte años después, La Gabarra tampoco se siente reparada. “Debería darse una reparación integral que tiene que ver con lo colectivo e individual, pero no se han restablecido ni restituido los bienes perdidos y a la mayoría de sus miles de desplazados no se les ha compensado económicamente.  Deberían haber proyectado un hospital y nos vienen tomando el pelo desde hace años.”, lamenta Oscar Rico, del Comité de Impulso formado por diferentes sectores sociales de la localidad que luchan por la reparación de La Gabarra.

Una de las medidas de satisfacción que si es una realidad, aunque no completa, fue la construcción de un megacolegio. Josias no trabaja en él como profesor, pero se siente orgulloso: “Fue una obra del presidente Santos como recompensa a toda la violencia vivida. La verdad es que es una maravilla pero falta todavía que entre a funcionar la parte del internado y dotarle de recursos para que los niños y niñas de las veredas puedan quedarse en régimen interno. Esperemos que para el próximo curso ya funcione completamente”.

![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1231-1024x683.jpg){width="1024" height="683" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1231.jpg?resize=1024%2C683&ssl=1 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1231.jpg?resize=300%2C200&ssl=1 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1231.jpg?resize=768%2C512&ssl=1 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_1231.jpg?resize=370%2C247&ssl=1 370w"}

###### Pintada del ELN en una calle de La Gabarra. La guerrilla del ELN sigue teniendo una gran presencia en la región del Catatumbo 

Las víctimas de La Gabarra y el Catatumbo también se han ido organizando. Carmen García ha impulsado recientemente el movimiento Madres del Catatumbo por la Paz desde donde se le ha hecho un llamado al presidente Duque para que respete el proceso de paz. “No queremos parir ni un hijo más para la güerra e instamos al gobierno a volver a la mesa de negociacianes con el ElN y a buscar dialogo con el EPL. Desde el catatumbo pedimos respeto por las victimas del Catatumbo y tambien a los grupos armados que respeten la vida de los líderes sociales. Tampoco queremos erradicación ni fumigación de la coca porque la única riqueza que nos queda es nuestra tierra. Nos sentimos abandonados”, dice.

Luz Neyda Marín es miembro de la Asociación de Mujeres Campesinas, Negritudes, Emprendedoras para un Buen Futuro (AMUCANEFU) desde donde quieren visibilizar la situación de la mujer campesina en estos territorios y brindarles oportunidades laborales: “Aquí escasea el empleo, no hay muchas oportunidades y vemos niñas de 13 años que solo encuentran como opción casarse, ir a raspar coca o trabajar en un prostíbulo o en una cantina”. La vida de Luz Neyda no ha sido fácil. Trabajó siempre en el campo raspando y cocinando para los jornaleros de la coca. Tras la desaparición de su marido, no quiso saber nada más de la coca y salió desplazada para Venezuela, embarazada de cuatro meses. De regreso a La Gabarra, acaba de abrir una cafetería. Como mujeres víctimas y campesinas, el sueño de Luz Neyda es que sus hijos tengan una vida diferente a las suyas y un futuro mejor. “Ojalá pudiera haber becas de estudio para los hijos e hijas de las víctimas porque, sin oportunidades, lo más fácil para el muchacho es irse para los grupos armados o a raspar”, explica.

<figure>
[![Asentamiento informal de migrantes venezolanos en la Gabarra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_0630-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_0630.jpg)  

<figcaption>
Asentamiento informal de migrantes venezolanos en la Gabarra

</figcaption>
</figure>
<figure>
[![Puesto de control militar a la entrada de La Gabarra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2039-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2039.jpg)  

<figcaption>
Puesto de control militar a la entrada de La Gabarra

</figcaption>
</figure>
<figure>
[![El siempre animado Parque central de La Gabarra es el corazón del pueblo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2343-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/MG_2343.jpg)  

<figcaption>
El siempre animado Parque central de La Gabarra es el corazón del pueblo

</figcaption>
</figure>
Casi tres años después de la firma de la paz, en el Catatumbo se siguen viviendo realidades de guerra. El control del negocio de la coca dejado por las FARC lo tomaron el ELN y el EPL, las otras dos guerrilleras de fuerte influencia en esta zona extremadamente militarizada que comparte para lo mejor y para lo peor más de 100 kilómetros de frontera con Venezuela y en la que también hay ya presencia de grupos armados residuales de la antigua guerrilla de las FARC.

A pesar de todo, el pueblo de La Gabarra está relativamente muy tranquilo, se respira siempre un ambiente muy animado en su parque central y existe una gran vida comercial. Sus calles siguen sin pavimentar, los fines de semana son un desorden y hoy son los venezolanos los que buscan aquí refugio y han formando algunos asentamientos informales. Sin embargo, La Gabarra no ha podido sacarse de encima el estigma negativo que pesa habitualmente sobre todo el Catatumbo. “El poder para cambiar ese estigma lo tienen los medios de comunicación. La Gabarra no es como la pintan. Tenemos muchas cosas por mostrar: una gran biodiversidad, una cultura indígena bari ancestral y el empeño y la lucha de la gente por cambiarle la cara a esto. Que ya no nos muestren como un punto rojo en el mapa. Aquí el campesino está asqueado de la coca, sabe que lo que ha producido son muertos, pero al gobierno le da lo mismo. Muchos procesos y promesas y aquí no cambia nada”, dice Oscar Rico.

Para Josías, el estigma sobre las gentes del Catatumbo también es injusto. “Es verdad que la gente aquí se ha vuelto rebelde porque les han engañado muchas veces, les prometen cosas y luego no hacen nada. Se ha vuelto frentera, pero es la gente mas buena que pueda haber en Colombia. Aquí le ofrecen a uno siempre lo mejor que tienen. Conmigo ha sido así”, agradece el profesor Josías

**Por Javier Sulé**

[![¿Dónde están? el grito que retumbó en el Encuentro por la Verdad](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/desaparecidos-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/encuentro-por-la-verdad/)

#### [¿Dónde están? el grito que retumbó en el Encuentro por la Verdad](https://archivo.contagioradio.com/encuentro-por-la-verdad/)

El Encuentro por la Verdad espacio que unió la experiencia de buscadores nacionales e internacionales, para el fortalecimiento…  
[![Excombatientes de las AUC alertan sobre "campaña de exterminio" en su contra](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/AUC-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/excombatientes-de-las-auc-alertan-sobre-campana-de-exterminio-en-su-contra/)

#### [Excombatientes de las AUC alertan sobre "campaña de exterminio" en su contra](https://archivo.contagioradio.com/excombatientes-de-las-auc-alertan-sobre-campana-de-exterminio-en-su-contra/)

Excombatientes de las AUC atribuyen este incremento de asesinatos contra sus integrantes a disidencias y agentes del Estado  
[![Existe un 99,5% de impunidad en casos de desaparición forzada en Colombia](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Minga-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/existe-un-995-de-impunidad-en-casos-de-desaparicion-forzada-en-colombia/)

#### [Existe un 99,5% de impunidad en casos de desaparición forzada en Colombia](https://archivo.contagioradio.com/existe-un-995-de-impunidad-en-casos-de-desaparicion-forzada-en-colombia/)

Cartografía de la Desaparición Forzada en Colombia analiza y propone la búsqueda de nuevos lenguajes y herramientas para…  
[![Asesinan al comunero Iván Mejía, víctima de la crisis humanitaria en el Cauca](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/06/muerte-de-lideres-sociales-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/muere-lider-indigena/)

#### [Asesinan al comunero Iván Mejía, víctima de la crisis humanitaria en el Cauca](https://archivo.contagioradio.com/muere-lider-indigena/)

Es asesinado el comunero indígena Iván Mejía, en el Resguardo de López Adentro, en Caloto Cauca, fue abatido con…  
[![Faltan garantías del Estado para retorno de Comunidad Wounaan](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/WhatsApp-Image-2019-08-25-at-8.19.42-PM-370x260.jpeg){width="370" height="260"}](https://archivo.contagioradio.com/faltan-garantias-del-estado-para-retorno-de-comunidad-wounaan/)

#### [Faltan garantías del Estado para retorno de Comunidad Wounaan](https://archivo.contagioradio.com/faltan-garantias-del-estado-para-retorno-de-comunidad-wounaan/)

La comunidad indígena Wounaan ya tiene listo el plan de retorno, sin embargo hacen falta las garantías por…
