Title: Militares respaldan a la JEP y mantienen su compromiso con la verdad
Date: 2019-03-15 10:52
Category: Comunidad, Paz
Tags: Fuerzas militares, JEP, militares, verdad
Slug: militares-respaldan-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/Captura-de-pantalla-2019-03-15-a-las-10.50.10-a.m.-e1552665089770.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Mar 2019]

En entrevista concedida a Contagio Radio, el Mayor Cesar Maldonado, presidente de la Fundación Militares por la Reconciliación ONG, aseguró que no ven por ninguna parte la impunidad a la que se refiere el presidente Duque y que por el contrario como fue acordado el sistema mantiene la centralidad de las víctimas que es lo más importante para esa organización. (Le puede interesar: ["¿Qué podría pasar con la Ley Estatutaria de ja JEP en el Congreso?"](https://archivo.contagioradio.com/que-podria-pasar-con-la-jep-en-el-congreso/))

La Fundación, que agrupa a **927 militares que se han acogido a la JEP, estaría en disposición de “hacerle honor” a ese compromiso con la verdad que han asumido**. Sin embargo generales como Mario Montoya, Uscátegui o Rito Alejo del Rio quien estuvo vinculado hasta hace algún tiempo, no hacen parte del grupo, lo que limita el aporte a la verdad que podrían brindar estos altos mandos militares.

### **Tengo que reconocer la valentía de las víctimas de aunar voces para ser escuchadas** 

Según Maldonado una de los aspectos a resaltar de las víctimas es su valentía para hacerse escuchar. Además recalcó que una de las principales peticiones que ven de parte de ellas es que se sepa la verdad, más allá de castigos ejemplarizantes lo que los ayuda a la reconciliación y a avanzar para construir la paz. En la misma línea de mantener el compromiso de la palabra empeñada, están integrantes de las FARC con los que Maldonado ya ha sostenido algunos encuentros según afirmó.

### **Nosotros tenemos parte de la verdad y la vamos a contar** 

Maldonado aseguró que es necesario que cada parte, cada integrante de las FFMM o de los paramilitares o de las FARC, cuente su parte de la verdad pues esta conflicto los agrupó a todos y todos ellos deben aportar a construirla. El Mayor aseguró que esperan que la sociedad en general asuma una postura constructiva que permita y facilite el camino de las víctimas de la guerra para la reconciliación y la verdad.(Le puede interesar: ["Presentan a la JEP informe que vincula a comandante de la cúpula militar con falsos positivos"](https://archivo.contagioradio.com/jep-adolfo-leon-hernandez-martinez/))

<iframe id="audio_33411857" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_33411857_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]

<div class="osd-sms-wrapper">

</div>
