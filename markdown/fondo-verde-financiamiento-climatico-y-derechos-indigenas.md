Title: Fondo Verde, financiamiento climático y derechos indígenas
Date: 2016-10-20 06:00
Category: Ambiente y Sociedad, Opinion
Tags: Ambientalismo. Naciones Unidas, cambio climatico, fondo verde
Slug: fondo-verde-financiamiento-climatico-y-derechos-indigenas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/fondo-verde.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### [**<u>¿</u>El Fondo Verde es un mecanismo adecuado para los derechos de los pueblos indígenas en la lucha contra el cambio climático?**] 

#### Por: [Marisa Amori Fonseca-Asociación Ambiente y Sociedad ](https://archivo.contagioradio.com/margarita-florez/) 

**E**l cambio climático es un tema cada vez más importante para la sociedad. Las políticas que han existido hasta ahora se han enfocado en los aspectos físicos del cambio climático, como la contaminación, mientras que los aspectos sociales no han tenido tanta publicidad. Todavía, algunos grupos de personas, como los pueblos indígenas, están muy impactados por el cambio climático y tienen una percepción distinta de los cambios que la de los gobiernos. De hecho, las comunidades indígenas son observadoras de los ciclos, de la naturaleza y tienen un conocimiento bastante sofisticado sobre los fenómenos naturales. Ellas se han dado cuenta de muchos cambios sobre la temperatura o la estacionalidad, por ejemplo\[1\].

La Comunidad Internacional reaccionó con la **Convención de las Naciones Unidas sobre el cambio climático** en 1992 y políticas más específicas como el **protocolo de Kioto** en 1997. La necesidad de ayudar y financiar a los países que tienen menos recursos se desarrolló y se concretó con la creación del Fondo Verde para el Clima. Este Fondo consiste en la creación de estructuras que permitan financiar proyectos que tienen por objetivo desarrollar prácticas sustentables y bajas en carbono a través de, por ejemplo, inversiones en energía solar. La conferencia de Cancún en 2010 adoptó una serie de mecanismos para seguir y verificar el financiamiento.\[2\]

Pero algunas comunidades fueron puestas de lado en temas como la consulta de proyectos supuestamente verdes\[3\], lo que resulta en un grave error para el combate contra el cambio climático, pues son estas comunidades las que se encuentran involucradas en esta lucha. Esto podemos verlo, por ejemplo, con la creación del Foro internacional de los pueblos indígenas, el cual reúne varias comunidades con el fin de tener más poder e influencia en la toma de decisiones. Otro ejemplo de lo anterior, fue la conferencia sobre los pueblos indígenas y el cambio climático, organizada por el *Grupo internacional de trabajo sobre asuntos indígenas* (IWGIA) en febrero de 2008. El objetivo de esta última era hacer un debate sobre los desafíos y las amenazas que enfrentaban los pueblos indígenas. Allí se consideró que las barreras legales e institucionales, y la falta de reconocimiento de los derechos humanos de estos pueblos, impedían a las comunidades indígenas el poder utilizar estrategias existentes de forma eficaz.

La creación del Foro internacional de los pueblos indígenas sobre el cambio climático\[4\] permitió a los pueblos tener un reconocimiento que no tendrían si no se hubiesen juntado para tener una contribución en los asuntos relativos al impacto al medio ambiente.

Adicionalmente, varios problemas aparecen para las comunidades indígenas en temas relacionados con el financiamiento climático.\[5\]  El hecho de no participar en la toma de decisiones les ha llevado a realizar sus propias reuniones y propuestas. En un taller regional organizado en Lima el 25 y 26 de abril de 2016 sobre los pueblos indígenas y las finanzas del clima, cuyo objetivo era la revisión de los procesos, implementación del Fondo Verde del clima y sus políticas relativas a los pueblos indígenas, se concluyó que: la estructura actual del Fondo Verde no permite garantizar la participación plena y efectiva de los pueblos indígenas y, además, los procesos del Fondo no permiten transmitir la información. Lo anterior resulta obviamente contrario al **convenio 169 OIT** el cual estipula que los gobiernos deberán:

“*a) Consultar a los pueblos interesados, mediante procedimiento apropiados y en particular a través de sus instituciones representativas, cada vez que se prevean medidas legislativas o administrativas susceptibles de afectarles directamente;*

1.  *b) Establecer los medios a través de los cuales los pueblos interesados puedan participar libremente, por lo menos en la misma medida que otros sectores de la población, y a todos los niveles en la adopción de decisiones en instituciones electivas y organismos administrativos y de otra índole responsables de políticas y programas que les conciernan*”.

Por eso las asociaciones que participaron en este taller emitieron varias recomendaciones para poder cumplir con las obligaciones del convenio OIT frente al Fondo Verde. Dentro de esas recomendaciones se encontraba: garantizar la protección de los Derechos Humanos a través del Convenio 169 de la OIT, garantizar el proceso de participación, la creación de un equipo técnico que permite la participación, garantizar espacios de observadores indígenas, garantizar la información y toma de decisiones, y también desarrollar criterios y políticas claramente definidas para el proceso de acreditación de la “entidad acreditada”. Además, pidieron que las comunidades indígenas pudieran participar en los procesos de selección de las entidades acreditadas, así como no acreditar entes con actividades extractivas o contaminantes porque eso estaría contra los principios del Fondo y de los pueblos indígenas. Estas propuestas permitirían a las comunidades indígenas tener un verdadero poder en el financiamiento climático. No obstante, sus sugerencias no tuvieron mucho éxito pues empresas como HSBC y Credit Agricole\[6\], que invierten en energías fósiles y están involucradas en problemas de derechos humanos\[7\], fueron acreditadas en la 12ª reunión de la Junta Directiva.

Es necesario recordar que las entidades implementadoras del Fondo Verde tienen que cumplir con normas y salvaguardas sociales y ambientales adecuadas, para ello es necesario la información y la participación. No obstante, esos procedimientos parecen ser olvidados por el Fondo Verde, el cual impide tener una plena transparencia por su gran complejidad. Por eso, continúa existiendo la preocupación que ya existía en las comunidades indígenas antes de la creación del Fondo Verde sobre los mecanismos para combatir el cambio climático, aún después del Acuerdo de París.

De hecho, varias preguntas siguen latentes sobre el acceso al manejo de los recursos que llegan a las comunidades, ya que la mención de “comunidades indígenas” o “pueblos indígenas” no se encuentra en al articulado, pero sí en los anexos del Acuerdo de París, sin embargo, estos no son muy precisos.  Además, por el momento, no hay mención de los pueblos indígenas en el proceso de acreditación. Únicamente se autorizó, en octubre 2015, a la Federación por la autodeterminación de los pueblos indígenas (FAPI) el ser acreditada como observadora.

Pero además de las dificultades que existen en la estructura del Fondo Verde para las comunidades indígenas, también hay problemas sobre algunos proyectos financiados por el Fondo Verde para promover una actividad más sostenible.

En Panamá, por ejemplo, un proyecto de energía hidroeléctrica tuvo incidencias sobre las comunidades indígenas. Aunque el proyecto fuera efectivamente para cambiar la energía fósil y utilizar energía más limpia, que era uno de sus objetivos, varios errores fueron cometidos por el Fondo Verde. Primero, no hizo un estudio suficiente sobre los impactos de este proyecto sobre las comunidades ngabe, que estaban siendo afectadas, y segundo, estos indígenas no fueron consultados a pesar de los impactos sociales, ambientales y económicos posibles que iban a ocurrir y a pesar de las oposiciones.

En Guatemala\[8\] realizaron el proyecto de la empresa hidroeléctrica Santa Rita, ubicado en el departamento de Alta Verapaz, el cual ganó créditos compensatorios de carbono que podrían comercializarse en el Mercado Europeo y permitiría reducir las emisiones de carbono para llegar a las metas. Pero, igual que en Panamá, este proyecto fue muy controversial, pues no hicieron una consulta previa y libre.

Estos no son casos aislados y podemos preguntarnos si el Fondo verde es realmente una estructura adecuada para los pueblos indígenas, por lo menos si no se cambia el proceso que existe ahora. La *Coordinadora de Organizaciones Indígenas de la Cuenca Amazónica* (COICA\[9\]) considera, por ejemplo, que el financiamiento debería efectuarse de forma directa porque el objetivo no sería captar nuevos fondos, sino asegurarse que los pueblos indígenas tengan un acceso y la posibilidad de manejarlos.

En una reunión de la Organización de las Naciones Unidas de la Alimentación y Agricultura (FAO) en 2016\[10\], el director notó que pocos países se han comprometido a garantizar los derechos de los pueblos indígenas de forma muy clara como se puede ver en el Acuerdo de París.

Por lo tanto, una participación indígena más amplia, la garantía del derecho a la tierra y una gestión de los territorios más grandes sería una respuesta más conforme con los objetivos del Fondo Verde, y de forma más general, más alineada en la lucha/adaptación al cambio climático porque el tema ambiental no puede funcionar si las problemáticas sociales no son solucionadas.

------------------------------------------------------------------------

###### \[1\] <http://www.territorioindigenaygobernanza.com/cambioclimatico.html> 

###### \[2\] <http://unfccc.int/portal_espanol/informacion_basica/la_convencion/conferencias/cancun/items/6212.php> 

###### \[3\] <http://www.elmundo.cr/gobierno-panameno-decide-mantener-polemico-proyecto-hidroelectrico/> 

###### \[4\] <http://www.cinu.org.mx/prensa/especiales/indigenas/> 

###### \[5\] <https://es.scribd.com/document/310374900/Fundamentos-de-las-Finanzas-para-el-Clima-para-los-Pueblos-Indigena> 

###### \[6\] <http://www.financeresponsable.org/fiche-entreprise.php?id_entreprise=15> 

###### \[7\] <http://www.slate.fr/story/100573/fairfinance-oxfam-evaluation-politique-investissement-banques> 

###### \[8\] <https://cmiguate.org/represa-hidroelectrica-verde-provoca-acusaciones-de-flagrantes-violaciones-a-los-derechos-humanos/> 

###### \[9\] <http://www.coica.org.ec/> 

###### \[10\] <http://www.fao.org/home/es/> 
