Title: Las "Casas del terror" paramilitar persisten en Medellín
Date: 2015-10-08 12:34
Category: DDHH, Entrevistas
Tags: Antioquia, Autodefensas Gaitanistas, buenaventura, Casas de pique, Casas del terror, Centro de Consultoría y Conflicto Urbano, Comuna 10 de Medellín, CORPADES, CTI, Fernando Quijano, FFMM, Medellin, Paramilitares en medellín, Paramilitarismo, policia, Subastas de Virginidad, Urabaeños
Slug: las-casas-del-terror-paramilitar-persisten-en-medellin
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Medellin.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:pulzo.com 

<iframe src="http://www.ivoox.com/player_ek_8865137_2_1.html?data=mZ2jl5aXe46ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRkMLnjIqgqIqndoa2osjO1cbXb8XZzZDhx9fWs9OZlKuSpZeJfaWf0cbfw9LNsMrowteY0srWt8rn1crbjcrSb87ZxZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Luis Guillermo Pardo, Director C3] 

###### [8 oct 2015]

Por lo menos **15 "Casas del Terror" se encontraron en la Comuna 10 de Medellín**, que comprende la mayoría de los barrios de centro de la ciudad. Según el **Centro de Consultoría y Conflcito Urbano**, C3, que realizó un estudio de campo entre el 2011 y 2012 se pudo evidenciar que el desmembramiento de personas es una práctica habitual y se sigue realizando en el 2015 a pesar de las denuncias y las evidencias.

Según Luis Guillermo Pardo, director del C3, luego de la publicación del estudio las autoridades judiciales no han realizado acciones para desmantelar los grupos paramilitares ni para ubicar y desmantelar las "Casas del Terror" a pesar de que el estudio concluyó en 2012 y  las evidencias entregadas. **Pardo afirma que a las organizaciones les corresponden las investigaciones sociales y a las autoridades las acciones penales.**

Algunos **detalles que entrega el estudio del C3**, señalan que las "Casas del Terror" están ubicadas en el centro de la ciudad porque se necesitan espacios aislados con mucho ruido al rededor y en algunas ocasiones con doble pared, para evitar la ubicación de los lugares por parte de los vecinos del sector y neutralizar cualquier sospecha por parte de las autoridades de policía.

Pardo explica que la práctica consiste en torturar a las personas, asesinarlas, descuartizarlas y luego empacarlas en bolsas de polietileno duro para luego deshacerse de los cuerpos en los afluentes de ríos que circundan la ciudad. **Muchas veces los paramilitares ajustan cuentas de sus deudores en ese tipo de "casas"** que algunas veces tienen sótanos.

Según cifras de un estudio presentado en el concejo de esa ciudad, **entre 2012 y 2014 se han hallado los** **cuerpos de 196 personas descuartizadas** en diferentes lugares de Medellín, **muchas de las víctimas son tiradas a los rios cercanos** para intentar evadir las investigaciones y desaparecer los cuerpos por completo, información que corrobora la versión entregada por el C3.

Aunque Pardo no señala concretamente al grupo paramilitar que operaría de esa manera, otras investigaciones como las de **CORPADES** tanto en las Comunas de los cerros en Medellín como en las comunas centrales operan y mantienen el control los paramilitares de **los Urabeños** y las **Autodefensas Gaitanistas de Colombia** que también tienen presencia en amplias zonas del departamento de Antioquia.
