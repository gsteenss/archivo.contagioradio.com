Title: Ejército habría torturado, asesinado e intentado desaparecer a Dimar Torres
Date: 2019-04-25 17:14
Author: CtgAdm
Category: DDHH, Nacional
Tags: Catatumbo, Dimar Torres, Ejecuciones Extrajudiciales, Norte de Santander
Slug: ejercito-habria-torturado-asesinado-e-intentado-desaparecer-a-dimar-torres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Dimar-Torres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: BenedictoFarc] 

Luego de que la comunidad del municipio de **Convención en Norte de Santande**r logró recuperar el cuerpo de Dimar Torres asesinado a manos del ejército el pasado 22 de abril, se conoció que el campesino fue torturado, asesinado y su cuerpo quiso ser desaparecido. Dimar fue integrante de las FARC pero  ya estaba trabajando **en la vereda Campo Alegre** al al lado de su padre, un hombre mayor de 70 años de edad al momento de la desaparición.

Según varias fotografías y videos, que este medio se abstiene de publicar por respeto a las víctimas y los lectores, luego de los disparos los campesinos se dirigieron al sitio en el que se escucharon tres disparos encontrando a un pelotón militar cavando un hoyo y el rastro de que por allí habría sido movido un cuerpo. **Según los campesinos lo que intentaban hacer era desaparecer el cuerpo de Torres.**

 

> [\#Video](https://twitter.com/hashtag/Video?src=hash&ref_src=twsrc%5Etfw) 2/2 "que este crimen sea castigado y juzgado como lo manda la ley y que tengan todo el peso de la ley, que no se quedé como los falsos positivos de 2007 que hasta hoy las madres de Soacha y las del Catatumbo no saben cuánto van a pagar los asesinos”.[\#QuePareLaMuerte](https://twitter.com/hashtag/QuePareLaMuerte?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/581a5tv0Lc](https://t.co/581a5tv0Lc)
>
> — AscamcatOficial (@AscamcatOficia) [24 de abril de 2019](https://twitter.com/AscamcatOficia/status/1121156693117763589?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Al encontrarse el cadáver se observaron varias situaciones, una en la que le había sido mutilado su órgano genital, marcas en las manos y las piernas y varios golpes en el cuerpo. Adicionalmente su cráneo quedó desfigurado al parecer por el impacto del proyectil que habría terminado con su vida.[(Lea también: Señalan a Ejército Nacional de asesinar al excombatiente Dimar Torres)](https://archivo.contagioradio.com/excombatiente-dimar-torres-habria-sido-asesinado-por-ejercito-en-el-catatumbo/)

A pesar de que no se conocen los resultados oficiales de medicina legal acerca de las circunstancias concretas de la muerte, en el sentido de si las mutilaciones se realizaron antes o después de la muerte, **la comunidad asegura que es evidente que el ejército es responsable y que los delitos que se cometieron allí fueron tortura, asesinato e intento de desaparición forzada.**

Por su parte, y contrario a los mútiples testimonios de los pobladores que hallaron el cuerpo del excombatiente, el ministro de Defensa, Guillermo Botero sostiene que Dimar murió en medio de un forcejeo con un cabo del Ejército, “según el cabo, le intentaron arrebatar el fusil y en ese forcejeo se dio la muerte. No tengo el informe de Medicina Legal pero suena a que la lucha fue de cuerpo a cuerpo”, afirmó en medio de una rueda de prensa.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
