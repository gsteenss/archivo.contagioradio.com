Title: "Colombia debe analizar el costo-beneficio de la extracción petrolera": Manuel Rodríguez
Date: 2017-06-22 18:38
Category: Ambiente, Entrevistas
Tags: producción petrolera
Slug: extraccion_petrolera_colombia_manuel_rodriguez_becerra
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/petroleo1-e1468449932490.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Confidencial Colombia 

###### [22 Jun 2017] 

Para mayo de 2017, **Colombia tuvo una producción petrolera de 851.000 barriles por día,** lo que quiere decir que dicha actividad disminuyó 0,595 frente a abril. Pero además, desde el Ministerio de Minas también se informó que la producción de gas fue de 905 millones de pies cúbicos por día, lo que representa una disminución de 1,21% con respecto a abril.

Cifras que, en el caso petrolero contrastan con el aumento de la actividad exploratoria de crudo que se tenía prevista para este año. El **gobierno esperaba que se  perforaran 500 pozos, es decir un 108 % más que el año pasado.**

Por otra parte, esta semana la compañía de exploración, operación y consolidación de petróleo y gas GeoPark, anunció cuatro nuevas perforaciones "exitosas" en el Bloque Llanos 34, ubicado en el departamento de Casanare. Además, en respuesta a la baja producción, la Agencia Nacional de Hidrocarburos (ANH), señala que los bajos precios del petróleo **permiten estimar que habrá una mayor confianza de los operadores para reactivar el desarrollo de campos productores. **

### ¿Incoherencia? 

Mientras esta semana se conocía la baja en la producción petrolera, y los planes del gobierno para aumentarla, la semana pasada se celebraba que el Congreso de la República aprobara el proyecto de Ley mediante el cual Colombia se adhiere al acuerdo de París sobre el cambio climático, y **se comprometía a disminuir en un 20%  la emisión de gases de efecto invernadero.**[(Le puede interesar: Ningún lugar de Colombia se salva de los impactos del cambio climático)](https://archivo.contagioradio.com/listo-proyecto-de-ley-para-poner-en-marcha-acuerdo-sobre-cambio-climatico/)

No obstante las afirmaciones y planes del gobierno parecen no ir en la misma vía que el pacto de París. Manuel Rodríguez Becerra, director del Fondo Nacional Ambiental y exministro de ambiente, asegura que lo que debe hacer el país es **“un análisis serio del costo beneficio sobre los proyectos petroleros para determinar cuáles van y cuales no**”, teniendo en cuenta los graves impactos ambientales y sociales que ya se han registrado en varios proyectos extractivistas.

Becerra reconoce que el petróleo, el gas, la generación de energía mediante hidroeléctricas, y la extracción de carbón son necesarias. También manifiesta que si "Colombia no encuentra reservas petroleras va a estar en una situación económica muy complicada” y añade que “**En este momento  no es factible sustituir las fuentes de energía eléctrica”.** No obstante, Colombia debe cumplir con el acuerdo de París, lo que implica una transición, aunque no pueda ser inmediata.

El país se encuentra en una encrucijada, expresa el ambientalista. Pero aclara que es claro que las comunidades actualmente no están dispuestas a entregar sus territorios a las multinacionales ni para petróleo, minerías ni hidroeléctricas. [(Le puede interesar: Las incoherencias de Colombia tras el Acuerdo de París)](https://archivo.contagioradio.com/las-incoherencias-de-colombia-despues-del-acuerdo-de-paris-frente-al-fracking/)

“Hay que entender que todos los sitios no son explotables pero también hay que tener en cuenta que necesitamos el petróleo y la minería, así que lo que se debe es **generar procesos participativos** con las comunidades”, dice el exministro, y concluye, “si se construye la paz, **no se puede ser arbitrario desde Bogotá y  desconocer lo que quiere la gente, porque por eso es que se dan los problemas que tiene el país**”.

<div id="content" class="span8 right right" data-motopress-type="loop" data-motopress-loop-file="loop/loop-single.php">

<article id="post-42670" class="post__holder post-42670 post type-post status-publish format-standard has-post-thumbnail hentry category-actualidad category-ddhh tag-cpdh tag-derechos-humanos tag-medios-de-comunicacion cat-73-id cat-13-id">
<div class="post_content">

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

</div>

</article>

</div>

<div id="sidebar" class="span4 sidebar" data-motopress-type="static-sidebar" data-motopress-sidebar-file="sidebar.php">

</div>
