Title: A Gabo le fue mal en Colombia
Date: 2015-05-06 05:00
Author: CtgAdm
Category: Abilio, Opinion
Tags: Carlos Mario Vega, feria del libro, Feria Internacional del Libro, gabo, Gabriel García Márquez, María Fernanda Cabal, Pilar López, William Ospina
Slug: a-gabo-le-fue-mal-en-colombia
Status: published

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) -[~~@~~Abiliopena](https://twitter.com/Abiliopena)

Esta semana terminó la Feria Internacional del Libro que tenía de "país" invitado a Macondo, como homenaje póstumo a Gabriel García Márquez, en un reconocimiento cargado de un cierto tono de desagravio por el desprecio probado de sectores de las élites gobernantes ante sus posturas políticas, su irreverencia, sus amistades de izquierda.

El homenaje a Gabo era consciente de esa historia y libreros independientes tomaron claro partido por su reivindicación. Ya la ausencia de Colombia en la vida de Gabo, gritaba con fuerza al mundo: su valioso archivo personal fue entregado por su familia a la universidad  de Texas en Estados Unidos y el último adiós se lo dio la solidaridad del pueblo mexicano que con su tradición libertaria lo acogió y lo hizo su hijo. No fue Colombia el país en el que decidió morir. Ya en vida debió huir como paria de esta nación cada que los militares y políticos del poder se sentían amenazados al leer sus textos de libre pensador y cuando se posicionaba del lado de escritores del continente que apostaban por una transformación de las estructuras sociales y que se distanciaban de la hegemonía liberal - conservadora.

Gran parte de su vida la pasó fuera de Colombia en razón de los exilios forzados, aunque el establecimiento, hoy oculte ese transfondo, aunque el presidente Santos haya asistido a su sepelio hace un año.

Los organizadores de Macondo en la feria del libro, a nuestro juicio, contaban con ese desprecio de las élites a la persona de García Marquez y organizaron un escenario nada opulento en el stand de Macondo, parcialmente rico en memoria, en literatura colombiana y latinoamericana. Al lado de el libro robado de la edición de 1967 de Cien  Años de Soledad, encontramos en letra pequeña una de  las crónicas que dio cuenta de la persecución  por la misma doctrina militar del enemigo interno, que persigue todo lo que huela a comunista, como nos lo recordó, poco después de la muerte de García Márquez, la senadora Uribista María Fernanda Cabal y lo repitió en vísperas de la feria.

La crónica fue escrita por Carlos Mario Vega y Pilar López el 31 de marzo de 1981. Habla de llamadas anónimas amenazando al escritor, luego de sus columnas relacionados con la ruptura de relaciones entre  el gobierno de Colombia y el de Cuba y de la información de buena fuente según la cual, sería conducido a las caballerizas de Usaquén para ser interrogado por sus nexos con Cuba, por supuesto tráfico de armas y por supuestas relaciones con la guerrilla.

Se trataba de la misma caballerizas de la Escuela de Caballería del gobierno del Estatuto de Seguridad Nacional de Turbay Ayala y de la mismas comandadas por  el teniente coronel Alfonso Plazas Vega, el de los desaparecidos del Palacio de Justicia, quien prendió fuego al palacio “salvando la democracia maestro”, en el gobierno del amigo de Gabo Belisario Betancourt Cuartas, apenas cuatro años después de este episodio.

William Ospina resume ese sentimiento de colombianos decepcionados con una patria desecha por la mezquindad de la clase dirigente, que desprecia la mayor riqueza de los pueblos, su propia gente:  “lo que uno no entiende es que esos poderes hayan minado con tanto rencor toda expresión de inconformidad, la hayan desautorizado con tanta severidad y la hayan perseguido con tanta saña, cuando su modelo mental no fue capaz de construir un país del que siquiera ellos pudieran sentirse orgullosos”.

Gabo debió salir huyendo de ese país, del que quizás, no se pudo sentir orgulloso. Al menos contó con talento y oportunidades  para poder hacer su vida en otro lugar, suerte distinta a la de los mas de seis millones de víctimas que han debido soportar el peso de su propio anonimato.
