Title: Con nueva cúpula militar uribismo asume control de seguridad y defensa
Date: 2018-12-17 12:31
Author: AdminContagio
Category: DDHH, Nacional
Tags: Alvaro Uribe, cúpula militar, entrevista
Slug: con-nueva-cupula-militar-el-uribismo-asume-control-de-seguridad-y-defensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/DuobNncW4AEfuPY-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: MinDefensa 

###### 17 Nov 2018 

Este lunes serán posesionados los nuevos integrantes de la cúpula militar que estarán a cargo de la seguridad del país; el académico, Ramiro Bejarano expresó algunas de sus preocupaciones sobre dicho nombramiento, una decisión que según el también jurista, significaría el regreso de las chuzadas y ratificaría el poco liderazgo del presidente Duque pues se trata de una cúpula nacida de “las entrañas del doctor Uribe”, sentenció.

Según Bejarano, la cúpula conformada por el almirante Evelio Ramírez, el general Ramsés Rueda a cargo de la Fuerza Aérea Colombiana, el general de la Policía, Óscar Atehortúa, el comandante de las Fuerzas Militares, general, Luis Fernando Navarro Jiménez y el general Nicacio de Jesús Martínez, garantizaría a Uribe "control sobre la defensa y la seguridad nacional", adicionalmente su partido y el Senado en general no se han pronunciado en contra de la decisión, silencio que podría entenderse como una muestra de satisfacción.

**Nueva cúpula, un mal augurio para  acuerdos de paz**

Frente a la continuación de la implementación de los acuerdos, Bejarano se mostró pesimista y concluyó que tanto Gobierno como Uribismo “van a torpedear el proceso de paz” y advierte que con estos nuevos mandos militares podrían regresar los seguimientos y las chuzadas ilegales, como ocurrió hace unos años cuando regía la seguridad democrática. [(Le puede interesar: Nueva Cúpula Militar ¿Vuelve la seguridad democrática? ) ](https://archivo.contagioradio.com/nueva-cupula-militar-vuelve-la-seguridad-democratica/)

De cara al anuncio de cese de operaciones violentas del ELN,  el académico indicó que no le extrañaría que la promesa de un cese al fuego "sea truncada por la desconfianza" pues no existe una voluntad por parte del Gobierno para continuar con las negociaciones, algo que se confirma con el nombramiento de esta cúpula.

<iframe id="audio_30848916" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30848916_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
