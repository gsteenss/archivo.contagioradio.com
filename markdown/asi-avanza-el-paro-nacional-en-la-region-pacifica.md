Title: Así avanza el paro nacional en la Región Pacífica
Date: 2016-05-30 00:07
Category: Movilización, Paro Nacional
Tags: Paro Nacional
Slug: asi-avanza-el-paro-nacional-en-la-region-pacifica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/05/REGION-PACIFICO.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [30 May 2016] 

### **[Valle del Cauca]** 

**10 de Junio:** El viernes 10 de Junio se realizará una homilía popular en el parque de los poetas de la ciudad de Cali a partir de las 5:30 pm

### **7 Junio:** Hacia las 6 de la tarde se iniciará un Cacerolazo en la Plaza Banderas de la ciudad de Cali. 

Trabajadores de UNIMETRO, sindicato de trabajadores del Sistema MIO **están reunidos en los patios del operador, el oriente de Cali**. Se espera que este **miércoles a primera hora salgan a protestar en otro punto de la ciudad.**

De los 14.000 camioneros afiliados en el departamento, por lo menos el 90% se sumarán al a cese de actividades según informó Pedro Aguilar, presidente de la Asociación Colombiana de Camioneros.

**3 de Junio:** En el corregimiento La Delfina se presentan fuertes ataques del ESMAD y las Fuerzas Militares a las comunidades indígenas en 3 puntos de concentración. Sobre el sector hacen presencia por lo menos 350 integrantes del ESMAD y 4 tanquetas que hacia las 2:30 de la tarde arremetieron contra los manifestantes. Se reportan 2 heridos, uno de ellos el **gobernador del resguardo Quipara, Robinson Nequirucama, por disparo de fúsil en el rostro y Sebastián Velásquez herido en una de sus piernas por un disparo de gas lacrimógeno.**

700 indígenas de la organización Aciva RP y la Orivac concentrados en el Resguardo NASA kiwe de la Delfina, volvieron a tomarse la vía. La exigencia es que el presidente se siente con los pueblos indígenas.

**2 de Junio:** Sobre las 4 de la tarde, se registran disturbios en la Universidad del Valle.

[**30 de Mayo:** Aproximadamente 1200 integrantes de comunidades indígenas del Valle  del Cauca se reúnen en el resguardo indígena ubicado en el sector de la Delfina, las comunidades reportan algunos enfrentamientos con el ESMAD hacia a los extremos de Cali y Buenaventura por el ingreso de la fuerza pública a los resguardos.]

[**29 de Mayo: **La ONIC, denuncia que en  el km 50 sector Alto Calima entre Cali y Buenaventura, 100 indígenas fueron retenidos por la Policía Nacional.]

Hacia las 10:30 de la noche de este domingo, se presentaron enfrentamientos entre el Escuadrón Movil Anti Disturbios ESMAD, e indígenas en el lugar conocido como 'la Delfina', Buenaventura, donde resulto herido el comunero Manuel Jovel Dagua con arma de fuego en el brazo izquierdo y otros tres resultaron golpeados por acción de los mismos uniformados según declaraciones de habitantes de la zona.

En estos mismo hechos el comunero indígena  Willington Quibarecama Nequirucama, Embera Chami, de 26 años de edad, murió luego de caer de uno de los puentes cuando trato de esquivar una tractomula que por allí se moviliza, en esta misma acción resulto herido otro joven indígena.

Hasta la publicación de la presente, las comunidades aguardaban por la presencia de organizaciones de DDHH que puedan verificar las agresiones de las que están siendo víctimas las comunidades en el marco de la Minga Nacional.

### **Cauca** 

**08 de Junio:** Los voceros y voceras de la Cumbre Agraria se reúnen en Santander de Quilichao con la comisión de garantes de Derechos Humanos para establecer las condiciones de protección para los manifestantes. El último comunicado de Cumbre Agraria afirma que están listos para establecer la Mesa Única Nacional una vez el gobierno se comprometa con las garantías de DDHH para la movilización.  
**3 de Junio:** En Popayán en medio de la movilización se presenta un choque con la policía que impide el ingreso al centro de la ciudad.  
<iframe src="http://co.ivoox.com/es/player_ej_11716472_2_1.html?data=kpakk5uYe5Ohhpywj5WXaZS1lZmah5yncZOhhpywj5WRaZi3jpWah5yncanZxNnc1JCxpdPdz5CajajTs9PYytPOxtTWb67Z1MaYtMrLrdDiwtGYxsqPmoa3lIqupsjYrc7V1JCwj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**2 de Junio:** En Caldono se denuncia que Gersaín Cerón Cuainás, indígena Nasa, fue asesinado con un arma de fuego que usaba la policía ubicada detrás de los agentes del ESMAD. El hecho se presentó en el punto conocido como El Pital, en el Resguardo Las Mercedes. Otra de las víctimas del ataque fue el indígena Óscar Guetio, herido gravemente con una una bala de munchique. [(Más información)](https://archivo.contagioradio.com/un-indigena-nasa-muerto-y-otro-mas-herido-en-la-minga-agraria-en-cauca/)  
**1 de Junio:** Según reporte de la ACIN, a medio día la Organización Sat Tama Kiwe de Caldono y ACIN se encontraban en 4 puntos de concentración en aproximadamente 18 kilómetros desde La Agustina a La María Piendamó, con aporte de más de 6 mil comuneros del Norte del Cauca y Caldono, quien en mediaciones de Mondomo resistieron el embate de la fuerza pública. Reportaron acciones violentas de la fuerza pública, no solo a la población civil sino en la destrucción de 40 hectáreas de maíz.

**31 de Mayo:** De acuerdo a denuncias de la ONIC, se presentan disparos con fusil por parte de ESMAD  contra indígenas del Cauca. También se asegura que están reprimiendo la movilización con con gases lacrimógenos.

Sobre las 3 de la tarde, autoridades indígenas del Norte del Cauca, denuncian que la fuerza pública hirió a mujer indígena en Santander de Quilichao.

Municipio de Caldono, Corregimiento El Pital, Vereda el Rosal, cerca de las 9:00 de la noche, nuevamente las delegaciones de campesinos del norte del Cauca, fueron retenidos por la Policía y el ESMAD, por aproximadamente 40 minutos, quienes les manifestaron que no podrían continuar su camino. Ante la negativa e imposibilidad de las instituciones en solucionar esta problemática pese la negativa de autoriza el paso los campesinos decidieron continuar con su camino. Estos hechos violan los acuerdos a que se habían llegado con la Administración Departamental.

En el sitio La Agustina, municipio de Santander de Quilichao, cerca de las 9:00 de la noche fue retenido por miembros del ESMAD, el Guardia Indígena Estiven Cuene, comunero del resguardo indígena de Toez Caloto, por participar de las jornadas de protesta, quien posteriormente fue entregado a la ONU.

**30 de Mayo:** Cerca de 2 mil personas de distintas comunidades afro, indígenas y campesinas se preparan para movilizarse en el municipio de Buenos Aires Cauca. Durante el transcurso de la mañana no se ha detectad ningún tipo de ataque o represión por parte del Ejército o del ESMAD.

### Chocó 

<p>
**1 de Junio:** En inmediaciones de la vía que de Quibdó conduce a la ciudad de Pereira en el occidente colombiano, más de mil indígenas, campesinos y afrocolombianos que participaban de la Minga Nacional Agraria campesina Étnica y popular, fueron agredidos por las fuerzas represivas del ESMAD, la Policía y el Ejército.  

<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
  

<iframe src="http://co.ivoox.com/es/player_ej_11714439_2_1.html?data=kpakk5mYd5qhhpywj5WbaZS1lJiah5yncZOhhpywj5WRaZi3jpWah5yncavVzsrgjbHFttPZwpCajbHFb6XZzcvW0MaJdqSfo9rS0Mbaqc%2Fo1tfOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen">
</iframe>
</p>
**31 de Mayo:** A través del comunicado público 001, las comunidades indígenas, afros y campesinas del Chocó, informaron a las autoridades civiles y militares sobre su participación activa desde hoy en la Minga Nacional haciendo presencia entre las vías Quibdo - Medellin y Quibdo - Pereira.

En el escrito las comunidades instan a todos los sectores sociales a unirse desde sus hogares, sus barrios y sus trabajos, apoyando las exigencias elevadas ante los gobiernos nacional, departamental y municipal, a cumplir los acuerdos hechos con las comunidades en el paro agrario de 2013 y la Minga Nacional de 2015 y solicitan el acompañamiento de las diferentes organizaciones de Derechos Humanos, ante los posibles excesos en el uso de la fuerza que pudiesen presentarse.

Según se denuncia, sobre las 4 de la madrugada efectivos del ESMAD, la policía y el Ejército, utilizando armas de fuego, retroexcavadoras y otro tipo de armamento como granadas de gas, destruyeron los cambuches de los manifestantes, “robaron y dañaron toda su comida, su agua y sus implementos personales”, dejando varias personas heridas, entre los que posiblemente se encuentran mujeres, ancianos, niños y niñas en el municipio de Tadó.

### **Nariño** 

**10 de Junio**: En la carretera que comunica pasto con Tumaco se presenta represión del ESMAD a las comunidades indígenas sobre la carretera desde las 6 de la mañana.

El Pueblo Indígena Eperara Siapidara y el Pueblo Awá se suman a las jornadas de movilización sobre la vía Panamericana entre el municipio de Ricaurte y Palmar. Cerca de 1.200 indígenas, más de 800 campesinos y otros de 100 afros participan de la Minga Nacional. Las comunidades denuncian estar cercados por la fuerza pública.

[Actividades]

**Cali:** En la Calle 5 con carrer 94, Estación MIO Meléndez, el próximo 5 de  junio a las 5:30 pm se realizará un cacerolazo en defensa de los bienes públicos del país.  
 
