Title: Temor en Antioquia ante amenaza de paro de paramilitares de las AGC
Date: 2017-07-13 15:10
Category: DDHH, Nacional
Tags: AGC, Antioquia, Panfletos, paramilitares
Slug: temor-en-antioquia-ante-amenaza-de-paro-de-paramilitares-de-las-agc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/Captura-de-pantalla-2017-07-11-a-las-12.16.56-p.m.-e1499793834908.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Google] 

###### [13 Jul. 2017] 

A través de un panfleto los paramilitares autodenominados “Autodefensas Gaitanitas de Colombia” –AGC-, han asegurado que comenzarán este jueves un paro que irá hasta el próximo 15 de julio en cuatro municipios del departamento de Antioquia. **En la misiva aseguran que de no recibir el apoyo de las comunidades comenzarán un paro armado** indefinido. Ante estas amenazas, la población ha manifestado temor por sus vidas.

Los **municipios mencionados en el panfleto son Dabeiba, Buriticá, Anzá y Armenia Mantequilla,** a quienes les ordenan cesar sus actividades económicas, laborales y académicas “invitamos a todas las comunidades que nos acompañen (…) pedimos a las entidades encargadas de la economía en estos municipios, no hacer ningún pedido de materias fuera del municipio”.

Añaden también que **las solicitudes se extienden a las “entidades laborales y educativas” haciendo referencia a hospitales, alcaldías,** flotas de transporte “no prestar sus servicios por estos días”. Le puede interesar: [Paramilitares afianzan presencia en San José de Apartadó](https://archivo.contagioradio.com/paramilitares-afianzan-presencia-en-san-jose-de-apartado/)

De no cumplir las órdenes que se indican en el panfleto, **los paramilitares han amenazado con iniciar un paro armado indefinido** “si vemos que el apoyo de la comunidad es nulo, dejaremos a un lado el paro pacífico para comenzar con un paro armado indefinido”.

El objeto de dicho paro por parte de los paramilitares, dicen en su panfleto, se debe a la conmemoración del asesinato de uno de sus comandantes Andrés Usuga Gil, ocurrido ell 13 de junio de 2017. Le puede interesar: [Paramilitares amenazan a víctimas colombianas exiliadas en España](https://archivo.contagioradio.com/amenazas-de-paramilitares-llegan-a-las-victimas-colombianas-exiliadas-en-espana/)

En varias declaraciones de **los Alcaldes de estos municipios, aseguran que los panfletos comenzaron a aparecer desde hace cerca de 20 días** y que han comunicado al Ejército y a la Policía para que estén atentos a las comunidades, sin embargo se desconocen acciones de combate a esas estructuras en la región.

Cabe recordar que según La Defensoría del Pueblo el Clan Úsuga o "AGC" hace presencia en 22 departamentos de Colombia como Antioquia, Bolívar, Caldas, Casanare, Magdalena, Meta, Nariño, Norte de Santander, Quindío, Risaralda, Cauca, Cesar, Chocó, Córdoba, Sucre, Cundinamarca, y Guajira. Le puede interesar: [Paramilitares irrumpen en Zona Humanitaria Nueva Esperanza en Jiguamiandó, Chocó](https://archivo.contagioradio.com/paramilitares-jiguamiando-choco/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).

###### 
