Title: El agua, los páramos y la vida le ganan un round a los negocios en Colombia
Date: 2016-02-09 16:46
Category: Ambiente, Nacional
Tags: Corte Constitucional, Minería en páramos, PND
Slug: el-agua-los-paramos-y-la-vida-le-ganan-un-round-a-los-negocios-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Páramo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Parques Nacionales Naturales ] 

<iframe src="http://www.ivoox.com/player_ek_10374665_2_1.html?data=kpWgmZmaepahhpywj5aWaZS1lJyah5yncZOhhpywj5WRaZi3jpWah5yncabgjMbU18aJdqSfzdTgjdWJh5SZopbfw9LTt4ztjNHOjdvNqMKfzcqYycbSpc%2Bf1tOY1NTZssWfwpDZ0diRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Alirio Uribe, Polo Democrático] 

###### [9 Feb 2016]

Este lunes la Sala Plena de la Corte Constitucional [[tumbó tres artículos del Plan Nacional de Desarrollo](https://archivo.contagioradio.com/corte-habria-tumbado-licencias-ambientales-express-del-plan-nacional-de-desarrollo/)] que permitían la minería en zonas de protección ambiental, atendiendo a la demanda de inconstitucionalidad interpuesta por algunos Congresistas y Representantes del Polo Democrático y del Partido Verde, entre ellos Alirio Uribe, quien asegura que “**ésta es una noticia muy importante para el país**, teniendo en cuenta la crisis que se está viviendo con el tema de la sequía”, y que hace necesario "**priorizar el agua, la vida y la salud de las comunidades, sobre los negocios de las empresas nacionales y multinacionales**".

En el marco de la crisis ambiental actual no sólo en Colombia sino a nivel mundial, es importante que se protejan los páramos, afirma Uribe, quien cita como ejemplo el páramo de Santurban con el que se abastecen de agua **por lo menos 1 millón 200 mil personas**, y agrega que éste fue un gran debate que senadores como Iván Cepeda y representantes como Alberto Castilla, dieron **en el Congreso**, pero que lo perdieron, "porque allí **se priorizan los negocios sobre la vida de la gente**”.

Pese a que se está esperando el contenido final del fallo de la Sala Plena de la Corte, lo que hasta el momento se conoce es que se declaró inconstitucional la medida que impedía a las víctimas ser restituidas sí reclamaban tierras destinadas para la implementación de megaproyectos, pues como demandaba el Representante “**los megaproyectos no están por encima del derecho que tienen las victimas a que se les restituya la tierra**”.

De otra parte ya no dependerá de decisiones nacionales la implementación de proyectos mineros contra la voluntad de las comunidades o de autoridades locales, pues de acuerdo a la decisión de la Corte **las regiones a través de las CAR podrán ser autónomas para el manejo ambiental y de licencias mineras en los territorios. **Dependerá entonces de las mismas comunidades el monitoreo y control sobre estas instituciones para que cumplan con las medidas de protección ambiental ordenadas, pues como asevera Uribe "los intereses privados no pueden volverse públicos".

El interés del país debe ser el respeto por los derechos humanos, asevera Alirio Uribe e insiste en que la política minera en Colombia no puede seguirse cambiando para favorecer a las empresas, teniendo en cuenta que **es posible que “en el pos acuerdo los conflictos socio-ambientales estén a la orden del día”**.

###### Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u) ]o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
