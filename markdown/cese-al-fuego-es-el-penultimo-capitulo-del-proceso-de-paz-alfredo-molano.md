Title: "Cese al fuego es el penúltimo capítulo del proceso de paz" Alfredo Molano
Date: 2015-07-13 11:39
Category: Entrevistas, Paz
Tags: Alfredo Molano, Centro Democrático, Cese al fuego, Conversacioines de paz en Colombia, dialogos, FARC, habana, Humberto de la Calle, paz, Santos
Slug: cese-al-fuego-es-el-penultimo-capitulo-del-proceso-de-paz-alfredo-molano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Captura-de-pantalla-2015-07-13-a-las-12.37.13.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Pulso 

<iframe src="http://www.ivoox.com/player_ek_4783331_2_1.html?data=lZyllZiXdY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRaZOmpMrgx5DFsIza1srU0ZDJt4zZzZDdx9OJh5SZo6bZ1s7Rs4zXwtWSpZiJhaXo1tHcjcnJsIzk09TQx9jTb8Whhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alfredo Molano, Comisión Histórica del Conflicto y sus Víctimas] 

##### [13 jul 2015] 

El domingo 12 de Julio mediante el comunicado conjunto número 55, las delegaciones de paz de las FARC-EP y el gobierno colombiano, dieron a conocer un acuerdo mediante el cual busca acelerarse el proceso de conversaciones de paz y poner en marcha un cese al fuego y de hostilidades definitivo.

Según expone el comunicado, la sub-comisión técnica que adelanta este tema contará con el apoyo de las Naciones Unidas y la Unasur para acordar el mecanismo idóneo de implementación y verificación del cese de hostilidades. Entre tanto, y teniendo como antesala la decisión de las FARC de decretar un cese al fuego unilateral a partir del domingo 20 de julio, iniciaría un proceso de desescalamiento del conflicto.

El proceso, según anuncia el comunicado, contará con un cronograma para "la implementación de medidas de construcción de confianza."

Para el sociólogo y columnista Alfredo Molano, quien hiciera parte de la Comisión Histórica del Conflicto Armado y sus Víctimas, este sería el "penúltimo capítulo" del proceso de paz entre la insurgencia y el gobierno.

<div>

Si bien, se presentan riesgos y accidentes en un proceso de desescalamiento, para el columnista es claro que "las FARC cuentan con unidad total de mando en sus frentes, mientras el gobierno no cuenta con unidad en todas sus fuerzas", y eso podría generar hechos de guerra que puedan ser mal interpretados o utilizados para atacar el proceso de paz.

"El problema no es solo controlar el paramilitarismo, como han pedido las FARC. El problema también son las manzanas podridas que están siendo estimuladas desde fuera de los batallones, concretamente por el Centro Democrático, y eso puede llevar a otro descalabro", insistió Molano.

La delegación de paz de las FARC insistió en la necesidad de establecer una comisión para el esclarecimiento del fenómeno del paramilitarismo en Colombia, pues consideran que  "es importante que cada una de las determinaciones que estamos adelantando tengan la garantía de la No Repetición de los dramáticos hechos que han caracterizado el conflicto en nuestro país".

</div>

Lea a continuación el Comunicado Conjunto:  
 

<header>
#### COMUNICADO CONJUNTO NO. 55: AGILIZAR EN LA HABANA Y DESESCALAR EN COLOMBIA 

</header>
<div class="itemBodyWrap">

<div class="itemBody gkHasAside">

<div class="itemIntroText">

##### Con el objetivo de:

##### - Fortalecer la confianza de los colombianos y las colombianas en el proceso de paz, y también la confianza entre las delegaciones; 

##### - Agilizar la construcción de acuerdos sobre todos los aspectos restantes de la Agenda del Acuerdo General; y 

##### - Crear las condiciones para la puesta en marcha del Cese al Fuego y de Hostilidades Bilateral y Definitivo (Sub punto1 del Punto 3 “Fin del Conflicto” de la Agenda del Acuerdo General). 

</div>

<div class="itemFullText">

##### Las delegaciones del Gobierno Nacional y las FARC-EP hemos decidido: 

##### **I.** Hacer todos los esfuerzos necesarios para llegar sin demoras a la firma del Acuerdo Final, para lo cual cambiamos la metodología por la de un trabajo técnico, continuo y simultáneo sobre los puntos centrales de la Agenda, a la vez que se construyen acuerdos en la Mesa. Para ello definimos un plan con metas preestablecidas. 

##### **II.** En particular, acordar sin demoras los términos del Cese al Fuego y de Hostilidades Bilateral y Definitivo y Dejación de Armas, incluyendo el sistema de monitoreo y verificación. 

##### Para ello, las delegaciones solicitarán el acompañamiento en la sub comisión técnica de un delegado del Secretario General de Naciones Unidas y un delegado de la presidencia de UNASUR (actualmente Uruguay), con el fin de que contribuya a poner en marcha la discusión sobre el sistema de monitoreo y verificación y a preparar desde ahora su implementación, sin perjuicio de la decisión de las delegaciones sobre la participación en el futuro, en el sistema de monitoreo y verificación, de otras organizaciones o países. 

##### **III.** En el entretanto : 

##### Las FARC-EP, como medida de desescalamiento mantendrán la suspensión unilateral de todo tipo de acciones ofensivas. 

##### Por su parte, el Gobierno Nacional a partir del 20 de julio pondrá en marcha un proceso de desescalamiento de las acciones militares, en correspondencia con la suspensión de acciones ofensivas por parte de las FARC-EP. 

##### En todo caso el Gobierno Nacional y sus instituciones, en cumplimiento de sus obligaciones constitucionales, continuarán garantizando la protección de todos los colombianos y el cumplimiento de la ley en todo el territorio Nacional. El Gobierno continuará persiguiendo el delito y en ningún caso tolerará que ninguna organización al margen de la ley ejerza coerción sobre las comunidades mediante el uso de las armas. En cumplimiento de lo anterior el Gobierno Nacional promoverá el respeto y garantizará el libre ejercicio de los derechos fundamentales de todos los colombianos y las colombianas. 

##### En cuatro meses, a partir de la fecha, cada una de las delegaciones hará una primera evaluación tanto del cumplimiento de estas medidas de desescalamiento, como de los resultados de los esfuerzos para agilizar los avances de la Mesa, y tomará las decisiones que considere pertinentes. 

##### Lo anterior sin perjuicio de la posibilidad de dar inicio al Cese al Fuego y de Hostilidades Bilateral y Definitivo, si se llega a un acuerdo. 

##### **IV.** El Gobierno Nacional y las FARC-EP intensificarán, sobre la base de un cronograma, la implementación de medidas de construcción de confianza. 

</div>

</div>

</div>
