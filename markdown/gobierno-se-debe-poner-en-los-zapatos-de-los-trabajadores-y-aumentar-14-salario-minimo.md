Title: Gobierno se debe poner en los zapatos de los trabajadores y aumentar 14% salario mínimo
Date: 2016-12-07 16:35
Category: Economía, Entrevistas
Tags: Aumento, Centrales Obreras, CUT, Ministerio de TRrbajo, salario minimo
Slug: gobierno-se-debe-poner-en-los-zapatos-de-los-trabajadores-y-aumentar-14-salario-minimo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/salario-minimo-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Universal ] 

###### [7 Dic. 2016.] 

**Todos los años los sindicatos de trabajadores, los empresarios y el Ministerio de Trabajo se dan la cita para poder llegar a un acuerdo en lo que respecta al aumento del salario mínimo.** Este año no fue la excepción. Las grandes novedades que trae este diciembre, es que los sindicatos lograron unirse por primera vez y pujar por una sola propuesta que es del 14% para el año 2017.

Sin embargo, la tarea no parece nada sencilla, los sindicatos están solicitando un incremento muy por encima del que han pedido en años anteriores. Para Fabio Arias, secretario general de la Central Unitaria de Trabajadores – CUT- el interés de un 14% se debe a que **“existe un impacto regresivo sobre los salarios de los trabajadores por efecto de la reforma tributaria que deben ser compensados”.**

**Pese a ello, Arias advirtió que incluso haciendo un aumento del 14% no resuelve todas las necesidades que tienen los trabajadores que ganan el salario mínimo**, y agregó que sería la “oportunidad para reconocer el trabajo y de alguna manera recuperar el poder adquisitivo que se ha perdido en los últimos años”.

**De aprobarse el aumento que están solicitando, el incremento para el año 2017 sería de \$92.000 pesos más, de modo que el salario mínimo estaría rondando lo \$785.000 aproximadamente.**

De igual modo, Arias manifestó que estarán atentos a la reforma tributaria puesto que esta continuarían afectando lo bolsillos de los ciudadanos de a pie. Propuesta de reforma que ha sido catalogada como regresiva por los sindicatos.

**“Estaremos atentos para canalizar esas inconformidades y buscar cómo hacemos una causa común los ciudadanos de a pie y los trabajadores, y le hacemos entender al gobierno que no puede seguir legislando y tomando decisiones únicamente a favor de los empresarios (…) los colombianos también somos los trabajadores” agregó Arias.**

Así mismo, los sindicatos han hecho referencia a la precarización y la tercerización a la que se ven enfrentadas un gran número de personas en el país “la informalidad está en un 64%, es decir solo el 36% de los trabajadores tenemos un contrato de trabajo de acuerdo a la ley y el 50% ganan un salario mínimo” contó Arias

<iframe id="audio_14723209" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14723209_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).

<div class="ssba ssba-wrap">

</div>
