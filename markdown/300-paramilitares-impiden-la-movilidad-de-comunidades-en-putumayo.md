Title: 300 paramilitares impiden la movilidad de comunidades en Putumayo
Date: 2017-05-03 15:53
Category: DDHH, Nacional
Tags: comunidades, paramilitares, Putumayo
Slug: 300-paramilitares-impiden-la-movilidad-de-comunidades-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Paramilitares.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [03 May. 2017] 

Según una denuncia entregada por la Comisión Intereclesial de Justicia y Paz **más de 300 paramilitares autodenominados "Autodefensas Gaitanista de Colombia"- AGC, impiden la libre movilidad** en el municipio de Vegas de Piñuña, municipio de Puerto Asis en Putumayo.

Dicha situación se presenta desde el pasado 19 de abril, razón por la cual **las comunidades se han visto obligadas a dejar de trabajar en sus labores diarias**, incluyendo la extracción maderera. Le puede interesar: [Confinadas comunidades indígenas y afros en el Litoral de San Juan Chocó](https://archivo.contagioradio.com/san-juan-choco-paramilitares/)

“Los AGC vestidos de camuflado y armas largas se movilizaron entre la inspección de El Cedral, municipio de Puerto Caicedo y el corregimiento de Piñuña Blanco, municipio de Puerto Asís, **asentándose en la comunidad de Vegas del Piñuña desde hace siete días**” dice la denuncia.

Según sostienen los pobladores, las AGC les manifestaron que “no debían denunciar, les prohibieron actividades como el corte de madera, y les manifestaron la prohibición a la libre movilidad y la libertad de pensamiento”. Le puede interesar: [En Bajo Atrato paramilitares reclutan jóvenes para control social](https://archivo.contagioradio.com/paramilitares-con-armas-cortas-controlan-territorios-del-choco/)

Así mismo, dice la denuncia, que los pobladores afirmaron que **los paramilitares les aseguraron que no iban a abandonar el territorio** y que “asesinarían a toda persona que se presuma haya colaborado con las FARC- EP, así como líderes y lideresas que estén trabajando a favor del acuerdo de paz y su implementación”.

Esta denuncia coincide con los hechos evidenciados por los comuneros Nasa de los cabildos de Ksxa’w Nasa y Yu’ Luucx, quienes hace una semana aseguraron haber visto hombres armados de las AGC entre los municipios de Puerto Asis y Puerto Caicedo. Le puede interesar: [Enfrentamientos entre paramilitares y ELN desplazan 300 familias en Chocó](https://archivo.contagioradio.com/desplazamiento-choco-enfrentamientos/)

Finalmente, la comunicación dice que ya han puesto en conocimiento de las autoridades dichas situaciones hace más de 8 días sin encontrar respuesta del Estado **“la situación de temor es generalizado en los pobladores” concluye el comunicado.**

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="ssba ssba-wrap">

</div>
