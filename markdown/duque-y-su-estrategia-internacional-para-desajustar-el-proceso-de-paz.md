Title: Duque y su estrategia internacional para desajustar el proceso de paz
Date: 2019-09-30 15:02
Author: CtgAdm
Category: Paz, Política
Tags: Cuba, Defendamos la Paz, Diplomacia, Venezuela
Slug: duque-y-su-estrategia-internacional-para-desajustar-el-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Iván-Duque-ante-la-ONU.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Video Presidencia de la República- Colombia  
] 

En un comunicado, el movimiento ciudadano Defendamos la Paz le pidió al gobierno de Iván Duque que cambie la forma en que se están desarrollando las relaciones internacionales, sobre todo con los países que han jugado un papel determinante en las negociaciones de paz. El documento se emitió luego que el presidente Duque expresara ante la Asamblea general de la Organización de las Naciones Unidas (ONU) que Cuba debía decidir si prefería "la relación con Colombia o la relación con criminales", haciendo referencia al Presidente de Venezuela; un hecho que según expertos, obecedería a una estrategia del Gobierno colombiano para 'desajustar' el proceso de paz.

### **En contexto: Colombia y Cuba, una tensión creada por Iván Duque** 

En enero de 2019, luego que el Ejército de Liberación Nacional (ELN) atentara contra la escuela General Santander en Bogotá, el presidente Duque ordenó la suspensión de la mesa de negociaciones con esta guerrilla, y solicitó al gobierno de Cuba ignorar el protocolo establecido en caso de que los diálogos de paz se vieran truncados. Dicho protocolo establecía que la delegación del ELN tenía que ser protegida y llevada a territorio colombiano, con el acompañamiento de los países garantes y el propio Estado de Colombia. (Le puede interesar:["Colombia no puede omitir el DIH ni violar los protocolos de la mesa con ELN"](https://archivo.contagioradio.com/colombia-no-puede-omitir-el-d-i-h-ni-violar-los-protocolos-de-la-mesa-con-eln/))

En su lugar, Duque prefirió pedir a Cuba la extradición de los integrantes del ELN que participaban en la mesa de negociaciones, un acto que juristas como el director de la Comisión Colombiana de Juristas (CCJ), **Gustavo Gallón, calificó como la de "niño de colegio".** En el marco de la 74a Asamblea General de la ONU, Duque arremetió nuevamente contra Cuba, pidiendo que decida si prefiere mantener lazos diplomáticos con Colombia o con Venezuela. (Le puede interesar:["Violar protocolo establecido con ELN sería un irrespeto a la comunidad internacional"](https://archivo.contagioradio.com/protocolo-eln/))

### **"Hay una estrategia diseñada para desbarajustar el proceso de paz en materia internacional"** 

**Juan Fernando Cristo, exministro de interior y ahora integrante de Defendamos la Paz,** afirmó que dicho movimiento tiene la preocupación sobre las "distintas decisiones, actitudes y posiciones que ha tenido el Gobierno en el manejo de las relaciones internacionales, especialmente con las que tienen que ver con el cumplimiento y verificación del Acuerdo de Paz". En ese sentido, señaló que han evidenciado con hechos que el Gobierno ha debilitado todos los mecanismos de seguimiento a los acuerdos.

Cristo aseguró que el Gobierno ha debilitado el papel de los países garantes (Cuba y Noruega), **ha venido desapareciendo el papel del grupo de notables (los expresidentes Felipe González y José Mujica)** y "ha asumido una actitud de resistencia frente a la importante labor de la Oficina en Colombia del Alto Comisionado para los Derechos Humanos de la ONU". Según el exministro, estos hechos forman parte de "una estrategia diseñada para desbarajustar el proceso de paz en materia internacional, y afectar el papel de las organizaciones y países que tienen que ver con su cumplimiento".

### **Relaciones diplomáticas de paz, no de guerra** 

En su comunicado, el movimiento ciudadano agradeció a la comunidad internacional su apoyo al proceso de paz, mientras reclamó al Gobierno respetar a los países garantes, así como a aquellos que apoyan la paz en el país. Por último, concluyó que **"Colombia necesita una política exterior de paz y no de guerra, una de diálogos y no de rompimientos"**. (Le puede interesar:["Actitud del Gobierno de Colombia es un deterioro de la diplomacia"](https://archivo.contagioradio.com/deterioro-de-diplomacia/))

> Una Política Exterior para La Paz y no para la guerra | Declaración [\#DefendamosLaPaz](https://twitter.com/hashtag/DefendamosLaPaz?src=hash&ref_src=twsrc%5Etfw) rechaza la hostilidad del Gobierno del Presidente Duque hacia los miembros de la arquitectura de verificación y acompañamiento internacional del Acuerdo de Paz[\#SíAlApoyoInternacional](https://twitter.com/hashtag/S%C3%ADAlApoyoInternacional?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/hEphQZz3lt](https://t.co/hEphQZz3lt)
>
> — DefendamosLaPazColombia (@DefendamosPaz) [September 30, 2019](https://twitter.com/DefendamosPaz/status/1178617482971615233?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
