Title: Organizaciones sociales denuncian reagrupamiento paramilitar en Tolima
Date: 2017-01-11 14:48
Category: DDHH, Entrevistas
Tags: astracatol, marcha patriotica, Paramilitarismo, Paramiliutares, Tolima
Slug: 34519-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/paramilitares-tolima-AUC.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: pulzo] 

###### [11 Ene 2016]

Con la firma de las Autodefensas Unidas de Colombia, fueron amenazados de muerte los y las integrantes de la Asociación de Trabajadores Campesinos del Tolima, ASTRACATOL, la Asociación de Cabildos Indígenas del Tolima, ACIT, y los miembros de Marcha Patriótica en ese departamento. Paramilitares ofrecieron una “recompensa” de 5 millones de pesos para quien atente contra la vida de líderes e integrantes de esas organizaciones.

### **Reagrupamiento de las AUC en el Tolima** 

Según Juan Bermudez, integrante de ASTRACATOL y de Marcha Patriótica, la amenaza llegó a través de los correos electrónicos el pasado 6 de enero, y no solamente está firmado por las AUC, sino por diferentes grupos  paramilitares que han proferido amenazas contra integrantes del comité ambiental que trabaja contra los daños que produce la minería en el departamento, e incluso el acalde de Ibagué Guillermo Alfonso Jaramillo.

Otra de las situaciones que preocupa a los líderes sociales es que, según las hipótesis que tejen alrededor de estas amenazas, en el Tolima se estaría gestando una alianza entre grupos herederos del paramilitarismo que estarían comenzando a actuar de manera conjunta o colectiva y, por ello, deducen que la firma principal obedece a ese reagrupamiento que se identifica con las AUC.

### **¿Un ataque paramilitar a las luchas por la defensa del ambiente?** 

Cabe resaltar que el trabajo que realizan varias organizaciones sociales en el Tolima son considerados “el bastión” de la lucha contra los desastres ambientales producidos por las empresas mineras, convirtiéndose en ejemplo. También, en el caso de Marcha Patriótica, Bermudez afirma que son el catalizador de las apuestas en el marco del proceso de paz con las FARC. (Lea también: [Comité ambiental y lucha contra la minería](https://archivo.contagioradio.com/consulta-minera-busca-frenar-proyecto-la-colosa-en-cajamarca-tolima/))

### **Así opera el paramilitarismo en el Tolima** 

Según la Defensoría y el mapa realizado por Bermúdez, en el Norte del departamento operan las AGC, versión Tolima y con un rango de acción en Lérida. En el centro, en el cordón Espinal - Guamo – Saldaña, opera un grupo autodenominado los M-Z, y en el Sur han operado las AUC. En el caso de amenazas a ambientalistas, quienes han firmado las amenazas, son las Águilas Negras. ([Le puede interesar: Consulta popular minera en Cajamarca](https://archivo.contagioradio.com/suspendia-consulta-popular-minera-en-cajamarca-por-fallo-de-consejo-de-estado/))

Por otra parte, la respuesta de las autoridades ha sido negar la existencia de los grupos paramiitares y rechazar la propuesta de trabajo para el desmonte de esas estructuras, tomando de manera conjunta y estructural. Sin embargo, la Defensoría del Pueblo ha afirmado en 3 informes dónde y cómo operan esas estructuras. ([Lea también: Nuevas amenazas contra ambientalistas](https://archivo.contagioradio.com/siguen-las-amenazas-de-muerte-en-contra-de-ambientalistas-de-ibague/))

### **Reconocer y combatir el paramilitarismo, una tarea urgente** 

Para Bermudez es urgente que se reconozca la existencia de esos grupos, se apliquen las medidas contenidas en el acuerdo de paz determinadas en el punto sobre fin del conflicto, y una estrategia efectiva que garantice la posibilidad de participación en la vida política de las organizaciones presentes en el departamento.

<iframe id="audio_16133814" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16133814_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
