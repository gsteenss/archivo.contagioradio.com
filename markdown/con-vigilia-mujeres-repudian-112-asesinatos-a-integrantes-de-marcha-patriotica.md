Title: Con vigilia, mujeres repudian 112 asesinatos a integrantes de Marcha Patriótica
Date: 2016-03-18 14:38
Category: DDHH, Mujer, Nacional
Tags: colombia, DDHH, Juan Manuel Santos, marcha patriotica, persecución, piedad cordoba
Slug: con-vigilia-mujeres-repudian-112-asesinatos-a-integrantes-de-marcha-patriotica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Cd2bw39WEAAYDWb.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Marcha Patriótica ] 

###### [18 mar 2016]

Con un acto simbólico y de manera pacífica, 150 mujeres integrantes del movimiento social y político Marcha Patriótica, se toman la iglesia de San Francisco en Bogotá para denunciar la actual situación en materia de derechos humanos del país, la persecución y estigmatización contra las organizaciones sociales y populares, y particularmente el genocidio contra este movimiento, que cuenta con 112 de sus dirigentes asesinados.

"¡Las mujeres de Colombia nos declaramos en vigilia permanente por la vida y la paz!", manifiestan, con velos blancos sobre sus cabezas, velas encendidas y pétalos de rosas en la entrada de la iglesia.

En el año 2014, tras diálogos entre la dirigente de Marcha Patriótica Piedad Córdoba, y el presidente Juan Manuel Santos por la grave situación humanitaria, persecución y amenazas contra el movimiento social, el Presidente se comprometió a garantizar la seguridad de los y las integrantes del movimiento, sin embargo, asegura Córdoba, que "estamos ante una reparamilitarización".

##### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]

\
