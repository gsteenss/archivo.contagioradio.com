Title: Si no nos matan nos judicializan: lideresa de Arauca
Date: 2020-02-10 17:25
Author: CtgAdm
Category: Nacional
Tags: Arauca, Falsos Positivos Judiciales, lideres sociales
Slug: si-no-nos-matan-nos-judicializan-lideresa-de-arauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/FotoAsociaciónMinga-770x400.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Minga

<!-- /wp:paragraph -->

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/sonia-lopez-sobre-persecusion-a-lideres-sociales-en_md_47664194_wp_1.mp3">
</audio>
  

<figcaption>
Entrevista &gt; Rosa Sonia López | Vocera (Movimiento Político Masa Social y popular del centro oriente de Colombia)

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:paragraph {"align":"justify"} -->

El Movimiento Político Masa Social y Popular del Centro Oriente de Colombia denuncia lo que sería la configuración de un nuevo falso positivo judicial contra dirigentes sociales del departamento de Arauca. Luego de casos como la detención de líderes como José Vicente Niño en diciembre 7, **se presentaron tres nuevos casos de allanamientos y dos capturas contra dirigentes comunales y organizaciones campesinas que han denunciado los daños provocados por la industria petrolera.**  
  
Este 10 de febrero la Policía capturó a los dirigentes comunales **Wbeimar Alexander Cetina, presidente de la Federación de Juntas de Acción Comunal de Arauca y Horacio Ramírez, dirigente de la Asociación Nacional Campesina José Antonio Galán Zorro (ASONALCA)** a quienes allanaron sus viviendas, igual que la del presidente de la Asociación Municipal de Juntas de Acción Comunal de Arauquita, **Juan de Jesús Gómez**, quien pese a no ser detenido, fue víctima de esta intervención.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**Sonia López, vocera de la organización** señala que las capturas y allanamientos fueron desarrollados en cabeza de la DIJIN, que trasladó a los campesinos a la capital del departamento a la espera de que avance el proceso legal. Cabe mencionar que las personas capturadas han participado en las jornadas de movilización y en los espacios de interlocución con el Estado y empresas petroleras como Ecopetrol o Cenit que históricamente han hecho presencia en Arauca. [(Le puede interesar: Denuncian nuevo falso positivo judicial contra líder José Vicente Murillo, en Arauca)](https://archivo.contagioradio.com/denuncian-nuevo-falso-positivo-judicial-contra-lider-jose-vicente-murillo-en-arauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

La vocera señala que de ser procesados de la misma forma en que lo han sido otros líderes en el pasado, los dos líderes comunales serían imputados por cargos como concierto para delinquir, obstrucción de vías públicas y rebelión.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Cuatro líderes sociales han sido judicializados en menos de tres meses

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

En la actualidad, otros líderes capturados previamente, como **José Vicente Niño** y **Jorge Enrique Niño Torres** completan 2 meses recluidos en La Picota en Bogotá, mientras **Hermes José Burgos** lleva 15 meses detenido en la cárcel de la Guafílla en Yopal, Casanare, sin que existan mayores avances dentro del proceso penal, que adelanta la Fiscalía 117, unidad destinada a estructuras organizadas dando a los líderes un "trato delincuencial".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Sonia explica que esta persecución judicial "selectiva" se viene presentando en Arauca desde el año 2002, en especial, a lo largo de los dos mandatos del expresidente Álvaro Uribe y su política de seguridad democrática, una situación que ha resurgido con el gobierno de Iván Duque bajo el mismo argumento, "nuestro accionar lo relacionan con la insurgencia, justificando la arremetida contra el movimiento social y así poder desarticular el tejido social".

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En Arauca la violencia se justifica con estigmatización

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

El más reciente informe realizado por [**Human Rights Watch** (HRW)](https://www.hrw.org/es/report/2020/01/22/los-guerrilleros-son-la-policia/control-social-y-graves-abusos-por-parte-de-grupos) revela que diversas organizaciones armadas como el ELN y el Frente Décimo Martín Villa parte de las disidencias de las FARC, imponen sus propias reglas. "Es innegable que en la región hay una presencia activa de organizaciones que ejercen un control y actividad militar y política".

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Mientras el Ministerio de Interior desestimó la información recavada por HRW en Arauca, donde hubo 161 homicidios entre enero y fines de noviembre de 2019, líderes señalan que "es algo que no se puede esconder" planteando además que la violencia ha servido como una excusa para militarizar el territorio y ejercer control, para criminalizar a las comunidades y estigmatizar a los defensores de derechos humanos que continúan siendo judicializados. [(Lea también; Ante la JEP buscan justicia para casos de ejecuciones extrajudiciales en Arauca)](https://archivo.contagioradio.com/ante-la-jep-buscan-justicia-para-casos-de-ejecuciones-extrajudiciales-en-arauca/)

<!-- /wp:paragraph -->
