Title: FECODE recibe nuevas amenazas después del Paro del Magisterio
Date: 2019-09-03 18:00
Author: CtgAdm
Category: Educación, Líderes sociales
Tags: Acuerdos Paz, CUT, maestros, Paro FECODE
Slug: fecode-rueda-prensa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/FECODE-anuncia-paro-nacional.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: FECODE  
] 

La Federación Colombiana de Trabajadores de la Educación (FECODE) ha convocado una rueda de prensa para denunciar la grave situación de violencia contra el magisterio colombiano. En concreto denunciaron que el Comité Ejecutivo de la agremiación recibió amenazas, a través de un panfleto, este 3 de septiembre, firmado por las Águilas Negras.

Nelson Alarcón, presidente de FECODE, pone de manifiesto la violencia que ha sufrido este colectivo: “en estos 25 años, más de mil compañeros, maestros y maestras han sido asesinados. En el 2019 **más de casi 700 compañeros y compañeras han sido amenazados** en territorio colombiano”. (Le puede interesar [Panfleto de Águilas Negras amenaza a maestro de la sabiduría Wayúu](https://archivo.contagioradio.com/panfleto-de-aguilas-negras-amenaza-maestro-de-la-sabiduria-wayuu/))

Ante esta situación, se vieron obligados a **suspender la movilización** prevista para el 6 de septiembre. Sin embargo, dejan claro que no tienen miedo: “nosotros hoy deploramos, rechazamos todas estas circunstancias que estamos viviendo pero seguiremos, con nuestras banderas, defendiendo los acuerdos de paz, defendiendo la escuela como territorio de paz, defendiendo la democracia”.

### **Garantías para el colectivo** 

FECODE exige al gobierno nacional que dé garantías para la seguridad de los dirigentes sindicales y apela a la Procuraduría General de la Nación, a la Fiscalía y a los organismos internacionales para que se proteja a los maestros y maestras y a la dirigencia sindical. “El Estado no se pronuncia absolutamente en nada de los hechos que han acontecido y que acontecen en nuestro país”. En esa misma línea, Diógenes Orjuela, presidente de CUT Nacional, exige al gobierno nacional cumplir con los acuerdos firmados con FECODE fruto de las negociaciones de los últimos años.

(Le puede interesar [Ante incumplimientos del gobierno maestros irían a paro nuevamente](https://archivo.contagioradio.com/gobierno_incumple_maestros_paro/))

En los próximos días, la organización dará a conocer las movilizaciones previstas por todo el territorio colombiano. Alarcón afirma que lo harán “**para fortalecer la escuela como territorio de paz, porque nosotros, maestros de Colombia y FECODE somos defensores de la paz**”.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
