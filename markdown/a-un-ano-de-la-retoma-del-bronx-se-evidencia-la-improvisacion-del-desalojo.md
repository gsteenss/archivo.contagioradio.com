Title: Desalojo del Bronx sólo favoreció a empresas privadas
Date: 2017-05-24 12:36
Category: DDHH, Nacional
Tags: Alcaldía de Bogotá, Bronx, Habitantes de calle, habitantes de calle en el Bronx
Slug: a-un-ano-de-la-retoma-del-bronx-se-evidencia-la-improvisacion-del-desalojo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/100x100_bronx_bogota-e1495646996178.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Idealista.com] 

###### [24 may 2017] 

En la zona del Bronx se construirán locales comerciales cuyo metro cuadrado costará más que los ubicados en Unicentro. El costó del metro cuadrado, según el representante Alirio Uribe,  **es de \$60 millones de pesos.**

Para el Representante a la Cámara, el desalojo del Bronx, **"sólo logró hacer una recuperación de espacio para la elaboración de proyectos urbanísticos y empresas privadas que buscan apropiarse del lugar".** En una audiencia, llevada a cabo el lunes 22 de mayo, en la localidad de Los Mártires, Uribe pudo escuchar a comerciantes[ y vecinos de la zona desalojada del Bronx, quienes “coinciden que el desalojo causó un impacto negativo en las condiciones de vida de la ciudadanía”. Le puede interesar: ["\$60 millones por metro cuadrado: el negocio tras la crisis humanitaria del Bronx"](https://archivo.contagioradio.com/60-millones-por-metro-cuadrado-el-negocio-tras-la-crisis-humanitaria-del-bronx/)]

Las advertencias sobre la improvisación en el desalojo del Bronx, llevan un año reflejándose en los diferentes sectores de la sociedad en Bogotá. El maltrato, el desplazamiento, los asesinatos de habitantes de calle y el aumento del micro tráfico son la realidad en una ciudad que ha sembrado **odio y desprecio producto del discurso de la Alcaldía de Bogotá.**

Igualmente, la predicción de la creación de nuevos Bronx y la crisis humanitaria se ha materializado con 2 mil personas consumiendo droga en las calles y las redes de micro tráfico amenazando la seguridad en Bogotá. **A esto se suma que 22 mil habitantes de calle no caben en los albergues** que dispuso la Alcaldía para prevenir que estas personas deambulen por la ciudad y no se crearon políticas integrales para garantizar la recuperación de los habitantes de calle. Le puede interesar: ["Desalojo del Bronx no tuvo una planificación integral"](https://archivo.contagioradio.com/desalojo-del-bronx-no-tuvo-una-planeacion-integral/)

**SITUACIÓN DE LOS HABITANTES DE CALLE**

Ante la falta de un plan de mitigación, para el acompañamiento a los habitantes de calle que fueron desalojados del Bronx, y según Alirio Uribe, “ellos van dos días a los albergues por comida caliente pero luego vuelven a las calles a seguir consumiendo drogas. A esta población **se les vulneraron sus derechos,** están siendo olvidados y odiados por la sociedad. ”

El plan de mitigación que había propuesto el Representante se basaba en construir zonas humanitarias para que los habitantes de calle se pudieran concentrar. Allí mismo se les suministraría la droga que consumen para ir **paulatinamente llevándolos al consumo de drogas blandas** y asegurar un proceso de eliminación del consumo total. Le puede interesar: ["Alcaldía, Bronx y caos"](https://archivo.contagioradio.com/alcaldada-bronx-y-caos/)

Según Uribe, **la Alcaldía sabía que se debía realizar un plan de disminución de consumo con los habitantes de la calle** y que “no estaban preparados para desalojar”. Además, las normas para realizar el censo de la población que vive en la calle, la atención médica y las políticas de resguardo que se hicieron hace 20 años no se han cumplido.

Uribe afirmó que “a los habitantes de calle los familiares no los recibe y la gente y los comerciantes no quieren tenerlos al lado. **Con el desalojo se le lanzó una piedra a un avispero y se quitó la colmena.** Ahora esta población está deambulando por toda la ciudad y no hay una política integral”.

<iframe id="audio_18883638" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18883638_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
