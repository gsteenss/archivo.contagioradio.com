Title: "Es el tiempo de la construcción de la paz": Ricardo Tellez, Delegación de paz FARC
Date: 2016-08-03 16:33
Category: Entrevistas
Tags: FARC, Gobierno, La Habana, paz, Ricardo Tellez
Slug: es-el-tiempo-de-la-construccion-de-la-paz-ricardo-tellez-delegacion-de-paz-farc
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Ricardo-Tellez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: You Tube 

###### [3 Ago 2016] 

Ricardo Tellez, integrante de la delegación de paz las FARC habló para los micrófonos de Contagio Radio, sobre los principales cuestionamientos que existen actualmente frente a la recta final del proceso de paz entre esa guerrilla y el gobierno colombiano.

Tiempos estipulados para lograr la firma final del acuerdo, situación de los 29 indultados de las FARC, el impacto en la organización tras el trabajo de la subcomisión de género, el plebiscito por la paz y las conversaciones con el ELN fueron algunos de los temas que se tocaron durante la entrevista.

“Si queremos que el país cambie para una democracia más avanzada, obligatoriamente debe haber una Asamblea Nacional Constituyente que estudie lo que quede por fuera de los acuerdos y otras cuestiones para modernizar el país, pues es el tiempo de la construcción de la paz”, dice el integrante de las FARC.

Escuche aquí el diálogo completo:

<iframe src="http://co.ivoox.com/es/player_ej_12431062_2_1.html?data=kpehlZaUepOhhpywj5eYaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncbPdxMbfxtSPmMbgzcrnh5enb6XZzcrUw8jNaaSnhqeg0JDIqYzEwt-YqKa2h4y5sZKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
