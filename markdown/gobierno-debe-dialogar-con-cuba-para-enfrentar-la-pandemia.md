Title: Gobierno debe dialogar con Cuba para enfrentar la pandemia
Date: 2020-04-01 08:55
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: Covid-19, Cuba, Organizaciones Indígenas de colombia
Slug: gobierno-debe-dialogar-con-cuba-para-enfrentar-la-pandemia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/IMG_7116.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right"} -->

Foto: Gobierno- Comunidades/ ONIC

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

**La Comisión Étnica para la Paz y Defensa de los Derechos Territoriales**, en el contexto de la actual pandemia del Covid-19 y el impacto que esta tendrá sobre comunidades, territorios y sus derechos, ha exigido al Gobierno que adopte de forma urgente medidas que eviten una afectación de poblaciones que en su mayoría pertenecen a zona rurales donde el Estado no llega. Dichas medidas incluyen establecer un diálogo con Cuba y solicitar una misión médica enfocada en los territorios étnicos.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

[Armando Valbuena](https://twitter.com/ArmandoWayuu), , secretario de la Comisión, **destaca la importancia de reconocer que en Colombia habitan 115 pueblos milenarios con una población total de 1.900.000 personas y 2.980.000 personas del pueblo afro** en regiones donde no se ha garantizado la prevención, el acceso a la salud ni tratamientos médicos y donde prevalece la desinformación, la pobreza y altas tasas de mortandad.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> "La Colombia étnica es en esencia rural donde la estructura de la república aún no llega", por lo que hay una necesidad de atender esas zonas. - Armando Valbuena

<!-- /wp:quote -->

<!-- wp:paragraph {"align":"justify"} -->

El secretario se refirió a lugares c**omo Quibdó que tiene población afro e indígena al igual que en Buenaventura, Maicao donde el servicio médico no ofrece las garantías suficientes para sus habitantes.** [(Lea también: Comunidades indígenas generan espacios de protección contra el COVID - 19)](https://archivo.contagioradio.com/comunidades-indigenas-generan-espacios-de-proteccion-contra-el-covid-19/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Qué medidas solicitan las comunidades étnicas al Gobierno?

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Dentro de las medidas que exigen al Gobierno han solicitado se logre una coordinación con las autoridades étnico-territoriales para establecer medidas y una respuesta a la crisis pandémica, de igual forma han solicitado una capacitación para cada étnia, incluidos sabedores y autoridades médicas calificadas para controlar la enfermedad. [(Lea también: Fuego y desabastecimiento consumen al Magdalena. Gobierno nacional no aparece)](https://archivo.contagioradio.com/fuego-y-desabastecimiento-consumen-al-magdalena-gobierno-nacional-no-aparece/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

A su vez solicitan ambulancias aéreas, terrestres y acuáticas con su respectivo equipo y personal médico especializada en atención hospitalaria y toma de pruebas diagnósticas. También se ha pedido dotación extra y complementaria en casas, centros de salud e instalación de hospitales de campaña en los territorios [(Lea también: Comunidades piden cese al fuego y atención del Gobierno a problemas en los territorios)](https://archivo.contagioradio.com/comunidades-piden-cese-al-fuego-y-atencion-del-gobierno-a-problemas-en-los-territorios/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Armando Valbuena resalta la necesidad de proveer redes de comunicación en las zonas rurales donde existen teléfonos, "¿cómo hacemos para ocmunicar a los pueblos y naciones de la Amazonía y la Orinoquía como en Leticia donde 22 idiomas distintos o en Buenaventura donde hay cinco lenguas milenarias", cuestiona.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El pueblo de Cuba está ayudando a garantizar la existencia de los pueblos

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Los firmantes, conscientes que el país no está preparado para enfrentar la crisis y mucho menos las comunidades le han solicitado al presidente Duque que acuda al Gobierno de Cuba y **así establecer una cooperación humanitaria que incluya una misión médica para atender específicamente a la población étnica**, ventiladores manuales, pruebas rápidas de diagnóstico y otros elementos de respuesta.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Valbuena destaca que por más de 60 años, Cuba ha sido una nación que ha trabajado desde el ámbito médico por la humanidad siendo una garantía para otros pueblos mientras que en Colombia **"se desprecia la iniciativa de orden humano que ofrece un país que ha demostrado al mundo que tiene otra economía y otros principios y estéticos para la vida".**

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

En medio de esta crisis, el personal médico y enfermero cubano ha viajado alrededor del globo para asistir a los servicios sanitarios en países como Italia, Andorra y China. Por su parte son 50 las naciones que han solicitado al país insular el antiviral que en China resultó exitoso para luchar contra el Covid-19.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
