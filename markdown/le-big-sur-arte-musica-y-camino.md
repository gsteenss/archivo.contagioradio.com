Title: "Le Big Sur" Arte, música y camino
Date: 2015-08-10 13:10
Category: Sonidos Urbanos
Tags: Le big sur, Ludófono- Proyecto Lúdico de Artes Integradas
Slug: le-big-sur-arte-musica-y-camino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Le-big-sur.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<iframe src="http://www.ivoox.com/player_ek_6451024_2_1.html?data=l5mik5WWeI6ZmKiakpaJd6Kkkoqgo5mccYarpJKfj4qbh46kjoqkpZKUcYarpJK5x5CmrciftNrfh5enb8TjzcrQ1s7as4zYxpDRy9jJaaSnhqee0YqWh4zV09nSjd6PsYa3lIqvo9jNp8KfxNTbjdjJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Le Big Sur en "A la Calle"] 

###### [[![Le Big sur](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/IMG-20150810-WA0000.jpg) 

######  

######  

##### Juan y David/HS de "Le Big sur" con Angel de "A la Calle" 

######  

"Le Big Sur" es una propuesta multicultural creada en el año 2012 en Bogotá, Colombia, que explora la Música, Fotografía, Arquitectura, Diseño, Arte y Camino[. En lo musical, la banda navega entre sonidos que van desde Champeta al Son Cubano, de la Música Andina hasta el Gipsy Punk. En la propuesta ha primado la incorporación y búsqueda de alternativas multiculturales expresadas desde un concepto base: la mirada musical del viajero con cada uno de sus acercamientos a diferentes culturas, una fotografía de cada lugar hecha música. Con su proyecto lúdico de artes integradas "Ludófono" el colectivo apuesta por el desarrollo social que abarca diferentes talleres de expresión artística presentadas a los niños con metodologías que evidencian la trascendencia del arte en el mundo.]{#descripcion_audio}

Facebook: facebook.com/LeBigSur            Twitter: @LEBIGSUR
