Title: Amenaza ambiental por perforación de petrolera Amerisur en Putumayo
Date: 2016-03-21 08:27
Category: Ambiente, Nacional
Tags: Amerisur, Cauca, Impacto ambiental, Ingenierías de Vías S.A, Putumayo
Slug: empresas-petroleras-y-exctractivistas-en-colombia-ponen-en-riesgo-el-ambiente
Status: published

###### [Foto: Comisión JyP] 

###### [18 mar 2016] 

Según denuncian las comunidades habitantes de la Zona de Reserva Campesina 'Perla Amazónica', la petrolera Amerisur realizó una excavación bajo el lecho del río Putumayo. Lo preocupante, aseguran los pobladores, es que **no encuentran el punto de salida del túnel para poner en funcionamiento el Oleoducto Binacional Amerisur (OBA).**

La obra debe traspasar por debajo del lecho del río Putumayo una línea de transferencia de cruce subfluvial de 1410 metros, hasta territorio ecuatoriano. Según la información la **empresa habría realizado el cruce subfluvial hasta territorio Ecuatoriano por un lugar distinto al proyectado**. Hasta el momento los funcionarios de la empresa petrolera no han podido establecer el punto de salida del ducto.

Así mismo, advierten que **[la comunidad no ha sido informada sobre las implicaciones socio-ambientales que pueda causar este proyecto de alto impacto.](https://archivo.contagioradio.com/zona-de-reserva-campesina-se-moviliza-contra-amerisur/)** "Este incidente, de ser confirmado, pone en alerta por los daños al frágil ecosistema amazónico, y requiere actuación, monitoreo y seguimiento inmediato por parte de las instituciones ambientales y entes de control", puntualizan.

**Extracción de material de arrastre, Cauca**

Una vez más, la empresa Ingeniería de Vía S.A extrae material de arrastre de los ríos Dos Ríos y Patía en el municipio Mercaderes, de la vereda el Pilón,  afectando directamente a los habitantes de la región, y presentando riesgos ambientales, confirma la Comisión de JyP.

"Las comunidades de los  Consejos Comunitarios Raíces afro descendientes de Galíndez Patía y Corredor Panamericano del Pilón Mercaderes, sostienen que en lo corrido de este año, la explotación de material de arrastre sobre el punto conocido como Dos Ríos, continúa de manera desproporcionada, sin  ningún tipo de control por la autoridad ambiental regional CRC", aseguran.

No se ha efectuado el compromiso asumido  por el delegado del ministerio de minas en la audiencia, de hacer una visita a la planta de Galíndez para constatar los impactos presentados hasta la fecha, afirma la Comisión.
