Title: Las mujeres cultivadoras de coca del sur de Colombia se movilizan por la equidad
Date: 2017-05-26 12:57
Category: Nacional, yoreporto
Tags: cocaleras, hoja de coca, mujeres cultivadoras
Slug: las-mujeres-cultivadoras-de-coca-del-sur-de-colombia-se-movilizan-por-la-equidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/campesinas-cocaleras.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [26 de May. 2017] 

Ana Monroy y Lilia Forero son caucanas, de veredas vecinas conectadas por trochas, ríos y vías en mal estado. Socorro Estrada es de la vereda El Placer, Putumayo, la misma donde la población vivió bajo el terror del orden paramilitar entre 1999 y 2006. Las tres mujeres tienen varias cosas en común, han **trabajado la hoja de coca, creen firmemente en los Acuerdos de Paz, son lideresas en sus comunidades** y estuvieron en el Encuentro de Mujeres Cocaleras del Sur de Colombia.

En este primer semestre del año **se han realizado dos encuentros de mujeres cocaleras en Putumayo**. El primero, el Encuentro de Mujeres Cocaleras del Sur de Colombia, realizado el 17 y 18 de marzo en Puerto Asís, el cual reunió a más de ochenta mujeres, quienes discutieron su situación actual en relación a lo pactado en los puntos 1 y 4 del Acuerdo Final, particularmente lo relacionado al desarrollo rural integral, sustitución de cultivos y tratamiento penal diferenciado.

Del Encuentro surgió una declaración que recoge la postura de las mujeres frente a estos temas, así como sus exigencias al Gobierno Nacional. El evento fue organizado por **La Corporación Humanas y la Alianza de Mujeres "Tejedoras de Vida" del Putumayo,** en asocio con más de diez organizaciones de mujeres de todo el país.

El segundo encuentro fue realizado el 28 de abril, también en Puerto Asís, y congregó a cuarenta mujeres de **organizaciones como Fensuagro y la COCCAM, acompañadas por Dejusticia.** Discutieron, entre otros asuntos, sus inquietudes respecto a los Planes de Atención Inmediata – PAI, que son la ayuda económica ofrecida por el Gobierno Nacional, y también el tratamiento penal diferenciado y los compromisos a los que deben someterse con base en este.

Las mujeres cocaleras del sur del país se han empezado a movilizar por su derecho a la equidad. Sumado a este panorama de encuentros, Ana, Lilia y Socorro, viajaron un largo camino desde sus territorios para reunirse el pasado 17 y 18 de mayo en Bogotá con varias entidades responsables de su futuro como cultivadoras, como campesinas y como mujeres.

Hablaron con representantes del Ministerio de Justicia, el Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito – PNIS, ONU Mujeres, Oficina de la ONU para la Droga y el Delito – UNODC, Alto Comisionado de Naciones Unidas para los Derechos Humanos - OACNUDH, representantes de la Comisión de Seguimiento, Impulso y Verificación a la Implementación del Acuerdo Final –CSIVI, tanto de las FARC como Gobierno, la representante a la Cámara Ángela María Robledo y la Consejería Presidencial para la Equidad de la Mujer. Las mujeres estuvieron acompañadas por las organizaciones: Corporación Humanas, Alianza de Mujeres Tejedoras de Vida del Putumayo y Dejusticia, en representación del Grupo G-Paz.

### **¿Qué buscaban las mujeres y que se acordó en las reuniones?** 

En las reuniones, las mujeres afirman que **hay mucha desinformación respecto a los planes de sustitución en sus comunidades** “el Ministerio de Defensa dice una cosa, la oficina para sustitución de cultivos nos dice otra. Simplemente no hay claridad en el proceso, por lo tanto, no podemos informar a nuestra gente”.

Agregan que desde hace varios meses, **el ejército aterriza sus helicópteros, acampa por dos o tres días en lugares cercanos a los campos de coca** y procede, ayudado a veces por la policía, a arrancar las plantas; sin previo aviso y armados.

Las organizaciones acompañantes a las reuniones resaltaron que los preacuerdos de sustitución firmados por algunos municipios no establecen con claridad las obligaciones de las partes. **Si las comunidades no cumplen, se las castigará con la cárcel, pero si el gobierno y los órganos responsables incumplen no se determinan sanciones.**

Es decir, la existencia de un preacuerdo firmado con las comunidades para sustituir la hoja de coca no garantiza que no haya erradicación. Claramente, esto mina la confianza de los campesinos y las campesinas cultivadores.

Las principales demandas de las mujeres fueron:

-   Participar de forma activa en la implementación del acuerdo de paz con otras organizaciones y en representación equilibrada con los hombres como lo establece el enfoque de género incluido en el Acuerdo de Paz.

<!-- -->

-   Vincular a todas las mujeres que están dentro de la economía de la hoja de coca (cultivadoras, recolectoras, transformadoras, transportadoras y vendedoras) con proyectos productivos agropecuarios de sustitución. Dichos procesos de sustitución deben formar parte de las políticas públicas de los departamentos y municipios, deben comprometerse a la construcción y mejora de vías de acceso y deben estar articulados con lo establecido en el punto 1 del Acuerdo. La sustitución de cultivos no debe poner en riesgo su derecho a la tierra.

<!-- -->

-   Garantías para que se mantenga el tratamiento penal diferenciado tanto para cultivadoras como para mujeres condenadas por delitos menores de, tal como está estipulado en los Acuerdos. Rechazan las iniciativas del Fiscal General de la Nación de continuar con la persecución penal a los eslabones más débiles de la cadena del narcotráfico.

Con respecto a estas demandas, las distintas entidades reaccionaron de forma favorable y manifestaron su voluntad para **pactar agendas conjuntas en torno a la construcción de un plan piloto para la sustitución de cultivos ilícitos con enfoque de género** en el Putumayo. Este piloto serviría como guía para la implementación en los demás departamentos del país.

Ana, Lilia y Socorro se fueron con la satisfacción de haber sido reconocidas como campesinas y no como narcotraficantes. Regresaron **esperanzadas, a socializar lo pactado con sus comunidades** y sobre todo listas para trabajar por una paz con las mujeres.

Firman: Corporación Humanas y la Alianza de Mujeres Tejedoras de Vida del Putumayo.

Con el apoyo de: LolaMora Producciones
