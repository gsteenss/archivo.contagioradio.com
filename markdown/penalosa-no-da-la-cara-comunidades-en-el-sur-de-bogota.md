Title: "Peñalosa no da la cara": Comunidades en el sur de Bogotá
Date: 2017-08-18 13:53
Category: DDHH, Nacional
Tags: alcaldía bogotá, Enrique Peñalosa, Mochuelo, relleno doña juana
Slug: penalosa-no-da-la-cara-comunidades-en-el-sur-de-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/peñalosa.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

###### [18 Ago 2017] 

Habitantes del área de influencia del relleno Doña Juana reclaman la presencia del alcalde de Bogotá Enrique Peñalosa quien **aún no se ha reunido con ellos para solucionar la crisis socio ambiental** que viven desde hace 30 años, por los malos manejos de los desechos que llegan al relleno. Denuncian además que las fumigaciones que se están realizando usan químicos tóxicos para la vida humana.

Yurani Muñoz, habitante del barrio el Mochuelo, indicó que desde que terminó el plantón a la entrada del relleno sanitario Doña Juana el martes pasado, **no han recibido ninguna respuesta por parte de la alcaldía** de Bogotá.

Manifestó además que Enrique Peñalosa estuvo presente en el lugar un día después de que ocurrieran las manifestaciones y **“no se tomó la molestia de charlar con nosotros”.** Según Muñoz, los habitantes estuvieron esperando por hora y media a que el alcalde se reuniera con ellos “y **solo estuvieron presentes los policías del ESMAD**”. (Le puede interesar: ["ESMAD también arremetió contra manifestantes en el relleno Doña Juana"](https://archivo.contagioradio.com/esmad-continua-arremetida-contra-manifestantes-en-el-relleno-dona-juana/))

**Químicos de fumigaciones están afectando la salud de los habitantes**

Yurani Muñoz indicó que el químico que se está usando para realizar las fumigaciones hacen parte del grupo de los piretroides y las piretrinas. El resumen de salud pública de la Agencia para las Sustancias Tóxicas y el Registro de Enfermedades, establece que estos insecticidas **“interfieren con el funcionamiento normal de los nervios y del cerebro”**.

Adicionalmente, si esta sustancia entra en contacto con la piel, es posible que **“se experimenten sensaciones de adormecimiento, comezón, ardor**, hormigueo o calor que puede durar horas”. Ante el uso de este químico, las comunidades denunciaron que no se están tomando las precauciones necesarias. (Le puede interesar: ["Comunidades reclaman cierre definitivo del relleno "Doña Juana"](https://archivo.contagioradio.com/comunidades-reclaman-cierre-definitivo-del-relleno-dona-juana/))

Por otro lado, Muñoz aseguró que las fumigaciones **no resuelven el problema de salubridad que han tenido por más de 30 años**, “hasta que no se resuelva el problema de fondo la fumigación va a seguir siendo una solución a corto plazo pero tenemos que seguir aguantando los malos olores y las toneladas de basura que llegan a diario”.

Finalmente, ellos y ellas siguen insistiendo en que es necesario que se replanteé el modelo de recolección de basuras en la capital. Han propuesto, en repetidas ocasiones que se debe apostar por** un aprovechamiento de las basuras para generar energía** y así desaparecería la acumulación que llama a los moscos y los roedores.

Muñoz recordó que el 27 de septiembre 8 localidades del sur de Bogotá **realizarán un paro cívico** para protestar por el problema que viven a diario. Invitó a la ciudadanía a hacer parte de las movilizaciones en la autopista sur, la entrada al llano y la entrada al relleno sanitario.

<iframe id="audio_20407513" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20407513_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU).
