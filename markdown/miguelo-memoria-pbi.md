Title: Partió el Miguelo, “Todos vamos a morir”
Date: 2017-05-19 08:25
Category: Abilio, Opinion
Tags: colombia, Defensores, Derechos Humanos
Slug: miguelo-memoria-pbi
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/img010-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo personal 

#### Por **[Abilio Peña ](https://archivo.contagioradio.com/abilio-pena-buendia/) - [~~@~~Abiliopena](https://twitter.com/Abiliopena)

###### 18 May 2017 

Hace siete días Miguel Ángel Jimenez, El Miguelo, nos dejó. Tres días antes tuvimos la suerte de verlo en el hospital de cuidados paliativos. Estaba con su esposa que esperaba el desenlace. No había nada que hacer, su cabeza, siempre bien puesta, soportaba ahora  la agresividad de un cáncer que le hizo metástasis.

Ella nos contaba el duro tránsito de los días en que con su hija lo cuidaban, hasta que agradeció el haber tenido el valor de trasladarlo hasta el lugar en que terminó sus días. Con evidente paz comentó que no le dolía nada. Nos dijo, también, que una médica le preguntaba a él: -tiene miedo, a qué le tiene miedo? A lo que respondió "a morirme" y ella con la sapiencia humana y profesional para preparar lo inexorable, concluía cariñosa "todos nos vamos a morir"

Lo conocimos en el Cacarica, Chocó, acompañando las comunidades desde el voluntariado que hacía con Brigadas Internacionales de Paz. Venía de animar los Comités Romero de Madrid, a los que siempre perteneció. Se involucró con tal pasión con la comunidad que, además de lidiar con el acoso militar y paramilitar  de sus días de permanencia en la cuenca, le pidieron  ser padrino de bautismo de un bebé. A pesar de excederse en el mandato como voluntario y de la advertencia sobre las futuras distancias, apegos, dependencias que se podrían generar, su buen corazón pudo más y "cargó" el niño que hoy debe estar recordando a su padrino ausente.

Así era su pasión solidaria por la vida de las víctimas de carne y hueso. De ellos, de sus victimarios, de las estructuras que reproducen local  y globalmente a los unos y a los otros, escribió en una prosa casi perfecta  los párrafos con los  que introducía su selección de textos que compartía al grupo de distribución de correo electrónico. Al final de su nombre, como para despejar dudas sobre el lugar social desde el que hablaba, estaba la frase del Pedro Páramo del mexicano Juan Rulfo "el fuego pá que caliente, tié que venir desde abajo".

En el mensaje de  fin de año  del dos mil dieciséis, que seguramente es uno de sus últimos escritos, habló de sus deseos para quiénes seguimos vivos:

"En éste último día del año quiero desearos un mundo más fraterno. Necesitamos terminar con las violencias de todo tipo. Sobre todo las que matan, marginan, empobrecen y arrasan a los más débiles, al mismo tiempo que benefician a los poderosos. No ha de ser así por siempre. La larga noche de las opresiones que generan muerte y llanto entre los más humildes tendrá que ser superada y la luz ha de brillar. Parecen deseos vanos. Pero de deseos vanos se hace la historia de liberación de los pueblos.Unamos nuestras manos y nuestros corazones para proclamar que ha de llegar algún día la justicia de los humildes. Que es la única Justicia. Mi abrazote gordo y mi cariño, El Miguelo".

Todos nos vamos a morir, como decía la médica, pero el miedo y el dolor por la partida de nuestros seres queridos están ahí, en la piel. Es muy difícil asimilarlo. En nuestro encuentro con ésta experiencia límite nos paramos justo en las puertas del misterio. Al final, es quizás el cómo hayamos vivido la vida lo que cuenta, en fidelidad a lo que creemos. En el caso de Miguel, a los deseos que parecen vanos como un mundo más fraterno, sin las violencias que matan, marginan, empobrecen y benefician a los poderosos. En coherencia con esas palabras, entregó su vida a pedazos, soñando con que un día brille la luz de la justicia de los humildes.

Llegar a comprender para qué vivimos ha llevado a algunos hombres y mujeres a relativizar su vida biológica. Eduardo Umaña por ejemplo, decía que era mejor “morir por algo que vivir por nada” o Josué Giraldo escribía que “ceder es mas terrible que la muerte”. Y Jesús, el de Nazaret, decía que el que “salva su vida la perderá y el que la pierde por mí la salvará”, el mí del otro y la otra, del necesitado, pues es la solidaridad con el excluido lo que cuenta: dar de comer al hambriento, vestir al desnudo, dar de beber al sediento, visitar al preso, dar casa al forastero. El Miguelo a eso dedicó su existencia, hasta el último respiro.

#### **[Leer más columnas de Abilio Peña](https://archivo.contagioradio.com/abilio-pena-buendia/)** 

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
