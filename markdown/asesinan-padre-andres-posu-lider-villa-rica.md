Title: Asesinan al padre de Andrés Posu, líder amenazado en Villa Rica
Date: 2019-07-01 11:48
Author: CtgAdm
Category: Líderes sociales, Nacional
Tags: Andrés Posu, Atentado, Cauca, lideres sociales
Slug: asesinan-padre-andres-posu-lider-villa-rica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/andres-posu-e1561999295122.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo familar 

La noche de este domingo se conoció sobre el asesinato del señor **Francisco Ríos, padre del líder y exconcejal Andrés Felipe Posu**, quien desarrolla su actividad en el municipio de Villa Rica, Cauca, y sobre quien ya existían amenazas por su incidencia en distintos procesos.

El hombre **era víctima de desplazamiento en Villavicencio, Meta, ciudad en la que asesinaron a su esposa.** Andrés Posu fue objeto de un atentado en su contra cuando se movilizaba entre Guachené y Villa Rica en el Norte del Cauca en abril de 2017 por parte de hombres armados.

En desarrollo...

###### Reciba toda la información de Contagio Radio en [[su correo]
