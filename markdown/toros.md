Title: Cancelación de temporada taurina en Medellín: un triunfo de la ciudadanía
Date: 2019-01-09 16:35
Author: AdminContagio
Category: Ambiente, Animales
Tags: Bogotá, corridas de toros, Medellin, tauromaquia
Slug: toros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/Toros-Regresan-e1477072188751.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/toros.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/01/toros-en-bogota.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/toros-en-colombia.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/corridas-de-toros-e1486324945371-770x400.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Animanaturis 

###### 09 Ene 2019 

El alcalde de Medellín Federico Gutiérrez, expresó que  sostuvieron diálogos con los socios del Centro de Espectáculos La Macarena para no llevar a cabo la feria taurina, la cual, además incentivaba a menores de edad para que asistieran de forma gratuita a esta clase de eventos.

Ante el anuncio, **Natalia Parra, directora de la plataforma colombiana por los animales ALTO** afirma que se trata de “un triunfo de la ciudadanía”, la cual venía haciendo un llamado al alcalde de Medellín y a D’Groupe, dueño mayoritario del Centro de Espectáculos La Macarena para evitar la realización de esta clase de espectáculos.

Parra explicó que para finales de 2018 se había establecido que no se realizarían corridas de toros en Medellín pues ni la **Corporacion Taurina** ni el anterior dueño mayoritario, la Fundación Hospital San Vicente contaban con los recursos para pagar el alquiler del lugar, La Macarena pasó a manos de la empresa de entretenimiento D’ Groupe la cual, según Parra, no ofrecía las mismas facilidades a la Corporación como en el pasado.

El panorama cambió con la aparición del inversor Pablo Moreno, quien ofreció a D’Groupe una valiosa oferta para alquilar el **Centro de Espectáculos La Macarena**. Al anunciar las fechas para el evento, también se invitó a los menores de edad a entrar de forma gratuita, una convocatoria que agudizó la polémica.

Según la directora de la Plataforma Alto, en “concordancia con la política de bienestar animal de Medellín”, y con el fin de proteger a los menores de edad, la Alcaldía de Medellín como socia de la Plaza de Toros La Macarena negoció con D'Groupe, y acordaron no prestar la plaza para dicho evento.

> No habrá temporada taurina en Medellín.  
> Hemos trabajado por el bienestar animal y lo seguiremos haciendo. [pic.twitter.com/90tHBfS7c0](https://t.co/90tHBfS7c0)
>
> — Federico Gutiérrez (@FicoGutierrez) [9 de enero de 2019](https://twitter.com/FicoGutierrez/status/1083035245199413249?ref_src=twsrc%5Etfw)

<p style="text-align: justify;">
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
  Aunque en otros escenarios taurinos como **Duitama, Boyacá,** hubo muy poca afluencia de público y en otras ciudades como Cartagena ya no se celebre dicha práctica, Parra no descarta que inversores como Moreno también busquen invertir en Bogotá donde la temporada contará con tres corridas este 2019.

</p>
> Reportando que la temporada taurina 2019 en Duitama, Boyacá, fue un total fracaso. Los empresarios de la muerte de seres vivos, tuvieron cuantiosas pérdidas. [@PlataformaALTO](https://twitter.com/PlataformaALTO?ref_src=twsrc%5Etfw) [pic.twitter.com/Czk8NQPH1W](https://t.co/Czk8NQPH1W) — @MiguelRincónC ♻️ (@miguelrinconc) [9 de enero de 2019](https://twitter.com/miguelrinconc/status/1082994509208829952?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **¿Y mientras tanto qué sucede con las corridas de toros en Bogotá?** 

Frente a la próxima temporada taurina en la capital del país, Parra expresó su inconformidad sobre el método de contratación del **IDRD** el cual en lugar de establecer un canon fijo por el alquiler de la **Plaza la Santa María**, cobraría el 14.9% de la boletería, lo que facilita la realización del evento a la Corporación Taurina.

Asimismo, la directora de ALTO reprochó el cerco que la Alcaldía propuso para proteger las zonas contiguas a la plaza en el centro de Bogotá, una medida que para Parra limita y debilita la protesta social, “consideramos indigno ir a manifestarse con las condiciones que plantea la **Alcaldía**, salvaguardando a los taurinos para que disfrutan su espectáculo e invitando a los antitaurinos para que protesten lejos de la plaza, no nos queremos prestar para eso”, afirmó.

Parra indicó que las condiciones impuestas por la Alcaldía no cumplen el sentido de una manifestación social, por la cual próximamente comunicarán qué otro tipo de actividades se plantearán para involucrar a la ciudadanía y hacer frente a la temporada taurina en Bogotá.

<iframe id="audio_31374564" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31374564_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
