Title: Fiscalía de la CPI respalda a la JEP y manifiesta preocupación por asesinato de líderes
Date: 2020-05-29 13:52
Author: CtgAdm
Category: Actualidad, Nacional
Tags: asesinato, cpi, Estados Unidos, Fuerzas Militares de Colombia, lideres sociales
Slug: fiscalia-de-la-cpi-respalada-a-la-jep-y-manifiesta-preocupacion-por-asesinato-de-lideres
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/05/Diseño-sin-título-13.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

La Fiscal **Fatou Bensouda de la Corte Penal Internacional, quien está a cargo del caso colombiano**, se manifestó este viernes a favor de la [Jurisdicción Especial de Paz](https://archivo.contagioradio.com/victimas-de-sucre-entregan-informe-sobre-desapariciones-forzadas/) (JEP) y expresó su preocupación en torno a los reiteraros asesinatos de líderes sociales en el país, que en lo corrido del 2020 suman ya 84 casos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La manifestación de la fiscal se dio en el marco de un encuentro virtual llevado a cabo con parlamentarios europeos, en el que también se habló de varios casos que ha asumido [Bensouda](https://archivo.contagioradio.com/la-corte-penal-internacional-exigir-proteccion-lideres/)en otros países.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### El respaldo a la JEP es directo

<!-- /wp:heading -->

<!-- wp:paragraph -->

Sobre la JEP la fiscal manifestó *"**Apoyamos los esfuerzos de esa jurisdicción para asegurar que hay una adecuada rendición de cuentas de los mayores responsables de los crímenes,** incluyendo los comandantes de las [FARC](https://archivo.contagioradio.com/partido-farc-solicitara-formalmente-medidas-cautelares-de-la-cidh-a-favor-de-excombatientes/) y los más altos cargos de la jerarquía militar en los llamados casos de "falsos positivos".*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Cabe señalar que este pronunciamiento llega en un momento en el que el partido de gobierno, el Centro Democrático, ha arreciado sus críticas al [acuerdo de paz](https://www.justiciaypazcolombia.com/colectivo-ram-voces-para-los-desaparecidos/) y a los organismos del Sistema Integral de Verdad Justicia Reparación y No Repetición. Recientemente el Ministro de Defensa Carlos Holmes Trujillo aseguró que es necesario hacerle cambios al acuerdo de paz para mejorarlos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese mismo sentido se pronunció el senador Álvaro Uribe Vélez quien aseguró que no era necesario atacar la JEP pues ese mecanismo se acabaría solo pues, según él, tiene un sesgo que la va a deslegitimar y va a terminar por acabarla.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La preocupación por el asesinato de líderes sociales en Colombia traspasó las fronteras

<!-- /wp:heading -->

<!-- wp:paragraph -->

En otro sentido la fiscal de la Corte Penal Internacional aseguró que hay una preocupación especial en torno al asesinato de líderes sociales en Colombia. En este sentido la preocupación también se ha hecho visible por parte de integrantes del parlamento europeo y de EEUU quienes incluso han llegado a cuestionar la ayuda militar bridanda por EEUU y países de la UE a las FFMM y de seguridad en Colombia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
