Title: Durante festejo de Navidad fue asesinado Reinaldo Carrillo líder social en Huila
Date: 2019-12-25 15:14
Author: CtgAdm
Category: DDHH
Tags: asesinato, Huila, Lider social, Pitalito, Reinaldo Carrillo
Slug: durante-festejo-de-navidad-fue-asesinado-reinaldo-carrillo-lider-social-en-huila
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/EMp4dPzXsAstqof.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Diseño-sin-título.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:ArchivoPersonal] 

Reinaldo Carrillo, líder social en el departamento del Huila, fue asesinado la madrugada del 25 de diciembre, en Pitatilo, mientras departía con su familia. De acuerdo con las primeras versiones, **tres sujetos armados llegaron hasta la vivienda**, ubicada en el barrio Los Pinos y uno de ellos le disparó en varias oportunidades.

Reinaldo Carrillo se desempeñaba como dirigente  social, integrante de la Asociación Nacional de Usuarios Campesinos ANUC, organización **que apoya procesos relacionados con la reforma agraria y la parcelación de predios.** (Le puede interesar:[Estado debe reconocer sistematicidad en asesinato a líderes sociales: Somos Defensores](https://archivo.contagioradio.com/estado-debe-reconocer-sistematicidad-en-asesinato-a-lideres-sociales-somos-defensores/))

### **Triste Navidad para la vida de líderes sociales** 

Durante el mes de diciembre, en Colombia, han sido asesinados  líderes sociales: **Humberto Londoño**, en Tarazá - Antioquia,  quien era coordinador del Comité de Conciliación de la vereda El Socorro- Cañón de Iglesias, y también hacia parte de la Asociación de Campesinos del Bajo Cauca (Asocbac). (Le pude interesar:["El líder social Humberto Londoño, es asesinado en Tarazá, Bajo Cauca")](https://archivo.contagioradio.com/el-lider-social-humberto-londono-es-asesinado-en-taraza-bajo-cauca/)

Posteriormente, el 13 de diciembre en el espacio de reincorporación Santa Lucía, en Ituango, fue asesinado **Manuel González,** excombatiente que lideraba un proyecto de ganadería para continuar en la implementación de los Acuerdos de Paz. (Le puede interesar: [Un nuevo espacio para narrar la verdad se abre en Huila)](https://archivo.contagioradio.com/un-nuevo-espacio-para-narrar-la-verdad-se-abre-en-huila/)

De igual forma el pasado 24 de diciembre fue asesinada Lucy Villarreal, líderesa cultural en el municipio de Llorente-Tumaco, luego de impartir un taller a menores de edad. Villarreal **se dedicaba a la promoción de la cultura en su territorio** y la defensa de derechos humanos y estos hechos se suman el homicidio de Carrillo.  (Le puede interesar:["Líder cultural Lucy Villarreal fue asesinada en Llorente-Tumaco"](https://archivo.contagioradio.com/lideresa-cultural-lucy-villarreal-fue-asesinada-en-llorente-tumaco/))  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
