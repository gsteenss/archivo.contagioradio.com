Title: '9 habitantes de calle son asesinados cada mes': Alirio Uribe Muñoz
Date: 2016-12-04 19:09
Category: DDHH, Nacional
Tags: Alcaldía Enrique Peñalosa, Desalojo de habitantes de calle, desalojo del Bronx, Limpieza social
Slug: 9-habitantes-de-calle-son-asesinados-cada-mes-alirio-uribe-munoz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Habitantes-de-calle..jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [4 Dic 2016] 

Una audiencia pública convocada por los representantes Ángela María Robledo, Inti Asprilla y Alirio Uribe Muñoz, de la Alianza Verde y el Polo Democrático respectivamente, para discutir la situación de los más de 3.500 habitantes de calle desalojados del Bronx, reveló que **después de 7 meses del suceso el distrito sólo ha atendido a 550 personas y el fenómeno de la limpieza social ha venido afectando de manera particular a esta población.**

Alirio Uribe denunció en el debate público que de acuerdo con reportes de Medicina Legal **“este año han sido asesinados 99 habitantes de calle en Bogotá hasta el 30 de noviembre: es decir, 9 habitantes de calle cada mes,**  lo cual es una situación muy dramática que significa que por lo menos cada tercer día se cometen crímenes por intolerancia contra los habitantes de calle”, aseguró el representante.

Las cifras sobre el fenómeno de limpieza social según datos del CINEP y el libro del Centro de Memoria Histórica ‘Limpieza Social, Una violencia mal nombrada’, señalan que es una práctica con presencia en **28 departamentos y 356 municipios del país que ha dejado un saldo de 4.928 personas asesinadas en todo el país.** Le puede interesar: Habitantes de Calle [exigen se respeten sus derechos.](https://archivo.contagioradio.com/habitantes-de-calle-se-encuentran-hacinados-en-la-carrera-30-con-calle-sexta/)

De esos casos, “189 fueron en Bogotá dejando un saldo de **346 homicidios, perpetrados en parte por sujetos aún no identificados y otros por miembros del Bloque Capital”.** Debido a los vacios legales del aparato normativo colombiano, esta es una modalidad de crimen que está en total impunidad, pues “no existen mayores registros sobre el tema ni en las instituciones ni en las investigaciones académicas” concluye el informe del Centro de Memoria.

### **¿Cuáles son las responsabilidades de las instituciones?** 

Frente a la dramática situación, la alcaldía de Enrique Peñalosa no ha realizado las mejoras necesarias para solventar el déficit de oferta institucional, que se traduce en que los establecimientos de atención a los habitantes de calle han llegado a niveles de ocupación superiores **al 90%, en los hogares de paso día y paso día y noche la ocupación es de 289 habitantes y 285 respectivamente,** lo que representa una ocupación del 96% y 95%.

Por último, la audiencia en la que también participaron educadores, sociólogos, funcionarios del IDIPRON, ex habitantes de calle y artistas urbanos del Bronx, puntualizó que “el alcalde Enrique Peñalosa está en la obligación de dar cumplimiento a lo establecido por la **Ley 1641 de 2013, respecto a la protección y restablecimiento de derechos a habitantes de calle y las sentencias de la Corte Constitucional 881 de 2002 y 043 de 2015,** que indican que son sujetos de especial protección”.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio.]{.s1}](http://bit.ly/1ICYhVU)
