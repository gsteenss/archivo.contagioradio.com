Title: Animalistas seguirán movilizándose para exigir no más olé
Date: 2017-02-02 17:13
Category: Animales, Nacional
Tags: Animalistas, Antitaurinos, Congreso, Taurinos
Slug: animalistsa-seguiran-movilizandose-35690
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Marcha-No-a-las-corridas-de-toros-30-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:  Contagio Radio 

###### 2 Ene. 2017 

**“Dos años son mucho, al Congreso le corresponderá estar a la altura en el tema de los toros” así lo aseguró Natalia Parra**, directora de la Plataforma Alto, a propósito del plazo de dos años dado por la Sala Plena de la Corte Constitucional al Congreso de la República para que legisle sobre las corridas de toros y los espectáculos como las corralejas, el coleo, las peleas de gallos, entre otros, que incluyen maltrato a animales.

En el debate que se llevó a cabo en la Sala Plena los magistrados declararon inexequible el parágrafo 3 del artículo 5 de la ley 1774 de 2016, de la cual fue excluida la penalización de las corridas de toros. Le puede interesar: [“Los animalistas somos pacifistas”](https://archivo.contagioradio.com/los-animalistas-somos-pacifistas-35020/)

**Serán 2 años en lo que los taurinos podrán buscar algunas medidas para que este tipo de “fiesta brava”** que ha sido considerado por ellos como un “arte” pueda seguirse dando en el territorio nacional. Sin embargo, si el Congreso no toma una decisión de fondo en ese mismo tiempo, se entenderá que este tipo de espectáculos quedan prohibidos en el país. Le puede interesar: [Caracol Radio: ¿Los Antitaurinos son terroristas?](https://archivo.contagioradio.com/caracol-radio-los-antitaurinos-son-terroristas/)

Se conoció que ya se encuentran trabajando en conjunto diversos sectores políticos como el ministro de Interior Juan Fernando Cristo y el viceministro del Interior Luis Ernesto Gómez y animalistas de la Plataforma Alto y la Coalición Ciudadana Colombia Sin Toreo, para integrar los proyectos de ley que existen sobre este tema y de esta manera **aunar esfuerzos para abolir las corridas de toros en Colombia.**

Además el movimiento animalista anunció que se continuarán llevando a cabo diversas actividades y movilizaciones para sensibilizar y seguir rechazando los espectáculos en los que estén los animales y se les torture.

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}

<div class="ssba ssba-wrap">

</div>

<div class="ssba ssba-wrap">

</div>
