Title: Empresa Cerrejón tiene al borde de la muerte al Río Ranchería
Date: 2015-08-22 14:41
Category: DDHH, Entrevistas, Nacional
Tags: CORPOGUAJIRA, Empresa Cerrejon, Guajira, Hambre en la Guajira, La Serranía de Macuira, Niños de la Guajira, ONIC, Organización Nacional Indígena de Colombia, río Ranchería, Wayuu
Slug: empresa-cerrejon-tiene-al-borde-de-la-muerte-al-rio-rancheria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/guajira_wayuu_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: confidencialcolombia 

<iframe src="http://www.ivoox.com/player_ek_7293194_2_1.html?data=mJemlZadeI6ZmKiak5yJd6KkmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRhdPhwtPR0ZC6pc3W1srbw4qWh4zdz8mSpZiJhaXbxtPOjbzFvdbpjN6Y0tfJt8rYxtPhx5DIqYzgwpC81JKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Armando Valbuena,presidente de la **ONIC**] 

###### [22 Ago 2015] 

**La Serranía de Macuira y el río Ranchería**, han sido afectadas tanto por el cambio climático, como por los 30 años de extracción carbonífera por parte de Cerrejón. **La empresa minera para extraer el carbón usa las aguas del rio y luego vierte en su cuenca las aguas contaminadas resultantes.** Lo más grave es que esas son las dos únicas fuentes de las que depende el abastecimiento de agua y alimentos del pueblo Wayuu,

Por otra parte, pese a que según datos del proyecto Children's Worlds los niños colombianos son los segundos más felices del mundo, la **corrupción en las administraciones y los proyectos mineros implementados en la Guajira han provocado la muerte de por lo menos 4000 niños en todo el departamento.**

Armando Valbuena, indígena Wayuu y presidente de la **Organización Nacional Indígena de Colombia, ONIC**, denuncia deficiencia en los servicios nutricionales administrados por el Ministerio de Educación Nacional e implementados por el Instituto Colombiano de Bienestar Familiar a los niños en La Guajira, los alimentos que provienen de otras partes del país están llegando en estado de descomposición, sumado a la baja calidad en la atención hospitalaria para estos pueblos indígenas.

Según Valbuena es evidente la inacción de los entes estatales, pues aun cuando la **Corte Constitucional profirió un auto en 2009 que obligaba al Estado a ejecutar un plan de salvaguarda** para la región éste no ha sido aplicado, por el contrario ha aumentado la corrupción en sus funciones administrativas y los avances en la solución de estas problemáticas han sido mínimos.

El pueblo indígena Wayuu exige que el Ministerio de Educación Nacional, el Instituto Colombiano de Bienestar Familiar y el Ministerio de Salud se reúna con las comunidades para la construcción de planes de acción concretos, claros y precisos que permitan que los niños **indígenas Wayuu cuenten con servicios nutricionales de calidad, pues no se trata solamente de aumentar la cobertura.**
