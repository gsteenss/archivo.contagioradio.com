Title: Organizaciones indígenas y afro radican tutela contra reforma a Ley de Tierras
Date: 2018-10-01 12:44
Author: AdminContagio
Category: DDHH, Movilización
Tags: Ley de Tierras, ONIC, planton, tutela
Slug: tutela-reforma-ley-de-tierras
Status: published

###### [Foto: Contagio Radio] 

###### [1 Oct 2018] 

Este lunes, organizaciones indígenas y afrodescendientes interpusieron una **tutela contra el Proyecto que modifica la Ley de Tierras, porque no fue consultada con las comunidades.** Por esta razón, en medio de un plantón frente al Tribunal Superior de Bogotá, la Organización Nacional Indígena de Colombia (ONIC) pidió que se retire el **Proyecto de Ley 03 de 2018**, radicado por el gobierno Santos antes de terminar su mandato.

> [\#LasConsultasSeRespetan](https://twitter.com/hashtag/LasConsultasSeRespetan?src=hash&ref_src=twsrc%5Etfw)| Continuamos de pie en defensa de la vida y nuestros derechos. Por eso hoy radicamos ante el Tribunal Superior de Bogotá la tutela contra el proyecto inconsulto de reforma a la Ley 160 de 1994. [@luiskankui](https://twitter.com/luiskankui?ref_src=twsrc%5Etfw) [@MPCindigena](https://twitter.com/MPCindigena?ref_src=twsrc%5Etfw) [@MovimientoMAIS](https://twitter.com/MovimientoMAIS?ref_src=twsrc%5Etfw) [@IvanDuque](https://twitter.com/IvanDuque?ref_src=twsrc%5Etfw). [pic.twitter.com/zrTniKbMsX](https://t.co/zrTniKbMsX)
>
> — ONIC (@ONIC\_Colombia) [1 de octubre de 2018](https://twitter.com/ONIC_Colombia/status/1046797433613647874?ref_src=twsrc%5Etfw)

Según **Luis Fernando Arias, consejero mayor de la ONIC,** la tutela busca amparar el derecho fundamental al territorio y la consulta previa libre e informada de los pueblos indígenas, frente a la reforma de la Ley 160 de 1994, o Ley de tierras, que hace su curso en el Congreso. Con la acción legal, **la Organización busca que el Proyecto sea retirado del Legislativo,** y se cumplan los procedimientos constitucionales para realizar modificaciones normativas.

Para el Consejero, **el Gobierno incumplió a la Constitución al presentar el Proyecto sin concertación**; sin embargo, confía en que la Corte Constitucional, como garante de la carta legislativa de 1991, "salvaguarde y ampare los derechos fundamentales de los pueblos indígenas", teniendo en cuenta que se trata de un tema estructural, como la reforma a la Ley que establece el marco legal para la titulación de los territorios étnicos.

El líder indígena señaló que este Proyecto de Ley por su naturaleza es importante para sectores étnicos y campesinos, por eso espera que la Corte mantenga la posición que ha tenido en estos temas; y recordó que **cuando el ex presidente Uribe presentó el Estatuto de Desarrollo Rural, también fue demandado por inconsulto,** y el alto tribunal se pronunció en favor de los indígenas, protegiendo sus derechos. (Le puede interesar:["Más de 300 mil hectáreas se han entregado de forma irregular en Colombia"](https://archivo.contagioradio.com/300-hectareas-irregular-colombia/))

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
  

<iframe id="audio_29007254" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_29007254_4_1.html?c1=ff6600">
</iframe>
</p>
###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio](http://bit.ly/1ICYhVU)]
