Title: 800 indígenas en riesgo de desplazamiento por presuntos paramilitares
Date: 2016-08-17 13:40
Category: DDHH, Nacional
Tags: Paramilitares en Chocó, presencia paramilitar en San Juan, tortura a mujer indígena en Chocó
Slug: 800-indigenas-en-riesgo-de-desplazamiento-por-presuntos-paramilitares
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/El-Bagre-paramilitarismo.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Contagio Radio ] 

###### [17 Ago 2016 ] 

De acuerdo con la Comisión Intereclesial de Justicia y Paz este lunes en horas de la mañana **dos hombres armados, presuntamente paramilitares, torturaron a mujer indígena** que se encontraba en su parcela ubicada en el cabildo de Puerto Guadualito, a orillas de la Quebrada Cheché, del Resguardo Wounaan de Puerto Pizario, en el municipio de Litoral de San Juan, Chocó.

Los hombres armados, retuvieron y torturaron a Evelina Quiro Chirimía, indígena de 34 años de edad y madre de 5 hijos, cuando llegaba a su parcela en busca de alimentos; le preguntaron por la identidad de un indígena que estaba fotografiado en un celular y al guardar silencio la golpearon con patadas y con las armas que llevaban, tiempo después **con un cuchillo le hicieron cortes en cuatro de sus dedos**.

Según testimonios de varios de los pobladores, estos golpes causaron en Evelina pérdida temporal del conocimiento, por lo que **no se descarta que haya sido abusada sexualmente**. Evelina fue encontrada por su esposo e hijos, trasladada a la comunidad y luego al centro de salud de Palestina desde donde fue remitida a la Clínica Santa Sofia de Buenaventura, donde recibe atención médica.

De acuerdo con el comunicador popular Saul Chamarra, la comunidad está atemorizada pues llegó **una orden que les prohíbe el tránsito entre las fincas**, persiste la presencia de actores armados y pese a las denuncias presentadas ninguna autoridad se ha manifestado, por lo que contemplan desplazarse temporalmente al municipio de Buenaventura, como ya lo hicieron los familiares de Evelina.

Vale la pena recordar que las comunidades de Puerto Pizario, Unión San Juan y Puerto Guadualito, **decidieron declararse como Territorio de Paz, Humanitario y Biodiverso** desde el pasado 22 de junio ante el asedio de las estructuras paramilitares que controlan el río San Juan.

<iframe src="http://co.ivoox.com/es/player_ej_12650383_2_1.html?data=kpejl5WXfJShhpywj5aUaZS1lpiah5yncZOhhpywj5WRaZi3jpWah5yncarixYqwlYqliMjZz8bgjbzTuc_VwtOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 

###### 
