Title: Colombia registró más de 90 mil desplazamientos forzados en 2017
Date: 2018-06-20 13:38
Category: DDHH, Nacional
Tags: ACNUR, Catatumbo, colombia, Desplazamiento forzado, Norte de Santander
Slug: colombia-registro-mas-de-90-mil-desplazamientos-forzados-en-2017
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/desplazamiento-3.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [20 Jun 2018] 

El más reciente informe de la Agencia para los Refugiados de la ONU (ACNUR) reveló la grave situación que continúan viviendo las personas víctimas de desplazamiento interno en el país. El documento señaló que, desde **1985 Colombia ha tenido 7.7 millones de personas desplazadas, mientras que la cifra del 2017 es de más de 90 mil personas**.

De acuerdo con Rocío Castañeda, oficial de información pública de la ACNUR, si bien esta cifra del último año es alarmante, en términos generales ha existido una reducción del desplazamiento forzado al interior del país debido a que, en años anteriores, esta cifra llegaba hasta **las 200 mil personas víctimas de desplazamiento**. Pata Castañeda, la disminución en las víctimas de este flagelo, es producto de “el Acuerdo de paz, que ha tenido unas consecuencias positivas en este sentido”.

### **El desplazamiento forzado en Colombia** 

En el caso de Colombia, las regiones que presentaron mayores desplazamientos forzados durante el 2017 son el bajo Cauca Antioquieño, el Pacífico, Nariño, Norte de Santander y el Catatumbo, en donde también se presenta una alta vulnerabilidad para las personas víctimas de esta violencia. Además**, son lugares en donde se ha concentrado la presencia de grupos armados que se disputan el control de los territorios.**

En lo corrido de este año, ACNUR reportó que se han registrado **34 eventos de desplazamientos masivos, que han obligado a que 13.706** personas se muevan de sus territorios, afectando principalmente a comunidades campesinas. Asimismo, de 2016 a 2017, la cifra total de desplazamientos internos pasó de 6.3 millones de personas a 6.2 millones.

“Desde ACNUR en Colombia estamos haciendo un llamado especialmente a la situación en ciertas regiones del país en donde el año pasado y este año se ha concentrado la situación de desplazamiento forzado masivo” afirmó Castañeda. (Le puede interesar: "[Estado pide perdón por desplazamiento de comunidad de Inaia Sue, en Tenjo"](https://archivo.contagioradio.com/inaia-sue/))

En esa medida, los retos y desafíos que tiene el país son varios. El primero de ellos tiene que ver con lograr una presencia integral en los territorios más afectados, que no solo se refleje en el aumento del píe de Fuerza y la seguridad. Otro de los retos, de acuerdo con Castañeda, **se encuentra en el apoyo a las entidades locales, que en muchas ocasiones se ven sobrepasadas por las situaciones de desplazamientos masivos**.

“Es importante que se mejore la coordinación entre las entidades del orden central y local para que finalmente la atención humanitaria a la población que se ve obligada a desplazarse se de manera oportuna y eficiente” aseguró la vocera de la ACNUR.

Frente a la cifra de personas que lograr retornar a su territorio, luego de haber sido víctimas de desplazamiento forado, Roció Castañeda expresó que los índices son muy bajos. Los grupos que más regresan a sus lugares de origen son las comunidades indígenas y en la gran mayoría de los casos **lo hacen sin el acompañamiento estatal necesario que garantice la permanencia y condiciones dignas de los mismos.**

### **La migración de venezolanos a Colombia**

Otra de las situaciones que debe afrontar el país, es la migración de más de 800 mil venezolanos que han llegado al país. En ese sentido la ACNUR recomienda la creación de políticas **públicas que permitan la respuesta y atención adecuada a esta situación, con un enfoque humanitario**.

El documento también menciona que los riesgos a los que más se ven expuestas las personas que tuvieron que irse de sus territorios, son la falta de rutas seguras por dónde movilizarse, los muros y **barreras que obstaculizan el ingreso de las personas que los deja a la deriva de las redes de tráfico de personas, la explotación laboral, entre otras. **(Le puede interesar: ["Comunidades del Litoral Bajo San Juan retornan a su territorio tras desplazamiento forzado"](https://archivo.contagioradio.com/comunidades-del-litoral-del-bajo-san-juan-retornan-a-su-territorio-tras-desplazamiento-forzado/))

[5b27be547](https://www.scribd.com/document/382202984/5b27be547#from_embed "View 5b27be547 on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_25335" class="scribd_iframe_embed" title="5b27be547" src="https://www.scribd.com/embeds/382202984/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-hRyRz1O7BU5R858qpkTP&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7080062794348508"></iframe><iframe id="audio_26642348" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26642348_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
