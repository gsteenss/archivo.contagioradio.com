Title: Resguardos indígenas de San Vicente del Caguán denuncian presencia paramilitar en la zona
Date: 2015-03-16 23:15
Author: CtgAdm
Category: DDHH, Nacional
Tags: Asociación de Cabildos Indígenas, Caguán Vive, Derechos Humanos, FARC, Naciones Unidas, proceso de paz, San Vicente del Caguán
Slug: resguardos-indigenas-de-san-vicente-del-caguan-denuncian-presencia-paramilitar-en-la-zona
Status: published

###### Foto: [noticias.ipesderechoshumanos.org]

##### <iframe src="http://www.ivoox.com/player_ek_4222514_2_1.html?data=lZeflJqVeI6ZmKiakp6Jd6KpmZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjc%2FYhqigh6aoq8biwtiYx9OPl8LijLvWxcrSuMafxcrZjajFq9aZpJiSo5bSb9Tjz5Djh6iXaaK4xNnWz8bXb8XZjJKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### **[Luz Mery Panche, Asociación de Cabildos Indígenas]** 

La Asociación de Cabildos Indígenas, en San Vicente del Caguan, **denuncia la presencia de encapuchados armados en el municipio**, que de acuerdo a la comunidad, harían parte de grupos paramilitares que tienen gran presencia en ese sector.

Según Luz Mery Panche, quien hace parte de la Asociación de Cabildos Indígenas, “la situación es preocupante”, **la comunidad ha hecho una alerta temprana debido a que el pasado 5 de marzo,** las personas de la vereda Puerto Amor, reportaron la presencia de civiles encapuchados en una de las fincas del sector, en donde intentaban llevarse ganado, “pero la comunidad reaccionó y lo impidió”, asegura Panche.

“En ese sector se estaba desarrollando una misión de verificación sobre la situación de DDHH, y la comisión tuvo conocimiento de ese hecho”, indica la indígena, quien señala que esa comisión está integrada por delegados de las Nacionales Unidas y miembros del gobierno que estaban en la zona donde sucedió el hecho. Sin embargo, pese a que se está verificando la situación y hay varias denuncias ante la Fiscalía, **la comunidad no ha conocido ningún resultado sobre las investigaciones de las autoridades**.

Pero no solo se trata de este hecho, los indígenas llevan denunciando desde el año pasado las **amenazas que llegaron directamente a la corporación de derechos humanos, Caguán Vive, a través de internet y panfletos,** donde se pone en peligro la vida de varios dirigentes campesinos e indígenas, entre ellos el alcalde del municipio, Domingo Pérez.

Así mismo, la comunidad del resguardo Yaguará, denunció en febrero la presencia de las fuerzas militares, lo que ha generado una situación de desplazamiento forzado por la presión que ejerce el Ejército Nacional sobre los indígenas.

La integrante de la Asociación de Cabildos Indígenas, indica que una de las posibles causas de los enfrentamientos en esos territorios **es la explotación petrolera que se lleva a cabo en ese lugar.** Finalmente dijo que la comunidad respalda el proceso de paz y subrayó que aplauden la del cese de bombardeos contra las FARC-EP, ya que “**la población siempre está en medio de los ataques”, afirmó.**
