Title: A dos años de la masacre de El Tandil, en Tumaco persiste la impunidad
Date: 2019-10-03 16:08
Author: CtgAdm
Category: DDHH, Nacional
Tags: El Tandil, Fuerza Pública, Masacre de El Tandil, Tumaco
Slug: a-dos-anos-de-la-masacre-de-el-tandil-en-tumaco-persiste-la-impunidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-03-at-3.16.38-PM.jpeg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/WhatsApp-Image-2019-10-03-at-3.16.39-PM.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASOMINUMA] 

El 5 de octubre de 2017, mientras habitantes de la vereda El Tandil formaban un cordón humano, oponiéndose a que la Fuerza Pública erradicara a la fuerza los cultivos de uso ilícito a los que habían accedido, uniformados de la policía dispararon contra la población **asesinando a 7 personas e hiriendo a otras 25**. Dos años después la investigación que está en manos de la justicia penal militar, no ha esclarecido los hechos ni castigado a los responsables de la masacre, ni tampoco ha brindado garantías de participación a las víctimas.

La abogada de la Corporación Jurídica Yira Castro, Lucía Aldana afirma que existe un alto grado de impunidad en la que resulta ser la primera masacre ocurrida tras la firma del Acuerdo de Paz, pues únicamente se han realizado una audiencia de imputación y otra de acusación de la que **no se notificó ni a las víctimas ni a sus representantes**. [(Le puede interesar: Entre los policías que atacaron la Misión Humanitaria "estaban los que nos mataron")](https://archivo.contagioradio.com/los-policias-que-nos-mataron-tumaco/)

Este hecho representa una ausencia de garantías de participación de las víctimas, pero lo más grave , explica Aldana, radica en que en las audiencias realizadas, se definió que el caso cumplía con los requisitos para ser llevado a la justicia penal militar, un escenario que señalan "de impunidad total en el que los militares mismos investigarían una situación que se sale de las funciones del servicio de la Fuerza Pública".

De igual forma no existen avances en las imputaciones a Luis Fernando González, mayor de la V División del Ejército y Javier Enrique Soto, capitán de la Policía, quienes debieron proteger a la comunidad. Tampoco se ha imputado a los autores materiales, siendo la versión de la Fuerza Pública que durante los hechos actuaron ante ataque de las disidencias de las FARC, versión que fue desmentida por la población presente en el lugar de los hechos.

Es por tal motivo que los representantes de las víctimas, están considerando una acción de nulidad contra la audiencia de acusación donde se suponía debían ser acreditadas las víctimas o incluso llevar el caso a la Corte Interamericana de Derechos Humanos. [(Le puede interesar: Los testimonios que comprometen a la Fuerza Pública en masacre de Tumaco)](https://archivo.contagioradio.com/los-testimonios-que-comprometen-a-la-fuerza-publica-en-masacre-de-tumaco/)

Cabe resaltar que el 7 de octubre, días posteriores a los hechos, durante una visita realizada por una Misión humanitaria que se dirigía hacia el lugar donde sucedió la masacre, defensores de derechos humanos, periodistas y delegados de diferentes organizaciones fueron atacados con gases lacrimógenos, bombas aturdidoras y disparos por integrantes de la Fuerza Pública. [(Lea también: Policía ataca a misión humanitaria en el lugar de la masacre de Tumaco)](https://archivo.contagioradio.com/policia-ataca-mision-humanitaria-47729/)

### El Tandil, dos años después

Dos años después de la masacre, relata Aldana, es más evidente la fricción entre la Fuerza Pública y los habitantes del municipio. Estos últimos han manifestado un sentimiento de impotencia, pues quienes dispararon directamente contra la población aquel 5 de octubre, continúan haciendo presencia en el territorio ejerciendo sus actividades.

A propósito del sentir de la comunidad de El Tandil y el trabajo que viene realizando la Corporación Yira Castro, se realizara un acto de memoria este 5 de octubre donde se conmemorará a través de una eucaristía y un acto simbólico la memoria de quienes fueron asesinados aquel día. Acto seguido, el 7 de octubre la población realizará un plantón pacífico para reclamar justicia y exigir un mayor avance en las investigaciones.

### **Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
