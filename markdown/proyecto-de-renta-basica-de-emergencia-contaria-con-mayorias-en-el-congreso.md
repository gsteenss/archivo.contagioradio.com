Title: Proyecto de Renta Básica de Emergencia contaría con mayorías en el congreso
Date: 2020-05-27 21:05
Author: AdminContagio
Category: Actualidad, Política
Tags: Congreso, pandemia, Renta básica
Slug: proyecto-de-renta-basica-de-emergencia-contaria-con-mayorias-en-el-congreso
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Mínimo-Vital-para-la-Vida.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este miércoles 27 de mayo, más de 54 congresistas de 9 partidos presentaron el Proyecto de Ley mediante el que se modifica el Decreto Ley 518 de 2020, creando así una renta básica de emergencia para afrontar la pandemia. El Decreto que modifica es el que creó el programa Ingreso Solidario, considerando que precisamente será de estos recursos, y de otras fuentes, de donde se obtendrá la financiación de la renta básica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Durante el espacio de formalización de la presentación, también se entregaron las firmas de 20 mil ciudadanos y ciudadanas que apoyaron la petición de la creación de una renta básica para atender la población más vulnerable en momentos de pandemia. (Le puede interesar: ["El Mínimo para la Vida que propone la bancada de oposición"](https://archivo.contagioradio.com/la-propuesta-del-minimo-para-la-vida-de-la-bancada-de-oposicion/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/comunacuerdo/status/1265786716377493511","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/comunacuerdo/status/1265786716377493511

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Una propuesta que tiene mayoría en el Congreso

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según explicó el senador Iván Marulanda, la propuesta de la renta básica de emergencia pretende beneficiar a 6,8 millones de hogares pobres que suelen derivar su sustento del rebusque y a 2,2 que se sustentan con micronegocios o de forma independiente. De esta manera, se cubriría a un total de 9 millones de familias, es decir, cerca de 30 millones de personas.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La idea es que las familias reciban el total de un Salario Mínimo Legal Vigente (877 mil pesos) durante 3 meses, con posibilidad de prórroga. Sus fuentes de financiación serían los programas de asistencia ya existentes: Familias en Acción, Jóvenes en Acción, Adulto Mayor, Ingreso Solidario, entre otros, que serían complementados para alcanzar el monto de la Renta Básica.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, los congresistas plantean la necesidad de reorientar el gasto público para dejar de hacer inversiones en elementos no prioritarios y dirigir esos recursos a la renta de emergencia. Otra fuente de financiación sería por la vía de una emisión del Banco Central, a modo de préstamo que tendría respaldo en las reservas internacionales; y también lo podría ser la refinanciación de la deuda.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En otras ocasiones, distintas organizaciones han impulsado la necesidad de este ingreso de emergencia con propuestas como la de FARC o el Mínimo Vital pero el Gobierno aún no responde sobre esta exigencia. Sin embargo, los senadores que presentaron la propuesta destacaron que la misma tiene mayoría en la cámara alta del Congreso, contando con el aval de 54 curules, de 9 partidos.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Sobre la necesidad de la renta básica de emergencia

<!-- /wp:heading -->

<!-- wp:paragraph -->

El senador Jorge Robledo explicó que la renta sería, al tiempo, una inyección de recursos para la economía porque "si la gente tiene dinero, compra más y se estimula el desarrollo económico". Por su parte, el director de Viva la Ciudadanía, Luciano Sanín, reclamó que el 60% de los colombianos están en condición de pobreza o vulnerabilidad, de tal forma que la propuesta es un salvavidas que se considera un derecho de las y los ciudadanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por su parte, la integrante de la plataforma DHESCA María Eugenia Ramírez manifestó que la pandemia tiene efectos desproporcionados sobre la vida de las mujeres porque según el DANE, de los 9 millones de hogares que se beneficiarían de la propuesta, más del 40% tienen a una mujer como cabeza de familia. A ello se suma que más del 50% del trabajo informal lo realizan mujeres, de tal forma que se hace necesario este auxilio para ellas.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Próximo round: 23 de junio, en la nueva legislatura

<!-- /wp:heading -->

<!-- wp:paragraph -->

Los senadores recordaron que la propuesta de mínimo vital se presentó al Ministerio de Hacienda, pero el minsitro Carrasquilla la rechazó. Por eso anunciaron que será vía legislativo, mediante la modificación de un[Decreto Ley](https://www.justiciaypazcolombia.com/proyecto-de-ley-por-medio-del-cual-se-modifica-el-decreto-ley-518-de-2020-ingreso-solidario-y-renta-basica/), que buscarán que se adopte esta propuesta, que desde su nacimiento cuenta con mayorías en el Senado.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual forma, los Congresistas anunciaron que también presentarán una reforma tributaria estructural que esté en línea con las necesidades actuales, es decir, que sea equitativa, progresiva y suficiente. Dicha propuesta buscaría suprimir los beneficios tributarios, que por año se estima que cuestan al país más de 10 billones de pesos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También lo invitamos a consultar: [Saqueos se detienen con renta básica no con represión: MOVICE Caldas](https://archivo.contagioradio.com/saqueos-se-detienen-con-renta-basica-no-con-represion-movice-manizales/).

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
