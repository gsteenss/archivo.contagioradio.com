Title: Pensarse desde lo colectivo en la apuesta de comunicar para la paz: innovar para transformar
Date: 2019-10-18 13:03
Author: JUSTAPAZ
Category: Columnistas
Tags: Acuedo de Paz, Justapaz, paz
Slug: pensarse-desde-lo-colectivo-en-la-apuesta-de-comunicar-para-la-paz-innovar-para-transformar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Comunicación-770x540.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio radio] 

##### **Por: Wendy Ramos – JUSTAPAZ** 

> “Mucha gente pequeña, en lugares pequeños, haciendo cosas pequeñas, puede  
> cambiar el mundo” Eduardo Galeano

Las actuales problemáticas sociales, culturales, económicas y políticas en Colombia y América Latina nos invitan a re-pensar las formas como estamos comunicando, la inspiración de nuevas utopías que nacen desde la sociedad civil y los movimientos sociales, nos invitan a recordar que comunicar para la paz es un  
compromiso político, social, cultural y ético, necesario e importante en este momento coyuntural.

Debemos pensar una comunicación intencionada hacia el empoderamiento activo de la ciudadanía, que genere espacios críticos como sujetos y sujetas de derechos frente a una dinámica de transición hacia la paz y pensarnos como un país con fuertes apuestas hacia la transformación y una cultura de la reconciliación. Pero *¿Cómo nos pensamos esta visión alternativa de mundo?*

#### Son diferentes los caminos… 

Primero, es importante revisar qué estamos haciendo para comunicar para la paz.

*¿Tenemos conciencia del rol de los medios dominantes?* El mismo Chomsky y diferentes teorías de la comunicación nos han alertado sobre cómo estos medios de poder tienen el arte de interrumpir el pensamiento alternativo y el sentido crítico de la ciudadanía.

##### Y allí está nuestro primer desafío: 

debemos empoderar a las personas y visibilizar aquellas voces históricamente opacadas, informar con  
veracidad y no tener miedo a contrastar las voces que no siempre pensarán igual que nosotras/os.

Revisemos nuestras prácticas de cara al contexto, apostemos a la innovación y a nuevas estrategias comunicativas que nos permitan producir sentido y re-significarlo. Desafiemos nuestros formatos y formas de comunicar, revisemos si lo tradicional sigue siendo vigente, no olvidemos que la comunicación es un actor social con incidencia y que la comunicación para la paz es dinámica y necesita encontrar nuevas rutas desde una mirada crítica y analítica.

Valdría la pena recoger una de las bases metodológicas de la educación popular, Freire nos enseñó a ver el contexto, escudriñarlo, luego a juzgarlo, haciendo un análisis juicioso y profundo y luego a actuar, y es allí donde estamos llamados a generar nuevas estrategias de comunicación innovadoras en creación colectiva de compromisos para transformar la realidad.

le puede interesar: ([Para la guerra nada, nuestra bandera es la paz](https://archivo.contagioradio.com/para-la-guerra-nada-nuestra-bandera-es-la-paz/))

Por otra parte, hacer comunicación para la paz es un aprendizaje colectivo, que difícilmente sea viable si se hace individualmente. Es importante apostar a realizar un trabajo articulado –*que en algunas oportunidades, los egos lo dificultan*- Sin embargo, los medios de comunicación y las herramientas para comunicar para la paz tienen el poder de tender puentes entre diversas experiencias, es un deber pensarnos en conjunto, abonar esfuerzos nunca será una pérdida de tiempo y por el contrario dinamiza y cataliza las apuestas de creer que sí es posible un país en paz, desde la educación y la comunicación movilizadora, alternativa, participativa y ciudadana.

La invitación final, es a abrir espacios comunicativos de diálogo e integración que recojan las estructuras simbólicas en las que la sociedad ve, interpreta y vive las realidades y problemáticas para transformarlas, apostemos a democratizar la palabra y hagamos de ella una herramienta de transformación.

Desde la terquedad no dejemos de hacer cosas pequeñas pensando en grande, innovemos y hagámoslo en conjunto. Pensémonos desde lo alternativo, seamos protagonistas y hagamos protagonistas a los indignados/as. Gritemos fuerte y demostremos a las estructuras de dominación que la transformación se hace con las personas, resistiremos… y la comunicación para la paz es la mejor herramienta para hacerlo posible.

vea mas:([somos justapaz)](https://www.justapaz.org/)
