Title: UNEES denuncia persecución y amenazas desde Fuerza Pública a estudiantes
Date: 2018-12-11 11:16
Author: AdminContagio
Category: DDHH, Educación
Tags: Derechos Humanos, ESMAD, estudiantes, Fuerza Pública, universidad
Slug: persecucion-fuerza-publica-estudiantes
Status: published

###### [Foto: @UneesCol] 

###### [10 Dic 2018] 

La Unión Nacional de Estudiantes de Educación Superior denunció una serie de actos violentos, **como hostigamientos, persecuciones y acoso sexual**, que se han registrado en contra de voceros de esta plataforma e integrantes de la comisión de derechos humanos, que a pesar de ser denunciados ante las autoridades, no han tenido ninguna respuesta.

Si bien algunos estudiantes afirmaron que pueden estarse dando muchos más casos de este tipo en su contra, en los micrófonos de Contagio Radio denunciaron **6 casos en concreto de pares que han sido víctimas de persecuciones, amenazas y actos violencia**. (Le puede interesar: ["Con marcha, paro estudiantil conmemora 90 años de la masacre de las bananeras"](https://archivo.contagioradio.com/masacre-de-las-bananeras/) )

### **Las persecuciones a las y los voceros de la UNEES** 

Frente a las vocerías e integrantes de la UNEES que han sido víctimas de persecución, la Red Distrital de Derechos Humanos aseguró que hasta el momento han podido recolectar información sobre 6 casos reportados en la Universidad Pedagógica, donde una de las víctimas es la vocera **Ana María Flores, mientras que la Universidad Distrital y la Universidad Nacional, han reportado 3 casos cada una.**

A estas denuncias se suma la de Waldir Achury, integrante de la comisión de prensa de la plataforma estudiantil, quien afirmó haber recibido varias amenazas en medio de las movilizaciones, una de ellas de muerte, y aseguró que a pesar de haber denunciado a las entidades correspondientes como **la Fiscalía, ha sido revictimizado y aún no ha recibido la atención necesaria por parte de las autoridades.**

Cristian Guzmán, vocero de la Universidad Nacional sede Medellín, denunció que desde la vicerectoría de ese centro educativo, se habrían hecho afirmaciones en contra de los estudiantes, sindicándolos de ser parte de estructuras armadas; acto que pone en riesgo la vida y seguridad de las y los voceros y que según Guzmán, da paso a procesos como los mal llamados "falsos positivos judiciales", en contra del movimiento estudiantil.

### **Estudiantes de universidades privadas también denuncian violaciones de DD.HH** 

De igual forma Santiago Rodríguez, vocero de la UNEES de la Universidad Javeriana, denunció persecuciones en su contra. Mientras que Luis Veléz, estudiante de la Universidad Autonoma Latinoaméricana de Medellín, manifestó que durante la movilización del pasado 10 de octubre, las directivas de ese centro educativo decidieron cerrar las puertas, sin permitirle al estudiantado participar  de la marcha y realizaron un llamado a la comunidad académica para que no se acercaran a la institución, porque se presentarían **"actos vandálicos".**

### **El acoso sexual otra forma de hostigar al movimiento estudiantil** 

La Red de Derechos Humanos señaló que otras de las denuncias que han podido recolectar tienen que ver con los acosos sexuales de parte de integrantes del ESMAD hacia las mujeres que han participado de las movilizaciones y que en reiteradas ocasiones, han utilizado un lenguaje sexista en contra de ellas, mientras acompañan la marcha. (Le puede interesar: ["Estigmatización al movimiento estudiantil"](https://archivo.contagioradio.com/estigmatizacion-al-movimiento-estudiantil/))

Asimismo, Marcela Giraldo, vocera de la UNEES Huila, **denuncio que en la Universidad Sur Colombiana, han empezado a viralizarse una serie de **[**memes**], publicados en un perfil de Facebook, en donde instan a golpearla y a dos personas más que han estado participando activamente del movimiento estudiantil, sin que la institución tome medidas efectivas frente a estas violencias en su contra.

### **El Estado debe dar respuestas** 

El próximo 14 de diciembre el movimiento estudiantil tendrá una audiencia pública en la que esperan que participe el Ministerio de Defensa y de respuesta a las múltiples agresiones de las que han sido víctimas las y los estudiantes, al mismo tiempo esperan que se presente el Ministerio de Educación y otros organismos para hablar de la defensa de los derechos humanos. (Le puede interesar: ["119 estudiantes fueron capturados tras la movilización estudiantil en Bogotá"](https://archivo.contagioradio.com/119-estudiantes-fueron-capturadas-tras-la-movilizacion-estudiantil-en-bogota/))

###### [Reciba toda la información de Contagio Radio en] [[su correo][[Contagio Radio]
