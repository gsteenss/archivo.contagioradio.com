Title: Desempleo en Latinoamérica subió en 2016 y continuará aumentando en 2017: OIT
Date: 2016-12-14 18:16
Category: Economía, El mundo, Nacional
Tags: América Latina, desempleo, OIT
Slug: desempleo-latinoamerica-subio-2016-continuara-aumentando-2017-oit
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/desempleo-e1481757362711.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 10 minutos 

###### [14 Dic 2016] 

Este año, **el desempleo en Latinoamérica aumentó 1,5%**, es decir, la cifra más preocupante en los últimos 10 años. De acuerdo  con el más reciente informe de la Organización Internacional del trabajo, OIT, el desempleo llegó a “niveles que no se habían registrado ni siquiera durante la crisis financiera internacional 2008-2009".

‘Panorama Laboral 2016’, es el nombre del reporte de la OIT presentado en Lima, en el que se resalta que “**La tasa de desocupación regional volvió a aumentar**, **esta vez de forma abrupta al pasar del 6,6% en 2015 a 8,1%** (estimado preliminar) en 2016”.

Ese 8,1% de desempleo, indica que en América Latina y el Caribe existen 25 millones de personas sin empleo, es decir **5 millones más que en el 2015.** Además, una de las cosas más graves que manifiesta la OIT, es que la calidad del tipo de empleo ha empeorado ya que los salarios han ido disminuyendo y ha aumentado la informalidad.

Como ha venido sucediendo, **las mujeres y los jóvenes son los que más sufren esta situación económica.** La tasa de desocupación en las mujeres llegó a 9,8% y entre los jóvenes a 18,3%. La tasa de desocupación del 2016, fue un fenómeno más extendido a nivel regional porque creció en 13 de los 19 países.

"Las fuertes recesiones de Brasil y Venezuela afectaron el promedio latinoamericano, a lo que se sumaron las contracciones económicas de Argentina y Ecuador", señala la OIT en su informe en el que además estima que **para el 2017, estas cifras podrán aumentarse nuevamente a un 8.4%.**

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
