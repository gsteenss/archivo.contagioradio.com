Title: Nos dejaron sin casa y sin con qué vivir: Barequeros desalojados de Ituango
Date: 2020-11-13 18:00
Author: AdminContagio
Category: Actualidad
Tags: Briceño, Desalojos, Hidroituango, Ituango
Slug: nos-dejaron-sin-casa-y-sin-con-que-vivir-barequeros-desalojados-de-ituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Desalojo-ESMAD-Ituango-y-Briceno.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Desalojos-ESMAD-Ituango-y-Briceno.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

El **[Movimiento Ríos Vivos](https://twitter.com/RiosVivosColom) denunció los desalojos de cerca de 70 barequeros ancestrales que se encontraban asentados en la rivera del río Cauca en los municipios de Ituango y Briceño por parte del Escuadrón Móvil Antidisturbios de la Policía -ESMAD-**, el cual fue ordenado por la Alcaldía de Ituango y la Gobernación de Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según Isabel Zuleta, vocera del Movimiento, el procedimiento incurrió en varias irregularidades entre ellas, que no se notificó oportunamente a las personas sobre el procedimiento, que se impidió el acceso al abogado que representa a la comunidad y que **la Policía no acató la decisión de un Juez de tutela que ordenó la suspensión provisional de los desalojos.**

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RiosVivosColom/status/1326512346576072704","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RiosVivosColom/status/1326512346576072704

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Según la información que se ha conocido, en medio de los desalojos, **la Policía utilizó bombas aturdidoras para dispersar a las personas,** **lo cual causó zozobra y temor en la comunidad, ya que, en la zona hay sembradas minas antipersona.** (Lea también: [Por mina antipersona muere indígena en Ituango](https://archivo.contagioradio.com/por-mina-antipersona-muere-indigena-en-ituango/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por otra parte, aún no se ha confirmado si la Fuerza Pública utilizó gases lacrimógenos, los cuales fueron prohibidos para ser usados en medio de la pandemia; sin embargo, en algunos videos grabados por los afectados señalaron que sí. (Le puede interesar: [Suspensión de uso de gas lacrimógeno: Un freno más a la represión policial](https://archivo.contagioradio.com/suspension-de-uso-de-gas-lacrimogeno-un-freno-mas-a-la-represion-policial/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RiosVivosColom/status/1326628168346853377","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RiosVivosColom/status/1326628168346853377

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

**Muchas de las familias desalojadas denuncian haber sido expulsadas y despojadas previamente de otros predios por cuenta del proyecto Hidroituango** y la presión que según ellas, ejerció Empresas Públicas de Medellín -EPM- para sacarlos del territorio que habitaron por años. (Lea también: [EPM debe frenar el sufrimiento que ha causado: Comunidades](https://archivo.contagioradio.com/con-epm-o-aceptamos-lo-que-nos-dan-o-nos-quitan-todo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Isabel Zuleta, sostiene que las personas allí asentadas **son personas vulnerables que han perdido todo con la ejecución del proyecto Hidroituango pues este no solo los desterró del sitio en el que vivían, sino que les quitó su fuente de sustento que era el barequeo artesanal y ancestral** que realizaban en las playas de la rivera del rio Cauca.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/RiosVivosColom/status/1326616159777599488","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/RiosVivosColom/status/1326616159777599488

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Entes de control “cómplices” de los desalojos y violaciones de DD.HH.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Isabel Zuleta, señaló que **la Procuraduría y la Defensoría del Pueblo han sido cómplices de la violación de derechos humanos** que ha rodeado al proyecto Hidroituango desde sus inicios. “***Estos entes han estado ahí de manera directa, siendo testigos de la violación de los derechos de las comunidades, sin hacer nada*”**, afirmó la lideresa en entrevista para Contagio Radio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Particularmente, frente al caso de los desalojos, la vocera de Ríos Vivos, afirmó que el procurador delegado que atendió la diligencia simplemente se había atenido a decir que sugería la participación del abogado de las comunidades, **cuando en realidad tenía la potestad de detenerlo por la violación de ese y otros derechos pero no lo hizo.**

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Estos entes no son garantes de nada, ven que se comenten violaciones a los derechos y encima mienten, hablando en nombre de nosotros, diciendo que todos los derechos están garantizados cuando la realidad es otra”
>
> <cite>Isabel Zuleta, vocera del Movimiento Ríos Vivos</cite>

<!-- /wp:quote -->

<!-- wp:block {"ref":78955} /-->
