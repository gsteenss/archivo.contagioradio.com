Title: Sin soluciones concretas, vendrían nuevas calamidades para Mocoa
Date: 2018-08-13 15:51
Category: Ambiente y Sociedad, Nacional
Tags: avalancha, Mocoa, Putumayo, Unidad Nacional de Gestión del Riesgo y Desastre
Slug: sin-soluciones-concretas-para-mocoa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-13-a-las-3.37.42-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @COMANDANTE\_EJC] 

###### [13 Ago 2018] 

Tras las intensas lluvias, el colapso de un puente vehicular y el traslado de más de 20 mil personas a puntos seguros de asistencia, el Director General de la Unidad Nacional de Gestión de Riesgo y Desastre (UNGRD), **Eduardo José González,** declaró la **calamidad pública en Mocoa.** Sin embargo, según habitantes de la zona, desde la avalancha del pasado abril de 2017, se han re ubicado unas 200 familias de las cerca de 600 que resultaron afectadas.

Después de más de 7 horas de fuertes lluvias, el Sistema de Alerta Temprana, que se instaló para prevenir una catástrofe como la ocurrida en abril del año pasado, se activó en la madrugada del 12 de agosto por el incremento del nivel en los ríos Mulato, Sangoyaco y Mocoa, así como en las quebradas Taruca y Tarumita según información de la UNGRD.

Según Luis Ulcué, comunero del pueblo Nasa, **las lluvias que se presentaron el fin de semana fueron más fuertes que las que provocaron la catástrofe**. Sin embargo, los simulacros de preparación que se realizaron en el Municipio sirvieron para que más de 20 mil personas llegarán a los puntos de encuentro y se evitará las perdidas de vidas humanas. Otra de las medidas de prevención efectiva fue que se cerraron los puentes sobre el río, evitando que las personas los transitarán, y en caso de ser destruidos fueran arrastradas por la corriente.

De otro lado, Ulcué afirmó que la muralla que se construyó para evitar otra avalancha, era de arena, razón por la que el agua ha ido destruyendo el dique y terminó inundando nuevamente **5 barrios y 3 veredas de la capital del Putumayo.** Adicionalmente, por está nueva inundación se vieron afectadas las mismas personas que hace un año. (Le puede interesar:["Instituciones nacionales se van de Mocoa sin superar la emergencia"](https://archivo.contagioradio.com/mocoa-emergencia/))

### **Otra vez se están viendo afectadas las mismas personas que el año pasado ** 

El miembro del Pueblo Nasa, manifestó que desde la tragedia de hace un año, las personas no han sido reparadas adecuadamente. Se han marcado casas que deben ser demolidas y se ha evacuado a personas en sitios de riesgo, pero una vez tomadas estas medidas, las personas no tienen a donde ir y deciden regresar a sus viviendas o hacer nuevas construcciones cerca al cause del río, por esta razón, **"otra vez este año se vieron afectadas las mismas personas en Mocoa".**

Según Ulcué, Corpoamazonas no ha adelantado los estudios de riesgo adecuados para hacer el manejo de la situación. Adicionalmente, se ha establecido un Puesto de Mando Unificado (PMU), y pese a que el cubrimiento de la situación "ha sido muy mediático", **no se han tomado las medidas urgentes que se requieren,** como garantizar el suministro de agua potable en la zona. (Le puede interesar: ["Gobierno Nacional se olvidó de los damnificados en Mocoa"](https://archivo.contagioradio.com/gobierno-nacional-se-olvido-de-los-damnificados-en-mocoa/))

Tras la calamidad, el comunero espera la presencia del Presidente de la República en la zona para atender la situación, igualmente, espera que se tome un rumbo concreto para la reconstrucción de Mocoa, pues hasta el momento **se han re ubicado unas 200 familias, número mínimo si se tiene en cuenta que fueron afectadas unas 3200 personas,** provenientes de 17 barrios.

<iframe id="audio_27780216" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_27780216_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [ Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u) [o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por] [Contagio Radio](http://bit.ly/1ICYhVU) 

<div class="permalink-inner permalink-tweet-container">

</div>
