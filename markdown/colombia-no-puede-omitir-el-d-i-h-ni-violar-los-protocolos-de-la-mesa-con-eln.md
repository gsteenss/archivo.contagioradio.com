Title: Colombia no puede omitir el DIH ni violar los protocolos de la mesa con ELN
Date: 2019-01-21 14:15
Author: AdminContagio
Category: DDHH, Política
Tags: Centro Democrático, ELN, guerra, Iván Duque, paz
Slug: colombia-no-puede-omitir-el-d-i-h-ni-violar-los-protocolos-de-la-mesa-con-eln
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ELN Paz] 

###### [21 Ene 2019 ] 

El analista político Fernando Giraldo señaló que el llamado del presidente Iván Duque a pasar por alto el protocolo establecido en la mesa de conversaciones entre el gobierno y el ELN, envía un mal mensaje a la comunidad internacional de un país **que intenta comportarse como un "forajido" pasando por alto el Derecho Internacional Humanitario**  y solo sería una acción para mejorar la imagen del primer mandatario al interior del país.

**Duque está buscando radicalizar la sociedad colombiana para superar sus dificultades de favorabilidad**

Del otro lado, frente a la política interna, Giraldo expresó que los recientes hechos sirven para que sectores autoritarios o proclives a la antidemocracia, que se encuentran en el poder político, se unifiquen en torno a posturas más ortodoxas, situación que se evidencia en la radicalidad del discurso de Iván Duque, **afirmado que le importan muy poco los protocolos y el Derecho Internacional Humanitario.**

"Duque está buscando radicalizar la sociedad colombiana para superar sus dificultades de favorabilidad o los problemas de imagen que tiene en la en la población, medida en las encuestas desde que subió a la presidencia" aseguró Giraldo y agregó que además, esta postura le serviría para generar una sola posición al interior del Centro Democrático, arriconando a los menos radicales.

### **Hay que cumplir lo acordado**

Giraldo señaló que Colombia se acoge a un ordenamiento jurídico internacional, por ende el primer mandatario debe actuar como símbolo de lo que la Nación piensa frente a un problema, e hilar su política interna con la externa. En este caso Duque debe responder ante países que actuaron en el proceso de paz como garantes y otro que se ofrecieron como sede para el desarrollo de los mismos.

"Lo que dice el presidente no solo es violatorio de los tratados internacionales, es violatorio de un acuerdo que tiene el Estado colombiano en la negociación, hasta donde iban con el ELN, con unos países garantes que se abstienen al **Derecho Internacional y a la forma como se manejan las relaciones internacionales entre Estados**" aseveró Giraldo.

Razón por la cual pedir la extradición de los integrantes del ELN, que se encuentran en Cuba, sin respetar los protocolos, es "anular completamente cualquier pudor en la política en el escenario internacional por parte de Colombia", pasar por alto el Derecho Internacional Humanitario, e**"intentar comportarse como un foragido**", acciones que según el profesor recibiran el rechazo de la comunidad internacional.

### **Colombia y su fratricida guerra ** 

De acuerdo con Giraldo, Colombia ha permanecido tanto tiempo en la guerra debido a las profundas desigualdades sociales que existen y a las dificultades por construir una verdadera democracia, que debe posibilitar, por lo menos, una igualdad en los derechos políticos. Hecho que impide la construcción de una paz que supere las inequidades **"y siempre buscando en el otro el total responsable y mal de los problemas sociales".**

Razón por la cual, para el profesor existen sectores que quieren la guerra, apoyados por políticos que "creen que lo que da más réditos es mantener una sociedad desigual e injusta", y así impedir ver las problemáticas reales como la corrupción, la educación, la salud, la inclusión, la justicia, entre otros. (Le puede interesar: ["Colombia debe exigir que el ELN y el Gobierno se vuelvan a sentar a dialogar: Iván Cepeda"](https://archivo.contagioradio.com/sociedad-colombiana-debe-exigir-que-eln-y-gobierno-se-vuelvan-a-sentar-en-la-mesa-ivan-cepeda/))

<iframe id="audio_31650542" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_31650542_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
