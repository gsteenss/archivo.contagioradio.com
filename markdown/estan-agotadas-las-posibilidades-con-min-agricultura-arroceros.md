Title: "Están agotadas las posibilidades con Min. Agricultura": Arroceros
Date: 2017-08-30 12:28
Category: Nacional
Tags: Arroceros, Ministerio de Agricultura
Slug: estan-agotadas-las-posibilidades-con-min-agricultura-arroceros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/arroceros-piden-salvaguardia-arroz-importado_ELFIMA20140108_0002_1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Financiero] 

###### [30 Ago 2017] 

Arroceros lograron que las gobernaciones del Tolima, Meta y Huila, atendieran su llamado para establecer un diálogo con el gobierno Nacional, y se comprometieron a citar un encuentro con el presidente Juan Manuel Santos para proteger las cosechas de arroz, porque consideran que con el Ministerio de Agricultura **“están agotadas las posibilidades”** para evitar que crezca la crisis que actualmente afronta este sector de la agricultura del país.

De acuerdo con Eudoro Álvarez, presidente de la Asociación de arroceros del Meta, las 3 gobernaciones manifestaron que la reunión con el presidente Santos se podría dar el día de mañana, sin embargo, hasta que este encuentro no se dé, se mantendrán en alerta máxima para continuar con el paro. (Le puede interesar:["Las razones del paro de los Arroceros"](https://archivo.contagioradio.com/las-razones-del-paro-de-arroceros-en-colombia/))

El líder social manifestó que la solución a esta problemática pasa por un cambio de modelo económico que beneficie la producción nacional y que sea la misma la que sea consumida por los colombianos, “**se trata de pelear contra un modelo que no le conviene ni a la agricultura ni al país”** y agregó que, en esta medida, la producción agrícola del país abastece la oferta.

Por tal razón han manifestado que una de las posibilidades para solucionar esta problemática tiene que ver con suspender las **próximas importaciones de arroz, que serían alrededor de 64 mil toneladas y que estarían programadas para el 15 de octubre**.

Además, según Álvarez, esta movilización ha servido para recoger la situación que afrontan campesinos que cultivan productos como el plátano o la yuca, y de esta forma tener un panorama más general de la crisis que afronta el agro en Colombia, **“los cultivadores de plátano, yuca, piña y productores de leche manifestaron, manifestaron cosas que a mi juicio son más graves que lo que está sucediendo con el arroz”**.

En ese sentido, Eudoro expresó que los campesinos que se reunieron el día de ayer estaría pensando en conformar una plataforma que agrupe a los diferentes sectores del agro para empezar a realizar acciones conjuntas que frenen las afectaciones de las importaciones y los tratados de Libre Comercio en el país. (Le puede interesar:["¿Cómo ha dejado a Colombia 4 años de TLC con Estados Unidos?"](https://archivo.contagioradio.com/que-le-ha-dejado-a-colombia-4-anos-de-tlc-con-estados-unidos/))

<iframe id="audio_20607641" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20607641_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
