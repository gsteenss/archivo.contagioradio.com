Title: Teodomiro Sotelo, segundo líder asesinado en El Tambo, Cauca en un mes
Date: 2020-04-17 17:36
Author: CtgAdm
Category: Actualidad, DDHH
Tags: Teodomiro Sotelo
Slug: teodomiro-sotelo-segundo-lider-asesinado-en-el-tambo-cauca-en-un-mes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/Teodomiro-Sotelo-Anacona.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: Teodomiro Sotelo/@MAPPOEA

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Este 17 de abril en la vereda Betania de El Tambo, Cauca fue asesinado el líder social Teodomiro Sotelo, quien además de promover procesos de sustitución de cultivos de uso ilícito, pertenecía al Consejo Comunitario Afro Renacer, del Coordinador Nacional Agrario y de Congreso de los Pueblos. Teodomiro se convierte en el segundo líder asesinado en este municipio después del homicidio de Jorge Macana.

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

El asesinato de Tedomiro Sotelo habría ocurrido cuando hombres de un grupo armado llegaron su casa y lo asesinaron frente a su familia y trabajadores del sector de San Juan del Micay, en El Tambo, una región donde hacen presencia **estructuras del ELN** y donde desde hace un tiempo, se ha advertido de la llegada de integrantes de las disidencias de las Farc.  [(Lea también: Líderes caucanos huyen de su tierra amenazados y perseguidos)](https://archivo.contagioradio.com/se-agudiza-conflicto-armado-en-el-cauca/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Esta mismo patrón se ha presentado en Argelia donde desde el sábado 12 de abril en diversos corregimientos y veredas, hombres armados vestidos de camuflado y botas, van de casa en casa de en búsqueda de excombatientes y líderes de sus comunidades. Habitantes del municipio señalan que en medio de la disputa territorial, se está dando un plan en contra de la sustitución. [(Le puede interesar: Puerta a puerta armados persiguen a excombatientes y líderes en Argelia, Cauca)](https://archivo.contagioradio.com/puerta-a-puerta-armados-persiguen-a-excombatientes-y-lideres-en-argelia-cauca/)

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### No hay tregua para el campesinado en Cauca

<!-- /wp:heading -->

<!-- wp:paragraph {"align":"justify"} -->

Durante las más recientes semanas de confinamiento, la violencia contrario a reducirse con motivo del aislamiento, ha incrementado en el departamento del Cauca. El pasado 3 de abril fue asesinado Hamilton Gasca Ortega y sus dos hijos de 10 y 7 años, integrante de la Asociación de Trabajadores Campesinos de Piamonte, organización que promueve la sustitución voluntaria en el departamento y que ha sido blanco de amenazas desde la firma del Acuerdo de Paz. [(Le puede interesar: Masacre en Piamonte, un reflejo de la persecución a campesinos)](https://archivo.contagioradio.com/masacre-en-piamonte-reflejo-persecucion-campesinos/)

<!-- /wp:paragraph -->

<!-- wp:paragraph {"align":"justify"} -->

Cabe recordar que el pasado mes de marzo, en el mismo municipio se denunció el asesinato del líder Jorge Macana en el corregimiento de Playa Rica. Macana apostaba a la sustitución de cultivos de uso ilícito, y hacía parte de la Mesa Departamental encargada de tratar ese tema, además trabajaba en la creación de proyectos productivos, como alternativa los ingresos de recursos distintos a la Coca.[Con el asesinato de Jorge Macana, El Tambo pierde un pilar de su organización social](https://archivo.contagioradio.com/con-el-asesinato-de-jorge-macana-el-tambo-pierde-un-pilar-de-su-organizacion-social/)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Noticia en desarrollo....

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
