Title: Recién nacido muere ahogado buscando atención hospitalaria
Date: 2015-12-16 17:53
Category: Comunidad, Nacional
Tags: atención médica deficiente en Colombia, crisis de la salud, Puerto Merizalde Valle del Cauca
Slug: recien-nacido-muere-ahogado-buscando-atencion-hospitalaria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/baby-feet17.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### 

<iframe src="http://www.ivoox.com/player_ek_9749071_2_1.html?data=mpyhm5WbdY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRlsbXyoqwlYqlfc%2Bfz8bQy8nTb87pxtfSjcbMs8jVxdSYxNrXp8LixdSYw9nJssTdhqigh6eXsozc0Njdy9nFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Familiar del menor fallecido] 

##### 16 Dic 2015 

##### Por: Comunicadores CONPAZ- Río Naya (Buenaventura) 

La madrugada del pasado 10 de diciembre, se reportó la muerte de un recién nacido al caer a las aguas del oceáno pacífico, cuando era trasladado desde el hospital San Agustín, de Puerto Merizalde hacia del municipio de Buenaventura.

Un cuadro de deficiencia respiratoria que no se pudo atender por la falta de equipos necesarios en el centro médico, fue el motivo por el cual se ordenó de urgencia la remisión del infante sin acompañamiento de la madre, debido a las malas condiciones de salud en que se encontraba luego de dar a luz.

De acuerdo con una familiar de la jóven madre, quien es menor de edad, la lancha en que se desplazaban naufragó por la turbulencia de las aguas, cayendo todos sus tripulantes al agua, incluída la persona que llevaba en sus brazos al menor, que fue arrastrado por las aguas y se asume falleció ahogado.

Las deficiencias en la atención, sumadas a la distancia de 2 horas entre las instituciones de salud y a la ausencia de transporte del hospital en el momento de la emergencia, dan cuenta de las múltiples fallas en la prestación de servicios de salud. La madre del menor debió ser internada de nuevo tras el fuerte impacto ocasionado por la perdida de su hijo.
