Title: "Por el derecho a ser joven" nace Juventud Rebelde Colombia
Date: 2015-04-06 16:42
Author: CtgAdm
Category: Nacional, Resistencias
Tags: Alcoholiricos, Escartel, feminista, joven, jovenes, juventud rebelde, objetor, paz, Real Academia del Sonido, Red Nois, Reincidentes
Slug: por-el-derecho-a-ser-joven-nace-juventud-rebelde-colombia
Status: published

###### Foto: Juventud Rebelde Colombia 

<iframe src="http://www.ivoox.com/player_ek_4313154_2_1.html?data=lZielZaZeI6ZmKiak5WJd6Kmk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmsdTfjcrQb8XZ08rQytSPpYznxteYzNTaqc%2BZk5eY0MbHqYy%2B1tvS0NnZqIzGxsfSzsnJb6TjzdSah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Cristian Hurtado, Juventud Rebelde] 

Del 7 al 9 de abril, más de 10 mil jóvenes de toda Colombia se darán cita en Bogotá para constituir la organización **Juventud Rebelde Colombia**, un iniciativa que busca "juntar las rebeldías" para la movilización social por la paz.

Según anuncia Cristian Hurtado, del Comité Nacional de Impulso de la Juventud Rebelde, este proceso viene gestándose desde el año 2013 en 29 departamentos del país. "La idea es formalizar la constitución de este acumulado", indica Cristian.

**Jóvenes campesinos y campesinas, de los barrios, jóvenes artistas, científicos, feministas, objetores de conciencia**, buscarán encontrar las coincidencias de sus luchas y reivindicaciones, no para apartarse de otros sectores sociales, sino para reconocerse como actores sociales y políticos que hace parte del campo social colombiano.

Para las y los jóvenes Rebeldes, a través de la lucha por "el derecho a ser joven" pueden potenciarse dinámicas juveniles, y encontrarse caminos para el fin del conflicto social y armado en Colombia.

Durante los 4 días del Congreso Constitutivo, se desarrollarán 8 páneles de discusión como **"Por trabajo Dignificante: Jóvenes y Situación Laboral", "Rebeldía en la Cama y la Barricada: Mujeres, Género y Disidencias Sexuales"** y "Por Un Nuevo Sentido Común: Viejo Orden ante la Cultura, Contracultura y Resistencia.", entre otros.

El cierre del Congreso tendrá como escenario un concierto en el Parque Simón Bolívar, en el escenario de El Lago, que contará con la participación de **La real academia del sonido, Reincidentes, Red Nois, Alcoholiricos y Escartel**. Los más de 10 mil jóvenes de Juventud Rebelde participarán además en la movilización del 9 de abril en Bogotá para apoyar el proceso de paz y exigir cese bilateral al fuego, según señala Hurtado, porque "nuestro lanzamiento obedece al sitio del que venimos, y venimos de las acciones colectivas en la calle, la movilización y la marcha".
