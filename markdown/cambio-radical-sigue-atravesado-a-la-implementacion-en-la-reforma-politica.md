Title: Cambio Radical sigue “atravesado” a la implementación en la reforma política
Date: 2017-10-13 16:01
Category: Nacional, Política
Tags: Cámara de Representantes, Reforma Política
Slug: cambio-radical-sigue-atravesado-a-la-implementacion-en-la-reforma-politica
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/debate-congreso.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Rios vivos] 

###### [13 Oct 2017]

Luego del bochornoso acto en el que el presidente de la Cámara Rodrigo Lara se retirara de una audiencia pública en la que participaba Jesús Santrich, integrante de las FARC, y de la carta en que cerca de 40 integrantes de la **Cámara de Representantes piden que Lara se aparte de las discusiones del acuerdo de paz** en esa corporación, pues lo que está haciendo torpedea la implementación del acuerdo y en concreto de la **reforma política.**

Así lo han manifestado en reiteradas ocasiones tanto el representante a la Cámara, Alirio Uribe, como Germán Navas, ambos integrantes del Polo Democrático. Sin embargo, **no solamente Cambio Radical o el Centro Democrático se han opuesto sino que también el ausentismo típico de representantes del Partido Liberal, del Partido Conservador y de la U** han cobrado factura al trámite de la reforma política.

Aunque la propuesta ha sido recortada en algunos aspectos esenciales continúa la oposición, sin embargo partidos como el Polo Democrático y el Partido Verde siguen insistiendo en la necesidad de aprobarla para que en las elecciones de 2018 se cumpla el objetivo de “ampliación de la democracia”.

### **¿Qué planteaba la reforma política?** 

La reforma planteaba la posibilidad de que nadie pudiera ser elegido por más de dos periodos consecutivos, bajar la edad para postularse, generando mayor inclusión de los jóvenes, garantías para la participación de las mujeres, garantías para partidos minoritarios, voto electrónico, financiación estatal de las campañas, prohibición de dádivas o regalos en campaña, voto electrónico y reforma al Consejo Nacional Electoral entre otras, sin embargo muchos de esos puntos han sido recortados.

### **¿Qué se puede hacer?** 

Aunque el tiempo del Fast Track se está acabando, quedan posibilidades como la que el gobierno haga un llamado a los partidos que quedan en la Unidad Nacional y que la ciudadanía se movilice de manera contundente para empujar las reformas, aseguran varios representantes. Además el calendario de debates también se vería afectado por el calendario electoral, una situación que requiere de medidas urgentes.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
