Title: El Neoliberalismo del coronavirus.
Date: 2020-03-19 09:48
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: Coronavirus, Donald Trump Estados Unidos, neoliberalismo, pandemia
Slug: el-neoliberalismo-del-coronavirus
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/coronavirus.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"customTextColor":"#4f5c66"} -->

###### Foto tomada de Pagina 12 argentina 

<!-- /wp:heading -->

<!-- wp:heading {"level":6} -->

###### Por: Itayosara Rojas

<!-- /wp:heading -->

<!-- wp:paragraph -->

El geógrafo radical **David Harvey** asegura que el proyecto neoliberal es un proyecto antidemocrático. La idea de que unos pocos poderosos, junto con otros tecnócratas decidan y tomen acciones sobre asuntos que afectan a la mayoría de la población debería ser razón más que suficiente para oponerse, movilizarse y organizarse contra un proyecto económico excluyente. su argumento sigue vigente, Y en la actualidad podemos ver las implicaciones de un proyecto altamente antidemocrático a través de las consecuencias y desarrollos de la pandemia del coronavirus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los países con un Estado de bienestar medianamente consolidado intentan proteger sus sistemas de salud, evitando una sobrecarga de estos tomando medidas para evitar la propagación; pero además han destinado una línea de créditos especial para esta ocasión que le permita a las empresas y pequeños negocios continuar con su funcionamiento con parte de sus trabajadores en casa (homeworking), como es el caso de Alemania.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

#####  En otros países de la Unión Europea las consecuencias sociales y económicas del brote son mayores.

<!-- /wp:heading -->

<!-- wp:paragraph -->

España afronta en Madrid una crisis en su sistema de salud y las secuelas de la crisis del 2008 siguen latentes, la economía ha sido profundamente afectada y esta semana se presentaron los primeros despidos masivos. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En el Reino Unido tras el Brexit y con la alianza Trump-Johnson el gobierno británico ha decidido no tomar ninguna media para evitar la propagación del virus, por el contrario, está interesado en que la economía siga funcionando con normalidad y prácticamente ha abandonado a sus ciudadanos más vulnerables ante el virus. De entrada, el primer ministro Boris Johnson ha renunciado a evitar la propagación del virus. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Estados Unidos por su parte, no ha tomado las mismas medidas drásticas en su sistema de salud que tomó en términos económicos, tal y como sucedió con la cancelación de vuelos provenientes de Europa. Medida que buscaba principalmente sabotear las economías de los países de la Unión.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los gobiernos Johnson y de Trump fueron electos bajo consignas y banderas que supuestamente los separaban del **establishment**, sin embargo, con sus últimas medidas han dejado más que claro que son los principales defensores del establishment. No han cumplido hasta ahora sus promesas a los ciudadanos, por el contrario, los han dejado a su suerte ante la propagación del virus, especialmente a los ciudadanos mayores y con enfermedades preexistentes. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El proyecto neoliberal que hoy conocemos además de ser criticado por su antidemocracia debe ser criticado por su profundo y completo desprecio hacía la vida, las vidas humanas, la vida de la naturaleza, la vida de otros seres en el planeta que habitamos.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Parece inverosímil que los líderes del mundo hayan renunciado a tomar medidas serias para proteger la salud y la vida de sus ciudadanos. Las consecuencias de este brote a penas las empezamos a conocer, la perdida de empleos, de hogares, la imposibilidad de acceder al sistema salud, pero por sobre todo las consecuencias del rescate del sector privado por parte del Estado lo veremos en los próximos meses. Parece ser el caldo de cultivo perfecto para proyectos ultraconservadores, autoritarios y fascistas. Por lo tanto, inclinar la balanza hacia el lado más democrático depende de las fuerzas sociales que defienden la vida, de su radicalidad, sus exigencias y propuestas. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El quehacer no está claro sobre todo en contextos como el británico donde el **proyecto Corbyn** fue deliberadamente saboteado y derrotado, las elecciones en EE. UU. serán un pulso relevante entre un proyecto neoliberal excluyente en manos de Trump y Biden y una alternativa Sanders. Sí algo positivo puede dejar esta crisis globalizada y extendida es que por lo menos dejemos de elegir a quienes nos trajeron a este punto en el que se paga con la propia vida la irracionalidad de un sistema de acumulación y producción como el que tenemos.  El neoliberalismo como dice Harvey no solamente es un proyecto antidemocrático, es un proyecto de muerte y sus tecnócratas y defensores ni se inmutan al pensarlo, entre la vida y la acumulación, siempre elegirán la acumulación.  

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver mas: [otras columnas de opinión](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
