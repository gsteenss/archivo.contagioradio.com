Title: Gina Parody no le responde a 300 etnoeducadores afrocolombianos
Date: 2015-07-14 11:54
Category: Comunidad, Nacional
Tags: afro, afroamericano, afrocolombiano, buenaventura, educacion, etno, etnoeducacion, gina parody, mesa, nariño
Slug: gina-parody-no-le-responde-a-300-etnoeducadores-afrocolombianos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/etnoeducación.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: HechoEnCali 

   
<iframe src="http://www.ivoox.com/player_ek_4801759_2_1.html?data=lZ2dk5yZfY6ZmKiak5eJd6KlmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkcriytjhx9fNs4zYxpCyxtrHpcTdhqigh6eXsozYxtjQ0dPTp8afwpDS1tPTqcXpxMbR0dfJt4zYxtGah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Marín Antonio Quiñonez, Mesa Departamental de Etnoeducación de Nariño] 

 

###### [14 jul 2015] 

Al rededor de 300 maestros afrocolombianos, etnoeducadores del departamento de Nariño, se encuentran hace 3 días en Bogotá realizando una toma pacífica de la Iglesia de San Francisco, a la espera de que la Ministra de Educación, Gina Parody, atienda sus demandas laborales.

Según denuncian los maestros, el Ministerio de Educación Nacional aprobó un decreto mediante el cual realiza el concurso docente para etnoeducadores saltándose el proceso de consulta previa, e ignorando las especificidades de las comunidades afrocolombianas.

Es por esto que en año 2013 los maestros afrocolombianos realizaron un proceso de resistencia civil, negándose a participar en el concurso docente. El Ministerio de Educación ha negado la solicitud de revisar el proceso, y actualmente adelanta la contratación de maestros y maestras para el territorio pacífico colombiano.

\[caption id="attachment\_11213" align="alignright" width="220"\][![Etnoeducadores se toman iglesia en Bogotá](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/Captura-de-pantalla-2015-07-14-a-las-11.45.52-150x150.png){.wp-image-11213 width="220" height="220"}](https://archivo.contagioradio.com/gina-parody-no-le-responde-a-300-etnoeducadores-afrocolombianos/captura-de-pantalla-2015-07-14-a-las-11-45-52/) Etnoeducadores se toman iglesia en Bogotá\[/caption\]

"Si el proceso continúa va a generar conflictos en el territorio", afirma Marín Antonio Quiñonez, Representante de la Mesa Departamental de Etnoeducación de Nariño, e insiste en que se revise el decreto y se garantice un proceso acorde a los derechos de los consejos comunitarios de los 10 municipios de Nariño y del área urbana y rural de Buenaventura.

"Estamos en Bogotá, señora Ministra. No tiene que ir al pacífico. Sentémonos a hablar", dicen los maestros, quienes afirman que "el pacífico colombiano es la zona más discriminada por parte del gobierno nacional, y donde hay un alto indice de necesidades básicas insatisfechas, porque no hay unas políticas publicas serias para el etno-desarrollo de la región".
