Title: Emergencia por COVID llegó al INPEC y tampoco hay protocolo
Date: 2020-04-16 09:16
Author: CtgAdm
Category: Actualidad, DDHH
Tags: carcel, covid19, INPEC, Villavicencio
Slug: emergencia-por-covid-llego-al-inpec-y-tampoco-hay-protocolo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/Inpec.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

De acuerdo con el dragoneante Jhon Alracón, presidente del sindicato SINTRAPECUN del INPEC, no existen un plan para la mitigación de la pandemia del Covid 19 en la cárcel de Villaviciencio ni para personas privadas de la Libertad (PPL) o para las personas que integran la guardia. Hecho que se da tras el reporte de contagio de **15 personas, entre reclusos y guardia**s.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la denuncia del guardia, la EPS y la Secretaría Departamental **no han practicado las correspondientes pruebas** a los administrativos y el cuerpo de custodia. (Le puede interesar: ["Decreto presidencial no resuelve hacinamiento ni emergencia por COVID"](https://archivo.contagioradio.com/decreto-presidencial-no-resuelve-hacinamiento-ni-emergencia-por-covid/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo, señala que la Aseguradora de Riesgos Laborales, Positiva no se ha pronunciado para **presentar el plan de contingencia**. También asegura, que los pocos elementos que han conseguido, como tapabocas o batas, fueron reunidos por los mismos funcionarios del centro penitenciario.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además, el cuerpo de custodia asevera que la cárcel solo cuenta con una enfermera y un auxiliar durante el día, para 1.600 reclusos. Suceso que evidencia que "la unidad de servicios penitenciario y carcelarios no planeo o tiene un plan de contigencia" frente a la población vulnerable.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En ese sentido, la guardia del INPEC de la cárcel de Villavicencio está solicitando la intervención de la comisión de derechos humanos del Senado, para garantizar y **salvaguardar la vida de las personas que se encuentran allí.**

<!-- /wp:paragraph -->

<!-- wp:heading -->

Director del INPEC amedranta guardias
-------------------------------------

<!-- /wp:heading -->

<!-- wp:paragraph -->

El dragoneante también pide que esa comisión que investigue la persecución en contra de guardias que denuncian la falta de medidas dentro de las cárceles. (Le puede interesar: «[Todos tenemos derechos, ellos también-2](https://www.justiciaypazcolombia.com/todos-tenemos-derechos-ellos-tambien-2/)«)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Situación que se da luego de que el penalista Miguel Ángel del Río expresara que el director del INPEC, el general Mojica, habría ordenado a los directores regionales denunciar penalmente a **cualquier persona que haga una manifestación** en referencia al manejo de la pandemia en las cárceles.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por este hecho ya habría en curso **una queja disciplinaria en contra del Brigadier General Mojica**, realizada por los distintos sindicatos de trabajadores del INPEC.

<!-- /wp:paragraph -->
