Title: "Aquí no se han desmontado las estructuras paramilitares" Aida Avella
Date: 2016-12-01 13:43
Category: Paz, Política
Tags: Aida Avella, amenazas paramilitares, asesinato defensores de derechos humanos
Slug: aqui-no-se-han-desmontado-las-estructuras-paramilitares-aida-avella
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/aida_avella_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Colprensa] 

###### [1 Dic. 2016] 

La presidenta nacional de la Unión Patriótica, Aida Avella, fue demandada por el partido Centro Democrático por el delito de presunta injuria agravada. Sin embargo  Avella ha asegurado que considera que en este momento una denuncia no es tan importante comparada a las **alarmantes situaciones que se vienen presentando en contra del movimiento social en el país.**

Aida Avella afirmó para los micrófonos de contagio radio que lo que más le preocupa son “las matanzas de los campesinos en este país” y la poca atención que el Estado le está dando a esta problemática. **“Yo creo que vamos a tener que acostumbrarnos que  la vida de un campesino es tan importante como la de un ministro”. **Le puede interesar: ["No les vamos a dar el gusto de desfallecer: Marcha Patriótica" ](https://archivo.contagioradio.com/cuanto-mas-podra-resistir-marcha-patriotica/)

Y es que según Avella, en las últimas horas se dio a conocer el homicidio de **Jorge Ramírez Guzmán**, campesino que habitaba en la vereda Santa Rosa del Arcial, en el corregimiento de Piñalito en el departamento de Córdoba, los hechos se habrían presentado el pasado  27 de noviembre cuando Ramírez se dirigía a revisar el acueducto comunal. Hasta el momento no se conoce como se dio la acción violenta.

Estos actos de violencia en contra de defensores de derechos humanos, dirigentes y líderes del movimiento social y sindicalistas tiene detrás a las mismas personas que intentaron exterminar al partido político Unión Patriótica, “**Por qué hay un plan grande contra la Marcha Patriótica, eso quiere decir que hay autores intelectuales y hay financieros**, y como hay esa clase de cosas el Estado no puede dejar de castigar este tipo de cosas”.

De igual forma Aida Avella expresó que le ha pedido al Físcal General de la Nación que revele la lista de las personas que financiaron en Antioquía el genocidio de la Unión Patriótica. Además afirmó que la próxima semana **se llevará a cabo una reunión con la Corte Interamericana de Derechos Humanos para exponer estos casos sistemáticos.**

**“Yo creo que no se está iniciado otro genocidio es que se continúa el genocidio** contra los sectores de oposición, es que aquí no se han desmontado las estructuras paramilitares que están completamente intactas” Afirmó la dirigente social y una de las sobrevivientes del exterminio a la Unión Patriótica.

###### Reciba toda la información de Contagio Radio en [[su correo]
