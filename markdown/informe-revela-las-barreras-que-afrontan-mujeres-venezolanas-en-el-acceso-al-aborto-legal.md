Title: Informe revela las barreras que afrontan mujeres venezolanas en el acceso al aborto legal
Date: 2019-12-04 16:27
Author: CtgAdm
Category: Entrevistas, Mujer
Tags: aborto, Aborto Legal en Colombia, migrantes, mujer, Venezuela
Slug: informe-revela-las-barreras-que-afrontan-mujeres-venezolanas-en-el-acceso-al-aborto-legal
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/05/IMG_7900.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] Christina Noriega 

Este 3 de diciembre La Mesa por la Vida y la Salud de las Mujeres, en alianza con  la Fundación Oriéntame, la Corporación Mujer, Denuncia y Muévete, y Médicos del Mundo-Francia, presentaron el informe ***M******igrantes venezolanas en Colombia: barreras de acceso a la Interrupción Voluntaria del Embarazo (IVE);*** un documento que cuenta las diferentes barreras a las que se enfrentas las mujeres venezolanas que pretendan practicarse un aborto legal y seguro en el país.

Este informe se baso en 21 casos de mujeres migrantes a las que La Mesa hizo acompañamiento desde noviembre del 2018 a septiembre del 2019, según Carolina Triviño, abogada de La Mesa por la Vida y la Salud de las Mujeres, "las situación de las mujeres venezolanas se recrudece debido a su condición de migrantes, y **en muchos casos las instituciones prestadoras de salud publica, les niegan el servicio de IVE  o les solicitan requisitos adicionales, que muchas veces se convierten en un gran obstáculo para ellas**".

En el informe se evidencian muchas de estas barreras,  una es el desconocimiento por parte de las mujeres migrantes especialmente aquellas que se encuentran en estados precarios,"en Colombia el aborto legal y seguro es un derecho fundamental bajo los tres parámetros señalados por la Corte Constitucional en la sentencia C355 de 2006, **el desconocimiento genera que las mujeres no acudan a la red hospitalaria por miedo a la criminalización o la deportación**", señaló Triviño.

Por otro lado el informe resalta el papel de los centros de salud, los cuales solicitan  a las mujeres el permiso especial de permanencia que respalde su  legalidad en el país,  "**la Corte y el Ministerio de Salud, han señalado que la población migrante debe ser atendida por urgencia, incluyendo en los servicios de salud relacionados con el embarazo**, como la IVE, que deben ser atendida de manera prioritaria y urgente" afirmó Triviño. (Le puede interesar: [Mujeres imparables, 20 años de lucha por la despenalización del aborto en Colombia](https://archivo.contagioradio.com/mujeres-imparables-20-anos-de-lucha-por-la-despenalizacion-del-aborto-en-colombia/))

Así mismo el informe destaca que las migrantes que han logrado acceder a este servicio, **han sido victimas de malos tratos, de violencia obstetricia, y en algunos casos les han  negado el suministro de medicamentos para el dolor**. De las 21 mujeres que contactó La Mesa, 18 permaneces de manera irregular en el país, 3 cuentan con permiso de permanencia, 11 tienen entre 19 y 25 años, 5 entre 26 y 35 años, 2 entre 14 y 18 años, y 3  no reportaron su edad.

De igual forman el documento señala que solo el 19% puedo lograr la Interrupción Voluntario del Embarazo (IVE)  por medio de la red publica, mientras que el 47,6 % se han financiado con subsidio de terceros, mientras que **el 19% restante desistió debido a las barreras para acceder a este método, y especialmente por superar las edades gestacionales permitidas en la ley  por estar en medio de tramites largos y dispendiosos.**

"La idea es que las mujeres sepan que no están solas, que hay organizaciones que prestan el  acompañamiento y asesoría legal, que pueden solicitar en el 3202733179 donde encontrarán una compañía permanente", agregó Triviño; y destacó que el objetivo de este informe también es **hacer un llamado al Gobierno para avanzar frente a las políticas en materia de derechos sexuales y reproductivos de las mujeres,** sin importar su estado migratorio. (Le puede interesar:[Mujeres en Colombia se suman para exigir aborto legal y seguro](https://archivo.contagioradio.com/mujeres-en-colombia-exigir-aborto-legar-y-seguro/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
