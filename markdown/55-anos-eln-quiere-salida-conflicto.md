Title: Tras 55 años en armas, ELN afirma que quiere una salida negociada al conflicto
Date: 2019-07-04 12:39
Author: CtgAdm
Category: Paz, Política
Tags: Comunidad Internacional, ELN, Iván Duque, paz
Slug: 55-anos-eln-quiere-salida-conflicto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/55-AÑOS-DEL-ELN.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ELN  
] 

Este 4 de julio se cumplen 55 años de la primera marcha del Ejército de Liberación Nacional (ELN); momento en que **este grupo ha declarado su intención de jugársela por la paz y no por la violencia.** Según el analista político Carlos Medina Gallego, luego de más de medio siglo en armas, el ELN está decidido a trabajar por que la sociedad colombiana transite hacia una paz completa. (Le puede interesar: ["ELN depositó acuerdos parciales de paz ante la comunidad internacional"](https://archivo.contagioradio.com/eln-acuerdos-comunidad-internacional/))

### **Se cumplen 55 años del ELN, y podrían cumplir otros 50 años**

> Este 4 de Julio de 2019 se cumplen 55 años de haber iniciado el ELN la lucha armada y seguramente podrán pasar otro medio siglo y podrán seguir en esa lucha, pero este país a pesar de su actual gobierno está necesitando de una paz completa y verdadera.
>
> — Carlos Medina G (@CarlosMedinaG1) [3 de julio de 2019](https://twitter.com/CarlosMedinaG1/status/1146528245602574338?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
El Profesor señaló que en estas cinco decadas y media, el ELN ha ido creciendo y es, tal vez, la única organización armada que ha desarrollado una guerra de más de 50 años y que ha colonizado una gran parte del territorio colombiano, llegando a estar en la región Pacífica, ocupando partes de la región Caribe, Amazónica, Andina y de la Orinoquía. Además, es una organización que con sus altibajos, periodos de crisis, recomposición y cambios de estrategia ha logrado sobrevivir pese "a las ofensivas militares y a las estrategias del Gobierno para aniquilarlos".

Así, este grupo conmemora 55 años desde la primera marcha que realizó un 4 de julio de 1964; y como señaló en su cuenta de Twitter, otros 50 años podrían pasar en la lucha armada, pero reconocen que **Colombia en este momento, "a pesar de su actual Gobierno, está necesitando de una paz completa y verdadera".** Ello se evidencia en la consignación de los avances logrados con el gobierno Juan Manuel Santos ante organismos internacionales como la ONU y el Comité Internacional de la Cruz Roja (CICR).

### **El ELN quiere la paz, pero el Gobierno...**

A propósito de este hecho, Medina afirmó que **"todo lo que se puede hacer frente a la comunidad internacional y todo lo que ésta pueda hacer para retomar las conversaciones de paz es útil, necesario y urgente"**. En ese sentido, dijo que la posibilidad de contar con el apoyo de iglesias, la comunidad nacional e internacional es importante para que el Gobierno piense en retomar las conversaciones; pero la disposición tiene resonancia si el gobierno de Iván Duque toma la decisión de retomar las conversaciones.

En esa medida, el Analista recordó que el Gobierno ha sido renuente a reanudar las conversaciones al poner "líneas rojas" o prerrequisitos que dificultan avanzar en un proceso de paz. Razón por la que pidió que el Presidente entienda la necesidad de acabar con el conflicto y con los brotes de violencia en el país, "porque nos damos cuenta que se acaba el conflicto pero se escala la violencia", y muestra de ellos son los más de 500 líderes sociales asesinados desde la firma del Acuerdo de Paz".

### **ELN pide retomar el camino de los diálogos sobre lo acumulado durante las conversaciones de paz**

El Profesor expuso los tres puntos que serían claves para reiniciar las conversaciones de paz con el ELN entre los que se encuentran retomar el trabajo que se adelantó con el gobierno Santos, es decir, "que no se eche por la borda lo alcanzado"; que se retome la experiencia del cese al fuego bilateral, así como la agenda de temas planteados para la discusión; y que se definan con claridad las rutas de participación de la sociedad civil. (Le puede interesar: ["Anvances de diálogos con el ELN deberán ser consignados EN la ONU"](https://archivo.contagioradio.com/avances-de-dialogos-con-el-eln-deberan-ser-consignados-en-la-onu/))

En resúmen, **"transitar el camino de los diálogos sobre lo acumulado en ocasiones anteriores"** es lo que pide este grupo, al tiempo que desescalar el conflicto, en aras de evitar que las poblaciones sufran; así como implementar medidas para detener la guerra en general. En conclusión, Medina Gallego sentenció que el aniversario por los 55 años de nacimiento del ELN es una oportunidad importante para llamar la atención sobre el proceso de paz con este grupo, "si se quiere tener una paz completa, y dar cumplimiento al mandato constitucional que dictamina que la paz es un derecho y un deber de obligatorio cumplimiento".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38021934" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38021934_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
