Title: Renuncia el fiscal general de la nación, Néstor Humberto Martínez
Date: 2019-05-15 13:08
Author: CtgAdm
Category: Nacional, Política
Tags: Fiscal, Fiscalía, Néstoy Humberto Martínez, Renuncia
Slug: renuncia-el-fiscal-general-de-la-nacion-nestor-humberto-martinez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/nestor.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las Dos Orillas] 

Este miércoles 15 de mayo en medio de una rueda de prensa el fiscal general de la nación, Néstor Humberto Martínez presentó su renuncia irrevocable por un supuesto "estado de cosas antijurídicas" que han sido advertidas por la Institución que él preside, y no han sido atendidas.

> <https://t.co/IMQwv85W38>
>
> — Fiscalía Colombia (@FiscaliaCol) [15 de mayo de 2019](https://twitter.com/FiscaliaCol/status/1128720570873524224?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
El anuncio se da luego de conocerse que la Jurisdicción Especial para la Paz (JEP) decidió amparar con al garantía de no extradición a Zeuxis Pausias Hernández Solarte, más conocido como Jesús Santrich; y la ratificación de que Hernán Darío Velásquez Saldarriaga, conocido como El Paisa, tendrá que ser presentado ante ese tribunal. (Le puede interesar: ["JEP niega extradición de Jesús Santrich y ordena que sea liberado"](https://archivo.contagioradio.com/jep-niega-extradicion-de-jesus-santrich-y-ordena-que-sea-liberado/))

Desde que fue elegido Fiscal son varios los escándalos que involucran a Néstor Humberto Martínez con acciones que tendrían que ser objeto de investigación; entre ellas su relación con Odebrecht cuando era ministro para la presidencia durante el gobierno de Juan Manuel Santos, la dudosa adquisición de bienes en España, la muerte de testigos en el caso Odebrecht mediante la ingesta de cianuro, la desatención a los asesinatos recurrentes de líderes sociales y los ataques al Acuerdo de Paz.

### **¿Por qué se genera la renuncia de Martínez Neira?**

> “Exhorto a la ciudadanía a movilizarse con determinación por restablecimiento de legalidad y defensa de la paz, en marco de justicia especial que exhale confianza para todos los colombianos y no para unos pocos y, particularmente, para las víctimas del conflicto”: Fiscal General [pic.twitter.com/PzGv7Kga4B](https://t.co/PzGv7Kga4B)
>
> — Fiscalía Colombia (@FiscaliaCol) [15 de mayo de 2019](https://twitter.com/FiscaliaCol/status/1128728076391780352?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Según analistas, del discurso del Fiscal, resulta preocupante el llamado a movilizarse para restablecer la legalidad, alegando un estado de cosas antijurídicas; además aprovecho el pronunciamiento de la JEP sobre Santrich para victimizarse y descalificar el Acuerdo de Paz, al tiempo que al Sistema Integral de Verdad, Justicia, Reparación y No Repetición.

Otra de las preocupaciones expresadas por diferentes sectores es que se inicie el llamado a una constituyente que intente cambiar el ordenamiento jurídico del país, modificando aspectos fundamentales del Acuerdo de Paz; esto mediante un argumento que antepone la moralidad de Martínez Neira contra la criminalidad supuestamente cubierta por la JEP.

### **Renunció también la vicefiscal Maria Paulina Riveros**

Tras conocerse la renuncia de Néstor Humberto Martínez, la vicefiscal Maria Paulina Riveros era quien debía asumir el lugar dejado por su superior; sin embargo, Riveros también opto por apartarse del cargo.

 

En desarrollo.

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
