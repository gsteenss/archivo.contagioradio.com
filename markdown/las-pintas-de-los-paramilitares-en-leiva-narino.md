Title: "De nuevo a las armas" graffitis alusivos a las autodefensas en Nariño
Date: 2017-07-04 10:24
Category: DDHH, Nacional
Tags: Amenazas, Autodefensas Gaitanistas
Slug: las-pintas-de-los-paramilitares-en-leiva-narino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/autodefensas-gaitanistas-2.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Fotos: COCCAM] 

###### [4 Jul 2017] 

Durante el fin de semana las paredes y algunos sitios públicos de las **comunidades de Leiva en Nariño amanecieron con pintas o grafitis alusivas al grupo autodenominado "Autodefensas gaitanistas de Colombia"** los mensajes fueron encontrados en el corregimiento de Santa Lucia y en la cabecera municipal de Leiva.

"De nuevo a las armas por la paz de Colombia" es el mensaje que aparece en la mayoría de los escritos. Según la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana COCCAM, Nariño estos mensajes han generado terror en la población y pone en riesgo a los líderes sociales. Ver: [Cerca de 50 paramilitares se toman la cabecera municipal en Bagre, Antioquia ](https://archivo.contagioradio.com/cerca-de-50-paramilitares-se-toman-la-cabecera-municipal-en-bagre-antioquia/)

\

 
