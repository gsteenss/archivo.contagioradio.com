Title: Estos son los acuerdos a los que llego la ACA con RCN y Caracol
Date: 2017-04-20 17:07
Category: DDHH, Nacional
Tags: Asociación Colombiana de Actores
Slug: estos-son-los-acuerdos-a-los-que-llego-la-aca-con-rcn-y-caracol
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/aca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ACA] 

###### [20 Abr 2017] 

La Asociación Colombiana de Actores dio a conocer los acuerdos a los que llegó con los canales RCN y Caracol, **para garantizar los derechos laborales de los artistas y de esta forma mejorar las condiciones en términos de contratación**, afiliación a salud y establecer horarios para las jornadas de trabajo.

Los acuerdos a los que llegaron con el canal RCN en términos **de remuneración, es que se pagará al inicio de las grabaciones el 50% y el otro 50% restante al finaliza**r. Las jornadas de grabación serán de 12 horas diarias, máximo, en las que habrá una hora para el almuerzo, las condiciones de logística también se adecuaran a las necesidades de los artistas. **Comparado a las condiciones anteriores en donde no había claridad en los horarios.**

También se comprometieron a no llamar a los actores a realizar grabaciones o viajes, en días festivos como los días domingos; jueves, viernes y sábado de semana santa; 1 de enero; 1 de mayo y 25 de diciembre, además se garantiza el descanso de los artistas los días domingos, **a menos que acuerden previamente con los delegados de la ACA un cambio para este día**.

Con el canal Caracol los artistas, de igual forma pactaron jornadas laborales de 12 horas máximo que no superen las 72 horas semanales y se comprometió a dar a conocer el calendario de grabaciones con los delegados de **ACA para poder presentar comentarios y ajustarlo en conjunto**. Le puede interesar: ["ACA decimos sí: una apuesta de la Asociación Colombiana de Artistas por la paz"](https://archivo.contagioradio.com/aca-decimos-si-una-apuesta-de-la-asociacion-colombiana-de-actores-por-la-paz/)

En caso de existan escenas que requieran paramédicos el **canal se compromete a tenerlos presentes y no arriesgar la seguridad de los artistas** y además pondrá en disposición de sus canales un book de sus afiliados para otras eventuales contrataciones.

De acuerdo con ACA, **“han logrado acuerdos históricos para el gremio actoral del país”** y expresaron que continúan en constante comunicación con las directivas de ambos canales para hacer seguimiento a los acuerdos. Le puede interesar: ["Reconocer los derechos de los actores y actrices es una tarea básica"](https://archivo.contagioradio.com/reconocer-los-derechos-de-los-actores-y-actrices-es-una-tarea-basica/)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
