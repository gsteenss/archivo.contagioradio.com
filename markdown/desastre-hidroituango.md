Title: Desastre en Hidroituango: ¿Y ahora qué?
Date: 2019-02-20 14:46
Author: AdminContagio
Category: Voces de la Tierra
Tags: emp, Hidroeléctricas, Hidroituango
Slug: desastre-hidroituango
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2018-09-27-a-las-12.23.04-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: @ISAZULETA 

###### 14 Feb 2019 

¿Entrará en funcionamiento?, ¿debe desmantelarse? ¿alguien responderá por las afectaciones?; son sólo algunas de las inquietudes que surgen tras la tragedia provocada por el proyecto hidroeléctrico Hidroituango, para las comunidades y el ambiente en el Bajo Cauca Antioqueño.

En este programa de Voces de la tierra, nos acompañaron el profesor **Modesto Portilla**, Geólogo de la Universidad Industrial de Santander, **Oscar Puerta,** Ingeniero Civil y Magister en Gestion Ambiental con estudios en Recursos Hidricos, e **Isabel Cristina Zuleta** del Movimiento Ríos Vivos Antioquia, para analizar las consecuencias de las fallas en el megaproyecto, y los posibles escenarios a los que se enfrentaría en un futuro próximo.

<iframe id="audio_32713893" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32713893_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Encuentre más información ambiental en[ Voces de la tierra ](https://archivo.contagioradio.com/programas/onda-animalista/)y escúchelo los jueves a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
