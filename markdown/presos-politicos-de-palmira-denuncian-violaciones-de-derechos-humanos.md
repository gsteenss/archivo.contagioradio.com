Title: Presos políticos de Palmira denuncian violaciones de Derechos Humanos
Date: 2017-02-03 16:24
Category: DDHH, Nacional
Tags: Equipo Jurídico Pueblos, INPEC, presos politicos
Slug: presos-politicos-de-palmira-denuncian-violaciones-de-derechos-humanos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/presos_políticos.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Derecho del Pueblo] 

###### [3 Feb 2017] 

Hostigamientos, torturas, abusos sexuales a cónyuges y negligencias médicas fueron algunas de las denuncias hechas por los presos políticos de la cárcel de Palmira Valle, sucesos que llevaron a que el pasado 28 de enero, **el interno Brayan Alexis Osorio, se suicidara en su celda.**

En el comunicado, señalan casos puntuales de abusos a internos como el de Lukas Mayoma Echavarría, quien fue diagnosticado con VIH antes de ingresar a la cárcel y allí los médicos **“siempre dicen que no tiene nada, que está en perfectas condiciones”**, el mismo dictamen que dieron a Brayan Osorio, quien había sido diagnosticado en su ingreso como paciente con esquizofrenia. ([Le puede interesar: Presos políticos exigen liberación de 180 reclusos en grave estado de salud](https://archivo.contagioradio.com/presos-politicos-exigen-liberacion-de-180-reclusos-en-grave-estado-de-salud/))

Comentan que el interno Mayoma se encontró una noche en graves condiciones de salud, y al acudir al Dragoneante Arteaga encargado de la guardia, este respondió que “no tiene nada, se hace el enfermo”, los compañeros de celda deciden protestar por el hecho y acto seguido Arteaga se lleva al interno hacia el calabozo y le dice que **“si sigue jodiendo lo vamos a gasear y a garrotear”.**

Por otra parte, revelaron que las compañeras sentimentales de Jairo Agudelo Ortiz y Lukas Mayoma, fueron sometidas a humillaciones por parte de funcionarios del INPEC, quienes presumieron que las dos mujeres portaban estupefacientes y por ello fueron llevadas a un alojamiento y allí **“fueron abordadas por una dragoneante quien les introdujo los dedos en sus partes íntimas, senos y vagina”.**

El grupo de presos políticos, resalta que desde hace más de un mes vienen denunciando dichas situaciones perpetradas por el personal médico, la Fiduprevisora y el INPEC, “están sometiendo a los internos a tratos crueles, degradantes e inhumanos (…) como tortura física y sicológica”, pero **hasta el momento sus denuncias no han sido atendidas por los entes correspondientes. **([Le puede interesar: Con huelga de hambre exigen libertad a 106 presos políticos con enfermedades terminales](https://archivo.contagioradio.com/con-huelga-de-hambre-exigen-libertad-a-106-presos-politicos-con-enfermedades-terminales/))

Por último hacen un llamado al Estado colombiano, organizaciones defensores de derechos humanos Nacionales e Internacionales, a la Procuraduría, Defensoría del Pueblo, la Personería y el Equipo Jurídico Pueblos, **para que les brinden apoyo y garantías a su integridad.**

[Comunicado\_Presos\_Políticos\_Palmira\_Valle](https://www.scribd.com/document/338348133/Comunicado-Presos-Poli-ticos-Palmira-Valle#from_embed "View Comunicado_Presos_Políticos_Palmira_Valle on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_75320" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/338348133/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-jxB1cHty4H5tHnYoMNFw&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
