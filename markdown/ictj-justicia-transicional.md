Title: “La justicia transicional no se limita al debate sobre la cárcel como está sucediendo en Colombia” ICTJ
Date: 2015-03-05 21:48
Author: CtgAdm
Category: Judicial, Otra Mirada
Tags: justicia transicional en colombia, Proceso de conversaciones de paz de la Habana
Slug: ictj-justicia-transicional
Status: published

###### Foto: cercapaz.org 

<iframe src="http://www.ivoox.com/player_ek_4166266_2_1.html?data=lZajmJeaeo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzVjKjOz87QpYzB0NfS0NSJdqSfxc7fx8jYs9PVjMnSzpCth7W%2BjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Camila Moreno, Directora de ICTJ] 

En cada contexto y en cada país las condiciones son particulares y por ello no se puede hablar de un solo modelo de **justicia transicional**, en cada sociedad las demandas de las víctimas son diferentes y ese modelo debe ajustarse a esas exigencias. Además hablar de **justicia transicional es hablar de una serie de mecanismos,** el debate no se limita a decir si cárcel si o no, son varios los elementos que se deben tener en cuenta, afirma Camila Moreno, directora del ICTJ.

Otros de los elementos de la justicia transicional son la verdad, pero no solo la **verdad** de una de las partes. Los modelos de JT tienen también elementos sobre la **reparación integral** y los **ajustes estructurales en las instituciones del Estado** que propiciaron la guerra, por ejemplo reformar el sector de seguridad, fortalecer el aparato de justicia, y mecanismos para que la sociedad vuelva a confiar en las instituciones. En conclusión **Justicia Transicional no es impunidad y no eso sólo el debate sobre la cárcel**.

En cuanto a los máximos responsables Moreno afirma en Colombia debe darse también un debate para definir quiénes son los máximos responsables, sin embargo esa discusión debe tener en cuenta que **la responsabilidad penal es individual y no solamente porque una persona sea el máximo jefe de una organización no significa que sea responsable de todos los delitos** cometidos por la organización que comanda. “*El que no existan penas privativas de libertad no quiere decir que haya impunidad”* concluye Moreno.

Según Moreno hay que abandonar la idea del “caso a caso” que no funcionó en la ley 975, habría que indagar más de planes, patrones, cómo se establecieron y quienes los definieron, ese modelo permitiría ubicar las responsabilidades reales. Esta forma de abordar la problemática implica también la responsabilidad de las fuerzas militares, políticos y empresarios quienes deben rendir cuentas ante la sociedad por sus delitos.

Frente al tema de la justicia Moreno afirma que las preguntas sobre la legitimidad del aparato de justicia son válidas, por ello hay que buscar las alternativas de tribunales, que sean nacionales pero que pueden tener algún tipo de presencia internacional.
