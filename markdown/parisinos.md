Title: ¿Parisinos?
Date: 2017-02-08 11:37
Category: Camilo, Opinion
Tags: FARC, Zonas Veredales
Slug: parisinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/10f1d81e-9824-4bc5-b5aa-d21a6de98a3b-1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

#### **[Por Camilo de Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/)** 

###### 4 Feb 2017 

¡Acaso vienen de París!, así me dicen, se refirió María Isabel Rueda al diagnóstico de un guerrillero sobre la situación logística y sanitaria en las Zonas de Transición en su comentario en la radio W.

La irónica expresión de la abogada es por decir lo menos, arrogante e ignorante ante la expresión de un ser humano sobre la crisis de salud por la transmisión de enfermedad por los zancudos.

Una paradoja es que esta periodista, que seguro conoce París, olvidó  que en la capital francesa, también hay moscas y zancudos, en algunas estaciones del año con más presencia y que por salud pública se adoptan medidas preventivas.

Desde el púlpito mediático de Prisa, su comentario reitera la doctrina excluyente que desconoce la llamada Colombia profunda. Esa misma, que publicitariamente se conoció en "Magia Salvaje" en bella fotografía y artificiosa voz documental de Julio Sánchez.

Esa Colombia vasta que comunidades negras, indígenas, y mestizas rurales conocen de otro modo, junto con algunos millares de colombianos que por diversas razones allí conviven y que han logrado adaptarse, y sobrevivir a los riesgos de enfermedades del trópico y de la selva húmeda, es el  ambiente en el que ha sobrevivido la guerrilla en movilidad.

Desconoce la ironía de María Isabel que los militantes guerrilleros, los  que se fueron de las ciudades a esas zonas y los que allí nacieron, en desarrollo de la guerra de guerrillas, aprendieron a conocer, a convivir y crecer, en unas condiciones más hostiles que las de las  zonas de agrupamiento.

Sobreaguar a pestes, virus y enfermedades tropicales en medio de una movilidad permanente, no propiamente viviendo en un chalet Suizo es diferente al aquietamiento en el que se encuentran.

Un grupo humano agrupado por salubridad debe realizarlo en arreglo a protocolos preventivos y de respuesta inmediata para evitar la reproducción de enfermedades transmitibles en esa convivencia. El sarcasmo de María Isabel es equívoco, exalta otras formas de discriminación y despotismo mediático.

Hoy quizás si se da ella y muchos la oportunidad de ver, se trata de otra cosa.  Lo planteado es el respeto a la dignidad que merece cualquier ser humano, como la dignidad de ella y la de todos, el derecho a la salud.

El cese de la movilidad y el agrupamiento permanente para empezar por 180 días, y de ahí muchos meses o quizás años, en no más de 20 puntos del país, es un paso posible hacia el reencuentro como nación en la ética de un buen existir, para todas y todos en medio de las diferencias y frente a un modelo extractivista en implementación exponencial.

Hoy esos seres que cesan su movilidad son un potencial de vida para éste país clasista y amargamente excluyente que vive siempre en el sueño europeo o norteamericano.

Estamos ante la posibilidad que la putrefacción del cadáver insepulto de nuestro proyecto de país sea fermento. Es tiempo de liberar  la carroña inoculada en nuestros seres. ¿A quién quieren cortejar sin libertad, los millones de María Isabel que se expresan igual a ella? ¿Acaso a su propia esclavitud mental?

#### [**Leer más columnas de opinión de  Camilo de las Casas**](https://archivo.contagioradio.com/camilo-de-las-casas/) 

------------------------------------------------------------------------

######  Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
