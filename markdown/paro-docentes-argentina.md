Title: Ante represión de Macri docentes en Argentina convocan a Paro
Date: 2017-04-10 16:33
Category: El mundo, Otra Mirada
Tags: Argentina, Docentes, paro, Represión
Slug: paro-docentes-argentina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/17834848_1861151104126821_6294676712810714441_o.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: Comunicación CTERA 

##### 10 Abr 2017

En reunión  plenaria de la Confederación de trabajadores de la República Argentina CTERA, maestros y maestras convocaron a un **paro nacional docente de 24 horas** programado para este **martes 11 de abril**, como respuesta a la represión violenta impartida por parte del gobierno Macri, a las actividades de protesta pacífica del fin de semana.

En los hechos del domingo, resultaron **cientos de maestros rociados con gas pimienta, varios golpeados y cuatro detenidos,** por cuenta de  la violenta intervención policial que **no permitió la instalación de un aula itinerante** que a manera de protesta los y las docentes del país pretendían construir frente a la sede del Congreso Nacional en Argentina.

Como parte de la respuesta a la represión, **los maestros convocan para la tarde de este lunes a un abrazo por la defensa de la escuela pública**, en el mismo lugar donde comenzó a instalarse la escuela itinerante. Asimismo anunciaron que el próximo miércoles se realizarán clases en todas las escuelas públicas del país donde se podrán leer expresiones elaboradas por los docentes.

**Macri no cumple la ley**

Desde hace algunos meses, el gremio docente del país viene movilizandose frente al incumplimiento por parte del gobierno Macri de la **ley que le obliga a convocar los sindicatos del país para fijar el salario mínimo de los maestros**, a pesar de la existencia de un fallo judicial, emitido la semana anterior, que le ordena cumplir la legislación convocando la paritaria nacional docente.

Con la actividad del domingo, los maestros buscaban llevar sus argumentos al Congreso y luego a distintos puntos del país, con **un acto creativo** que no implicara parar las clases en los colegios de la nación mientras se lograba alcanzar un acuerdo entre las partes. Voluntad que fue respondida con brutal violencia, por orden directa del Presidente de la República según manifestaron en voz propia varios policías. Le puede interesar: [Macri ordena impedir el paso de movilización de sindicatos en Argentina](https://archivo.contagioradio.com/macri_movilizaciones_argentina/).

![4ec4634b-4e54-435c-bccc-070df01f6c3b](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/4ec4634b-4e54-435c-bccc-070df01f6c3b.jpg){.alignnone .size-full .wp-image-38992 width="960" height="640"}

"**Cuando hay paro se nos deslegitima y cuando hay otras medidas se las reprime con la policia**" aseguró Eduardo Pereira, secretario General CTERA, agregando que con estas acciones el gobierno envía el mensaje de que lo único que se permite "es estar domesticado y no reclamar, cosa que no vamos a hacer y vamos a seguir en la lucha"

La ley de salarios, desconocida por el gobierno Macri, **fue votada en su momento por el actual ministro de educación Esteban Bullrich** cuando se desempeñaba como diputado de la nación. Al concluir la jornada y a pesar de la violenta reacción, los maestros se mostraron dispuestos a continuar dialogando con el gobierno asegurando que "**Queremos el debate y vamos a seguir sosteniendo la defensa de la escuela pública**"

<iframe id="audio_18068776" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18068776_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
