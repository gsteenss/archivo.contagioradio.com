Title: Así puede obtener su libreta militar con Ley de Amnistía
Date: 2019-07-03 13:19
Author: CtgAdm
Category: Entrevistas, Nacional
Tags: Amnistia, ejercito, jovenes, libreta militar
Slug: asi-puede-obtener-su-libreta-militar-con-ley-de-amnistia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Libreta-Militar.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Las Dos Orillas  
] 

Esta semana entró en vigencía la nueva **Ley de Amnistía,** que le permitiría a cerca de **700 mil personas en calidad de "remisos"** acceder a la libreta militar. La norma cobija de forma exclusiva a jóvenes declarados como remisos, y hayan cumplido la edad máxima para prestar el servicio militar obligatorio (24 años) o presenten alguna de las causales de exoneración que están contempladas en el artículo 12 de la Ley 1861 de 2017 que reglamentó el servicio militar.

El abogado **Alvaro Peña, integrante de la organización Justapaz,** reiteró que esta Ley es exclusivamente para jóvenes declarados como remisos, y quienes no tengan esta condición, deben seguir el procedimiento establecido por la mencionada [Ley 1861.](http://www.secretariasenado.gov.co/senado/basedoc/ley_1861_2017.html) Según información de la dirección de reclutamiento del Ejército, serían cerca de 700 (691.888) mil los jóvenes que se podrían beneficiar con esta medida; definiendo así su situación militar y evitando inconvenientes para acceder a un trabajo.

Peña dijo que la Ley plantea que los jóvenes remisos tienen derecho a una amnistría y "están exonerados de la cuota de compensación militar", por lo que **solo tendrían que pagar el 15% de un salario mínimo mensual legal vigente (125 mil pesos)**; y para ello tendrían cerca de un año. (Le puede interesar: ["La Objeción de Conciencia, un derecho ignorado en Colombia"](https://archivo.contagioradio.com/objecion-conciencia-derecho-ignorado/))

### **El Ejército debe corregir errores que cometió durante la pasada amnistía**

El Abogado afirmó que es fundamental que las fuerzas militares hagan una divulgación de esta Ley para que se puedan acorger todos los jóvenes porque la anterior amnistía, que tuvo vigencia entre lo meses de agosto de 2017 y 2018, fue que solo se acogieron unos 300 mil remisos por desconocimiento del proceso y fallas en el método para resolver las amnistías. En ese momento, los distritos militares solo destinaron un mes por semestre para resolver este tipo de procedimientos, lo que limitó que la mayoría de remisos lograran beneficiarse con esta normatividad.

En esta ocasión el proceso debería ser distinto, pero solo se sabrá en unos días, cuando las Fuerzas Armadas publiquen los documentos y procedimientos que se deben seguir para esta amnistía. No obstante, Peña recordó que en líneas generales el proceso es el mismo:

**Las personas tienen que acercarse al distrito militar que los declaró como remisos, llevar una serie de documentos que serán indicados próximamente, luego se le entregaría un recibo para generar el pago por el valor correspondiente a la emsisión de la tarjeta y posterioremnte tendría que esperar cerca de dos semanas para la expedición de la misma.**

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_37975866" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_37975866_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
