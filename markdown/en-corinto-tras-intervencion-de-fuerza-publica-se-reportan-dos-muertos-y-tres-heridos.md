Title: En Corinto tras intervención de fuerza pública se reportan dos muertos y tres heridos
Date: 2020-08-13 16:25
Author: CtgAdm
Category: Actualidad, Comunidad
Tags: ACIN, Agresiones contra indígenas, Cauca, FFMM, Hostigamientos, indigenas
Slug: en-corinto-tras-intervencion-de-fuerza-publica-se-reportan-dos-muertos-y-tres-heridos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/943ec700-313a-4c60-84a1-8aeb584eebea.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Video-2020-08-13-at-2.33.56-PM.mp4" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Image-2020-08-13-at-2.32.47-PM.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Audio-2020-08-13-at-1.13.08-PM.ogg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"very-dark-gray","fontSize":"small"} -->

*Foto: Cortesía Indepaz*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Este 13 de agosto en horas de la tarde el **Tejido de Defensa de la Vida y los Derechos Humanos de ACIN,** denunció un ataque por parte de la Fuerza Pública contra comunidades libertadoras de la madre tierra en Corinto, [Cauca](https://archivo.contagioradio.com/5-ninos-afro-fueron-masacrados-en-el-barrio-llano-verde-en-cali/).

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El hecho según la organización, **causo la muerte de Abelardo Lis, comunicador de la emisora indígena de Corinto y dos integrantes de la comunidad Nasa**, producto del fuego cruzado entre Ejército, Policía y [grupos armados](https://www.justiciaypazcolombia.com/agc-asesina-a-joven-en-llano-rico/) en medio de un proceso de desalojo en el norte del departamento, el cuál llevaba más de dos días.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"**Ejército de Colombia da tratamiento militar a los libertadores de la madre tierra en el norte del Cauca**, se reportan dos muertos y tres heridos de la comunidad, van dos días de intensas represiones alertamos a la comunidad internacional"*
>
> <cite>Cxhab WalaKiwe - ACIN</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Según la organización esta situación **se viene presentando desde el 12 de agosto en los diversos puntos de liberación en el municipio de Corinto**, en donde desde las 10:00 de la mañana del miércoles se registró **una intervención coordinada, entre el Escuadrón Móvil Antidisturbios** (ESMAD), el cuerpo de carabineros de la **Policía**, el **Ejército** y la seguridad privada de la empresa **Asocaña**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Intervención en la que denuncian, **se destruyó el punto de control territorial para la prevención de la pandemia**, así como la quema de "cambuche", y destrucción de cultivos por parte de los uniformados; sumado al hostigamiento entre grupos armados, **dejando así en una encrucijada y un alto riesgo a la vida** de las comunidades de este territorio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Posteriormente este jueves 13 agosto sobre las 11:00 de la mañana, la comunidad denunció una nueva intervención por parte del ESMAD, *"**el escuadrón realizó una nueva arremetida, mucho más violenta** en la que no solo continúa destruyendo cultivos y cambuches, sino que junto al Ejército dispararon ráfagas de fusil contra la comunidad liberadora"* , señaló la organización .

<!-- /wp:paragraph -->

<!-- wp:video -->

<figure class="wp-block-video">
<video controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Video-2020-08-13-at-2.33.56-PM.mp4">
</video>
  

<figcaption>
Vídeo: Cortesia Indepaz

</figcaption>
</figure>
<!-- /wp:video -->

<!-- wp:paragraph -->

Y agregaron que como consecuencia de esa intervención armada, se registró la muerte de el periodista indígena **Abelardo Lis** , quién fue **impactado con arma de fuego en el pecho y el hombro** en medio del fuego cruzado.

<!-- /wp:paragraph -->

<!-- wp:image {"id":88220,"sizeSlug":"large"} -->

<figure class="wp-block-image size-large">
![](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Image-2020-08-13-at-2.32.47-PM.jpeg){.wp-image-88220}  

<figcaption>
*Foto: Periodista indígena **Abelardo Lis** en la emisora Nacion Nasa*

</figcaption>
</figure>
<!-- /wp:image -->

<!-- wp:paragraph -->

A su vez al interior de la emisora Nación Nasa, resultaron heridos dos comunicadores más; uno de ellos fue impactado en el abdomen, y otro en la rodilla. A pesar de varios obstáculos, los comunicadores fueron trasladados hacia el hospital de Cali, **sin embargo Abelardo Lis debido a la gravedad de sus heridas falleció en el camino**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**La organización indicó que esta situación pudo evitarse, pero que debido a la intervención de la Fuerza Pública** que impidió el ingreso oportuno del vehículo, así como del personal de salud que prestara una atención a los comunicadores, no se puedo llegar antes al centro de atención.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> ***"Al tratar de trasladar los heridos en los vehículos disponibles de la zona, el Escuadrón Móvil los atacó con las armas lanza gases -trufl-, disparando contra las ventanas"***

<!-- /wp:quote -->

<!-- wp:paragraph -->

*"Se trata de una embestida sistemática que en lo ocurrido 2020 se ha presentado en al menos 12 ocasiones",* ante esto la organización hizo un llamado a la opinión pública así como a las organizaciones nacionales e internacionales para denunciar estos actos de agresión coordinados por la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Hechos que se dan en contra de las comunidades, y que e**n las últimas horas ha causado la muerte de 2 personas y 3 heridos, en medio de dos días de represión y hostigamiento hacia los pobladores** que defienden sus cultivos, su territorio y su independencia de protección ante la pandemia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->

<!-- wp:audio {"id":88221} -->

<figure class="wp-block-audio">
<audio controls src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/WhatsApp-Audio-2020-08-13-at-1.13.08-PM.ogg">
</audio>
  

<figcaption>
Escuche el audio conocido por Contagio Radio de la comunidad en medio del fuego cruzado

</figcaption>
</figure>
<!-- /wp:audio -->

<!-- wp:block {"ref":78955} /-->
