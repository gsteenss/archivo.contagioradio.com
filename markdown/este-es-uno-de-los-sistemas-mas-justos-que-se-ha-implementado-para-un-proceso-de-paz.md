Title: "Este es uno de los sistemas más justos que se ha implementado para un proceso de paz"
Date: 2015-09-24 14:17
Category: Nacional, Paz
Tags: Justicia y Paz, Mesa de negociación, Organizaciones civíles, Paz y reconciliación, proceso de paz, Punto numero seis del proceso de paz, Punto sesis, Santos, Timochenko, tRIBUNAL ADHOC, Unidad de investigación y acusación, víctimas
Slug: este-es-uno-de-los-sistemas-mas-justos-que-se-ha-implementado-para-un-proceso-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/justicia-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: proceso 

<iframe src="http://www.ivoox.com/player_ek_8593684_2_1.html?data=mZqmlZuceI6ZmKiak56Jd6KokpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmptjhx5DJt4zpz9SYxsqPsNDnjNjW1dnJscLnjNKSpZiJhZLnjM%2Fi1dnTt4zl1sqY1cqPrMKfytLdzpKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Enrique Santiago, Abogado] 

###### [24 Sep 2015] 

El acuerdo de justicia que se firmó el día de ayer en la Habana brinda todas las garantías para que **no exista impunidad** si se cumple tal como está pactado, con la **inclusión** de las fuerzas del Estado, guerrilleros, empresarios y civiles que hayan participado directa o indirectamente del conflicto aporten con sus versiones a la justicia, la reparación y la no repetición de estos hechos.

### **"Las vícitmas serán las más beneficiadas": Santiago Romero**

El abogado español, Enrique Santiago, miembro del equipo de asesores que gestaron este acuerdo es **un ejemplo para todo el mundo** ya que “*recoge propuestas históricas del movimiento de víctimas y de derechos humanos”* además que *“el sistema se ha construido sobre el combate a la impunidad*”.

Con las versiones sobre el conflicto que den las organizaciones de víctimas y derechos humanos se podrá tener una visión mucho más completa de lo que ha sido el conflicto, Santiago Romero dice que “*cubrirán un importante vació que nunca han cubierto los organismos del Estado como lo es la participación de políticos, grupos económicos y financiadores del conflicto*”.

### **Oposición y Justicia:**

Frente a las voces de oposición que indican que este mecanismo permitirá la **impunidad** Romero indica que “*cualquier crítica que diga que el sistema está basado en la impunidad es una crítica que no tiene sentido*” y se refirió al proceso de Justicia y paz aprobado por el expresidente Álvaro Uribe, en el cual se desmovilizaron 35000 paramilitares y solo se impusieron 33 sanciones con penas máximas de 8 años de cárcel.

Este sistema además impondría las penas de acuerdo a lo que sea rebelado y constatado por el tribunal adhoc el cual estaría conformado por **7 salas con 5 jueces** en cada una de estas estancias,  debido a que “*las infracciones y delitos que han ocurrido durante el conflicto son demasiadas*” indicó Romero. Estos miembros serían  escogidos de acuerdo al **sistema que la mesa de negociación establezca**.

De esta forma “*lo que hay contemplado es **una unidad de investigación y acusación** la cual se nutra de todas las acusaciones formuladas históricamente por los distintos organismos de acusación colombianos, Fiscalía, Procuraduría, comisión de acusaciones cámaras, jurisdicción militar*”, además de las **organizaciones civiles**.

Así mismo Romero  plantea que este es **uno de los sistemas más justos** que se ha implementado para un proceso de paz, pero resalta la importancia de que este se concluya con un **programa estricto de reparación, justicia y no repetición**, además de **garantías** para aquellos que se desmovilicen e integren a la vida civil y política.
