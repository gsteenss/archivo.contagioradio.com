Title: Claudia López ¿más de lo mismo?
Date: 2017-03-07 05:00
Author: JOHAN MENDOZA TORRES
Category: Johan Mendoza, Opinion
Tags: candidatos presidenciales, Claudia Lóez, Partido Verde
Slug: claudia-lopez-mas-de-lo-mismo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/Claudia-lopez1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Avanza Colombia 

#### **Por: [Johan Mendoza Torres](https://archivo.contagioradio.com/johan-mendoza-torres/)** 

[A veces uno ni sabe cómo comenzar a escribir sobre Claudia López, uno siente que si escribe o dice algo, probablemente al otro día toquen a la puerta y sea ella, dispuesta a hablarle con esa voz y esa gran oratoria que tiene (porque la tiene) cargada de todos los atributos simbólicos de un discurso que si uno lo escucha con los ojos cerrados y sin atender tanto al contenido como sí a la fonética, lo primero que se viene a la cabeza es la voz de una mamá enfadada, lo que por causa de ese efecto carismático, provoca que se genere un grado de intimidación casi automático.  ]

[Antes de que sus nuevos escuderos me ataquen, es decir, personas que consumen slogans políticos como “]*[abajo la corrupción]*[” o los que siguen empeñados en creer que criticar esta falsa democracia que existe en Colombia significa ser socialista sin siquiera saber qué es socialismo; quiero mencionar que este artículo, es una crítica decente, pensada, que no pretende hablar sin argumentos de nadie, y que tiene como propósito fundamental: desmantelar la figura mesiánica, objetiva y neutral, que las redes sociales y algunos medios han construido sobre López, no para denigrar de ella, o de sus fotografías con Peñalosa, sino para que los lectores la ubiquen políticamente.]

[No se trata ahora de que los mamertos derechocitos me acusen de ser un “defensor de la corrupción” por hablar en contra de López, sino de entender que por ejemplo la corrupción es un síntoma y no una enfermedad, y que no se arregla dejando solo tres periodos a los corruptos, no termina haciendo que los corruptos muestren al público los alcances de su propiedad privada, tampoco se acaba recordando que un funcionario público sirve para servirle al público, tampoco terminará con veedurías a la ejecución del presupuesto si ya el ramillete de contratistas está arreglado, tampoco sirve que votemos porque la mermelada es mala, o porque en google podemos encontrar los avistamientos de extraterrestres que han tenido lugar, mucho menos sirve decir que los corruptos irán a la cárcel cuando no hay un solo cupo, y a lo bien a lo bien, ni hay cárceles para tanto pícaro (va tocar arrendar celdas en Holanda, donde no tienen ni presos), y mucho menos sirve, bajar sueldos si la gente no puede]**decidir** [qué va a pasar con esa plata que supuestamente se va a ahorrar.]

[No existen soluciones mesiánicas, ni mucho menos se puede creer que la solución a un sistema fracasado y mal diseñado, provendrá de ese mismo sistema fracasado y mal diseñado ¡por favor!; esto, será un proceso largo, que tal vez algunos no logremos verlo, pero confío en que llegará el momento en que los colombianos comunes y corrientes apoyados por amplias capas del Ejército Nacional y la Policía Nacional saldrán a las calles a marchar en masa y ocuparán el congreso de la república, el palacio de justicia y la casa de Nariño, dando un gran golpe social y político contra todos los partidos, contra el sistema mismo. Pero bueno… para eso aún hace falta. Por ahora es mejor que siga sonando como un “chiste”.   ]

[Hay que comenzar diciendo con total tranquilidad que López no representa neutralidad u objetividad, ni ha hecho mérito más allá del marketing político para que la ubiquen como “anti derecha y anti izquierda”, en realidad afirmar su “neutralidad” es desde un punto de vista político e ideológico, una gran falacia.]

[López, supuestamente, toma distancia de la izquierda y de la derecha y por eso muchos la aman… bueno, los que dicen eso por lo general ni saben qué es izquierda ni qué es derecha, es más si la mismísima López, asegura que socialismo consiste en suprimir derechos de propiedad privada y libertad política pues ni tan lejos anda de los embaucadores que se inventaron el concepto “castrochavismo”.]

[Por lo pronto sí habría que apelar a la sensatez y reconocer que López es una persona suficientemente inteligente, con un gran discurso, y con algo que sólo tiene ella y nadie más, un gran carisma, que como venía diciendo, radica en los atributos simbólicos de su discurso.   ]

[López parece social demócrata pero no lo es, pues la social democracia es supuestamente una rama moderada del socialismo, y afirmar esto, sería para ella un notable insulto. ¿De qué tendencia es López? evidentemente no es izquierda. Tampoco es una política que promueva el proteccionismo económico o el nacionalismo como punto de partida ideológico. López es, sin lugar a dudas una liberal que parece fundamentada en lo más arcaico y trasnochado de la filosofía política: Hobbes.]

[Para López la abstención electoral es el peor mal que tiene Colombia ¿Pero por qué la abstención es asumida como el peor mal? asegurar que la abstención es un mal es “pasarse por la galleta” el complejo fenómeno social que cada cuatro años y desde hace más de 200 años demuestra que la abstención gana porque los colombianos no creemos que aquí haya democracia, la abstención gana desde hace más de 200 años porque los colombianos sabemos que este sistema es un negocio en el que ganamos poquitico; y muy a pesar de sus críticas al socialismo, resulta que en Colombia sí que menos podemos hablar de libertad política o si no que lo digan los cientos de líderes sociales asesinados… ¿o será que los]*[claudialopiztas]*[ no sabían que aquí matan a la gente por pensar políticamente diferente?   ]

[López está en contra de la abstención, al igual que un banco está en contra de que la gente guarde la plata debajo del colchón. Ella es una misionera declarada de ese discurso trasnochado Hobbesiano que impulsa al sujeto para que ejerza su derecho civil al voto, pues al votar se entrega la libertad política, por ejemplo, a senadoras como López, convirtiendo a ese sujeto libre, en un “buen ciudadano”, es decir, en súbdito del Estado, un Estado que hasta López lo sabe, ha sido durante casi toda su historia: un intento fallido, pero con muy buen marketing.   ]

[Si a todo lo anterior le sumamos que nos encontramos en una sociedad que vive atosigada de información, que por culpa de un puñado de pseudo-periodistas y editores, se ha convertido también en una sociedad ucrónica, que no cree en la historia por conclusión crítica y seria, sino por los inventos alternativos que se “aprenden” a través de novelas televisadas o memes en redes sociales, y que configura individuos que se aferran al primer slogan que les suene chévere, y lo instalan como verdad.]

[Claudia López en resumen es un happening, una nueva moda, o como diría el dicho más sabio que persiste hasta nuestros días: ella es más de lo mismo, es liberal, y sólo anda resentida con los socialistas que intentan desenmascararla, y quieren hacer a un lado el marketing para mostrarla como lo que realmente es en términos políticos.   ]

[Hace poco, López decía que votar es un arma. Votar no es un arma. Es un derecho civil, que deben cumplir por las buenas los buenos ciudadanos. Y si esos socialistas coartadores de la libertad no votan, pues para eso está la coacción, y los obligamos, total… cuando un liberal no puede convencer, tiende a obligar o bien a denigrar del enemigo político; en últimas, tiende a ignorar fenómenos que demuestran el fracaso del sistema, es decir, ese 55% (según López) que no votamos y llevamos ganando 200 años porque no podemos celebrar semejante engaño.]

[Definitivamente López como buena liberal, que reencaucha ahora el jurásico discurso Hobbesiano del buen ciudadano y se presenta como esa “gran novedad” respaldada por su carisma, es sin duda una maestra del oportunismo. ¿Qué querrá? Gobernar democráticamente… dirán sus fieles defensores. Por mi parte, entenderé que la democracia según López es un sistema para el pueblo y por el pueblo, eso sí, siempre y cuando en ese pueblo no haya gentuza de corte socialista, pues esa clase de país no le cabe en la cabeza a esa señora, a esa buena ciudadana que es Claudia López.   ]

#### **[Ver más columnas de opinión de Johan Mendoza](https://archivo.contagioradio.com/johan-mendoza-torres/)**

------------------------------------------------------------------------

###### Las opiniones expresadas en este artículo son de exclusiva responsabilidad del autor y no necesariamente representan la opinión de Contagio Radio.
