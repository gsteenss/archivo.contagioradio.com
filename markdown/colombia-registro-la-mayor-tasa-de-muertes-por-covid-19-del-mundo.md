Title: Colombia registró la mayor tasa de muertes por Covid-19 del mundo
Date: 2020-08-14 20:13
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Coronavirus, Covid-19, Tasa de mortalidad, Universidad John Hopkins
Slug: colombia-registro-la-mayor-tasa-de-muertes-por-covid-19-del-mundo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/08/Covid-19.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

De acuerdo con cifras de la Universidad [John Hopkins](https://coronavirus.jhu.edu/us-map), **Colombia es el país con el brote más mortífero de Covid-19 de todo el mundo en la actualidad.** (Le puede interesar: [El sistema de salud está totalmente colapsado afirman gremios de la salud](https://archivo.contagioradio.com/el-sistema-de-salud-esta-totalmente-colapsado-afirman-gremios-de-la-salud/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según el reporte los registros de la última semana arrojaron que **Colombia tuvo 43 muertes por millón de habitantes** superando a países como Brasil con 32 muertes y Estados Unidos con 24, los cuales se han caracterizado por la laxitud de sus gobiernos para contener la pandemia. (Le puede interesar: [En el Amazonas por cada diez mil personas hay 190 contagios](https://archivo.contagioradio.com/en-el-amazonas-por-cada-diez-mil-personas-hay-182-contagios/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/IvanCepedaCast/status/1293586631698583552","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/IvanCepedaCast/status/1293586631698583552

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Colombia encabeza la lista seguido por países como Bolivia, Panamá, Perú, México y Brasil lo que da cuenta que **Latinoamérica está siendo la región más afectada por el Covid-19 en la actualidad.** En el informe también señala que nuestro país es el cuarto con mayor número de contagios totales en América Latina.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A juzgar por las **cifras la política pública que ha llevado a cabo el Gobierno no es efectiva en la contención del virus, pese a que están por completarse cinco meses de confinamiento** y a que el presidente Duque ha dedicado gran parte de su agenda al virus, incluso con su programa televisivo denominado «*Prevención y acción*» espacio en el que expone toda sus políticas para la mitigación del virus.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, **cárceles y batallones se han convertido en grandes focos de infección.** Por una parte, los centros penitenciarios, que experimentan un hacinamiento que asciende a un 51%, a inicios de agosto registraban una cifra de contagios cercana a los 4.000 reclusos con Covid-19. Por otro lado, los centros castrenses han registrado contagios masivos como en el caso del Batallón de Artillería y Defensa Antiaérea Nueva Granada de Barrancabermeja donde los infectados superaron el centenar. (Lea también: [Muere recluso de Covid 19 por negligencia del INPEC en la Picota](https://archivo.contagioradio.com/muere-recluso-de-covid-19-por-negligencia-del-inpec-en-la-picota/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Italia prohibió el ingreso de personas desde Colombia como medida contra el Covid-19

<!-- /wp:heading -->

<!-- wp:paragraph -->

**Italia, país que hace unos meses era epicentro de la pandemia, prohibió el ingreso de personas provenientes desde Colombia por la pandemia**, así lo confirmó el Ministro de Sanidad italiano, Roberto Speranza.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

**Colombia fue incluida dentro de los países con mayor riesgo debido al elevado número de casos confirmados que se encuentra entre los 10 mil y 12 mil contagios confirmados por día**, lo que llevó a las autoridades italianas a decretar la restricción.

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
