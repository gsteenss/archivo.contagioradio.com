Title: Hay que reconocer la importancia de terminar el conflicto: Alejo Vargas
Date: 2015-05-29 13:35
Category: Entrevistas, Paz
Tags: Alejo Vargas, Catatumbo, Cauca, cese bilateral del fuego, conflicto armado, Conversacioines de paz en Colombia, ELN, Guapi, Magdalena Medio, Putumayo, San Agustín
Slug: hay-que-reconocer-la-importancia-de-terminar-el-conflicto-alejo-vargas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/05/alejo-vargas-contagio-radio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: hsbnoticias 

<iframe src="http://www.ivoox.com/player_ek_4559813_2_1.html?data=lZqim52Vd46ZmKiak52Jd6Kkk5KSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYy1zcrX0ZC6pdPbwtiah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Entrevista: Alejo Vargas] 

###### [29 May 2015] 

En el marco del conflicto armado que ocurre actualmente en **Colombia** y después de la muerte de 26 guerrilleros en un bombardeo en la vereda **San Agustín**, del municipio de **Guapi**, **Cauca;** el pasado 22 de Mayo, **Alejo Vargas**, Director del Centro de Pensamiento de la Universidad Nacional de Colombia, habla acerca de la tensión que genera el escalamiento del conflicto.

La solidez de los acuerdos es lo que va a permitir que la situación se pueda resolver y que se retome la dinámica de disminución y des escalamiento. El analista se refiere a la diferencia que hay entre la población del sector urbano que opina desde el exterior, y otros territorios donde la guerra es la cotidianidad; regiones como **Cauca, Putumayo, Arauca, Magdalena Medio y Catatumbo, **donde hay bombardeos y enfrentamientos que ponen en peligro la vida de sus habitantes, por lo que se debe pensar en la necesidad de terminar la confrontación armada.

"*El gobierno debería establecer acuerdos pronto, porque hasta que no se llegue a acuerdos definitivos, el riesgo de que hayan hechos que puedan poner en peligro las conversaciones de paz es alto, se debe empezar por el reconocimiento de las víctimas*" aseguró. Entre los puntos primordiales del proceso de paz, **Alejo Vargas** recalca la importancia de crear una campaña de comunicación, pedagogía e información para que los colombianos conozcan la importancia de terminar el conflicto armado para llevarlos progresivamente hacia la paz, pero esto no se gana solamente con propaganda, afirma el analista.

El **ELN** ha dicho que las fuerzas militares han presionado para el fin del cese bilateral, por lo tanto es insostenible un anuncio de entendimiento; "*Cada organización que es protagonista del conflicto, tiene su posición, no se debe pensar en quién tiene más o menos razón, todo hace parte de interpretaciones, análisis y miradas; se debe retomar el camino de ir bajando el nivel del conflicto, tenemos una opinión en la sociedad que en parte es manipulada por los medios pero que se ha construido con el tiempo*", enfatiza **Alejo Vargas**.
