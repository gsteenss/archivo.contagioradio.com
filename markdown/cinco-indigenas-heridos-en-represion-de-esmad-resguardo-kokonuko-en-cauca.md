Title: Cinco indígenas heridos en represión de ESMAD en resguardo Kokonuko en Cauca
Date: 2017-07-12 11:05
Category: DDHH, Nacional
Tags: ESMAD, indígenas, Kokonuko, ONIC
Slug: cinco-indigenas-heridos-en-represion-de-esmad-resguardo-kokonuko-en-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/indigena-kokonuko.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ONIC] 

###### [12 Jul. 2017] 

Según información entregada por Nelsón Avirama, Gobernador del Resguargo Kokonuko en la Finca Aguas Tibias \#2 en el departamento del Cauca, en medio del proceso de liberación de la madre tierra el Escuadrón Móvil Antidisturbios – **ESMAD – arremetió contra esta comunidad, dejando 5 personas heridas.**

“Nos quieren sacar del territorio y nos mandan balas de goma. **Nos están desalojando de nuestro territorio. Estamos desde las 3 a.m**. y seguiremos defendiéndolo” relató Avirama. Le puede interesar: [Trabajadores de INCAUCA serían responsables del asesinato de comunero indígena](https://archivo.contagioradio.com/incauca-responsables-del-asesinato-de-comunero-indigena-del-cric/)

Así mismo, se asegura que **uno de los heridos es un comunicador indígena para el que se ha solicitado una ambulancia** “pero no se permite el ingreso de la ambulancia al territorio” informaron en un breve comunicado difundido a través de las redes sociales.

En entrevistas anteriores dadas a Contagio Radio la comunidad había asegurado que** necesita que se haga un saneamiento del reguardo y que se cumplan los acuerdos a los que se han llegado con **el gobierno el cual se refiere a la concertación con el dueño del predio para que permita que ellos lo adquieran.

Según cifras de la ONIC durante el proceso de liberación de la madre tierra **desde el 2009 a la fecha han sido asesinados ocho comuneros.** Le puede interesar: [Continúa proceso de liberación de la madre tierra en Aguas Tibias, Cauca](https://archivo.contagioradio.com/continua-proceso-de-liberacion-de-la-madre-tierra-en-aguas-tibias-cauca/)

<iframe id="audio_19766053" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19766053_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>
