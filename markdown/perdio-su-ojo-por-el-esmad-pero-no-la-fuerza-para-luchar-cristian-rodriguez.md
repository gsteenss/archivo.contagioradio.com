Title: Cristian Rodríguez perdió un ojo por el ESMAD, pero no la fuerza para luchar
Date: 2020-02-24 18:23
Author: AdminContagio
Category: Actualidad, DDHH, Nacional
Tags: ESMAD, Fuerza Pública, ojos
Slug: perdio-su-ojo-por-el-esmad-pero-no-la-fuerza-para-luchar-cristian-rodriguez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/cristian-rodriguez-perdida-de-ojo.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/cristian-rodriguez-2-perdida-de-ojo.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/cristian-rodriguez-perdida-de-ojo-1.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### reportajes

Cristian Rodríguez perdió un ojo por el ESMAD, pero no la fuerza para luchar  
-----------------------------------------------------------------------------

**24 Febrero 2020**

Cristian Rodríguez es una de las doce personas con lesiones oculares producto del accionar desmedido por parte de los integrantes del ESMAD, en el marco de las movilizaciones del paro nacional que se registraron el año pasado.

</p>
###### Texto: Sandra Gutíerrez - Fotos ESMAD:Gabriel Galindo/Contagio Radio 

"El 16 de diciembre de 2019, Cristian Rodríguez se encontraba en la entrada de la Universidad Nacional sobre la carrera 30. Él, junto a otro grupo de personas, intentaba ingresar al centro educativo para escapar de los gases lacrimógenos lanzados por el Escuadrón Móvil Antidisturbios (ESMAD). En menos de un segundo, la luz de su ojo izquierdo se apagó.

**Cristian Rodríguez** es una de las doce personas con lesiones oculares producto del accionar desmedido por parte de los integrantes del ESMAD, en el marco de las movilizaciones del paro nacional que se registraron el año pasado.

</p>
### Víctimas de esta fuerza policial

Estos hechos hacen parte del prontuario de este cuerpo, que a lo largo de sus 20 años de existencia, ha cobrado la vida de por lo menos 34 personas y que, en tan solo los 10 primeros días de paro (del 21 al 30 de noviembre de 2019) hirió a 300 manifestantes, la mayoría de ellos jóvenes.

Cristian fue herido el 16 de diciembre, cuando se encontraba participando en una movilización que se dirigía hacia la Universidad Nacional de Colombia. El dictamen médico fue estallido ocular, que le provocó la perdida total de su ojo izquierdo.

https://youtu.be/h9ImUxE8hNA

Pero, ¿por qué en los ojos? De acuerdo a la defensora de derechos humanos e integrante de la Comisiòn de Justicia y Paz Camila Forero, si el ESMAD actuara conforme a lo establecido en el marco de los derechos humanos, no habría cabida a que lesionaran e incluso, asesinaran a las personas.

No obstante, para ella, estos hechos son producto del exceso de fuerza que tiene este grupo, del mal manejo que le dan a las armas, que en teoría son no letales y de la falta de un enfoque en derechos humanos para salvaguardar la vida.

En este caso, referente a **las heridas en los ojos, la defensora señala que estas son consecuencia, primero de la violación al protocolo para el uso de las mismas, que deberían ser disparadas al cielo o al suelo**, con la intensión de dispersar y nunca de forma frontal a las personas.

Segundo, Forero manifiesta que este hecho también estaría relacionado a la formación de estos Escuadrones, **"el ESMAD, es el ejemplo claro de cómo a personas se les impone una doctrina del enemigo interno, y de cómo esta es ejecutada, acabando con vidas, ojos...**".

En relación al uso inadecuado de las armas, el 14 de enero de 2020, la Procuraduría General de la Nación pidió a la Policía la suspensión inmediata del uso de escopeta calibre 12 utilizada por el ESMAD para disolver disturbios y bloqueos de vías. Esta misma arma no letal, fue la que acabó con la vida de Dilan Cruz el pasado 23 de noviembre. (Le puede interesar: "Procuraduría: ESMAD no está entrenado para usar escopeta Calibre 12")

<figure>
![Cristian Rodriguez ESMAD](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/cristian-rodriguez-2-perdida-de-ojo-1024x630.jpg){width="1024" height="630" sizes="(max-width: 1024px) 100vw, 1024px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/cristian-rodriguez-2-perdida-de-ojo-1024x630.jpg 1024w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/cristian-rodriguez-2-perdida-de-ojo-300x185.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/cristian-rodriguez-2-perdida-de-ojo-768x472.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/cristian-rodriguez-2-perdida-de-ojo-370x228.jpg 370w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/cristian-rodriguez-2-perdida-de-ojo.jpg 1200w"}  

<figcaption>
Cristian Rodriguez con la pancarta que llevaba el día que se movilizaba en la ciudad de Bogotá - Gabriel Galindo/Contagio Radio

</figcaption>
</figure>
### El desmonte del ESMAD  

De acuerdo con el informe de la organización Temblores ONG, titulado “Pa’ fuera, pa’ la calle. Silencio Oficial: Un aturdido grito de justicia por los 20 años del ESMAD”, desde su creación, se registra un total de 24 personas que han sido presuntamente asesinadas por uniformados.

Por otro lado, según Forero, el número de heridos es más difícil de obtener porque la gran mayoría de ellos no denuncian este tipo de hechos, sin embargo, recalcó que en tan solo los 10 primeros días del paro nacional del 2019, el ESMAD hirió a 300 personas.

> «La aparición del ESMAD en situaciones de contención de orden público es en sí misma violenta, porque esta es una fuerza de choque incontrolable, y es ahí cuando debe cuestionarse su existencia, y hablar del desmonte de ese escuadrón» sostiene Forero.

Para Cristian Rodríguez, «no es justo que un ciudadano pierda un órgano» en estas circunstancias, por ello asegura que se hace urgente «repensarse» la labor que realiza esta institución. Asimismo, afirma que ya interpuso una demanda en contra del Estado, pero manifiesta que sabe lo arduas y peligrosas que pueden ser este tipo de acciones que buscan justicia.

Según el informe de Temblores, las investigaciones adelantas en contra de miembros de esta estructura se encuentran en la impunidad, debido a que **desde 1999 hasta 2019 solo existen dos procesos penales en la Fiscalía en contra de agentes del ESMAD y se sabe de una sola condena.** (Le puede interesar: [“Pa’ fuera, pa’ la calle. Silencio Oficial: Un aturdido grito de justicia por los 20 años del ESMAD»](https://www.academia.edu/41239342/Silencio_Oficial_un_aturdido_grito_de_justicia_por_los_20_a%C3%B1os_del_Esmad))

A pesar de esta situación, Cristian expresa que seguirá en la defensa y construcción de una Colombia distinta, y que aunque su vida se haya transformado para siempre, este hecho es un impulso más para trabajar por un país distinto.

[![Persecución a pobladores de La Macarena en desarrollo de Operación Artemisa](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/ERj7C2JXUAEPF_k.jpg){width="370" height="182"}](https://archivo.contagioradio.com/persecucion-a-pobladores-de-la-macarena-en-desarrollo-de-operacion-artemisa/)  

#### [Persecución a pobladores de La Macarena en desarrollo de Operación Artemisa](https://archivo.contagioradio.com/persecucion-a-pobladores-de-la-macarena-en-desarrollo-de-operacion-artemisa/)

La Macarena arde no solo por las llamas que arrasan con la fauna protegida; también por las múltiples…  
[![Cristian Rodríguez perdió un ojo por el ESMAD, pero no la fuerza para luchar](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/cristian-rodriguez-perdida-de-ojo-1-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/perdio-su-ojo-por-el-esmad-pero-no-la-fuerza-para-luchar-cristian-rodriguez/)  

#### [Cristian Rodríguez perdió un ojo por el ESMAD, pero no la fuerza para luchar](https://archivo.contagioradio.com/perdio-su-ojo-por-el-esmad-pero-no-la-fuerza-para-luchar-cristian-rodriguez/)

reportajes Cristian Rodríguez perdió un ojo por el ESMAD, pero no la fuerza para luchar 24 Febrero 2020…  
[![Con fútbol comunidad logra desalojar al ESMAD](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/ESMAD-370x260.jpeg){width="370" height="260"}](https://archivo.contagioradio.com/con-futbol-comunidad-logra-desalojar-al-esmad/)  

#### [Con fútbol comunidad logra desalojar al ESMAD](https://archivo.contagioradio.com/con-futbol-comunidad-logra-desalojar-al-esmad/)

En una inusual protesta, un grupo de personas logró desalojar al ESMAD de un parque público que viene…  
[![Un cacerolazo para amplificar la voz de casi 200 excombatientes asesinados](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Excombatientes-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/cacerolazo-mplificar-voz-casi-200-excombatientes-asesinados/)  

#### [Un cacerolazo para amplificar la voz de casi 200 excombatientes asesinados](https://archivo.contagioradio.com/cacerolazo-mplificar-voz-casi-200-excombatientes-asesinados/)

Según el último reporte de la Misión de Verificación de la Organización de las Naciones Unidas, 12 excombatientes…  
[![Atentado contra Orlando Castillo, reflejo de la guerra en Buenaventura](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Orlando-Castillo-370x260.jpg){width="370" height="260"}](https://archivo.contagioradio.com/atentado-contra-orlando-castillo-reflejo-de-la-guerra-en-buenaventura/)  

#### [Atentado contra Orlando Castillo, reflejo de la guerra en Buenaventura](https://archivo.contagioradio.com/atentado-contra-orlando-castillo-reflejo-de-la-guerra-en-buenaventura/)

El líder quien se encuentra a salvo afirmó que se trata de un exterminio contra líderes y líderes…
