Title: Solicitan medidas cautelares a CIDH para proteger a los líderes sociales
Date: 2018-05-02 12:44
Category: DDHH, Movilización
Tags: asesinatos de líderes sociales, defensores de derechos humanos, medidas cautelares CIDH, protección a líderes sociales
Slug: solicitan-medidas-cautelares-a-cidh-para-proteger-a-los-lideres-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/lideres-sociales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [02 May 2018] 

Los Congresistas Alirio Uribe Muñoz e Iván Cepeda junto a organizaciones de la sociedad civil hicieron un llamado a la Comisión Interamericana de Derechos Humanos para que se **adopten medidas cautelares** y se proteja la vida de líderes sociales y defensores de derechos humanos del país. Pidieron que haya un mecanismo donde un grupo de personas expertas cumpla la función de hacer seguimiento al cumplimiento de medias de protección que son de carácter colectivo.

En rueda de prensa hicieron alusión al incremento en el número de asesinatos y agresiones contra quienes defienden los derechos humanos, que según Somos Defensores a lo largo de 2018 se han cometido **46 asesinatos, frente a los 282 que se registraron entre 2016 y 2017. **A la vez que argumentaron las falencias que ha tenido el Estado colombiano para garantizar la protección de los líderes y asegurar que se lleven a cabo las investigaciones de los asesinatos que han ocurrido.

### **Solicitud agrupa a 445 líderes y lideresas sociales** 

De acuerdo con el senador Iván Cepeda, se radicó una solicitud colectiva en Washington, sede de la CIDH, “que incluye a 445 mujeres y hombres líderes sociales, que representan a un conjunto de plataformas que han sido objeto de **exterminio de líderes sociales** y defensores en el país”. Afirmó que las peticiones son para otorgar medidas cautelares fruto de que los “asesinatos no han parado (…) y las medidas que se han tomado **no son suficientes**”.  (Le puede interesar:["Atentan contra la vida de líder indígena Wayuú en Rioacha, Guajira"](https://archivo.contagioradio.com/atentan-contra-la-vida-de-lider-indigena-wayuu/))

### **Estado de Colombia tiene el deber de proteger a sus ciudadanos** 

Por su parte, el representante Alirio Uribe, enfatizó en que las medidas cautelares de la CIDH deben ser de obligatorio cumplimiento basándose en el deber del Estado de proteger a las personas. Indicó que se ha denunciado que los **homicidios están relacionados con la actividad** que realizan los defensores de derechos humanos y hay patrones en la forma en que se han cometido.

El congresista dijo que la medida cautelar busca “ponerle de presente al Estado las obligaciones que tiene de proteger a sus ciudadanos” teniendo de presente que las autoridades “**conocen la situación de riesgo que viven los defensores** de derechos humanos”, que en el 80% hacen parte del sector agrario, indígenas, campesinos y afrodescendientes.

Recordó que la medida cautelar debe ser un campanazo para el Estado que ha optado por realizar medidas de militarización **“en territorios donde aún así siguen sucediendo asesinatos”**. Afirmó que, por ejemplo, desde el Estado no hay un esfuerzo por consolidar las cifras de asesinatos que según Indepaz son 64 en lo que va del año.

### **Organizaciones sociales piden que se detengan los homicidios** 

Al llamado de los congresistas se sumaron las organizaciones sociales y movimientos que defienden derechos humanos, quienes manifestaron que hay una preocupación por la **falta de acción de instituciones** como la Fiscalía que aún no entrega avances en las investigaciones para conocer los autores materiales de los crímenes que se han cometido contra los líderes sociales del país. (Le puede interesar:["Ministerio del Interior propuso mecanismo de protección colectiva para líderes sociales"](https://archivo.contagioradio.com/ministerio-del-interior-propuso-mecanismo-de-proteccion-colectiva-para-lideres-sociales/))

La Coccam afirmó que sólo en este año han sido asesinados **31 líderes de esa organización,** alertando que las bandas criminales y los grupos armados “reinan en los territorios” donde se han venido realizando trabajos como la sustitución voluntaria de cultivo ilícitos. De igual forma, argumentaron que “no es posible que los asesinatos sigan ocurriendo sin que nada pase, hace falta medidas colectivas y no individuales”.

Por su parte, el Movimiento de Víctimas de Crímenes de Estado, indicó que las víctimas no han sido el centro de los Acuerdos de Paz como se había dicho en un principio. De igual forma, Aida Avella, integrante del partido político Unión Patriótica, recordó que se debe poner en marcha medidas que protejan la vida de los defensores “para evitar un **derramamiento de sangre** como ocurrió en el pasado con la UP”.

###### Reciba toda la información de Contagio Radio en [[su correo]
