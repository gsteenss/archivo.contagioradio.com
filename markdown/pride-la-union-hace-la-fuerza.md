Title: PRIDE: La unión  hace  la  fuerza
Date: 2015-11-21 11:00
Author: AdminContagio
Category: 24 Cuadros, LGBTI
Tags: Activistas LGBT, Marcela Sanchez Directora de Colombia Diversa, Pride, Pride la unión hace la fuerza, Pride una película dirigida por Matthew Warchus
Slug: pride-la-union-hace-la-fuerza
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Pride.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:hoyesarte 

###### [19 nov 2015 ] 

El 17 de noviembre se  estrenó  en  las salas de cine colombianas "Pride", una película  dirigida  por Matthew Warchus y escrita  por Stephen Beresford la  cual bajo el drama  y la comedia reivindica los derechos, la unión y la equidad.  Marcela Sanchez Directora de Colombia Diversa  una de las organizadoras y asistente al estreno del filme, habló sobre la importancia que  tiene el cine y la  comedia como plataforma  contra  la discriminación  y lo esencial que  es la unión frente a la desigualdad en los derechos humanos.

**La  película  está basada  en hechos de la  vida  real**. Tras la  huelga  de los mineros británicos en 1984, un grupo de activistas LGBT toman la iniciativa de recaudar  dinero  para las familias afectadas en un  pequeño  pueblo en Gales, quienes no todos  están  totalmente  de acuerdo de recibir esta ayuda proveniente de un grupo de gais y lesbianas, lo que si es  cierto es que esta unión de comunidades e iniciativas les cambiará la  vida.

**Pride fue galardonada  con el premio Queer  Palm y proyectada en el Festival de Cannes de 2014** ha pocos  meses de  su realización, y cuenta con la actuación estelar de la recordada Imelda Staunton como Hefina Headon una habitante de  Onllwyn, personaje que le valió la nominación como mejor actriz de reparto en Los Premios de La Academia de Cine Británicos, categoría ganó en los British Independent Film Awards.

El  filme presenta una  continuidad cautivadora, que transporta e involucra al espectador frente a las problemáticas latentes de los años 80´s en Inglaterra, la evolución de todos los personajes frente a la lucha de equidad de derechos y sus conflictos personales hace de "Pride" una película bien fundamentada e interesante, diciente en temas de unión  social y los alcances políticos y sociales a los que pueden  llegar si derrumban los muros de la diferencia. **“Cuando Luchas  contra un enemigo más  grande que tú, tener a tu lado a un amigo que no creías que existía, es la mejor sensación que hay en el mundo”**.

<iframe src="https://www.youtube.com/embed/4WYmpuzYdVI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
