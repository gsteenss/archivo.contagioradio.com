Title: UNASUR insiste en necesidad cese al fuego bilateral en Colombia
Date: 2015-06-16 09:39
Category: Otra Mirada, Paz
Tags: Cese al fuego, ernesto samper, FARC, habana, juan manuel, paz, Santos, unasur
Slug: comunidad-internacional-ofrece-verificar-cese-al-fuego-bilateral-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/UNASUR.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Torres Cobo 

###### [16 jun 2015] 

Una vez más la comunidad internacional se mostró dispuesta a verificar un eventual cese de hostilidades entre las Fuerzas Militares y la guerrilla de las FARC en el marco del proceso de paz que se adelanta en Colombia entre esta insurgencia y el gobierno de Juan Manuel Santos.

Esta vez, el Secretario General y portavoz de la **UNASUR**, Ernesto Samper, declaró que "*El Gobierno y Farc podrían evitar muertes pactando ya un cese bilateral del fuego con acompañamiento internacional*", y añadió que "*en las actuales circunstancias se justifica estudiar este cese bilateral. Estamos en la última etapa del proceso y es la etapa en la que los enemigos de la paz van a tratar de que fracase. Además es la hora de la verdad y se van a tener que conocer los sacrificios que hagan las Farc y el Gobierno para celebrar una paz*”.

Cabe recordar que el 17 de diciembre del 2014, cuando las FARC hicieron pública su intensión de declarar un cese al fuego unilateral a partir del 20 de diciembre, con acompañamiento y verificación nacional e internacional, fue el presidente Juan Manuel Santos el que descartó la posibilidad de un cese bilateral con verificación internacional.
