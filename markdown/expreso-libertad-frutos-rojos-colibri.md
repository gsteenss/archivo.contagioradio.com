Title: Expreso Libertad : Frutos Rojos Colibrí
Date: 2019-07-08 12:24
Author: CtgAdm
Category: Expreso Libertad
Tags: Cooperativismo, Mujeres Excombatientes, Proyecto Solidario, reincorporación
Slug: expreso-libertad-frutos-rojos-colibri
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/yufid.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: FARC 

<div>

[Las mujeres Farianas que hacen su tránsito a la reincorporación, están colocando en marcha [un proyecto productivo de economía solidaria, en dos Espacios Territoriales de Capacitación y Reincorporación, ubicados en Dabeiba y Mutata, ambos en el departamentos de Antioquia. Con esta iniciativa no solo buscan generar un trabajo de cooperativismo, sino también construir lazos con las comunidades desde el enfoque de género y el empoderamiento de las mujeres.]

<div>

[ ]

</div>

<div>

[Conozca en este programa del Expreso Libertad el surgimiento de la Planta Frutos Rojos Colibrí y su esperanza de aportar a la construcción de igualdad en Colombia.]

</div>

</div>

<div>

</div>

<div>

\[embed\]https://www.facebook.com/contagioradio/videos/1091143997744013/\[/embed\]

</div>

<!-- Load Facebook SDK for JavaScript -->

<!-- Your embedded video player code -->

###### Encuentre más información sobre Prisioneros políticos en[ Expreso libertad ](https://archivo.contagioradio.com/programas/expreso-libertad/)y escúchelo los martes a partir de las 11 a.m. en [Contagio Radio](https://archivo.contagioradio.com/alaire.html) 
