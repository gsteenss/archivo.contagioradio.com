Title: Gobierno debe garantizar seguridad para líderes y defensores de DDHH
Date: 2017-04-06 21:17
Category: DDHH, Nacional
Tags: defensores de derechos humanos, lideres sociales, Paramilitarismo
Slug: gobierno-debe-garantizar-seguridad-para-lideres-y-defensores-de-ddhh
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_274.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [06 Abril 2017] 

Un grupo de más de 100 académicos, políticos, sindicalistas, artistas entre otros sectores de la sociedad, enviaron una carta al presidente Juan Manuel Santos y al Físcal General, Néstor Humberto Mártinez, para expresarles su **preocupación ante la crisis de falta de garantías para la participación política de líderes y la defensa de los derechos humanos.**

En la misiva, señalan que ésta situación es producto de la persistencia de patrones de violencia en **“función de poderes y negocios” en disputa de tierras, rentas y formas de hacer política, infundados en discursos de odio y de la corrupción**, que permiten que grupos al margen de la ley persigan y asesinen a líderes sociales y defensores de derechos humanos, en todo el país.

Sumado a ello, se encuentran una serie de detenciones arbitrarías contra integrantes del movimiento social, bajo el argumento del peligro que representan para el Estado y las comunidades por promover marchas;  que de acuerdo con el texto, **pone en riesgo la presunción de inocencia de estas personas, su derecho a la protesta social y a la posibilidad real de construir paz.** Le puede interesar: ["Milena Quiroz fue condenada al destierro: abogados de la defensa"](https://archivo.contagioradio.com/milena-quiroz-fue-condenada-al-destierro-abogados-de-la-defensa/)

Frente a estos hecho, lo más de 100 firmantes de la carta, le exigen al presidente, que en cumplimiento de su obligación a la protección adecuada de esos liderazgos, reconozca la persistencia de esos patrones de violencia contra líderes, defensores y comunidades del país, ya que de no hacerlo **“pone en riesgo la implementación de los acuerdos de paz”.**

Organizaciones defensoras de derechos humanos recientemente estuvieron en la Comisión Interamericana de Derechos Humanos, evidenciando el escalamiento y reagrupación de grupos paramilitares en el país, que están estableciéndose en los territorios abandonados por la guerrilla de las FARC-EP, y **la persecución y asesinato sistemáticos de líderes y defensores de derechos humanos**, a manos de estos grupos. Le puede interesar: ["Asesinado Jairo Chilito profesor de ASOINCA, en Sucre, Cauca"](https://archivo.contagioradio.com/asesinado-jairo-chilito-profesor-de-asoinca/)

###### Reciba toda la información de Contagio Radio en [[su correo]
