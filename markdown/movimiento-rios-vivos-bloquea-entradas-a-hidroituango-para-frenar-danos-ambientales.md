Title: Movimiento Rios Vivos bloquea entradas a HidroItuango para frenar daños ambientales
Date: 2015-09-04 12:12
Category: Ambiente, Economía, Movilización, Nacional
Tags: Autoridad Nacional de Licencias Ambientales, Cumbre Agraria Campesina Étnica y Popular, Empresas Públicas de Medellín, Gobernación de Antioquia, Hidroituango, Movimiento Ríos Vivos, política minero energética, Represa Ituango
Slug: movimiento-rios-vivos-bloquea-entradas-a-hidroituango-para-frenar-danos-ambientales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/Ituango.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: archivo Movimiento Ríos Vivos] 

<iframe src="http://www.ivoox.com/player_ek_7906091_2_1.html?data=mJ6dmJWddY6ZmKiak5eJd6KolJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRkdDqytLWx9PYs4zGytTgjbvNutDnjMfZ0dbZqcKfxtPh1MbIpdSfwpC1y8nWs6ro1sbbydSPtI6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Isabel Cristina Zuleta, Mov Ríos Vivos] 

###### [4 Sept 2015] 

[Desde el pasado lunes 31 de agosto, afectados por el proyecto Hidroituango bloquean actividades de la represa y adelantan bloqueos en Briceño, Ituango, Toledo, Puerto Valdivia y el Aro en Antioquia, para exigir el cumplimiento de los acuerdos pactados para hacer frente a las **afectaciones sociales y ambientales provocadas por la construcción de la hidroeléctrica desde el año 2009**.]

Isabel Cristina Zuleta, vocera del Movimiento Ríos Vivos, asegura que la construcción de la hidroeléctrica ha modificado el caudal del rio Cauca, **destruyendo especies animales e impidiendo la subienda de peces, fuente económica de la región.** **Hectáreas de bosque húmedo tropical han sido taladas para la construcción de vías y túneles. Especies como iguanas y serpientes han tenido que migrar a las partes altas**, afectando los cultivos e impactando a los pobladores.

De estas afectaciones las **comunidades tienen pruebas, videos y testimonios**, no obstante EPM niega su responsabilidad e impide la entrada de organizaciones para el estudio de la situación, según explica Zuleta.

Entes gubernamentales, como la Autoridad Nacional de Licencias Ambientales ANLA, no han hecho más que agudizar la problemática, aprobando modificaciones a la Licencia que dio curso al proyecto y que posibilita mayor acaparamiento del rio por parte de EMP.

[El proyecto en manos de **Empresas Públicas de Medellín, EPM, con la gobernación de Antioquia como socia mayoritaria**, ha afectado directamente a cerca de 50.000 pobladores e indirectamente a 6.000, entre pescadores que han visto afectada su economía e integrantes de organizaciones a las que les han sido violados sus derechos humanos por cuenta de las acciones represivas de los militares que están en las bases instaladas en cercanías a la represa.]

[Se espera de las Jornadas de Indignación lideradas por la **Cumbre Agraria campesina, étnica y popular**, marco en el que se adelantan estos bloqueos y movilizaciones, resulten avances significativos en la política minero-energética en Colombia a fin de frenar estás problemáticas.     ]
