Title: Medidas de La Fiscalía para proteger Parques Naturales afecta a los pequeños campesinos
Date: 2020-02-26 13:21
Author: AdminContagio
Category: Ambiente, Entrevistas
Tags: campesinos, Fiscalía General de la Nación, La Macarena, Picachos
Slug: parques-naturales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/Parque-Nacional-Natural-Tinigua.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/02/WhatsApp-Image-2020-02-25-at-2.55.55-PM-1.jpeg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->  
<iframe id="audio_48357228" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_48357228_4_1.html?c1=ff6600"></iframe>  
<!-- /wp:html -->

<!-- wp:paragraph -->

Ante los graves incidentes sociales y ambientales evidenciados en los últimos días en los **Parques Naturales Tinigua y Los Picachos**, la Fiscalía General de la Nación solicitó medidas de protección y restricción para frenar la destrucción del bosque nativo.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/FiscaliaCol/status/1232274323274113028","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/FiscaliaCol/status/1232274323274113028

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

La solicitó se hizo ante los jueces de garantías de **Florencia y Cartagena del Cahirá en Caquetá**, quienes determinaron la prohibición de cualquier movimiento de reces, así como el establecimiento de campesinos en el territorio.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### ¿Cuáles son las medidas en los Parques Naturales?

<!-- /wp:heading -->

<!-- wp:paragraph -->

La primera medida señala que la Federación Colombiana de Ganaderos (**Fedegan**) y el Instituto Colombiano Agropecuario (ICA), **deberán suspender la movilización de ganado,** así como la expedición de bonos para el comercio de ganado en Los Picachos y Tinigua.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Asimismo los alcaldes de San Vicente del Caguán, La Macarena y La Uribe, *"no podrán emitir certificaciones de colonos ni autorizar la permanencia temporal de personas en los parques naturales"*, acción que intenta detener el **crecimiento poblacional y los asentamientos humanos en las zonas protegidas**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Determinó también, que el **Banco Agrario** no podrá aprobar créditos a quienes pretendan desarrollar proyectos agropecuarios en los parques de la Amazonía. (Le puede interesar: <https://archivo.contagioradio.com/persecucion-a-pobladores-de-la-macarena-en-desarrollo-de-operacion-artemisa/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En la audiencia también se establecieron medidas sobre 20 personas judicializadas, de estas **16 quedaron en libertad y cuatro con medida de detención domiciliaria** **bajo el cargo de invasión de zona protegida**, y que según el juez causó el daño a **1.800 hectáreas de selva** por el desarrollar de actividades agropecuarias.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### "¿Conservar para qué y para quién?"

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según Mercedes Leudo, docente de la universidad de la Amazonia, en este territorio "***es común encontrar esas políticas mal aplicadas**, como resultado tenemos el origen de muchos de los conflictos que tenemos ahora".* (Le puede interesar: <https://archivo.contagioradio.com/intereses-empresariales-los-principales-deforestadores-de-colombia/>)

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Y agregó, que el único problema no es la actividades ganadera, *"son los carteles de tierra, la ilegalidad manifestada en cultivos de uso ilicito, extorsión, robos, entre otras, entonces la pregunta es; ¿siento esto de conocimiento público**, por qué se adoptan medidas que afectan principalmente a los pequeños campesinos?**"*

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Leudo resaltó también que en la zona hay **alrededor 9 bloques petroleros** demarcados por la Agencia Nacional de Hidrocarburos, *"**en el Tapir hay un ramal a un oleoducto del pacificó que lleva ese nombre, y que hay traído múltiples afectaciones"**,* según la docente esta es más que una razón del porqué la gente se agrupó allí y posteriormente se dió un operativo militar.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> *"Nosotros bajo ningún punto de vista estamos de acuerdo con la deforestación, así como tampoco la ampliación de la frontera ganadera y agrícola, pero pensamos en la parte humanitaria; la gente que habita estos parques desde antes que existiera esta figura y que hoy es desplazada de sus hogares"*
>
> <cite> Mercedes Leudo, Universidad de la Amazonia </cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Finalmente señaló que el Gobierno debe hacer una distinción entre los habitantes que llevan generaciones allí, entre 30 y 40 años, y los colonos que han venido llegando, *"eso es tierra de nadie donde todo mundo llega, afectando las condiciones de por sí, ya precarios de los campesinos nativos, esto es lo que el Presidente debe atender, los más de 3.000 campesino que lo están perdiendo todo ".*

<!-- /wp:paragraph -->
