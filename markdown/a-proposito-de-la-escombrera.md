Title: A propósito de la Escombrera
Date: 2015-07-30 16:52
Category: Camilo, Opinion
Tags: desaparecidos colombia, Diego Fernando Murillo, General Mario Montoya, La escombrera, Medellin, operación orion
Slug: a-proposito-de-la-escombrera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/07/la-escombrera-medellin.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

#### [**[Camilo De Las Casas](https://archivo.contagioradio.com/camilo-de-las-casas/) -[~~@~~CamilodCasas](https://twitter.com/CamilodCasas) 

###### [30 Jul 2015] 

En la Escombrera, vertedero de basuras de Medellín, Diego Fernando Murillo, afirmó que por lo menos  300 restos de seres humanos entre ellos víctimas de la operación "Orión" allí fueron inhumados.

Las declaraciones del ex mando paramilitar cotejadas con otras fuentes, dan la certeza del enterramiento de decenas de personas víctimas de Estado en ese paraje de la ciudad antioqueña. Orión  fue una de las macabras actuaciones de inauguración de la "Seguridad Democrática", orientada desde la casa de Nari por el propio Uribe y en este caso, de su intimo amigo el General Mario Montoya, en ese momento al mando de la 4ta Brigada.

El pretexto de enfrentar a las milicias de las FARC y el ELN de la Comuna 13 justificó entre otras la desaparición de centenares de personas en una operación de 2002 en la que  "Don Berna", ni fue el planificador ni instigador, sino el ejecutor por una llamada y orden recibida del general Montoya, ¡así de simple! . La 4ta Brigada dotó de uniformes y demás apoyo logístico al grupo seleccionado por Murillo, quien ejecutó el plan diseñado por Montoya  con conocimiento de la sede presidencial, legitimada por el MinInterior, Fernando Londoño. En  la monstruosa operación criminal de Estado, el ente investigador de la Fiscalía se prestó para asegurar la "pulcritud" del crimen, haciendo ver la acción armada conforme al derecho.

La arquitectura  de la impunidad que se pretendió mantener por mucho tiempo se ha ido desmoronando parcialmente por el camino de dignidad en la memoria de las víctimas y sus familiares. El inicio de las exhumaciones en Medellín es un logro de las víctimas, de un grupo reducido de las Hermanas de las Madres Lauras y de la Corporación Jurídica Libertad, CJL. No ha existido acción alguna no emprendida por ellas, entre ellas, el abordar a Diego Murillo a la salida de un Tribunal en la ciudad de New York en donde estaba siendo juzgado o con pocos recursos lograr la presencia de integrantes de la  Comisión Ética y evidentemente en medio del terror una estrategia colectiva de memoria.

El caso de Orión, el protagonismos de las víctimas y la actitud del Estado, es la expresión simbólica y dramáticamente trágica de nuestra propia tragedia.

Hace dos días  voces se hicieron presentes allí, el MinInterior Juan Fernando Cristo y el alcalde de esa ciudad al inicio de las excavaciones, una de las mayores fosas comunes hasta ahora reconocidas. Sus expresiones reflejan al establecimiento y la actitud de la clase dirigente, una retórica propagandística oficial que no guarda una actitud de respeto y consideración digna a las víctimas. Un ritual de palabras distantes de las víctimas que demuestran el uso estratégico de los derechos de las víctimas para maquillar un ejercicio de Estado y de gobierno que por convicciones de economía global se ve obligado a reconocer que hay víctimas, que hay conflicto armado y ha tratar de pactar la paz con las guerrillas.  
La retórica se acompaña por una tímida decisión de asumir desde ya responsabilidades, pero también, por el hecho de que las excavaciones e identificación de las víctimas tendrá un costo mayor a 3 mil millones de pesos, que no están asegurados. Así de farisaica es la actitud,  mientras para La Habana gasta la delegación del gobierno solamente en viáticos diarios para cada uno de sus representantes 500 mil pesos o para la publicidad de su "Prosperidad Democrática" más que los 3 mil millones, para las víctimas no hay disposición presupuestal, hasta para eso se ingenió el techo fiscal.

Para las víctimas y lo social siempre se aduce la necesidad de la austeridad, los factores macroeconómicos, entre tanto, sus billeteras y bolsillos se llenan de dinero y sus circuitos de negocios se van traduciendo en enriquecimiento, tratando de no ser tan distantes de los empresarios poderosos. La retórica sobre las víctimas les sirve son parte de la contabilidad.

En La Habana aducen a su contraparte,  la guerrilla, que Santos y los gobiernos anteriores han hecho más de medio centenar de reconocimientos de responsabilidad, y seguramente lo harán con esta Escombrera. ¡Son indolentes y miserables! La razón muy pocos políticos no se han beneficiado de la violencia estatal y su engendro paraestatal.

Para algunos no es secreto los vínculos de familia del alcalde y el desplazamiento y despojo en Urabá y el bajo Atrato. Esa verdad es negada en la retórica oficial que compromete a políticos y empresarios, y sectores de la iglesia Católica.

Poco se sabe de  la verdad real, esa no es conveniente, es incómoda no hay disponibilidad política ni económica, existe una actitud negacionista y mal llamada ética basada en encubrir y en mentir.  Así ha sido y pretende ser, el tema es sensible en La Habana y en Colombia.

Este tiempo de las víctimas es el definitivo para torcer la impunidad jurídica, social y política. Eso explica la resistencia más que de los militares y paramilitares a reconocer la verdad y a buscar un mecanismo de justicia coherente con los derechos de las víctimas, de los políticos y empresarios, que definieron políticas y financiaron el terror de Estado. La Escombrera es una lección de dignidad ante un Estado insensible y proclive al olvido como uno de los mecanismos del sometimiento. Que vengan nuevas Escombreras para recordar que nuestro país, es un cementerio de fosas comunes, como lo expresó Piedad Córdoba y fue dilapidada, y desde esa memoria colectiva propiciar el nuevo país que nos merecemos.
