Title: Denuncian intimidaciones contra Daniel Prado, abogado de las víctimas en caso Santiago Uribe
Date: 2017-10-27 18:25
Category: DDHH, Nacional
Tags: Los 12 apostoles, Santiago uribe
Slug: denuncian-intimidaciones-contra-abogados-de-las-victimas-en-juicio-contra-santiago-uribe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/juiocio-contra-santiago-uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: semana] 

###### [27 Oct 2017]

Durante la audiencia que se realizaba este viernes en el Palacio de Justicia de Medellín y en la que se alista el proceso de juicio contra **Santiago Uribe Vélez** por el asesinato de Camilo Barrientos y más de 570 crímenes del grupo paramilitar los 12 Apóstoles, los **representantes de la parte civil denunciaron que personas que se encontraban en la sala intimidaron y hostigaron al abogado Daniel Prado.**

Según la denuncia que hicieron pública al reiniciarse la audiencia, una mujer se acercó al abogado Prado, quien le preguntó a ésta si se conocían y sí ese era el tipo de intimidaciones que hacían a las víctimas. La mujer le habría respondido que a eso había ido, "a verlo". Después, a la salida del juzgado, otro hombre los increpó en tono amenazante. [Lea también: 14 revelaciones de la resolución de acusación contra Santiago Uribe](https://archivo.contagioradio.com/las-revelaciones-de-la-resolucion-de-acusacion-contra-santiago-uribe-velez/)

Para los abogados apoderados de las víctimas este tipo de acciones son claras intimidaciones y pretenderían evitar el ejercicio de la defensa de las víctimas. Por ello, decidieron dejar constancia formal de los hechos ante el juez 1 especializado de Antioquia, quien aseguró que tramitará la denuncia. Además, se solicitó que el público asistente a la audiencia guarde el decoro y el respeto por los sujetos procesales. [Lea también: Los doce apóstoles habrían cometido más de 570 crímenes.](https://archivo.contagioradio.com/santiago_uribe_homicidios_12_apostoles/)

Los abogados anunciaron que acudirán ante instancias internacionales para evitar que el proceso se afecte por este tipo de acciones. Para los representantes de las víctimas es necesario que **este tipo de procesos ofrezca todas las garantías para satisfacer plenamente los derechos de las víctimas.**

Algunos portales como “Las dos Orillas” han señalado que varios de los integrantes del grupo paramilitar Los Doce Apóstoles o allegados a sus actividades, que podrían ser determinantes a la hora del juicio, han muerto o han sido asesinados y citan siete casos: “alias “El Relojero, los hermanos Múnera, Hernán Darío Zapata y los Pemberthy, sicarios de los 12 apóstoles.

### **¿Qué ha pasado durante la audiencia?** 

El juez a cargo del proceso se ha referido a las razones de por las cuales no aceptó el 90% de las pruebas solicitadas por la defensa del acusado. Según lo que se ha podido establecer, la Defensa de Uribe está solicitando pruebas que ya hacen parte del proceso y repetirlas podría significar una acción de dilación.

###### Reciba toda la información de Contagio Radio en [[su correo]
