Title: La JUCO denuncia nuevo caso de persecución a sus integrantes
Date: 2016-03-29 13:27
Category: Nacional
Tags: Ivanovich Jiménez, juco, Persecución a defensores de derechos humanos
Slug: la-juco-denuncia-persistente-persecucion-e-intimidacion-a-sus-integrantes
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/11154935_1202593143102259_2013383777673385278_o-e1459275409810.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### Foto: JUCO Bogotá 

##### [29 Mar 2016] 

Desde la Juventud Comunista JUCO se denuncia que persisten las acciones de hostigamiento y persecución a sus integrantes, en esta oportunidad contra Ivanovich Jiménez, Responsable Nacional de Educación, quien la mañana de este martes habría sido abordado e intimidado por motorizados que se hacían pasar como miembros de la SIJIN.

De acuerdo con lo expresado por la organización, los hombres nunca presentaron algún tipo de identificación que corroborara su calidad de agentes de inteligencia estatal, y procedieron a requisar sus pertenencias, llevándose consigo algunos apuntes y textos políticos que portaba cuando se encontraba en las inmediaciones de la Universidad Distrital de Bogotá, sede La Macarena.

En la denuncia, elevada a instancias tanto nacionales como internacionales, se responsabiliza de las acciones a "fuerzas oscuras" que pretenden "silenciar el nivel de descontento que existe en el país", a la vez que se exige del presidente Juan Manuel Santos algún tipo de pronunciamiento al respecto y compromiso "con el fin de la persecución, al paramilitarismo y al terror estatal" en particular[ contra las organizaciones sociales, populares, democráticas y revolucionarias](https://archivo.contagioradio.com/persecucion-politica-y-campana-somos-dignidad-contagio-radio/)en Colombia.

 
