Title: Tres propuestas para respirar mejor en Bogotá
Date: 2019-04-01 20:34
Author: CtgAdm
Category: Ambiente, Entrevistas
Tags: Calidad del aire, Transmilenio
Slug: tres-propuestas-para-respirar-mejor-en-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-50.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

La semana pasada la Alcaldía de Bogotá declaró la tercera alerta amarilla este año por la mala calidad de aire, situación que indica para muchos ciudadanos que las medidas que se ha tomado por la administración son inefectivas e insuficientes. Al respecto, Camilo Prieto, vocero del Movimiento Ambientalista Colombiano, aportó dos propuestas para mitigar los efectos de la crisis actual y una para prevenir su prolongación en el futuro.

Por un lado, el vocero señala que el pico y placa que impuso la Secretaría de Ambiente no debió aplicarse exclusivamente a vehículos de carga de más de dos toneladas sino a **vehículos fabricados antes de cierto año** ya que estos manejan una tecnología anticuada y más contaminante. Este lunes 1 de abril la alerta amarilla persiste en la mayoría de la ciudad excepto en el suroccidente, donde sigue prendida la alerta naranja.

Como otra medida temporal, Prieto también propone el uso de la **mascarilla N-95**, de entre 4.000 y 10.000 pesos, en los buses de Transmilenio y SITP para mitigar los efectos de las emisiones para los usuarios del transporte público. "Hay que recordar cuando un viaja en un bus de Transmilenio, respira **ocho veces más** emisiones contaminantes que una persona que está por fuera del bus y una persona que viaja por dentro del SITP, respira **11 veces más** emisiones contaminantes", indicó el ambientalista.

Además, una persona que viaja 70 minutos en Transmilenio respira emisiones contaminantes **1,25 veces superiores a lo que la Organización Mundial de la Salud considera normal en 24 horas. **(Le puede interesar: "[Peñalosa no atiende recomendaciones académicas sobre calidad del aire en Bogotá](https://archivo.contagioradio.com/penalosa-no-atiende-recomendaciones-academicas-calidad-del-aire-bogota/)")

Para mediano y largo plazo, se recomienda que la Alcaldía de Bogotá impulse tecnologías limpias como los vehículos híbridos y eléctricos. "En eso estamos todavía muy colgados en Colombia y se tiene con urgencia comenzar a pensar en ello", manifestó Prieto, que además indica que esta tecnología se debería implementar en todo el país ya que el deterioro del aire se manifiesta en todas las regiones. En **76%** de las estaciones de mediciones de calidad se registran señales de deterioro del aire.

Ante las recientes declaraciones del Secretario de Ambiente en Bogotá, Francisco Cruz, que la nueva flota de Transmilenio ayudaría a reducir los niveles de contaminación de la ciudad, el ambientalista afirmó que esto no afectaría sustancialmente las emisiones de dióxido de nitrógeno, las cuales generan cáncer de pulmón. Justamente, este riesgo para la salud es una de las razones por las cuales que la flota que compró el Distrito no cumple con estándares establecidos en Europa y no circulan en ese continente, explicó Prieto.

"El Distrito perdió una oportunidad histórica para iniciar la transición hacia energías limpias en Bogotá. Nos tendríamos que acostumbrar a estos buses porque lamentablemente la licitación ya salió adelante. Lo que esperamos ahora es que por lo menos el riesgo se reduzca para los pasajeros de esos buses", afirmó.

Además de recomendar el uso de tapabocas N-95, el vocero indicó que para este año electoral, la ciudadanía debería conocer las propuestas de los candidatos a la Alcaldía de Bogotá en temas de ambiente y movilidad.  A propósito, el Movimiento Ambientalista Colombiano iniciará una serie de entrevistas, a través de Facebook Live, con los candidatos. Hoy será la primera sesión con Claudia López a las 6:30 de la tarde.

<iframe id="audio_33950327" src="https://co.ivoox.com/es/player_ej_33950327_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
