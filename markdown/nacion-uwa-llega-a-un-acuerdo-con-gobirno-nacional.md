Title: Nación U´wa llega a un acuerdo con Gobierno Nacional
Date: 2016-07-28 12:23
Category: Nacional
Tags: Indígenas U´wa, Toledo
Slug: nacion-uwa-llega-a-un-acuerdo-con-gobirno-nacional
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/07/uwa1-e1473281630152.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:RadioMacondo] 

###### [28 de Jul] 

Después de más de **60 días de toma por parte de la Nación U´wa a la sede de Ecopetrol** en Toledo, el gobierno finalmente llego a un acuerdo con la comunidad en la que los indígenas **reafirmaron que permanecerán en el territorio, debido a que estos son parte de los predios de propiedad colectiva y han estado bajo su control ancestralmente.**

La reunión entre el gobierno y los voceros de la comunidad se prolongo durante doce horas en donde finalmente,  el gobierno se comprometió a través de[un acta a proteger y suspender la actividad turística en el Parque Nacional Natural el Cocuy](https://archivo.contagioradio.com/indigenas-uwa-se-toman-planta-de-ecopetrol-antes-incumplimientos-del-gobierno/), territorio sagrado para la Nación U´wa,  generar una **asignación presupuestal para el saneamiento en el área** de superposición entre el Parque Nacional y el resguardo Uniddo U´wa y **brindar maquinaria para la construcción de vías al interior del municipio**.

Sobre a los títulos coloniales, los voceros reiteraron su posición frente a que [es una obligación del gobierno presentarlos y entregarlos a las comunidades, por ende](https://archivo.contagioradio.com/quien-se-tiene-que-ir-es-ecopetrol-no-nosotros-indigenas-uwa/), la Nación U´wa se acogió al espacio que se está realizando con los demás pueblos indígenas para la construcción del decreto que regula el proceso de reconocimiento de los títulos coloniales.

De acuerdo con la abogada Aura Tegria, miembro y defensora de la comunidad especificó que durante la reunión aclararon  al gobierno y Ecopetrol que deben "**respetar el territorio y así mismo protegerlo de cualquier proyecto mineroenergético o de cualquier licencia ambiental**".

Uno de los temas que aún  no se ha tratado es el de las **bases militares** que se encuentran en el territorio indígena de los U´wa y que para la abogada, "es necesario buscar el **mecanismos de cómo se retira ese tipo de presencia militar porque afecta culturalmente y espiritualmente a la población generando intranquilidad"**.

El día de hoy, desde las dos de la tarde, las diferentes **vocerías se reunirán con la comunidad para socializar los acuerdos a los que se llego con el gobierno** y posteriormenete, suspender la acción colectiva pacífica en presencia de la Defensoría del Pueblo. El próximo 4 de agosto se reunirá en Bogotá la masa étnica en compañía de las entidades garantes para continuar el diálogo con más detalle para los avances y el cumplimiento de los acuerdos pactados.

<iframe src="http://co.ivoox.com/es/player_ej_12367740_2_1.html?data=kpegmJybeJGhhpywj5aVaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5yncaLp08aYtsrLtsrVhpewjabGs8jVxcaYj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en[ [su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]

 
