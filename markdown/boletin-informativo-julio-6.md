Title: Boletín informativo julio 6
Date: 2015-07-06 17:52
Category: datos
Tags: Corte suprema liberaría a Plazas Vega, Entrevista Humberto de la Calle, Gobernador debería detener llenado de "El Quimbo", Ultimatum a proceso de paz
Slug: boletin-informativo-julio-6
Status: published

[*Noticias del Día: *]

<iframe src="http://www.ivoox.com/player_ek_4729505_2_1.html?data=lZyfm5qUeY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRhtDgxtmSpZiJhaXijK7byNTWscLoytvcjcnJb6vpzc7cjZuRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

-Ultimatum del gobierno al proceso de paz obedece a **intereses** de sectores que siempre se han opuesto y a los que el gobierno les sigue otorgando espacios. Para Carlos Lozano, analista político, es evidente que detrás del mal momento que atraviesan las negociaciones, están sectores de **derecha** y de **ultra derecha** que ven amenazados sus intereses cuando se plantean temas como la verdad y la justicia para todos los actores de la guerra.

-La Corte Suprema de Justicia estaría pensando en **absolver al Coronel (r) Alfonso Plazas Vega**, lo que implicaría que continúe la impunidad como ya se ha hecho durante 30 años, según afirma René Guarín, familiar de una de las personas desaparecidas en la retoma del **Palacio de Justicia**.

-Gobernador del huila deberia parar el llenado de la represa "**El Quimbo**", acción en la que se han omitido los informes de la ANLA y la Contraloría donde se evidencian las múltiples irregularidades y riesgos que representan este proyecto hidroelectrico. Habla Miller Dussán investigador de ASOQUIMBO.
