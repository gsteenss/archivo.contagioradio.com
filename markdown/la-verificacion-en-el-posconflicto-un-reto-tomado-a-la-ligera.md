Title: La verificación en el posconflicto: un reto tomado a la ligera
Date: 2015-12-14 08:31
Category: Ana Maria, Opinion
Tags: posconflicto, proceso de paz, Santos
Slug: la-verificacion-en-el-posconflicto-un-reto-tomado-a-la-ligera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/presidente-santos-paz1.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: SIG] 

#### **[Ana María Ribón](https://archivo.contagioradio.com/ana-maria-ribon/) - [~~@~~AnaMariaRibon](https://twitter.com/AnaMariaRibon)

###### [14 de Dic 2015 ] 

[**Un acuerdo entre el Gobierno y las FARC parece inminente, de ahí que estemos discutiendo mecanismos de refrendación**, que el presidente Santos esté de gira por Europa haciendo lobby en busca de una verificación del cese bilateral por parte de las Naciones Unidas. Sin embargo, ante el hecho de que aparentemente ya no hay vuelta atrás, y lo atragantada que me siento con tanto preparativo para la fiesta, mi inquietud es ¿qué es lo que vamos a celebrar?]

[Las experiencias internacionales de posconflicto han evidenciado que es un proceso absolutamente complejo, de preparativos inimaginables y de un sinnúmero de elementos a considerar, que este gobierno, en su afán por lograr una foto que circule por medios internacionales y que se adorne con titulares que desborden con elogios la labor de Santos y su equipo, está agilizando con evidente irresponsabilidad. Es un fenómeno que Robert K. Merton llamó “la imperiosa inmediatez del interés”, y que consiste en el afán de un gobierno por satisfacer necesidades inmediatas sin contemplar los impactos a largo plazo.]

[Parece que todos hemos sido víctimas de ese “síndrome de afán” con los ojos puestos en una paz, que buscada a la ligera, no vislumbra efectos prometedores.]

[Ahora bien, el gobierno está empeñado en lograr la aprobación unánime por parte del **Consejo de Seguridad de las Naciones Unidas** para el envío de una Fuerza De Paz que verifique el cese bilateral en territorio colombiano.]

[¿Se ha preguntado usted lo que sería necesario para un proceso de verificación? Independientemente de quien la lleve a cabo, la infraestructura e ingeniería al rededor de esto requiere el estudio y previsión de una serie de elementos que van desde el más inocuo hasta el más complejo, pero que en su totalidad, constituye la INFRAESTRUCTURA GENERAL PARA LA VERIFICACIÓN. La llegada de civiles y militares verificadores internacionales, sus gastos diarios, su sistema de salud, su contrato de indemnización, los carros, los helicópteros, la gasolina para estos, las zonas de concentración, la alimentación para quienes estarán en concentración, para quienes vienen a verificar, la determinación del tiempo que durará el proceso y otra serie de interrogantes me hacen pensar que esto no puede estar avanzando sin que un país reclame ser informado, esto sencillamente no puede estar avanzando sin nuestro consentimiento por la sencilla razón de seremos los receptores del posconflicto, sus consecuencias y desafíos.]

[Lo anterior me hace preguntarme dos cosas, o es el gobierno absolutamente ignorante y no está tomando en cuenta todo este conglomerado de interrogantes, o todo ha acordado previamente con las FARC y de ahí el afán por buscar la aprobación para iniciar con la verificación.]

[Ambos escenarios son gravísimos y ameritan nuestra atención. En el primer caso, resulta altamente irresponsable que un gobierno apresure una aprobación sin una planeación que tenga en cuenta tantos procesos. El segundo escenario, es igualmente preocupante, pues significaría que sin ningún tipo de consulta o planeación integral que tenga en cuenta la postura de diferentes sectores del país, se haya acordado algo tan definitivo y que involucrará tantos aspectos, y todo por el desmedido afán y la sed de inmediatez de este gobierno.]

[Mi berrinche solo pretende tener un efecto: generar que usted también haga el berrinche y que juntemos nuestros berrinches para pedirle a este gobierno que deje tanta ligereza, que nos consulte, o que nos informe, ¡o que haga algo con nosotros!]
