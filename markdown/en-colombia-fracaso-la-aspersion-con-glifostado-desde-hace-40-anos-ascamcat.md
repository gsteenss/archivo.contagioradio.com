Title: En Colombia fracasó la aspersión con Glifostado desde hace 40 años: ASCAMCAT
Date: 2018-09-14 16:08
Author: AdminContagio
Category: Entrevistas, Nacional
Tags: Catatumbo, Glifosato, Juan Carlos Quintero, Sustitución de cultivos de uso ilícito
Slug: en-colombia-fracaso-la-aspersion-con-glifostado-desde-hace-40-anos-ascamcat
Status: published

###### [Foto:Revista Semana] 

###### [14 Sep 2018] 

Organizaciones sociales enviaron un mensaje de urgencia al gobierno de Iván Duque para que no permita que retorne la aspersión aérea con glifosato, **ya que consideran que la medida ha fracasado desde que se utilizó**. Además, le recordaron que ya hay unos Acuerdos de Paz y pactos con las comunidades en donde se concertaron programas de sustitución voluntaria, algunos de ellos ya en marcha.

Para Juan Carlos Quintero, vocero de la Asociación  Campesina del Catatumbo, esta decisión "muestra una flagrante violación al Acuerdo de paz, firmado a la luz de la comunidad internacional, una violación a la buena fe de cientos de familias cultivadora de Coca, Amapola y Marihuana, y **un favor a las grandes redes de lavados de activo y narcotráfico"**.

Debido a que según Quintero, esta política se ha implementado desde hace 40 años y nunca ha demostrado combatir efectivamente los cultivos de uso ilícito, el narcotráfico y acabar con los carteles de la droga en el país. (Le puede interesar: ["Aspersión terrestre con glisofato es absurda" Camilo González Posso"](https://archivo.contagioradio.com/aspersion-terrestre-con-glifosato-es-absurda-camilo-gonzalez-posso/))

### **Las Afectaciones a las las comunidades** 

Quintero narró las consecuencias de la aspersión con glifosato que vivieron tanto él como su comunidad antes de que la Corte Constitucional emitiera un concepto negativo sobre esta medida. La primera de ella fue el desplazamiento de campesinos que migraron a otros lugares del país, cuando se produjeron las primeras fumigaciones, que posteriormente continuaron con la siembra de coca en otros territorios.

Referente a la salud, el vocero de ASCAMCAT afirmó que se presentaron casos de personas que presentaron perdida del cabello en zonas c**omo la cabeza y las cejas, aumento de abortos espontáneos**. En cuanto al ambiente, Quintero señaló que el glifosato fue vertido en el río Catatumbo cuando no podía ser usado en las fumigaciones y muchas veces se regaron cultivos de pancoger.

<iframe id="audio_28590228" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_28590228_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
