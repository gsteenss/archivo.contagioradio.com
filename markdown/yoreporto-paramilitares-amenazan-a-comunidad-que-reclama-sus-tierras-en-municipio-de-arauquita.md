Title: #YoReporto Paramilitares amenazan a comunidad que reclama sus tierras en municipio de Arauquita
Date: 2015-02-17 22:42
Author: CtgAdm
Category: Nacional, yoreporto
Slug: yoreporto-paramilitares-amenazan-a-comunidad-que-reclama-sus-tierras-en-municipio-de-arauquita
Status: published

###### Foto: [cineminga-es.blogspot.com]

Cansados de padecer sufrimientos y desarraigo por más de dos décadas, 54 familias del municipio de Arauquita, desplazadas por el Estado colombiano y la transnacional OXI para adelantar explotación petrolera en Caño Limón, desde hace año y medio han decidido retomar sus tierras y reconstruir sus proyectos de vida individuales y colectivos.

Durante este tiempo, estas familias han recibido constantemente estigmatización, amenazas de desalojo y daños a sus cultivos de parte del Estado al servicio de la transnacional a través de la Policía Nacional, sin embargo los campesinos se han resistido a abandonar nuevamente sus tierras.

Un nuevo hecho que generó zozobra en esta comunidad, fue la aparición de un letrero alusivo a las AUC dentro de los terrenos retomados por los labriegos, que queda ubicado dentro del complejo petrolero de Caño Limón de control de la OXY, área completamente militarizada y monitoreada permanentemente a través de dispositivos electrónicos.

Ya antes se había presentado una situación de amenaza contra el Personero Municipal Dr. Jesús Hernando Mancera Niño, que en cumplimiento de sus funciones ha estado acompañando a esta comunidad en su justa lucha y velando por el respeto de la Constitución y la Ley, y quien en el mes de agosto de 2014, a eso de las 6:00 pm., al salir de la oficina de la personería en el Centro de Convivencia del municipio de Arauquita, fue abordado por tres (3) hombres quienes se movilizaban en una camioneta blanca doble cabina y vestidos de manera refinada e impecable, le advirtieron “usted no tiene ni la menor idea del poder que nosotros tenemos, cumpla con su función hasta donde le corresponda y no vaya más allá, porque si no, nosotros nos veremos obligados a ir más allá”, cuestión que fue puesta por el agente del ministerio pública en conocimiento del Mayor Cuellar.

Corporación de Derechos Humanos Joel Sierra para la sección Yo Reporto
