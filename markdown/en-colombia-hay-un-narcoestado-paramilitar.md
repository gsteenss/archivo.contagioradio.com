Title: En Colombia no hay postconflicto, en Colombia hay un Narcoestado Paramilitar.
Date: 2020-03-06 16:47
Author: AdminContagio
Category: Columnistas invitados, Opinion
Tags: narcoestado, Narcoparamilitares, narcotrafico, Narcotráfico en Colombia, paramilitares en Colombia, paramilitarismo en Colombia
Slug: en-colombia-hay-un-narcoestado-paramilitar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/20191123_181710.mp4.00_00_04_15.Imagen-fija001.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"level":6} -->

###### **Por: Itayosara Rojas**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Esta semana ha sido asesinada otra firmante del acuerdo de paz en Bogotá, hacía dos semanas presenciamos como los miembros de un **ETCR** completo debían desplazarse forzadamente al no contar con garantías de seguridad en su espacio territorial. El informe de derechos humanos presentado por el relator de Naciones Unidas Michel Frost registra 108 asesinatos de defensores de derechos humanos, produciéndose un aumento del 50 % en la cifra respecto al 2018.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De acuerdo con los numerales 45,46,47,48,49,50,51,52,53,54,55,56,57,58 y 59 de este informe, las fuerzas militares colombianas incurrieron en violaciones serias del derecho internacional humanitario y violaciones de derechos humanos, tales como el asesinato de menores de edad en áreas rurales, tortura, detenciones ilegales de campesinos, asesinato de persona protegida y 15 privaciones arbitrarias de la libertad. Así mismo, el informe establece que durante el 2019 se presentaron 36 masacres en Colombia, que implicaron el asesinato de 133 personas siendo esta cifra la más alta desde el 2014.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### El informe ha sido cuestionado por el gobierno colombiano, quien inclusive torpedeó su elaboración y presentación. 

<!-- /wp:heading -->

<!-- wp:paragraph -->

La respuesta del gobierno fue el rechazo al informe mediante declaraciones desobligantes y negligentes por parte de sus ministros y representantes. Además, exactamente el mismo mes en el que el informe fue presentado la prensa registró el hallazgo de un laboratorio de procesamiento de cocaína en la finca del embajador del gobierno colombiano en Uruguay. Parece muy afortunado el embajador puesto que la droga incautada durante el operativo judicial no aparece y la policía aún no responde por algo que está bajo su responsabilidad.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las autoridades no habían terminado de esclarecer la cantidad real de droga hallada debido a la capacidad de procesamiento del laboratorio, cuando el país ya se sacudía con el escándalo sobre la compra de votos y cooperación directa entre el hoy presidente de Colombia Iván Duque y el ganadero Ñeñe Hernández. El Ñeñe fue asesinado el año pasado al parecer por sostener negocios y transacciones con Peco González, miembro del cartel de la Guajira, y el narcotraficante Marcos Figueroa. El apoyo de Hernández a Duque y al uribismo fue tan evidente que hasta participaron en la posesión presidencial el 7 de agosto de 2018.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El gobierno de Iván Duque, su partido político y quien es el máximo líder del Uribismo: Álvaro Uribe Vélez, parecen tener vínculos reales con poderes mafiosos regionales, como lo evidencia el hallazgo del laboratorio de coca en la finca del embajador, o las interceptaciones telefónicas realizadas por la justicia en las que se demuestra la compra de votos que beneficiaron la elección de Duque. Mientras tanto, el país se desangra con el asesinato de líderes sociales, de firmantes del acuerdo y un proceso tortuoso y lento de la implementación de un acuerdo de paz desfalleciente. 

<!-- /wp:paragraph -->

<!-- wp:heading {"level":5} -->

##### Es mentira que en Colombia exista un postconflicto, es mentira que exista voluntad para la implementación del acuerdo de paz.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Las elites mafiosas que gobiernan con Duque y el uribismo no están interesadas en ejecutar la reforma rural integral, ni mucho menos acabar con los cultivos ilícitos; por el contrario, facilitan la apertura de frentes de economía ilegal en las regiones donde avanza la tala y quema de bosques para que entren vacas mientras la tierra queda en manos de ganaderos y terratenientes. En lugar de apoyar la sustitución concertada de cultivos de uso ilícito, que ha mostrado mejores resultados en la reducción de cultivos, el gobierno prefiere generar incentivos  directos para incrementar el precio de la coca a través de medidas como la aspersión aérea con glifosato, estrategia que ha demostrado tener un efecto directo sobre el precio de la coca.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así las cosas, nada parece estar más alejado de la realidad colombiana que la categoría de postconflicto, usada tanto en la academia como en las agencias de cooperación que llevan a cabo intervenciones en el país tras la firma del acuerdo de paz. Es necesario reemplazar esta categoría, pues las masacres, los asesinatos y las violaciones de derechos humanos en general, son una muestra fehaciente de que no existe un postconflicto, por el contrario, se ha terminado de consolidar el poder mafioso, terrateniente y paramilitar que empezó a gestarse desde 1980.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las cosas por su nombre, Colombia no vive un postconflicto, eso que las agencias de cooperación ven con preocupación tiene un nombre: **Narcoestado Paramilitar**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Le puede interesar: [Nuestros columnistas](https://archivo.contagioradio.com/opinion/columnistas/)

<!-- /wp:paragraph -->
