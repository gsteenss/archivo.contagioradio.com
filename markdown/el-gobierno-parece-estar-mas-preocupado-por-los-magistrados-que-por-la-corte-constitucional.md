Title: “El gobierno parece estar más preocupado por los magistrados que por la Corte Constitucional”
Date: 2015-03-25 17:04
Author: CtgAdm
Category: Entrevistas, Política
Tags: Alfredo Beltran, Corte Constitucional, Crisis de la justicia, Magistrado Prettelt, ramiro bejarano
Slug: el-gobierno-parece-estar-mas-preocupado-por-los-magistrados-que-por-la-corte-constitucional
Status: published

###### Foto: eluniversal.com 

<iframe src="http://www.ivoox.com/player_ek_4263561_2_1.html?data=lZejlZqadY6ZmKiakpuJd6KplpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaZOmrcaYy9PIqdHZz8nS0MjNpYzYxpDZ0diPrtbZxMrgjcrXb8Tpxtjhy4qnd4a2lNOYxsqPp8Lmhqigh6aVp9Whhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alfredo Beltrán Ex magistrado de Corte Constitucioinal]  
<iframe src="http://www.ivoox.com/player_ek_4263516_2_1.html?data=lZejlZqVeo6ZmKiakpmJd6KplJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaamhp2dh56nic2fyNTPy8rWstCf0cbfx8jJb8bn1cbfjdKJh5SZopbgjdXWqdDX1tXOxtSPtNDmjNHc1ZDRpcihhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Ramiro Bejarano, ex directo del DAS] 

Todas las decisiones que el gobierno anuncia son producto de leyes futuras, todas las reformas propuestas tendrían una salida a 4 meses en el mejor de los casos, señala **Ramiro Bejarano**, quién promueve la revocatoria de los magistrados de las altas cortes. Para Bejarano al gobierno no le interesa la revocatoria porque puede creer que se puede generar una crisis mayor o que **Nestor Humberto Martínez** estaría aspirando a ser fiscal general de la nación.

Una de las salidas reales puede ser decretar un estado de conmoción interior en que los propios magistrados pudieran sancionar a sus pares. Bejarano también llama la atención sobre el hecho de que los propios **magistrados no tienen un juez natural** y esa situación propicia acontecimientos de corrupción como los actuales.

Sobre el tribunal de aforados, Bejarano señala que se tiene más competencia porque puede investigar y acusar, sin **embargo la decisión final sigue quedando en el congreso.**

El ex magistrado de la Corte Constitucional Alfredo Beltrán señala que la crisis actual debe ir más allá de buscar salidas a la coyuntura, sin embargo afirma que hay un acierto en la alocución del presidente Santos cuando resalta que hay personas contra las que se debería actuar y no contra toda la Corte Constitucional. **"Hay que distinguir la legislación vigente, la reforma en curso y el caso coyuntural"**

También rescata el Ex magistrado que los magistrados deben tener unas calidades profesionales y éticas **"La independencia de los jueces es cuestión de carácter y la honorabilidad de ética"**.
