Title: ¿Por qué renuncia el General Arias Cabrales a la Jurisdicción Especial de Paz?
Date: 2017-04-24 17:21
Category: Judicial, Nacional
Tags: FARC, Jesus Armando Arias Cabrales, Jurisdicción Espacial de Paz, Palacio de Justicia
Slug: general-arias-cabrales-jurisdiccion-especial-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/arias_cabrales_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: elespectador] 

###### [24 Abr 2017]

Este lunes se conoció una carta enviada por el General ® Jesús Armando Arias Cabrales dirigida a la secretaría general de la Jurisdicción Especial de Paz, en cabeza de Nestor Raúl Correa, en que el general sentenciado por la desaparición de 6 personas durante la retoma del Palacio de Justicia, **renuncia a la posibilidad de que su caso sea acogido por ese tribunal**.

En una solicitud enviada el pasado 30 de Marzo el general expresaba querer ser acogido por la JEP, una decisión que para algunas víctimas podría significar acceso a la verdad, aunque Cabrales se hubiera negado a ella durante todo el proceso judicial que concluyó en Abril de 2011 con una sentencia cercana a los 30 años de prisión. Sin embargo, Arias Cabrales, afirmó que seguirá en espera del recurso de casación ante la CSJ.

\[caption id="attachment\_39619" align="aligncenter" width="464"\]![tomada de El Tiempo](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/04/carta-arias-cabrales.jpeg){.wp-image-39619 width="464" height="348"} tomada de El Tiempo\[/caption\]

### **El proceso judicial y la ausencia de verdad** 

El general, sentenciado en primera instancia, por la desaparición forzada de Carlos Augusto Rodríguez Vera, Bernardo Beltrán Luz Mary Portela, David Suspes e Irma Franco apeló, sin embargo la decisión fue ratificada por el **Tribunal Superior del Distrito Judicial de Bogotá en Octubre de 2014.**

En este momento el General retirado se encuentra a la espera de la decisión de casación interpuesta ante la CSJ. Sin embargo, uno de los abogados de los y las familiares de las víctimas del Palacio de Justicia afirma que hay un antecedente preocupante y es la **absolución del Coronel ® Luis Alfonso Plazas Vega, que después de presentar ese mismo recurso se encuentra en libertad. **([Lea también: Se reafirma la impunidad con absolución de Plazas Vega](https://archivo.contagioradio.com/se-reafirma-la-impunidad-en-caso-del-palacio-de-justicia/))

### **Pugna entre JEP y altas cortes** 

Si Arias Cabrales fuera absuelto por la CSJ se pondrían en evidencia las pugnas que implican la puesta en marcha de los tribunales establecidos por el acuerdo de paz entre las FARC – EP y el gobierno de Colombia, y los **poderes judiciales tradicionales como la CSJ y el Consejo de Estado** que han manifestado reiteradamente sus reparos a la JEP.

Según algunos analistas la JEP estaría desplazándolos y pasando por encima del ordenamiento jurídico. Sin emabargo para las víctimas y organizaciones de Derechos Humanos, el tribunal podría significar un golpe a la impunidad, el acceso a la verdad y la aplicación de la **Justicia Restaurativa que tiene como centro la reparación del daño y no solamente la carcel**. ([Lea también: familiares del Palacio de Justicia seguirán exigiendo verdad y justicia](https://archivo.contagioradio.com/familiares-de-las-victimas-del-palacio-de-justicia-seguiran-exigiendo-justicia/))

Mientras que los asesores jurídicos del proceso de paz han manifestado que tanto la JEP, como el SIVJRNR tienen como centro la verdad y las víctimas, la **absolución de Plazas Vega por parte de la CSJ ha significado una gran retroceso en materia de verdad y justicia** para las víctimas del Palacio de Justicia, afirmó en su momento uno de los integrantes del grupo de familiares.

###### Reciba toda la información de Contagio Radio en [[su correo]
