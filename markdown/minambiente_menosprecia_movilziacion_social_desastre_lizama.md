Title: Minambiente "menosprecia" plantón ante desastre ambiental en la Lizama
Date: 2018-03-27 18:03
Category: Ambiente, Nacional
Tags: ANLA, derrame de petróleo, Ecopetrol, Lizama, Ministerio de Ambiente
Slug: minambiente_menosprecia_movilziacion_social_desastre_lizama
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/luis_g_murillo_1_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Minambiente] 

###### [27 Mar 2018] 

Ante el plantón realizado por la ciudadanía y organizaciones ambientalistas frente al Ministerio de Ambiente, exigiendo medidas realmente eficaces para controlar tal tragedia ambiental en la Lizama, Santander,  la respuesta del ministro Luis Gilberto Murillo, parece no ser la adecuada para la situación, como lo han asegurado los ambientalistas. El jefe de esa cartera le dice a quienes se manifestaron que le es muy fácil protestar "desde los cómodos sitios de Bogotá, cuando están sentado en los cómodos cafés de Bogotá mandando trinos".

Ante dicha respuesta, los organizadores del plantón rechazan "tajantemente las declaraciones en las que Ministro de Ambiente menosprecia la movilización social, y la indignación de la ciudadanía", señala Natalia Parra, directora de la Plataforma ALTO y una de las personas que hizo parte del plantón de este lunes.

<iframe src="https://co.ivoox.com/es/player_ek_24859217_2_1.html?data=k5mll56WdZihhpywj5WVaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5ynca_V1cbZy8aPlMLm08aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Pos su parte, otro de los participantes, Juan Camilo Caicedo, asegura que de no ser por los trinos y los plantones a los que se refiere el Ministro, este no hubiera hecho presencia en la Lizama, ya que lo hizo 15 días después de que iniciará el derrame. "Si los ciudadanos no salen a la calle, sino hubiera manifestaciones en redes sociales y no se hubiera exigido la renuncia del ministro, este seguiría pasando de agache junto a Ecopetrol y la ANLA".

<iframe src="https://co.ivoox.com/es/player_ek_24859232_2_1.html?data=k5mll56Wd5Ohhpywj5WUaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5yncavpwtOYpcbRrc3jjKjOy8jJqNChhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

### **Gobierno Santos sigue negando la movilización social** 

Aseveran también que esta respuesta es típica del gobierno Santos, que durante todos estos años se ha caracterizado por desconocer el movimiento social. "El ministro desconoce una preocupación auténtica que desde todas las ciudades del país empieza a brotar. Lo que deja ver con esta arrogancia es que intenta descalificar el movimiento social para dejar de lado la discusión de fondo y es la responsabilidad política y las competencias en torno a la regulación del sector petrolero y la actividad extractiva", expresa Mateo Córdoba, también integrante de la Plataforma ALTO.

<iframe src="https://co.ivoox.com/es/player_ek_24859217_2_1.html?data=k5mll56WdZihhpywj5WVaZS1lJqah5yncZOhhpywj5WRaZi3jpWah5ynca_V1cbZy8aPlMLm08aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Desde ese sector de la ciudadanía que exige la renuncia de Murillo, se explica que la renuncia se pide para que se denote que existen responsabilidades políticas que no pueden dejarse de lado, "Y para que se entienda que desde todas las ciudades la preocupación por el ambiente va a seguir siendo auténtica y en pro de la transición energética", dice Córdoba.

### **"Esa no puede ser la respuesta de un servidor público"** 

Los ambientalistas afirman que esa respuesta no es propia de un ministro. "Sorprende que la afirmación venga de una servidor público del nivel de un ministro. Olvida que existen unas competencias constitucionales y legales para los servidores públicos, por eso existe un Ministerio de Ambiente y unos funcionarios con salarios, competencias y un conocimiento técnico para desarrollar unas funciones. No es posible que un ministro sugiera delegar esas funciones a la ciudadanía", expresa la directora de la Plataforma ALTO.

Dicha respuesta se da en medio del empeoramiento de la situación que atraviesa la fauna, la flora y las comunidades de la Lizama, ya que en la madrugada del martes se presentó un nuevo afloramiento de crudo, que de acuerdo con los líderes de la comunidad podría ser aún pero que el primero. Por su parte Ecopetrol asegura que la situación está controlada.

El Ministro que ya se ha hecho presencia en las veredas afectadas por el derrame, y las desembocaduras del río Sogamoso. Además ha dicho que la ANLA ha exigido rápidas respuestas de Ecopetrol, debido a que se trata de una tragedia que podía haberse evitado, ya que "es evidente que Ecopetrol no se respondió a la contingencia con toda la celeridad que debió haber respondido".

\[KGVID\]https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/WhatsApp-Video-2018-03-27-at-10.49.00-AM.mp4\[/KGVID\]

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
