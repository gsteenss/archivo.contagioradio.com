Title: Más de 5 mil personas se movilizarán contra la temporada taurina de Bogotá
Date: 2017-01-19 21:10
Category: Animales, Voces de la Tierra
Tags: animalismo, Bogotá, corridas de toros, temporada taurina
Slug: mas-5-mil-personas-se-movilizaran-la-temporada-taurina-bogota
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Marcha-No-a-las-corridas-de-toros-5-.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [18 Ene 2017] 

En respuesta al inicio de la temporada taurina en Bogotá, este 22 de enero se espera que [más de 5 mil personas alcen su voz contra la tortura animal,](https://www.facebook.com/events/972171882927429/) ese mismo día sobre las 2 de la tarde frente a la Plaza la Santamaría, en un acto fúnebre que recordará los nombres de los toros que serán asesinados.

“La movilización que planeamos es una representación del descontento ciudadano por el retroceso causado tras la decisión de la Corte Constitucional”, expresa Natalia Parra, directora de la Plataforma ALTO, quien agrega que se trata de un acto en el que los ciudadanos que participarán son todos aquellos que “se sienten vulnerados en su derecho a vivir en una ciudad libre de tortura legalizada por el propio Estado”.

Y es que desde hace varios días, carteles y publicidad en los paraderos de la ciudad, invitan a los bogotanos a asistir a la nueva temporada taurina, en nombre de la libertad, lo que para Parra significa “un absurdo” pues, según ella no se puede estar “manoseando” en pleno siglo XXI el concepto de la libertad, ya que esta termina cuando empieza la de otro, y en la plaza se empezará a afectar la vida e integridad de un ser sintiente.

### Razones para movilizarse el 22 de enero 

**Llamar la atención del Congreso:** “Queremos decirle al Congreso de la República que dé un paso adelante y deje de lado una serie de intereses alrededor de la tauromaquia y la ganadería. Los legisladores deben tomar conciencia, de que Colombia está cansada de la legalización de la tortura”.

**Las víctimas de la tauromaquia:** “No solo es la vida del toro, sino la de los caballos que muchas veces salen heridos o muertos. Las niñas y los niños  también son víctimas, al ser expuestos a la violencia del las corridas de toros, como lo ha señalado la ONU”. [(Le puede interesar: Desde el Congreso buscan alejar a los niños de la violencia de la tauromaquia)](https://archivo.contagioradio.com/desde-el-congreso-buscan-alejar-a-los-ninos-de-la-violencia-de-la-tauromaquia/)

**Defender la Ley contra el maltrato animal:** “Presionar a la misma Corte Constitucional para que defienda la Ley 1774 que penaliza el maltrato animal,  y no se tomen decisiones que puedan afectar la ley, por los intereses de magistrados abiertamente taurinos”. [(Le puede interesar: Hasta 60 salarios mínimos pagará quien maltrate a un animal)](https://archivo.contagioradio.com/hasta-60-salarios-minimos-pagara-quien-maltrate-a-un-animal/)

**Que los taurinos dejen las armas:** “Confiamos en que más temprano que tarde, por decisión propia o de los estamentos representativos del “pacto social”, los toreros, rejoneadores, novilleros, etc; dejen las armas. Muchos los esperamos, así como la no repetición, incluso olvidando el daño que le han causado a tantos seres”.

**Realizar la consulta antitaurina:** "Seguimos esperando que la Corte Constitucional de vía libre para la realización de la consulta popular antitaurina, y se evidencie si existe o no arraigo cultural sobre este tipo de espectáculos".

**Las corridas no pagan impuestos:** La Contraloría de Bogotá en 2009, que llamó la atención por el no de cobro de cerca de \$5.000 millones a la Corporación Taurina por concepto del impuesto de Pobres, por los periodos comprendidos entre 2001 y 2008. Actualmente mientras las corridas de toros pagan 0% de IVA, los colombianos debemos pagar un 19%”.

<iframe id="audio_16560632" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_16560632_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
