Title: Gobierno se sale con la suya al aprobar Reforma Tributaria: Robledo
Date: 2016-12-22 15:02
Category: Economía, Entrevistas
Tags: Jorge Robledo, Reforma tributaria
Slug: gobierno-se-sale-con-la-suya-al-aprobar-reforma-tributaria-robledo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/reforma-tributaria-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Acuda] 

###### [22 Dic. 2016] 

**Luego de más de 13 horas en las que los integrantes de la Cámara de Representantes de Colombia debatieron y aprobaron por 78 votos contra 14 en la plenaria, el proyecto de Reforma Tributaria** los diversos sectores que se oponen a este no han dejado de disimular su descontento frente a esta.

En contra de esta Reforma Tributaria, se conoció que votaron el Polo Democrático, el Partido Verde y el Centro Democrático.

Uno de los temas más complejos fue el tema del **IVA, el cual tendrá un aumento del 16% al 19%**, siendo este un solo punto de los 367 con los que cuenta el articulado propuesto por el Gobierno Nacional.

Para el senador del Polo Democrático, Jorge Robledo, **esta es la peor reforma tributaria que podría imaginarse en muchos años** “esta reforma le va a subir los impuestos en cerca de 8 billones de pesos a los pobres y a las clases medias y les va a disminuir más de 1 billón de pesos los impuestos principalmente a las transnacionales”. Le puede interesar: [Reforma Tributaria hace pagar mayores impuestos al ciudadano de a pie](https://archivo.contagioradio.com/reforma-tributaria-pagar-mayores-impuestos-al-ciudadano-pie/)

Además, **para Robledo esta Reforma es inconstitucional dado que “el artículo 363 de la Constitución exige que los impuestos sean progresivos y estos son absolutamente regresivos”** puntualizó el senador.

**Se grabarán los bienes de primera necesidad**

Así lo manifestó Robledo quien agregó “para poner un ejemplo, **todos los artículos de aseo: jabones, desodorantes, escobas, toallas higiénicas se aumentarán en un 19% el IVA”.**

Otros elementos que se verán afectadas por el aumento son: **el servicio del transporte, los combustibles, la ropa, el aceite, la telefonía celular con los planes de datos, las motocicletas** “es una barbaridad” agrega Robledo “una motocicleta es un instrumento de trabajo, de transporte de los ciudadanos del común, aquí ningún magnate lo ve uno andando en moto al sol y al agua”.

**Para el Congresista, los jefes de los partidos santistas le mintieron al país** “duraron todo el año diciendo que no votarían un IVA del 19%, que ni pensarlo, que eso era insoportable, que anunciaban que no lo iban a votar (…) es inaceptable que hayan engañado a los colombianos diciendo que no iban a votar la reforma (…) no sólo cometen el crimen – llamemoslo así sino que además se burlan de la gente”. Le puede interesar: [Sindicatos rechazan reforma tributaria y convocan a paro general](https://archivo.contagioradio.com/sindicatos-rechazan-reforma-tributaria-y-convocan-a-paro-general/)

Y concluye diciendo “nosotros se lo advertimos a los colombianos, si eso se deja para que los Congresistas haga lo que se les dé la gana estamos muertos (…) y llamamos a la movilización y **los colombianos prefirieron quedarse en sus casas y ahí nos van a desplumar”.**

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1} {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio. .p1}

<div class="ssba ssba-wrap">

</div>
