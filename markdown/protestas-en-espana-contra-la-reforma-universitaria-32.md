Title: Protestas en España contra la reforma universitaria 3+2
Date: 2015-02-26 19:22
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: 26f Huelga estudiantil, 26f Vaga estudiantil, Decreto 3+2 Hulga estudiantil España
Slug: protestas-en-espana-contra-la-reforma-universitaria-32
Status: published

###### Foto:Diso press 

##### **Entrevista con [Alex Aguilar]:** 

<iframe src="http://www.ivoox.com/player_ek_4138216_2_1.html?data=lZagmpeVeo6ZmKiak5WJd6KmmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRjNbZzczOjcnJb8bn1drRy8bSuMbnjMrbjarXtMKZpJiSpJbFb8Tjz9nfw5DIqcTmxtncjZiJdqOmjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

Multitudinarias protestas se han vivido en el conjunto del estado español, en el marco de la **jornada de huelga estudiantil convocada por sindicatos de estudiantes en contra de la reforma "3+2" impuesta por el ministro de educación José Ignacio Wert, del gobierno conservador del PP.**

La reforma que pretende equiparar el sistema universitario con el europeo, bajaría los grados de 4 años a 3 y permitiría que los postgrados fueran de 2. **Esta medida supondría que los postgrados en manos de la universidad privada (60%) tendrían más peso que el grado** ya que la especialización pasaría a realizarse en los estudios superiores, encareciendo la universidad y elitizándola aún más.

Por otro lado la **externalización de parte de la universidad beneficia a bancos y empresas privadas pero no al alumno que ha visto como en 3 años han aumentado en un 65% el precio de las matrículas**. Otro problema es que la reforma permite a cada universidad aplicarla como quiera creando diferencias entre universidades elitizando aún mas la educación universitaria.

**El 90%** de los estudiantes y del profesorado universitario **secundaron la huelga**, que terminó en grandes manifestaciones, destacando la de la ciudad de Valencia con un seguimiento de más de 10.000 personas.

Como es habitual la jornada de lucha estuvo acompañada de "piquetes" informativos con el objetivo de apoyar la huelga, acciones contra instituciones públicas y privadas como bancos o la Bolsa de Barcelona, terminando esta con la ocupación del rectorado de la Universidad autónoma de Barcelona.

En total ha habido **dos cargas policiales, una en Gasteiz y otra en Madrid al finalizar las manifestaciones, la primera ha dejado un herido grave por bala de goma en la cabeza y dos detenciones**.

La carga policial de Madrid se ha realizado en medio de un enfrentamiento provocado por miembros del Sindicato de Estudiantes (sindicato afín al PSOE, y al que desde todos los otros sindicatos y grupos de lucha acusan de ser parte de los recortes y del propio decreto), contra estudiantes del movimiento autónomo y libertario. Dicho enfrentamiento ha suscitado una gran polémica en las redes sociales.

Por último distintas **organizaciones sociales y sindicatos estudiantiles han convocado un referéndum para poder hacer partícipe a la comunidad universitaria sobre la decisión de la aplicación del decreto**. También se han sumado en distintas ciudades estudiantes pertenecientes a la secundaria.

Entrevistamos a Alex Aguilar, miembro del sindicato de estudiantes "A Contracorrent" del País Valenciano, quién nos explica las reivindicaciones y la organización de la jornada de lucha, así como las futuras acciones para paralizar el decreto 3+2.
