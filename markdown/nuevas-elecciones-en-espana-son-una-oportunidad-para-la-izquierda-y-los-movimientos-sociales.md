Title: Nuevas elecciones en España,  una oportunidad para la izquierda y los movimientos sociales
Date: 2016-04-29 18:28
Category: El mundo, Política
Tags: Eleccioines, españa, Izquierda Unida, podemos, PP, PSOE
Slug: nuevas-elecciones-en-espana-son-una-oportunidad-para-la-izquierda-y-los-movimientos-sociales
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/marcha-españa-contagioradio.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: hepertextual] 

###### [29 Abril 2016]

Desde diciembre de 2015 no se ha podido llegar a un acuerdo para elegir gobierno en España, ante esta situación se decidió convocar a nuevas elecciones que se realizarán en el mes de Junio, sin embargo para muchos ciudadanos españoles , con una nueva jornada electoral **podría repetirse el mismo escenario en que ningún partido llegue a la mayoría absoluta** y las coaliciones no se puedan dar por diferencias programáticas.

Según el periodista José Rocamora, ninguno de los pactos que se pensaban pudieron concretarse y la terea del Rey de definir un nuevo primer ministro quedó pendiente. Ante dicha imposibilidad, **plantear unas nuevas elecciones no es la salida puesto que las fuerzas políticas y el propio electorado no ha cambiado de manera sustancia**l, por ello casi el 70% de los ciudadanos afirman que unas nuevas elecciones significarán “un despilfarro”.

El **Partido Popular y el Partido Socialista tienen mucho que perder, los múltiples escándalos de corrupción, algunos de ellos ligados con el escándalo de Panamá Pappers y otros en los que también ha salido afectado “Ciudadanos”**  podrían poner en peligro algunos de los escaños obtenidos en las elecciones de diciembre, además los grandes partidos tienen casi las mismas políticas económicas, lo que indica que si pactan y ganan nada cambiaría.

**La verdadera oportunidad la tienen Podemos, por la cantidad de escaños que alcanzó y la Izquierda Unida** que tiene muchas circunscripciones, y que esta misma semana IU autorizó a Garzón para pactar con Podemos, lo que resultaría en más escaños para esa coalición y más fuerza a la hora de pactar un gobierno.

Para Rocamora, es muy importante recalcar **que los movimiento sociales siguen muy movilizados** y además han ganado con Podemos, y están gobernando capitales tan importantes como la propia Barcelona en la que la alcaldesa es líder del movimiento anti desahucios. Otro elemento es la **salida de prisión de Arnaldo Otegui, líder histórico de la Izquierda Abertzale** lo que podría ser un elemento más en alcanzar escaños suficientes para ser gobierno.

<iframe src="http://co.ivoox.com/es/player_ej_11382348_2_1.html?data=kpagmpeXeJmhhpywj5aZaZS1lpmah5yncZOhhpywj5WRaZi3jpWah5yncavj1MqYtNTHpc7j08aYj5C0qdPd0MnW1dnFcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
