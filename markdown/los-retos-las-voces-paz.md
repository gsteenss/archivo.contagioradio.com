Title: Los retos para las "Voces de Paz"
Date: 2016-12-15 15:03
Category: Nacional, Paz
Tags: democracia colombiana, FARC-EP, Implementación de Acuerdos, participación ciudadana, voces de paz
Slug: los-retos-las-voces-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/colombia_marcha-paz_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Dic 2016] 

Para el analista y politólogo Fernando Giraldo, son bastantes los retos que tiene la nueva iniciativa política Voces de Paz, entre tantos, resalta el** conquistar con sus posturas ideológicas, el corazón, por lo menos de una parte de los ciudadanos, lograr una participación política en su nombre, ser veedores y garantes para que lo acordado no sea vulnerado.**

Giraldo, investigador y politólogo, asegura que con la creación de esta nueva iniciativa “se supera un elemento y es el de la violencia política, el hecho que las FARC-EP haya decidido **salir de la clandestinidad para participar abiertamente en política significa un gran avance para el país”.**

El analista señala que es una oportunidad para que los preceptos de la ciudadanía respecto a los partidos políticos cambie, pues “históricamente los partidos políticos en nuestro continente han sido excluyentes, **existe un monopolio de candidaturas en cargos de elección popular que han estado centralizados en quienes han tenido el privilegio** y la exclusividad para presentar candidatos, es decir, los partidos políticos”.

Giraldo resalta que es responsabilidad de todos los colombianos “no cerrar la puerta a esta iniciativa, porque ahora **están haciendo lo que siempre se les pidió y quieren transformar lo que siempre se les recriminó”** y advierte que una de las mayores dificultades que puede enfrentar Voces de Paz es que “hay sectores políticos tradicionales que **querrán cobrarles su pasado y depende de ellos y ellas crear una figura de partido en un momento donde la tendencia está en tomar distancia”.**

El analista agrega que “el nuevo partido, debe verificar que todo se ciña a lo pactado en los acuerdos, para que el **Gobierno y el Congreso no tengan margen de maniobra más que el de aplicar”.** Le puede interesar: [“Nuestras voces serán las voces de los acuerdos de paz”.](https://archivo.contagioradio.com/voces-paz-colombia-movimiento/)

Por último, Giraldo comenta que la mayoría de sectores políticos “quieren resolver el problema de la paz en el estrecho marco de la constitución y la ley colombiana, cosa que no es posible porque es justo ese marco el que ha generado el conflicto” y puntualiza que esa será otra de las tareas de Voces de Paz **“tramitar leyes y demandas que respondan a las realidades de las comunidades”.**

<iframe id="audio_15042183" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_15042183_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
