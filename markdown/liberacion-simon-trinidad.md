Title: Liberación de Simón Trinidad estaría en trámite
Date: 2016-12-27 19:44
Category: Nacional
Tags: colombia, extradicion, proceso de paz, Ricardo Palmera alias Simón Trinidad Comandante de las FARC
Slug: liberacion-simon-trinidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/simon-trinidad.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [27 Dic 2017] 

Se ha conocido que existe la posibilidad de que "Simón Trinidad" se sujeto de perdón judicial por parte del gobierno de Estados Unidos, existen diversas versiones que así lo confirman, sin embargo, fuentes cercanas al proceso afirman que se está esperando el cese de la especulación para poder continuar.

De acuerdo con información publicada por el diario El espectador, la liberación de Ricardo Palmera, alias Simón Trinidad, **estaría en trámite ante la justicia de los Estados Unidos y su suerte dependería de una decisión del presidente saliente Barack Obama**.

De acuerdo con la publicación, el abogado defensor Oscar Silva, aseguró que **el perdón judicial "se encuentra en curso"** y que de esa manera se corregiría el error histórico cometido por EEUU al no respetar el "condicionamiento histórico hecho por la Corte Constitucional cuando avaló la extradición".

En [entrevista concedida a Contagio Radio en 2015](https://archivo.contagioradio.com/acabar-con-la-impunidad-es-investigar-es-perseguir-y-es-sancionar/), el abogado Enrique Santiago, asesor jurídico del proceso de paz, aseguró que lo particular en el caso de Simón Trinidad "es que no está sometido a la jurisdicción colombiana, se ha sometido a la justicia de Estados Unidos, si estos hechos, esta condena se hubiera dado en Colombia sin duda alguna sería amnistiable".

En concepto del abogado, **la liberación del jefe de las FARC EP depende "de la buena voluntad del gobierno de EEUU "** que permitan encontrar formas alternativas para que cumpla su condena y bien se le pueda aplicar la amnistía en un posible retorno a Colombia, como se establezca en el proyecto de ley que cursa en el Congreso de la República.
