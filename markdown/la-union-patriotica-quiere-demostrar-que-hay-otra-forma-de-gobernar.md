Title: La Unión Patriótica quiere demostrar que hay otra forma de gobernar
Date: 2015-08-14 14:42
Category: Nacional, Política
Tags: Aida Avella, Alcaldía, alianza, Bogotá, Clara López, Unión Patriótica
Slug: la-union-patriotica-quiere-demostrar-que-hay-otra-forma-de-gobernar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/aida_avella_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: telemundo33.com]

<iframe src="http://www.ivoox.com/player_ek_6741102_2_1.html?data=l5yhk5aUdo6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRkMKf1tPWh6iXaZqnz5Ddw9nWrYa3lIqmldnNp8Kf0trWx9fJb8XZztTg1tfFtozl1sqYysbdb9Do08aYyNTWcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Aida Avella, Presidenta de la Unión Patriótica] 

###### [14 ago 2015]

La Unión Patriótica, encabezada por Aida Avella, ve a Bogotá como una ciudad moderna, pero además incluyente e igualitaria. Una de las primeras metas del partido es la **reducción de la pobreza y la desigualdad, una tasa que al día de hoy está en 5.4% (400.00 personas en condiciones de pobreza).**

Avella también dio su respaldo a la candidata **Clara López del Polo Democrático** por la Alcaldía de Bogotá, y anunció su candidatura al concejo de Bogotá, encabezando la lista del partido. Las propuestas que se trabajarán desde el concejo de Bogotá con la ayuda de los ediles buscan **resolver distintas problemáticas que se presentan en la ciudad. **

Según la candidata al concejo de Bogotá la UP “*promoverá un sistema de transporte público por nodal, integrado, amable con el ambiente*”, además de resaltar que las vías de los cuatro costados de la ciudad requieren de atención especial en cuanto al transporte público. Además de la construcción y mejora de la malla vial en los barrios.

Otra de las propuestas de ciudad es la **implementación de ascensores para los barrios altos de Bogotá** –aquellos que se encuentran en las lomas-, un sistema que es rápido y amable con el medio ambiente, además que es una alternativa al uso del carro.

La salud y la educación son dos apuestas fundamentales para mejorar las condiciones de vida de las personas, la primera sería con **acceso integral y con calidad, además que establecer una red pública de alto nivel**; para la segunda esta alianza buscaría generar más condiciones para el acceso y además impulsar la cátedra de historia en los colegios.

Sobre el delito en la ciudad de Bogotá la candidata tiene tres propuestas que van encaminadas hacia las oportunidades laborales, **la educación y además la cultura, temas que recogerían a gran parte de la población incluyendo a los desplazados.**

La candidata al concejo indicó que los ciudadanos también tendrán la oportunidad de participar en la construcción de Bogotá con sus propuestas. Las dos candidatas en las pasadas elecciones presidenciales, con una anterior coalición obtuvieron cerca de dos millones de votos a nivel nacional, superando los 500.000 sufragios en Bogotá.
