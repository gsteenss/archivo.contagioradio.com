Title: "Los nadie" hermandad, arte callejero y punk
Date: 2016-09-16 12:54
Author: AdminContagio
Category: 24 Cuadros
Tags: "Los Nadie", Cine Colombiano, Eduardo Galeano, Festival cine de Venecia
Slug: los-nadie-hermandad-arte-callejero-y-punk
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/peliculalosnadie.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Pacifista 

###### [15 Sep 2016] 

[Luego de ser la primera película colombiana en participar y obtener el premio del público durante la Semana de la Crítica en Venecia, el largometraje: “[Los Nadie](http://www.lbv.co/velvet_voice/losnadie/com007_losnadie_salas.html)”, dirigido por el antioqueño **Juan Sebastián Mesa**, se estrenó este jueves en salas de cine de Bogotá, Medellín, Cali y Bucaramanga.]{._5yl5}

La película, inspirada en el **poema "Los nadies" de Eduardo Galeano**, busca a través del uso de blanco y negro centrar la atención en los personajes ‘**Pipa’, 'La rata', ‘Ana’, ‘Manu’ y 'El Mechas**', un grupo de 5 amigos que buscan emprender un viaje por Sudamérica, para lo cual acudirán a su capacidad de hacer **arte callejero** en medio de una ciudad hostil, teniendo al punk como contexto en común.

La ópera prima de Juan Sebastián Mesa, que inauguró la edición 56 del Festival Internacional de Cine de Cartagena y fue parte de la edición 27 de Cine en Construcción en Toulouse, Francia; fue rodada en locaciones de la ciudad de Medellín como Robledo, el centro, Manrique y Laureles, en apenas 10 días y con un presupuesto de 2.000 dólares.

<iframe src="https://www.youtube.com/embed/8rpE0N3h7_A" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
