Title: Campesinos exigen que Ejército diga la verdad del asesinato de Didian Agudelo
Date: 2020-03-09 17:16
Author: AdminContagio
Category: Actualidad, Movilización
Tags: Antioquia, campamento, Lider social
Slug: campesinos-exigen-que-ejercito-diga-la-verdad-del-asesinato-de-didian-agudelo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/03/Marcha-por-Didian-Agudelo.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:html -->

<figure class="wp-block-audio">
<audio controls src="https://co.ivoox.com/es/gabriel-sanchez-campesinos-exigen-ejercito-diga-la_md_48754620_wp_1.mp3&quot;">
</audio>
  

<figcaption>
Entrevista &gt; Gabriel Sanchéz | Integrante de la Asociación Campesina del Norte de Antioquia

</figcaption>
</figure>
<!-- /wp:html -->

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: ASCNA {#foto-ascna .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este lunes 9 de marzo la comunidad de Campamento, Antioquia, realizó una manifestación en horas de la mañana para exigir la verdad tras el asesinato del líder comunal [Didian Agudelo](https://archivo.contagioradio.com/comunidad-responsabiliza-al-ejercito-del-asesinato-y-desaparicion-del-lider-didian-agudelo/), presuntamente a manos del Ejército, así como reclamando por el respeto de los derechos humanos y la posibilidad de habitar en su tierra sin temor a perder la vida.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CceeuAntioquia/status/1236674141199138816","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CceeuAntioquia/status/1236674141199138816

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### **En contexto: El asesinato de Didian en una zona de alta presencia militar**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado 29 de febrero, organizaciones sociales informaron que encontraron el cuerpo de Didian Agudelo, un líder comunitario y político que había sido desaparecido desde el 25 de febrero mientras se encontraba trabajando con sus animales de pastoreo en una finca de la vereda La Frisolera, municipio de Campamento, en Antioquia.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Su cuerpo fue encontrado colgado del cuello con su propia camisa, en un lugar en el que ya se había realizado su búsqueda. Adicionalmente, en La Frisolera hay una alta presencia militar, llegando a encontrarse incluso un batallón. Por esta razón, la comunidad señaló al Ejército de estar relacionado con el homicidio.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Al asesinato de Didier, se suman temores previos en la comunidad, que denuncia haber sido retenida sin justificación por el Ejército así como requisas indiscriminadas y señalamientos como guerrilleros por parte de algunos uniformados. (Le puede interesar: ["No convencen explicaciones del Ejército tras intento de retención a líder Nasa"](https://archivo.contagioradio.com/no-convencen-explicaciones-del-ejercito-tras-intento-de-retencion-a-lider-nasa/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **La marcha exigiendo verdad y respeto**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Gabriel Sánchez, integrante de la Asociación Campesina del Norte de Antioquia, explicó que las comunidades se reunieron este lunes para exigir el esclarecimiento de los hechos alrededor de Didian. De igual forma, se manifestaron reclamando al Ejército para que cese la hostilidad contra los habitantes, que viven en permanente zozobra y han llegado a tomar la determinación de que niños no vayan a la escuela ni los hombres al trabajo, para evitar encuentros con la Fuerza Pública.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por último, las comunidades pidieron que no se estigmatice la marcha, porque su convocatoria fue posible a raíz del dolor por la muerte del líder Didian y por el temor de la población, en lugar de otras razones. Esta exigencia la hicieron a medios como [El Colombiano](https://www.elcolombiano.com/colombia/el-plan-de-cabuyo-para-evadir-militares-PA12568601), que señalaron en sus portales de información que la marcha sería un plan usado por las disidencias para permitir la salida del territorio de uno de sus líderes.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Desde esta posición, apoyados por testimonios de un funcionario civil y de la Fuerza Pública, la gente que marchó este lunes habría salido a las calles obligada para exigir la salida del Ejército y así permitir la huída de alias 'Cabuya'. (Le puede interesar: ["Fuerza de Tarea Aquiles no evitó desplazamiento de más 120 familias en Bajo Cauca"](https://archivo.contagioradio.com/fuerza-de-tarea-aquiles-no-evito-desplazamiento-de-mas-120-familias-en-bajo-cauca/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Sánchez señaló que la intención de la comunidad es reunirse con las autoridades civiles y militares para generar un ambiente de tranquilidad en favor de las personas, que se sienten confinadas por el Ejército. En el mismo sentido, sostuvo que las comunidades rechazan todas las violaciones a los derechos humanos de cualquier actor armado, y lo que quieren es evitar ser víctimas de estos grupos.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AscatNa/status/1237054257142075399","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AscatNa/status/1237054257142075399

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:heading {"level":3} -->

### Actualización: Los compromisos tras la marcha

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según informó en un comunicado el nodo Antioquia de COEUROPA, tras la marcha y el encuentro en el Coliseo, campesinos e instituciones acordaron que la Fiscalía haría el acompañamiento para la investigación de lo ocurrido con Didian. También se pidió al Ejército el respeto estricto por las normas establecidas en el Derecho Internacional Humanitario (DIH) y los derechos humanos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A los medio, los campesinos les pidieron detener la estigmatización, mientras que los campesinos se comprometieron a no señalar al Ejército de la muerte de Didian hasta que se surtieran las investigaciones del caso. Por último, se acordó que se constituiría una mesa de trabajo entre ciudadanos e instituciones para velar por la promoción y protección de los DD.HH.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/CceeuAntioquia/status/1237333163837468672","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/CceeuAntioquia/status/1237333163837468672

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:block {"ref":78980} /-->
