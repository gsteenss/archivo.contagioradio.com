Title: Nuevo caso de reclutamiento forzado del Ejército en el Huila
Date: 2015-05-20 14:31
Author: CtgAdm
Category: DDHH, Otra Mirada
Tags: batidas ilegales, Eider Causaya Rivera reclutado forzadamente, Ejército Colombiano recluta forzadamente, Engaño y reclutamiento forzado joven Huila
Slug: nuevo-caso-de-reclutamiento-forzado-del-ejercito-en-el-huila
Status: published

###### Foto:Doralnewsonline.com 

###### **Entrevista con[ Martha Bertilia], madre de Eider Casuaya Rivera joven reclutado forzadamente:** 

<iframe src="http://www.ivoox.com/player_ek_4523370_2_1.html?data=lZqflZibdI6ZmKiakp6Jd6KmlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYzX0NOYr8bWuMnVjKfS1NnNsMrVjNLOxtfJb8XZjM%2Fc2MrSb9PZxNHi1sbIs46ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

El joven campesino **Eider Cusuaya Rivera del municipio de Monserrate en el departamento del Huila fue reclutado forzadamente** por el ejército Colombiano el viernes 20 de Febrero de 2015.

Eider acudía a las 10:00 de la mañana a la Registraduría Nacional ha recoger su cédula de ciudadanía. La Registraduría está cerca de una estación de policía, de la que salió un militar que le pidió los papeles de identificación.

Fue entonces cuando el **joven campesino pudo escuchar que al militar lo llamaban **[**“dragoneante Quesada”**, el mismo que le indicó que no tenía la libreta militar y que ya **tenía edad para ingresar al ejército,** Eider le espetó que el no quería alistarse, a lo cual el militar le dijo **que no era para la guerra sino para el cuerpo de "ingenieros".**]{lang="ES"}

El **militar retuvo al joven y lo metió en un coche que lo llevó al **[**Batallón Cacique Pigoanza** en la Plata H, donde pasó la noche ya reclutado forzadamente.]{lang="ES"}

Más tarde y  en esa misma noche junto con otros jóvenes fue llevado a [Neiva, al batallón Tenerife, para luego de tres días volver a ser trasladado al batallón Magdalena en Pitalito. H.]{lang="ES"}

Estos no fueron los únicos traslados sino que aún sería movido por los batallones [de Ingenieros de Construcción (BICON) Nº53 yal Batallón de Instrucción, **Táctica, Entrenamiento y Reentrenamiento – Vicente de la Rocha y Flórez- BITER No.9  **con sede en la Plata-Huila, donde actualmente se encuentra internado.]{lang="ES"}

**Martha Bertilia, madre del joven** nos cuenta como su hijo nunca había pensado en alistarse al ejército y también que **al intentar salir del reclutamiento Eider ha sido castigado reiteradas veces, **además nos explica las condiciones infrahumanas en las que mantienen a los jóvenes reclutas.

**Falta de comida y exposiciones duraderas a sol descubierto provocando graves quemaduras en la piel del joven campesino** es lo que le ha podido contar a su madre en la única comunicación que han tenido durante estos tres meses.

Eider deja una **familia con 4 hermanos y una madre, también deja de lado el cultivo del café** en lo que trabajaba antes de ser reclutado.

La madre del joven campesino del Huila ha intentado mediante instancias judiciales sacarlo pero ha sido imposible debido a que **fue obligado a firmar un documento en el que se podía leer que se había alistado voluntariamente.**

A pesar del fallo de la Corte Suprema que **ilegaliza las batidas ilegales, se siguen presentando casos como el de Eider, reclutando forzadamente** campesinos jóvenes de los departamentos.

 
