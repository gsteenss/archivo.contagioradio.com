Title: Con sus propios testimonios ante fiscalía amenazan a comunidad de las Pavas
Date: 2017-02-01 17:36
Category: DDHH, Nacional
Slug: con-sus-propios-testimonios-ante-la-fiscalia-amenazan-a-comunidad-de-las-pavas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/las-pavas-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: ASOCAB] 

###### [01 Feb 2017] 

La comunidad de las Pavas denunció que ahora la intimidación y el hostigamiento se da a través de la divulgación de los testimonios que varios de los líderes de la comunidad rindieron ante la fiscalía. La denuncia advierte que las **hojas de transcripción de los relatos, con logo de la fiscalía, amanecieron ubicados en postes, calles y caminos veredales el pasado 27 de Enero**.

![las pavas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/las-pavas.jpg "Casco urbano"){.alignleft .wp-image-35617 width="317" height="236"}

![Comunicado caso Las Pavas-2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Comunicado-caso-Las-Pavas-2.jpg){.wp-image-35618 .alignright width="183" height="239"}

Las amenazas a través de la divulgación de los testimonios se da en el contexto de los pocos avances que ha tenido el proceso judicial que pretende esclarecer y juzgar el desplazamiento forzado de la comunidad en 2003.

Entre los avances se cuenta la detención de algunos de los determinadores del hecho así como los trabajadores de la **empresa palmera Aportes San Isidro S.A.S y miembros de estructuras paramilitares** que operan en la región.

En el comunicado la comunidad advierte que en medio del evidente reforzamiento de la presencia y el control paramilitar en la región y también en un contexto en que los rumores acerca de que las personas detenidas no están solos, es decir, tendrían el apoyo de personas que siguen intentando despojar las tierras en el corregimiento de **Buenos Aires, Municipio del Peñón, Departamento de Bolívar**. ([Le puede interesar Protesta de campesinos del Sur de Bolívar en medio del Paro Nacional](https://archivo.contagioradio.com/en-medio-de-fuerte-militarizacion-avanza-el-paro-nacional-en-el-sur-de-bolivar/))

Las familias de las pavas asociadas en ASOCAB han sido víctimas de **reiteradas acciones por parte de los trabajadores de la empresa Aportes San Isidro y paramilitares** entre las que se cuentan la quema de los ranchos de almacenamiento de las cosechas, el levantamiento de las cercas de delimitación y protección del predio,  las amenazas directas a los líderes de la asociación, incluso el desplazamiento forzado de algunos de ellos.

La denuncia, firmada por 6 organizaciones que acompañan a la comunidad campesina concluye con una serie de **exigencias de protección, de avance en las investigaciones y de respaldo nacional e internacional para que se concluya con la garantía de derechos de los campesinos**,  así como la posibilidad de vivir dignamente en los territorios que pretenden ser arrebatados por empresarios y paramilitares.

[Comunicado Caso Las Pavas (1)](https://www.scribd.com/document/338155454/Comunicado-Caso-Las-Pavas-1#from_embed "View Comunicado Caso Las Pavas (1) on Scribd") by [Contagioradio](https://es.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_80543" class="scribd_iframe_embed" src="https://www.scribd.com/embeds/338155454/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-EvxiGIiERXEF9Gb6rnzx&amp;show_recommendations=true" width="75%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
