Title: 20 de noviembre día mundial de las niñas y los niños
Date: 2016-11-20 08:00
Category: Nacional
Tags: niños
Slug: 20-de-noviembre-dia-mundial-de-las-ninas-y-los-ninos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/11/dia-del-nino-y-nina.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### 20 Nov 2016 

A través de la resolución 836 (IX) presentada por la Asamblea General de las Naciones Unidas en 1954, se recomendó  instituir el **día mundial de los niños y niñas**, seguido por la declaración y convención sobre los derechos del niño el 20 de noviembre de 1959 y  en 1989.

En dicha declaración se establecieron como principios fundamentales el derecho a la igualdad sin distinción de raza, religión o nacionalidad; a recibir protección especial para el desarrollo físico, mental y social; a tener un nombre y nacionalidad;  a la educación, vivienda y alimentación; a una atención especial en materia de educación y tratamiento para aquellos que sufren alguna discapacidad física o mental; a la comprensión y amor; y a recibir protección contra cualquier forma de abandono, crueldad y explotación.

De acuerdo con el Programa Mundial de Alimentos, para 2015 el 45% de las muertes de niños y niñas menores de cinco años se debieron a una nutrición deficiente, en lo que se estimó un aproximado de 3,1 millones de fallecimientos al año.

En el informe también se señala la situación de la niñez en materia de educación y las negativas condiciones en que esta se da. En los países en desarrollo cerca de 6,6 millones de niños (as) en edad escolar primaria asisten a las clases con hambre; en promedio 1 de cada 4 padecen retrasos en el crecimiento, pudiendo incluso elevarse la proporción a 1 de cada 3.

África es una de las regiones en las que el panorama se torna aún más crítico; 23 millones de niños (as) asisten a clases con hambre, mientras que en lo que respecta al África Subsahariana 1 de cada 4 personas presentan cuadros de desnutrición.

Según la Organización de las Naciones Unidas para la Educación, la Ciencia y la Cultura (UNESCO), en el mundo son 53 millones los niños (as) entre 6 y 11 años sin escolarizar, mientras que según la proyección de UNICEF entre 2016 y 2030 cerca de 60 millones de menores de edad  no asistirán a la escuela.

### ** En Colombia la violación contra los derechos de los niños y niñas continúa** 

Según el informe presentado por UNICEF en 2015 cerca de 45 mil han sido víctimas de homicidio, casi 2,3 millones han quedado desplazados y 8 mil han desaparecido desde 1985 año en el que se inició la recopilación de datos.

En el informe sobre *Los Derechos de la Niñez en Colombia* presentado en 2015 ante el Comité de las Naciones Unidas, las observaciones finales dieron cuenta de la crítica situación en la que viven los niños y niñas:

-          Persiste la discriminación contra la niñez indígena, afrocolombiana, desplazada y LGBT en Colombia

-          Prevalece una extensa impunidad en los casos de asesinatos y  desapariciones de niños (as), aún en aquellos cometidos por agentes del gobierno.

-          La violencia sexual continúa de tal manera que un alto número de niñas menores de 14 años dan a luz en hospitales como consecuencia de esta.

-          No existen estrategias ni condiciones  idóneas para lograr una educación inclusiva para la niñez con algún tipo de discapacidad.

-         El aumento de la explotación sexual de las niñas y niños, a la que se suma la expansión de la trata de menores afectando principalmente a las poblaciones desplazadas, indígenas y afrocolombianas.

A esto se suma la información brindada por la Defensoría del Pueblo en 2016 en la que según las estadísticas realizadas en los dos primeros meses de este año hubo 2.594 violaciones contra menores y 358 muertes violentas.

De acuerdo a la Red Paz Paz, alrededor del 33,98% de niños (as) y adolescentes se encuentran en condiciones de pobreza multidimensional, la mayoría de ellos son aquellos pertenecientes a las comunidades afro, indígenas y de las zonas rurales.

La comisión de Derechos Humanos de los Pueblos Indígenas presento en 2016 un comunicado en el que señalaba la grave situación que viven las comunidades; en tan sólo cinco años murieron 4 mil niños y niñas Wayúu, además de las 45 muertes sufridas por el pueblo Emberá Katío de niños (as) entre los 0 y 8 años en un años por enfermedades gastrointestinales.

Según el Informe Mundial Sobre el Trabajo Infantil presentado por la Organización Internacional del Trabajo (OIT) en 2015,  en el mundo eran 169 millones los niños (as) que seguían siendo víctimas de trabajo infantil, mientras que para el caso de Colombia eran 1.039. 000.
