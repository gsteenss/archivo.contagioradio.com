Title: Ejército continúa haciendo erradicación forzada en el Carmelo, Cauca
Date: 2017-07-27 16:41
Category: Ambiente, Nacional
Tags: COCCAM, Sustitución de cultivos de uso ilícito
Slug: ejercito-continua-haciendo-erradicacion-forzada-en-el-carmelo-cauca
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/7d15abe6213b9e29ccb9bb6d04f6143e_0.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [27 Jul 2017] 

Campesinos del corregimiento del Carmelo, en el municipio de Cajibio, Cauca, denunciaron que, pese a que se han acogido a los planes de sustitución de cultivos de uso ilícito y habían firmado actas comprometiéndose, la Fuerza Pública viene realizando erradicaciones de cultivo forzadas, **la última se hizo el pasado 25 de julio en una operación en conjunto entre el Ejército y el ESMAD**.

De acuerdo con Deivin Hurtado, líder social e integrante de la plataforma Marcha Patriótica, fueron aproximadamente **300 hombres pertenecientes a la Fuerza Pública**, los que hicieron las erradicaciones forzadas, sin escuchar a los campesinos que les manifestaban que ya se encontraban en acuerdos de sustitución voluntaria.

<iframe src="http://co.ivoox.com/es/player_ek_20035461_2_1.html?data=k5WdlZqYepKhhpywj5WaaZS1kpmah5yncZOhhpywj5WRaZi3jpWah5yncaXZytvW0JCsudPowsnch5enb9Tjw9fSjcrWtsLYysjOxc7TssbnjMvc1N_FqMLnjMrbjcrQb6TV05KSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

“La comunidad no permitió la erradicación, inicio una confrontación verbal, en algún momento, de acuerdo con la comunidad, fueron amenazados con las guadañas con las que se estaba haciendo la erradicación, **hubo algunos disparos por partes de miembros del Ejército y se lanzaron gases lacrimógenos que afecto a la comunidad**” manifestó Hurtado. (Le puede interesar:["Campesinos del Putumayo logran acuerdos de sustitución de Cultivos"](https://archivo.contagioradio.com/campesinos-del-putumayo-logran-acuerdos-de-sustitucion-de-cultivos/))

Los campesinos, han señalado que están a la espera de poderse sentar a la mesa con el Ejercito para que deje de hacer erradicaciones forzadas y **que comprenda que hasta que no se pongan en marcha los planes de sustitución de cultivos ilícitos aún muchas familias dependen de esos cultivos.**

### **La esquizofrenia del Gobierno Nacional** 

<iframe src="http://co.ivoox.com/es/player_ek_20035489_2_1.html?data=k5WdlZqYfJqhhpywj5WdaZS1lpaah5yncZOhhpywj5WRaZi3jpWah5yncaTZ1Mbfja_Jtoa3lIqum9-JdqSf1NTP1MqPpcTo1sbQy9TSqdSfxcrZjczTpsrZ09PcjcrSb9HgwtPS1ZCRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg..&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

De acuerdo con Cesar Jérez, vocero de la Coordinadora Nacional de Cultivadores de Coca, Amapola y Marihuana, el gobierno se encuentra en estado de **esquizofrenia debido a que por un lado realiza el lanzamiento de los Planes de Desarrollo** con Enfoque Territorial, con los que se reglamenta la sustitución de cultivos ilícitos, mientras que por el otro continúa realizando erradicaciones forzadas.

“De un lado hay unos acuerdos en la Habana que buscan formular un plan Nacional integral de sustitución y unos planes locales de sustitución, está la reforma integral rural y los PDTS, **pero el gobierno insiste en una política de fuerza de erradicación**, en la fumigación manual terrestre y se plantea volver a las fumigaciones aéreas” afirmó Jerez. (Le puede interesar:["Plan de sustitución de cultivos de uso ilícito debe ser integral o no durará: COCCAM"](https://archivo.contagioradio.com/sustitucion-cultivos-ilicitos-cocam/))

En este sentido Jeréz manifestó que la única opción que le queda al campesinado es movilizarse para exigir al gobierno que dé cumplimiento a lo pactado en los Acuerdos de Paz e inicie la implementación de los planes de sustitución de cultivos ilícitos.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
