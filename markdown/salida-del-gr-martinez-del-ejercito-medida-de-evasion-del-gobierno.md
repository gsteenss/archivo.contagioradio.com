Title: ¿Salida del Gr. Martínez del Ejército, medida de evasión del Gobierno?
Date: 2019-12-28 12:16
Author: CtgAdm
Category: Nacional, Política
Tags: Duque, ejercito, falsos positivos, Nicacio Martínez
Slug: salida-del-gr-martinez-del-ejercito-medida-de-evasion-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/12/Captura-de-Pantalla-2019-12-28-a-las-12.12.02-p.-m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Fernando Vergara] 

Este 27 de diciembre el presidente Ivan Duque anunció el retiro del general Nicacio Martínez como Comandante del Ejército, luego de un año y 17 días al frente de la institución, y múltiples investigaciones en su contra, principalmente relacionadas con casos de ejecuciones extrajudiciales. En su lugar quedará el general Eduardo Zapateiro. (Le puede interesar: [Los casos por los que debería ser investigado el Mayor General Nicacio Martínez](https://archivo.contagioradio.com/deberia-investigado-mayor-general-nicacio-martinez/))

A pesar de ello el presidente en su pronunciamiento afirmó que Nicacio de Jesús Martínez, se retira por motivos familiares, *"He acogido con un inmenso sentimiento gratitud y  de admiración por su tarea cumplida a lo largo de más de 38 años de servicio",* exclamó el mandatario. (Le puede interesar: [Comunidades del Cauca responsabilizan al Ejército del asesinato de Flower Jair Trompeta](https://archivo.contagioradio.com/comunidades-del-cauca-responsabilizan-al-ejercito-del-asesinato-de-flower-jair-trompeta/))

### **Los escándalos de Martínez**

El accionar del general Nicacio en los últimos meses ha sido cuestionado, y  su nombre ha salido a relucir en casos como el asesinato del excombatiente Dimar Torres, cuya muerte es atribuida a hombres del Ejército. También en el reportaje publicado por el periódico New York Times, que reveló las órdenes impartidas por Martínez para aumentar los éxitos de las unidades militares, sin importar que con este aumentarían las cifras de ejecuciones extrajudiciales en toda Colombia, y las muertes a miles de líderes sociales, campesinos, comunidad afro y excombatientes.

Su nombre también figura como responsable en la muerte de ocho niños, en medio de un bombardeo a un campamento de FARC en el Caquetá; al igual que el asesinato a manos del Ejército del líder indígena Flower Trompeta, así como varios casos de corrupción.

Varios congresistas se opusieron a su nombramiento como general del Ejército, y abrieron investigaciones en su contra, por las cuales no recibió ninguna imputación o sanción; por el contrario en junio 17 de 2019, alcanzó su cuarto sol, condecoración máxima que un soldado puede alcanzar.

> Procuraduría ([@PGN\_COL](https://twitter.com/PGN_COL?ref_src=twsrc%5Etfw) ) abrió indagación al comandante del Ejército, general Nicacio Martínez Espinel
>
> ?<https://t.co/okROLTHXtj> [pic.twitter.com/Bqk0lB7x5G](https://t.co/Bqk0lB7x5G)
>
> — Procuraduría Colombia (@PGN\_COL) [May 27, 2019](https://twitter.com/PGN_COL/status/1133138770491002881?ref_src=twsrc%5Etfw)

<p>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</p>
Hace algunos meses, Sebastián Escobar, integrante del Colectivo de Abogados José Alvear Restrepo, afirmó a este medio que dentro de las peticiones que hicieron diferentes movimientos sociales al Presidente solicitaban detener el proceso de ascenso, puesto que mientras Nicacio Martínez ejercía como segundo comandante de la Décima Brigada Blindada, "*Se cometieron* p*or lo menos, 31 casos de  las mal llamadas ejecuciones extrajudiciales*, *con un total de víctimas que supera el medio centenar"*.

Asímismo Escobar manifestó que el Colectivo  ha presentado distintos informes a la Jurisdicción Especial para la Paz (JEP) sobre algunos integrantes de la Fuerza Pública que podrían ser ascendidos como Nicacio Martínez, sin importar su responsabilidad en casos de vulneración a los DD.HH, y mencionó el caso de Adolfo León Hernández, quien fue comandante del Batallón La Popa, en Cesar, y podría estar relacionado con  26 casos de ejecuciones extrajudiciales.

### ¿Quién asumirá el cargo del Gr. Nicacio Martínez ?

La comandancia del Ejercito estará en manos de Eduardo Enrique Zapateiro Altamiranda, oriundo de Cartagena; ingresó a la institución en 1983, se ha desempeñado en cargos como Comandante de la Escuela de Paracaidismo Militar en Tolemaida, comandante del Comando Unificado de Operaciones Especiales (CUNOE), comandante de la Fuerza de Tarea Conjunto Omega, comandante de la Brigada XX en La Guajira y la V en Bucaramanga.

Zapataeiro estuvo involucrado en la operación Fénix, también conocida como **bombardeo de Angostura,  encabezada por el presidente Álvaro Uribe Vélez en el 2008** ; donde la fuerza aérea, militares y policías, atacaron en cercanías de la provincia ecuatoriana de Sucumbío, acción que causó la ejecución de 22 guerrilleros, entre ellos Raúl Reyes, y una crisis diplomática debido a la violación territorial de las fuerzas colombianas en territorio ecuatoriano.

**¿Qué tareas recaen sobre el nuevo comandante ?**

Zapateiro tendrá la responsabilidad de continuar manejando las denuncias [sobre casos de 'ejecuciones extrajudiciales' ante la JEP, primar por la seguridad y  ejecución de las]{#m940-939-941}[operaciones en las Zonas Futuro dentro del Programas de Desarrollo con Enfoque Territorial (PDET), en Nariño, Catatumbo, Bajo Cauca, Arauca y el Parque Chiribiquete; también tendrá que]{#m952-951-953}[dar seguimiento a los actos de corrupción al interior de la institución, y continuar con]{#m964-963-965}[la modernización del Ejército.]{#m976-975-977}

 

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
