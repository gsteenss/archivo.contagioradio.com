Title: Marcha Patriótica exige que se investigue el caso de Nataly Salas Ruíz
Date: 2016-12-04 10:36
Category: DDHH, Nacional
Tags: feminicidios en Colombia, marcha patriotica, Nataly Salas Ruíz
Slug: marcha-patriotica-exige-que-se-investigue-el-caso-de-nataly-salas-ruiz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Mujeres-Marcha-Patriótica-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio  
] 

###### [4 Dic 2016] 

La Comisión de Derechos Humanos del Movimiento Político y Social Marcha Patriótica en el departamento de Córdoba, emitió un comunicado en el que denuncia el asesinato de **Nataly Salas Ruíz de 19 años, encontrada con 3 tiros en la Laguna de oxidación en Montería** y aclara que la joven no pertenecía al movimiento social .

En la misiva la comisión de derechos humanos indica que si bien estarán al frente de la exigencia de investigaciones y aclaración de los hechos, **la joven no hacia parte de ninguna organización integrante de la Marcha Patriótica** y además pudieron verificar con los fundadores y organizadores del **Campamento por la Paz Montería que la joven tampoco hizo parte de dicha iniciativa.**

La comisión es enfática en manifestar que existe “una intención atemorizante, que a todas luces pretende desestabilizar y crear zozobra en los procesos de base organizativa y que **confluyen en la dinámica actual de la implementación de los acuerdos de habana refrendados por el senado y la cámara de representantes”.**

Por último extienden un mensaje de solidaridad a la familia de Nataly Salas, señalan su compromiso para que la muerte de la joven no quede impune y solicitan a los entes investigadores del estado que sean exhaustivos en **clarificar el asesinato y la procedencia del mensaje que circula por la redes en el que se afirma la pertenencia de Nataly a la Marcha Patriótica.**

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio.]{.s1}](http://bit.ly/1ICYhVU)
