Title: Asesinan a Miguel Pérez impulsor de sustitución de cultivos de uso ilícito en Tarazá, Antioquia
Date: 2017-10-22 18:22
Category: DDHH, Nacional
Tags: Antioquia, Sustitución de cultivos de uso ilícito, Tarazá
Slug: asesinan_miguel_perez_taraza_antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/miguel_perez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @AscatNa] 

###### [22 Oct 2017] 

Según se ha denunciado, este fin de semana son dos los hechos que se han cometido contra la población civil. Este domingo, en el municipio de Tarazá Departamento de Antioquia un grupo de hombres armados habría asesinado a Miguel Pérez.

Se trata de un líder social que impulsaba el proceso de sustitución gradual y voluntaria de cultivos de uso ilícito en la región. Además era integrante de la Asociación Campesina del Bajo Cauca, de la COCCAM Tarazá, presidente de la sub directiva Cañón de Iglesia, de ANZORC y del Movimiento Político y Social Marcha Patriótica.

Por otra parte, se ha denunciado que sobre las 6 de la mañana de este domingo en la vereda la Paz, municipio de El Retorno, en Guaviare, **habrían sido heridos por policías al mando del teniente Castro, 3 campesinos que se oponían a la erradicación forzad**a. Las personas golpeadas A esta hora los heridos son trasladados al Hospital de San José del Guaviare.

> [\#ALERTA](https://twitter.com/hashtag/ALERTA?src=hash&ref_src=twsrc%5Etfw) | 3 campesinos heridos por la policía por oponerse a erradicación forzada en la vereda La Paz, municipio de El Retorno, Guaviare. [pic.twitter.com/b5pX2ob4tw](https://t.co/b5pX2ob4tw)
>
> — Marcha Patriótica (@marchapatriota) [22 de octubre de 2017](https://twitter.com/marchapatriota/status/922206518514249729?ref_src=twsrc%5Etfw)

Con este asesinato se completarían mas de **123 asesinatos desde que se firmó el acuerdo de paz y cerca de 81 en lo que va corrido del 2017.** Sin embargo, según la fiscalía, los crímenes no son sistemáticos y en más de la mitad de los hechos investigados por ese ente no se han podido establecer los responsables.

Esta creciente cifra de asesinatos ha impulsado la realización de un Paro Nacional indefinido en el que se espera que miles de personas, entre ellos integrantes de la COCCAM en todo el país salgan a movilizarse, exigiendo, entre otros, el cumplimiento de acuerdos en cuanto a la sustitución de cultivos de uso ilícito.

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
