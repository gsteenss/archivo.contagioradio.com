Title: Frenan construcción de polémica hidroeléctrica en Antioquia
Date: 2019-04-24 17:01
Author: CtgAdm
Category: Ambiente, Movilización
Tags: Antioquia, Celsia, Porvenir II, Río Samaná
Slug: frenan-construccion-de-polemica-hidroelectrica-en-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/04/Captura-de-pantalla-71.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: El Espectador] 

Comunidades antioqueñas celebraron la decisión de la empresa Celsia de frenar la construcción de la hidroeléctrica Porvenir II en el oriente del departamento que desde años se planteaba como una de las más importantes generadoras de energía en el país. Entre los aspectos que habrían aportado a esta decisión está la férrea oposición de las comunidades a este tipo de proyectos.

Los habitantes de la zona de influencia del proyecto, en los municipios de Caracolí, San Carlos, San Luis y Puerto Nare, Antioquia, se habían manifestado en contra del megaproyecto desde 2012, cuando la ciudadanía se enteró de los planes para su construcción sobre el norte del Río Samaná, que implicaba la edificación de un muro de 140 metros y la inundación de más de 1.000 hectáreas de terreno.

Según Carlos Olaya, integrante[ del Movimiento Social Por la Vida y la Defensa del Territorio (MOVETE), la construcción de esta hidroeléctrica hubiera ocasionado graves afectaciones para el caudal del río y los habitantes de la zonas quienes dependen de este recurso hídrico para la pesca, la agricultura, la ganadería y la barequería. (Le puede interesar: "[Comunidades dicen no a hidroeléctrica en Tolima](https://archivo.contagioradio.com/tolima-no-hidroelectrica-rio-totare/)")]

El presidente de Celsia, Ricardo Sierra, anunció el pasado 20 de marzo la empresa estaría vendiendo el proyecto y afirmó que en el caso de que no encontrarán un comprador, la empresa no va a construir la presa dado su interés actual en invertir en las energías renovables.

Olaya sostiene que el rechazo de las comunidades más las recientes polémicas que se dieron alrededor de la construcción de Hidroituango seguramente tuvieron un impacto en la decisión de Celsia de abandonar el megaproyecto. "Hidroituango es el proyecto demostrativo de por qué no se deberían seguir interviniendo los ríos con hidroeléctricas", dijo.

Entre tanto, el ambientalista indica que el movimiento en defensa de los recursos naturales no termina dado que existen otros planes de represar el Río Samaná, como el proyecto Palaguas impulsada por la empresa Isagen. Por tal razón, se unen al paro nacional del 25 de abril para manifestarse en contra del fortalecimiento del modelo extractivista bajo el nuevo Gobierno. (Le puede interesar: "[El Plan Nacional de Desarrollo, un respaldo al sector minero-energético](https://archivo.contagioradio.com/plan-nacional-desarrollo-respaldo-al-sector-minero-energetico/)")

<iframe id="audio_34997827" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_34997827_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
