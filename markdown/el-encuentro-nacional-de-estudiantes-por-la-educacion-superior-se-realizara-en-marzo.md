Title: El Encuentro Nacional de Estudiantes por la Educación Superior se realizará en marzo
Date: 2018-02-20 15:24
Category: Educación, Nacional
Tags: ENEES, Ministerio de Educación, Movimiento estudiantil, Universidades Privadas, Universidades Públicas
Slug: el-encuentro-nacional-de-estudiantes-por-la-educacion-superior-se-realizara-en-marzo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/6.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [20 Feb 2018] 

Las y los estudiantes de Colombia ya tienen fecha para su próximo **Encuentro Nacional de Estudiantes que se realizará los próximo 16, 17 y 18 de marzo en Bogotá**. Este espacio, que reunirá a estudiantes de universidades públicas y privadas, será el escenario para que el movimiento estudiantil vuelva a generar una agenda de movilización frente a las exigencias que le hacen al Ministerio de Educación sobre, gratuidad de la educación, financiación de la educación superior, autonomía universitaria y bienestar Universitario.

Sobre la política pública de “Ser Pilo paga” que en diferentes ocasiones los estudiantes han manifestado que, **en vez de facilitar el acceso a la educación superior, desfinancia la universidad pública** y ensancha las arcas de las Universidades Privadas, que han venido aumentando el costo de las matrículas.

“Estamos en un sistema que pondera la educación para ciertos sectores y que a otros los deja en el olvido. **El programa “Ser Pilo Paga” ha tenido un aumento considerable en el presupuesto**, cosa que nunca pasó con el aumento a la base presupuestal de las universidades públicas, ahí uno se da cuenta de que hay una voluntad política amañada” afirmó Cristian Guzmán, representante de la Facultad de Derecho y Ciencias Políticas de la Universidad Nacional, sede Medellín.

Previo a este espacio, los estudiantes de los diferentes centros de educación, realizaran actividades regionalmente, en donde se intentará definir la situación por regiones de la Educación Superior, para posteriormente llegar al ENEES con un panorama mucho más claro que permita tener herramientas más claras sobre el panorama de este derecho en el país.

Cristian Guzmán afirmó que otro de los puntos que debe re pensarse el movimiento estudiantil, **es la construcción de paz y la participación política en el país que también será un eje transversal** a las discusiones que se den en el ENEES. (Le puede interesar: ["Estudiantes de la Universidad Libre en paro por altos costos de matrículas"](https://archivo.contagioradio.com/estudiantes-de-universidad-libre-en-paro-por-altos-costos-en-matriculas/))

El 22 de febrero, en la Plaza Che de la Universidad Nacional se realizará un carnaval para dar a conocer el ENEES y la necesidad del encuentro, así mismo universidades privadas realizaran este mismo día, jornadas de información. (Le puede interesar: ["Nueva versión de Ser Pilo Paga le quitaría 800 mil millones a universidades Públicas"](https://archivo.contagioradio.com/48884/))

<iframe id="audio_23953071" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_23953071_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
