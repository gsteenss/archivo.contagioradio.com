Title: Operación Orión ¡Nunca Más!, 17 años buscando justicia y verdad
Date: 2019-10-16 13:15
Author: CtgAdm
Category: Memoria, Paz
Tags: Comuna 13, Medellin, operación orion, víctimas
Slug: victimas-operacion-orion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/comuna-13-operacion-orion.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Movice] 

Este 16  de octubre se cumplen 17 años de la Operación Orión en la Comuna 13 de Medellín, ante ello y con la finalidad de exigir respuestas frente a los responsables intelectuales y materiales, **organizaciones defensoras de derechos humanos conmemorarán a las víctimas de este hecho de violencia** desde las 2:00 p.m. con diferentes actividades culturales y simbólicas en  el Museo Casa de la Memoria.

Esta jornada iniciará con un  acto organizado por la plataforma Mujeres Caminando por la Verdad, seguido por un conversatorio sobre lo ocurrido en la Comuna 13 en relación con las operaciones militares en el 2002 , posteriormente se llevará acabo una intervención artística, donde se presentará el grupo RAM (Resistencia Arte y Memoria) , jóvenes que apuestan a la memoria.

Según Luz Elena Galeano  integrante de Mujeres Caminando por la Paz, "el objetivo de esta mesa es la conversación, es el  intercambio sobre los actos de violencia vividos, las cicatrices y formas de sanación que se han dado al interior de la comunidad, al mismo tiempo, **pretende identificar patrones de violencia y zonas de resistencia que hay en la comuna a raíz de esta Operación**".

### ¿Qué fue la Operación Orión? 

Entre el 16 y 18 de octubre del año 2002, 1.500  hombres de la Fuerza Pública, en conjunto con estructuras paramilitares  ingresaron a la Comuna 13, bajo la orientación general Mario Montoya, comandante de la Brigada IV Operación Orión, un conjunto de arremetidas contra la población civil que **dejó 17 homicidios cometidos por el Ejército, 71 personas asesinadas por paramilitares, 12 personas torturadas, 370 detenciones arbitrarias y 6** **desapariciones forzadas.**

Actualmente tanto el general Mario Montoya como el entonces general de la Policía Metropolitana de Medellín, Leonardo Gallego, son investigados por connivencia con grupos paramilitares y exceso de uso de fuerza. No obstante, 17 años después, los avances en términos de justicia han sido nulos. (Le puede interesar: ["La JEP nos hizo sentir protegidas: Víctimas en la primera audiencia pública de la Comuna 13"](https://archivo.contagioradio.com/jep-audiencia-comuna-13/))

### La Comuna 13, un territorio en disputa 

Según Galeano, después de la Operación Orión, la comunidad continúa viviendo la violencia, "**solo que ahora ocurre en silencio, no es visibilizado por los medios de comunicación,** porque las autoridades en sí ya no están haciendo nada, dejan en el aire lo que ocurre". Asimismo, la defensora resalta que las personas tampoco denuncian porque ven que los procesos no avanzan.

A pesar de esto, Galeano afirma que en la Comuna se están dando procesos  organizativos y colectivos que le apuestan a la verdad, la justicia y en especial la no repetición,"los movimientos hacemos un trabajo en la búsqueda de los familiares desaparecidos, no solo de la Comuna 13 , si en general de Medellín".

Al mismo tiempo la defensora, resalto que "en  la Comuna siempre hacemos memoria y no tendemos a olvidar, y por eso siempre recordamos esta fecha"; sin embargo, destaca que este es un evento indiferente para la ciudadanía, **porque aún falta mucho trabajo desde las instituciones gubernamentales y medios de información en la búsqueda de la verdad y la construcción de memoria**, según Galeano porque principalmente los perpetradores fueron agentes estatales y no quieren que salga esto a la luz pública".

### "hoy en día muchas de las víctimas nos hemos encargado de esta tarea, de visibilizar" 

Las víctimas de la Operación Orión consideran que tienen esperanza en el Sistema Integranl de Verdad Justicia Reparación y no Repetición, "el sistema integral nos genera una luz de esperanza, donde depositamos nuestra confianza, porque hemos visto en la JEP, centralidad  de las victimas cuando el estado fue un compromiso que abordó en el punto 5 de los acuerdos de paz, eso no lo vimos por parte del gobierno, pero ahora sí".

Por tal motivo esperan que el **próximo 18 de noviembre** cuando se de la primera audiencia que tendrán en este espacio, se pueda iniciar un camino en búsqueda de la verdad,"con el sistema integral logramos desde el movimiento una audiencia pública el 18 de septiembre , donde queremos visibilizar el contexto de lo ocurrido y en especial los daños y acciones generados por los agentes".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
