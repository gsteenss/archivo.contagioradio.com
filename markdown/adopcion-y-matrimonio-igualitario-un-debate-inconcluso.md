Title: "Adopción y matrimonio igualitario, un debate inconcluso"
Date: 2015-05-15 17:40
Author: CtgAdm
Category: LGBTI, Nacional
Tags: Adopción igualitaria, Colombia Diversa, Foro Igualdad, Funcación Buen Gobierno, Homosexualidad, Igualdad, Inclusión, matrimonio igualitario, Monseñor Juan Vicente Córdoba, Universidad de los Andes
Slug: adopcion-y-matrimonio-igualitario-un-debate-inconcluso
Status: published

##### [Foto: Colombia-diversa.org] 

###### [15 May 2015]

El pasado 14 de Mayo se llevó a cabo el Foro Igualdad **“Adopción y matrimonio igualitario, un debate inconcluso”** apoyado por **Colombia Diversa**, la Universidad de Los Andes y la Fundación Buen Gobierno, en el que intervinieron varios personajes influyentes en el contexto social, religioso, político, entre otros, haciendo énfasis en la posición desde su campo y su perspectiva sobre el debate planteado.

Entre las intervenciones más relevantes, estuvo la congresista de Alianza Verde, Claudia López, que habló desde su experiencia personal; más allá de su ideología política, expresó su inconformidad con la vulneración de los derechos de las mujeres lesbianas en el país, haciendo énfasis en que “*La principal promesa de una democracia es que todos seamos considerados personas y ciudadanos iguales ante la ley y el Estado*”, pero aun así se sigue evidenciando la discriminación hacia las minorías, de lo que ella misma ha sido víctima.

La congresista aseguró que por la discriminación ha tenido que acudir a escenarios que representan mayorías como el congreso, para promover la causa de la igualdad. “***No es un capricho la lucha que adelanta la comunidad LGBTI***” puntualizó.

Por su parte, el Monseñor Juan Vicente Córdoba, mostró su posición desde sus creencias y convicciones religiosas, abriendo el panorama hacia un catolicismo mucho más incluyente, por medio de intervenciones en las que aseguraba que la sexualidad no afecta de ninguna manera las creencias de una persona; referente al tema de adopción y matrimonio igualitario expresó, "no es de ganar batallas por genitalidad -una pelea de penes o vaginas- esto no son peleas de ese estilo. Queridos hermanos y hermanas, homosexuales y lesbianas, la Iglesia los ama y somos pastores de muchos de ustedes".

A pesar de mostrarse a favor de los derechos de la comunidad LGBTI, resulta contradictorio que no se incluya el concepto de familia para personas homosexuales en la iglesia, de acuerdo a Monseñor Córdoba, "No nos oponemos a que hombres o mujeres homosexuales vivan juntos y que se amen pero en la Iglesia a eso no lo llamamos ni matrimonio mi familia".
