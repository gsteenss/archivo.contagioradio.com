Title: Referendo de adopción no es solo contra gais sino también contra solteros
Date: 2016-12-14 16:51
Category: LGBTI, Nacional
Tags: Adopcion, LGBTI, referendo
Slug: referendo-de-adopcion-no-es-solo-contra-gais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Adopcion-igualitaria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

###### [14 Dic. 2016 ] 

Pese a que hace cerca de un año la Corte Constitucional le dio su aval en una sentencia histórica, a la adopción por parte de parejas del mismo sexo, **sectores políticos del país continúan oponiéndose a esta posibilidad.**

En esta ocasión el Senado aprobó un referendo que pretende modificar la Constitución de Colombia, para que solamente las parejas heterosexuales puedan adoptar. **En lo que han considerado como un \#ReferendoDiscriminatorio (tendencia promovida en Twitter), las organizaciones sociales y de la comunidad LGBTI han expresado su rechazo.**

Y es que fueron **53 votos a favor y 21 en contra para que este referendo tuviera luz verde,** lo que se traduce en que solamente resta que la Cámara de Representantes le dé su aprobación para que se lleve a cabo. La fecha establecida es el próximo mes de marzo.

Marcela Sánchez, directora de Colombia Diversa asegura que es importante saber que **este proyecto de ley no solamente pretende prohibir a las parejas gais sino a los solteros** “es decir, este referendo también prohibiría que solteros heterosexuales adopten”. Le puede interesar: [Buscan prohíbir adopción a parejas homosexuales y personas solteras](https://archivo.contagioradio.com/proyecto-de-referendo-pretende-prohibir-adopcion-igualitaria-y-de-personas-solteras/)

Para Sánchez hasta el momento no es muy claro cuáles son los beneficios de este proyecto para los niños y las niñas “ (...) de qué manera cambia la actual situación que tienen los niños abandonados”.

**Según Marcela Sánchez este proyecto estaría creando dos tipos de niños y niñas, lo que se traduciría en una discriminación** “por un lado están los niños que tienen familia, es decir yo – Marcela Sánchez – podría adoptar a mi sobrino aunque fuera soltera, pero siendo igualmente soltera no puedo adoptar un niño que este en el Bienestar Familiar, ¿ahí cuál sería el criterio de diferenciación con los niños?”.

Aunque el movimiento LGBTI, una vez conocida esta iniciativa del referendo ha intentado discutir con estudios científicos y argumentos con los diversos sectores el por qué no se debe gestionar este tipo de leyes que podrían ser regresivas, Sánchez asegura que “a ellos -promotores del referendo- no les importna los argumentos sino su propia experiencia en la crianza de los hijos”.

Ante este panorama, Marcela Sánchez concluye diciendo “**es importante mirar que hay hacer una movilización con todas las personas sean gais o no lo sean pues porque tenemos que movilizar a nuestros compañeros de trabajo, familias a las personas que piensan distinto** (…) hay que alzar las voces de otras personas que son distintas, independientemente de su orientación sexual”. Le puede interesar: [Referendo contra la adopción busca hacer de la religión una política pública](https://archivo.contagioradio.com/referendo-contra-la-adopcion-busca-hacer-de-la-religion-una-politica-publica/)

<iframe id="audio_14995567" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14995567_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
