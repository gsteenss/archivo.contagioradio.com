Title: No aparecen los vídeos que prueban agresión de ESMAD contra estudiante de UDistrital
Date: 2016-05-02 14:29
Category: DDHH, Nacional
Tags: estudiante UDistrital en coma, paro universidad distrital, Universidad Distrital
Slug: habrian-borrado-videos-que-pobrarian-agresion-de-esmad-contra-estudiante-de-udistrital
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/ESMAD.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RED Colombia ORG ] 

###### [2 Mayo 2016]

Según denuncia Bryan Silva, estudiante de Ciencias Sociales de la Universidad Distrital, los vídeos en los que se prueba que el alumno Miguel Ángel Barbosa fue agredido por una granada aturdidora lanzada por integrantes del ESMAD, fueron **borrados por funcionarios de la institución**, mientras que Barbosa continúa en estado de coma inducido.

Hasta el momento **ningún directivo de la Universidad ha manifestado su posición frente al estado de salud del estudiante**, ni al [[actuar desproporcionado del ESMAD](https://archivo.contagioradio.com/en-estado-de-coma-estudiante-udistrital-agredido-por-esmad/)], el único pronunciamiento ha sido el del Consejo Superior que ha exhortado a los estudiantes para que no se manifiesten.

Entretanto alumnos y docentes acordaron entrar en paro indefinido, ante la negativa del Consejo Superior de derogar el acuerdo 001 de 2016 en el que definen la asignación del rector, sin tener en cuenta la [[reforma universitaria de 2014](https://archivo.contagioradio.com/consejo-superior-impide-eleccion-democratica-del-nuevo-rector-de-u-distrital/) ]en la que **se acordó que el cargo se aprobaría mediante voto popular**.

En el marco de este cese de actividades en las distintas sedes de la Universidad se han programado **asambleas, movilizaciones, foros y talleres**, para dar a conocer la difícil situación por la que atraviesa la institución.

<iframe src="http://co.ivoox.com/es/player_ej_11383332_2_1.html?data=kpagmpiXd5Ohhpywj5WYaZS1lZiah5yncZOhhpywj5WRaZi3jpWah5yncaPmwt7O0JC3rc3qwpCajarXuNbYysbb1sqRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]]
