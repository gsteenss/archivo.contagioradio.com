Title: Mandatarios no deberían tenerle miedo a la revocatoria
Date: 2017-05-08 17:09
Category: Nacional, Política
Tags: Consejo Nacional Electoral, Revocatoria a Peñalosa
Slug: mandatarios-no-deberian-tenerle-miedo-a-las-revocatorias
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/03/revocatoriaestampida.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Unidos Revocaremos a Peñalosa] 

###### [08 May 2017] 

El próximo 17 de mayo se llevará a cabo una audiencia pública citada por el Consejo Nacional Electoral para discutir la posible reglamentación de la revocatoria de gobernadores. Sin embargo, Alejandra Barrios, directora de la Misión de Observación Electoral señala que, **si bien es cierto que este mecanismo debe estar bajo ciertos parámetros, este no es el momento para regularla.**

La reglamentación que buscaría hacer el Consejo Nacional Electoral pretendería que los argumentos por los cuales se convoca a una revocatoria puedan ser verificados a través de pruebas sobre los incumplimientos a los planes de gobierno. No obstante, **de ser aprobada la ponencia, las modificaciones tendrían efecto sobre los procesos de revocatoria actuales como la de Enrique Peñalosa.**

Alejandra Barrios expresó que si bien es cierto que hay que reglamentar no es debido que se haga después de la entrega de las más de 600 mil firmas recolectadas contra la administración de Peñalosa “el mensaje más importante es que los **mandatarios, en lugar de tenerle miedo a las revocatorias, deberían aprovechar esos escenarios para presentar los avances o ajustes de acuerdo a lo que los ciudadanos** señalan “afirmó. Le puede interesar: ["Con movilizaciones y acciones legales defenderán revocatoria contra Peñalosa"](https://archivo.contagioradio.com/mas-de-600-mil-firmas-expresan-insatisfaccion-hacia-gobierno-de-penalosa/)

Además, agregó que lo que se tiene que establecer son los indicadores o herramientas que permitan medir si se está cumpliendo o no con un programa de gobierno, que al ser tan genéricos en su formulación no permiten esta medición. Por el contrario, **Barrios expresó que los Planes de desarrollo si pueden tener herramientas que permitan denotar su avance o no.**

Sin embargo, la directora de la MOE deja abierta la pregunta sobre si sería el Consejo Nacional Electoral el órgano encargado de revisar si una gobernación o alcaldía está cumpliendo con su plan de desarrollo o programa de gobierno, en términos de la responsabilidad y las funciones que tiene esta autoridad. Le puede interesar: ["Comités de revocatoria a Peñalosa prometen entregar un millón de firmas"](https://archivo.contagioradio.com/comites-de-revocatoria-buscan-recolectar-un-millon-de-firmas-contra-penalos/)

<iframe id="audio_18563820" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_18563820_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
