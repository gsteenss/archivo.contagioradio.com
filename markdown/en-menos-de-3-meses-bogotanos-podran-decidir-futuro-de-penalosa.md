Title: En menos de 3 meses bogotanos podrán decidir futuro de Peñalosa
Date: 2017-08-02 13:06
Category: Nacional, Política
Tags: Bogotá, Comité revocatoria, Enrique Peñalosa, revocatoria
Slug: en-menos-de-3-meses-bogotanos-podran-decidir-futuro-de-penalosa
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/08/revocatoria-e1501697040830.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Partido Comunista Colombiano] 

###### [02 Ago 2017] 

El Comité Unidos Revocamos a Peñalosa señaló que la decisión de dejar en firme el proceso de revocatoria, “era previsible” y **celebran que se haya ratificado la decisión de la Sala Penal del Tribunal Superior de Bogotá** el 7 de junio de este año. El siguiente paso, según el Comité, "es comenzar a hacer pedagogía en la ciudad para revocar a Peñalosa en las urnas".

Para Carlos Carrillo, integrante del Comité Unidos Revocamos a Peñalosa, **“el fallo era previsible, cualquier otra cosa era violar la constitución”**. Manifestó además que la defensa del Alcalde está “buscando hacer algo ilegal porque el mecanismo de revocatoria está contemplado en la constitución y protegido por una ley estatutaria”. (Le puede interesar: ["La estrategia de Peñalosa para retrasar proceso de revocatoria"](https://archivo.contagioradio.com/hay-un-intento-por-demorar-revocatoria-en-bogota-unidos-revocamos-a-penalosa/))

De igual manera, estableció una vez más que, las medidas de Peñalosa al querer verificar las firmas recogidas para el proceso, es un intento por dilatar y “esperan un salvavidas para el alcalde”. A los ojos de Carrillo y del Comité para la revocatoria, este proceso es imparable y **garantizan que los bogotanos irán a las urnas en menos de 3 meses**.

En cuanto a la revisión que busca hacer Peñalosa de las más de 500 mil firmas recogidas por el Comité de la revocatoria, **Carrillo afirmó que es un proceso muy complejo**. Indicó que el alcalde tiene poco tiempo para encontrar un grupo de grafólogos y “hay que ver de dónde saca la plata para pagarlos, que esperamos no sea del bolsillo de los bogotanos”. (Le puede interesar:["Proceso de revocatoria no se ha suspendido": Comité Unidos Revocamos a Peñalosa"](https://archivo.contagioradio.com/proceso-de-revocatoria-no-se-ha-suspendido-comite-revoquemos-a-penalosa/))

Mientras que se cumple el plazo de 15 días para revisar las firmas recogidas por parte de la defensa de Peñalosa, Carrillo aseguró que el **Comité de la revocatoria va a comenzar a realizar campañas pedagógicas** para que la ciudadanía conozca las razones y los argumentos por los cuales se debe revocar al alcalde, “Ya hay una inconformidad en la ciudad y se necesita que las personas conozcan las razones de esa inconformidad y que no se dejen engañar por la alcaldía” afirmó Carrillo.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
