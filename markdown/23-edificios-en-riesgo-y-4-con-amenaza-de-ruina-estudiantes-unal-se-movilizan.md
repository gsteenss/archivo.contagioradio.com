Title: Universidad Nacional se "raja" en infraestructura
Date: 2016-08-25 14:44
Category: Educación, Nacional
Tags: crisis presupuestal, educacion, Universidad Nacional
Slug: 23-edificios-en-riesgo-y-4-con-amenaza-de-ruina-estudiantes-unal-se-movilizan
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/universidad-nacional-se-cae-10.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [23 Ago 2016] 

Los estudiantes de la **Universidad Nacional** se movilizan durante esta semana exigiendo **mayor financiación y atención a la [[crisis](https://archivo.contagioradio.com/espantapajarracos-ahuyentaran-a-aguilas-negras-en-la-universidad-nacional/)] de infraestructura** que se viene presentando desde hace por lo menos 3 años y que ahora se agudiza, como es evidente en el edificio de artes, en el que se dictan las clases de cine y televisión, artes plásticas y arquitectura, según afirma Álvaro Díaz, representante estudiantil.

De acuerdo con el representante, la construcción de un nuevo edificio para la Facultad de Artes se ha calculado en \$92 mil millones de los que han sido aprobados \$30 mil millones, por lo que se hace urgente que se defina de qué forma se garantizará iniciar y culminar las obras, teniendo en cuenta que los estudiantes han llegado al punto de **tratar de sostener una de las salas del edificio con palos de madera**.

La administración de la Universidad presentó recientemente un informe sobre la situación de infraestructura, con el que evidenció que 49 edificios están en estado de fragilidad, 23 en riesgo de vulnerabilidad y 4 en amenaza de ruina y en la facultad de artes las grietas son de por lo menos 5 metros de largo por 7 centímetros de ancho. Según este informe el costo de **restauración de la planta física estaría por el orden de los \$2 billones**.

Por su parte Jaime Franky, vicerrector de la Universidad, asegura que **es urgente invertir en la infraestructura**, pues no sólo es el mal estado del edificio de artes, sino el de varias instalaciones como la concha acústica; obras para las que han destinado \$60 mil millones, de los que \$33 mil millones se invertirán en la construcción de nuevos edificios y \$30 mil millones en adecuaciones.

A esta problemática se suma la posibilidad de que algunos de los predios del Alma Mater podrían ser parte de las arcas de [constructoras privadas que se adueñarían de buena parte de los terrenos del centro de estudios](https://archivo.contagioradio.com/predios-de-la-universidad-nacional-quedarian-en-manos-de-inmobiliaria-privada/), algunos de ellos sobre los que se han proyectado obras de ampliación del hospital universitario y la facultad de medicina.

Contagio Radio recorrió algunos edificios de la **Universidad Nacional** y esto fue lo que encontró:

\

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU) ]] {#reciba-toda-la-información-de-contagio-radio-en-su-correo-o-escúchela-de-lunes-a-viernes-de-8-a-10-de-la-mañana-en-otra-mirada-por-contagio-radio .rtejustify}
