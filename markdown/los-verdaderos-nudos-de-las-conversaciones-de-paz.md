Title: Los verdaderos "nudos" de las conversaciones de paz. Entrevista a Carlos Medina
Date: 2015-06-01 14:11
Category: Entrevistas, Paz
Tags: carlos medina gallego, cese bilateral del fuego, conversaciones de paz, Cuba, ELN, FARC, Noruega, Timoleon Jimenez
Slug: los-verdaderos-nudos-de-las-conversaciones-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Carlos-Medina-Gallego-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: contagioradio.com 

<iframe src="http://www.ivoox.com/player_ek_4571148_2_1.html?data=lZqkk5aYfI6ZmKialJqJd6KmkpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2Fo08rjy9jYpYy3wtfZ0diPkcbYytPOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Entrevista: Carlos Medina] 

Según Carlos Medina Gallego, integrante del Centro de Pensamiento de la Universidad Nacional, a pesar de la crisis en la que se dice está inmerso el proceso de conversaciones de paz, lo realmente preocupante son una serie de temas cruciales o "nudos", que son los que verdaderamente tienen enfrascadas las discusiones.

**Sobre los países garantes**  
**  
Carlos Medina Gallego** del **Centro de pensamiento de la Universidad Nacional** asegura que los países garantes juegan un papel muy importante en el desarrollo de este proceso; lo han hecho con discreción y apuntando a que las conversaciones que se desarrollan en la Habana marquen una ruta de éxito que les permita a los colombianos encontrarse en un ambiente sin conflicto armad. Han sido anfitriones del proceso y han brindado todo su apoyo.

Según Medina, el papel de los países garantes es garantizar que continúe le proceso, y por ello no está de acuerdo con las voces que la semana pasada criticaron la posición de los mismos, y afirmaban que pidiendo el cese bilateral, Cuba y Noruega habían tomado partido a favor de una de las partes. En este caso, de lo que se trata es de proteger el proceso de conversaciones de paz, asegura Medina.

**Los nudos del proceso  
**  
Frente a algunos sectores sociales, el proceso de conversaciones de paz se encuentra en un punto difícil por dos razones: una de ellas la dilación y extensión en el tiempo de las conversaciones, que se resolvería definiendo una fecha límite para la firma de acuerdos, y el otro problema es que los hechos de la guerra demostrarían que no hay voluntad de paz por parte de las FARC que han seguido con su accionar "terrorista".

Sin embargo, Medina afirma que esos no son los problemas reales, sino los que se muestran a la opinión pública para generar una postura a favor de la postura del Estado.

Medina se refiere a dos nudos: El punto de las víctimas y el tema de justicia transicional en relación con ellas. Según el analista, las FARC sí son una de las organizaciones responsables del conflicto en Colombia, pero no la única. Para Medina hay 7 responsables que debe asumir su responsabilidad totalmente, sólo así se lograría una verdadera justicia.

1\. Estado colombiano, por su política de seguridad y defensa al ser el garante de **DDHH y Derechos Internacionales Humanitarios. **2. Fuerzas Militares, con actitudes específicas frente al conflicto. 3. Insurgentes **FARC** y **ELN.** 4. **Paramilitares,** por ser responsables de crímenes de desplazamiento forzado. 5. Empresarios ganaderos y comerciantes que financiaron grupos ilegales. 6. Empresas transnacionales, que también financiaron el desarrollo de la guerra en Colombia. 7. **EEUU,** que genera la política de seguridad a lo largo de 60 años que hizo que la población se convirtiera en blanco y como elemento adicional, los partidos políticos que se beneficiaron de la parapolítica.

El otro nudo es el tema de la verdad: Para Medina Gallego, solamente con la aceptación de la responsabilidad por parte de todos los actores del conflicto y la posibilidad de asumir esas responsabilidades frente a la sociedad en general se podría desatar ese nudo. El problema radica en que se está queriendo mostrar como el único responsable a las FARC-EP.

**Cambios en el gobierno “oxigenarían el desarrollo de conversaciones de paz”**

Timoleón Jiménez, comandante de las FARC, emitió un comunicado en el que afirma que la presencia de la canciller Angela Holguín y el cambio del ministro de defensa van a darle un impulso a la paz, pero que esto no significa que se van a parar de la mesa de conversaciones y que es necesario perseverar con esperanza y tranquilidad.

Carlos Medina asegura que Luis Carlos Villegas sabe cómo funciona la mesa de conversaciones, conoce la delegación de las FARC, y es bueno que él ocupe el Ministerio con un propósito importante en materia política que es construir y sacar adelante el proceso de paz y persuadir a los enemigos internos; se deben llegar a acuerdos políticos no militares.

Por su parte, la llegada de la canciller Ángela Olguín conduce a que se produzca la repatriación de Simón Trinidad y de otros guerrilleros que están extraditados. Sería útil que hicieran parte de la mesa de conversaciones para oxigenar el desarrollo de estas conversaciones.
