Title: Paramilitares arremeten en diferentes regiones del país
Date: 2017-02-13 18:07
Category: DDHH, Otra Mirada
Tags: Fuerza Pública, Paramilitarismo
Slug: paramilitares-arremeten-contra-diferentes-regiones-del-pais
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/Paramilitarismo1-e1459970325314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo Particular] 

###### [13 Feb 2017] 

Despues de que se esperaba que Colombia tuviese un respiro de la guerra y el conflicto armado**, una fuerte arremetida de paramilitares se vivió este fin de semana en  diferentes regiones del país**, el territorio colectivo de Nueva Esperanza EM Dios en Cacarica, en Chocó; comunidades del Catatumbo, en Norte de Santander;  indígenas del Resguardo Wounnam, en el Bajo Calima, en Valle del Cauca y comunidades del Putumayo denunciaron la presencia y accionar de estos grupos  y la violación a derechos humanos de la que fueron víctimas

### **Zona Humanitaria de Nueva Esperanza, Cacarica, en Bajo Atrato - Chocó** 

El domingo, un grupo de **paramilitares ingresó a la Zona Humanitaria portando armas largas y vistiendo camuflados**, allanaron casa por casa buscando a personas que tenían en una lista y que se habrían negado a ser parte del reclutamiento de este grupo, posteriormente, amenazaron con volver al territorio hasta encontrarlos, hecho que generó miedo entre los habitantes de la comunidad.

Esta situación tiene como agravante, el **conocimiento por parte de las Fuerzas Militares e instituciones de que la incursión paramilitar iba a suceder,** debido a  las alarmas y denuncias que ya habían hecho los campesinos y organizaciones defensoras de derechos humanos.

**La presencia del ejército en la Zona Humanitaria se dio después de 8 horas, cuando los paramilitares ya habían abandonado el territorio**. La comunidad está pidiendo una comisión de verificación para evitar nuevos desplazamientos o desapariciones en la región. Le puede interesar: ["Con lista en mano paramilitares amenazan a pobladores de Cacarica"](https://archivo.contagioradio.com/paramilitares-amenazan-pobladores-cacarica-36208/)

![INFOGRAFIA CORRECION](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/INFOGRAFIA-CORRECION.jpg){.alignnone .size-full .wp-image-36372 width="620" height="848"}

### **Putumayo** 

Comunidades del Putumayo denunciaron la **aparición de panfletos firmados por grupos que se autodenominan como Autodefensas Unidas**, en donde amenazan de muerte a quienes infrinjan los horarios, anuncian las nuevas “limpiezas sociales” y el cobro de vacunas a los habitantes de Mocoa y zonas aledañas.

De igual forma, el defensor de derechos humanos Carlos Fernández, señaló que estructuras paramilitares están anunciando en diferentes zonas de esta región, que harán **reclutamientos, permanecerán en los territorios y cobrarán vacuna por cultivos de coca y otras actividades que “son el trabajo habitual de las comunidades”**

La Policía Nacional departamental del Putumayo indicó que tanto el panfleto como mensajes de whatsapp que están circulando son **“falsos y fueron creados por personas inescrupulosas** que solo quieren acabar con la tranquilidad de la comunidad”. Le puede interesar:["Comunidades denuncian amenazas paramilitares"](https://archivo.contagioradio.com/comunidades-del-putumayo-denuncian-amenazas-paramilitares/)

### **La Gabarra, Norte de Santander** 

Campesinos e Indígenas denunciaron la presencia de grupos que se identificaron como Águilas Negras, en la vereda Neiva, en cercanías a La Gabarra en el departamento del Norte de Santander, que amenazaron con **“ocupar los territorios que dejaron las FARC-EP y con sacar de estos lugares a los campesinos que no quisieran unirse a ellos”.**

Por estos hechos, aproximadamente **mil campesinos rodearon la caravana de las FARC-EP que se dirigía hasta el punto de Caño Indio**, para exigirle al gobierno garantías de vida a las comunidades y presencia por parte de las Fuerzas Militares.

Sin embargo, tanto el ministro de Defensa Juan Carlos Villegas, como el Comandante de las Fuerzas Vereda Conjunto Vulcano, señalaron que estos hechos no son verdad. Los campesinos e indígenas **convocaron la presencia de la Comisión de Seguimiento e Impulso a la Verificación e Implementación del Acuerdo y del Consejo Nacional de Reincorporación. **Le puede interesar:["Presencia de paramilitares impide llegada de FARC-EP a zona veredal"](https://archivo.contagioradio.com/presencia-paramilitar-impide-llegada-de-farc-ep-a-zona-veredal/)

### **Bajo Calima, Valle del Cauca** 

El Gobernador del Resguardo Indigena Wounaan Nonam Santa Rosa de Guayacán, del Bajo Calima, denunció que desde la madrugada del sábado 10 de febreo, inició el **desplazamiento forzado de 31 familias, producto de las constantes operaciones armadas criminales**, de lo que se presume son grupos paramilitares que rondan el Resguardo Humanitario Biodivierso y zonas aledañas.

De acuerdo con el gobernador, desde hace 15 días se había denunciado la presencia de estos grupos armados en el territorio, que portan armas largas y visten de negro quienes los han hostigado, **amenazado e incluso maltratado y torturado como en el caso del comuner**o[ Jose Cley Chamapuro](https://archivo.contagioradio.com/paramilitares-torturaron-a-indigena/) ocurrido el pasado 4 de febrero.
