Title: Luis Bedoya y la corrupción del fútbol en Colombia
Date: 2015-12-07 16:30
Category: Judicial, Nacional
Tags: Concacaf, corrupción, Federación Colombiana de Fútbol, FIFA, FIFA-Gate, Luis Bedoya, Rafael Callejas
Slug: luis-bedoya-y-la-corrupcion-del-futbol-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/12/Fotos-Vanguardia-y-la-prensa-e1449506929940.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Fotos: Vanguardia y La Prensa. 

###### [7 Dic 2015] 

A partir del 27 de mayo de 2015 se dio a conocer el caso ‘FIFA-Gate’, que podría ser el escándalo de corrupción más grande en el mundo del deporte, en donde entre otros, existen casos multimillonarios de sobornos, chantajes, fraude, lavado de dinero y pago de favores del que no salen bien librados los altos mandos del organismo internacional. Figuras como Joseph Blatter (presidente), Michel Plattini (presidente de la UEFA y uno de los futbolistas franceses más talentosos de la historia), directores de las federaciones y confederaciones han estado en el ojo del huracán por el proceso jurídico llevado a cabo por la Fiscalía de Nueva York.

A ese ‘selecto grupo’ ha ingresado recientemente Luis Bedoya. El presidente de la Federación Colombiana de Fútbol entre los años de 2006 y 2015, confesó ante un tribunal de los Estados Unidos, los delitos de fraude en transferencia bancaria y conspiración de soborno.  Bedoya, junto con dirigentes como Marco Polo del Nero, Eduardo Deluca, José Luis Meiszner, Romer Osuna, Ricardo Teixeira, Reynaldo Vásquez, Juan Ángel Napout, Manuel Burga, Carlos Chávez y Luís Chiriboga han sido sindicados de recibir sobornos por parte de las corporaciones Datisa, Traffic y Torneos y Competencias para recibir los derechos de transmisión de copas internacionales.

En ese sentido, también se señala que Bedoya tiene una cuenta secreta en Suiza, que usó durante más de 8 años para recibir los réditos provenientes de esos negocios. Según afirman medios y expertos en justicia, el ex máximo rector de la Federación Colombiana de Fútbol, enfrentaría una pena de 20 años de cárcel que podría reducirse por su colaboración con la justicia.

El que también tiene que dar explicaciones a la opinión pública y a la justicia estadounidense, es el expresidente de la República de Honduras Rafael Callejas en el periodo 1990-1994 y presidente de la Federación Nacional Autónoma de Fútbol de Honduras durante 13 años, quien declaró que el próximo martes 8 de diciembre dará detalles sobre su versión del FIFA-Gate, aunque hasta el momento ha reiterado que es inocente.

Sin embargo, las acusaciones hacia Callejas no son nuevas: desde su etapa como Jefe de Estado fue acusado de siete casos de abuso de poder y malversación de fondos públicos. Ahora, desde Estados Unidos se ha solicitado su extradición por recibir sobornos de MEdiaWorld por más de 600 millones de dólares para la transmisión exclusiva de los partidos de la Selección Nacional de Honduras.

Aún queda tela por cortar en este caso de corrupción global que permea a la dirigencia del deporte más popular del mundo. Sin embargo, las capturas masivas, las acusaciones de la Fiscalía de Nueva York y las investigaciones que todavía están pendientes a los altos mandos de la FIFA garantizan que el caso FIFA-Gate no concluya en el mediano plazo.
