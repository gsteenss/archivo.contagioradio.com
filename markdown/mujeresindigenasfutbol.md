Title: En Mapiripán las mujeres indígenas juegan fútbol por su dignidad
Date: 2017-06-17 10:16
Category: Otra Mirada
Tags: colombia, indígenas, mapiripan, mujer
Slug: mujeresindigenasfutbol
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/IMG-20170618-WA0018.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Justicia y Paz 

###### 17 Jun 2017 

Con el propósito de fortalecer los lazos entre las comunidades indígenas Jiw y Sikuani,  del municipio de Mapiripán en el Departamento del Meta, unidas históricamente por la lucha en exigencia de sus derechos, del 17 al 19 de junio tiene lugar el campeonato de fútbol de mujeres.

![fútbol indígena](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/06/IMG-20170618-WA0016.jpg){.alignnone .size-full .wp-image-42408 .aligncenter width="1000" height="600"}

Un total de 12 equipos, 8 pertenecientes a la comunidad Jiw y 4 a la Sikuani, se darán cita en el torneo realizado posterior al encuentro de mujeres indígenas, que busca encontrar puntos de unión entre las comunidades para apoyarse y defenderse ante los abusos a la dignidad de la mujer indígena que se han presentando en el municipio.

El compartir de las mujeres servirá para acerca aún más a dos comunidades que históricamente han sido oprimidas y hoy juntas exigen sus derechos, particularmente por la ampliación del resguardo Sikuani y la adjudicación de tierras para la comunidad Jiw. Le puede interesar: [Mujeres en Colombia están viviendo una tragedia humanitaria](https://archivo.contagioradio.com/mujeres-en-colombia-estan-viviendo-una-tragedia-humanitaria-fdim/).
