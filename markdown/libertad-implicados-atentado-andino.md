Title: Juez ordena libertad de 11 implicados en atentado al centro comercial Andino
Date: 2018-08-24 17:41
Category: DDHH, Judicial
Tags: Andino, Juez, Libertad
Slug: libertad-implicados-atentado-andino
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/Captura-de-pantalla-2018-08-24-a-las-5.34.20-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @MantillaIgnacio] 

###### [24 Ago 2018] 

Juez ordenó levantar la medida de aseguramiento impuesta a 11 de los implicados en el caso del atentado al Centro Comercial Andino de Bogotá, que tuvo lugar en junio del año pasado. La decisión se produce tras más de 1 año privados de estar privados de la libertad sin que se resolviera su situación jurídica, sumado a la petición de retención por parte de la Fiscalía presentada de forma extemporánea, que el juez decidió no aceptar.

Algunos de los sindicados de pertenecer al Movimiento Revolucionario del Pueblo, ya habían expresado las irregularidades que se cometieron en su proceso de acusación y captura, e igualmente en las pruebas que se presentaron durante su caso. (Le puede interesar:["La versión de los jóvenes capturados por el atentado en el centro comercial Andino"](https://archivo.contagioradio.com/entrevista-a-boris-rojas-y-lizeth-rodriguez-capturados-por-atentado-a-centro-comercial-andino/))

Noticia en desarrollo...

###### [Reciba toda la información de Contagio Radio en] [su correo](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por ][Contagio Radio](http://bit.ly/1ICYhVU). 

<div class="osd-sms-wrapper">

</div>
