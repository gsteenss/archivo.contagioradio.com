Title: La labor del periodista en el conflicto armado colombiano
Date: 2016-02-09 08:00
Category: Entrevistas
Tags: Día nacional del periodista, John Lee Anderson
Slug: la-labor-del-periodista-en-el-conflicto-armado-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/jon_lee_anderson.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: runrun.es 

<iframe src="http://www.ivoox.com/player_ek_10360194_2_1.html?data=kpWgmJWVfZWhhpywj5adaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5ynca3VjNHOxNTWb8XZzZDdx9fNs8WZpJiSo6nXuMKfxtOYx9GPp9Dix9HWxdnTb8LmzsbR0ZDHs83jzsfWj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [8 Feb 2016]

Jon Lee Anderson, reportero especializado en temas de latinoamérica, con amplia experiencia en el cubrimiento del conflicto en países como Honduras, El Salvador e Irlanda del Norte, hablo con Carolina Garzón de Contagio Radio, sobre la labor del periodista en tiempos de guerra y la importancia que tendrá su trabajo luego de una eventual firma de paz en Colombia.

En el contexto de conflicto interno en los países, Lee Anderson manifiesta que el periodista es “clave” para entender e interpretar la realidad para un público que no necesariamente puede hacerlo por sí mismo, gracias a la posibilidad de “observar de cerca los procesos” e informarlo con buenas intenciones.

**La labor del periodista durante y después del conflicto.**

El reportero considera un derecho y un deber el trabajo informativo con el propósito de aportar a la construcción de juicios de valor necesarios, para que el público “tenga facultades de criterio y saber que pasa delante de ellos” en procesos tan complejos como lo es el final de la guerra y la consecuente construcción de paz en Colombia.

El trabajo de los periodistas es fundamental en la búsqueda de una paz con justicia, que debe llegar de la mano de la transparencia como asegura el reportero, “todos los colombianos se tienen que mirar bien en el espejo y darse cuenta que papel tuvieron y reconocerlo ante los demás en un mundo ideal”, porque a su juicio para sanear a Colombia “se tiene que ventilarlo todo”.

En un conflicto armado de tantos años como el colombiano, muchas son las responsabilidades que deben reconocerse, incluyendo a los medios y periodistas que han fallado en el ejercicio de su labor. Lee Anderson admite que así como ha encontrado casos en que estos se autocensuran por amenazas contra su vida e integridad son varios casos en que lo hacen “por sus relaciones o vínculos de empatía o sangre con grupos armados ilegales”.

A su juicio, considera que es importante que los periodistas puedan hablar con la insurgencia, algo que “antes era mal mirado por la sociedad porque pensaban  que se humanizaba a los terroristas” y que se hacía propaganda a las FARC. Es importante que hoy los líderes de las FARC sean más conocidos y “deben responder preguntas que antes no” incluso las que el comunicador cataloga como “incómodas”.

“Los periodistas tiene que ser parte esencial del proceso tienen que hacer las preguntas incomodas a todo el mundo”, cumpliendo con un rol informativo y no propagandista, aunque hace una excepción personal en el tema de la paz porque éste “no  tiene ideología” asegurando que no puede ser imparcial al respecto y agrega que si por eso “deducen que soy más cercano a Timochenko y a Santos que a Uribe lo acepto (..) yo como John Lee Anderson pienso que no hay nada mejor que la paz con la paz se construye todo sin la paz no se construye nada”.

**Responsabilidad estatal en el origen y desarrollo del conflicto.**

En relación con la naturaleza y origen del conflicto, Anderson asegura que “muchas de las guerrillas en el mundo han surgido producto de la marginalización en situación de desigualdad”, y aunque no comparte la opción armada o violenta considera que no es una decisión que se toma “porque sí” al ser una vida tan difícil donde lo único seguro que se tiene es la muerte.

Para encontrar la paz con justicia es indispensable que el Estado también asuma sus culpas necesario para alcanzar la reconciliación y el perdón “Los estados más sensatos encuentran las fórmulas de aceptar culpas propias y hacer un esfuerzo por ser más inclusivos y traer de vuelta a los hijos perdidos” afirma el reportero añadiendo a partir de su experiencia que en las muertes ocurridas en la mayoría de las guerras civiles América Latina “la guerrilla fue responsable de un porcentaje menor a las producidas por el estado o agentes paralelos como los grupos paramilitares”.

Lee Anderson da crédito a los crímenes cometidos por la insurgencia, pero considera que el Estado “tiene posibilidad moral y legal, tiene que ser pulcro y si tiene jueces, comandantes corruptos deben ir a la cárcel” para alentar a la gente a deponer las armas y salir del monte por la garantía que serán respetados sus derechos y se les dará una mano en temas como el empleo, la educación, y a la justicia amparada en la ley.

Para el periodista es importante que los acuerdos alcanzados en la negociación del proceso de paz sean respetados “el estado colombiano tiene que encontrar la fórmula de cumplir lo prometido”, para lo cual debe crear o acudir a “instituciones regidoras” y solicitar el apoyo de la ayuda de la comunidad internacional.

Al ser increpado durante su presentación en el Hay Festival sobre los presuntos dineros que tendrían los grupos guerrilleros resultado de sus acciones durante estos años, el periodista califica como un “atenuante” para la situación de los paramilitares que no fueron extraditados y que “viven a sus anchas en el país que son gobernadores o están emparentados con gente Pudiente”, sin preguntar dónde está su dinero y quien es el dueño de las tierras que han poseído.

**Lucha contra el narcotráfico.**

Colombia tiene una buena oportunidad de acabar con el negocio del narcotráfico, pero Lee considera que es necesario que el Estado y las FARC trabajen de la mano. De lograrlo en su opinión el país sería una potencia y podría ayudar a ser “un guardián de las leyes, de la justicia social” de lo contrario podría terminar como una mexicanización de Colombia, como ocurrió a la inversa cuando el país centroamericano se “colombianizó” en los años 90.
