Title: Aún se puede defender el espíritu de la JEP
Date: 2018-06-29 11:31
Category: Nacional, Paz
Tags: Derechos Humanos, JEP, Movilización social, paz
Slug: aun-se-puede-defender-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/Captura-de-pantalla-2018-06-29-a-las-3.19.01-p.m..png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @OjoalaPaz] 

###### [28 Jun 2018] 

Aunque el Senado aprobó la ley de procedimiento para la JEP, organizaciones sociales se mostraron inconformes por los artículos incluidos desde el Centro Democrático, que incluyen la creación de una sala especial de justicia para la fuerza pública, la modificación sobre el proceso de extradición y la eliminación del enfoque de género.

### ¿Cómo afecta la creación de una sala especial que juzgue a las fuerzas militares? 

Soraya Gutiérrez, presidenta del colectivo de abogados José Alvear Restrepo (CAJAR) y Coordinadora Jurídica del Movimiento Nacional de Víctimas de Crímenes de Estado (MOVICE) asegura que, con la inclusión de este punto se hace necesario presentar una reforma constitucional que tardaría al menos 18 meses en ser aprobada, razón por la cual, esta sala especial es un obstáculo para que las víctimas de crímenes cometidos por parte de agentes del Estado accedan a su derecho a la justicia. (Le puede interesar: ["¿Se acaban las esperanzas para la JEP?"](https://archivo.contagioradio.com/se-acaban-las-esperanzas-para-la-jep/))

Mientras esta sala especial es puesta en marcha, los procesos que se adelantan contra miembros de la Fuerza Pública quedarían congelados, pese a esto, Gutiérrez afirma que sería la Fiscalía la llamada a continuar con estos casos, puesto que así lo ordena la ley estatutaria de la JEP.

Por su parte, José Antequera, integrante de Hijos e Hijas por Colombia opina que con esta modificación se está atacando el corazón del Acuerdo de Paz, "porque cuando se busca crear una sala para militares lo que se está buscando es romper con ese principio de que los militares tienen responsabilidad sobre hechos terribles del conflicto".

### ¿Qué cambió en el proceso de extradición con la nueva ley? 

El Proyecto de Ley que aprobó el Senado hizo una modificación en el proceso de extradición de quienes son juzgados por la JEP: Ahora, este tribunal no podrá practicar pruebas para determinar si los hechos por los que las personas son pedidas en extradición ocurrieron antes o después de la firma del acuerdo, sino que la limitará a decidir sobre la fecha en que ocurrió el delito.

Esta modificación es relevante, pues si se cometen delitos posterior a la firma del acuerdo, el acusado pierde los beneficios que otorga la JEP. (Le puede interesar: ["Las razones de la JEP para detener la extradición de Jesús Santrich"](https://archivo.contagioradio.com/las-razones-de-la-jep-para-detener-la-extradicion-de-jesus-santrich/))

David Flórez, integrante del partido político FARC, señaló que es una falla al debido proceso que a un tribunal como la JEP se le restrinjan sus capacidades, y se le diga que debe probar únicamente la fecha en que se acusa de cometer los delitos sin poder probar cuando ocurrieron los mismos. De forma tal que, se deja en una situación jurídica débil a los anteriores combatientes de las FARC, e incluso a otros actores del conflicto

### ¿Por qué se elimina del Proyecto de Ley de Procedimiento de la JEP el enfoque de género? 

Con este cambio, Paloma Valencia, senadora del Centro Democrático busca que se excluya del proyecto de ley la sigla 'LGBT', pues como lo han señalado desde antes del plebiscito, para ellos tiene que ver con una ideología de género que se busca imponer en los acuerdos.(Le puede interesar: ["Ideología de género en Colombia y América Latina"](https://archivo.contagioradio.com/39456/))

Flórez, opina que la coalición de gobierno que ganó estas elecciones "va contra la paz, contra las libertades democráticas y evidentemente persigue las libertades sexuales y la diversidad de género". Pero aclara que esto no es algo que ocurrió únicamente con la JEP, dado que, de todas las leyes que se han aprobado en el congreso y tienen que ver con la implementación del acuerdo, ninguna tiene un enfoque de género ni respeta la diversidad sexual.

Sin embargo, Soraya Gutiérrez sostiene que la corte constitucional revisará la ley conforme a demandas que van a presentar  desde el CAJAR, para que estas normas sean declaradas exequibles y contra la constitución: "Vamos a poner este punto como un factor de discriminación, dado que existen unas normas y leyes que protegen a estas comunidades".(Le puede interesar: ["Un año retador para el enfoque de género en el acuerdo de paz"](https://archivo.contagioradio.com/las-mujeres-continuan-apostandole-al-enfoque-de-genero/))

### ¿Qué es lo que se está afectando con estas modificaciones? 

Gutiérrez y Flórez coinciden en afirmar que es finalmente la virtud de producir verdad lo que se esta afectando con estas modificaciones. Ambos, aseveran que el asunto fundamental de la JEP es el establecimiento de responsabilidades sobre los hechos del conflicto, y con la sala especial para la fuerza pública, así como la exclusión de terceros en la JEP, están promoviendo la idea de que solo las FARC fueron responsables del conflicto armado.

Desde el punto de viste de José Antequera, "el tema central es que después de estas modificaciones a la JEP buscarán afectar la participación política de la FARC". Y con este proceso se juega con la virtud de producir derechos para todas las víctimas, además, dice que estos hechos obedecen a un plan de Álvaro Uribe Vélez que busca reconstruir un poder entorno a la violencia.

### Aún se puede defender el espíritu de la JEP 

Pese a lo negativo que pueden resultar los hechos recientes, Gutiérrez y Antequera coincidieron en el papel preponderante que tiene la comunidad internacional: De una parte, en la investigación por parte de la Corte Penal Internacional en la investigación de máximos responsables del conflicto si es que Colombia no avanza en ese proceso. De otra parte, con el apoyo y supervisión del conjunto de países que acompaño el proceso y que debería seguir siendo garantes de la implementación de la paz.

Aunque según Flórez, "la agenda del uribismo es tan agresiva que viene por todo y contra todos" al grado de ir contra las libertades democráticas del Estado de derecho, es importante que se ponga de presente la unidad, la iniciativa política, el respeto por la diversidad y se llegue a una agenda común que permita la resistencia y la propuesta.

Adicionalmente, indica Flores, hay una gran bancada en el congreso que buscará defender los acuerdos de paz y en ese sentido, esa bancada deberá tener un respaldo en la calle. Así que, "es la posibilidad para que el movimiento social se encuentre, y deje la dispersión en la que se encuentra".

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2Fvideos%2F10155528488515812%2F%3Ft%3D91&amp;width=500&amp;show_text=false&amp;appId=894195857389402&amp;height=281" width="500" height="281" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo] 
