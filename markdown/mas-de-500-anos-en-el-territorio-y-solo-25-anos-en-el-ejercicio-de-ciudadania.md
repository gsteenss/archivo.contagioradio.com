Title: "Más de 500 años en el territorio y sólo 25 años en el ejercicio de ciudadanía"
Date: 2016-08-09 15:56
Category: DDHH, Nacional
Tags: Día Internacional Pueblos Indígenas, Proceso de paz en Colombia, Pueblos indígenas en Colombia
Slug: mas-de-500-anos-en-el-territorio-y-solo-25-anos-en-el-ejercicio-de-ciudadania
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/Pueblos-Wayuu-e1479951584703.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Zona Cero ] 

###### [9 Ago 2016] 

Este martes se conmemora el Día Internacional de los Pueblos Indígenas los que durante las últimas décadas en Colombia han tenido que enfrentar el desplazamiento forzado, tras la **constante presencia de grupos armados legales e ilegales en sus territorios**; aumentando el número de refugiados en países como Brasil, Ecuador, Panamá, Perú y Venezuela. De acuerdo con el DANE, los 102 pueblos indígenas colombianos que hablan 65 lenguas, corresponden al 3,4% de la población total.

Aída Quilcué, líder del Consejo Regional Indígena del Cauca, asegura que en el marco del momento histórico que vive Colombia, las comunidades consideran que la paz se evidencia en las distintas iniciativas que han adelantado para lograr su pervivencia, razón por la que **apoyan las negociaciones con la guerrilla de las FARC-EP**, pero llaman la atención porque a su juicio temas como la [[presencia de otros actores armados en el territorio](https://archivo.contagioradio.com/5-indigenas-del-norte-del-cauca-han-sido-asesinados-en-ultimos-dias/)], la reinserción de excombatientes y la constitución de zonas veredales, deben ser concertados con las comunidades.

Por su parte Armando Valbuena, líder Wayúu, considera que los pueblos indígenas llevan 25 años en el ejercicio de su ciudadanía, en el marco de una **Constitución que encarna una política extractivista que ha tenido impactos sociales, económicos y ambientales negativos**. Agrega que los problemas que se enfrentan en La Guajira tienen que ver con la falta de control en la gestión de los recursos públicos, las decisiones que toma autoritariamente el Gobierno nacional frente a proyectos mineros y portuarios, y la [[falta de responsabilidad del ICBF y el Ministerio de Salud](https://archivo.contagioradio.com/no-solamente-son-ninos-es-todo-el-pueblo-wayuu-el-que-se-esta-muriendo/)] en la atención de la población Wayúu.

Desde Arauca el líder Álvaro Hernández Romero, asevera que para los pueblos indígenas de esta región son muy importantes las conversaciones de paz y los acuerdos a los que está llegando la guerrilla de las FARC-EP y el Gobierno nacional, teniendo en cuenta que **esta zona ha sido golpeada por la violencia y que ellos han quedado en el centro del conflicto**, enfrentando problemas asociados a la desatención estatal, la permanencia en el territorio, la soberanía alimentaria, la salud y la educación, principalmente.

<iframe src="http://co.ivoox.com/es/player_ej_12493155_2_1.html?data=kpehm5iVeZahhpywj5WaaZS1lZqah5yncZOhhpywj5WRaZi3jpWah5yncaLdxcaYrc7Wr9bZhpewjai2jaShhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
<iframe src="http://co.ivoox.com/es/player_ej_12493165_2_1.html?data=kpehm5iVepahhpywj5WXaZS1lJaah5yncZOhhpywj5WRaZi3jpWah5yncaLg18bf0ZCsqdPihqigh6aVssXZ24qfpZDQaaSnhqaxxsrWb8rixYqwlYqliMjZz8aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ..&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe><iframe src="http://co.ivoox.com/es/player_ej_12493174_2_1.html?data=kpehm5iVe5Whhpywj5WaaZS1kpeah5yncZOhhpywj5WRaZi3jpWah5yncaLmzsbbxtSPmsLgw9rS0MaJdqSfzYqwlYqliMXZ05DEw96Jh5SZo6bij4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
