Title: En fotos | Marcha en apoyo a la JEP
Date: 2019-03-25 17:15
Author: CtgAdm
Category: Fotografia
Tags: JEP
Slug: en-fotos-marcha-en-apoyo-a-la-jep
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01972.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<figure>
[![DSC01972](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01972-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01972.jpg)

</figure>
<figure>
[![DSC02131](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC02131-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC02131.jpg)

</figure>
<figure>
[![DSC01984](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01984-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01984.jpg)

</figure>
<figure>
[![DSC01625](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01625-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01625.jpg)

</figure>
<figure>
[![DSC02014](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC02014-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC02014.jpg)

</figure>
<figure>
[![DSC01796](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01796-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01796.jpg)

</figure>
<figure>
[![DSC01713](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01713-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01713.jpg)

</figure>
<figure>
[![Defender la Jep](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC02307-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC02307.jpg)

</figure>
<figure>
[![DSC01965](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01965-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01965.jpg)

</figure>
<figure>
[![DSC01989](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01989-770x540.jpg)](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/03/DSC01989.jpg)

</figure>
A la Plaza de Bolívar llegaron hombres y mujeres a defender la JEP luego que el presidente Duque detuviera el avance de la ley estatutaria. El próximo 18 de marzo seguirán las movilizaciones en apoyo al mecanismo de justicia que nació del proceso de paz.  Fotos: Carlos Zea/Contagio Radio
