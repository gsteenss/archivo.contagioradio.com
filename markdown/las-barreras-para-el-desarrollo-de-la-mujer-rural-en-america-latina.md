Title: Las barreras para el desarrollo de la mujer rural en América Latina
Date: 2019-10-15 18:31
Author: CtgAdm
Category: Entrevistas, Mujer
Tags: Campo Colombiano, CIDH, Mujer Rural
Slug: las-barreras-para-el-desarrollo-de-la-mujer-rural-en-america-latina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Agencia-de-Desarrollo-Rural-Mujer.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

En el marco del **Día Internacional de la Mujer Rural**, es valido examinar cuál es la situación que vive día tras día la mujer rural de América Latina y los desafíos a los que se enfrentan en particular en un país como Colombia donde el 51% de la población son mujeres, de las que 5.442.241 habitan en zonas rurales, 37% viven en condiciones de pobreza y 4.85% trabaja en la producción agrícola, según datos aportados por la Agencia de Desarrollo Rural.

Esto a propósito de las recomendaciones y advertencias que ha realizado para esta fecha la **Comisión Interamericana de Derechos Humanos (CIDH)**, exigiendo, se les garanticen las medidas necesarias para erradicar la violencia y la discriminación, dos de las grandes problemáticas que enfrentan las mujeres en el campo y que impiden el goce pleno de sus derechos en comparación con los hombres rurales y con las mujeres y hombres urbanos.

Para **Ana María Restrepo del Centro de Investigación y Educación Popular** (CINEP), a estas problemáticas que afectan la calidad de vida de las mujeres, no solo en Colombia sino en general en América Latina, es necesario sumarles **los daños que ocasionan el extractivismo y los megaproyectos sobre sus cuerpos y su condición de salud**, la persecución a las defensoras de DDHH, en un contexto en que se criminaliza su labor cada vez con más fuerza y una última situación ligada al **fortalecimiento de discursos de derecha que niegan la participación política de las mujeres** y que tienen un mayor impacto en las zonas rurales.

Así las cosas, la CIDH alerta que para las mujeres existe un mayor riesgo de ser víctimas de violencia a causa de la persistencia de actitudes discriminatorias, esto sumado a los desafíos que enfrentan para acceder efectivamente a la justicia, y tolerancia social, problemáticas que a menudo también están ligado a otros fenómenos como el desplazamiento forzado, asesinatos y violencia sexual. [(Le puede interesar: Mujeres Campesinas "rebeldes pero con causa")](https://archivo.contagioradio.com/lmujeres-campesinas-rebeldes-pero-con-causa/)

\[caption id="attachment\_75015" align="aligncenter" width="960"\]![Mujer Rural](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/10/Agencia-de-Desarrollo-Rural-Mujer.jpg){.size-full .wp-image-75015 width="960" height="640"} Foto: Agencia de Desarrollo Rural\[/caption\]

Esta desigualdad, según la Comisión es evidente en el acceso a la tierra, a los recursos naturales, educación y servicios de salud, los que en su mayoría son limitados, por lo que, para Restrepo, "**es muy importante seguir buscando las garantías de derechos de propiedad de la mujer rural"** que aunque no estén negados no favorecen a las mujeres impidiendo que ellas puedan acceder a títulos, créditos o a tener un protagonismo en la toma de decisiones de los territorios.

En la mayoría de los casos, son las mujeres rurales quienes realizan la mayor parte de la carga de trabajo no remunerado en los hogares e incluso cuando tienen un empleo formal, no siempre son cubiertas sus garantías sociales básicas, "si el hombre cultiva es productor pero si ella cultiva es economía doméstica, es importante dar esa discusión en su rol como productora", afirma la investigadora del CINEP. [(Lea también: Mujeres rurales trabajan más que los hombres y ganan menos)](https://archivo.contagioradio.com/mujeres-rurales-trabajan-mas-que-los-hombres-y-ganan-menos/)

### La importancia de la heterogeneidad de la mujer en el campo 

Por su parte, Hada Marlén Alfonso, consejera nacional de Planeación del Sector Mujer Rural, menciona que estas dificultades "son realidades que no han disminuido como se espera en el papel que el Gobierno tiene como garante de derechos", sin embargo resalta que desde los diversos gremios que involucran al campo se han ido involucrando como parte del Estado para incidir en algunas líneas. "En el territorio trabajamos con las compañeras en la reivindicación de las mujeres afro e indígenas, hacemos énfasis el Gobierno tiene que ser asertivo para orientar y hacer efectivos esos enfoques"-

Es por eso, que para superar esta brecha, es clave que el Gobierno consolide datos que permitan conocer la situación de la mujer del campo colombiano y así resaltar  la heterogeneidad de la población y su diversidad de identidades, un llamado que han realizado tanto mujeres campesinas, como indígenas y afrodescendientes, "no se trata de habitar solamente un espacio sino que hay una identidad alrededor de una forma de vida particular". concluye Restrepo.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
