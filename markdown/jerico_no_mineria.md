Title: Concejo de Jericó, Antioquia también le dice NO a la minería
Date: 2017-06-07 16:48
Category: Ambiente, Nacional
Tags: Jericó, Mineria
Slug: jerico_no_mineria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Megamineria.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Elsalmonurbano.blogspot] 

###### [8 Jun 2017] 

Jericó, en Antioquia, sigue en la lista de los municipios que buscan prohibir la minería. Este miércoles, con 6 votos a favor y 4 en contra, el Concejo Municipal tomó la decisión de prohibir la actividad minera en su territorio, **incluyendo el proyecto minero Quebradona de la AngloGold Ashanti.**

Para que sea un hecho, **la decisión tomada por el Concejo debe pasar ahora a manos del alcalde,** quien deberá ratificar dicha medida. De esta manera, quedarían vetados los proyectos mineros en esa zona, excepto la pequeña minería y otras actividades de construcción.

"En un precedente importante, un hecho político trascendental. Jericó le manda un mensaje al gobierno nacional, y es que **no queremos proyectos con propuestas de desarrollo que no son convenientes",** expresa el alcalde del municipio, Jorge Andrés Pérez, quien agrega que la población debe estar unida para decirle al gobierno que se deben repensar los modelos económicos en los territorios de vocación agropecuaria.

Esta medida se da tras las protestas de los habitantes del municipio quienes en abril pasado impidieron la movilización de dos camionetas con 10 miembros de la multinacional Anglogold Ashanti logrando que se detuviera la explotación minera. Una **actividad que se desarrolla desde el año 2008, con la exploración que de acuerdo con los pobladores ha afectado ambientalmente su municipio**. ([Le puede interesar: Campesinos de Jericó le cierran la puerta a Anglo gold Ashanti)](https://archivo.contagioradio.com/campesinos-de-jerico-antioquia-se-movilizan-contra-extractivismo-en-su-territorio/)

“Algunas fuentes de agua subterránea fueron afectadas mermando los flujos que abastecen los acueductos del corregimiento de Palo Cabildo. **Algunas lagunas terminaron perdiendo sus aguas y 4 fuentes hídricas que abastecen otros acueductos han disminuido sus caudales**”, denuncia uno de los líderes de la comunidad.

Entre otras cosas, también han señalado que Anglogold Ashanti tiene un proceso sancionatorio abierto, ante Corantioquia, por **ubicar una plataforma a 15 metros de una zona de protección** según el esquema de ordenamiento territorial del municipio.[(Le puede interesar: “Gobierno debe encontrar consensos frente a la minería”: Rodrigo Negrete)](https://archivo.contagioradio.com/gobierno_mineria_rodrigo_negrete_consultasoulares_tamesis/)

**AngloGold Ashanti no tiene licencia de explotación en Jericó**

Hace 12 años, AngloGold Ashanti llegó a Jericó para hacer exploración general de metales con sobre vuelos en helicópteros y detectores de metales. Las perforaciones iniciaron con taladros a 2 mil metros bajo la tierra donde encontraron un yecimiento de cobre, oro y plata. En su intento de explotación, según lo afirma Fernando Jaramillo, miembro del comité ambiental de Jericó, **“requerían extraer de las montañas 617 millones de toneladas de rocas que iban a ser trituradas, mezcladas con toneladas de agua y  con químicos”.**

Estas acciones dejarían 25 millones de metros cúbicos contaminados de lodo que iban a ser abandonados por la empresa una vez finalizaran su trabajo. “Ellos pretendían exportar 65 millones de toneladas de cobre y **nos iban a dejar 550 millones de toneladas de desechos en nuestro municipio”**, afirmó Jaramillo. Las afectaciones en el ambiente y en los caudales de agua que surten a la población eran evidentes y ya se habían presentado en otras ocasiones.

Estos daños ambientales fueron tenidos en cuenta por el consejo municipal de Jericó quien aseguró que la decisión que se tomó es retroactiva. Para Jaramillo, **“la empresa minera no tiene licencia para adelantar trabajos de explotación minera por lo que su presencia está prohibida”.**

<iframe id="audio_19158663" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_19158663_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU). 
