Title: Suspensión de uso de gas lacrimógeno: Un freno más a la represión policial
Date: 2020-11-04 15:02
Author: AdminContagio
Category: Actualidad, Nacional
Tags: Abuso policial, ESMAD, Fuerza Pública, gas lacrimógeno
Slug: suspension-de-uso-de-gas-lacrimogeno-un-freno-mas-a-la-represion-policial
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/ESMAD-gas-lacrimogeno.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este 29 de octubre, un **Juez Constitucional ordenó al Presidente Iván Duque, al Ministro de Defensa, Carlos Holmes Trujillo y a la Policía Nacional; suspender el uso de agentes químicos como el gas lacrimógeno utilizados por la Fuerza Pública para dispersar las movilizaciones sociales.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El fallo del Juez de Tutela, se sustentó principalmente en la situación de pandemia que aqueja al país, argumentando que **“*resultaría una combinación muy peligrosa el uso de estos gases en momentos de propagación del virus, porque su efecto en el cuerpo humano degrada las defensas antivirales de los pulmones*** *y por tanto, el ciudadano queda en mayor riesgo y su salud más expuesta en caso de contagiarse por Covid*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, el Juez consideró el efecto que produce el gas en las personas, obligándolas a toser, aumentando así el riesgo de propagación del virus. “*En tal sentido, cualquier norma o protocolo de bioseguridad se va al traste al momento en que estos agentes químicos se utilicen*”, se lee en uno de los apartes del fallo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Aunque **el Juez de Tutela extendió la prohibición del uso de gas lacrimógeno únicamente por el tiempo en el que se encuentre vigente la emergencia del Covid-19**; los motivos que argumentó no fueron únicamente relacionados con la pandemia, pues para el despacho: **“*la utilización de estos agentes químicos que buscan dispersar a la multitud no solo atentan contra el derecho fundamental a la salud de quienes hacen parte de la protesta, sino también el de transeúntes, habitantes y trabajadores del sector afectado,*** *quienes no deberían soportar la carga desproporcionada de ver afectada, así sea temporalmente, de sus capacidades sensoriales como consecuencia del uso indiscriminado de esta sustancia*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por ello, **exhortó al Gobierno, al [Ministerio de Defensa](https://twitter.com/mindefensa) y a la Policía para que “*se debata seriamente la necesidad de mantener el uso de agentes químicos o determinar su prohibición absoluta*”.** (Lea también: [El aparato del Estado se revistió de legalidad para afectar la Movilización: Análisis](https://archivo.contagioradio.com/aparato-estado-revistio-legalidad-afectar-movilizacion/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Para algunos analistas la decisión de este Juez implica “*un freno más a la represión policial promovida desde la directriz institucional del Gobierno*” y puede interpretarse como **una reafirmación del derecho fundamental a la protesta social en tiempos de pandemia.**

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### La suspensión del gas lacrimógeno no es el único fallo contra el proceder de la Fuerza Publica

<!-- /wp:heading -->

<!-- wp:paragraph -->

El pasado mes de septiembre en un fallo histórico **la Corte Suprema de Justicia recalcó la sistematicidad con el que operaba la Fuerza Pública en contra de los civiles en las movilizaciones sociales** y por ello, ordenó al Gobierno Nacional adoptar una serie de medidas para garantizar el derecho fundamental a la protesta. (Le puede interesar: [Agresiones de la fuerza pública contra la protesta social han sido sistemáticas: Corte Suprema](https://archivo.contagioradio.com/corte-suprema-ordena-a-min-defensa-ofrecer-disculpas-por-exceso-en-el-paro-nacional/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Entre las muchas órdenes impartidas por la Corte, se destacaron la prohibición de la escopeta calibre 12 utilizada por los agentes del ESMAD —con la cual se causó la muerte del estudiante Dilan Cruz—, la creación de un protocolo de intervención de la Fuerza Pública que protegiera el derecho a la protesta pacífica y la orden directa al ministro de Defensa Carlos Holmes Trujillo, para que ofreciera  disculpas públicas al país por los abusos y excesos de la Policía en el marco de la movilización.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Orden —esta última— que el ministro Holmes Trujillo se negó a acatar en primera oportunidad y que lo hizo solo después de que el Tribunal de Cundinamarca le diera un ultimátum para pedir disculpas en 24 horas de lo contrario sería declarado en desacato. (Lea también: [\[En vídeo\] 10 delitos de la Policía en medio de las movilizaciones](https://archivo.contagioradio.com/en-video-10-delitos-de-la-policia-en-medio-de-las-movilizaciones/))

<!-- /wp:paragraph -->

<!-- wp:block {"ref":78955} /-->
