Title: "En las cárceles de Colombia torturan a los prisioneros" Informe CCCT
Date: 2015-06-26 16:48
Category: Judicial, Otra Mirada
Tags: abogado, carcel, dignidad, fundacion, ignasio herrero, ivan lopez, julian joaquin realpe, julian realpe, lazos, Popayán, presos, prisioneros, san isiro, suyana, tortura
Slug: en-las-carceles-de-colombia-torturan-a-los-prisioneros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/Carcel.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Pluma 

<iframe src="http://www.ivoox.com/player_ek_4693348_2_1.html?data=lZumlZiYfI6ZmKiakpyJd6KnlpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BfzcbgjciJh5SZopbfxcrQqdSfxcqYpdTQs87WysaY1tTWuNbmwtOYw5DQs9Sf0dfW1c7Tssbm0Niah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Iván López, Corporación Colectivo de Abogados Suyana] 

<iframe src="http://www.ivoox.com/player_ek_4693419_2_1.html?data=lZumlZmVfY6ZmKiakpWJd6KkkZKSmaiRdI6ZmKiakpKJe6ShkZKSmaiRmNDm1drfw5DFb9HmytjW0dPJttDnjNXczs7YrcTj1JKSmaiRh9Di1cbUy9SPlsLYytSYj4qbh46o&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Gloria Silva, Comité de Solidaridad con los Prisioneros Políticos] 

###### [26 jun 2015] 

Organizaciones de Derechos Humanos denuncian que la dirección de la cárcel de San Isidro, en Popayán, realiza torturas, tratos crueles e inhumanos contra los prisioneros políticos recluidos en este penal.

A partir de la jornada nacional de protesta que convocó el Movimiento Nacional Carcelario en el mes de abril para exigir condiciones de vida digna al interior de los centros penitenciarios, el director del penal en la ciudad de Popayán tomó represalias contra algunos de los prisioneros políticos, como el aislamiento en calabozos y el traslado a prisiones en otros departamentos del país.

Según denuncia la Corporación Colectivo de Abogados Suyana y la Fundación Lazos de Dignidad, el prisionero Julian Joaquín Realpe cumplió este viernes 26 de junio, 15 días en huelga de hambre, pues fue trasladado del patio 8 de prisioneros políticos, al patio 6, donde se encuentran personas que cumplen condenas por paramilitarismo, lo que pone en riesgo su vida. Por otra parte, el prisionero político Ignasio Herrero fue trasladado de la cárcel de Popayán a la penitenciaría de Cartago, lo que imposibilita la visita por parte de sus familiares.

A propósito de la conmemoración del Día Mundial en Apoyo a las Víctimas de la Tortura, defensores de Derechos Humanos recuerdan que la tortura contra la población carcelaria es una realidad, mediante el hacinamiento, la atención deficiente en salud, alimentación en mal estado y en general tratos crueles, inhumanos y degradantes. Según afirma el informe de la Comisión Colombiana Contra la Tortura, la violación a los Derechos Humanos de los prisioneros no se cometería por omisión o negligencia del Estado, sino como una política sistemática de adoctrinamiento social.
