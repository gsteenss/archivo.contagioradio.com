Title: Habitantes de la Macarena también se oponen a explotación petrolera
Date: 2016-04-19 13:24
Category: Ambiente, Nacional
Tags: Caño Cristales, Movilización, Rechazo
Slug: habitantes-de-la-macarena-tambien-se-oponen-a-explotacion-petrolera
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/río-guayabero-e1461084507360.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alcaldía La Macarena] 

###### [19 Abril 2016]

Con diferentes iniciativas y movilizaciones, las comunidades de La Macarena le dicen No a la perforación de pozos de petróleo en sus territorios y aclaran que la comunidad no ha sido consultada frente a las perforaciones  en esta región.

De acuerdo con Ismael Medellín, alcalde de La Macarena, las comunidades del municipio tanto en las diferentes audiencias públicas que se han realizado, como en los medios de información han expresado el **No rotundo a la explotación petrolera,** esto con el fin de que el Gobierno Nacional elimine el pensamiento de realizar exploración y perforaciones de petróleo en la Macarena".

Inicialmente el lugar en donde se realizarían las perforaciones de **los pozos por parte de la Empresa Hupecol, es la cabecera del Municipio en donde nacen los ríos Guayabero, Duda Lozada, Platanillos, Caño Gringo y Caño Perdido**, estos ríos desembocan en el río Guayabero que a su vez,  irrigan los ríos hacia Caño Cristales y La Macarena.

El alcalde explica que  la máxima amenaza radica justamente  en que los pozos se realicen en los nacederos y que [cualquier error de aguas vertidas en el río Guayabal acabaría con todas las especies de peces en el río](https://archivo.contagioradio.com/presumo-que-hubo-torciditos-para-lograr-esa-licencia-lamacarena/), incluso con el solo hecho de trasladar maquinaría hacía este lugar ya que alteraría el ecosistema.

**"Nosotros no permitimos que los turistas entren a Caño Cristales con bloqueador o repelente para proteger el ecosistema como para que las empresas si vengan a dañar el ecosiste"**

El 18 de abril se llevó a cabo una jornada 24 horas de las emisoras regionales en contra de la exploración de petroleo, de otro lado, se esta realizando una firmaton en los 9 centros poblados que tiene la Macarena reafirmando el No rotundo a la explotación y exploración en la Macarena. **Mañana se realizará un plantón en la ciudad de Villavicencio rechazando la exploración de petroleo.**

Para finalizar frente a las declaraciones del Presidente de Ecopetrol el Alcalde de la Macarena, considera que es una falta de respeto frente a la comunidad y al profesor Vanegas, "Son opiniones descontextualizadas, la realidad es otra y el solo habla de cortinas de humo"

<iframe src="http://co.ivoox.com/es/player_ej_11224293_2_1.html?data=kpaflJmWfZShhpywj5WbaZS1lJuah5yncZOhhpywj5WRaZi3jpWah5yncarnzsbSzpCxqcXZzdGSpZiJhaXijJKYo9HHpc3YxpC5w5CxpcTV08rbw5KJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

######  Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] 
