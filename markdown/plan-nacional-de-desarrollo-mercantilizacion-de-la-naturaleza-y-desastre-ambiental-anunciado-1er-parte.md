Title: Plan Nacional de Desarrollo: Mercantilización de la naturaleza y desastre ambiental anunciado (1era parte)
Date: 2015-03-12 16:37
Author: CtgAdm
Category: Opinion
Tags: Expropiación Express, Plan Nacional de Desarrollo 2014 - 2018, PND
Slug: plan-nacional-de-desarrollo-mercantilizacion-de-la-naturaleza-y-desastre-ambiental-anunciado-1er-parte
Status: published

###### Foto: tomada de internet 

#### Por**[[Isabel Zuleta](https://archivo.contagioradio.com/isabel-cristina/)- [@ISAZULETA](http://bit.ly/1MvYsoI)]** 

El tema no es sencillo de abordar. La vida cotidiana se ve implicada por una suerte de ley que borra la precaria y confusa legislación ambiental y social existente en Colombia, hasta el derecho penal tan difuso en materia ambiental se ve implicado (no he podido entender cuales son los delitos ambientales pues según el caso se acomodan, como otros delitos, a la disputa de poder y son imputables o no según intereses generalmente económicos).

Los conflictos socio-ambientales emergen al ritmo de la destrucción, el destierro por diferentes vías, todas violentas, de las culturas que han conservado los ecosistemas más sensibles del país facilita la tarea. No he conocido ningún caso en el que un plan de desarrollo que se convierte en Ley pase por una revisión de constitucionalidad en su integralidad (sólo por artículos), es decir, una revisión del modelo que representa que a su vez es el trasfondo de la destrucción.y la imposibilidad del derecho a un ambiente sano.

Del documento Bases del **Plan Nacional de Desarrollo 2014 - 2018** destacaré algunos de los temas que me dan indigestión, si se trata de poner en el centro de la vida el agua**:** vías e Hidrocarburos, por incapacidad corporal dejaré para las próximas energía eléctrica, minería y “crecimiento verde**”.** Saber que se convertirá en ley un plan de desarrollo que en nombre del ambiente legisla para el despojo y en nombre del desarrollo destruye los ecosistemas, no puede dejar de generarme indignación. La conclusión para el agua es privatización, contaminación, degradación, escasez y mercantilización.

El plan es nefasto la apuesta es clara: **campo sin campesinos, campo para la minería, ríos para las represas, oceanos para el petróleo, subsuelo sin agua o con agua contaminada y producción de alimentos insalubres.**

¿Que entiende el plan por “desarrollo integral del campo”? ¿Es coherente con los diálogos de la Habana? ¿Es necesaria la coherencia?

La infraestructura estratégica vislumbra grandes vías, las mismas que justifican la venta de Isagen y Ecopetrol que son una deuda del presidente con cierto sector, las vías terciarias quedan relegadas como siempre a un segundo plano y los caminos ni aparecen como sino existieran. Los adelantos legislativos en este sentido no se han hecho esperar, el último en diciembre de 2014 a pupitrazo limpio legislaron para el despojo, con carácter de “urgente” el gobierno presento el proyecto de ley 175 que se convertiría en la ley 1742 “**Expropiación Express”.** Las super vías 4G atravesaran bosques y montañas, los innumerables túneles que requieren secaran incontables fuentes de agua, los mayores afectados los campesinos cuya tierra se quedará sin ella. El túnel de desviación del río Guarino en Caldas seco 22 fuentes de agua y nada paso.

Hidrocarburos es quizás uno de los temas más sensibles para el agua, a partir de este documento se puede concluir: 1. A pesar del llamado urgente sobre los océanos, en la producción de petróleo lo importante es aumentar las reservas, como si fuera poco se darán “incentivos tributarios” a las empresas que se metan en nuestros mares. 2. El Francking sea o no para la paz va porque va, las aguas del país no tienen nada que ver en esto, se disminuye el impacto aduciendo sequías “naturales” pero si falta el agua tranquilos, la que sale de la explotación petrolera se usara para riego en la producción de alimentos (me queda la duda de si toda la comida o sólo la de los empobrecidos o si Pomona pondrá la etiqueta frutas y verduras regadas con agua de posos petroleros). 3. Los asuntos ambientales se resuelven con “guias” del ministerio del Ambiente no vinculantes y cartillitas de mejores practicas.

El negocio que propone el gobierno sobre el agua es la imitación del de Chevron en California. El juego es cruel pero simple. Generan sequía al afectar fuentes de agua subterranea y después venden el agua como parte del negocio de las petroleras así contenga residuos de crudo (como en muchas partes del mundo, las reglas de control de agua subterránea en Colombia no son claras, mejor dicho si se secan o contaminan no pasa nada) y cuando los ambientalistas protestan por las grandes cantidades de agua que se requieren para la extracción del petróleo, el argumento es que el agua del subsuelo no es apta para el consumo humano.

Hidroeléctricas y minería por doquier, bien adornadas con aplicativos presuntamente ambientales, mezclada con crecimiento (no sabemos hacia donde) verde (tampoco que tipo de verde) quedan para la próxima.
