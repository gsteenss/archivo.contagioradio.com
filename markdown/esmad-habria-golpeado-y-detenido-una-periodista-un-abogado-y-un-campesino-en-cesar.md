Title: Abogado y periodista golpeados por ESMAD en protesta de Puerto Ayacucho, Cesar
Date: 2017-07-01 17:47
Category: DDHH, Nacional
Tags: Colombia Informa, Equipo Jurídico Pueblos, ESMAD
Slug: esmad-habria-golpeado-y-detenido-una-periodista-un-abogado-y-un-campesino-en-cesar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/07/IMG-20170701-WA0006.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Derecho del Pueblo] 

###### [01 Jul. 2017] 

Rommel Durán abogado del Equipo Jurídico Pueblos, María Montiel, periodista de Colombia Informa y el Líder campesino Jorge Alonso **habrían sido atacados, lesionados, detenidos por el ESMAD e ingresados a la estación de Policía de la Mata** y luego llevados a la estación de Policía de Aguachica en el departamento del Cesar, según lo denuncia Sinaltrainal a través de un comunicado.

Según la comunicación en medio de las protestas que realizaban las comunidades del corregimiento Ayacucho y Mata en el departamento del Cesar, **el abogado y la periodista que verificaban los abusos de la Fuerza pública contra la protesta social fueron lesionados** y posteriormente detenidos. Le puede interesar: [La fuerza pública es el principal perpetrador de los casos de tortura en Colombia](https://archivo.contagioradio.com/en-los-ultimos-5-anos-se-han-reportado-casos-de-tortura-en-141-paises/)

Asegura Sinaltrainal que durante la protesta, el abogado del Equipo Jurídico Pueblos, habrían denunciado que Integrantes del Batallón Energético y Vial - BAEV 3, en compañía de la Policía Nacional, llegaron al sitio de la protesta conocido como “Mata Vieja” y habrían fotografiado a los integrantes de la comunidad. Imágenes que según la comunicación se encuentran en poder del jefe de seguridad de la subestación Ayacucho de Ecopetrol Víctor Manuel González.

“Se instalaron entre la manifestación intimidando a quienes participan, violando el principio de distinción que exige el Derecho Internacional Humanitario” manifiestan. Le puede interesar: ["El ESMAD está de más", Día Contra la Brutalidad Policial](https://archivo.contagioradio.com/el-esmad-esta-de-mas-dia-contra-la-brutalidad-policial/)

### **Se hizo caso omiso de las solicitudes de los abogados para respetar el derecho a la protesta** 

Según Sinaltrainal **los integrantes del ejército nacional y el ESMAD han estado cometiendo diversos abusos contra la comunidad** y contra el integrante del Equipo Jurídico Pueblos y la periodista de Colombia Informa, quien requirió al Cabo Tercero Jorge Andrés Cabanillas del Ejército Nacional, que “cesara su procedimiento irregular”, ante lo cual el Capitán Villarreal y el Sargento Mayor José Jiménez reaccionaron.

“La respuesta de los miembros del Ejército fue decir que ellos y cualquier ciudadano podía tomar fotografías; indicando además que estaba prohibido, por seguridad pedirles información sobre sus nombres y rangos; olvidando al parecer, que a **los miembros del ejército como todo servidor público, están obligados a identificarse** y que la ley regula sus actuaciones” asevera el comunicado.

Las protestas se desarrollan en contra de la implementación del proyecto de infraestructura la Ruta del Sol concesionado a CONSOL, consorcio del cual hace parte la firma Odebretch. Le puede interesar: [Ecopetrol y Ocensa demandadas por comunidades de Santa Cruz del Islote](https://archivo.contagioradio.com/ecopetrol-y-ocensa-demandadas-por-comunidades-de-santa-cruz-del-islote/)

Por estas actuaciones **las organizaciones presentes en el lugar han exigido el respeto al derecho a la protesta**, además de la libertad inmediata del abogado Defensor de Derechos Humanos Rommel Durán y la Periodista María Montiel de Colombia Informa.

A través de su cuenta en Twitter **la Fundación para La libertad de Prensa -FLIP- pidió a la Policía aclarar la situación** de los hechos contra la corresponsal de Colombia Informa.

> Periodista de [@Col\_Informa](https://twitter.com/Col_Informa) fue detenida en el cubrimiento de una manifestación en Cesar. Exigimos a [@PoliciaColombia](https://twitter.com/PoliciaColombia) que aclare la situación
>
> — FLIP (@FLIP\_org) [1 de julio de 2017](https://twitter.com/FLIP_org/status/881247090894147584)

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)

<div class="osd-sms-wrapper">

</div>

<p>
<script src="//platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>

