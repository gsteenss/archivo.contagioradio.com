Title: Ex vicepresidente español detenido por blanqueo, fraude y alzamiento de capitales
Date: 2015-04-16 17:25
Author: CtgAdm
Category: El mundo, Otra Mirada
Tags: ex vicepresidente español detenido, fraude alzamiento de capitales, Partido Polpular, Rodrigo rato detenido blanqueo, Rodrigo Rato detenido en el registro de su vivienda
Slug: ex-vicepresidente-espanol-detenido-por-blanqueo-fraude-y-alzamiento-de-capitales
Status: published

###### Foto:Leonoticias.com 

El e**x vicepresidente de España** , ex presidente del FMI y ex presidente de Bankia Rodrigo Rato ha sido detenido esta tarde durante el registro que ha efectuado en su casa de Madrid la fiscalía anticorrupción por los **delitos de blanqueo, fraude y alzamiento de capitales.**

La detención llega después de que el pasado martes, Rato anunciara acogerse a la amnistía fiscal decretada por el gobierno en 2012. Dicha amnistía ha sido fuertemente criticada por los movimientos sociales, partidos de izquierda y el 15-M por ser la **legalización de capitales procedentes del dinero negro y de la corrupción.**

A través de una denuncia interpuesta por la **Agencia Tributaria**, ha sido la fiscalía quién ha ordenado el registro de su vivienda y en el transcurso de este su detención.

Rodrigo Rato fu vicepresidente entre 1996 y 2004 durante los gobiernos del presidente José María Aznar, presidente del FMI en 2007, y presidente d la entidad bancaria conocida como Bankia entre 2010 y 2012.

Actualmente ocupa el cargo de asesor para Latinoamerica y Europa en la empresa **Telefónica.** Como otros mandatarios del país había utilizado la conocida como ¨puerta giratoria¨, que consiste en pasar de un cargo político a un cargo con alto estatus en una multinacional, similar a los casos de ex preisdentes como Felipe Gonzalez y José María Aznar. La abolición de las ¨puertas giratorias¨ es una de las medidas que reclama la izquierda.

Conocido por ser uno de los **artífices de la grave crisis económica española** fue ministro de economía durante el gobierno de **Aznar,** donde impulsó al sector inmobiliario sin ningún tipo de regularización y que terminó con la conocida burbuja inmobiliaria arrastrando a los otros sectores económicos a la quiebra.

Cuando las principales **cajas de ahorros españolas,** después de haber malversado fondos sociales, tuvieron que pedir un rescate financiero a la UE debido al impago de la deuda con los bancos alemanes, fue quién dirigió la **fusión de Caja Madrid y Caja Valencia en la conocida entidad bancaria ¨Bankia¨**, también trabajó en la creación del ¨banco malo¨, donde se acumuló toda la deuda con el objetivo de sacarla a bolsa.

Para los movimientos sociales, pero sobre todo **para el 15-M, la detención supone una gran victoria para la ciudadanía** que a través de plataformas ha denunciado jurídicamente y públicamente todos los casos de corrupción en los que ha estado envuelto, principalmente el de Bankia.

Según la web del 15-M ¨**...ha sido la ciudadanía organizada ha sacado a la luz las prueba de sus fraudes, desde la salida a bolsa de Bankia y las Tarjetas negras...¨**, además añaden ¨...**Si lo elegimos a él fue precisamente porque no hay nadie con poder en este país que no haya sido cómplice suyo...¨,** en la web recuerdan que tienen que caer todos y que no van a permitir que sea el chivo expiatorio de toda la corrupción española.

A penas se ha realizado la detención, las redes sociales han explotado en  twitters sobre el triunfo ciudadano en contra de la corrupción y a favor de un cambio social, con los hastags **\#laciudadaníalohizo y @15MPaRato,** los movimientos sociales están expresando su clara victoria y las ansias de que caigan todos los demás implicados en la crisis económica.
