Title: En Colombia persiste la violencia hacia población transgénero
Date: 2015-11-20 17:30
Category: closet, LGBTI
Tags: Activistas LGBT, Deidy Olarte, día en contra de la violencia de las personas Transgeneristas, discriminación en Colombia, LGBT, población trans en Colombia, Sudirección de Asuntos LGBTI, Transgeneristas
Slug: 17583-2
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/MG_9294-e1492469910185.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

<iframe src="http://www.ivoox.com/player_ek_9458948_2_1.html?data=mpmimp6YfI6ZmKiakpyJd6KnlJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRic%2BfxNTZ0dLGrcKf0crf1c7XuMafzcaY2M7TsMbixM7Ojc3Fp8rVjNXcxNHFp8qZpJiSm5jSb9XmwtOah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Deisy Olarte] 

###### [20 nov 2015]

Este 20 de noviembre se conmemora el **Día Contra la Violencia hacia las personas Transgeneristas**, una fecha dedicada a la memoria de aquellos que han sido asesinados o maltratados  p**or la transfobia, la discriminación y el odio a las personas trans.**

En Colombia persiste la violencia hacia esta población, pese a que se ha trabajado por implementar políticas públicas que garanticen los derechos de la comunidad LGBTI. Bogotá, por ejemplo, es una ciudad en la que el maltrato psicológico en diferentes ámbitos de la vida cotidiana, es mucho más evidente que las agresiones físicas, mientras que en ciudades como Barranquilla, Sincelejo, Cali y Maicao, el maltrato contra la población trans suele ser físico.

**“Debido a nuestras construcciones identitarias por ser personas transgénero, sufrimos agresiones físicas, desapariciones y el desplazamiento forzado"** afirma Deysi Olarte, mujer trans y quién además trabaja en la Subdirección para asuntos LGBTI de la Alcaldía de Bogotá. Ella agrega que cualquier cifra oficial sobre víctimas de violencia por ser personas trangénero, no evidencia nunca la realidad y la complejidad de esta problemática.

Deysi hace un llamado a todo la comunidad LGBT que ha sido victima de maltrato y descriminación a que denuncien, e manera que este fenómeno no se naturalice en la sociedad colombiana.
