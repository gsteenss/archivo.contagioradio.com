Title: Mujeres del mundo alzaron su voz en contra de Donald Trump
Date: 2017-01-23 15:14
Category: El mundo, Mujer
Tags: Feministas contra Trump, Marcha de las Mujeres, Women's March
Slug: mujeres-del-mundo-alzaron-su-voz-en-contra-de-donald-trump
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Marcha-de_lasMujeres.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Twimg] 

###### [23 Ene 2017] 

“Los próximos 1.459 días del gobierno de Trump serán 1.459 días de resistencia: **resistencia en la calle, resistencia en las clases, resistencia en el trabajo, resistencia en nuestro arte y en nuestra música”** fueron las palabras pronunciadas por Angela Davis, ex integrante del Black Panther Party y miembro del Partido Comunista Estadounidense, en la Marcha de Mujeres contra Trump en Washington.

La iniciativa llamada Women’s March o Marcha de las Mujeres, fue convocada por mujeres norteamericanas a través de redes sociales minutos después de la posesión de Donald Trump, tuvo una acogida casi inmediata en ciudades estadounidenses como Dallas, Denver, Miami, Seattle, Filadelfia, San Francisco y logró tener eco en otras ciudades del mundo como Bogotá, Buenos Aires, México D.F, Barcelona, París, Ginebra, Manchester, Lisboa, Londres, Milán y Sidney.

Más de 2 millones 500 mil personas incluyendo mujeres, hombres, niños, abuelos y personas de la comunidad LGBTI en Estados Unidos y otras miles en diferentes ciudades, alzaron su voz **para decirle al mundo que los derechos de las mujeres y de los grupos sociales minoritarios deben ser respetados.**

### Las razones y las acciones 

Acusaciones de actrices, modelos y funcionarias sobre abusos sexuales por parte del magnate, los discursos misóginos, homófobos, xenófobos y racistas durante su campaña y posesión y las propuestas de gobierno, que muchos aseguran, **pretenden un retroceso en los logros de comunidades afro, migrantes, LGBTI y mujeres en materia de derechos,** son sólo algunas de las razones de las protestas.

A dicha causa se han unido personalidades como Alicia Keys, Cher, Scarlett Johansson, Michael Moore y Madonna quien dijo durante el evento “bienvenidos a la revolución del amor (…) a la rebelión, **a nuestra negativa como mujeres de aceptar esta nueva era de tiranía”.**

Otra de las acciones que se ha convertido en el símbolo de las protestas son los "Pussyhat" o gorros rosa, que han usado las mujeres manifestantes y hace referencia a los controvertidos comentarios de Donald Trump de 2005, cuando dijo **"puedes hacer cualquier cosa a las mujeres", incluso "agarrarlas de la vagina".**

Por otra parte, las integrantes Women’s March han iniciado la campaña **‘10 Acciones para los primeros 100 días’ la cual pretende dar continuidad a las acciones iniciadas** durante la multitudinaria movilización. La primera acción, invita a que ciudadanas y ciudadanos norteamericanos escriban una postal a los Senadores, hablando sobre los temas que merecen mayor prioridad.

\[caption id="attachment\_35024" align="aligncenter" width="588"\]![Women's March](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/Captura-de-pantalla-2017-01-23-a-las-15.01.53.png){.size-full .wp-image-35024 width="588" height="551"} "10 acciones en los primeros 100 días" Women's March\[/caption\]

###### Reciba toda la información de Contagio Radio en [[su correo]
