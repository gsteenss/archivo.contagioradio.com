Title: Impunidad, persecución y amenazas fue lo que encontró Caravana Internacional de Juristas
Date: 2016-08-29 19:21
Category: DDHH, Nacional
Tags: caravana de juristas, Derechos Humanos, lideres sociales
Slug: impunidad-persecucion-y-amenazas-fue-lo-que-encontro-caravana-internacional-de-juristas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/Derechos-humanos-2.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Archivo 

###### [29 Ago 2016] 

Concluyó la caravana internacional de juristas en Colombia que había llegado a verificar la situación de los defensores y defensoras de derechos humanos en el país. Aunque no se han entregado las recomendaciones al gobierno colombiano, la **caravana se encontró con altos índices de persecución y amenaza contra líderes sociales, y de impunidad** frente a los ataques a los que se han visto expuestos.

Los riesgos persisten. Las y los defensores de derechos humanos, siguen siendo víctimas de hostigamiento por parte del Ejército, hay **persecución y asesinato a militantes de plataformas de sociales, que además se ven expuestos a la falta de atención y garantías del Estado,** como lo explicó Julia Figueroa, abogada del Colectivo Luis Carlos Pérez.

Precisamente en Cúcuta, la comisión expresó preocupación frente al asesinato y persecución de líderes y lideresas sociales, ya que en lo corrido del año [se han registrado 50 asesinatos de líderes y lideresas](https://archivo.contagioradio.com/3-desapariciones-y-30-asesinatos-alarman-a-comunidades-del-catatumbo/), además interpelaron a la **Unidad de Víctimas y a la Procuraduría acerca de la crisis humanitaria de las víctimas**, el poco acceso a la justicia y los altos niveles de corrupción en la misma.

Allí mismo, justamente la abogada defensora de derechos humanos y ex candidata a la gobernación de Norte de Santander, **Judith Maldonado Mojica** denunció que conoció de un testigo directo que las organizaciones criminales de la región preparan en una finca de Puerto Santander **un plan de asesinato.**

En las próximas semanas se espera que la misión, que esta vez contó con **55 delegados de 15 países, elabore** un informe con recomendaciones a las autoridades colombianas y plantearán las acciones necesarias de acuerdo con las  peticiones y denuncias identificadas para que se ataquen los problemas estructurales que ponen en riesgo la vida de los defensores, así mismo se darán recomendaciones para blindar a las comunidades y organizaciones de las actuaciones empresariales.

<iframe src="http://co.ivoox.com/es/player_ej_12715195_2_1.html?data=kpekk5qVfZahhpywj5aWaZS1lp2ah5yncZOhhpywj5WRaZi3jpWah5yncavpzc7OjavNq9bZ09TOh5enb6TjzcrQ1s7as4zA1s7gjajFts3j1JC9h6iXaaKt08rnj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
