Title: Luis Alfonso Hoyos será juzgado en Colombia a pesar de estar prófugo
Date: 2015-08-21 18:10
Category: Nacional, Política
Tags: Diálogos de paz en la Habana, FARC-EP, Fiscalía General de la Nación, Luis Alfonso Hoyos
Slug: a-pesar-de-estar-profugo-luis-alfonso-hoyos-sera-juzgado-en-colombia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/luis.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: lapatria.com 

###### [21 Ago 2015]

Luis Alfonso Hoyos, ex asesor espiritual de la campaña del Centro Democrático, sería **procesado judicialmente** tras **no asistir a la audiencia de imputación de cargos** por los delitos de **espionaje, violación de datos personales, uso de software malicioso, acceso abusivo a un sistema y concierto para delinquir.**

Según la investigación, la interceptación ilegal a los integrantes de la mesa de conversaciones de la Habana habría sido organizada por Luis Alfonso Hoyos y ejecutada por el hacker Andrés Fernando Sepúlveda, quien luego de aceptar sus delitos manifestó haber recibido **ordenes directas del dirigente de buscar información importante acerca de los diálogos en la Habana. Sepúlveda está condenado a 10 años de prisión y en la actualidad se encuentra en el bunker de la Fiscalía.**

La Fiscalía general de la nación apeló la decisión del juez del control de garantías de procesar al dirigente político **en contumacia o como persona ausente**, luego de que este no se pronuncie para asistir a la audiencia luego de planear una videoconferencia desde su lugar de residencia en Estados Unidos.

Cabe añadir que el 18 de enero del año 2014 la Fiscalía solicitó la primera audiencia de imputación de cargos contra Hoyos por su participación en el caso del "hacker" Andrés Sepulveda. Posterior a la acusación, el ex asesor sale del país en busca, según el Centro Democrático, de asilo político.  Ver: [Con Hoyos aumenta el número de uribistas prófugos de la justicia](https://archivo.contagioradio.com/con-luis-alfonso-hoyos-aumenta-numero-de-uribistas-profugos-de-la-justicia/).
