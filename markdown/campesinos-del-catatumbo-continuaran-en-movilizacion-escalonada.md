Title: Campesinos del Catatumbo continuaran en movilización escalonada
Date: 2017-09-20 11:20
Category: DDHH, Nacional
Tags: Catatumbo, Erradicación Forzada
Slug: campesinos-del-catatumbo-continuaran-en-movilizacion-escalonada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/servicios-ambientales.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Servicios Ambientales] 

###### [19 Sept 2017] 

Campesinos del Catatumbo continuaran en movilización escalonada permanente hasta que el gobierno nacional cumpla con los acuerdos de paz de La Habana, en el punto 4 y los acuerdos de sustitución voluntaria que ya había pactado con la comunidad. De acuerdo con Olga Quintero **las erradicaciones forzadas son una estrategia estatal y atenta contra las propuestas que han hecho los campesinos.**

Para Quintero la erradicación de **las 25 mil hectáreas, hacen parte de los alcances de la resolución 3080, de abril de 2016**, generada desde el Ministerio de Defensa para combatir y luchar contra el narcotráfico, que iría en contravía de los acuerdos de paz y de lo que se había pactado como un acto voluntario y concertado de los campesinos para iniciar la sustitución de cultivos ilícitos con la garantía de tener un plan para subsistir económicamente.

“Lo que dicen los campesinos es, bueno hay un acuerdo como vienen a violentarnos, si ni siquiera han venido a socializar y hasta el 9 de septiembre se firmó el acuerdo municipal y tres días después anuncian la erradicación manual forzada y militarizan el territorio” afirmó Quintero. (Le puede interesar: ["Campesinos bloquean vía Ocaña-Cúcuta, exigiendo cumplimiento de acuerdos de sustitución de cultivos"](https://archivo.contagioradio.com/campesinos-bloquean-via-ocana-cucuta-exigiendo-cumplimiento-de-acuerdos-de-sustitucion-de-cultivos/))

### **Abusos de la Fuerza Pública** 

El pasado sábado 16 de septiembre, los campesinos de la vereda el Treinta, en el municipio de Sardinata, denunciaron que Policía, Ejercitó y ESMAD llegaron al territorio, provocando confrontaciones en las que salieron heridos dos campesinos, **uno con traumas testiculares y el otro con traumas abdominales, ambos se recuperan de sus operaciones.**

### **La propuesta del campesinado del Catatumbo** 

Olga Quintero señaló que durante esta primera semana, los campesinos han decidido continuar con acciones escalonadas, mientras se decide si convocar o no a paro regional, además proponen que sesione el Consejo Permanente de Dirección estratégico del PNES, para resolver cómo se realizará la sustitución, de igual forma plantean que el **cronograma de socialización se construya en conjunto.**

Otra de las propuestas es que se retiren los erradicadores de la zona para que se restablezca la confianza de los campesinos y se continúe con los planes de sustitución de cultivos ilícitos. (Le puede interesar: ["Erradicación forzada en el Catatumbo afecta a más de 300 mil personas"](https://archivo.contagioradio.com/erradicacion-forzada-en-el-catatumbo-afecta-a-mas-de-300-mil-personas/))

<iframe id="audio_21002693" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_21002693_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 
