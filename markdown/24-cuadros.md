Title: 24 Cuadros
Date: 2015-02-05 15:53
Author: AdminContagio
Slug: 24-cuadros
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/03/24-cuadros-cine.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

### 24 CUADROS

[![Festival centro 10 años](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400.jpeg "Festival centro 10 años"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400.jpeg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-300x156.jpeg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-768x399.jpeg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/01/MIDBO-20-770x400-370x192.jpeg 370w"}](https://archivo.contagioradio.com/festival-centro-10-anos/)  

###### [Festival centro 10 años](https://archivo.contagioradio.com/festival-centro-10-anos/)

[<time datetime="2019-02-27T20:30:13+00:00" title="2019-02-27T20:30:13+00:00">febrero 27, 2019</time>](https://archivo.contagioradio.com/2019/02/27/)Foto: Con el slogan "Vuelve a tu centro" desde este 30 de enero inicia una edición más del Festival Centro, evento artístico y cultural que desde hace una década tiene lugar en la capital colombiana...[LEER MÁS](https://archivo.contagioradio.com/festival-centro-10-anos/)  
[![6to Festival del Cine por los derechos humanos abre convocatorias](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Cine-y-ddhh-770x400.jpg "6to Festival del Cine por los derechos humanos abre convocatorias"){width="770" height="400" sizes="(max-width: 770px) 100vw, 770px" srcset="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Cine-y-ddhh-770x400.jpg 770w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Cine-y-ddhh-770x400-300x156.jpg 300w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Cine-y-ddhh-770x400-768x399.jpg 768w, https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Cine-y-ddhh-770x400-370x192.jpg 370w"}](https://archivo.contagioradio.com/festival-cine-derechos-humanos/)  

###### [6to Festival del Cine por los derechos humanos abre convocatorias](https://archivo.contagioradio.com/festival-cine-derechos-humanos/)

[<time datetime="2019-02-13T14:47:59+00:00" title="2019-02-13T14:47:59+00:00">febrero 13, 2019</time>](https://archivo.contagioradio.com/2019/02/13/)La 6ta versión del Festival Internacional de Cine por los Derechos Humanos tiene abiertas sus convocatorias en 7 categorías[LEER MÁS](https://archivo.contagioradio.com/festival-cine-derechos-humanos/)  
[](https://archivo.contagioradio.com/latinoamerica-preseleccion-para-los-oscar/)  

###### [Los rostros de Latinoamérica en la preselección para los Oscar](https://archivo.contagioradio.com/latinoamerica-preseleccion-para-los-oscar/)

[<time datetime="2018-12-21T16:00:43+00:00" title="2018-12-21T16:00:43+00:00">diciembre 21, 2018</time>](https://archivo.contagioradio.com/2018/12/21/)Foto: 21 Dic 2018 Esta semana que termina, la Academia de Artes y Ciencias Cinematográficas de Hollywood anunció la lista de 9 cintas preseleccionadas para participar en la categoría película en lengua no inglesa, en la próxima edición...[LEER MÁS](https://archivo.contagioradio.com/latinoamerica-preseleccion-para-los-oscar/)  
[](https://archivo.contagioradio.com/wajib-coproduccion-colombo-palestina/)  

###### [Wajib, primera coproducción colombo-palestina se estrena hoy](https://archivo.contagioradio.com/wajib-coproduccion-colombo-palestina/)

[<time datetime="2018-11-22T16:31:12+00:00" title="2018-11-22T16:31:12+00:00">noviembre 22, 2018</time>](https://archivo.contagioradio.com/2018/11/22/)Se estrena en salas del país la cinta Wajib, primera coproducción colombo-palestina un drama romántico sobre las relaciones entre padres e hijos[LEER MÁS](https://archivo.contagioradio.com/wajib-coproduccion-colombo-palestina/)  
[](https://archivo.contagioradio.com/fotogramas-resistencia-cine-memoria-paz/)  

###### [Fotogramas en resistencia, un festival de cine por la memoria y la paz](https://archivo.contagioradio.com/fotogramas-resistencia-cine-memoria-paz/)

[<time datetime="2018-10-25T20:15:16+00:00" title="2018-10-25T20:15:16+00:00">octubre 25, 2018</time>](https://archivo.contagioradio.com/2018/10/25/)La plataforma de gestión cultural Independencia Récords, abre convocatorias para el primer Festival de Cine “Fotogramas en Resistencia”.[LEER MÁS](https://archivo.contagioradio.com/fotogramas-resistencia-cine-memoria-paz/)  
[](https://archivo.contagioradio.com/midbo-2018-documental/)  

###### [En MIDBO 2018 el documental va más allá de las pantallas](https://archivo.contagioradio.com/midbo-2018-documental/)

[<time datetime="2018-09-28T15:54:51+00:00" title="2018-09-28T15:54:51+00:00">septiembre 28, 2018</time>](https://archivo.contagioradio.com/2018/09/28/)La experimentación y la experiencia sensorial del espectador llegan en los 20 años de la Muestra Internacional Documental de Bogotá[LEER MÁS](https://archivo.contagioradio.com/midbo-2018-documental/)
