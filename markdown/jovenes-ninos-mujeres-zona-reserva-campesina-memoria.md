Title: Mujeres, niños y jóvenes construyen memoria en la Zona de Reserva Campesina de Putumayo
Date: 2019-07-21 16:58
Author: CtgAdm
Category: Cultura, DDHH
Tags: memoria, TDH, Zona de Reserva Perla Amazónica
Slug: jovenes-ninos-mujeres-zona-reserva-campesina-memoria
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Putumayo.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Foto1.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/foto2.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Foto3.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Mujeres.png" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

El pasado 5 de julio, 33 niños y niñas, 12 hombres y 15 mujeres y jóvenes provenientes de 6 comunidades que conforman la Zona de Reserva Perla Amazónica en Puerto Asís, Putumayo, se reunieron alrededor del diálogo, el cine y la fotografía para construir los sueños en sus territorios y el país como un acto en la construcción de la memoria y la verdad.

Con el apoyo  de **TDH Alemania,** el taller realizado con niñas, niños, adolescentes y jóvenes (NNAJ) reflejó su historia familiar, generacional y territorial al reconstruir las expresiones culturales como la música, la vestimenta, las celebraciones tradicionales, la comida y las costumbres características de una comunidad.

Así, se exaltó la importancia de la fotografía como una forma de poner en evidencia sus preocupaciones como la agudización del conflicto, el reclutamiento de grupos armados y las problemáticas de consumos de drogas en sus territorios, así como la ausencia de voluntad política del Gobierno Nacional por implementar la paz.

Su preocupación mayor está en el cumplimiento del Programa Nacional de Sustitución de Cultivos Ilícitos y la reforma rural integral de la que han participado con sus ideas. Manifiestan, que no quieren volver a repetir este escenario en el que la fumigación con glifosato ha sido la causante de la muerte de muchas vidas humanas y afectaciones ambientales.

A través de de cámaras auxiliares los niños y niñas reconocieron su realidad y construyeron modelos para el arraigo territorial  a través de ejemplos sencillos y encontraron en la fotografía una forma de preservar sus tradiciones, los recuerdos de su familia y los conceptos básicos necesarios para tomar una foto y de esta forma seguir soñando con la paz y armonía en los territorios que cohabitan.

\

### El Putumayo a través del lente de la cámara 

La fotografía se ligó al de los derechos fundamentales de los niños y niñas, lo que para mayor comprensión se relacionó con lo que ellos sueñan dentro de su comunidad, en términos que ellos definieron como educación, felicidad, amor o familia y cómo estos se verían reflejados en elementos cotidianos que podrían ser fotografiados en la Zona de Reserva Campesina.  [(Lea también: Por medio de dibujos y fotografías, niños Embera reconstruyen la memoria)](https://archivo.contagioradio.com/por-medio-de-dibujos-y-fotografias-ninos-embera-reconstruyen-la-memoria/)

Al final de la jornada, reconocieron la importancia de la fotografía como una forma de preservar las tradiciones y recuerdos de su familia. Pese a la lluvia que acompañó el día, se tomaron fotografías que harán parte de una galería de fotos que además de incluir al Putumayo, revelará conceptos que para niños y niñas de diferentes regiones del país construyen un ejercicio del derecho restaurador por la verdad y la memoria para el país desde sus voces.

### Mujeres, madres, hijas y  de la Zona de Reserva trabajan en conjunto 

Alrededor de 60 mujeres entre los 14 y los 60 años se reunieron en el centro humanitario de la ZRCPA en un día que han denominado como "su día". Expresión que hace alusión a los espacios de autocuidado para la formación política que dedican exclusivamente una vez al mes con sus amigas de **Mi Nombre es Mujer Perla Amazónica (MEMPA)**. Allí, no se tiene presente darle comida a las gallinas, cuidar de los cerdos o preparar los alimentos en la casa para sus esposos, hijos e hijas.

![ZRCPA](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Mujeres-1024x602.png){.aligncenter .wp-image-70975 .size-large width="1024" height="602"}

Los diálogos, que versan sobre sus relaciones familiares, sus formas de descanso, de economías, se juntaron este viernes 5 de julio para dar cabida a la inclusión de propuestas frente a la realidad del consumo de alcohol y sustancias psicoactivas que está afectando a los y las jóvenes. Situación que se ha venido agudizando frente a la falta de oportunidades y la implementación de los acuerdos para la construcción de la paz y más fuertemente del **Programa Nacional de Cultivos de Uso Ilícito.**

Durante la jornada, identificaron rutas de atención a nivel familiar, educativo, comunitario y de la asociación MEMPA, "hoy ha sido muy interesante porque hemos tocado los problemas que nos afectan en este momento y empezamos a plantear soluciones, ha sido muy constructivo" expresa Magola Aranda, una de las mujeres que hace parte de MEMPA y quien asiste desde hace un año y medio a estos encuentros.

El diálogo se centró en la necesidad del amor, del diálogo, del cuidado y la inclusión de los y las NNAJ en la vida comunitaria y las decisiones familiares. Además, posicionaron los espacios de formación educativa como los encuentros de jóvenes que reúnen una vez al mes a los adolescentes de la ZRCPA y la escuela como **lugares esenciales para la construcción de propuestas, que abanderando los principios de la comunidad permitan seguir forjando el arraigo territorial y los derechos de la casa en la que cohabitan.**

### Los jóvenes de Putumayo también construyen comunidad 

Al igual que las mujeres y los niños y niñas, los y las jóvenes del lugar también asistieron a un espacio para explorar las dificultades y nuevas experiencias a las que se enfrentan los adolescentes, su relación con sus padres, el descubrimiento de su identidad sexual e incluso la responsabilidad con la que hay que asumir las nuevas tecnologías.

**"Esto es de gusto, de apropiarse de nuestro territorio"**, afirma Julian Villareal de 16 años, "hablamos de cultura, deporte, política y otros ejes, es muy importante, esto nos ayuda mucho a los niños y jóvenes, nos ayuda a conocer las problemáticas de nuestro país, autovalorarnos y crecer como persona", agrega.

Julián reconoce varias de las dificultades que se presentan en Putumayo, resaltando el consumo de drogas por lo que señala la importancia de crear más talleres y campañas para combatir esta problemática, pero a su vez destaca todo lo que los encuentros han aportado a su formación, "he tenido la oportunidad de conocer y vivir varias experiencias, uno aprende, descubre talentos y tiene apoyo para los problemas, es muy importante y sería bueno fomentar los temas del ambiente".

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
