Title: ¿Dónde están los restos de Camilo?
Date: 2016-04-27 12:44
Category: Hablemos alguito
Tags: Documental Camilo Torres, El rastro de Camilo, Exhumación Camilo Torres, Restos de Camilo Torres
Slug: donde-estan-los-restos-de-camilo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/el-rastro-e1461778995272.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: El rastro de Camilo 

###### 27 Abr 2016 

El paradero de los restos mortales del sacerdote y revolucionario Camilo Torres Restrepo, continua siendo una incógnita. Los más recientes resultados de la [exhumación](https://archivo.contagioradio.com/asi-fue-la-exhumacion-de-los-posibles-restos-del-padre-camilo-torres/), realizada en el mes de enero de este año por la Fiscalía General de la nación, de los posibles despojos mortales, revelaron que estos no correspondían al perfil genético de los familiares del religioso e ideólogo.

¿Dónde están los restos de Camilo? es la pregunta que seguirán planteando en diversos escenarios que ven este hecho como un gesto necesario [para la reconciliación y la paz](https://archivo.contagioradio.com/busqueda-de-camilo-torres-un-gesto-hacia-la-paz/)de los colombianos, y un aporte simbólico de cara a las conversaciones con la guerrilla del ELN en su fase pública.

**"El rastro de Camilo"**

A partir de artículos de prensa, videos, archivos de audio, y entrevistas con diversas personas que de una u otra forma hicieron parte de la vida de Camilo Torres Restrepo, el director Diego Briceño Orduz, presenta a manera de documental apartes de la vida del sacerdote revolucionario.

“El rastro de Camilo”, es el título de la producción, estrenada en el marco de la conmemoración de los cincuenta años de la muerte de Camilo Torres, con el que sus realizadores y productores buscaron rendir homenaje a la vida y obra del sacerdote, teniendo en cuenta su influencia en el devenir político latinoamericano y revivir la incógnita sobre el paradero de sus restos mortales.

<iframe src="https://www.youtube.com/embed/wuay8NyFiME" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

“Cuando me di cuenta que en Colombia, no podíamos realizar el amor al prójimo solamente con la caridad, sino que había que cambiar las estructuras políticas económicas y sociales del país, entonces entendí que el amor al prójimo estaba muy ligado a la revolución”, es una de los apartes de los discursos de Camilo que se presentan en el documental, en el que amigos, familiares e incluso militares hablan sobre el destino de los restos de Camilo y las preguntas que hay alrededor de este hecho, ¿Por qué no aparecen? ¿Dónde están? ¿Por qué no quieren que se encuentren?

“Mi objetivo no era que la historia de Camilo llegara a los mismos de siempre, quería contar la historia para el común de los colombianos”, dice el director del documental, quien asegura que no quisiera dejar inconclusas aquellas preguntas, él espera tener una segunda parte del documental cuando hayan sido encontrados los restos del sacerdote.

En Hablemos Alguito nos acompañó el director Diego Briceño, la historiadora Lorena López y el psicólogo Nicolás Herrera, equipo de producción de "El rastro de Camilo".

<iframe src="http://co.ivoox.com/es/player_ej_11325583_2_1.html?data=kpaglJqZfJShhpywj5WVaZS1k5mSlaaWe46ZmKialJKJe6ShkZKSmaiRdI6ZmKiaqsbGsMbh0NiYo9HLucro0JKYp9GPtsLn1dfcjcnJb6TVzs7Z0ZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
