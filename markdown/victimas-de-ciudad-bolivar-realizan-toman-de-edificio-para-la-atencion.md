Title: Víctimas de Ciudad Bolívar se toman edificio CLAV en Bogotá
Date: 2017-12-11 12:38
Category: DDHH
Tags: Bogotá, ciudad bolivar, Gobierno Nacional, víctimas del conflicto armado
Slug: victimas-de-ciudad-bolivar-realizan-toman-de-edificio-para-la-atencion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/clav-e1513013868723.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Rodrigo Serrano] 

###### [11 Dic 2017] 

Para exigir que se satisfagan las necesidades de las víctimas del conflicto armado que habitan en la localidad de Ciudad Bolívar en Bogotá, más de 150 personas se tomaron las instalaciones **del Centro Local de Atención de Víctimas en el barrio el Restrepo en Bogotá**. El pliego de peticiones incluyen tierra, protección y garantías de no repetición.

De acuerdo con el comunicado que presentaron a la opinión pública, las propuestas de las víctimas no han sido tenidas en cuenta por los diferentes entes territoriales quienes **no les han brindado las garantías suficientes** en la ejecución de medidas de reparación y vida digna. En medio de las peticiones, manifestaron su intención que permanecer en el edificio de Atención a las Víctimas hasta que se establezca una mesa de diálogo y sus exigencias sean escuchadas. (Le puede interesar: ["Víctimas conmemoran los 20 años de la operación Septiembre Negro"](https://archivo.contagioradio.com/spetiembre_negro_operacion_victimas_memoria/))

### **Estas son las exigencias de las víctimas** 

Según Alba Quiñonez, representante de la Fundación Renacer de Familias de Víctimas del Conflicto Armado, ellas y ellos han redactado 11 puntos en donde reclaman que, primero, se les brinde **la oportunidad de desarrollar proyectos productivos dignos y sostenibles**. Además, exigen que se les otorgue vivienda gratuita como medida de reparación integral al igual que servicios gratuitos de educación superior.

También indican que es necesario que se establezca una **casa local para los derechos de las víctimas, la paz y la reconciliación**. Requieren “la organización de una ruta de retornos y reubicaciones colectivas en las zonas rurales y la elaboración de un censo multipropósitos “que permita una caracterización integral de la población víctima del conflicto armado” que habita en Ciudad Bolívar. (Le puede interesar: ["Víctimas de crímenes de Estado interponen tutela a favor de circunscripciones especiales de paz"](https://archivo.contagioradio.com/tutela_circunscripciones_paz_victimas_crimenes_estado/))

Para que mejoren sus condiciones de vida en Bogotá, le han exigido al Gobierno Nacional y a la administración local que **“construya una guardería y un jardín comunitario** para los niños y niñas, hijos de la población víctima del conflicto”. Además, indican que es necesario que se hagan los pagos correspondientes de “los apoyos de transporte y compensación para los integrantes de la Mesa Local de Víctimas”.

Con lo anterior en mente, las víctimas han exigido que hagan presencia en el edificio representantes del Estado incluida la **Alta Consejera de la Presidencia para los Derecho Humanos**. Quiñonez indicó que esperan reunirse con funcionarios de la Secretaría de Hábitat, la Unidad de Víctimas y la Secretaría de Gobierno.

![peticiones víctimas](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/12/peticiones-víctimas.jpeg){.alignnone .size-full .wp-image-49867 width="720" height="1280"}

<iframe id="audio_22585258" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22585258_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
