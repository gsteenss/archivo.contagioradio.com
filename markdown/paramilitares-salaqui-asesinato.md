Title: Paramilitares asesinan a joven de 26 años en Salaquí
Date: 2016-05-02 16:11
Category: Comunidad, DDHH
Tags: AGC, Chocó, Paramilitarismo, Salquí
Slug: paramilitares-salaqui-asesinato
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/Paramilitarismo1-e1459970325314.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

Foto:  
2 May 2016

El asesinato de Elias Pertúz, se da varios meses después de que las comunidades habitantes del territorio colectivo del Salaquí, **denunciaran una fuerte incursión paramilitar en la que participan entre 200 y 300 hombres**, con armas largas y uniformes, que se han identificado como integrantes de las Autodefensas Gaitanistas de Colombia.

Según la información suministrada por la Comisión de Justicia y Paz, el 26 de Abril **Elias habría sido acusado como colaborador de la guerrilla y el asesinato se produjo en presencia de la comunidad** que se encontraba en el sitio. La denuncia también advierte sobre la posibilidad de otros asesinatos ya que las [AGC anunciaron que venían más represalias contra quienes califican como “sapos”](https://archivo.contagioradio.com/paramilitares-persona-asesinada-choco/).

La denuncia va más allá, puesto que los mismos **paramilitares habrían afirmado que un desplazamiento tendría más consecuencias, si se desplazan “les irá peor”** relata la denuncia. Estas situación condenaría a las comunidades afrodescendientes a un confinamiento y un control territorial por parte de esas estructuras.

Otro de los aspectos señalados es que las entradas al [río Salaquí](https://archivo.contagioradio.com/?s=salaqui) y a ese territorio se llega por el río Atrato el cual está fuertemente custodiado por las Fuerzas Militares y la Armada Nacional haciendo presencia permanente y control sobre las embarcaciones, lo cual es evidencia de una **inacción o complicidad con las estructuras paramilitares que permanecen y controlan el territorio de comunidades negras.**

En el mismo territorio colectivo se presentó un asesinato de 5 pobladores, justo en el momento en que se dieron a conocer las primeras incursiones armadas en ese terriotorio. Hasta el momento se desconoce si hay acciones de combate a esas estructuras paramilitares por parte de las FFMM que hacen presencia permanente en la región.
