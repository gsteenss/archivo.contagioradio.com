Title: 19 seccionales en todo el país apoyan el paro camionero
Date: 2015-02-23 20:56
Author: CtgAdm
Category: Economía, Movilización
Tags: Asociación de Transportadores de Carga, Ecopetrol, Gobierno Nacional, Ministerio de Transporte, Paro camionero, Precios de los combustibles
Slug: al-menos-300-mil-camiones-estan-paralizados-en-paro-de-transportadores
Status: published

##### Foto: [ocadizdigital.es]

<iframe src="http://www.ivoox.com/player_ek_4128095_2_1.html?data=lZafmpWdeY6ZmKiakpeJd6KomJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaSmhqezstTWb9Lphqigh6adb9TdjM3O25DUpdPjjMjOz87Tssbm0IqgqJKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Alberto Arango, presidente de la Asociación Colombiana de Camioneros en Quindío] 

<iframe src="http://www.ivoox.com/player_ek_4128141_2_1.html?data=lZafmpaYdY6ZmKiakpyJd6KkmpKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaSmhqezstTWb9Lphqigh6adb9TdjM3O25DUpdPjjMjOz87Tssbm0IqgqJClstXd0Nbiy8aRaZi3jqjc0NnFq8rjjLfOxs7Tb46ZmKialg%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Pedro Aguilar, representante de los camioneros en Antioquia] 

<iframe src="http://www.ivoox.com/player_ek_4128150_2_1.html?data=lZafmpaZdI6ZmKiakpyJd6KnkZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRaaSmhqezstTWb9Lphqigh6adb9TdjM3O25DUpdPjjMjOz87Tssbm0IqgqJCyuMafxcqYtcbSuMLixcrfjZKJe6ShpNTb1sbLrdCfs8bRy9SPcYarpJKh&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Yamil Galeano, representante de los camioneros en Norte de Santander] 

<iframe src="http://www.ivoox.com/player_ek_4128106_2_1.html?data=lZafmpaUeo6ZmKiakpiJd6KplJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRl8ro1sbQy4qnd4a2lNOYxsqPsNDnjMjOz87Tssbm0NiYx9OPhtbZz8bjx9PYudPVjoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Juan Carlos Ramírez, representante de los camioneros en Buenaventura] 

<div class="vermas_ficha_desc">

</div>

**“Cada vez más transportadores se unen a la protesta... Casanare, Norte de Santander, Meta, Ipiales, Santander, Cundinamarca, Valle**”, asegura Pedro Aguilar representante de la Asociación Colombiana de Camioneros en Antioquia.

Son más de 300 mil los camiones que  están paralizados por cuenta del paro pacífico que realizan varios gremios de camioneros  en diferentes carreteras, con el objetivo de que **el gobierno nacional no solo escuche sus peticiones sino que cumpla sus compromisos con el gremio.**

“**Hay 19 seccionales que apoyan el paro** porque el gobierno siempre hace caso omiso a las peticiones y no hace valer los decretos que firmamos”,  y agrega que “los camioneros hemos sido pacíficos, es nuestra manera de visibilizar las propuestas hacia el gobierno – debido a que - la situación del camionero es crítica y caótica” resaltó Aguilar.

Las peticiones que resaltan los representantes de los camioneros de todo el territorio nacional son básicas para que el gremio pueda vivir de esta actividad. El primero punto, se refiere a que el gremio **asegura que el pago de fletes de las empresas no cubre los costos operativos,** esto, debido al Sistema de Información de Costos Eficientes (Sice) y la política de libertad vigilada.

Así mismo, el precio de los combustibles es otro de los incumplimientos del gobierno, ya que el sector camionero alega que de acuerdo al precio internacional del petróleo, en Colombia, se esperaba una **reducción de 3.700 pesos en la gasolina**, aun así, los camioneros pidieron únicamente una reducción de mil pesos, pero el gobierno solo ofrecía en un principio 200 pesos y finalmente **solo se logró que el combustible bajara 300 pesos**, algo irrisorio para los camioneros.

También le solicitan al gobierno controlar los precios de los peajes, ya que, de acuerdo a Pedro Aguilar, “hubo un incremento del **300% de las casetas de peaje y la creación de 17 casetas nuevas”.**

El tercer punto, se relaciona con el desplazamiento de 11 mil propietarios de tractomulas en zonas como Norte de Santander, Arauca, Putumayo y Casanare, ya que no hubo renovación de contratos por parte de Ecopetrol.

La última exigencia del gremio de camioneros al gobierno, es que** su actividad se clasifique como de alto riesgo**, de manera que puedan obtener su pensión al cumplir 20 años de trabajo.

Así las cosas, Yamil Galeano, representarte del gremio en el Norte de Santander del gremio, hace las cuentas sobre cuánto gastaría haciendo un recorrido de Norte de Santander a Bogotá:

“Un viaje de Cúcuta a Bogotá son alrededor de  800 kilómetros, para eso son necesarios 200 galones de gasolina que salen por 2 millones de pesos, en peajes, se gasta aproximadamente 300 mil pesos. Además, en el pago de fletes son 3 millones de pesos, también hay que agregarle los costos de carga y descarga, que son cerca de 600 mil pesos, y son costos que los camioneros no deberíamos pagar. Así mismo se debe sumar que al conductor se le cancela el 10%”. Todo esto quiere decir que lo que le queda al camionero tradicional prácticamente solo le alcanza para los gastos del viaje.

Pese a que hay algunos sectores de los camioneros que no están  de acuerdo con el paro, lo cierto es que “**el bolsillo del verdadero camionero está en vías de extinción”**, como lo dice, Alberto Arango, presidente de la Asociación Colombiana de Camioneros en el departamento del Quindío, uno de los departamentos que apoya el paro.

Por otro lado, Juan Carlos Ramírez, representante de los camioneros en Buenaventura, quien no entró asevera que “**las  protestas no han traído nada benéfico estamos, trabajando en otros mecanismo para que el gobierno nos cumpla”**. Sin embargo, el representante de Buenaventura, no niega que hay sobre oferta en el transporte de carga, lo que ha generado que el camionero tradicional esté quedando desplazado, ya que no tienen el músculo financiero para competir con empresas que prestan el mismo servicio.

El gremio asegura que se trata de exigencias **económicas necesarias para que los camioneros pueda operar tranquilamente,** pero por los constantes incumplimientos del gobierno, se vieron obligados a levantarse de la mesa de conversaciones con el Ministerio de Transporte.
