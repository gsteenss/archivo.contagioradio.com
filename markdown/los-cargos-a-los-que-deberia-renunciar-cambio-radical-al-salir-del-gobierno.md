Title: Los cargos a los que debería renunciar Cambio Radical al salir del gobierno
Date: 2017-10-10 13:12
Category: Nacional, Otra Mirada
Tags: Cambio Radical, cuota política cambio radical, JEP, parapolítica, partido cambio radical
Slug: los-cargos-a-los-que-deberia-renunciar-cambio-radical-al-salir-del-gobierno
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/10/cambio-radical-e1507659073410.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Publimetro Colombia] 

###### [10 Oct 2017] 

Tras la decisión del partido Cambio Radical, de **oponerse a respaldar la Jurisdicción Especial para la Paz** en el trámite que cursa en el Congreso de la República, el Gobierno Nacional, a través del Ministro del Interior, Guillermo Rivera, indicó que ese partido debe retirarse de la coalición del Gobierno y debe “asumir las consecuencias” de no contribuir a la aprobación de la JEP.

Si Cambio Radical es fiel a su palabra y sale de la coalición de Gobierno, no solamente debería **abstenerse de intervenir en los debates** de la implementación del acuerdo, sino que también debe retirarse de los cargos que ostenta como cuota de gobierno. (Le puede interesar: ["Cambio Radical y Centro Democrático se atraviesan al Fast Track"](https://archivo.contagioradio.com/cambio-radical-y-centro-democratico-se-atraviesan-al-fast-track/))

### **Funcionarios que deberían dejar el cargo** 

Con esto en mente, diferentes sectores han manifestado que los funcionarios que hacen parte de las instituciones del Gobierno y son cuota de Cambio Radical, deben presentar su renuncia. Por esto, diferentes medios de información han indicado que **ya hay funcionarios alistando su salida**.

Entre ellos se encuentran el Ministro de Ambiente Luis Gilberto Murillo y el Ministro de Vivienda Jaime Pumarejo, de quienes **se espera la renuncia este mismo día**. Al igual que ellos, deberían renunciar el Superintendente (E) de Notariado y Registro Jairo Alonso Mesa y la directora de Instituto Colombiano de Bienestar Familiar, Karen Abudinén. (Le puede interesar: ["Cambio Radical y los medios a la Jurisdicción Especial para la Paz"](https://archivo.contagioradio.com/cambio-radical-y-los-miedos-a-la-jurisdiccion-especial-de-paz/))

Si bien éstos son los más reconocidos, también **son cuota de ese partido** el Superintendente de Industria y Comercio, Pablo Felipe Robledo y el Superintendente de Servicios Públicos Domiciliarios José Miguel Mendoza. Además, según Noticias Uno, la presidenta de Servicios Postales de Colombia 4-72, Adriana Barragán y el presidente de Bancoldex, Mario Suárez Melo también son cuota política de Cambio Radical.

### **Cambio Radical tiene más de 349 integrantes sancionados** 

A la polémica y tensa relación con Gobierno Nacional, se suman las denuncias que han realizado diferentes sectores de la sociedad las cuales indican que **ese partido es el que más integrantes sancionados**, investigados y condenados tiene.

De acuerdo con el portal Las 2 Orillas, Cambio Radical **tiene sancionados 349 integrantes**, 41 han sido destituidos de sus cargos, 19 congresistas han sido condenados por parapolítica y 44 están siendo investigados. (Le puede interesar: ["Congresistas de la Unidad Nacional modificaron la JEP afectando derechos de las víctimas"](https://archivo.contagioradio.com/congresistas-la-unidad-nacional-modificaron-la-jep-afectado-derechos-las-victimas/))

A esto se suma, que el partido ha avalado en sus filas a personas condenadas por parapolítica como lo es **“Kiko Gómez” y Oneida Pinto**. También, en la actualidad 11 alcaldes de ese partido político están siendo investigados por corrupción y 8 gobernadores fueron investigados y algunos condenados por el mismo delito.

Finalmente, cabe recordar que, **algunos sectores han vinculado al ex Fiscal Anticorrupción**, Gustavo Moreno, uno de los responsables del escándalo de corrupción en las altas cortes y la Fiscalía, como cuota política de Cambio Radical. Además, en mayo de 2017, el presidente de ese partido, Jorge Enrique Vélez, había anunciado que, si Vargas Lleras llega a ser presidente, debe suspender las negociaciones con el ELN en Quito, Ecuador.

###### Reciba toda la información de Contagio Radio en [[su correo]
