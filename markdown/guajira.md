Title: “Duque, atienda primero la crisis humanitaria en la Guajira”: mujeres Wayuú
Date: 2019-02-21 12:57
Author: AdminContagio
Category: DDHH, Política
Tags: Fuerza de Mujeres Wayuú, intervención militar, La Guajira, Venezuela
Slug: guajira
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/guajira_wayuu_contagioradio.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/guajira.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/02/Guajira-e1455139314228.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/guajira2-e1494358244369.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/guajira-paro.jpeg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Wayuu_Desierto_La_Guajira.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Archivo] 

###### [21 Feb 2019] 

Ante el anuncio de que el Gobierno del Presidente Iván Duque prepara la entrega de apoyo humanitario a Venezuela para este viernes, algunos han denunciado que **el Estado no ha brindado el mismo respaldo a las comunidades en necesidad en el país**, **particularmente en la Guajira donde el pueblo Wayuú enfrenta una crisis humanitaria por el desabestecimiento de agua y alimentos.**

Jakeline Romero Epiayu, integrante de la organización Fuerza de Mujeres Wayuú, manifestó que si bien reconoce las necesidades del país vecino, también sostuvo que **las comunidades indígenas, afrodescendientes y campesinas de la Guajira padecen de problemáticas similares**, a raíz de las afectaciones sociales, económicas y ambientales producidas por el proyecto multinacional, El Cerrejón.

Cabe recordar, que en los 36 años en que se ha desarrollado El Cerrejón, la Guajira perdió **17 cuerpos de agua, varias especias de flora nativa y 72 mil hectáreas de las tierras más fértiles en el departamento,** ubicadas en el valle del Río Ranchería, donde hoy opera este megaproyecto de explotación de carbón. Actualmente, comunidades Wayuú de la Guajira temen la desaparición de su etnia ante la muerte de miles de niños cada año por desnutrición. (Le puede interesar: "[El río que se robaron: el exterminio de la Nación Wayuú](https://archivo.contagioradio.com/el-rio-que-se-robaron-el-exterminio-de-la-nacion-wayuu/)")

Romero indicó que el Presidente debería responder primero a las necesidades de su ciudadanía antes de atender a las problemáticas de pueblos en el exterior, particularmente cuando este apoyo humanitario se utiliza como pretexto para una intervención militar. Al respecto, la lideresa sostuvo que cada Estado, incluso el de Venezuela, tiene la autonomía para resolver sus asuntos internos.

Mientras tanto en la Guajira, las comunidades se indignan ante la cooptación de servicios que debería suministra y garantizar el Estado, como la agua, por parte de las empresas de El Cerrejón, los cuales generan una relación de dependencia entre el gobierno local y el sector privado. Por tal razón, debería ser una prioridad para el Gobierno de Duque la crisis de la Guajira. "Al Gobierno Duque, le decimos que primero atienda la casa, que atienda la crisis humanitaria que tenemos en la Guajira."

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
