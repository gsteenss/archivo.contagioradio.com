Title: Otra Mirada: Palestina un ejemplo de dignidad
Date: 2020-07-09 21:34
Author: PracticasCR
Category: Otra Mirada, Otra Mirada, Programas
Tags: Anexión de Palestina, Conflicto Israel-Palestina, Palestina
Slug: otra-mirada-palestina-un-ejemplo-de-dignidad
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/bandera_palestina_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph {"align":"right","textColor":"cyan-bluish-gray","fontSize":"small"} -->

Foto: radio.uchile.cl

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

La vida y dignidad del pueblo palestino se encuentran en riesgo pues sin ser suficiente con el aumento de los casos de contagio por Covid-19 ahora deben enfrentar la intención de anexión de parte de Palestina a Israel. Este hecho, invisibilizado por la comunidad internacional, dejaría a gran parte de la población palestina sin hogar y sin el territorio que les pertenece; sin embargo, frente a este conflicto han demostrado ser un ejemplo de dignidad y de resistencia. (Le puede interesar: [Consejo de Seguridad de la ONU llama a un cese al fuego global](https://archivo.contagioradio.com/consejo-de-seguridad-de-la-onu-llama-a-un-cese-al-fuego-global/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Nuestros invitados explican cómo es la situación del territorio por la pandemia y el conflicto, y qué significa para Palestina la anexión de su territorio . Este panel estuvo integrado por Víctor de Currealugo, analista internacional, Felipe Medina, magíster en estudios de medio oriente, Ali Nofal, colombo-palestino representante de la comunidad palestina y Juani Rishmawi, quien trabaja junto a la organización Health Work Committees.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Así mismo, compartieron cómo ven y experimentan lo que es la situación actual en el territorio palestino desde diferentes escenarios. Tal como Ali Nofal que desde Colombia, responde qué ha significado salir de Palestina, ver toda la situación y luchar desde otro continente. Además, explica que en Colombia “debemos unir nuestras fuerzas para ganar el apoyo del pueblo y luchar por la causa Palestina”. 

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

De igual manera, invitan a la comunidad colombiana a estudiar el conflicto palestino y descubrir que existen muchas similitudes entre lo que ocurre en Palestina y Colombia para no caer en el error de pensar que las distancias hacen que se vivan cosas completamente diferentes, sino por el contrario, solidarizarnos y luchar desde acá en contra del enfoque del presidente Duque frente a Israel. (Si desea escuchar el programa del 7 de julio: [Colombia y sus relaciones internacionales](https://bit.ly/2Z8JzY6))

<!-- /wp:paragraph -->

<!-- wp:core-embed/facebook {"url":"https://www.facebook.com/contagioradio/videos/614046866172793","type":"video","providerNameSlug":"facebook","className":""} -->

<figure class="wp-block-embed-facebook wp-block-embed is-type-video is-provider-facebook">
<div class="wp-block-embed__wrapper">

https://www.facebook.com/contagioradio/videos/614046866172793

</div>

</figure>
<!-- /wp:core-embed/facebook -->

<!-- wp:block {"ref":78955} /-->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
