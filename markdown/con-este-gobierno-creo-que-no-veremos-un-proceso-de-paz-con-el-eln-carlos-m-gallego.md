Title: Con este Gobierno creo que no veremos un proceso de paz con el ELN: Carlos M. Gallego
Date: 2020-04-28 11:12
Author: AdminContagio
Category: Actualidad, Paz
Tags: cese al fuego unilateral, comunidades, ELN
Slug: con-este-gobierno-creo-que-no-veremos-un-proceso-de-paz-con-el-eln-carlos-m-gallego
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/ivan-duque-y-eln.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:heading {"align":"right","level":6,"textColor":"cyan-bluish-gray"} -->

###### Foto: Contagio Radio {#foto-contagio-radio .has-cyan-bluish-gray-color .has-text-color .has-text-align-right}

<!-- /wp:heading -->

<!-- wp:paragraph -->

Este lunes se conoció un comunicado del Ejército de Liberación Nacional ELN, en el que se anuncia que el cese unilateral al fuego establecido por este grupo, atendiendo el llamado de organismos internacionales y comunidades para afrontar el Covid-19, se levantará el próximo 30 de abril como estaba dispuesto.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Además del anuncio, la guerrilla lamenta que el Gobierno no respondiera con un cese de sus acciones y diera pasos en el sentido de reanudar los diálogos de paz, suspendidos desde el pasado enero de 2019. Al tiempo que llama para que se respeten los protocolos preestablecidos en casos como ese, y se permita el regreso de la delegación de negociación a territorio colombiano.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El ELN ha dado pasos, el Gobierno...**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Carlos Medina Gallego, profesor de la Universidad Nacional en Ciencia Política señala que el ELN no ha encontrado una ruta para volver con el Gobierno a la mesa de negociación en primer lugar, porque el presidente Duque ha puesto "pre requisitos y condicionantes que impiden que el proceso marche".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

En segundo lugar, porque para Medina, decididamente el Gobierno no ha querido avanzar pese a los ceses unilaterales realizados por la guerrilla y varias entregas de secuestrados. A ello se suma que los jefes negociadores siguen en La Habana por la decisión de Duque de desconocer el protocolo establecido por las partes en negociación en caso de que se levantara la mesa de negociación.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **El reinicio de las hostilidades, una tragedia para el país**

<!-- /wp:heading -->

<!-- wp:paragraph -->

Medina sostiene que el conflicto armado está vivo en muchas regiones del país, desde el Catatumbo hasta el litoral pacífico, regiones en las que hace presencia el ELN y otros actores inmersos en dinámicas de diferentes tráficos y pugnas territoriales. Por esa razón, asegura que el anuncio de no prorrogar el cese al fuego es una tragedia, "porque significará la reactivación de las hostilidades" en muchos lugares de la geografía nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Siguiendo con esa idea, la paz con el ELN implicaría desarmar y reincorporar a uno de los actores armados que hace parte de los cinco Conflictos Armados No Internacionales (CANI) que tiene Colombia. Pero entiende que la guerrilla también vea ese panorama como difícil con este Gobierno. (Le puede interesar:["Cese Unilateral del ELN completa 18 días con balance positivo"](https://archivo.contagioradio.com/cese-unilateral-del-eln-completa-18-dias-con-balance-positivo/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Esto, porque el reflejo de lo que podría ocurrir lo tiene en el proceso de paz que se pactó con las FARC, y sobre el que el Gobierno tiene una narrativa de 'simulación' de la implementación que dice dar grandes avances en el contexto internacional, pero tiene un discurso de negación del Acuerdo en el plano nacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

A ello se suma la poca efectividad mostrada en la protección de excombatientes, evidenciada en que después de la Firma del Acuerdo han sido asesinados más de 190 firmantes de la paz. "Entonces, cuál es el escenario para construir paz cuando el Gobierno es el primer agente de la violencia, y lo es porque no cumple lo acordado", se pregunta Medina.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### **Yo soy escéptico de que este Gobierno desarrolle un proceso de paz con el ELN**

<!-- /wp:heading -->

<!-- wp:paragraph -->

El profesor se declara escéptico de que en este gobierno, que ya está llegando a la mitad de su mandato, "pueda desarrollarse un proceso con el ELN". Y añade que los interesados en la paz, han hecho propuestas "y todo lo que es posible sugerir para que un proceso de paz pueda ser exitoso, pero nada de eso llega a oídos del Gobierno".

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Otra opción planteada por Medina es que el [Gobierno](https://www.justiciaypazcolombia.com/la-paz-y-la-pandemia-silencio-del-gobierno-y-distorsion-del-eln/) haga 'oídos sordos' al clamor de las comunidades, la iglesia, la comunidad internacional y la sociedad civil en general que ha pedido retomar los diálogos de paz, y avanzar en la construcción de la misma mediante el diálogo. (Le puede interesar:["Comunidades del Cauca se sienten en riesgo ante la escalada de violencia"](https://archivo.contagioradio.com/en-cauca-toda-la-comunidad-se-siente-en-riesgo/))

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/arzobispodecali/status/1255112124730310657","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/arzobispodecali/status/1255112124730310657

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:html -->

[Comunicado ELN sobre cese a...](https://www.scribd.com/document/458797932/Comunicado-ELN-sobre-cese-al-fuego-unilateral#from_embed "View Comunicado ELN sobre cese al fuego unilateral on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe class="scribd_iframe_embed" title="Comunicado ELN sobre cese al fuego unilateral" src="https://www.scribd.com/embeds/458797932/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-JUfc3V18VT7umirwN0Do" data-auto-height="true" data-aspect-ratio="0.7729220222793488" scrolling="no" width="100%" height="600" frameborder="0"></iframe>  
<!-- /wp:html -->

<!-- wp:block {"ref":78955} /-->
