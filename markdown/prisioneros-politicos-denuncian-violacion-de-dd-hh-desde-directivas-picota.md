Title: Prisioneros políticos denuncian violación de DD.HH desde directivas de La Picota
Date: 2018-11-14 18:08
Author: AdminContagio
Category: DDHH, Nacional
Tags: La Picota, presos politicos
Slug: prisioneros-politicos-denuncian-violacion-de-dd-hh-desde-directivas-picota
Status: published

###### Foto: CC 

######  14 Nov 2018 

Desde el patio 4to de la cárcel **La Picota**, los prisioneros políticos de Farc denuncian la negligencia de las autoridades del centro penitenciario, en cabeza de Imelda López, quienes les niegan el suministro de objetos de aseo básicos, provocando el aumento de epidemias,  mientras que a otros reclusos, capturados por corrupción se les brinda toda clase de lujos.

Los internos responsabilizan a Imelda López, directora encargada de La Picota y al general Hernández de privarles útiles como  jabón, criolina, cloro, talco para los pies, escobas  y traperos. Dicha escasez de elementos no solo representa una violación a los derechos humanos, sino que también ha derivado en una **epidemia de diarrea y gripa entre los reclusos.**

Asimismo, informan que el **INPEC** se niega a venderles elementos básicos como papel higiénico, utensilio que tampoco puede ser ingresado y entregado por sus familiares los días de visita, "muchos de los prisioneros de guerra y el resto de población reclusa, la mayoría viene desde muy lejos, no tenemos quien nos envíe una encomienda", explica  José Ángel, uno de los prisioneros.

En contraste con la incertidumbre que viven los prisioneros políticos del patio 4to de La Picota, Imelda López, ha favorecido la situación de otros reclusos, como **Ronald Housni Jaller, ex gobernador de San Andrés**, capturado el pasado octubre y trasladado a la Picota, quien cuenta con diversos artículos en su celda que hacen más cómoda su estancia en la prisión.

Housni Jaller, recluido en el  pabellón ERE Sur e i**nvestigado por concierto para delinquir, peculado por apropiación y contrato sin cumplimiento de requisitos legales** cuenta con una celda que incluye un televisor, un decodificador de DirecTV, una nevera, un computador portatil, un escritorio, una impresora además de utensilios para baño como espejos, tapetes y canecas,  objetos autorizados desde la dirección del centro penitenciario, la mismo que niega  a sus presos políticos el acceso a utensilios básicos de sanidad.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU) 

###### 
