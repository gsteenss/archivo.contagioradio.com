Title: ESMAD destruye tres fincas en medio de movilización en Caquetá
Date: 2016-08-31 12:51
Category: Ambiente, Nacional
Tags: Caquetá, Emerald Energy en Colombia, ESMAD, petroleo, Represión
Slug: esmad-destruye-tres-fincas-en-medio-de-movilizacion-en-caqueta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/08/ESMAD-en-Paujil.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Alirio Muñoz ] 

###### [31 Ago 2016] 

Por lo menos **tres fincas campesinas de la vereda La Reforma, en Paujil fueron destruidas por miembros del ESMAD** en medio de la movilización que adelantan las comunidades en Caquetá, en rechazo a las actividades de exploración petrolera que la empresa Petroseimic realiza para Emerald Energy y Ecopetrol, según denuncia Martín Trujillo, integrante de la Mesa por la Defensa del Territorio, el Agua y la Vida.

De acuerdo con el poblador desde el domingo en la madrugada, Petroseismic realiza las actividades de sísmica en las veredas Puerto Albán y La Reforma, en las que se han presentado disturbios con integrantes del ESMAD quienes, según afirman los campesinos, **están custodiando a la empresa en la violación sistemática de la normatividad** referida a la autodeterminación de los pueblos frente a las actividades extractivas que afectan su normal desarrollo económico social y ambiental.

Trujillo insiste en que tanto las empresas como el Estado colombiano, violan el artículo 79 de la Constitución que establece que todas "Todas las personas tienen derecho a gozar de un ambiente sano. La ley garantizará la participación de la comunidad en las decisiones que puedan afectarlo. **Es deber del Estado proteger la diversidad e integridad del ambiente**, conservar las áreas de especial importancia ecológica y fomentar la educación para el logro de estos fines".

Según las más recientes declaraciones del Ministro de Minas y Energía, el presidente de Ecopetrol y el presidente de la Agencia Nacional de Hidrocarburos las actividades de exploración en las veredas ya finalizaron y por lo tanto se retirarían de la región; sin embargo, las comunidades manifiestan que **el daño ya está hecho y que deberán pasar por lo menos 500 años para repararlo**, pues de las actividades de exploración realizadas hace 3 años ha dependido la actual destrucción de fuentes hídricas.

El líder asevera que las comunidades están indignadas porque sus demandas para la preservación del medio ambiente no han sido tenidas en cuenta, la empresa petrolera insiste en continuar las actividades de exploración, mientras que la fuerza pública asegura que tienen una **orden directa desde Bogotá para que las exploraciones "se realicen por encima de lo que se les atraviese"**.

"Caquetá es una tierra pacífica, nosotros somos labriegos, somos gente de muy bajos recursos provenientes del desplazamiento que se dio en la época de la violencia, cuando nuestros padres y abuelos llegaron huyendo”, afirma Trujillo e insiste en que los choques con el ESMAD se han presentado porque **a menos de 10 metros de las fincas la empresa está realizando las perforaciones profundas**.

De la [[fuerte represión por parte del ESMAD](https://archivo.contagioradio.com/12-heridos-y-9-detenidos-deja-represion-de-la-fuerza-publica-en-caqueta/)] han resultado por lo menos 16 pobladores heridos y la vereda La Reforma amaneció con todas las vías de acceso bloqueadas, lo que para las comunidades es una muestra de que "el Estado no está trayendo la paz que está prometiendo". Pese a ello, los campesinos **adelantan el proceso para la consulta popular que les permita preservar la biodiversidad** endémica del departamento; continúan con sus denuncias a nivel internacional y para dentro de quince días tienen programada una gran marcha.

<iframe src="https://co.ivoox.com/es/player_ej_12730928_2_1.html?data=kpeklZWddpmhhpywj5acaZS1kpiah5yncZOhhpywj5WRaZi3jpWah5ynca7V09nW0JC4ttbeytHZ0YqWh4zBxtjOjdXTtozgwpCxx8vJstTVjMnSzpC4qdPmytnc1M7TaZO3jMrZj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk.&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)] ] 
