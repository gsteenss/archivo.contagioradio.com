Title: Captura de Santiago Uribe develaría más delitos de familia Uribe Vélez
Date: 2016-03-01 15:39
Category: Judicial, Nacional
Tags: Alvaro Uribe, Paramilitarismo, Santiago uribe
Slug: captura-de-santiago-uribe-develaria-mas-delitos-de-familia-uribe-velez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Santiago-Uribe.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: La Prensa 

<iframe src="http://co.ivoox.com/es/player_ek_10633900_2_1.html?data=kpWjlZiddJGhhpywj5WbaZS1k5Wah5yncZOhhpywj5WRaZi3jpWah5yncaTV0dni1MaPqMaftMbb1s7Fq9CfttfWxMqPqMbqxtHO1Iqnd4a1pcaYz4qnd4a1ktiYxsrQrdXj1JDRx5DKpY6ZmKiapdTSuMLbytSYtMbIrdCfjoqkpZKY&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Daniel Prado, Abogado] 

###### [1 Mar 2016] 

“Llevábamos años esperando para que la Fiscalía tomara esta decisión soportada sobre un sin número de pruebas”, dice Daniel Prado, abogado de la Comisión de Justicia y Paz que acompaña el caso de [[Eunicio Alfonso Pineda](https://archivo.contagioradio.com/capturan-a-hermano-alvaro-uribe-por-vinculos-con-paramilitarismo/)]**, trabajador en la hacienda** **La Carolina** que era propiedad del ganadero **Santiago Uribe.** Ahora, Pineda se encuentra en asilo político en **Europa** tras ser blanco de amenazas luego de haber rendido declaraciones sobre los vínculos entre la familia Uribe Vélez y grupos paramilitares.

El testimonio de Eunicio fue clave para la vinculación de Santiago a los más de 33 crímenes cometidos por el grupo paramilitar 'Los 12 Apóstoles', Pineda afirma que el apodo con el que se conocía al ganadero Uribe era 'El Abuelo' y reiteró que "los que **mandaban ahí eran Rodrigo Alzate, Julián Bolívar y Santiago Uribe".**

Después de dictarse esta medida de aseguramiento contra el hermano menor del senador Álvaro Uribe, el abogado Prado, asegura que tanto él como las víctimas, esperan que se inicie una investigación integral y se vincule a quienes se prestaron para ser el enlace entre la familia Uribe Vélez y estructuras paramilitares, pero también sobre los vínculos con el narcotráfico, "esperamos que las afirmaciones que se han hecho develen como se llegó al poder en Antioquia y nivel nacional".

El abogado habla también de Alberto Uribe Vélez quien tendría vínculos con el narcotráfico por su relación con 'Los Pepes' y Pablo Escobar. Así mismo, señala a **Ernesto Garcés Soto, exsenador y cafetero, quien habría sido el enlace entre familia Uribe y los hermanos Castaño.**

Garcés, está sindicado en otro proceso por haber cometido asesinatos y actos de tortura en Antioquia, además de haber patrocinado a grupos paramilitares. También algunos primos de Alvaro Uribe estarían vinculados a asesinatos, desapariciones y torturas, "esperamos que esta vez la Fiscalía haga una investigación integral y seria", sostiene el abogado.

Según Prado, Santiago Uribe debería responder tanto por los delitos de concierto para delinquir como la conformación de grupos paramilitares que a su vez implicaría, enfrentar la justicia por **las 33 víctimas asesinadas y desaparecidas por paramilitares, que se originaron en la región de Yamural,** dónde habría surgido el “embrión de la última generación de paramilitares”, indica el integrante de la Comisión de Justicia y Paz.

"Llamamos a las personas que conocen del actuar criminal de la familia Uribe Vélez para que confíen en la justicia”, expresa el abogado Prado quien añade que las víctimas se encuentran  preocupadas por su seguridad por la trascendencia del hecho, pese a que durante años han sido personas perseguidas.

En las próximas horas Santiago Uribe será trasladado a Bogotá para ser recluido en el búnker de la Fiscalía, luego de haber sido capturado por las declaraciones del **mayor (r) de la Policía, Juan Carlos Meneses quien aseguró que Uribe **está relacionado con la creación del grupo paramilitar 'Los 12 apóstoles'.
