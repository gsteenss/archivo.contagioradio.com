Title: Arte por la Vida, la vida por el Arte II
Date: 2015-08-21 10:25
Category: Movilización, Nacional
Tags: Asesinato de Diego Felipe Becerra, Brutalidad Policiaca, Diego Felipe Becerra, ESMAD, impunidad, Wilmer Alarcon
Slug: el-proximo-sabado-arte-por-la-vida-la-vida-por-el-arte-ii
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/tripido_homenaje_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Gustavo Trejos 

###### [21 Ago 2015]

En el marco de la **conmemoración del cuarto aniversario del asesinato del joven grafitero Diego Felipe Becerra** a manos del policía Wilmer Alarcón, se desarrollará este sábado el evento “El Arte por la Vida, La vida por el Arte II” en la zona verde contigua al puente de la calle 116 con avenida Boyacá en Bogotá desde las 9 am y hasta las 8 de la noche. El evento contará con la participación **más de 100 artistas que se manifestarán contra la brutalidad policial y por el respeto a los derechos de los jóvenes** en Bogotá y en Colombia.

La idea del evento es realizar una “Gran Pintanda” por lo que los organizadores recomiendan llevar brochas, rodillos, extensores o palos de escoba, vinilos, envases plásticos y las demás herramientas para desarrollar la actividad y “dejar un mensaje muy claro” afirman.

El caso de Diego Felipe Becerra **continúa en proceso de juicio con 12 personas capturadas pero ninguna condena hasta el momento**. Se espera que en Septiembre próximo inicie el juicio contra Wilmer Alarcón y se den las capturas necesarias para que la intención de la alteración de la escena del crimen y el encubrimiento por parte del cuerpo de policía no queden en la impunidad. Vea también [Vendrían más capturas por el asesinato de Diego Felipe Becerra](https://archivo.contagioradio.com/se-preparan-8-capturas-mas-por-el-crimen-de-diego-felipe-becerra/).
