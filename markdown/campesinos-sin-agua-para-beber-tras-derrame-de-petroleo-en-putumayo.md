Title: Fotos: Así quedó la carretera y las fuentes hídricas tras derrame de petróleo en Putumayo
Date: 2015-06-11 15:00
Category: Ambiente, Otra Mirada
Tags: Comisión Minero-energética y ambiental de la mesa Regional del Putumayo, Derrame de petróleo en Putumayo, Empresa petrolera Vetra, Paola Artunduaga, Vereda "La cabaña" Puerto Vega Teteyé
Slug: campesinos-sin-agua-para-beber-tras-derrame-de-petroleo-en-putumayo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/06/petroleo16.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

##### [ Foto: eluniversal.com.co] 

<iframe src="http://www.ivoox.com/player_ek_4628021_2_1.html?data=lZufmpWWdY6ZmKiak5WJd6KnlZKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRh8Ln1YqwlYqlddTo09TTx5DFscPdxtPhw9GPtNDmjMnS1NfFscLhysrb1tSPqMaf0crh1Iqnd4a2lNHS0ZDJcYarpJKw0dPYpcjd0JC%2Fw8nNs4yhhpywj5k%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Campesinos afectados de la vereda "La Cabaña"] 

##### <iframe src="http://www.ivoox.com/player_ek_4628032_2_1.html?data=lZufmpWXdo6ZmKiakpqJd6KmmJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRlMLjzcaYo9fYucXpwszOh5enb6Tjzs7gy4qnd4a2lNOYr87SqdPjjsrbx9fLaaSnhqam1s7HpYztjMbaxM7JstWhhpywj6jTstXVyM7cjbfFqMrjjJKSmaiReA%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe> 

##### [Paola Artunduaga,  Comisión Minero-energética y ambiental de la mesa Regional del Putumayo] 

[11 jun 2015]

Tras el derrame de petróleo ocurrido el día lunes, **la mayor preocupación de los habitantes de la vereda "La cabaña"** del corredor Puerto Vega Teteyé, Putumayo, tiene que ver con **la contaminación de seis fuentes de agua potable y siete espejos de agua dedicados a la piscicultura**, afectados por la catástrofe ambiental que perjudica a **75 familias residentes en el lugar**.

Contagio Radio junto a una comisión de derechos humanos, se dirigió al lugar de los hechos donde se habló con los pobladores de la zona. Allí se verificó el derrame que se ha expandido cerca de **4 kilómetros por donde se ubica la vereda La Cabaña**, uno de los lugares más afectados debido a que con el paso de los carros, buses y mulas, el crudo continúa esparciéndoce. Es por eso, que **los campesinos han decidido cerrar el paso de la carretera** con el único objetivo de mitigar el impacto ambiental.

Adicional al desastre sobre las fuentes hídricas, **los fuertes olores producidos por la contaminación, han empezado a provocar deterioro en la salud de algunos pobladores** quienes presentan fuertes dolores de cabeza y fiebre en niños y adultos por la exposición durante las 24 horas del día a estas condiciones. Así mismo, este derrame, afecta a los animales de los campesinos, como el ganado, las gallinas, perros y gatos, pero además a las especies silvestres como las aves que viven en ese lugar.

Aunque los vecinos de la vereda "la Cabaña" reportaron la visita de Instituciones como el Bienestar Familiar y la Defensoría del Pueblo durante el día de ayer, **hasta el momento no existe un compromiso por parte del Estado para ayudar a la comunidad,** y su presencia se ha limitado hasta el momento al registro fotográfico y documental de las afectaciones en la zona.

De igual manera, los habitantes manifiestan que **las empresas petroleras que explotan el crudo en la región hasta la fecha no han aportado** **"siquiera una bolsa de agua"** para ayudar a cubrir la necesidad de agua potable, situación que ha producido un sentimiento de abandono en los pobladores.

**Paola Artuduaga**, quien hace parte **de la Comisión Minero-energética y ambiental de la mesa Regional del Putumayo**, encargada de verificar la catástrofe ambiental, **asegura que en la zona una situación similar se había presentado en mayo de 2012**, y agrega que por la extensión del corredor, apróximadamente 15 km, han podido registrar **afectación con mayor impacto en 62 puntos** .

En relación con el tema de la responsabilidad sobre los daños ocasionados históricamente, **Artunduaga apunta que en 32 puntos de los 62 caracterizados** dentro del estudio de la Comisión, **se encontraron derrames y fallas técnicas producto del mal manejo del operador y de la empresa**, afirmando que **"**no es de olvidar y dejar de lado que hay responsabilidades directas también por parte de la empresa**"**.

La comisión encontró que **el plan de contigencia**, establecido para situaciones como la actual, "**nunca respondió a las necesidades reales de descontaminación de la zona, lo único que se hizo fue remolques de tierra y acumular el terreno afectado pero en ningún momento se le dio el tratamiento adecuado a los contaminantes hídricos**", lo que refuerza la exigencia de los habitantes de realizar una descontaminación eficiente y real.

Contrario a lo expuesto en algunos medios de información, **los pobladores tienen cerrada la vía** para reclamar una solución por parte del Estado en la descontaminación de las viviendas, donde el crudo ha ingresado hasta las habitaciones.

\
