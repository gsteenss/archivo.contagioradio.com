Title: Para los paramilitares la minería sería más rentable que la coca en el sur de Córdoba
Date: 2019-07-08 16:00
Author: CtgAdm
Category: DDHH, Entrevistas
Tags: campesino, coca, paramilitares, Sur de Córdoba
Slug: paramilitares-mineria-sur-de-cordoba
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Paramilitares-en-el-Sur-de-Córdoba.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: LaRazon.co  
] 

Este sábado se denunció el asesinato del campesino **Manuel Osuna Tapias, integrante de la Asociación de Campesinos del Sur de Córdoba (ASCSUCOR)** a manos de un actor armado ilegal; hecho que para los líderes sociales del territorio muestra la dinámica de desplazamiento que se quiere imponer en el sur de Córdoba. (Le puede interesar: ["Danuncian asesinato del campesino Manuel Osuna Tapias en San José de Uré"](https://archivo.contagioradio.com/asesinato-manuel-osuna/))

Según **Arnobis de Jesús Zapata, coordinador de Asociación,** estas acciones tienen la intención de generar terror "matando las personas delante de la gente, quitando cabezas; **cometiendo los mismos horrores que cometían los paramilitares en su tiempo para que la gente tenga temor de seguir viviendo en su territorio". **(Le puede interesar: ["Prácticas paramilitares del pasado resurgen en Sucre"](https://archivo.contagioradio.com/practicas-paramilitares-del-pasado-resurgen-en-sucre/))

### **¿Están matando líderes por acogerse al Plan Nacional Integral de Sustitución (PNIS)?** 

En el comunicado que denunció el hecho se llamaba la atención sobre la cantidad de asesinatos de campesinos en el sur de Córdoba: 15 campesinos asesinados este año que se habían acogido al PNIS. No obstante, Zapata indicó que "el hecho de que la mayoría de los asesinados sean integrantes de PNIS no consideramos que sea una razón para que los campesinos los maten; esto, porque **no ha existido una oposición al Programa"**. (Le puede interesar:["Asesinan al líder y beneficiario de Plan de Sustitución Manuel Gregorio González en Córdoba"](https://archivo.contagioradio.com/asesinan-al-lider-social-manuel-gregorio-gonzalez-en-cordoba/))

El Coordinador de ASCSUCOR aclaró que todos los que asesinan en el sur de Córdoba son beneficiarios del PNIS porque tenían Coca, pero los grupos armados no estarían interesados en atacar a quienes se acogieron al Programa. De hecho, Zapata detalló que en el Departamento solo hacen falta cerca de 400 hectáreas de este cultivo para que se declare al territorio libre de Coca y **los grupos ilegales que operan en la región no estarían interesados en detener este proceso, por el contrario "les aconsejan que se metan a programas de sustitución"**.

### **"Están matando campesinos para que la gente coja miedo y se vaya de los territorios"** 

El Activista aseguró que en el sur de Córdoba, la financiación de los grupos armados ilegales no es la Coca, porque se han quitado casi todos los cultivos por sustitución voluntaria, "la principal fuente de financiamiento es el cobro de extorsiones. Por lo tanto, "lo que determina los asesinatos son factores asociados a la urgencia que tienen intereses económicos de entrar a la región". (Le puede interesar: ["No se quedó en amenazas panfeto contra ocupantes de predios en Tierralta, Córdoba"](https://archivo.contagioradio.com/panfleto-contra-ocupantes-predios-tierralta-cordoba/))

Algunos de estos intereses estarían ligados a la explotación minero energética que se pueda generar en municipios como Puerto Libertador, Montelíbano y San José de Uré; en los que se encuentran yacimientos de cobre, carbón, oro y, recientemente se anunció el descubrimiento de yacimientos de Petróleo. En ese sentido, algunas organizaciones defensoras de derechos humanos que trabajan en el territorio creen que los grupos tendrían intereses económicos para desplazar a la gente.

Según lo resumió Zapata, **"la economía de la Coca no les generaría tanta rentabilidad como proteger a las multinacionales para que las comunidades no se opongan a los proyectos; o esperar que estas multinacionacionales lleguen para cobrarles vacunas y demás"**. Esta tesis explicaría los dos desplazamientos masivos que se han producido en la región, así como los asesinatos con grado de sevicia como el de Osuna o González, buscando que la gente tenga miedo "y se vaya de los territorios".

### **¿Y la Fuerza de Tarea que opera en el Sur de Córdoba?** 

Según lo han denunciado diferentes organizaciones de la región, allí hacen presencia las autodenominadas Autodefensas Gaitanistas de Colombia (AGC), llamadas por el Gobierno Clan del Golfo, el Bloque Virgilio Peralta Arenas o Los Caparrapos, y el Nuevo Frente 18. Organizaciones que, según Zapata, son fácilmente ubicadas en el territorio porque sus campamentos suelen dejar desorden, y no pasan desapercibidos ante los ojos de los campesinos.

A pesar de ello, la Fuerza Conjunta de Tarea Aquiles que también opera en el territorio con un pie de fuerza de 4 mil hombres parece que no los ve; porque "cada dos meses dice que hubo cuatro capturas, osea dos capturas por mes", y agregó Zapata, "los combates que se registran son esporádicos, y **el Ejército dice que son enfrentamientos, pero nosotros creemos que son tiros al aire para ahuyentarse porque no hay bajas ni heridos"**. (Le puede interesar: ["Cinco grupos armados se disputan control en Córdoba"](https://archivo.contagioradio.com/grupos-armados-disputan-cordoba/))

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38153215" frameborder="0" allowfullscreen scrolling="no" height="200" style="border:1px solid #EEE; box-sizing:border-box; width:100%;" src="https://co.ivoox.com/es/player_ej_38153215_4_1.html?c1=ff6600"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
