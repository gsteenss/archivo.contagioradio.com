Title: Así fue “el entrampamiento al Acuerdo de Paz” del exfiscal Néstor Humberto Martínez
Date: 2020-11-27 11:19
Author: AdminContagio
Category: Nacional
Tags: Caso Santrich, Entrampamiento, Gustavo Petro, Jesús Santrich, Néstor Humberto Martínez
Slug: asi-habria-sido-el-entrampamiento-al-acuerdo-de-paz-del-exfiscal-nestor-humberto-martinez
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/11/Petro-denuncia-entrampamiento-a-la-paz-del-exfiscal-nestor-humberto-martinez.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

Este jueves se llevó a cabo en la Comisión Primera del Senado **un debate de control político en contra del exfiscal Néstor Humberto Martínez por su actuación en el caso Santrich, con la que buscó, según palabras de los senadores citantes “*destruir el Acuerdo de Paz*” firmado entre el Estado colombiano y la extinta guerrilla de las FARC-EP.** (Le puede interesar: [Citan a debate de control político a altos funcionarios por caso Santrich](https://archivo.contagioradio.com/citan-a-debate-de-control-politico-a-altos-funcionarios-por-caso-santrich/) )

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Los citantes al debate fueron los senadores [Gustavo Petro](https://twitter.com/petrogustavo), Iván Cepeda, Antonio Sanguino y Roy Barreras quienes expusieron las irregularidades que según ellos había tenido la actuación del exfiscal Néstor Humberto Martínez en el procesamiento de Jesús Santrich y aseguraron que **el entonces fiscal sabía que la operación de narcotráfico en su contra fue un montaje entre la DEA y la propia Fiscalía.** (Lea también: [DEA y Fiscalía habrían consumado un concierto para delinquir y afectar el proceso de paz: E Santiago](https://archivo.contagioradio.com/entrampamiento-mas-que-una-persecucion-a-santrich-un-ataque-al-acuerdo-de-pz/))

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

El senador Petro encabezó el debate y señaló que premeditadamente la DEA y la Fiscalía contactaron a Marlon Marín (sobrino de Iván Márquez) con unos supuestos narcotraficantes del cartel de Sinaloa en México, los cuales resultaron ser agentes encubiertos del cuerpo de inteligencia estadounidense.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Petro señaló que la Fiscalía presentó a Marlon Marín como un miembro de las FARC, cuando no lo era; y que **siempre buscó en compañía de la DEA, que Marín estableciera comunicación entre Iván Márquez y Jesús Santrich con los supuestos narcos -que en realidad era agentes de la DEA- para así poderlos vincular con un supuesto envío de cocaína que se estaba coordinando entre Marín y los mexicanos.**

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Pese a los intentos, el senador Petro, aseguró que nunca pudieron establecer el contacto con Iván Márquez y por eso Marlon Marín usó un suplantador para que se hiciera pasar por este. Suplantador que de hecho habló con los agentes que se hacían pasar por narcos.

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Marlon Marín venía siendo presionado por la DEA para cazar a Iván Márquez y entonces se le ocurre poner un falso Iván Márquez.”
>
> <cite>Senador Gustavo Petro en debate de control político</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

El senador afirmó que cuando los agentes se percataron que quien pasó al teléfono no era en realidad Iván Márquez sino un suplantador, llamaron la atención de Marlon Marín “*porque sabían que no iban a poder sembrar la prueba*”.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ante el fracaso de establecer el nexo telefónico, el senador aseguró que**, entre la Fiscalía y la DEA buscaron coordinar una entrega controlada de cocaína que pertenecía a la propia Fiscalía para así inculpar a Jesús Santrich.** Para probar esto, el senador expuso documentos oficiales de la Fiscalía en los que se autorizaba la entrega firmados por la Fiscal Especializada Bertha Cecilia Neira Díaz.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/charry_manager/status/1332000391937093636","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/charry\_manager/status/1332000391937093636

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AndresCamiloHR/status/1332007168657256450","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AndresCamiloHR/status/1332007168657256450

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

Sin embargo, las denuncias del senador no pararon ahí, sino que además adjunto un dictamen pericial que se hizo al video que se difundió, en el que supuestamente Santrich arreglaba la entrega de la cocaína con los mexicanos y agregó que dicho video había sido manipulado, cortado y algunos de sus audios sobrepuestos, más precisamente, aquellos en los que el agente de la DEA que se hacía pasar por narco, pronunciaba las frases incriminatorias con las que luego se judicializó a Santrich y Estados Unidos solicitó su extradición.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Petro señaló que el exfiscal Martínez difundió deliberadamente este video ante los medios para ejercer presión y acabar con el Acuerdo de Paz. (Lea también: [Piden a ONU frenar la “embestida” del Gobierno contra el Acuerdo de Paz](https://archivo.contagioradio.com/piden-a-la-onu-parar-la-embestida-del-gobierno-y-su-partido-contra-el-acuerdo-de-paz/))

<!-- /wp:paragraph -->

<!-- wp:quote -->

> “Todo fue un libreto para sembrar pruebas”.
>
> <cite>Senador Gustavo Petro en debate de control político</cite>

<!-- /wp:quote -->

<!-- wp:paragraph -->

Ante esto y los otros hechos denunciados, el senador Iván Cepeda anunció, que junto a Antonio Sanguino, interpondría una denuncia contra el exfiscal ante la Comisión de Investigación y Acusación de la Cámara, para que sea investigado por prevaricato por acción y por omisión, fraude a resolución judicial, ocultamiento, alteración o destrucción de elemento material probatorio y traición a la patria, “*por cuanto habría realizado actos tendientes a afectar la naturaleza de Estado soberano de nuestro país*”.

<!-- /wp:paragraph -->

<!-- wp:core-embed/twitter {"url":"https://twitter.com/AntonioSanguino/status/1332021292636254208","type":"rich","providerNameSlug":"twitter","className":""} -->

<figure class="wp-block-embed-twitter wp-block-embed is-type-rich is-provider-twitter">
<div class="wp-block-embed__wrapper">

https://twitter.com/AntonioSanguino/status/1332021292636254208

</div>

</figure>
<!-- /wp:core-embed/twitter -->

<!-- wp:paragraph -->

<!-- /wp:paragraph -->
