Title: Gobierno colombiano nunca ha atendido la frontera según habitantes de Cúcuta
Date: 2015-08-25 14:42
Category: Nacional, Política
Tags: colombia, Cucuta, Deportados, Fronteta, gobierno colombiano, Nicolas Maduro, Santos, Venezuela
Slug: gobierno-colombiano-nunca-ha-atendido-la-frontera-segun-habitantes-de-cucuta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/08/venezuela.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: lapatilla.com] 

<iframe src="http://www.ivoox.com/player_ek_7598256_2_1.html?data=mJqmmpeZeo6ZmKiak5eJd6KplJKSmaiRdo6ZmKiakpKJe6ShkZKSmaiRi9DWysrf0NSPp9Dg0NLPy8bSs4zi1tPQw5DMpYzV1crbxs7Is4zgwpDT1NTSuMbmwpDgx8yJh5SZo6aah5yncaTjz9nOyc7Tb7PVxc7cjZKJe6ShlQ%3D%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

##### [Rafael Mora, Periodísta] 

###### [25 Ago 2015] 

Colombia y Venezuela han sido naciones con relaciones de hermandad "*hay raíces, hay gente que estudia en San Cristobal, venezolanos que estudian en Cúcuta*" indica Rafael Mora, periodista de la ciudad de Cúcuta, por ello es necesario que, de manera urgente, se restablezcan las condiciones para que se reabra la frontera y se solucionen los problemas de contrabando y paramlitarismo que afecta al vecino país.

Sin embargo, estas relaciones empezaron a verse afectadas cuando Álvaro Uribe subió a la presidencia. Mora **recordó los sucesos del año 2007 cuando el entonces Presidente implementó un peaje en la vía que comunica a los puentes internacionales de Simón Bolívar y Francisco de Paula Santander, que unen a Cúcuta con las localidades de San Antonio y Ureña**, situación que derivó en empeorar las relaciones que se manejaban entonces con el mandatario Hugo Chávez.

El Senador Álvaro Uribe Vélez,  quien hizo presencia el lunes en la frontera con Venezuela para referirse a la crisis, sin embargo qué tanto aporta para solucionar este conflicto es uno de los cuestionamientos claves que hace Rafael Mora.

Rafael Mora también mencionó el descuido permanente del Estado, de las autoridades locales y departamentales  en las zonas fronterizas y alertó que esto ha permitido la presencia de contrabando y paramilitares “**aquí en la zona de frontera impera el paramilitarismo y las autoridades colombianas no hacen presencia en la zona de frontera**” dijo Mora.

Sobre la visita del senador del Centro Democrático, Rafael Mora indicó que “el viene a echarle más leña a la hoguera” y que la participación del Senador hace más tensa la relación con el país vecino “Lo que necesitamos los cucuteños es gente que venga a ayudar a solucionar el problema, **que hayan buenas relaciones y que haya una convivencia entre los dos pueblos hermanos**”.
