Title: El 2015 inicia con amenazas a defensores de Derechos Humanos
Date: 2015-01-19 17:24
Author: ContagioRadio
Category: DDHH, Movilización, Nacional, Paz
Slug: el-2015-inicia-con-amenazas-a-defensores-de-derechos-humanos
Status: published

###### **Foto: Sebastián Jaramillo** 

La mañana del martes 13 de enero, a la casa de la defensora de Derechos Humanos y lideresa del Movimiento Político y Social Marcha Patriótica, Piedad Córdoba, llegó una corona fúnebre, que simbolizaría una amenaza de muerte.

Según varios medios de información la amenaza proviene de las Águilas Negras, grupo paramilitar que se ha asociado en otras amenazas con sectores institucionales contrarios al proceso de conversaciones de paz.

Esta acción se presenta en el marco de nuevas amenazas por parte de esa organización paramilitar, a activistas y defensores de Derechos Humanos en todo el país, según lo denunció en su cuenta en Twitter la Representante a la Cámara, Angela María Robledo, el pasado domingo 11 de enero.

[![Captura de pantalla 2015-01-13 a la(s) 11.12.52](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-11.12.52-221x300.png){.alignnone .size-medium .wp-image-2894 width="221" height="300"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-11.12.52.png)

Según cifras del programa Somos Defensores, durante el 2014, se presentaron cerca de 200 amenazas en contra de defensores y defensoras de Derechos Humanos, 74% de ellas, provenientes de grupos paramilitares como las Aguilas Negras, Los Rastrojos, el ERPAC, el Ejercito Antirestitución, y Clan Usuga.

Para el programa Somos Defensores, existe una relación entre el incremento de las amenazas y la posible firma de acuerdos de paz entre el gobierno colombiano y la guerrilla de las FARC, ligada a la intensión de algunos sectores del Estado y de la sociedad por truncar las conversaciones de paz.

Las personas y organizaciones sociales que apoyan la solución política al conflicto y los diálogos de paz, y que construyen iniciativas de paz en el país han sido el principal foco de amenazas.

Líderes de organizaciones sociales y partidos políticos se solidarizaron con la defensora de Derechos Humanos, por su parte el Ministro del Interior, Juan Fernando Cristo, aseguró que se brindarán las medidas necesarias para garantizar la seguridad de Piedad Córdoba.

[![Twt 5](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-10.49.05-300x104.png){.alignnone .size-medium .wp-image-2900 width="300" height="104"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-10.49.05.png) [![Twt 3](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-10.45.30-300x71.png){.alignnone .size-medium .wp-image-2898 width="300" height="71"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-10.45.30.png) [![Twt 4](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-10.48.44-300x105.png){.alignnone .size-medium .wp-image-2899 width="300" height="105"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-10.48.44.png) [![Twt 1](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-10.44.48-300x81.png){.alignnone .size-medium .wp-image-2897 width="300" height="81"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-10.44.48.png) [![Twt 2](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-10.44.13-300x69.png){.alignnone .size-medium .wp-image-2896 width="300" height="69"}](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/01/Captura-de-pantalla-2015-01-13-a-las-10.44.13.png)
