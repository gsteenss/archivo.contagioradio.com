Title: ¡ Venga esa mano por la paz!
Date: 2017-02-15 16:13
Category: Nacional, Paz
Tags: implementación acuerdos de paz, voces de paz
Slug: venga-esa-mano-por-la-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/Caminata-por-la-paz-60.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [15 Feb 2017]

Organizaciones sociales y populares lanzaron la campaña ¡ Venga esa mano por la paz! que pretende** denunciar  los incumplimientos en los acuerdos de paz, las condiciones precarias en las zonas veredales transitorias de normalización, el asesinato a líderes sociales y la crisis en materia d**e derechos humanos por cuenta de la reorganización del paramilitarismo.

Para Imelda Daza integrante de Voces de Paz, esta campaña significa un paso hacia la democracia y  paz **“esta iniciativa significa convertirnos todos en voceros de paz**, en darnos la mano en señal de que compartimos la ilusión de construir en Colombia una nueva sociedad donde prime la justicia social”

Esta campaña también pretende suscitar en la sociedad un **sentimiento de respaldo hacia la implementación de paz,** debido a los diferentes incumplimientos que se han dado por parte del gobiernol, como los intentos por modificar el acuerdo de paz, y de esta manera **generar presión y exigir que se cumplan los acuerdos, sin atentar en contra de la esencia del mismo. **Le puede interesar:["Paramilitares arremeten en diferentes regiones del país"](https://archivo.contagioradio.com/paramilitares-arremeten-contra-diferentes-regiones-del-pais/)

En este sentido Imelda Daza señaló que es importante que la ciudadanía empiece a hablarle a los medios sobre la **necesidad de acelerar la presentación de los proyectos de ley para implementar los acuerdos. **Le puede interesar: ["Crecen riesgos para los acuerdos de paz"](https://archivo.contagioradio.com/crece-el-riesgo-para-los-acuerdos-de-paz/)

En los próximos días se realizará una jornada en solidaridad con los guerrilleros que se encuentran en las diferentes zonas veredales. De igual forma organizaciones como ANZORC se encuentran adelantando la recolección de mercados con **alimentos no perecederos y elementos de aseo**, para garantizar un mínimo de condiciones para que guerrilleros y guerrilleras puedan iniciar su tránsito a la  sociedad civil. Le puede interesar: ["Inicia campaña de donaciones para guerrilleros de las zonas veredales"](https://archivo.contagioradio.com/inicia-campana-de-donaciones-para-guerrilleros-de-las-zonas-veredales/)

<iframe id="audio_17041028" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_17041028_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]{.s1}](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio]{.s1}](http://bit.ly/1ICYhVU)[.]{.s1}
