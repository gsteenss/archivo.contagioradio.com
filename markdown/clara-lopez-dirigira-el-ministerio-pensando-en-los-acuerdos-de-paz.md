Title: Clara López dirigirá el ministerio pensando en los acuerdos de paz
Date: 2016-04-26 18:03
Category: Nacional, Política
Tags: alirio uribe, Clara López, Polo Democrático Alternativo
Slug: clara-lopez-dirigira-el-ministerio-pensando-en-los-acuerdos-de-paz
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/clara-lopez-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: lasillavacia] 

###### [26 Abril 2016] 

En medio de una fuerte polémica el presidente Santos dio a conocer el nuevo gabinete que lo acompañará en su gobierno de transición, uno de estos cambios es Clara López, ex **presidenta del partido político POLO** Democrático, quién asumirá el Ministerio de Trabajo.

López renunció a su puesto como [presidenta del POLO](https://archivo.contagioradio.com/?s=polo+democratico), debido a que uno de los lineamientos que tomo la plataforma política en su último congreso nacional, fue ser oposición al gobierno del presidente Santos  y sus políticas, hecho que genero al interior del partido posiciones diversas.

De acuerdo con Clara López, los problemas que asumirá como Ministra de Trabajo, harán parte de una serie de apuestas de cara a una nueva etapa en la que los[acuerdos con las guerrillas de las FARC y el ELN](https://archivo.contagioradio.com/arranca-el-segundo-encuentro-de-sindicalismo-por-la-paz/) abrirán un abanico de posibilidades para el fortalecimiento de la democrácia. En ese sentido tendrá que trabajar, entre otros, **por un aumento de [salario digno para los trabajadores en Colombia](https://archivo.contagioradio.com/aumento-del-salario-minimo-es-inconstitucional/).**

En ese sentido asume retos como un aumento justo del salario para los trabajadores colombianos y las demandas en cuanto a justicia, verdad y reparación de las miles de víctimas del [movimiento sindical](https://archivo.contagioradio.com/salario-minimo-aumenta-el-7-en-colombia/) en decenios de conflicto y en medio de la persistencia del paramilitarismo.

En cuanto al propio partido López asegura que, **contrario a lo que afirma Jorge Robledo, tanto el propio ministerio, como las bases del partido y en general la izquierda colombiana saldrán fortalecidos** porque estamos ante una coyuntura en la que es necesario asumir con altura los cambios en las maneras de hacer política.

Por su parte, el representante a la Cámara Alirio Uribe, aseguró que el partido no se va a dividir y qque sigue firme en las dos líneas definidas en el último congreso del partido en la que aseguran que **seguirán haciendo oposición al gobierno pero respaldarán la construcción de la paz.**

<iframe src="http://co.ivoox.com/es/player_ej_11315174_2_1.html?data=kpagk5qVe5Whhpywj5aYaZS1lpqah5yncZOhhpywj5WRaZi3jpWah5yncaTgwtfOjbGJh5SZo5jdx9%2BPk8PmxsySpZiJhpTijoqkpZKns8%2FowszW0ZC2pcXd0JCah5yncZU%3D&amp;" width="100%" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>
