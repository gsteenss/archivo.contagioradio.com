Title: CIDH realiza audiencia sobre 12 desapariciones en La Esperanza en 1996
Date: 2016-06-20 11:44
Category: DDHH, Nacional
Tags: CIDH, desapariciones forzadas Colombia, nexos paramilitares y Ejército
Slug: cidh-realiza-audiencia-sobre-12-desapariciones-en-la-esperanza-en-1996
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/06/Audiencia-CIDH.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: CIDH ] 

###### [20 Junio 2016 ]

Este martes y miércoles la 'Comisión Interamericana de Derechos Humanos' CIDH, realiza una audiencia sobre la **desaparición forzada de doce campesinos en la vereda La Esperanza**, Antioquia, por parte de las Autodefensas del Magdalena Medio, al mando de Ramón María Isaza y con apoyo del ejército Nacional, entre junio y diciembre de 1996.

En 1996 las Autodefensas del Magdalena Medio, lideradas por Ramón María Isaza, junto al Ejército Nacional, atacaron de forma continúa durante seis meses a los habitantes de la vereda La Esperanza del municipio del Carmen de Viboral, Antioquia, durante estos meses **desaparecieron, torturaron y ejecutaron extrajudicialmente a miembros de la comunidad**, a quienes señalaron como presuntos colaboradores del Ejército Popular de Liberación.

En algunas de sus declaraciones Ramón Isaza asegura que parte de las masacres fueron ordenadas por el general de la IV Brigada del Ejército, Alfonso Monosalva Flores, entre ellas el asesinato de Helí Gómez Osorio, el personero del municipio que había denunciado los **nexos entre grupos paramilitares y el Ejército**.

Durante estos 20 años familiares y amigos de los desaparecidos han luchado sin cansancio por encontrar a sus seres queridos y aseguran que la audiencia que celebrará la CIDH, justo cuando se conmemora el vigésimo aniversario de los hechos, alimenta la esperanza de poder encontrarlos pues **con la revisión del caso iniciará la búsqueda de las víctimas**. En las oficinas de la Corporación Jurídica Libertad, de Medellín, los familiares realizarán este martes una rueda de prensa.

###### [Reciba toda la información de Contagio Radio en [[su correo](http://bit.ly/1nvAO4u)] o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [[Contagio Radio](http://bit.ly/1ICYhVU)]] 
