Title: Mayo:  Un mes de escándalos para el gobierno y las FFMM
Date: 2020-12-30 13:05
Author: CtgAdm
Category: Actualidad, DDHH, Especiales Contagio Radio, Resumen del año
Tags: Corrupción en Colombia, Derechos Humanos, Gobierno Duque
Slug: mayo-un-mes-de-escandalos-para-el-gobierno-y-las-ffmm
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/12/disenos-para-notas.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

<!-- wp:paragraph -->

En mayo, luego del escándalo por el **hallazgo del laboratorio de cocaína en la finca de propiedad del embajador Sanclemente**, que parece haber terminado en la renuncia del diplomático del gobierno Duque pues no se conocen más avances en materia de investigación o sanción, **se conoció un primer escándalo en las FFMM que tuvo que ver con la operación Bastón**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Uno de los hallazgos paralelos a esta operación, entre múltiples actividades ilegales relacionadas con diversos actos delictivos entre corrupción en contrataciones, violaciones a los DDHH y otros, fue el **hallazgo de una pista de aterrizaje, al parecer, al servicio del narcotráfico en territorio colectivo de comunidades negras del Cacarica** a pocos kilómetros de una base militar binacional.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

También en paralelo **se conoció un nuevo escándalo de espionaje ilegal desde las FFMM contra periodistas, políticos, y líderes de movimientos sociales entre otros**. Tanto la fiscalía como el gobierno anunciaron investigaciones prontas, sin embargo lo que se ha conocido del avance de dichas investigaciones es prácticamente nulo.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Adicionalmente, **la embajada de EEUU anunció la llegada de una brigada militar de ese país a Colombia**. Este hecho tuvo dos consecuencias en los análisis. Por una parte el hecho de la embajada de ese país hiciera un anuncio que le corresponde al gobierno colombiano y por otra parte que la autorización de dicha operación no pasara por el congreso de la república, lo que se caracterizaba, según varios sectores sociales, en una **clara violación de la soberanía muy al estilo del gobierno Trump y sin ningún tipo de acción por parte del gobierno Duque**.

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### En mayo: A pocos kilómetros de base militar binacional funcionaría una pista del narcotráfico en Chocó

<!-- /wp:heading -->

<!-- wp:paragraph -->

Según información obtenida tras el análisis de la Operación Bastón, que investiga diversos casos de corrupción y vinculación con la criminalidad por parte de integrantes activos de las FFMM, se ha establecido que **en medio de la selva del Darien existiría una pista clandestina desde la que despegan aeronaves del narcotráfico**, presuntamente con destino a Estados Unidos.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Según la [Comisión Intereclesial de Justicia y Paz](https://www.justiciaypazcolombia.com/a-pocos-kilometro-de-base-militar-binacional-militares-trafican-cocaina/), el lugar que estaría siendo usado para fines del nacrotráfico estaría en zonas controlados por las operaciones de la Fuerza Pública, y coincidiría con los lugares «que desde 1997 han sido epicentro de bombardeos y de tomas de los paramilitares de als Autodefensas Unidas de Colombia (AUC), en los territorios colectivos del Cacarica y el Salaquí».

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: ([A pocos kilómetros de base militar binacional funcionaría una pista del narcotráfico en Chocó](https://archivo.contagioradio.com/a-pocos-kilometros-de-base-militar-binacional-funcionaria-una-pista-del-narcotrafico-en-choco/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Espionaje de Ejército colombiano no puede seguir impune: Organizaciones de EE.UU.

<!-- /wp:heading -->

<!-- wp:paragraph -->

Quince organizaciones defensoras de DD.HH., de la sociedad civil y académicas enviaron una carta al embajador de Estados Unidos en Colombia y al Departamento de Estado en la que **expresaban su preocupación respecto al reciente caso descubierto de espionaje y perfilamiento a defensores de DD.HH., abogados, periodistas y lideres sociales en Colombia**.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Las organizaciones recuerdan hechos como el asesinato de Alvaro Gómez Hurtado, el escándalo de las chuzadas del Departamento Administrativo de Seguridad (DAS) , la operación Andrómeda y la creación de la lista de ‘oposición’ en Twitter que demuestran la forma en que algunas instituciones han hecho uso de la inteligencia militar, contra quienes considera que son sus ‘enemigos’.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: ([Espionaje de Ejército colombiano no puede seguir impune: Organizaciones de EE.UU.](https://archivo.contagioradio.com/espionaje-de-ejercito-colombiano-no-puede-seguir-impune-organizaciones-de-ee-uu/))

<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->

### Operaciones militares de EEUU en Colombia deben ser autorizadas por el Congreso

<!-- /wp:heading -->

<!-- wp:paragraph -->

A raíz del anuncio hecho por la embajada de Estados Unidos sobre la llegada de una brigada de Asistencia de Fuerza de Seguridad a Colombia, desde sectores alternativos del Congreso de la República han pedido al presidente del Senado, Lidio García que convoque a una reunión urgente en la corporación para debatir sobre su llegada y su propósito en el país.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Por medio de su cuenta en Twitter, **la senadora de la Unión Patriótica, Aida Avella cuestionó las intenciones de la estadía de integrantes del Ejército de los Estados Unidos**, «señor Ministro de Defensa, Ud, le puede aclarar al país, si los soldados Estadounidenses, vienen desarmados? Según sus declaraciones vienen a «‘asesorar» A quién engañan», declaró.

<!-- /wp:paragraph -->

<!-- wp:paragraph -->

Ver más: [(Operaciones militares de EEUU en Colombia deben ser autorizadas por el Congreso - Contagio Radio)](https://archivo.contagioradio.com/operaciones-militares-de-eeuu-en-colombia-deben-ser-autorizadas-por-el-congreso/)

<!-- /wp:paragraph -->
