Title: Globale 2016, Miradas críticas y emancipadoras
Date: 2016-09-18 11:42
Author: AdminContagio
Category: 24 Cuadros, Uncategorized
Tags: Cine y resistencia, Festival documental, Globale Bogota
Slug: globale-2016-miradas-criticas-y-emancipadoras
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Globale.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Globale Bogotá 

###### [15 Sep 2016] 

El próximo lunes inicia en Bogotá la sexta edición del **Festival Internacional de Vídeo Globale 2016**, un espacio de exhibición para las miradas críticas y emancipadoras que, desde la **producción audiovisual documental**, aportan a la construcción de la memoria del presente y del pasado, analizando sus causas y efectos desde una perspectiva sistémica y global particularizando sus expresiones locales.

Del **19 al 23 de septiembre de 2016**, las producciones que componen la [programación de Globale](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/14089220_1068249033256217_5863613176435714019_n.jpg), abordarán temas que hacen parte de la coyuntura actual que vive el mundo como es la **memoria sociopolítica, las realidades y resistencias, guerras, fronteras y control territorial** en diferentes paises de América Latina y el mundo.

Algunos de los documentales cuentan las luchas de las comunidades contra la **instalación de megaproyectos en sus territorios**, lo que produce desplazamientos, fragmentación de los tejidos sociales comunitarios, aumento de las violencias y grandes daños ambientales.

Las crisis migratorias en Europa, el conflicto armado en Colombia, el aumento del racismo y la **xenofobia** en diferentes latitudes, el **militarismo**, los **feminicidios**, la **represión estatal y no estatal**, son algunas de las realidades comunes que abordan las producciones audiovisuales seleccionadas.

El Festival Internacional Globale, ayuda a visibilizar los diferentes colectivos, organizaciones y movimientos sociales se resisten cada día ante los efectos de estos fenómenos, construyendo mediante acciones transformadoras y creativas, alternativas de vida solidarias y colectivas que dan cuenta de que otro mundo es posible.

Los documentales nacionales y de paises como Brasil, Uruguay, España, Argentina y Perú, se proyectarán a las **5 de la tarde y 7 de la noche en la Cinemateca Distrital de Bogotá**.

<iframe src="https://www.youtube.com/embed/f8U2TU1NV1s" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

 
