Title: Campesinos del sur de Córdoba no permitirán fumigación con glifosato ni erradicación forzada
Date: 2019-07-24 08:06
Author: CtgAdm
Category: Comunidad, Nacional
Tags: Aspersión Aérea con Glifosato, cordoba, Erradicación Forzosa, Plan de Sustitución de Cultivos de Uso ilícito
Slug: campesinos-del-sur-de-cordoba-no-permitiran-fumigacion-con-glifosato-ni-erradicacion-forzada
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/07/Coccam.jpeg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:] 

[Campesinos del Sur de Córdoba han alertado sobre el posible regreso de la fumigación manual con glifosato, por lo que han anunciado que tomarán medidas para defender la sustitución voluntaria que han venido implementando desde la firma de los Acuerdos de Paz. De igual forma aseguraron que mientras haya erradicación forzosa o aspersión con glifosato de cualquier índole, las comunidades se opondrán a estos métodos.  
]

La denuncia se da a conocer días después que la Corte Constitucional ratificara e hiciera una serie de aclaraciones sobre la sentencia de 2017 que prohíbe la aspersión aérea con glifosato, herramienta que para ser usada debe demostrar su ausencia de daño en la salud de las personas y el ambiente. Dicha declaración fue interpretada por el Gobierno como una puerta abierta al regreso de la fumigación sobre los cultivos de uso ilícito.

**José David Ortega, integrante de la Comisión Política de la Asociación de Campesinos del Sur de Córdoba,** aseguró que las Alcaldías de los municipios ya están notificadas del regreso de la aspersión y la erradicación forzosa en el territorio, una directriz que consideran alarmante pues "el presidente está desconociendo la sentencia de la Corte Constitucional, las leyes del Estado son para acatarlas".

### ¿No hay recursos para sustitución pero si para glifosato?

Para el campesino, resulta incomprensible que el Gobierno anuncie que no tiene dinero para seguir con el plan de sustitución pero  si para implementar la política del glifosato, "quieren seguir promulgando la erradicación forzosa porque para ellos es la política de ataque contra la población civil" afirmó Ortega. [(Lea también: Pactos de sustitución deben cumplirse, glifosato no debe ser prevalente)](https://archivo.contagioradio.com/pactos-de-sustitucion-deben-cumplirse-glifosato-no-debe-ser-prevalente/)

Ante esta alarma, las comunidades han decidido que reestablecerán los comités anti- erradicación en las veredas donde pueda hacer presencia la Fuerza Pública, "ahora intentan decir que el sur de Córdoba está nadando en coca para aplicar la política de erradicación forzosa y glifosato" señaló Ortega.

**"El Gobierno dice que nosotros defendemos el narcotráfico, ni más faltaba, queremos sustitución voluntaria y está demostrado que los campesinos cumplieron la meta que se pusieron"** denunció el integrante de la Comisión de [Campesinos del Sur de Córdoba.]

Según la certificación de la ONU,  **5.000 familias campesinas de Monte Líbano, Puerto Libertador y San José de Uré erradicaron voluntariamente  1.000 hectáreas de hoja de coca**., sin embargo en la zona aún quedan cerca de 230 familias que piden ser acogidas por el Programa Nacional Integral de Sustitución de Cultivos de Uso Ilícito (PNIS) y que por fuerza mayor no pudieron inscribirse al plan.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>  
<iframe id="audio_38869373" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_38869373_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
