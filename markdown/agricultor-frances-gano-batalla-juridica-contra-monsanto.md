Title: Agricultor francés ganó batalla jurídica contra Monsanto
Date: 2015-09-10 14:14
Author: AdminContagio
Category: El mundo, Resistencias
Tags: campesinos víctimas de Monsanto, demandas contra Monsanto, francia, herbicidas de Monsanto, intoxicación por Monsato, Monsanto, Paul Francois
Slug: agricultor-frances-gano-batalla-juridica-contra-monsanto
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/09/monsanto1111.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: 20minutos.fr 

###### [10 Sep 2015] 

La compañía **Monsanto deberá indemnizar al agricultor Paul Francois**, quien ganó la demanda contra la multinacional, tras demostrarse que esta había sido la responsable de la intoxicación del campesino hace 10 años, cuando, accidentalmente inhaló el producto.

Pese a que durante una audiencia el pasado mes de mayo Monsanto había asegurado que su producto “no era peligroso” y que  “los  perjuicios invocados no existen”. El agricultor afirmó que la multinacional si conocía los efectos perjudiciales del herbicida para la salud, por lo que este jueves **el tribunal de apelaciones de Lyon en Francia apeló a favor del campesino.**

En el 2004, Paul Francois, inhaló accidentalmente el herbicida mientras que trabajaba  en una finca de  Charente al suroeste de Francia. Luego de ello el agricultor empezó a sufrir vértigos, tartamudeo y otros problemas neurológicos. Meses más tarde detectaron la presencia de clorobenceno en sus cabellos y orina, por esta razón, **Paul denunció a Monsanto porque no había información de los productos sobre el daño que causaban**, y la presencia de clorobenceno no aparecía en la etiqueta del producto.

Cabe recodar que este no es el primer caso que pierde la multinacional. En 2014, Monsanto perdió la pelea contra el chileno José Riquelme, quien demandó a la compañía por incumplir un contrato relacionado con la venta de semillas de melón, ya que de estas crecieron otras variedades de la especie, **por lo que se indemnizó al agricultor con el monto de 13 millones 500 mil pesos chilenos.**
