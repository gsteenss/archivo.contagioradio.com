Title: "La paz en tus ojos": una jornada de esperanza y reflexión
Date: 2019-09-16 17:48
Author: CtgAdm
Category: Paz
Tags: ejercito, ELN, ENCUENTRO ESPIRITUAL, FARC, paz
Slug: la-paz-en-tus-ojos-una-jornada-de-esperanza-y-reflexion
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/PAZ-REFLEX-2.png" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/09/PAZ-REFLEX-2.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:Archivo Particular] 

En San Rafael, Antioquia, **un lugar azotado durante años por la violencia en Colombia; y en el que hoy se desarrollan actividades de sostenibilidad y ecoturismo, se dio una** jornada de reflexión y diálogo que giró alrededor del respeto y la paz; donde  empresarios, ex integrantes de la Fuerza Pública, fundaciones, excombatientes, artistas,  fotógrafos; líderes sociales y ancestrales; representantes de víctimas, integrantes de la Comisión de la Verdad y otros miembros de la sociedad civil, **se unieron en un trabajo por sanar heridas profundas y comunes causadas por el conflicto armado en Colombia**.

Ubaldo Zuñiga, director de Economías sociales del común (ECOMUN) y excombatiente de las FARC, afirmó que en este evento **lo primordial fue la posibilidad de interlocutar y de ser tolerantes frente a las opiniones de los demás**, “yo creo que eso fue muy importante, que muy a pesar de que teníamos opiniones encontradas podíamos discutirlas y hablarlas, en los espacios que teníamos”. (Le puede interesar: [Cinco propuestas de mujeres que le apuestan a la paz](https://archivo.contagioradio.com/cinco-propuestas-de-mujeres-que-le-apuestan-a-la-paz/))

Esta jornada de reflexión fue liderada por la guía espiritual  Preethaji, quien viajó desde la India especialmente para este evento,  el cual  según los participantes, **es una apuesta de conexión profunda por la paz y el respeto a la vida**, “fue un espacio muy rico desde el punto de vista de relacionamiento con los otros sectores que en el pasado éramos enemigos, y ahora estamos en el mismo espacio pensando la paz”, afirmó Zuñiga.

> *“Hoy, cuando unos pocos declaran el rompimiento del compromiso con la paz regresando a la ilegalidad y otros pierden la esperanza, queremos compartir con Colombia esta experiencia que fue como, por un par de días, ver el país posible, sentir la paz como una posibilidad cierta y cercana”.*

Este espacio, como lo manifestaron en un comunicado grupal los participantes, es “de la paz por la paz”; **donde se lograron no solo avances de manera espiritual, también, algo que no se tenía planeado, como los lazos que construyeron** **las empresa**s y las organizaciones públicas por la construcción de paz, “eso es un espacio necesario para avanzar y no seguir matándonos entre nosotros y encontrar salidas”. (Le puede interesar:[Defendamos la Paz pide al Papa seguimiento y observación para que avance la paz)](https://archivo.contagioradio.com/defendamos-paz-piden-papa-observacion/)

<iframe id="audio_41571690" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_41571690_4_1.html?c1=ff6600" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>  
**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
