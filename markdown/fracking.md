Title: A pesar de efectos nocivos el gobierno da vía libre al Fracking en Colombia
Date: 2017-08-24 13:43
Category: Ambiente, Voces de la Tierra
Tags: energía ronovable, fracking, medio ambiente, petroleo
Slug: fracking
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/11/Fracking.jpg" alt="foto" style="display:block"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/fracking-e1460586646301.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Fracking-San-Martin.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/fracking-e1472854044186.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/Fracking-Vaca-Muerta.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/fracking.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/12/fracking.png" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/fracking-e1487780191287.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/09/fracking-santander-e1504298360341.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/06/fracking-en-boyaca.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/08/FRACKING-COLOMBIA.jpg" alt="foto" style="display:none"></img>
<img class="" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2020/04/MG_1267-scaled.jpg" alt="foto" style="display:none"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto:  Gert Steenssens] 

###### [24 Ago 2017] 

Ante el anuncio del Ministerio de Ambiente, de darle vía libre a los yacimientos en el país en donde se podría realizar actividades de fracking, ambientalistas y organizaciones sociales calificaron la medida como perjudicial para el ambiente y los territorios, indicando que **detrás de esas actividades hay intereses económicos de compañías petroleras.**

Según el ambientalista y economista Álvaro Pardo, Colombia está viviendo un momento de pérdida de autosuficiencia en el campo energético y **no tiene ningún reemplazo asociado a la energía limpia y renovable,** argumentando que “en 5 años el país va a estar importando más crudo del que produce”. (Le puede interesar: ["Hay dos nuevos contratos para fracking en el departamento del Cesar"](https://archivo.contagioradio.com/nuevos_contratos_de_fracking_en_cesar/))

Igualmente, Pardo manifestó que debido a la poca inversión que hace el Gobierno Nacional en ciencia y tecnología, ha disminuido la posibilidad de **“aminorar el golpe de las reservas petroleras con energía limpia** en donde no se tenga que depender de la energía fósil”.

### Cultura impositiva del Gobierno 

Pardo indicó que el Gobierno Nacional ha venido teniendo un discurso “extorsivo” para realizar el fracking, **“en Colombia se impuso una cultura basada en la extorsión donde se le ha dicho a la sociedad que si no hay fracking el país se paraliza”** y que pese al rechazo de los colombianos a estas actividades, “desde hace varios años nos han impuesto cosas a las malas ”.

De igual forma,  señalo que **la sociedad está rechazando el fracking** al mismo tiempo que ven como “lo imponen de manera arbitraria porque el Gobierno se pellizcó cuando estamos al borde del abismo por no haber invertido en energía limpia”. (Le puede interesar: ["Gobierno prepara dos nuevos bloques para fracking"](https://archivo.contagioradio.com/gobierno_prepara_fracking_colombia/))

### No hay certeza científica de los impactos 

Pardo fue enfático en manifestar que ante la falta de argumentos científicos de los impactos del fracking, **“debe primar el principio de precaución** donde no se puede iniciar prácticas de las cuales no se tenga certeza de las consecuencias”.

Igualmente, frente a las declaraciones del Gobierno Nacional, que buscan desmentir las razones de los ambientalistas, Pardo dijo que **sus argumentos son los mismos de las naciones que se han opuesto al fracking**, “son países como Alemania en donde se han dado cuenta que esta práctica genera daños para el subsuelo, contamina las fuentes hídricas y profundiza las grietas de la tierra”.

El economista manifestó también que es necesario que el Gobierno Nacional **corrija los problemas de Ecopetrol**, quienes, según él, “están en la quiebra, endeudados y sin recursos para la exploración”. Además, insistió en que Colombia debería estar trabajando en hacer proyectos con uso de energías limpias como los proyectos de energía solar. (Le puede interesar: ["Fracking amenaza el abastecimiento de agua en Bogotá"](https://archivo.contagioradio.com/fracking-amenaza-abastecimiento-agua-bogota/))

Finalmente indicó que este viernes realizarán una reunión en Barrancabermeja para **darle a conocer a la ciudadanía los impactos de esta actividad**, “será una respuesta a las reuniones que han hecho las empresas petroleras en donde sólo invitan a las personas interesadas en el fracking y no escuchan las voces de los que estamos en contra”.

<iframe id="audio_20508224" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_20508224_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](http://bit.ly/1ICYhVU)
