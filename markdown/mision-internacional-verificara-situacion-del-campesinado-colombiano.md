Title: Misión internacional verificará situación del campesinado colombiano
Date: 2016-09-16 18:09
Category: DDHH
Tags: acuerdos de paz, campesinos, Cumbre Agraria, Gobierno, paz
Slug: mision-internacional-verificara-situacion-del-campesinado-colombiano
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/04/campesinos-boyaca.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: Contagio Radio 

###### [16 Sep 2016] 

Con el objetivo de apoyar el cumplimiento de los acuerdos de La Habana y específicamente sobre el punto uno de Reforma Agraria Integral, del **20 al 24 de septiembre se llevará a cabo la Misión Internacional de Solidaridad con Colombia,** organizada por La Vía Campesina Internacional y  la Coordinadora Latinoamérica de Organizaciones del Campo.

Se trata de una actividad en la cual se busca acompañar al campesinado en la exigencia del cumplimiento de los acuerdos de paz con las FARC y tras las movilizaciones sociales de años atrás con las que se exigía el cumplimiento de compromisos para garantizar la productividad,  la soberanía alimentaria,  el derecho a la tierra de las comunidades y el respeto a los derechos humanos.

La misión evaluará el estado actual de **tenencia de tierras, situación económica, injerencia e impactos de multinacionales, procesos de restitución y baldíos, entre otras problemáticas sobre los derechos a la tierra y al territorio** de campesinas y campesinos en el país, que también han sido abordadas en el punto uno del Acuerdo de Paz suscrito entre el gobierno colombiano y la guerrilla de las Farc-EP.

Justamente, uno de los temas que se tendrá en cuenta en el marco de la Misión, tiene que ver con la **[Ley Zidres](https://archivo.contagioradio.com/zidres-legalizaria-mecanismos-de-acumulacion-de-tierras/)impulsada por el gobierno nacional,** que asegura que se trata de una iniciativa que busca generar desarrollo para el campo colombiano. Sin embargo, de acuerdo con Nury Martínez, coordinadora de la Vía Campesina, esta Ley es contraria a los acuerdos de La Habana y los pactados con organizaciones sociales, pues aumenta la problemática del [acaparamiento de tierras.](https://archivo.contagioradio.com/campesinos-de-casanare-defienden-el-agua-y-dicen-no-a-ley-zidres/)

La Misión Internacional de la CLOC-Vía Campesina estará conformada por destacadas y destacados dirigentes campesinos de La Vía Campesina y de CLOC-Vía Campesina, provenientes de 16 países de cuatro continentes. Quienes participarán el  día 22 de septiembre las Audiencias Públicas **en Putumayo, Cauca, Meta, Magdalena y Norte de Santander.**

La Misión finalizará el 24 de septiembre con un evento central en el municipio de Apartadó, en la región bananera del Urabá antioqueño, con el acompañamiento de la totalidad de la Misión.

<iframe id="audio_12984991" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_12984991_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u) o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por[Contagio Radio](http://bit.ly/1ICYhVU) 
