Title: La memoria de las víctimas del Palacio de Justicia llega al teatro
Date: 2018-11-08 16:42
Author: AdminContagio
Category: Cultura, Nacional
Tags: centro de memoria, Palacio de Justicia, Paz y reconciliación, teatro
Slug: palacio-de-justicia-teatro
Status: published

###### [Foto: El Palacio Arde] 

###### [8 Nov 2018] 

Desde este jueves los bogotanos podrán asistir a **"El Palacio Arde"**, una obra de teatro en la que participan familiares de desaparecidos en la toma y retoma del Palacio de Justicia, e integrantes de la  Academia Superior de Artes de Bogotá (ASAB) de la Universidad Distrital Francisco José de Caldas, haciendo una **reconstrucción de memoria histórica sobre lo ocurrido hace 33 años en el centro de la Capital**.

El Palacio Arde, es una obra de creación colectiva y artística, en la que participan dos familiares de víctimas de desaparición forzada de los hechos del Palacio: **Inés Castiblanco,** hermana de Ana Rosa Castiblanco, quien se encontraba embaraza durante la toma y retoma; y **Pilar Navarrete**, esposa de Héctor Jaime Beltrán, quien trabaja en la cafetería en el momento de los hechos. De la obra también hacen parte un docente y dos estudiantes de la ASAB.

Esta puesta en escena que **reconstruye lo sucedido el 6 y 7 de noviembre de 1985**, fue ganadora de la beca “Bogotá Diversa: proyectos dirigidos a sectores sociales 2018”. Una muestra artística que Pilar Navarrete describe como “**un proyecto hermoso de hacer memoria, y contar un poco de la voz de las víctimas como es el conflicto, y que paso en el Palacio de Justicia**”; como una representación que busca motivar reflexiones sobre el dolor de la desaparición forzada, en uno de los casos que marcaron la historia del país.

Aunque Navarrete reconoció que **el arte es un vehículo ideal para hacer memoria**, y sostuvo que en el contexto colombiano se ha coartado este proceso, llevándolos a "tomar caminos diferentes para reconstruir la memoria; y así, insistir en el no olvido de los hechos, y en la búsqueda por la verdad. (Le puede interesar: ["Tejiendo, familiares conmemoran 33 años del P. de Justicia"](https://archivo.contagioradio.com/a-33-anos-del-palacio-de-justicia-familiares-invitan-a-tejer-por-la-memoria/))

Esta puesta en escena que Navarrete califica como “un tránsito difícil pero hermoso reconstruir las cosas vividas”, se presentará los días **8 y 9 de noviembre a las 6:30 pm**, en el **Centro de Memoria, Paz y Reconciliación** (Carrera 19B \# 24-86), con acceso libre hasta completar aforo. También se presentará **el sábado 10 de noviembre, en la sala Teatro de Garaje** (Carrera 10 \# 54ª-27) a las 7:30 de la noche, con un aporte de \$15.000.

<iframe id="audio_29936538" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_29936538_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
