Title: ¿Está mintiendo EPM sobre la casa de máquinas en Hidroituango?
Date: 2019-02-08 17:03
Author: AdminContagio
Category: Ambiente, Nacional
Tags: EPM, Hidroelectrica, Hidroituango, Río Cauca
Slug: epm-casa-maquinas
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/02/Captura-de-pantalla-2019-02-08-a-las-3.46.42-p.m.-770x400.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: @JulieDuque1] 

###### [8 Feb 2019] 

Pasados tres días del cierre de compuertas en la casa de maquinas,  varios medios de información publicaron una foto en la que se observa su interior y estructura casi intacta, sin embargo, habitantes cercanos a la presa señalan que la fotografía podría ser falsa, porque no parece registrar verdaderamente el paso de agua, lodo y escombros que tiene el Río Cauca antes de ingresar a los túneles de desviación. (Le puede interesar: ["Es alta la posibilidad de que falle el macizo rocoso en Hidroituango: Experto en Geología"](https://archivo.contagioradio.com/falle-macizo-hidroituango/))

La principal preocupación de los habitantes cercanos al Río Cauca sigue siendo su caudal, pues ampliamente se han divulgado los problemas que enfrentan especies que viven en el afluente o cerca a él. Y peses a que este viernes aguas comenzaron a fluir al río Cauca de nuevo cuando abrieron tres de las cuatro compuertas del vertimiento, las comunidades mantienen dudas de que el ecosistema del río regrese a la normalidad como lo ha indicado Empresas Públicas de Medellín (EPM). (Le puede interesar: ["El río Cauca en emergencia tras operación de Hidroituango"](https://archivo.contagioradio.com/el-rio-cauca-en-emergencia-ambiental-por-sequias/))

Adicionalmente, la Empresa sí se ha movido ampliamente a través de Medios insistiendo en que está trabajando por mejorar la situación, pero los únicos funcionarios que se han visto de EPM en realidad han estado sacando peces del Río que quedan retenidos en pozos con baldes, pero dejando muchas zonas sin cubrir. De hecho, ha sido la misma comunidad la encargada de salvar los pocos peces que siguen vivos en los pozos. (Le puede interesar: ["Hay riesgo de avalancha en municipios cercanos a Hidroituango"](https://archivo.contagioradio.com/informe-revela-riesgos-de-un-desastre-mayor-en-hidroituango/))

> FAKE NEWS desde [@hidroituango](https://twitter.com/hidroituango?ref_src=twsrc%5Etfw)  
> ¿Desde qué extremo se tomó la presunta fotografía, si el túnel de acceso a casa de máquinas está destruido por derrumbes internos (uno de ellos detectado en octubre/2018 en la abscisa 600) cómo hicieron para entrar un dron allá?[\#ConlaVidaNOseJuega](https://twitter.com/hashtag/ConlaVidaNOseJuega?src=hash&ref_src=twsrc%5Etfw) [pic.twitter.com/D8sLpQOgel](https://t.co/D8sLpQOgel)
>
> — ClaudiaJulietaDuque (@JulieDuque1) [February 8, 2019](https://twitter.com/JulieDuque1/status/1093919761069690880?ref_src=twsrc%5Etfw)

<p>
<script src="https://platform.twitter.com/widgets.js" async charset="utf-8"></script>
</p>
### **EPM mintió con el proyecto y sigue mintiendo: Zuleta** 

En diferentes entrevistas, integrantes de EPM han dicho que se está haciendo lo mejor para las comunidades Río abajo de la Represa, un hecho que es una franca mentira, pues como lo señala la vocera del Movimiento Ríos Vivos Antioquia,  Isabel Cristina Zuleta, el cierre de compuertas y posterior baja en el caudal del Cauca ha significado hambre para las comunidades, porque no tienen pescado para comer, ni tener agua para barequear y sacar oro.

Adicionalmente, los censos para los afectados por la represa todos los días cambian, y las personas que un día tienen derecho a subsidio para manutención al otro día no lo tienen, o no lo consignan a tiempo. Aunque EPM ha alistado camiones para abastecer de agua a los municipios cercanos al Cauca, el liquido no es suficiente, porque está claro que un carrotanque no reemplaza el Río.

A ello se suma una fotografía que se hizo pública, en la que se observa lo que sería el interior de la Casa de Maquinas, pero que para Zuleta, es otra de las mentiras de EPM, porque el agua en la foto se ve mucho más limpia de lo que debe estar: la contaminación antes del muro es tan delicada que "los peces huyen del embalse, y el agua allí se ve verde". (Le puede interesar: ["Lo que EPM no ha dicho sobre apertura del vertedero en Hidroituango"](https://archivo.contagioradio.com/apertura-vertedero-hidroituango/))

Contagio Radio hizo eco de la denuncia de la comunidad anteriormente, porque el material orgánico producto de la tala efectuada por EPM se estaba quedando represado antes del muro, y ese mismo material ingresó por los túneles de desviación así como los que conducen a la casa de máquinas; razón por la que resulta extraña la fotografía en la que se observa un agua libre de escombros y sin material en descomposición. (Le puede interesar: ["Hidroituango, una bomba de tiempo"](https://archivo.contagioradio.com/hidroituango-una-bomba-de-tiempo/))

<iframe src="https://www.youtube.com/embed/NiXpDhcO8xU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Otras abnormalidades las explicó el animador 3d Santiago Ardilla Reyes en un video de Youtube, donde indicó que la presunta fotografía es "absolutamente falsa" y que se trata de una imagen creada digitalmente, aunque "muy mal hecha." Ardilla señaló que la imagen carece de sombras, la iluminación de la fotografía es poco realista, el cuerpo de agua se muestra transparente y quieta en cambio de opaco y turbolente, además no incluye los mismos remaches que aparecen en otras fotografías del interior de la represa.

<iframe id="audio_32397762" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_32397762_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en ][[su correo][[Contagio Radio]
