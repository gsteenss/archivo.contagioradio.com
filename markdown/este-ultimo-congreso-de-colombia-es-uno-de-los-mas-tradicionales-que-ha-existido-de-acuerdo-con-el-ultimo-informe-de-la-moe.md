Title: Congreso de Colombia uno de los más tradicionalista de los últimos periodos legislativos
Date: 2017-12-21 15:11
Category: Nacional, Política
Tags: Congreso, MOE, Tradicionalista
Slug: este-ultimo-congreso-de-colombia-es-uno-de-los-mas-tradicionales-que-ha-existido-de-acuerdo-con-el-ultimo-informe-de-la-moe
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/01/CONGRESO-e1483381822139.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: La Rozón] 

###### [21 Dic 2017] 

El más reciente informe de la Misión de Observación Electoral demostró que el 75 % de los congresistas a los que se les aplicó una encuesta, piensan diferente en lo privado, a la forma en la que votan en los debates, situación que evidencia de acuerdo con la MOE, **que hay un Congreso mucho más tradicionalista que en periodos anteriores.**

### **¿Qué votaron los congresistas de acuerdo a cómo piensan?**

El informe de la MOE pudo establecer que uno de los temas en donde se pudo concretar una relación entre el pensamiento y el voto estuvo en los debates sobre la implementación de los Acuerdos de Paz, en donde de acuerdo con Fabián Hernández coordinador de comunicaciones de esta organización, **quienes se mostraron en contra de los mismos mantuvieron su postura en los diferentes debates y se mantenían en la misma posición en ante cualquier instancia**.

De igual forma aseguró que al no saber cómo quedará compuesto el Congreso para el año 2018, será muy difícil saber si estas posiciones cambien, “lo que uno está viendo ahorita es que hay una dispersión completa de ideas  y de ideologías, y no hay una manera de que podamos prever cuál será el congreso que podamos tener el próximo año. (Le puede interesar:["No fue correcto archivar acto legislativo de las curules de paz: Alfredo Beltrán"](https://archivo.contagioradio.com/tribunal_superior_circunscripciones_paz/))

### **Temas sociales y de Derechos humanos** 

Frente a los puntos sociales y derechos humanos la encuesta arrojo resultado diferente, por ejemplo frente a la percepción con el matrimonio civil entre parejas del mismo sexo el 51% de los hombres encuestados afirmó estar de acuerdo, mientras que el 36% de las mujeres dijeron estar de acuerdo.

En cuanto a la adopción por parte de parejas del mismo sexo el 79% de mujeres congresistas están en desacuerdo al igual que el 74% de los hombres encuestados, situación que es muy similar en cuanto al aborto y su despenalización, en el que el **73% de las mujeres está en desacuerdo y un 60% de los hombres tienen la misma posición**.

Posiciones que para Hernández demuestran que este es un Congreso bastante tradicionalista, que ha mantenido su agenda, y que se ha radicalizado mucho más, tras la salida de Uribe de la presidencia.

Frente a las políticas económicas los congresistas en su mayoría expresaron su desaprobación frente a las diferentes medidas que se han tramitado durante este periodo presidencial, **sin embargo son ellos los que las han aprobado.**

### **Los salarios de los congresistas** 

El sueldo de 20 millones que ganan los congresistas también se tuvo en cuenta en esta encuesta y los resultados fueron bastante particulares debido a que el 63% de los congresistas considera que esta cifra es justa, **un 5% considera que debería aumentarse y tan solo el 32% considera que el monto debería disminuirse.**

“Los grandes partidos, los más tradicionales como el Partido de la U, el Conservador, el Liberal y Cambio Radical son los que más aportan a ese 63% que consideran que el salario es justo, el 5% está conformado por congresistas de diversos partidos” afirmó Hernández.

### **El Congreso que se va** 

Para Hernández las conclusiones que deja este Congreso que finalizará en agosto del año entrante es primero que deja **una gran ambigüedad sobre los Acuerdos de paz, debido a que pese a que fue acordado**, las posiciones de los congresistas ni fueron claras ni contundentes durante la implementación.

De igual forma, Hernández afirmó que si existió una voluntad para hacer una reforma política y la paradoja se encuentra en el qué pasó, en donde cabe la hipótesis de que no hayan querido transformar los mecanismos para ser elegidos por la proximidad de las elecciones.

<iframe id="audio_22788357" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_22788357_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
