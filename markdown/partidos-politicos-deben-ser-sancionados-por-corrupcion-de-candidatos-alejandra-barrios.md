Title: "Partidos políticos deben ser sancionados por corrupción de candidatos" Alejandra Barrios
Date: 2018-06-22 15:51
Category: Nacional, Política
Tags: Cambio Radical, Centro Democrático, Compra de votos, partidos politicos
Slug: partidos-politicos-deben-ser-sancionados-por-corrupcion-de-candidatos-alejandra-barrios
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/05/pleno-del-congreso-e1495751460417.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [ Foto: Archivo Particular] 

###### [22 Jun 2018] 

En una rueda de prensa la directora de la Misión de Observación Electoral, Alejandra Barrios, manifestó que las últimas cartas reveladas por el Fiscal de la Nación, Néstor Martínez, sobre los actos de corrupción de políticos, deja muy claras las rutas para tomar medidas **ejemplarizantes no solo sobre los políticos que cometieron estos actos, sino también sobre los partidos que les dieron los avales.**

Para Barrios, esas declaraciones permiten conocer a diferentes niveles cómo funcionan las relaciones entre política, corrupción y procesos electorales, que, a su vez, posibilitan avanzar en la construcción de reformas políticas electorales y hacer un llamado muy fuerte a las instituciones de carácter político porque **“siguen privilegiando la obtención de los votos sobre candidaturas transparentes**”.

De igual forma aseguró que los actos de corrupción que han sido destapados por Martínez deben significar llamados desde la ciudadanía urgentes para votar a la consulta anticorrupción y de esta manera frenar el accionar de esas instituciones políticas. (Le puede interesar: ["Los 4 partidos políticos inmersos en escándalos de compra de votos"](https://archivo.contagioradio.com/los-4-partidos-politicos-inmersos-en-escandalo-de-compra-de-votos/))

### **Las implicaciones de las organizaciones políticas** 

De acuerdo con la directora de la MOE, en esos hechos de corrupción están implicadas muchas instituciones políticas, “tenemos al partido Conservador, Cambio Rádical, de donde se han presentado la mayoría de casos, el partido de la U se presentan candidatos y también del Centro Democrático”.

Asimismo, agregó que “independientemente del partido al que están perteneciendo, lo que estamos viendo es que hay unas redes de corrupción que le sirven a todos los que tengan recursos de esos partidos políticos y que necesiten llegar al poder, no para legislar a nombre de los ciudadanos, **sino para favorecer sus empresas o los negocios que tienen con el Estado”**.

Frente a la responsabilidad de los partidos políticos en los actos de corrupción de estas redes, Barrios expresó que **“los partidos políticos saben cómo funciona”** debido a que privilegian la obtención de los votos en vez de apostar sobre candidatos honestos y a que tampoco tienen mecanismos de selección de candidatos, razón por la cual deben responder por los avales.

Entre las medidas sancionatorias que se le deberían aplicar a los partidos, Barrios manifestó que una de ellas es que regresen los recursos de la reposición de votos, que se les impongan multas, que se aplique la silla vacía y prohibírseles presentar candidatos para la misma circunscripción en el periodo siguiente.

<iframe id="audio_26682967" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_26682967_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en su correo bit.ly/1nvAO4u o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por Contagio Radio 
