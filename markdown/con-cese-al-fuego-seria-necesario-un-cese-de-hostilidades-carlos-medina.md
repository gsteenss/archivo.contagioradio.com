Title: Con cese al fuego sería necesario un cese de hostilidades: Carlos Medina
Date: 2016-01-21 11:49
Category: Entrevistas, Paz
Tags: Conversaciones de paz de la habana, ELN, FARC, Juan Manuel Santos, Paramilitarismo
Slug: con-cese-al-fuego-seria-necesario-un-cese-de-hostilidades-carlos-medina
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/01/Carlos_medina_contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto: notibarrioadentro. 

<iframe src="http://www.ivoox.com/player_ek_10153387_2_1.html?data=kpWel5iXfJihhpywj5aaaZS1kZeah5yncZOhhpywj5WRaZi3jpWah5yncaTjz5DQx9jJb8LgjMvix8zTb9TZ04qwlYqliMKfz8rQx9jFtsrjjNrbjcjJt8afxcqYytTXuMrgysnOj4qbh4630NPhw8zNs4zGwsnW0ZCRaZi3jpk%3D&amp;" width="238" height="48" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Carlos Medina Gallego, U. Nacional] 

###### [[(]21 Ene 2016[)]]

Para Carlos Medina Gallego, integrante del Centro de Pensamiento de la Universidad Nacional, los anuncios sobre el fin del conflicto, punto 3 de la agenda de conversaciones, "**son muy positivos y nos hacen pensar que el proceso de paz con las FARC va a ser exitoso**, siempre esperando que en los próximos días se anuncie el inicio de la fase pública de conversaciones con el ELN".

Tras el anuncio de la participación de la **[ONU y la CELAC para hacer veeduría a la dejación de armas y el cese al fuego definitivo](https://archivo.contagioradio.com/?s=onu+)** surge la discusión en torno al llamado cese de hostilidades y cese del fuego. Según Medina lo uno es silenciar los fusiles, pero cesar las hostilidades es frenar el accionar del paramilitarismo y dar las garantías suficientes a los movimientos sociales y políticos de que no van a ser criminalizados y el Estado debe garantizar esos derechos.

Además, son **muy buenas también las características de temporalidad, porque ya, a mediados de Enero, se están definiendo las comisiones para verificar la dejación de armas** y la concentración en algunas zonas del país que las FARC han denominado las “Terrepaz”. Para Medina, estas zonas deberían tener en cuenta las características territoriales de la guerra y se requieren más certezas de seguridad para las tropas concentradas.

Aunque Pablo Catatumbo ha afirmado que el **paramilitarismo no estaría desmontado para la fecha de la firma del acuerdo**, si habría que tener en cuenta que el nuevo paramilitarismo es “societal” y que tiene capacidad de control y de desarrollo de negocios que tiene que enfrentarse de manera estructural.

###### Reciba toda la información de Contagio Radio en [su correo](http://bit.ly/1nvAO4u)  o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por [Contagio Radio](https://archivo.contagioradio.com/).
