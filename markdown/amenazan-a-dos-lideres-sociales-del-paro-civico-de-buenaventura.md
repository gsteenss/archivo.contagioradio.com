Title: Amenazan a dos líderes sociales del paro Cívico de Buenaventura
Date: 2018-02-10 19:37
Category: DDHH, Entrevistas
Tags: María Miyela Riascos, Paro Cívico de Buenaventura, Policía Nacional
Slug: amenazan-a-dos-lideres-sociales-del-paro-civico-de-buenaventura
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/02/revista-semana.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Revista Semana] 

###### [10 Feb 2018]

La líderesa y defensora de derechos humanos, María Miyela Riascos, vocera de la comunidad de Buenaventura durante el paro del año pasado, denunció la falta de **garantías de protección por parte de las autoridades ,el trato despectivo y la estigmatizació**n que le dieron, tras comunicarse con la Policía Nacional y comentarles que hombres sospechosos estarían vigilando su hogar. Así mismo la comunidad denunció un posible plan para asesinar al Padre Jhon Rina, también líder del paro.

Los hechos ocurrieron el pasado 10 de febrero hacia las 9 de la mañana, cuando Riascos se percató de que **dos hombres se encontraban vigilando su vivienda de forma sospechosa y tomaban apuntes en un cuaderno, como si estuviesen haciendo un croquis de la misma**. La líderesa llamó a la línea de la Policía Nacional 123, en donde fue atendida por un policía al que le solicitó la presencia del cuadrante.

Sin embargo, la respuesta del policía, de acuerdo con Riascos fue **"llame a sus amigos, que sean ellos quienes vayan a sacar esos sujetos de allá, ustedes son bastante familia"**. Pese a que la líderesa le pide al agente que se identifique, este no lo hace. De acuerdo a la denuncia, los hombres permanecieron largo tiempo en frente a la vivienda, sin que ningún miembro de la autoridad competente fuera a identificarlos. (Le puede interesar: ["Roban información relacionada con la labor del líder Temistocles Machado en Buenaventura"](https://archivo.contagioradio.com/roban-informacion-relacionada-con-labor-de-lider-temistocles-machado-en-buenaventura/))

De igual forma, la comunidad denunció un posible plan que se estaría organizando para asesinar al Padre Jhon Reina, sacerdote de la Diócesis de Buenaventura, director de la Pastoral Social, defensor de derechos humanos y miembro del Comité del Paro Cívico. Los habitantes manifestaron haber escuchado a un hombre afro asegurar que le habían pagado para "**hacerle la vuelta al Padre Reina"**.  (Le puede interesar: ["La lucha por la que habría sido asesinado Temistocles Machado"](https://archivo.contagioradio.com/temistocles_machado_lider_buenaventura_paro_asesinado/))

La comisión de veeduría de derechos humanos del Paro Cívico de Buenaventura manifestaron su **rechazo a este tipo de accionar por parte de la Policía que estigmatiza la labor de los líderes y defensores de derechos humanos en el territorio** y lanzaron una alerta a organizaciones internacionales, la Defensoría del Pueblo y la Consejería Presidencial para los derechos humanos para que acompañen este proceso.

###### Reciba toda la información de Contagio Radio en [[su correo]

###### 
