Title: Jóvenes le apuestan a la participación política y el voto consciente
Date: 2018-05-04 12:29
Category: Paz, Política
Tags: campañas políticas, jovenes, participación juvenil
Slug: jovenes-le-apuestan-a-la-participacion-politica-y-al-voto-consciente
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/05/DYB6iubXUAEYHE8-e1525454245664.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Jóvenes Construyendo Paz] 

###### [04 May 2018] 

Por medio de varias iniciativas ciudadanas, un grupo de jóvenes crearon las campañas **“Más Voz, Menos Ruido” y “No se lo dé a Cualquiera”** con el objetivo de empoderar a esta población con la democracia, y de esta forma participar de manera activa en la campaña política que decidirá el rumbo de Colombia para los próximos cuatro años.

De acuerdo con Manuela Ángel, vocera de las campañas que trabajan de la mano de la Fundación Mi Sangre, los jóvenes quisieron **alzar la voz en el juego democrático y político** del país. Afirmó que la juventud colombiana debe apropiarse y participar con propuestas en la vida política para que los gobernantes tengan en cuenta sus necesidades.

### **Los temas que le preocupan a los jóvenes** 

Con esto, crearon la agenda política “Más Voz, Menos Ruido”, en donde hicieron un sondeo con 500 jóvenes para concretar los temas más importantes para la comunidad juvenil. De allí, “salieron cuatros temas fundamentales que son el **empleo digno**, la educación, la transformación del sector rural y la información y las comunicaciones”.

Estos temas fueron evidenciados como los **mayores problemas** que tiene el país y para solucionarlos, proponen que la educación "sea un eje transformador" y el empleo digno sea la base para el bienestar social. Además, creen que se debe atender el desarrollo rural como una deuda histórica que tiene el país y "la correcta difusión de la información como una herramienta de garantía y transparencia en el contexto de la contienda electoral y en el futuro de la construcción de paz".

Igualmente, con la campaña **“No se lo dé a cualquiera”**, quieren incentivar la actividad política en la población más joven teniendo de presente la importancia del voto consciente y la participación. (Le puede interesar:["Jóvenes piden una alianza de candidaturas de centro izquierda"](https://archivo.contagioradio.com/jovenes-piden-a-formulas-vice-presidenciales-una-alianza-de-candidaturas-de-centro-izquierda/))

### **Jóvenes buscan difundir su agenda  de participación política** 

Luego de haber creado sus propuestas en una agenda juvenil, la incluyeron en la plataforma [El Avispero Mov](http://www.elavisperomov.org/) con la intención de “fomentar el liderazgo ciudadano” y así “presionar a los candidatos para que tomen nuestra agenda y la hagan parte de sus planes de desarrollo” afirmó Manuela Ángel. Para hacer esto, están buscando que las personas ingresen a la plataforma y con su firma se adhiera a la “comunidad de agentes de cambio” a través de la campaña “Más Voz Menos Ruido”.

Finalmente, Ángel recordó que todos los jóvenes pueden participar de esta iniciativa para acercarse cada vez más a la actividad política para demostrar que **“los jóvenes podemos aportarle mucho al país”**. Una vez finalicen las elecciones, quieren desarrollar una veeduría ciudadana para asegurar que el presidente electo incluya las necesidades de los jóvenes.

<iframe id="audio_25793694" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_25793694_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]

<div class="osd-sms-wrapper">

</div>
