Title: Militares ingresaron a la zona Humanitaria de Nueva Vida en Curvaradó
Date: 2016-12-23 10:47
Category: DDHH, Nacional
Tags: militares, Zona Humanitaria de Nueva Vida
Slug: militares-ingresaron-a-la-zona-humanitaria-de-nueva-vida-en-curvarado
Status: published

###### [Foto: Confidencialcolombia.com] 

###### [23 Dic 2016]

**Un grupo de 12 militares ingresó el pasado 22 de diciembre a la Zona Humanitaria de Nueva Vida**, en Curvaradó, señalando que se encontraban allí presentes atendiendo un llamado por parte de la comunidad para auxiliar a dos personas que se encontraban enfermas. Información que fue desmentida por los habitantes de la zona humanitaria.

En este lugar se encontraban los y las defensores de derechos humanos de la Comisión Intereclesial de Justicia y Paz y EL Diálogo Intereclesial por la Paz (DIPAZ); a ellos se refirió el Teniente Candela, quién además de justificar su presencia por solicitud de la comunidad, también afirmó "**que ellos entendían que no debían estar dentro de la zona humanitaria, pero como ellos analizaban que había cambiado la intensidad del conflicto, no era riesgoso estar dentro de la zona**".

A lo que los defensores de derechos humanos respondieron  que “aunque había una acuerdo entre el gobierno y las FARC-EP, en el territorio había presencia de otros actores armados” como los neoparamilitares y el  ELN; agregando  “que en el contexto del conflicto se debía **regir sus accionar militar en el marco de los derechos humanos y el Derecho Internacional Humanitario**”. De igual forma señalaron  que hasta este momento el espacio aún está declarado como Zona Humanitaria” exigiendo el respeto al espacio y las personas que allí viven.

No obstante esta no ha sido la primera vez que han ingresado a la Zona Humanitaria, el 14 de diciembre también ingresaron a hacer uso del Kiosco vive digital, en ese momento la comunidad le **recordó a los militares que su presencia e este lugar o de cualquier actor armado no era permitida, a lo que el  Teniente hizo caso omiso**. Le puede interesar: ["Al aire, emisora comunitaria en zona humanitaria de Curvaradó"](https://archivo.contagioradio.com/al-aire-emisora-comunitaria-en-zona-humanitaria-de-curvarado/)

Frente a estos hechos los defensores indicaron que es preocupante el hecho de “m**ientras grupos armados tienen libre movilidad en el territorio** no hay ningún tipo de acción por parte del Estado”; y por otro lado si fuese el caso, el Ejercito no debería suplir deberes que tiene la parte civil del Estado de brindar atención en términos de salud a las personas. Le puede interesar:["Comunidad de zona humanitaria Nueva Vida amenazados por presencia paramilitar"](https://archivo.contagioradio.com/comunidades-de-nueva-vida-amenazados-por-presencia-paramilitar/)

######  Reciba toda la información de Contagio Radio en [[su correo]
