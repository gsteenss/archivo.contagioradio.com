Title: Celebración del saqueo
Date: 2015-10-15 12:13
Category: Eleuterio, Opinion
Tags: colombia, Día de la raza, Eleuterio Gabon, españa, Independencia, nada que celebrar
Slug: celebracion-del-saqueo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2015/10/Captura-de-pantalla-2015-10-15-a-las-12.10.53.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: RadioTemblorVideo] 

#### **[Eleuterio Gabón]** 

###### [12 de Oct 2015] 

No es nada nuevo que la fundación de las pretendidas grandes naciones remita a grandes héroes, a épicas hazañas y a horas precisas de gloria; ecos de leyenda que culminan con la aparición del nuevo esplendor que deja atrás un pasado incierto.

[Pero la escrupulosa historia, no la que representan viejos estandartes ni la que cantan los himnos pretenciosos, sino la de los hechos rigurosos, muestra como esos héroes suelen ser guerreros sin escrúpulos, como las pretendidas glorias son recuerdos de masacres y en definitiva como la mayoría de las naciones se han forjado a partir de la destrucción y el saqueo de otros pueblos.]

[De allí donde soy, el mes de octubre es por excelencia el mes de la patria. En apenas tres días, del 9 al 12, se concentran la exaltación de la patria regional (la valenciana) y la nacional (la española).  En Valencia  se celebra el día de su “liberación”, en el recuerdo de un 9 de octubre de ocho siglos atrás, cuando un rey cristiano puso fin a la cultura musulmana en el litoral levantino. O dicho de otro modo, celebra el saqueo, la expulsión y la persecución de un pueblo que llevaba cultivando su vida en nuestra geografía durante más de cinco siglos. Un pueblo que arraigó en estas tierras, entre otras cosas, la que tal vez sigue siendo su mayor seña de identidad, el arte de saber cultivar, como atestigua la fértil huerta que rodea la ciudad, amenaza hoy por el llamado progreso. En definitiva, El ]*[nou d´octubre]*[ señala el saqueo de un pueblo sobre otro, la imposición por la fuerza y el triunfo del fanatismo contra el diferente.]

Tal vez nos debería parecer extraño que nuestro reconocimiento como pueblo, sea el que sea, se señale en el recuerdo de la violencia ejercida sobre otros. ¿Tan triste es nuestra historia que no hay momento más humano con el que identificarnos? ¿O es que sólo la imposición por la fuerza es lo que legitima a un pueblo para sentirse digno?

Aquel mismo fanatismo culminaría con otra “reconquista”, la que funda en 1492 esa macabra leyenda de la España grande y libre, con la expulsión, previa masacre, de los últimos musulmanes de Granada. Cabe decir que los autoproclamados “reconquistadores” nunca lo fueron, sencillamente porque lo “reconquistado” jamás antes les había pertenecido. De la misma familia eran quienes se autodenominaron “descubridores” cuando el 12 de Octubre de ese mismo año, abrían las puertas de la historia a una de las mayores tragedias del saqueo que se conocen. En esta fecha se daba comienzo al exterminio y la esclavitud que todo un continente sufriría durante siglos a manos del fanatismo de una remota península. Así, España sella su fecha de nacimiento y el día de su orgullo como nación que celebra cada año sacando los tanques a la calle.

500 años después de la llegada de los saqueadores a América, muchos pueblos indígenas descendientes de aquellos pobladores originarios, siguen sufriendo y sobretodo, siguen resistiendo a los ataques de los descendientes de aquellos “descubridores”. Los hay para quienes estas fechas simbolizan 500 años de resistencia, son los mismos que escriben la otra cara la de la memoria y de la historia oficial. La historia de los pobres frente a la de los ricos, de los saqueados frente a los saqueadores, es por eso que hablamos aquí de saqueo porque no hay riqueza si no es a costa de la pobreza. Y no se engañen, por mucho que celebren fiestas nacionales y recuerden fechas históricas, los saqueadores no son tan patriotas, roban y extorsionan en su país igual que en el del vecino. Quizá por eso, parece lógico que aquellos que nos gobiernan hoy celebren con pompa y entusiasmo los saqueos de ayer.

[Para aquellos que se ofenden ante quienes no celebramos estas fechas y preguntan con recelo si no sentimos la patria, si me preguntan a mí, contestaré que cuando pienso en mi pueblo, sencillamente pienso en su clima, en su huerta, en el mar, en alguna música. También en su lengua, que es una mezcla de palabras griegas, latinas, árabes y castellanas, un legado vivo de los pueblos que vivieron aquí.]
