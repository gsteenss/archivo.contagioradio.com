Title: Sin voto de Colombia ONU adopta declaración por Derechos de los campesinos
Date: 2018-12-18 16:48
Author: AdminContagio
Category: Nacional
Tags: campesinos, DDHH, ONU
Slug: declaracion-derechos-campesinos
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/09/FOTO9.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### Foto:Archivo 

###### 18 Dic 2018 

Con la abstención de países como Colombia, Argentina y Brasil, durante la Sesión número 73 de la Asamblea General de las Naciones Unidas, adelantada en la ciudad de Nueva York, fue adoptada la **Declaración sobre los Derechos de los campesinos y otras personas que trabajan en zonas rurales**.

La determinación que exhorta a los estados a respetar los derechos,  protegerlos y hacerlos efectivos, fue aprobada con **121 votos a favor, 8 votos en contra y 54 abstenciones**,  y es resultado de un proceso histórico de 17 años adelantado por el **Movimiento Campesino Internacional Vía Campesina**, con el apoyo de organizaciones como FIAN y CETIM.

**Diego Montón**, integrante de la organización, "es un reconocimiento significativo de Naciones Unidas al **rol que cumple el campesinado tanto en la lucha contra el hambre como la mitigación del cambio climático** e incluso atacar de raíz el tema de las migraciones masivas y por lo tanto los Estados no pueden continuar con su característico ninguneo e invisibilidad" en cuanto a esos roles desde la perspectiva de la agricultura sostenible.

Para la ecuatoriana **María Fernanda Espinosa**, Presidenta de la sesión que contó con la representación de los 193 estados miembros, la declaratoria representa un hito histórico porque "**garantiza la protección de los derechos de los campesinos y de las poblaciones rurales en todos los países del mundo**"

La decisión representa el inicio de un nuevo capítulo en la lucha por los derechos de los campesinos y comunidades rurales en el mundo, como destaca Espinosa "No olvidemos que son los campesinos y particularmente las poblaciones rurales quienes **garantizan la seguridad alimentaria, aportan al desarrollo sostenible y a la protección de nuestro planeta**".

**Colombia se abstiene de votar una vez más**

**![Votación ONU](https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2018/12/Dup8iE0W4AE6vJt-800x379.jpg){.alignnone .size-medium .wp-image-59387 .aligncenter width="800" height="379"}**

Frente a la determinación del gobierno colombiano de abstenerse de votar favorablemente en la aprobación del acuerdo, diferentes organizaciones campesinas como la ANUC Popayán, el senador Alberto Castilla y la misma Vía campesina, han reprochado tal actitud, asegurando que "**se profundiza el modelo económico que no nos protege y nos despoja**".

Cabe recordar que el pasado 12 de diciembre, organizaciones  sociales y algunos congresistas de oposición manifestaron al Presidente Iván Duque, su inconformidad e indignación porque **en la votación realizada el 19 de noviembre Colombia asumió la misma posición**. (Le puede interesar: [Fallo histórico protegerá los territorios de títulos mineros](https://archivo.contagioradio.com/fallo-historico-protegera-los-territorios-de-titulos-mineros/))

"Verdaderamente **inaceptable que Colombia no haya votado favorablemente la declaración, cuando este es un país de tradición rural y campesina** y cuando en Colombia es el campesinado quien produce la mayor parte de los alimentos que sostiene la vida de la nación”, aseguraron en ese momento e instaron para cambiar su postura, lo que tampoco ocurrió en esta oportunidad.

Frente a estas determinaciones, Montón afirma que "Habran gobiernos que en primera instancia serán más amigos de la declaración y otros sobre los cuáles **va a depender más de como se agudice y se organice la resistencia y la lucha de las organizaciones del campo**, pero sobre todo también de la solidaridad, nosotros pensamos que esta declaración es una herramienta de diálogo por la paz en el campo" incluso con los Estados que no asumen abiertamente un compromiso con la misma.

[Declaración de las Naciones Unidas sobre los Derechos de los Campesinos y de Otras Personas que Trabajan en...](https://www.scribd.com/document/395959433/Declaracion-de-las-Naciones-Unidas-sobre-los-Derechos-de-los-Campesinos-y-de-Otras-Personas-que-Trabajan-en-las-Zonas-Rurales#from_embed "View Declaración de las Naciones Unidas sobre los Derechos de los Campesinos y de Otras Personas que Trabajan en las Zonas Rurales on Scribd") by [Contagioradio](https://www.scribd.com/user/276652802/Contagioradio#from_embed "View Contagioradio's profile on Scribd") on Scribd

<iframe id="doc_80115" class="scribd_iframe_embed" title="Declaración de las Naciones Unidas sobre los Derechos de los Campesinos y de Otras Personas que Trabajan en las Zonas Rurales" src="https://www.scribd.com/embeds/395959433/content?start_page=1&amp;view_mode=scroll&amp;access_key=key-MFeBguzIH3SmZs0eyT7g&amp;show_recommendations=true" width="100%" height="600" frameborder="0" scrolling="no" data-auto-height="false" data-aspect-ratio="0.7729220222793488"></iframe><iframe id="audio_30905667" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_30905667_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### Reciba toda la información de Contagio Radio en [[su correo]
