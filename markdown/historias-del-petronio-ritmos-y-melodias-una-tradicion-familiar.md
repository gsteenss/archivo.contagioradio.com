Title: Historias del Petronio: ritmos y melodías, una tradición familiar
Date: 2019-08-18 19:19
Author: CtgAdm
Category: Cultura
Tags: Cantaora, Marimba, Música, pacífico, Petronio Álvarez
Slug: historias-del-petronio-ritmos-y-melodias-una-tradicion-familiar
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2019/08/Javier.png" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio ] 

La familia Valencia García hace parte de la **Agrupación Uramba de López de Micay**, propuesta musical que Marcia, junto a sus hijos, Javier y Yohana han traído al Festival Petronio Álvarez. Con su marimba, sus tamboras, su guasa y su voz, hoy representan los ritmos de diversas regiones del Valle del Cauca como Guayabal, López Cabecera y el Naya.

Uramba, una palabra que congrega alrededor de la cultura la gastronomía, las bebidas, la música y el canto, generando unidad entre las comunidades con las que viene trabajando Javier, quien además de ser cantante y marimbero, realiza trabajo social  fomentando la unidad de la sociedad y de la familia desde sus saberes.

### Detrás del Petronio hay una historia familiar 

Javier y su familia llegan al Petronio en representación de un grupo de mujeres que destilan la caña de azúcar día tras día en su región, mujeres trabajadoras como es el caso de Marcia, la matriarca de los Valencia quien después del asesinato de su esposo, Norman en 1999 fue quien asumió el rol de padre y madre para sacar adelante a sus siete hijos, asumiendo el trabajo que dejó su pareja y convirtiéndose en la primera capitana de López de Micay.

"Mi padre transportaba víveres de Buenaventura a López de Micay, fue asaltado y asesinado en 1999, es un trabajo complicado y fuerte, pero mi mamá es una berraca y nosotros como hijos siempre resaltamos en ella ese valor, esa parte de resiliencia de poder seguir a pesar de las problemáticas que podamos tener en la vida" relata Javier.[.  (Le puede interesar: Historias del Petronio: Peinados afrocolombianos, un símbolo de resistencia)](https://archivo.contagioradio.com/historias-del-petronio-peinados-afrocolombianos-un-simbolo-de-resistencia/)

### "La música es lo que nos apasiona" 

De los siete hijos que hacen parte de esta familia - cuatro mujeres y tres hombres - son cuatro los que han demostrado interés y talento por la música,  "si bien digamos que cada saber es transmitido por abuelos, yo saqué el don de cantar, mi abuela cantaba mi mamá canta y a mi me encanta transmitir melodías, tengo otro hermano que toca el bombo, mi abuelo paterno y mis tíos también" expresa Javier quien afirma que esta unión de saberes permite una sinergia que conecta a la comunidad.

Desde pequeños, los hijos e hijas de Marcia fueron criados con la melodía de los **arrullos, jugas, alabaos y currulaos,** lo que según  Javier se le "impregnó en la sangre", un saber que transmite hoy  a los niños de su comunidad para que aprendan ese arte del pacifico "que es único y exclusivo".

"No a todos les nace cantar pero a mí sí me gustó y algunos de mis hijos  también nacieron con la misma mística, para mí es una alegría estar aquí compartiendo con ellos", afirma Marcia quien se mantuvo alejada de la música tras la muerte de su esposo, pero que ahora ha retomado junto a sus hijos, transmitiendo al cantar, el carisma familiar que los une.

**Síguenos en Facebook:**

<iframe style="border: none; overflow: hidden;" src="https://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fwww.facebook.com%2Fcontagioradio%2F&amp;width=450&amp;layout=standard&amp;action=like&amp;size=small&amp;show_faces=true&amp;share=true&amp;height=80&amp;appId=1237871699583688" width="450" height="80" frameborder="0" scrolling="no"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo][[Contagio Radio]
