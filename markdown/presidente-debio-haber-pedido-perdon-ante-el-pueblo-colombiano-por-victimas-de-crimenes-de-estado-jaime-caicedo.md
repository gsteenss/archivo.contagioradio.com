Title: "Presidente debió pedir perdón por víctimas de crímenes de Estado" Jaime Caicedo
Date: 2016-12-12 14:05
Category: Entrevistas, Movilización
Tags: Partido Comunista de Colombia, víctimas de crímenes de Estado
Slug: presidente-debio-haber-pedido-perdon-ante-el-pueblo-colombiano-por-victimas-de-crimenes-de-estado-jaime-caicedo
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/10/marcha-de-las-flores101316_274.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Contagio Radio] 

###### [12 Dic 2016] 

En un reciente comunicado de prensa emitido por parte del Partido Comunista de Colombia y del Unión Patriótica, expresaron su preocupación frente a la **invisibilización de las víctimas del Estado por parte del Presidente Santos, en su discurso pronunciado en Oslo**. El Partido Comunista afirmo que durante esta alocución no hubo “ni una referencia a las víctimas de los falsos positivos, ni al exterminio de la Unión Patriótica, hechos que desconocen la historia reciente del país”.

En el comunicado también exponen como el reconocimiento de las víctimas en su totalidad, y no solo las que han sido afectadas por la insurgencia, es un avance en la construcción de paz. De igual forma, señalan que esta **invisibilización da paso y aliento a los más de 100 asesinatos en contra de líderes del movimiento social** y defensores de derechos humanos que se han reportado este año en Colombia.

De acuerdo con Jaime Caicedo, miembro de la Unión Patriótica y vocero el Partido Comunista, esta es una realidad dolorosa “**el presidente debió haber pedido perdón ante el pueblo colombiano y ante la comunidad internacional** por todos los incidentes en donde el **Estado ha estado comprometido en el marco de una guerra contrainsurgente, contra civiles y activistas de la izquierda en Colombia.**

En este sentido, en el país el **98% de los delitos cometidos por agentes estatales se encuentran en la impunidad**, mientras  hay 4.475 víctimas de ejecuciones extrajudiciales atribuidas a la fuerza pública. Situaciones que aumentan la preocupación frente al proceso de paz, debido a que mientras se habla de la implementación de los acuerdos, continúan presentándose actos de violencia que ya cobró la vida de 124 personas.

De igual forma la Unión Patriótica en cabeza de Aida Avella, han venido denunciado que estos **actos son sistemáticos y que obedece a un plan para exterminar el pensamiento diverso** y han hecho el llamado al gobierno y a las instituciones para que no se repita otro genocidio como el de este partido políticol, del que aún poco  se conoce en términos de responsables. Le puede interesar:["Aquí no se han desmontado las estructuras paramilitares: Aida Avella"](https://archivo.contagioradio.com/aqui-no-se-han-desmontado-las-estructuras-paramilitares-aida-avella/)

“Es injusto que no se tenga en cuenta a las miles de víctimas de crímenes de Estado en el país, y esto tiene implicaciones políticas para el proceso de paz, porque quiere decir que continua la estigmatización que se ha venido denunciando” afirmó Caicedo. Le puede interesar: ["100% de violaciones a Derechos Humanos están impunes en Colombia"](https://archivo.contagioradio.com/100-violaciones-derechos-humanos-estan-impunes-colombia/)

<iframe id="audio_14901282" style="border: 1px solid #EEE; box-sizing: border-box; width: 100%;" src="https://co.ivoox.com/es/player_ej_14901282_4_1.html?c1=ff6600" width="300" height="200" frameborder="0" scrolling="no" allowfullscreen="allowfullscreen"></iframe>

###### [Reciba toda la información de Contagio Radio en][[su correo]](http://bit.ly/1nvAO4u)[ o escúchela de lunes a viernes de 8 a 10 de la mañana en Otra Mirada por][[Contagio Radio.]](http://bit.ly/1ICYhVU) 
