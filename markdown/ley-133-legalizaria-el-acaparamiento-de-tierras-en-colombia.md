Title: Ley 133 legalizaría el acaparamiento de tierras en Colombia
Date: 2014-12-02 04:00
Author: CtgAdm
Category: Ambiente, Judicial
Tags: acaparamiento, baldios, ley 133, Paramilitarismo, tierras
Slug: ley-133-legalizaria-el-acaparamiento-de-tierras-en-colombia
Status: published

###### Foto: nuestrasciudades.blogspot.com 

En rueda de prensa ofrecida por senadores y representantes a cámara del **Polo Democrático y del Partido Verde** hicieron varias **denuncias en contra de la conocida como ley 133 o ley sobre baldíos**, que hoy entró a primer debate en cámara.

Ellos denunciaron nuevos acaparamientos de tierras y futuro beneficio con la nueva ley por parte de ex ministros y familias influyentes del país, la **legalización de tierras obtenidas ilegalmente** o el método asociativo de Indupalma que obliga al campesinado a trabajar para multinacionales extranjeras o grandes empresarios del propio país.

También han aprovechado para realizar un recorrido sobre las distintas leyes similares que ya se habían presentado anteriormente, así como han denunciado la **inconstitucionalidad** de la misma. Por último han dejado claro que con esta ley se denomina al campesino, “trabajador agrario“, en vez de campesinado, contradiciendo la jurisprudencia de la corte constitucional.
