Title: Hay más de 700 solicitudes de amnistía a integrantes de las FARC sin respuesta
Date: 2017-02-03 12:50
Category: DDHH, Nacional
Tags: Implementación de los Acuerdos de paz, Ley de Amnistia, Presos políticos en Colombia
Slug: 700-solicitudes-de-amnistia-sin-respuesta
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2017/02/presos-politicos-climax-cover.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Posta] 

###### [3 Feb 2017] 

Hasta el momento se han presentado 700 solicitudes de amnistía, de las que no se ha recibido ni una sola respuesta afirmativa, organizaciones defensoras de derechos humanos como la Coalición Larga Vida a las Mariposas, han denunciado que **existe una intencionalidad de la rama judicial en retrasar el trámite** y aseguran que incluso hay algunos jueces que se han negado a recibir las solicitudes.

Mediante un comunicado, los prisioneros políticos de la cárcel Las Mercedes en Montería, denunciaron incumplimientos a lo pactado en La Habana y a las disposiciones de la Ley de Amnistía 1820 del 30 de diciembre de 2016. Manifestaron que perciben un **“afán de la rama judicial de dilatar la salida de los prisioneros políticos”**, y exigen al gobierno de Juan Manuel Santos, explique los motivos que tienen los Jueces y Fiscales para seguir haciendo caso omiso.

### Jueces piden requisitos que no hacen parte de la Ley 

Gustavo Gallardo, abogado de la Coalición, explicó que varios de los jueces han argumentado no poder resolver las solicitudes porque “están a la espera de una circular o directiva por parte del gobierno” que les diga cómo aplicar la Ley, sin embargo el abogado comenta que la Ley 1820 **“se expidió para su inmediato cumplimiento y en ningún caso el legislativo tiene la obligación de emitir un manual de aplicación”.**

Además, indicó que muchos jueces han exigido a los abogados defensores, “una carta o documento en el que las FARC-EP reconozcan al prisionero que presenta la solicitud de amnistía”, requisito que no está estipulado en la ley, explica Gallardo. ([Le puede interesar: Ley de amnistía es la más completa que se ha aprobado en Colombia](https://archivo.contagioradio.com/enrique-santiago/))

Agregó que la situación es preocupante para los más de 3300 prisioneros que estarían privados de su libertad por delitos políticos o conexos y que han sido reconocidos como amnistiables por la Oficina del Alto Comisionado de las Naciones Unidas.

Por último, la Coalición Larga Vida a las Mariposas ha dicho que se está preparando una denuncia pública y **“la solicitud de acciones disciplinarias para los jueces que se han negado a cumplir la Ley”**. ([Le puede interesar: Presos políticos siguen esperando gestos de paz del Gobierno](https://archivo.contagioradio.com/presos-politicos-siguen-esperando-gestos-de-paz-del-gobierno/))

###### Reciba toda la información de Contagio Radio en [[su correo]
