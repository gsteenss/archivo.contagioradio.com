Title: Asesinan a líder campesino William Castillo en el Bagre, Antioquia
Date: 2016-03-07 21:27
Category: DDHH, Nacional
Tags: Aheramigua, El Bagre Antioquia, Paramilitarismo, William Castillo
Slug: asesinan-a-lider-campesino-william-castillo-en-el-bagre-antioquia
Status: published
<div id="slider-div">
<img class="active" src="https://contagioradio.nyc3.cdn.digitaloceanspaces.com/wp-content/uploads/2016/03/william-castillo-contagioradio.jpg" alt="foto" style="display:block"></img><div class="btn-carousel" title="Anterior"  id="previous">&lt;</div><div class="btn-carousel" title="Siguiente" id="next">&gt;</div></div>

###### [Foto: Aheramigua] 

###### [07 Mar 2016] 

Según la denuncia de la **organización campesina Aheramigua, Wlliam Castillo, uno de los fundadores** de esa organización, fue asesinado por sicarios en el barrio Villa Echeverry del municipio del Bagre. Las primeras versiones dan cuenta de un ataque de sicarios que le dispararon en un establecimiento público hacia las 6 de la tarde de este Lunes.

Castillo, fue fundador de la **Asociación de Hermandades Agroecológicas y mineras de Guamocó** y fue uno de los líderes que denunció el desplazamiento de campesinos de la región por parte de paramilitares en el mes de Enero de este año. También estuvo al frente de las denuncias por la **[presencia paramilitar y contribuyó en la atención de las personas desplazadas.](https://archivo.contagioradio.com/familias-desplazadas-de-el-bagre-antioquia-exigen-garantias-para-retornar/)**

La situación en el municipio del Bagre sigue sin ser atendida de manera eficiente por las autoridades militares o civiles, de las que también se denunció su **[inoperancia frente al actuar de los paramilitares.](https://archivo.contagioradio.com/cada-tres-dias-asesinan-a-un-joven-en-puerto-claver-antioquia/)**

El muncipio y toda la región se mantienen bajo la constante amenaza de las empresas mineras de oro y otros minerales que operan en la región y que tienen más intereses en los territorios de los cuales fueron desplazados los campesinos y campesinas.
